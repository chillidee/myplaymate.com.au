<?php

namespace Dashboard;

use Controller, View, Config;
  
 class DirectiveController extends \Controller
  //class BusinessController extends Controller 
  {
  	public function getDirective($directive) {
      return View::make('directives/' . $directive );
  	}
  }