<div class="modal fade backdrop auth-modal" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<img src="/assets/frontend/img/logoblack.jpg" alt="MyPlaymate. The place to be.">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <p>If you have an account with us, please log in.</p>
            </div>
            <div class="modal-body">
			    {{ Form::open(array('route' => 'login','id'=>'login-form')) }}
					<div class="input-group">
			                {{ Form::text('email','',array('class'=>'form-control','placeholder'=>'Email')) }}
					  	<span class="input-group-addon">
					  		<img src="/assets/frontend/icons/pop-mail.png" alt="">
					  	</span>
					</div>

					<div class="input-group" id="login-password">
			                {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
					  	<span class="input-group-addon">
					  		<img src="/assets/frontend/icons/pop-padlock.png" alt="">
					  	</span>
					</div>

			        @if(Session::get('error'))
			            <div class="form-error" style="clear:both;">
			                <span>{{ Session::get('error') }}</span>
			            </div>
			        @endif

			        <div class="control-group">
			            <a class="btn" id="forgot-password">Forgot your password?</a>
			            <div class="controls">
			                {{ Form::submit('SIGN IN',array('class'=>'submit-button', 'id'=>'login-submit')) }}
			            </div>
			        </div>

					<div class="alignc">
				        <a class="swap-modal" href="#signup" data-dismiss="modal" data-toggle="modal">Not a Member yet? Click here!</a>						
					</div>
			    {{ Form::close() }}
            </div>
        </div>
	</div>
</div>



