<?php

namespace spec\MyPlaymate\Filters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Escort;

class EscortNameFilterSpec extends ObjectBehavior
{
	private $escorts = [
		[
			'escort_name' => 'ariana',
		],
		[
			'escort_name' => 'ginar',
		],
		[
			'escort_name' => 'maria'
		]
	];

    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Filters\EscortNameFilter');
    }

    function it_returns_3_for_empty()
    {
    	$this->apply($this->escorts,'','escort_name')->shouldHaveCount(3);
    }

    function it_returns_2_for_ari()
    {
    	$this->apply($this->escorts,'Ari','escort_name')->shouldHaveCount(2);
    }

    function it_returns_1_for_arian()
    {
    	$this->apply($this->escorts,'arian','escort_name')->shouldHaveCount(1);
    }


}
