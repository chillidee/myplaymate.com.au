<?php
use  Carbon\Carbon;
use MyPlaymate\Services\Meta;
use MyPlaymate\Services\FilterService;
use MyPlaymate\Repositories\EscortDbRepository;
use MyPlaymate\Services\LocationService;
  //use Carbon;

class EscortController extends \BaseController {

  protected $location;

  function __construct(LocationService $location)
  {
    $this->location = $location;
  }


  // protected $filterService;

  // protected $repository;

  // function __construct(FilterService $filterService, EscortDbRepository $repository)
  // {
  //   $this->filterService = $filterService;
  //   $this->repository = $repository;
  // }

  // public function loadEscorts()
  // {
  //   $escorts = $this->repository->getShowable();
  //   // return $escorts;

  //   foreach (Input::all() as $key => $value)
  //   {
  //     $escorts = $this->filterService->execute($escorts,$key,$value);
  //   }

  //   return $escorts;
  // }


  /**
   * Display a listing of the resource.
   * GET /escort
   *
   * @return Response
   */
  public function index()
  {
    $meta = Page::where('url_key','=','escorts')->first();
    $user = Auth::user();
    if ( !$user && isset($_COOKIE['mpm_session']) && file_exists('temp/' . $_COOKIE['mpm_session'] . '.js')) {
      $pass = array(
       'session' =>  'temp/' . $_COOKIE['mpm_session'] . '.js',
       'meta' => $meta,
       'escorts' => 'true'
       );
      return View::make('frontend.escorts.index',$pass );
    }
    else {
      $pass = array(
       'meta' => $meta,
       'escorts' => 'true'
       );
      return View::make('frontend.escorts.index',$pass); }
    }

  /**
   * Display a listing of the resource.
   * GET /city/{city_name}
   *
   * @return Response
   */
  public function city($city)
  {
   $cities = array(
    'brisbane', 'melbourne', 'sydney', 'perth', 'adelaide', 'tasmania', 'act', 'canberra', 'darwin', 'hobart' );
   if ( !in_array( $city, $cities ) )
     return Response::make('Page not found',404);
   $suburb_id = Suburb::whereName($city)->first()->id;

   if ($city == 'perth')
    $suburb_id = 9140;

  $meta = Page::where('url_key','=', $city . '_escorts')->first();
  return View::make('frontend.escorts.city',compact('city','suburb_id','meta'));
}

public function loadEscorts($amnt = 0)
{
  $input = Input::all();

    // Neutralize empty filters
  foreach ($input as $key => $value) {
    if (!isset($value))
     $value == '';
 }

    // Check if we're on a city page
 if ( isset( $input['city'] ) && $input['city'] != '' ){
  if ( $input['city'] == 'goldcoast' ) { $input['city'] = 'Gold Coast'; }
  $city = City::where( 'name','=',$input['city'])->first();
  if ( $city ){
    $input['latitude'] = $city->latitude;
    $input['longitude'] = $city->longitude;
  }
}
    // Sydney Fallback
if (!isset($input['latitude']) || $input['latitude'] == ''){
 $input['latitude'] = '-33.8693574';
 $input['longitude'] = '151.1109774';
}

    // Display only the approved escorts
$escorts = Escort::whereStatus(2);

if(!empty($input)){
  foreach ($input as $key => $value) {

    if ( $key == 'search'){

      $search = $input['search'];
      $escorts->where('escort_name', 'like', '%' . $search . '%' );
      $escorts->orWhere('phone', 'like', '%' . $search . '%' );
      $escorts->orWhere('email', 'like', '%' . $search . '%' );
      $escorts->orWhere('escorts.id', 'like', '%' . $search . '%' );
      $escorts->orWhere('seo_url', 'like', '%' .  $search . '%' );
      $escorts->orWhere('about_me', 'like', '%' . $search . '%' );
      $escorts->orWhere('seo_keywords', 'like', '%' . $search . '%' );
      $escorts->orWhere('notes', 'like', '%' . $search . '%' );
    }

        // AGE
    if ($key == 'age' && count($value)){
          // Convert the actual ages into ages_id for the escorts table
      $k = 0;

      foreach ($value as $i => $range) {
        if ( $k == 0 )
         $min_id = Age::whereName($range[0])->first()->id;
       $max_id = Age::whereName($range[1])->first()->id;
       $k++;
     }

     $escorts = $escorts->where(function($query) use ( $min_id,$max_id ){

      $query->whereBetween('age_id',[$min_id,$max_id]);

    });
   }

   if ($key == 'gender' && count($value)){
    $escorts = Escort::multiple_filter($value,'genders','gender_id',$escorts);
  }
  if ($key == 'ethnicity' && count($value)){
    $escorts = Escort::multiple_filter($value,'ethnicities','ethnicity_id',$escorts);
  }
  if ($key == 'body' && count($value)){
    $escorts = Escort::multiple_filter($value,'bodies','body_id',$escorts);
  }
  if ($key == 'services' && count($value)){
              // Convert the actual services into services_id"
              // for the escorts table
    foreach ($value as $service) {
      $service_id = Service::whereName($service)->first()->id;
      $services[] = $service_id;
    }

              // Fetch the pivot table for escorts associated to each service
    $escort_ids = [];
    foreach ($services as $service) {
      $escorts_array = EscortService::whereServiceId($service)
      ->get(['escort_id']);

      foreach ($escorts_array as $escort) {
                  // Avoid duplicates
        if(!in_array($escort->escort_id, $escort_ids))
          $escort_ids[] = $escort->escort_id;
      }
    }
              // Add the where and orWhere queries
    $escorts = $escorts->where(function($query) use ($escort_ids){

      $query->where('id','=',$escort_ids[0]);
      array_shift($escort_ids);

      foreach ($escort_ids as $i => $escort_id) {
        $query = $query->orWhere('id',$escort_id);
      }
    });
  }

  if ($key == 'hourly_rates' && count($value)){
          // Get the escorts with a hourly rate in the ranges
    foreach ($value as $i => $range) {
      $escorts_array = EscortRate::whereBetween('incall_1h_rate',array($range[0],$range[1]))
      ->orWhereBetween('outcall_1h_rate',array($range[0],$range[1]))
      ->get(['escort_id']);

      foreach ($escorts_array as $escort)
        $escort_ids[] = $escort->escort_id;
    }

          // Add the where and orWhere queries
    $escorts = $escorts->where(function($query) use ($escort_ids){

      $query->where('id','=',$escort_ids[0]);
      array_shift($escort_ids);

      foreach ($escort_ids as $i => $escort_id) {
        $query = $query->orWhere('id',$escort_id);
      }
    });
  }

  if ($key == 'hair_color' && count($value)){
    $escorts = Escort::multiple_filter($value,'hair_colors','hair_color_id',$escorts);
  }
  if ($key == 'eye_color' && count($value)){
    $escorts = Escort::multiple_filter($value,'eye_colors','eye_color_id',$escorts);
  }
  if ($key == 'height' && count($value)){
    $escorts = Escort::multiple_filter($value,'heights','height_id',$escorts);
  }
  if ($key == 'incallsOutcalls' && count($value))
    foreach ($value as $i => $element) {
      if ($element == 'Incalls')
        $escorts = $escorts->whereDoesIncalls(1);
      if ($element == 'Outcalls')
        $escorts = $escorts->whereDoesOutcalls(1);
    }

    if ($key == 'business' && $value != ''){
      $selected = [];
      foreach (Escort::has('businesses')->get() as $key => $escort) {
        foreach ($escort->businesses as $key => $business) {
          if ($business->type == $value)
            $selected [] = $escort->id;
        }
      }

          // Add the where and orWhere queries
      $escorts = $escorts->where(function($query) use ($selected){
        $query->where('id','=',array_shift($selected));
        foreach ($selected as $i => $escort_id)
          $query = $query->orWhere('id',$escort_id);
      });
    }
  }
}
$escorts = $escorts->orderBy('bumpup_date','desc');
    // Infinite scroll

$escorts = $escorts->get(['id','under_business','suburb_name','business_id','escort_name','age_id','suburb_id','seo_url','main_image_id','latitude','longitude','user_id','status','bumpup_date']);

    // Get coordinates
$user_lat = $input['latitude'];
$user_lng = $input['longitude'];

    //Get ready to grab what we need
$take = array();
$save = array();
$i = 0;
$amntArray = array();

foreach ($escorts as $key => $escort) {

  if ( $escort->status != 2 )
   continue;

    //Remove escorts from users on restricted plans

 $user = User::where('id','=',$escort->user_id )->first();
 if ( $user && $user->super_user != 1 ){
   $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
   if ( $plan ){
    if ( $plan->id == 8 || $plan->id == 9 )
     continue;
 }
 else {
  continue;
}
}
if ( !$user )
 continue;

    //Make Escorts under businesses use their businesses location

if ( $escort->under_business == 1 && $escort->business_id != '' ){
  $business = Business::where('id','=',$escort->business_id )->first();

          if ( $business ){  //Check we have a business first

              if ( $business->status != 2 ) //Take out escorts under businesses not approved
              continue;

             if ( $business->type !=  2){ //Make escorts under brothels and massage parlours use thier businesses location

               $escort->longitude = $business->longitude;
               $escort->latitude = $business->latitude;
             }
            if ( $business->type == '1'){ //Escorts under brothels user their brothels url

              $escort->busUrl = '/brothels/' . $business->seo_url . '?esc=' . $escort->id;
            }
            else if( $business->type == '3' ){  //Escorts under massage parlours use thier busineses url

              $escort->busUrl = '/massage-parlours/' . $business->seo_url;
            }
          }
        }

        $escort->distance = $this->location->calculateDistance($escort->latitude, $escort->longitude, $input['latitude'], $input['longitude'] );

        $distance = ( isset( $input['distance'] ) ? $input['distance'] : 50 );

        if ( isset( $input['lock'] ) && $input['lock'] != 0 ) {
         $distance = 100000;
       }
       else {
        $lock = 0;
      }


      if ($escort->distance < $distance ) {

        if ( $amnt != 0 ){
         if ( $i < $amnt ){
           $amntArray[] = $escort;
         }
         else {
           return $amntArray;
         }

       }

       if(EscortImage::find($escort->main_image_id))
         $escort->mainImageUrl = $escort->main_image_id > 0 ? EscortImage::find($escort->main_image_id)->filename : '';

       $escort->escortAge = $escort->age_id ? $escort->age->name : '';

      $escort->rounded_distance = 50 * floor($escort->distance / 50);


       if ( isset( $input['lock'] ) && $input['lock'] != 0 ){
        $take[] = $escort;
      }
      else {
        if ( $i < 60 ) {
         $take[] = $escort;
       }
       else {
         $save[] = $escort;
       }
     }
     $i++;
   }
 }
 if ( isset( $input['lock'] ) && $input['lock'] != 0 ){
  function array_orderby()
  {
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
      if (is_string($field)) {
        $tmp = array();
        foreach ($data as $key => $row)
          $tmp[$key] = $row[$field];
        $args[$n] = $tmp;
      }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
  }
  $take = array_orderby($take, 'rounded_distance', SORT_ASC, 'bumpup_date', SORT_DESC );
  $take = array_slice ( $take, 0 ,12 );
}
$filter_string = '';
$function_string = '';
foreach( $input as $key => $filter ) {
  if ( $filter != '' && $key  != 'sorting' && $key != 'distance' && $key  != 'latitude' && $key != 'lock' && $key  != 'longitude' && $key != 'business' && $key  != 'city' && $key  != 'page' && $key  != 'paginationCount') {
    $filter_string.= ucfirst(str_replace ('_',' ',$key)) .  ' : ';
    if ( is_array( $filter ) ) {
      foreach ( $filter as $value ) {
        if ( is_array ( $value ) ) {
         $i = 0;
         foreach ( $value as $a => $value ) {
           if ( $i < 1 ) {
             $filter_string.= $value . ' - ';
             $function_string .= 'addUsedFilter($( "#' . $value;
             if ( $key == 'hourly_rates' )
              $function_string .= '-';
          }
          else {
           $filter_string.= $value . ' | ';
           $function_string .= $value . '"));';
         }
         $i++;
       }
     }
     else {
      $function_string .= 'addUsedFilter($( "#' .  str_replace ('+','',str_replace ('/','', str_replace (' ', '-', strtolower($value) ) ) ). '" ) );';
      $filter_string.= $value. ' | ';}
    }
  }
}
}

$touringSoon = EscortTouring::where('from_date','<=', date('Y-m-d H:i:s',time() + 1209600 ))->where('to_date','>=',date('Y-m-d H:i:s',time() ))->get();
$escortsTouring = array();
$escortsCheck = array();
foreach( $touringSoon as $tour ){
  $distance = ( isset( $input['distance'] ) ? $input['distance'] : 50 );
  $tour->distance = $this->location->calculateDistance($tour->latitude, $tour->longitude, $input['latitude'], $input['longitude'] );
  if ($tour->distance < $distance ) {
   $escort = Escort::where('id','=',$tour->escort_id )->first();

           if ( !$escort || $escort->status != 2 ) //Remove escorts that are not approved
           continue;
           $escortUser = User::where('id','=',$escort->user_id )->first();

           //Remove escorts on a restricted plan

           if ( $escortUser && $escortUser->super_user != 1 ){
            $plan = BusinessPlan::where('id','=',$escortUser->business_plan )->first();
            $escort->plan = $plan;
            $escort->user = $escortUser;
            if ( $plan ){
             if ( $plan->id == 8 || $plan->id == 9 ){
               continue;
             }
           }
           else {
             continue;
           }
         }

         if ( $escort->under_business == 1 && $escort->business_id != '' ){
          $business = Business::where('id','=',$escort->business_id )->first();

                if ( $business ){  //Check we have a business first

                  if ( $business->status != 2 ) //Take out escorts under businesses not approved
                  continue;

                  if ( $business->type == '1'){ //Escorts under brothels user their brothels url

                    $escort->busUrl = '/brothels/' . $business->seo_url . '?esc=' . $escort->id;
                  }
                    else if( $business->type == '3' ){  //Escorts under massage parlours use thier busineses url

                     $escort->busUrl = '/massage-parlours/' . $business->seo_url;
                   }
                 }
               }
               if ( !in_array ( $escort->id, $escortsCheck )){
                 $escort->touringLocation = $tour->suburb_name;
                 $escort->distance = $tour->distance;
                 if(EscortImage::find($escort->main_image_id))
                   $escort->mainImageUrl = $escort->main_image_id > 0 ? EscortImage::find($escort->main_image_id)->filename : '';

                 $escortsCheck[] = $escort->id;
                 $escortsTouring[] = $escort;
               }
             }

           }
           $filterString = substr($filter_string, 0, -3);
           if ( $filterString != '' ) $filterString .= '<button id = \'clearSearch\'>Clear Search Results</button>';
           $sorted = array(
             'show' => $take,
             'save' => $save,
             'filters' => $filterString . '',
             'touring' => $escortsTouring
             );
           if ( isset( $input['lock']) && $input['lock'] == 0 ){
            if ( !isset( $_COOKIE['mpm_session'] ) || $_COOKIE['mpm_session'] == '' ) {
              function generateRandomString($length = 10) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                  $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
              }
              $lar = generateRandomString(15);
      setcookie('mpm_session',$lar,time() + 3600,'/'); // i hour
    }
    else { $lar = $_COOKIE['mpm_session']; }
    if ( isset($input['city'] ) && $input['city']  != '' ) $lar = $input['city']  . '-' . $lar;
    if( isset($input['business'] )&& $input['business'] != '' ) $lar = 'bus-' . $input['business']  . '-' . $lar;
    $handle = fopen('temp/' .$lar . '.js', 'w');
    $data = '
    window.check = ' . json_encode ($sorted) . '
    jQuery(document).ready(function($) {
      $( "#filter_information" ).html( "' . $filterString . '" );
      ' . $function_string . '
    });';
    fwrite($handle, $data);
    fclose($handle);
  }
  return json_encode ($sorted);

}

public function set_is_wishlist($escort)
{
  $user_wishlist = Wishlist::whereUserId(Auth::user()->id)->get();

  foreach ($user_wishlist as $key => $wish) {
    if ($wish->escort_id == $escort->id){
      $escort->isWishlist = true;
      break;
    }
    else
      $escort->isWishlist = false;
  }
}

public function increaseViewCounter($escort_id)
{
  $escort = Escort::find($escort_id);
  $escort->count_views++;
  $escort->save();

  $counter = new Visit;
  $counter->escort_id = $escort_id;
  $counter->date = Carbon::now();
  $counter->save();

  return $escort->count_views;
}

public function postPhoneReveal($escort_id)
{
 $escort = Escort::find($escort_id);
 $escort->count_phone++;
 $escort->save();

 $counter = new PhoneCall;
 $counter->escort_id = $escort_id;
 $counter->date = Carbon::now();
 $counter->save();
}

public function sendPlayMail()
{
  $input = Input::all();

  $escort = Escort::find($input['escort_id']);
  if ( $escort->under_business && $escort->business_id != '' ){
    $business= Business::where('id','=',$escort->id )->first();
    if ( $business ) {
      $escort->email = $business->email;
    }
  }
  $success = $escort->sendPlayMail($input);

  if ($success) {
    $escort->count_playmail++;
    $escort->save();
    $counter = new Playmail;
    $counter->escort_id = $input['escort_id'];
    $counter->date = Carbon::now();
    $counter->save();
  }

        // return $success;
  return 'Playmail sent correctly to '.$escort->escort_name.'!';
}
public function sendUserPlayMail()
{
  $input =  Response::json(Input::all());
  $input = json_decode( $input->getContent());
  $escort = Escort::find($input->escort_id );
  $data =  (array) $input;
  $success = $escort->sendPlayMail($data);

  if ($success) {
    $escort->count_playmail++;
    $escort->save();
    $counter = new Playmail;
    $counter->escort_id = $data['escort_id'];
    $counter->date = Carbon::now();
    $counter->save();
  }

  if ( $success == 1 ) {
    return 'success!';
  }
}

public function leaveReview()
{
  $input = Input::all();
  $review = new EscortReview();

  $review->escort_id   = $input['escort_id'];
  $review->name       = $input['name'];
  $review->review     = $input['review'];
  $review->rating     = $input['rating'];
  $review->status     = 1;
  $review->save();

  $success = Escort::find($input['escort_id'])->sendReviewMail();

  return 'Review registered correctly! Just wait for the staff approval to see it listed.';
}

public function redirectToShow($id)
{
  $escort = Escort::find($id);
  if (!$escort)
    return Redirect::to('/',301);

  return Redirect::to('/escorts/'.$escort->seo_url);

}
  /**
   * Display the specified resource.
   * GET /escort/{slug}
   *
   * @param  string  $slug
   * @return Response
   */
  public function show($slug)
  {
    $escort = Escort::whereSeoUrl($slug)->first();

    //  if(!$escort || $escort->active == 0 || $escort->status == 0 || $escort->status == 1  || $escort->status == 3)
    if(!$escort || $escort->active == 0 )
    {
     $slugParts = explode('-', $slug);
     $escortId = (int)array_pop($slugParts);

     if ($escortId > 0)
       return Redirect::to('/escorts/'.$escortId, 301);
     else
       return Redirect::to('/', 301);
   }

   if ( $escort->under_business == 1 || $escort->business_id != '' ){
     $business = Business::where('id','=',$escort->business_id )->first();
     if ( $business ){

              if ( $business->type == '1'){ //Escorts under brothels user their brothels url

                return Redirect::to( '/brothels/' . $business->seo_url . '?esc=' . $escort->id );
              }
            else if( $business->type == '3' ){  //Escorts under massage parlours use thier busineses url

              return Redirect::to( '/massage-parlours/' . $business->seo_url );
            }
          }
        }

        if($escort->status == 2 || $escort->status == 4 ) {
         //  $distance = $this->location->getCurrentDistance($escort, $this->location->getCurrentSuburb());
          $distance = 50;
          if ( isset($_COOKIE['latitude'] ) && isset($_COOKIE['longitude'] )){
            $escort->distance = $this->location->calculateDistance($escort->latitude, $escort->longitude, $_COOKIE['latitude'], $_COOKIE['longitude'] );
          }
          else {
            $escort->distance = 'NULL';
          }


          $meta = new Meta($escort->seo_title,$escort->seo_keywords,$escort->seo_description);

          $reviews = EscortReview::whereEscortId($escort->id)
          ->whereStatus(2)
          ->wherePublished(1)
          ->get();

          $tourings = EscortTouring::where('to_date','>=',date('Y-m-d H:i:s',time() ))->where('escort_id','=',$escort->id)->get();
          $rates = EscortRate::where('escort_id','=',$escort->id )->first();
          $bodyClass = 'escort-single';

          $user = User::where( 'id','=',$escort->user_id )->first();
          if ( !$user )
            $user = new User;

          if ( $escort->status == 4 || $user->super_user != 1 && $user->business_plan == 8 || $user->business_plan == 9 ){
       //  return View::make('frontend.escorts.index',array( compact('escort','reviews','tourings','meta', 'bodyClass','distance'), $esc_cont ));
           $pass = array(
            'meta' => $meta,
            'escort' => $escort,
            'escorts' => 'true',
            'status' => 'unavailable'
            );
           return View::make('frontend.escorts.index',$pass);
         }

         $escort->hair_color = HairColor::where('id','=',$escort->hair_color_id )->first();
         $escort->eye_color = EyeColor::where('id','=',$escort->eye_color_id )->first();
         $user = Auth::user();
         if ( $user ){
           $wishlist = Wishlist::where('escort_id','=',$escort->id )->where( 'user_id','=',$user->id )->first();
           if ( $wishlist )
             $escort->favourite = 1;
         }

         return View::make('frontend.escorts.show',compact('escort','reviews','tourings','meta', 'bodyClass','distance','rates'));
       }
       else {
         return Redirect::to('/', 301);
       }

     }

     public function escortProfilePreview($id)
     {
       $escort = Escort::find($id);
       if ( !$escort )
         return 'No escort found with that id';
       $meta = new Meta($escort->seo_title,$escort->seo_keywords,$escort->seo_description);

       $distance = $this->location->getCurrentDistance($escort, $this->location->getCurrentSuburb());

       $reviews = EscortReview::whereEscortId($escort->id)
       ->whereStatus(2)
       ->wherePublished(1)
       ->get();
       $tourings = EscortTouring::whereEscortId($escort->id)->where('to_date','>=',date('Y-m-d'))->get();
       $bodyClass = 'escort-single';
       $escort->preview = 1;
       $escort->banner_approved = 2;
       $rates = EscortRate::where('escort_id','=',$escort->id )->first();

       return View::make('frontend.escorts.show',compact('escort','reviews','rates','tourings','meta', 'distance','bodyClass'));
     }
   }
