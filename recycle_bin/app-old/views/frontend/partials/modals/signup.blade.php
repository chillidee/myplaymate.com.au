<div class="modal fade backdrop auth-modal" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<img src="/assets/frontend/img/logoblack.jpg" alt="MyPlaymate. The place to be.">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <p>CREATE AN ACCOUNT</p>
            </div>
            <div class="modal-body">
			    {{ Form::open(array('route' => 'postRegister','id'=>'login-form')) }}
			        <div class="control-group">
			            <div class="controls">
			                {{ Form::text('full_name','',array('class'=>'form-control','placeholder'=>'Name')) }}
			            </div>
			        </div>
				    @if($errors->has('full_name'))
				    <p class="form-error">
				        {{ $errors->first('full_name') }}
				    </p>
				    @endif
			        <div class="control-group">
			            <div class="controls">
			                {{ Form::text('email','',array('class'=>'form-control','placeholder'=>'Email address')) }}
			            </div>
			        </div>
				    @if($errors->has('email'))
				    <p class="form-error">
				        {{ $errors->first('email') }}
				    </p>
				    @endif
			        <div class="control-group">
			            <div class="controls">
			                {{ Form::text('phone','',array('class'=>'form-control','placeholder'=>'Phone number')) }}
			            </div>
			        </div>
				    @if($errors->has('phone'))
				    <p class="form-error">
				        {{ $errors->first('phone') }}
				    </p>
				    @endif
			        <div class="control-group">
			            <div class="controls">
			                {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
			            </div>
			        </div>
				    @if($errors->has('password'))
				    <p class="form-error">
				        {{ $errors->first('password') }}
				    </p>
				    @endif
			        <div class="control-group">
			            <div class="controls">
			                {{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Repeat password')) }}
			            </div>
			        </div>

					<div id="terms-and-conditions" class="checkbox">
						<label for="terms_and_conditions">
						    <input type="checkbox" name="terms_and_conditions"/> I agree with <a href="/advertiser-terms-and-conditions" target="_blank">terms of use</a>
						</label>
					</div>
				    @if($errors->has('terms_and_conditions'))
				    <p class="form-error">
				        {{ $errors->first('terms_and_conditions') }}
				    </p>
				    @endif

			        <div class="control-group">
			            <div class="controls">
			                {{ Form::submit('REGISTER',array('class'=>'submit-button', 'id'=>'register-submit')) }}
			            </div>
			        </div>

					<div class="alignc">
				        <a class="swap-modal" href="#login" data-dismiss="modal" data-toggle="modal">Are you a current member? Click here!</a>						
					</div>
			    {{ Form::close() }}
            </div>
        </div>
	</div>
</div>



