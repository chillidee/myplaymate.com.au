<?php

class ProfilesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //DB::table('profiles')->truncate();

        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 50; $i++) {
            $user = new EscortUser();
            $user->fullname = $faker->name;
            $user->email = $faker->firstName('female').'@webcomm.io';
            $user->password = Hash::make('test123');
            $user->type = 'escort';
            $user->active = true;

            $user->save();

            $profile = $user->profile;
            $profile->approved = $faker->boolean();
            $profile->theme = 'layout1';
            $profile->use_own_url = true;
            $profile->escort_name = $faker->name;
            $profile->email = $user->email;
            $profile->does_incalls = $faker->boolean();
            $profile->does_outcalls = $faker->boolean();
            $profile->does_travel_internationally = $faker->boolean();
            $profile->does_smoke = $faker->boolean();
            $profile->has_tattoos = $faker->boolean();
            $profile->has_piercings = $faker->boolean();
            $profile->is_approved = $faker->boolean();
            $profile->is_active = $faker->boolean();
            $profile->about_me = $faker->realText();
            $profile->suburb_id = $faker->numberBetween(1, 13082);
            $profile->age_id = $faker->numberBetween(1, 53);
            $profile->gender_id = $faker->numberBetween(1, 3);
            $profile->body_id = $faker->numberBetween(1, 11);
            $profile->height_id = $faker->numberBetween(1, 5);
            $profile->hair_colour_id = $faker->numberBetween(1, 8);
            $profile->eye_colour_id = $faker->numberBetween(1, 6);
            $profile->ethnicity_id = $faker->numberBetween(1, 10);
            $profile->featured = $faker->boolean();
            $profile->save();

            for ($j = 1; $j < 5; $j++) {
                $file = \Faker\Provider\File::file(public_path().'/assets/frontend/img/profile', public_path().'/assets/frontend/img/profile', false);
                $image = new ProfileImage();
                $image->profile_id = $profile->id;
                $image->filename = $file;
                $image->crop = true;
                $image->order = $j;
                $image->approved = $faker->boolean();
                $image->save();

                $profile->main_image_id = $image->id;
                $profile->save();

                ImageHelper::createThumbnail(public_path().'/assets/frontend/img/profile/', $image->filename);
            }
        }
    }
}
