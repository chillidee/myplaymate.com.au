var gulp = require('gulp');
var csso = require('gulp-csso');
var csslint = require('gulp-csslint')
var jshint = require('gulp-jshint');
//var jsmin = require('gulp-jsmin');
var rev = require('gulp-rev');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var phpspec = require('gulp-phpspec');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var run = require('gulp-run');
var notify = require('gulp-notify');
var gutil = require('gulp-util');

gulp.task('css',function(){
  gulp.src([
    'public_html/assets/frontend/css/libs/*.css',
    'public_html/assets/frontend/css/animate.css',
    'public_html/assets/frontend/css/banner.css',
    'public_html/assets/frontend/css/bootstrap.min.css',
    'public_html/assets/frontend/css/dropzone.css',
    'public_html/assets/frontend/css/font-awesome.min.css',
    'public_html/assets/frontend/css/jquery-ui.min.css',
    'public_html/assets/frontend/css/lato.css',
    'public_html/assets/frontend/css/pgwslideshow.min.css',
    'public_html/assets/frontend/css/nivo-slider.css',
    'public_html/assets/frontend/css/owlcarousel.css',
    'public_html/assets/frontend/css/retina.css',
    'public_html/assets/frontend/css/search-select2.css',
    'public_html/assets/frontend/css/nivo-slider.css',
    'public_html/assets/frontend/css/select2.min.css',
    'public_html/assets/frontend/css/sidemenu.css',
    'public_html/assets/frontend/css/style.css',
    'public_html/assets/frontend/css/josephin.css',
    'public_html/assets/frontend/css/custom.css'
    ])
    .pipe(csslint())
    .pipe(concat({path:'build.min.css',cwd:''}))
    .pipe(csso())
    .pipe(rev())
    .pipe(gulp.dest('public_html/assets/frontend/css/builds'))
    .pipe(rev.manifest())
    .pipe(rename('css-manifest.json'))
    .pipe(gulp.dest('public_html/assets/frontend'));
});

gulp.task('js',function(){
  gulp.src([
    'public_html/assets/frontend/js/lib/jquery-ui.min.js',
    'public_html/assets/frontend/js/lib/jquery-ui-touch-punch.min.js',
    'public_html/assets/frontend/js/lib/jquery.radioImageSelect.js',
    'public_html/assets/frontend/js/lib/jquery.themepunch.plugins.min.js',
    'public_html/assets/frontend/js/lib/jquery.themepunch.revolution.min.js',
    'public_html/assets/frontend/js/lib/owl.carousel.min.js',
    'public_html/assets/frontend/js/lib/js.cookie.js',
    'public_html/assets/frontend/js/lib/pgwslideshow.min.js',
    'public_html/assets/frontend/js/lib/dropzone.js',
    'public_html/assets/frontend/js/lib/select2.full.min.js',
    'public_html/assets/frontend/js/lib/parsley.min.js',

    'public_html/assets/frontend/js/ajax-functions.js',
    'public_html/assets/frontend/js/functions.js',
    'public_html/assets/frontend/js/map.js',
    'public_html/assets/frontend/js/modals.js',
    'public_html/assets/frontend/js/statistics.js',
    'public_html/assets/frontend/js/topbar.js',
    'public_html/assets/frontend/js/wishlist.js',
    ])
    .pipe(concat({path:'build.min.js',cwd:''}))
  //  .pipe(rename('js-manifest.json'))
    .pipe(uglify())
    .pipe(rev())
    .pipe(gulp.dest('public_html/assets/frontend/js/builds'))
    .pipe(rev.manifest())
    .pipe(rename('js-manifest.json'))
    .pipe(gulp.dest('public_html/assets/frontend/'))
});

gulp.task('test', function() {
   gulp.src('app/spec/**/*.php')
       .pipe(run('clear').exec())
       .pipe(phpspec('', { notify: true }))
       .on('error', notify.onError({
           title: 'Crap!',
           message: 'Your tests failed!',
           icon: __dirname + '/fail.png'
       }))
       .pipe(notify({
           title: 'Success',
           message: 'All tests have returned green!',
           icon: __dirname + '/success.png'
       }));
});

gulp.task('watch',function(){
    gulp.watch('public_html/assets/frontend/css/*.css',['css']);
    gulp.watch(['app/spec/**/*.php', 'app/Myplaymate/**/*.php'], ['test']);
    gulp.watch('public_html/assets/frontend/js/*.js',['js']);
});

gulp.task('default', ['js', 'watch']);