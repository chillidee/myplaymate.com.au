<div class="container" id="spotlight-escorts">
    <!-- .row start -->
    <div class="row escorts-filters">
        <section class="col-lg-12">
            <h2 class="alignc">IN THE SPOTLIGHT</h2>
            <div class="divider"></div>
        </section>

        </div><!-- .row escorts-filters end -->
        <!-- Where we load Escorts Image Sets -->
        <div class="row escorts-items-holder triggerAnimation animated grid-view " data-animate="fadeInUp">
            @if(count($filteredSpotlightEscorts)==0)
            <div class="col-md-3 col-sm-4 col-xs-12" >No profiles near you are available for the moment.</div>
            @endif
            @foreach($filteredSpotlightEscorts as $escort)
            <!-- Escort -->
            <div class="col-md-2 col-sm-4 col-xs-6 escorts-wishlist">
                <div class="escorts-img-container" id="{{ $escort->id }}">
                    @if(Auth::check())
                    <div class="figfavorite">
                        <a href="" class="is-not-wish">
                            <i class="fa fa-heart"></i>
                        </a>
                        <a href="" class="is-wish">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    @else
                    <div class="figfavorite">
                        <a href="{{url('register')}}">
                            <i class="fa fa-heart"></i>
                        </a>
                    </div>
                    @endif
                    <a href="/escorts/{{ $escort->seo_url }}">
                        <div class="info-container"><!-- info-container-->
                            <div class="figinfo"><!-- figinfo-->
                                <span class="figname">{{ $escort->escort_name }} </span>
                                <ul class="figdata">
                                    <li>{{ $escort->defaultLocation->name }}</li>
                                    <li>{{ $escort->age->name }} yo</li>
                                </ul>
                            </div><!-- figinfo-->
                        </div><!-- end of info-container-->
                        @if($escort->main_image_id != 0 && $escort->mainImage && $escort->mainImage->status == 2)
                            <img src="{{ $escort->mainImage->thumbnail(220,330) }}" alt="{{ $escort->escort_name }}">
                        @else
                            <img src="assets/frontend/img/profile_template.jpg">
                        @endif
                    </a>
                </div>
            </div>
            <!-- End of Escort-->
        @endforeach
    </div><!-- End of Desktop Version -->
    <div class="row">
        <div class="col-md-12">
            <div id="viewall">
                <a href="/escorts">
                    <i class="fa fa-th"></i>
                    <span>View All</span>
                </a>
            </div>
        </div>
    </div>
</div><!-- .container end -->