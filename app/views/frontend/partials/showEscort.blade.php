<div class ='container'>
  <div class ='row'>
    <div class ='col-md-8 col-lg-7'>
      @include( 'frontend.partials.escortSlide')
    </div>
    <div class ='col-md-4 col-lg-5'>
       <div id ='_busLogo'>
           <img src ='{{ Config::get('constants.CDN') }}/businesses/{{$business->image}}'>
       </div>
       <h3>{{$escort->escort_name}}</h3>
       <div>Age: {{$escort->escortAge}}</div>
       @if( isset( $business->phone ))  <div class ='actionPane actionPhone _showPhone' data-num ='{{Helpers::format_phone_number($business->phone)}}'>Ph: {{ substr(Helpers::format_phone_number($business->phone), 0, 7)}}...</div>@endif
       <p>{{nl2br ( $escort->about_me ) }}</p>
    </div>
</div>
</div>
