<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>MyPlayMate</title>
<link rel="stylesheet" href="{{ asset('assets/admin/css/style.default.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/style.shinyblue.css') }}" type="text/css" />
<script type="text/javascript" src="{{ asset('assets/admin/js/jquery-1.9.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/jquery-migrate-1.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/modernizr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/jquery.cookie.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/custom.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner">

        <div class="logo animate0 bounceIn"><img style="width: 270px;" src="{{ asset('assets/frontend/img/logo.png') }}" alt="myplaymate.com.au" /></div>
        <form id="login" action="" method="post">
            @if(Session::has('message_error'))
            <div class="inputwrapper login-alert">
                <div class="alert alert-error">{{ Session::get('message_error') }}</div>
            </div>
            @endif
            <div class="inputwrapper animate1 bounceIn">
                <input type="text" name="email" id="email" placeholder="Enter your email" />
            </div>
            <div class="inputwrapper animate2 bounceIn">
                <input type="password" name="password" id="password" placeholder="Enter your password" />
            </div>
            @include('admin.layout.messages')
            <div class="inputwrapper animate3 bounceIn">
                <button name="submit">Sign In</button>
            </div>

        </form>
    </div><!--loginpanelinner-->
</div><!--loginpanel-->

<div class="loginfooter">
    <p>&copy; {{ date('Y') }}. MyPlayMate. All Rights Reserved.</p>
</div>

</body>
</html>
