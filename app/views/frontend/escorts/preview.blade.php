@extends('frontend.layout')

@section('content')
<script>window.active_id = {{$escort->id}};</script>
@if ( $escort->banner_image )
<img class="full_width" src="https://d2enq9fr2fmuvm.cloudfront.net/escorts/{{$escort->banner_image}}">
@endif
    <div id="page-title" class="page-title-2" data-escort-id="{{ $escort->id }}">
        <div class="container">
            <div>
                <div class="profile-image col-md-2">
                    @if($escort->main_image_id > 0 && $escort->mainImage && $escort->mainImage->status == 2)
                        <img src="{{ $escort->mainImage->thumbnail(170, 170) }}" alt="{{ $escort->escort_name }}">
                    @else
                        <img src="/assets/frontend/img/profile_template.jpg" style="width:174px; height:174px;">
                    @endif
                </div>
                
                <div class="profile-name">
                    <span>{{ $escort->escort_name }}</span>
                    <ul>
                        <li>{{ $escort->defaultLocation->name }}</li>
                        <li>{{ $escort->gender->name  }}</li>
                        <li>{{ $escort->age->name  }} yo</li>
                    </ul>
                         @if($escort->seo_keywords != '')
                            <h1 class="keywords">{{ $escort->seo_keywords }}</h1>
                        @endif
                </div><!-- End of profile-name -->
                <div class="rating-widget-mobile">
                    <li class="rating-widget">
                        <a href="#leaveReviewModal" data-toggle="modal">
                        <?php
                            $sum = 0;
                            $avg = 0;
                            foreach ($reviews as $review)
                                $sum += $review->rating;

                            if (count($reviews)>0)
                                $avg = floor($sum / count($reviews));

                            for ($i = 1; $i <= 5; $i++) {
                                if ($i <= $avg)
                                    echo '<i class="fa fa-star"></i>';
                                else
                                    echo '<i class="fa fa-star-o"></i>';
                            }
                        ?>
                        </a>
                    </li>
                    <li><a class="add-review" data-toggle="modal" href="#leaveReviewModal">Add Review</a></li>   
                </div>
    

            </div><!-- End of Div -->
        </div><!-- End of Row -->
    </div><!-- End of profile Details -->

<div class="page-wrapper top-space">
    <div class="container">
    <!-- .row start -->
        <div class="row marginRow">
            <div class="col-md-8 col-xl-9">
                <div class="row">
                  
                  @include( 'frontend.partials.escortSlide')


                    <ul id="accordion">
                        <h3>INFO</h3>
                        <li class="active">
                            <div class="row">
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">GENDER</h4>
                                    @if(isset($escort->gender->name))
                                        <p class="alignc">{{ $escort->gender->name }}</p>
                                    @endif    
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">AGE</h4>
                                    @if(isset($escort->age->name))
                                        <p class="alignc">{{ $escort->age->name }}</p>
                                    @endif    
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 id="distance-title" class="alignc">DISTANCE FROM YOU</h4>
                                    <p class="alignc">~{{ $distance }} km</p>   
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">ETHNICITY</h4>
                                    @if(isset($escort->ethnicity->name))
                                        <p class="alignc">{{ $escort->ethnicity->name }}</p>
                                    @endif    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">HEIGHT</h4>
                                    @if(isset($escort->height->name))
                                        <p class="alignc">{{ $escort->height->name }}</p>
                                    @endif    
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">EYE COLOR</h4>
                                    @if(isset($escort->eye_color->name))
                                        <p class="alignc">{{ $escort->eye_color->name }}</p>
                                    @endif    
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">BODY TYPE</h4>
                                    @if(isset($escort->body->name))
                                        <p class="alignc">{{ $escort->body->name }}</p>
                                    @endif    
                                </div>
                                @if($escort->has1hIncallRate())
                                    <div class="col-md-3 col-xs-6">
                                        <h4 class="alignc">PRICE P/HR</h4>
                                        <p class="alignc">$ {{ $escort->rates->first()->incall_1h_rate }}</p>
                                    </div>
                                @endif
                            </div>
                            <a class="more">READ MORE</a>
                        </li>

                        <h3>RATES</h3>
                        <li id="rates">
                            <div class="row">
                                @if($escort->hasIncallRates())
                                <div class="col-sm-6">
                                    <h4>INCALL RATES</h4>
                                    @foreach($escort->rates as $rate)
                                        <p>{{ ($rate->incall_15_rate > 0) ? '$'.$rate->incall_15_rate.' for 15 minutes' : '' }}</p>
                                        <p>{{ ($rate->incall_30_rate > 0) ? '$'.$rate->incall_30_rate.' for 30 minutes' : '' }}</p>
                                        <p>{{ ($rate->incall_45_rate > 0) ? '$'.$rate->incall_45_rate.' for 45 minutes' : '' }}</p>
                                        <p>{{ ($rate->incall_1h_rate > 0) ? '$'.$rate->incall_1h_rate.' for 1 hour' : '' }}</p>
                                        <p>{{ ($rate->incall_2h_rate > 0) ? '$'.$rate->incall_2h_rate.' for 2 hours' : '' }}</p>
                                        <p>{{ ($rate->incall_3h_rate > 0) ? '$'.$rate->incall_3h_rate.' for 3 hours' : '' }}</p>
                                        <p>{{ ($rate->incall_overnight_rate > 0) ? '$'.$rate->incall_overnight_rate.' for overnight' : '' }}</p>
                                    @endforeach        
                                </div>
                                @endif

                                @if($escort->hasOutcallRates())
                                <div class="col-sm-6">
                                    <h4>OUTCALL RATES</h4>
                                    @foreach($escort->rates as $rate)
                                        <p>{{ ($rate->outcall_15_rate > 0) ? '$'.$rate->outcall_15_rate.' for 15 minutes' : '' }}</p>
                                        <p>{{ ($rate->outcall_30_rate > 0) ? '$'.$rate->outcall_30_rate.' for 30 minutes' : '' }}</p>
                                        <p>{{ ($rate->outcall_45_rate > 0) ? '$'.$rate->outcall_45_rate.' for 45 minutes' : '' }}</p>
                                        <p>{{ ($rate->outcall_1h_rate > 0) ? '$'.$rate->outcall_1h_rate.' for 1 hour' : '' }}</p>
                                        <p>{{ ($rate->outcall_2h_rate > 0) ? '$'.$rate->outcall_2h_rate.' for 2 hours' : '' }}</p>
                                        <p>{{ ($rate->outcall_3h_rate > 0) ? '$'.$rate->outcall_3h_rate.' for 3 hours' : '' }}</p>
                                        <p>{{ ($rate->outcall_overnight_rate > 0) ? '$'.$rate->outcall_overnight_rate.' for overnight' : '' }}</p>
                                    @endforeach        
                                </div> 
                                @endif

                                <div class="col-md-12 clear mt15 rates-notes">
                                    {{ $escort->notes }}
                                </div>
                            </div>
                            <a class="more">READ MORE</a>
                        </li>

                        <h3>AVAILABILITY</h3>
                        <?php
                            $showTitle = false;
                            foreach ($escort->availabilities as $availability) {
                                if ($availability->twenty_four_hours == 0)
                                    $showTitle = true;
                            }
                        ?>
                        <li id="availability">
                            <div class="row">
                                <div class="col-xs-4">
                                    <h4>DAYS</h4>
                                    @foreach($escort->availabilities as $availability)
                                        @if($availability->day == 1 && $availability->enabled == 1)
                                            <p>Monday</p>
                                        @endif
                                        @if($availability->day == 2 && $availability->enabled == 1)
                                            <p>Tuesday</p>
                                        @endif
                                        @if($availability->day == 3 && $availability->enabled == 1)
                                            <p>Wednesday</p>
                                        @endif
                                        @if($availability->day == 4 && $availability->enabled == 1)
                                            <p>Thursday</p>
                                        @endif
                                        @if($availability->day == 5 && $availability->enabled == 1)
                                            <p>Friday</p>
                                        @endif
                                        @if($availability->day == 6 && $availability->enabled == 1)
                                            <p>Saturday</p>
                                        @endif
                                        @if($availability->day == 7 && $availability->enabled == 1)
                                            <p>Sunday</p>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-xs-4">
                                  
                                    <h4>FROM</h4>

                                    @foreach($escort->availabilities as $availability)
                                        @for($i = 1; $i <= 7; $i++)
                                            @if($availability->day == $i && $availability->enabled == 1)
                                                @if($availability->twenty_four_hours == 1)
                                                    <p>24 hours</p>
                                                @else
                                                <p>
                                                    {{ Helpers::$dayHours[$availability->start_hour_id] }} {{ $availability->start_am_pm == 0 ? 'AM' : 'PM' }}
                                                </p>
                                                @endif
                                            @endif
                                        @endfor
                                    @endforeach
                                </div>
                                <div class="col-xs-4">
                                    @if($showTitle)
                                        <h4>TO</h4>
                                    @endif
                                    @foreach($escort->availabilities as $availability)
                                        @if($availability->twenty_four_hours == 0)
                                            <p>{{ Helpers::$dayHours[$availability->end_hour_id] }} {{ $availability->end_am_pm == 0 ? 'AM' : 'PM' }}</p>
                                        @else
                                            <p style="height:22px"></p>
                                        @endif

                                    @endforeach
                                </div>
                            </div>
                            <a class="more">READ MORE</a>
                        </li>

{{--                         @if(EscortUpdate::whereEscortId($escort->id)->count() > 0)
                            <h3>SEXY UPDATES</h3>
                            <li id="sexy-updates">
                                <div class="sexy-update">
                                    @if($escort->mainImage)
                                        <img src="{{ $escort->mainImage->thumbnail(170,170) }}" alt="{{ $escort->escort_name }}">
                                    @endif
                                    <div class="update-content">
                                        <h5 class="update-title">{{ $escort->escort_name }}</h5>
                                        <p>{{ EscortUpdate::whereEscortId($escort->id)->orderBy('created_at','desc')->first()->message }}</p>   
                                    </div>
    
                                </div>
                                <a class="more">READ MORE</a>
                            </li>
                        @endif --}}

                        
                        <h3>SERVICES</h3>
                        <li id="services">
                            <div class="row">
                                @foreach($escort->services as $service)
                                    <ul class="col-md-3 col-xs-6">
                                        <li>{{ $service->name }}</li>
                                    </ul>
                                @endforeach
                            </div>
                            <a class="more">READ MORE</a>
                        </li>

                        <h3>LANGUAGES</h3>
                        <li id="languages">
                            <div class="row">
                                @foreach($escort->languages as $language)
                                    <ul class="col-md-3 col-xs-6">
                                        <li>{{ $language->name }}</li>
                                    </ul>
                                @endforeach
                            </div>
                            <a class="more">READ MORE</a>
                        </li>

                        @if($escort->touring()->count() > 0)
                            <h3>TOURINGS</h3>
                            <li id="tourings">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h4>FROM</h4>
                                    </div>
                                    <div class="col-xs-4">
                                        <h4>TO</h4>
                                    </div>
                                    <div class="col-xs-4">
                                        <h4>LOCATION</h4>
                                    </div>
                                </div>
                                @foreach($escort->touring as $touring)
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{{ Helpers::db_date_to_user_date($touring->from_date) }}</p>
                                        </div>
                                        <div class="col-xs-4">
                                            <p>{{ Helpers::db_date_to_user_date($touring->to_date) }}</p>
                                        </div>
                                        <div class="col-xs-4">
                                            @if($touring->location)
                                                <p>{{ $touring->location->name }}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                                <a class="more">READ MORE</a>
                            </li>
                        @endif

                        <h3>MORE INFORMATION</h3>
                        <li>
                            <div class="row">
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">HAIR COLOR</h4>
                                    @if(isset($escort->hair_color->name))
                                        <p class="alignc">{{ $escort->hair_color->name }}</p>
                                    @endif    
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">SMOKER</h4>
                                    <p class="alignc">{{ $escort->does_smoke == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">INCALL</h4>
                                    <p class="alignc">{{ $escort->does_incalls == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">OUTCALL</h4>
                                    <p class="alignc">{{ $escort->does_outcalls == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">TATTOOS</h4>
                                    <p class="alignc">{{ $escort->tattoos == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h4 class="alignc">PIERCINGS</h4>
                                    <p class="alignc">{{ $escort->piercings == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                                @if($escort->licence_number != '')
                                    <div class="col-md-3 col-xs-6">
                                        <h4 class="alignc">LICENCE NUMBER</h4>
                                        <p class="alignc">{{ $escort->licence_number }}</p>
                                    </div>
                                @endif
    
                            </div>
                            <a class="more">READ MORE</a>
                        </li>
                    </ul>
                </div>
            </div><!-- .col-md-8 end -->

            <aside class="col-md-4 col-xl-3 aside aside-left triggerAnimation animated" data-animate="fadeInLeft">
                <div class="about-me">
                    <h5>ABOUT ME</h5>
                    <p>{{ $escort->about_me }}</p>
                    <a class="more">READ MORE</a>
                </div>

                <li class="widget contact-me">



            <li class="widget contact-me">
            @if($escort->contact_phone == 1 && $escort->phone != '') <div class ='actionPane actionPhone' data-num ='{{Helpers::format_phone_number($escort->phone)}}'>{{ substr(Helpers::format_phone_number($escort->phone), 0, 7)}}...</div>@endif
            @if($escort->contact_playmail == 1)
                <div class="playMailButton actionPane actionPlay" data-toggle="modal" href="#contactEscortModal">PLAY MAIL</div>
            @endif
            @if($escort->use_own_url == 1)
                <div class="privateSiteButton">
                    <a href="{{ (substr($escort->own_url,0,4) == 'http') ? $escort->own_url : 'http://'.$escort->own_url }}" rel="nofollow" target="_blank">VIEW MY PRIVATE SITE</a>
                </div>
            @endif
                </li><!-- .widget_categories end -->
                </li><!-- .widget_categories end -->

                <li class="wishlist">
                    <ul class="row">
                    @if(Auth::check())
                        <li id="wishlist" class="col-xs-6">
                            <a class="is-not-wish" id="{{ $escort->id }}"><i class="fa fa-heart"> ADD TO WISHLIST</i></a>
                            <a class="is-wish" id="{{ $escort->id }}"><i class="fa fa-heart"> REMOVE FROM WISHLIST</i></a>
                        </li>
                    @else
                        <li class="col-xs-6">
                            <a href="#login" data-toggle="modal">
                                <i class="fa fa-heart">ADD TO WISHLIST</i>
                            </a>
                        </li>
                    @endif
                        <li class="widget_recent_comments col-xs-6">
                            <ul class="alignc">
                                <li class="rating-widget">
                                    <a href="#leaveReviewModal" data-toggle="modal">
                                    <?php
                                        $sum = 0;
                                        $avg = 0;
                                        foreach ($reviews as $review)
                                            $sum += $review->rating;

                                        if (count($reviews)>0)
                                            $avg = floor($sum / count($reviews));

                                        for ($i = 1; $i <= 5; $i++) {
                                            if ($i <= $avg)
                                                echo '<i class="fa fa-star"></i>';
                                            else
                                                echo '<i class="fa fa-star-o"></i>';
                                        }
                                    ?>
                                    </a>
                                </li>
                                <li class="review-count"><a href="#readReviewModal" data-toggle="modal">{{ count($reviews) }} Review (s)</a></li>
                                <li><a class="" data-toggle="modal" href="#leaveReviewModal">(Add Your Review)</a></li>
                            </ul>
                        </li><!-- .widget.widget_recent_comments end -->
                    </ul>
                </li><!-- .widget.pi_recent_posts end -->

                <ul class="aside-widgets">
                    @if($escort->under_business > 0 && $escort->business)
                    <!-- .widget.widget_search start -->
                    <li class="widget Agency">
                        <h5>{{ $escort->business->name }}</h5>
                        @if($escort->business && $escort->business->image != '')
                            <img src="{{ url('uploads/businesses/'.$escort->business->image) }}" alt="{{ $escort->business->name }}"/>
                        @endif
                    </li><!-- .widget.widget_search end -->
                    @endif

                    <li>
                        @if($escort->seo_content != '')
                            <h5 class="mt15">ESCORTS TAGS</h5>
                            <p>{{ $escort->seo_content }}</p>
                        @endif
                    </li>
                </ul><!-- .aside-widgets end -->
            </aside><!-- .aside.aside-left end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div> <!-- End of Page Wrapper -->

<div id="mobile-bottom-buttons" class="container">
    @if($escort->contact_playmail == 1)
        <div class="col-xs-4">
            <a data-toggle="modal" href="#contactEscortModal">EMAIL ME</a>        
        </div>
    @endif
    @if($escort->use_own_url == 1)
        <div class="col-xs-4">
            <a href="{{ (substr($escort->own_url,0,4) == 'http') ? $escort->own_url : 'http://'.$escort->own_url }}" rel="nofollow" target="_blank">MY SITE</a>
        </div>
    @endif
    @if($escort->contact_phone == 1)
        <div class="col-xs-4">
            <a href="tel:{{ $escort->phone }}">CALL ME</a>            
        </div>
    @endif
</div>

@include ( 'frontend.partials.escortModal' )

@stop

@section('javascript')

    <script>

       jQuery( document ).ready(function($) {
            $('.pgwSlideshow').pgwSlideshow();

            // If it's not a preview, update the visit counter
            if(window.location.href.indexOf("preview") == -1)
                $.ajax({url: '/escorts/increaseViewCounter/' + {{ $escort->id }} });


            var aboutMe = $('.about-me p');
            if (aboutMe.height() > 110)
            {
                aboutMe.css({
                    'height': '110px',
                    'overflow': 'hidden'
                });
                aboutMe.siblings('.more').show();
            }
    

            $('.about-me').on('click', '.more', function(e) {
                e.preventDefault();
                var $this = $(this),
                    p = $this.siblings('p');

                if (p.hasClass('open'))
                {
                    $this.text('READ MORE');
                    p.removeClass('open').animate({height: '110px'}, 200);
                }
                else
                {
                    $this.text('SHOW LESS');
                    p.addClass('open').animate({height: '100%'}, 200);
                }
            });

            $('#accordion li').each(function(index, el) {
                if ($(el).hasClass('active')){
                    $(el).find('div').show();
                    $(el).find('.more').text('SHOW LESS');
                }
                else
                    $(el).find('div').hide();
            });
            
            $('#accordion').on('click', '.more', function(e) {
                e.preventDefault();
                var $this = $(this),
                    li = $this.closest('li');
                toggleAccordion($this);
            });

            if ($(document).width() < 415) {
                $('.showPhoneButton').text('').off( "click", "**" );
                $('.showPhoneButton').append('<a href="tel:{{ $escort->phone }}">CALL ME</a>');
                $('.playMailButton').text('EMAIL ME');
                $('.privateSiteButton').text('MY SITE');

                $('#distance-title').text('DISTANCE');
            }

            var bottomButtons = $('#mobile-bottom-buttons div'),
                bottomButtonsCount = bottomButtons.length;
            if (bottomButtonsCount == 2)
                bottomButtons.removeClass('col-xs-4').addClass('col-xs-6');
            if (bottomButtonsCount == 1)
                bottomButtons.removeClass('col-xs-4').addClass('col-xs-12');
        });

    </script>
@stop

 
@stop













