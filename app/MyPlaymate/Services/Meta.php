<?php
namespace MyPlaymate\Services;

class Meta
{
	public $meta_title;
	public $meta_keywords;
	public $meta_description;
	public $meta_url_key;

	public function __construct($title, $keywords, $description) {
		$this->meta_title = $title;
		$this->meta_keywords = $keywords;
		$this->meta_description = $description;
	}
}