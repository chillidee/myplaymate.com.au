@extends('frontend.layout')

@section('content')

<!-- .boxed version of the page -->
<div class="page-wrapper extra-top">
    <!-- .container start -->
    <div class="container">
        <!-- .row start -->
        <div class="row">
            <div id="login" class="col-md-5 login-section">
                <h3>LOGIN</h3>
                <div id="login-container">
                    <h6>If you have an account with us, please log in.</h6>
                    {{ Form::open(array('route' => 'login','id'=>'login-form')) }}
                        <fieldset>
                            <div class="control-group">
                                {{ Form::label('email','E-mail Address', array('class'=>'control-label')) }} <span>*</span>
                                <div class="controls">
                                    {{ Form::text('email','',array('class'=>'input-xlarge input03')) }}
                                </div>
                            </div>
                            <div class="control-group" id="login-password">
                                {{ Form::label('password','Password', array('class'=>'control-label')) }} <span>*</span>
                                <div class="controls">
                                    {{ Form::password('password',array('class'=>'input-xlarge input03')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::submit('LOGIN',array('class'=>'button3', 'id'=>'login-submit')) }}
                                    <a class="btn" id="forgot-password">Forgot your password</a>
                                </div>
                            </div>
                            @if(isset($error))
                             <div class="control-group" style="clear:both;">
                                    <span style="color: red;margin-top: 10px">
                                        {{ $error }}
                                    </span>
                                </div>
                            @endif
    
                        </fieldset>    
                    {{ Form::close() }}

                </div>
            </div>
            <div class="register-form col-md-5 col-md-offset-1">
                {{ Form::open(array('route' => array('postRegister'))) }}
                <h3>CREATE AN ACCOUNT</h3>

                <div class="register-form mpm-individual">
                        <fieldset>
                            <div class="control-group">
                                {{ Form::label('type','Choose your user type:',array('class'=>'control-label')) }}
                                {{ Form::select('type',[1=>'Customer',2=>'Escort / Business'],'',array('class'=>'form-control')) }}
                            </div>
                            <div class="control-group" id="register-name">
                                <label class="control-label">Name<span>*</span></label>
                                <div class="controls">
                                    {{ Form::text('full_name', null, array('class' => 'input-xlarge input03')) }}
                                    @if($errors->has('full_name'))
                                        <p class="form-error">{{ $errors->first('full_name') }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="control-group" id="register-email-address">
                                <label class="control-label">E-Mail Address<span>*</span></label>
                                <div class="controls">
                                    {{ Form::text('email', null, array('class' => 'input-xlarge input03')) }}
                                    @if($errors->has('email'))
                                    <p class="form-error">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                            </div>
 
                            <div class="control-group" id="register-password">
                                <label class="control-label">Password<span>*</span></label>
                                <div class="controls">
                                    {{ Form::password('password', array('class' => 'input-xlarge input03')) }}
                                    @if($errors->has('password'))
                                    <p class="form-error">{{ $errors->first('password') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="control-group" id="register-password-confirmation">
                                <label class="control-label">Repeat Password<span>*</span></label>
                                <div class="controls">
                                    {{ Form::password('password_confirmation', array('class' => 'input-xlarge input03')) }}
                                </div>
                            </div>
                            <div style=" margin-bottom:10px;"><input type="checkbox" name="terms_and_conditions" / >&nbsp;I Agree with <span style="color: #f55;"><a href="/advertiser-terms-and-conditions">terms of use</a></span>
                                @if($errors->has('terms_and_conditions'))
                                <p class="form-error">{{ $errors->first('terms_and_conditions') }}</p>
                                @endif
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::submit('REGISTER',array('class'=>'button3')) }}
                                </div>
                            </div>
                        </fieldset>
                </div>

                {{ Form::close() }}
            </div>
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- End of the Boxed Version Page -->

@stop


@section('javascript')

    <script>

    jQuery(document).ready(function($) {
        $('#forgot-password').on('click', function(event) {
            event.preventDefault();
            var form = $('#login-form');
            $('#login-password').slideUp(200);
            form.attr('action','users/forgot-password');
            $('#login-submit').val('SEND ME AN EMAIL');
        });;
    });

    </script>

@stop







