   <div class="modal fade backdrop" id="leaveReviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="leaveReviewForm" role="form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Leave Review for {{ $escort->escort_name }} </h4>
                    <div class="alert alert-success"></div>
                </div>
                <div class="modal-body">
                    <div id="leaveReviewFields">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name:</label>
                                    <input name="name" required type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="control-label">Rating:</label>
                                    <select name="rating" class="form-control">
                                        <option value="5">5</option>
                                        <option value="4">4</option>
                                        <option value="3">3</option>
                                        <option value="2">2</option>
                                        <option value="1">1</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Review</label>
                                    <textarea name="review" required class="form-control" placeholder="Message" style="height:100px"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="escort_id" value="{{ $escort->id }}">
                                    <button class="btn btn-default pull-right" id="leaveReviewButton">Leave Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade backdrop" id="readReviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="readReviewForm" role="form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Review(s) for {{ $escort->name }} </h4>
                </div>
                <div class="modal-body">
                    <div id="readReviewFields">
                        @foreach($reviews as $review)
                        <div class="row">
                            <div class="col-md-12">{{$review->review}}</div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade backdrop" id="contactEscortModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="contactEscortForm" role="form">
        <div class="modal-dialog wideModal darkDialog">
            <div class ='mpLoader' id ='playmailLoader'></div>
            <div id ='modalSuccess'>Your message has been sent</div>
            <div class="modal-content darkContactModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    @if(isset($escort->business))
                    <a <?php if(isset($escort->business->seo_url)): ?> href="/agencies/{{$escort->business->seo_url}}" <?php else: ?> href="/" <?php endif; ?> class ='modalLogo'>
                     <img src="{{Config::get('constants.CDN')}}/businesses/{{$escort->business->image}}" alt="{{ $escort->business->name }}"/>
                 </a>
                 @else
                 <a href="/" class ='modalLogo'>
                     <img src="/assets/frontend/img/logo.png" alt="Myplaymate.com.au. the place to be">
                 </a>
                 @endif
                 @if(isset($escort->business->name))
                 <h4 class="modal-title">Contact {{ $escort->business->name}} </h4>
                 @else
                 <h4 class="modal-title">Contact {{ $escort->escort_name }} </h4>
                 @endif
             </div>
             <div class="modal-body">
                 @if(isset($escort->business->phone) && isset($escort->business->name))
                 <div class ='col-md-6'>
                     <p class ='center_perc_70'>{{$escort->business->name}} is contactable by phone.  Click below to see their phone number, or use the form to send a message.</p>
                     <div class ='actionPane actionPhone' data-num ='{{Helpers::format_phone_number($escort->business->phone)}}'>{{ substr(Helpers::format_phone_number($escort->business->phone), 0, 7)}}...</div>
                 </div>


                 @else


                 @if($escort->contact_phone == 1 || $escort->under_business == 1 && $escort->phone != '' )
                 @if ( $escort->contact_playmail != 1 || $escort->email == '')
                 <div class ='col-md-12'>
                   <p class ='center_perc_70'>{{$escort->escort_name}} is contactable by phone.  Click below to see their phone number, or use the form to send a message.</p>
                   @else
                   <div class ='col-md-6'>
                    <p>{{$escort->escort_name}} is contactable by phone or Playmail.  Click below to see their phone number, or use the form to send a message.</p>
                    @endif
                    <div class ='actionPane actionPhone' data-num ='{{Helpers::format_phone_number($escort->phone)}}'>{{ substr(Helpers::format_phone_number($escort->phone), 0, 7)}}...</div>
                </div>
                @endif
                @endif
                @if ( $escort->contact_playmail == 1 || ( $escort->under_business == 1 && $escort->email != '' ) )
                @if ( $escort->contact_phone != 1 || ( $escort->under_business == 1 && $escort->phone == '' ))
                <div class ='col-md-12'>
                   @else
                   <div class ='col-md-6'>
                       @endif
                       <div id="playMailForm">
                        <div class="alert alert-success"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input name="name" required type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input name="phone" required type="phone" class="form-control" placeholder="Phone/Mobile Number" maxlength="10">
                                </div>
                                <div class="form-group">
                                    <input name="email" required type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <textarea name="message" required class="form-control" placeholder="Message" style="height:100px" maxlength="1000"></textarea>
                                </div>
                                <input type="hidden" name="escort_id" value="{{ $escort->id }}">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-default pull-right" id="contactEscortButton">Send PlayMail</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</form>
</div>