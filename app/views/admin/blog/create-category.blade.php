@extends('admin.layout')

@section('javascript')

   <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.datepicker').datepicker({dateFormat: 'dd/mm/yy'});
        });
    </script>    

@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-newspaper-o"></span></div>
    <div class="pagetitle">
        <h1>New Category</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open() }}
<div class="row">
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('name','Name*',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('name','',array('class'=>'form-control','required')) }}
            </div>
        </div>
  </div>
        <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('description','Category Description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('description','',array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
<div class="alignc col-md-12">
    {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
</div>

{{ Form::close() }}
@stop