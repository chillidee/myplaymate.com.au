<?php
namespace Admin;
use Controller, Auth, View, Helpers, Datatables, Input, ImageHelper, Carbon\Carbon,Config, Advertisement,Option, Media;
use Post, PostCategory;

class MediaController extends Controller {
  public function index()
  { 
    $media = Media::get();
    return View::make('admin.media.index',compact('media'));
  }
  public function base64_to_jpeg($base64_string, $filename, $directory ) {

           $output_file = Config::get('constants.PUBLIC_DIR') . $directory . $filename;

           $ifp = fopen($output_file, "wb"); 

           $data = explode(',', $base64_string);

           fwrite($ifp, base64_decode($data[1])); 
           fclose($ifp); 

           return $data[1];
  }
  public function uploadMedia () {
    $user = Auth::user();
      if ( !$user || !$user->super_user )
            die();

    $input = Input::all();
    $fileName = str_random(12) . '.jpg';
    $img = $this->base64_to_jpeg( $input['image'], $fileName, '/uploads/media/' );
    $media = new Media;
    $media->alt = $input['alt'];
    $media->file = $fileName;
    $media->save();
    return $fileName;

  }
}