@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class=deviceWidth style="width: 580px;" align=center bgcolor=#eeeeed border=0 cellpadding=0 cellspacing=0>
  <tbody>
    <tr>
      <td style="padding: 0;" bgcolor=#ffffff valign=top>
        <a href=http://www.myplaymate.com.au/>
        <img class=deviceWidth style="display: block; border-radius: 4px;" src="https://myplaymate.com.au/assets/emails/main-updated-4.png" alt border=0>
        </a>
      </td>
    </tr>
    <tr>
      <td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor=#eeeeed>
        <table bgcolor=#eeeeed>
          <tbody>
            <tr>
              <td style="padding: 0 10px 10px 0;" valign=middle>
                <a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href=#>
                  Hi {{$data['full_name']}},
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        <p>Thank you for registering with My Playmate. Before we get started on your profile, you’ll need to validate your email address. That can be done by clicking right <a style="text-decoration: none; color: #b60011; font-weight: bold;" href="{{Config::get('app.url')}}/{{$data['activation_link']}}">here</a>.</p>
        <p>Once you’ve done that, you will be able to upload your photos and update your information.</p>
        <p>Of course if you need any help, get in touch with us via <a href="mailto:admin@myplaymate.com.au">admin@myplaymate.com.au</a></p>
        <p>We look forward to having you on our front page!</p>
        <p>My Playmate x</p>
      </td>
    </tr>
  </tbody>
</table> <!-- End One Column -->

@stop