<?php
use MyPlaymate\Services\Meta;

class PageController extends Controller {

  public function catchall($slug)
  {
    $meta = Page::select('*')
    ->where('url_key', '=', $slug)
    ->where('published','=','1')
    ->first();
    if ( $meta ) {
     if ( $meta->type == '1' ) {
       $get = $meta;
       $bits = explode("-", $slug);
       $city = $bits[1];
       if ( $city == 'goldcoast' ) {
        $suburb_id = 13172;
      }
      else {
        $suburb_id = Suburb::whereName($city)->first()->id;
      }
      if ($city == 'perth')
       $suburb_id = 9140;
  //   $meta = new meta( $get->meta_title, $get->meta_keywords, $get->meta_description );
     $pass = array(
       'name' =>    $get->name,
       'city' =>    $city,
       'content' => $get->content,
       'top_content' => $get->top_content,
       'city_page' => '1',
       'main_image' => $get->main_image,
       'meta' => $meta,
       'img_alt' => $get->img_alt,
       'suburb_id' => $suburb_id
       );
     if (isset($_COOKIE['mpm_session']) && file_exists('temp/' . $city . '-' . $_COOKIE['mpm_session'] . '.js')) {
      $pass['session'] = '/temp/' . $city . '-' . $_COOKIE['mpm_session']  . '.js'; }
      return View::make('frontend.escorts.city',$pass );
    }
    else {
     return View::make('frontend.pages.layout' )
     ->with('title',$meta->name)
     ->with('meta',$meta);
   }
 }
 else {
  return Response::view('frontend.404', array(), 404);
}
}

public function media()
{
  $meta = Page::whereName('escort-media')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Escort media')
  ->with('meta',$meta);
}

public function about()
{
  $meta = Page::whereName('about')->first();

  return View::make('frontend.pages.layout')
  ->with('title','About')
  ->with('meta',$meta);
}

public function advertiserTerms()
{
  $meta = Page::whereName('advertiser terms and conditions')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Advertiser terms and conditions of use')
  ->with('meta',$meta);
}

public function faqs()
{
  $meta = Page::whereName('FAQs')->first();

  return View::make('frontend.pages.layout')
  ->with('title','FAQs')
  ->with('meta',$meta);
}

public function billingPolicy()
{
  $meta = Page::whereName('Billing and refund policy')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Billing and refund policy')
  ->with('meta',$meta);
}

public function clientTerms()
{
  $meta = Page::whereName('Client terms and conditions of use')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Client terms and conditions of use')
  ->with('meta',$meta);
}

public function links()
{
  $meta = Page::whereName('Links')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Links')
  ->with('meta',$meta);
}

public function membersTerms()
{
  $meta = Page::whereName('Members terms and conditions of use')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Members terms and conditions of use')
  ->with('meta',$meta);
}

public function privacyPolicy()
{
  $meta = Page::whereName('Privacy policy')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Privacy policy')
  ->with('meta',$meta);
}

public function escortsBrisbane()
{
  $page = Page::whereName('Escorts Brisbane')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Escorts Brisbane')
  ->with('meta',$page)
  ->with('content',$page->content);
}

public function escortsAdelaide()
{
  $page = Page::whereName('Escorts Adelaide')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Escorts Adelaide')
  ->with('meta',$page)
  ->with('content',$page->content);
}

public function escortsSydney()
{
  $page = Page::whereName('Escorts Sydney')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Escorts Sydney')
  ->with('meta',$page)
  ->with('content',$page->content);
}

public function escortsMelbourne()
{
  $page = Page::whereName('Escorts Melbourne')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Escorts Melbourne')
  ->with('meta',$page)
  ->with('content',$page->content);
}

public function escortsPerth()
{
  $page = Page::whereName('Escorts Perth')->first();

  return View::make('frontend.pages.layout')
  ->with('title','Escorts Perth')
  ->with('meta',$page)
  ->with('content',$page->content);
}
}
