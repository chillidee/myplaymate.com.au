@extends('admin.layout')

@section('content')
	<div class="pageheader">
	    <div class="pageicon"><span class="fa fa-user"></span></div>
	    <div class="pagetitle">
	        <h1>Create new user</h1>
	    </div>
	</div><!--pageheader-->

    {{ Form::open(['id' => 'create-user']) }}
        <div class="control-group">
            {{ Form::label('full_name','Full name',['class'=>'control-label']) }}<span>*</span>
            <div class="controls">
                {{ Form::text('full_name', '', ['class' => 'form-control','data-parsley-required']) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('email','Email',['class'=>'control-label']) }}<span>*</span>
            <div class="controls">
                {{ Form::text('email', '', ['class' => 'form-control','required','data-parsley-type'=>'email']) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('phone','Phone',['class'=>'control-label']) }}<span>*</span>
            <div class="controls">
                {{ Form::text('phone', '', ['class' => 'form-control','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)']) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('password','Password',['class'=>'control-label']) }}<span>*</span>
            <div class="controls">
                {{ Form::password('password', ['id'=>'password','class' => 'form-control','required']) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('confirm_password','Confirm password',['class'=>'control-label']) }}<span>*</span>
            <div class="controls">
                {{ Form::password('confirm_password', ['class' => 'form-control','required','data-parsley-equalto'=>'#password']) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('type','Type',['class'=>'control-label']) }}<span>*</span>
            <div class="controls">
                {{ Form::select('type', [1=>'Customer',2=>'Escort/Business'],'', ['class' => 'form-control','required']) }}
            </div>
        </div>

		<div class="checkbox">
    		<label for="active">
				{{ Form::checkbox('active', 1, false) }} Active
    		</label>
		</div>
		<div class="checkbox">
    		<label for="super_user">
				{{ Form::checkbox('super_user',1, false) }} Administrator
    		</label>
		</div>
        <div class="section-content">
            {{ Form::submit('CREATE',['class'=>'btn btn-primary']) }}
        </div>
    {{ Form::close() }}

    <script>
        jQuery(document).ready(function($) {
            $('#create-user').parsley();
        });
    </script>   
@stop