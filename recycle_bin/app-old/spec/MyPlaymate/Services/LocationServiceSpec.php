<?php

namespace spec\MyPlaymate\Services;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use MyPlaymate\Repositories\SuburbRepository;

class LocationServiceSpec extends ObjectBehavior
{
    function let(SuburbRepository $repo)
    {
        $this->beConstructedWith($repo);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Services\LocationService');
    }

    function it_returns_an_array_from_set_coordinates(SuburbRepository $repo)
    {
    	$this->setCoordinates(1)->shouldHaveCount(2);
    }

    function it_returns_distance_0_for_same_suburb()
    {
    	$this->calculateDistance(1,1)->shouldReturn(0);
    }

    // function it_returns_distance_9_for_chippendale_burwood()
    // {
    //     $this->setCoordinates(2377)->willReturn([]);
    //     $this->calculateDistance(2377,1845);
    // }
}













