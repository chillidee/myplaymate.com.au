<?php

namespace MyPlaymate\Repositories;

use EscortUpdate;

class EscortUpdateRepository {

	public function getLatest($number)
	{
		return EscortUpdate::leftJoin('escorts','escort_id','=','escorts.id')
						->where('escorts.status','=',2)
						->where('escort_updates.status','=',2)
						->orderBy('escort_updates.created_at', 'desc')
						->take($number)
						->get(['escort_updates.*', 'escorts.main_image_id']);
	}
}