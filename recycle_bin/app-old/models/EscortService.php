<?php

/**
 * Class EscortImage
 */
class EscortService extends Eloquent
{
    protected $table = 'escort_service';

    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [];

    /**
     *
     */
    public function escort()
    {
        $this->belongsTo('Escort');
    }
}
