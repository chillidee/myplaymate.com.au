<li class="widget">
    <ul class="social-links">
        <li>
            <a href="https://twitter.com/MyPlayMateAU" target="_blank"><i class="fa fa-twitter twitter-share-button" data-lang="en"></i></a>
        </li>
        <li>
            <a href="https://www.facebook.com/MyPlayMateAU" target="_blank"><i class="fa fa-facebook"></i></a>
        </li>
        <li>
            <a href="https://plus.google.com/111322677157732274301" target="_blank"><i class="fa fa-google-plus"></i></a>
        </li>
    </ul>
</li>