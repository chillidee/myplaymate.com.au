(function($) {
    function getParameterByName(name, url) {
       if (!url) url = window.location.href;
       name = name.replace(/[\[\]]/g, "\\$&");
       var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
       if (!results) return null;
       if (!results[2]) return '';
       return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
var signup = getParameterByName('signup'); 
if ( signup == 'true'){
    $( 'a[href="#signup"]').click();
}
if ( window.location.hash == '#login') {
    $( 'a[href="#login"]').click();
}

    $.fn.touchwipe = function(settings) {
        var config = {
            min_move_x: 20,
            min_move_y: 20,
            wipeLeft: function() {},
            wipeRight: function() {},
            wipeUp: function() {},
            wipeDown: function() {},
            preventDefaultEvents: true
        };
        if (settings) $.extend(config, settings);
        this.each(function() {
            var startX;
            var startY;
            var isMoving = false;

            function cancelTouch() {
                this.removeEventListener('touchmove', onTouchMove);
                startX = null;
                isMoving = false
            }

            function onTouchMove(e) {
                if (config.preventDefaultEvents) {
                    e.preventDefault()
                }
                if (isMoving) {
                    var x = e.touches[0].pageX;
                    var y = e.touches[0].pageY;
                    var dx = startX - x;
                    var dy = startY - y;
                    if (Math.abs(dx) >= config.min_move_x) {
                        cancelTouch();
                        if (dx > 0) {
                            config.wipeLeft()
                        } else {
                            config.wipeRight()
                        }
                    } else if (Math.abs(dy) >= config.min_move_y) {
                        cancelTouch();
                        if (dy > 0) {
                            config.wipeDown()
                        } else {
                            config.wipeUp()
                        }
                    }
                }
            }

            function onTouchStart(e) {
                if (e.touches.length == 1) {
                    startX = e.touches[0].pageX;
                    startY = e.touches[0].pageY;
                    isMoving = true;
                    this.addEventListener('touchmove', onTouchMove, false)
                }
            }
            if ('ontouchstart' in document.documentElement) {
                this.addEventListener('touchstart', onTouchStart, false)
            }
        });
        return this
    }
})(jQuery);




if ($(window).width() > 1025) {
    function testScroll(ev) {
        if (window.pageYOffset > 70) {
            $('header').css({
                'position': 'fixed',
                'top': '0'
            });
        } else {
            $('header').css({
                'position': 'absolute',
                'top': 'auto'
            });
        }
    }
    window.onscroll = testScroll;
}
$.fn.parallax = function(options) {
    var windowHeight = $(window).height();
    // Establish default settings
    var settings = $.extend({
        speed: 0.15
    }, options);
    // Iterate over each object in collection
    return this.each(function() {
        // Save a reference to the element
        var $this = $(this);
        // Set up Scroll Handler
        $(document).scroll(function() {
            var scrollTop = $(window).scrollTop();
            var offset = $this.offset().top;
            var height = $this.outerHeight();
            // Check if above or below viewport
            if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
                return;
            }
            var yBgPosition = Math.round((offset - scrollTop) * settings.speed);
            // Apply the Y Background Position to Set the Parallax Effect
            $this.css('background-position', 'center ' + yBgPosition + 'px');

        });
    });
}
var tbPrevChecked = '3';
$('#tb_escort').on('click', function() {
    $('input[name=full_name]').attr("placeholder", "Escort Name");
    $("#business_type input[value=" + tbPrevChecked + "]").prop("checked", true).change();
    $('#typeSelect').attr('value', '3');
    $('#tb_esc_tab').show();
    $('#tb_cust_tab').hide();
    $('#tb_bus_tab').hide();
    $('.tb_active').removeClass('tb_active');

    $(this).addClass('tb_active');
});
$('#tb_customer').on('click', function() {
    $('input[name=full_name]').attr("placeholder", "Customer Name");
    tbPrevChecked = $('input[name=type]:checked').val();
    $('#typeSelect').attr('value', '2');
    $("#business_type input.hidden").prop("checked", true).change();
    $('#business_type').hide();
    $('#tb_esc_tab').hide();
    $('#tb_bus_tab').hide();
    $('#tb_cust_tab').show();
    $('.tb_active').removeClass('tb_active');
    $(this).addClass('tb_active');
});
$('#tb_business').on('click', function() {
    $('input[name=full_name]').attr("placeholder", "Business Name");
    tbPrevChecked = $('input[name=type]:checked').val();
    $('#typeSelect').attr('value', '4');
    $("#business_type input.hidden").prop("checked", true).change();
    $('#business_type').hide();
    $('#tb_esc_tab').hide();
    $('#tb_cust_tab').hide();
    $('#tb_bus_tab').show();
    $('.tb_active').removeClass('tb_active');
    $(this).addClass('tb_active');
});
$( '#signup-form input').on('change',function() {
    $( this ).parents('.control-group').next('.form-error').html('');
});
var formInstance = $('#signup-form').parsley();
var field = $('#password1').parsley();
formInstance.validate({
    group: 'loginForm'
});

$('#mp_parralax').parallax({
    speed: .2
});
$('.actionPhone').on('click', function() {

    $(this).html($(this).data('num'));
    if ( window.type == 'escort'){
         jQuery.ajax({
             type: "POST",
             url: "/escorts/" + window.activeId + "/reveal-phone"
         });
          ga('send','event','Escorts','Phone Reveal',window.escortName); 
    }
    else {
        ga('send','event','Businesses','Phone Reveal',window.businessName); 
    }

})

function toggleAccordion(clicked) {
    var li = $(clicked).closest('li');
    if (li.hasClass('active')) {
        li.find('div').slideUp();
        li.removeClass('active');
        clicked.text('READ MORE');
    } else {
        li.find('div').slideDown();
        li.addClass('active');
        clicked.text('SHOW LESS');
    }
}

function clickOnPhoneNumber(event) {
    var $this = jQuery(this);

    if ($this.hasClass('showPhoneButton')) {
        event.preventDefault();
        $this.text('');
    }

    jQuery.ajax({
        type: "POST",
        url: "/escorts/'. window.escort_id .'/reveal-phone"
    });
    if ($this.hasClass('showPhoneButton')) {
        $this.removeClass('showPhoneButton');
        $this.parent().append('<span class="danger">Your message has been sent</span>');
    }

}

if (window.mapPage == 1) {
    var mapDiv = document.getElementById('map');
    var map = new google.maps.Map(mapDiv, {
        center: window.center,
        zoom: 15
    });
    var marker = new google.maps.Marker({
        position: center,
        map: map,
        title: window.mapName
    });
}
if (window.getGirls == 1) {
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    function getGirl(girl) {
        jQuery.ajax({
                type: "GET",
                url: "/business/getgirl/" + girl,
            })
            .success(function(data) {
                $('#escortHolder').addClass('showGirl');
                $('#escortHolder').html(data);
                $('.pgwSlideshow').pgwSlideshow();
                $(document).scrollTop($("#showEscort").offset().top);
                $('.actionPhone').off().on('click', function() {
                    $(this).html($(this).data('num'));
                    ga('send','event','Businesses','Phone Reveal',window.businessName); 
                    jQuery.ajax({
                        type: "POST",
                        url: "/escorts/" + window.activeId + "/reveal-phone"
                    });
                })
            });
    }
    var esc = getUrlParameter('esc');
    if (esc != '' && typeof(esc) != 'undefined') {
        getGirl(esc);
    }
    $('.img-profile').on('click', function() {
        getGirl($(this).data('id'));
    })
}
$("#left_pull_filters").touchwipe({
    wipeLeft: function() {
        $('#close_left_pull').hide();
        $('#left_pull_filters').animate({
            'width': '0px',
            'margin-left': '-50px'
        });
    },
    min_move_x: 20,
    min_move_y: 2000,
    preventDefaultEvents: false
});
$("ul.nav.navbar-nav").touchwipe({
    wipeLeft: function() {
        $('.arrowToggle').removeClass('arrowToggleClicked');
        $('#header .nav.navbar-nav').animate({
            'width': '0%'
        });
    },
    min_move_x: 60,
    min_move_y: 2000,
    preventDefaultEvents: false
});
$('#center').on('click', '.showPhoneButton', clickOnPhoneNumber);
$('#mobile-bottom-buttons').on('click', 'a', clickOnPhoneNumber);

var clicked = false;
jQuery('#contactEscortForm').on('click', '#contactEscortButton', function(e) {
    e.preventDefault();
    var form = $('#contactEscortForm');
    form.parsley();
    $( '#playmailLoader').show();
    $( '.darkContactModal').hide();
    url = ( window.type == 'escort' ? '/escort/sendPlaymail' : '/business/sendPlaymail' );
    $.ajax({
            url: url,
            data: form.serialize(),
        })
        .done(function(data) {
            $( '#playmailLoader').hide();
            $( '#modalSuccess').show();
            setTimeout(function() {
                $('#contactEscortModal').removeClass('in');
                $('.fade').fadeOut();
                 $( '#playmailLoader').hide();
                 $( '#modalSuccess').hide();
                 $( '.darkContactModal').show();
            }, 1000);
            ga('send', 'event', 'Playmail', 'sent', data );
        });
});
$('#leaveReviewForm').on('click', '#leaveReviewButton', function(e) {
    e.preventDefault();

    var form = $('#leaveReviewForm');
    $.ajax({
            url: '/escort/leaveReview',
            data: form.serialize(),
        })
        .done(function(data) {
            $('.alert-success').text(data).fadeIn(200);
            setTimeout(function() {$('body').removeClass('modal-open');
                $('#leaveReviewForm').fadeOut();
                $('.fade').fadeOut();
            }, 1000);
        });
});


            $('#leaveReviewButton').click(function(){removeClass('modal-open')});

//NO COLFLICT MODE OF JQUERY
var $ = jQuery.noConflict();


function dateFormat(date) {
    var date = new Date(date);
    return (date.getDate() + 1) + '/' + date.getMonth() + '/' + date.getFullYear();
}

function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g, '')
        .replace(/ +/g, '-');
}
// SCRIPTS START
(function($) {

      $('#register-submit').click(function(){
      $('.parsley-errors-list').each(function(){
        $(this).css('display','block');
      });
      $('input').each(function(){
        $(this).change(function(){
          if($(this).hasClass('parsley-error')){
            var parsley_id = $(this).attr('data-parsley-id');
            $('#parsley-id-' + parsley_id).css('display','none');
          }
        });
      });
    });

    if($('#contactEscortButton')){
      $('#contactEscortButton').click(function(){
        $('body').removeClass('modal-open');
        $('body').css('padding-right','0px');
      });
    }

    if($('body').hasClass('modal-open')){
      $('input').focus(function(){
        if($(this).parent().parent().next().hasClass('form-error')){
          $(this).parent().parent().next().css('display','none');
        }
      });
    }

    if($('html').width() <= 1024){
      $('#trigger-overlay-mobile').click(function(event) {
        $(this).removeClass('arrowToggleClicked');
            $('#header .nav.navbar-nav').animate({
                'width': '0%'
        });
        $.ajax({
          url: '/map_modal',
        })
        .done(function(data) {
          $.ajax({
            url: '/get_state_and_cities',
            data: {
              state_name: window.$stateName 
            },
          })
          .done(function(data) {

            $('#current-state').text(window.$stateName);
            $('#mapId-' + window.$stateName).attr('fill', $('#mapId-' + window.$stateName).data('pathcolor'));
            var theCity = 0;
            if ( typeof( Cookies.get( 'suburbName') ) != 'undefined' ) {
             theCity = Cookies.get( 'suburbName');
           }
           $.each(data.cities, function(index, city) {
            var sel = '';
            if ( theCity != 0 && theCity == city.name ){
              sel = ' selected ';
            }
            $('#city-select').append('<option ' + sel + 'value="' + city.id + '">' + city.name + '</option>');
          });

         });

                    // Show the modal window
                    $('#geoMap').html(data).modal();
                  //  setupAutoComplete();
                    ( typeof( Cookies.get( '_distance') ) != 'undefined' ? $( '#_distance' ).val(  Cookies.get( '_distance') ) : $( '#_distance' ).val( 50 ) );
                  });
      });
    }

    $('.arrowToggle').on('click', function() {
        if (!$(this).hasClass('arrowToggleClicked')) {
            $(this).addClass('arrowToggleClicked');
            $('#header .nav.navbar-nav').animate({
                'width': '100%'
            });
        } else {
            $(this).removeClass('arrowToggleClicked');
            $('#header .nav.navbar-nav').animate({
                'width': '0%'
            });
        }
    });
    $('.downCaret').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if ($(this).parent().children('.dropdown-menu').is(':visible')) {
            $(this).parent().children('.dropdown-menu').hide();
            $(this).removeClass('downCaretOpened');
        } else {
            $(this).parent().children('.dropdown-menu').show();
            $(this).addClass('downCaretOpened');
        }
    });

    $('#front_page_top_button').on('click', function() {
        ga('send', 'event', 'Go to Escorts page', 'click', 'Home page- top button');
    });
    $('#front_page_middle_button').on('click', function() {
        ga('send', 'event', 'Go to Escorts page', 'click', 'Home page- Middle page');
    });


    // Remove placeholder on focus
    $('input').on('focus', function() {
        var $this = $(this),
            placeholder = $this.attr('placeholder');
        $this.attr('placeholder', '');

        $this.on('blur', function() {
            $this.attr('placeholder', placeholder);
        });
    });

    // Scroll to top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scroll-up').fadeIn();
        } else {
            $('.scroll-up').fadeOut();
        }
    });

    $('.scroll-up').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600, function() {
            //set_static_header(0);
        });
        return false;
    });


    // Dropdown mobile arrow
    $('.dropdown-caret').on('click', function(e) {
        e.preventDefault();
        $(this).parent('li').children('ul').slideToggle();
    });

    $('.accordion').on('click', '.title', function(event) {
        event.preventDefault();
        $(this).siblings('.accordion .active').next().slideUp('normal');
        $(this).siblings('.accordion .title').removeClass("active");

        if ($(this).next().is(':hidden') === true) {
            $(this).next().slideDown('normal');
            $(this).addClass("active");
        }
    });
    $('.accordion .content').hide();
    $('.accordion .active').next().slideDown('normal');

    //SIDEBAR CAROUSEL
    $("#owl-sidebar").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsMobile: [479, 1],
        lazyLoad: false
    });
    $('.owl-item').show();
        $('.submit-button').on('click', function(e) {
            $('body').removeClass('modal-open');
            $('body').css('padding-right','0px');
        });
    $('#myToggleFilters').on('click', function() {
        if ($('#left_pull_filters').width() == 0) {
            $('#left_pull_filters').animate({
                'width': '500px',
                'margin-left': '-0px'
            }, {
                queue: false,
                complete: function() {
                    $('#close_left_pull').show();
                }
            });
        } else {
            $('#close_left_pull').hide();
            $('#left_pull_filters').animate({
                'width': '0px',
                'margin-left': '-50px'
            });
        }
    });
    $('#filter_information').on('click', function(e) {
        if ($('#left_pull_filters').width() == 0) {
            $('#close_left_pull').show();
            $('#left_pull_filters').animate({
                'width': '500px',
                'margin-left': '-0px'
            }, {
                queue: false
            }, function() {
                $('#close_left_pull').show();
            });
        } else {
            $('#close_left_pull').hide();
            $('#left_pull_filters').animate({
                'width': '0px',
                'margin-left': '-50px'
            });
        }
    });
    $('.no_close').on('click', function(e) {
        e.stopPropagation();
    });
    $('#close_left_pull').on('click', function() {
        $(this).hide();
        $('#left_pull_filters').animate({
            'width': '0px',
            'margin-left': '-50px'
        }, {
            queue: false
        });
    });
    $('body').not('.no-close').on('click', function() {
        $('#left_pull_filters').animate({
            'width': '0px',
            'margin-left': '-50px'
        }, {
            queue: false
        });
    });
    $('#_right_floater span').on('click', function() {
        if ($('#left_pull_filters').width() == 0) {
            $('#close_left_pull').show();
            $('#left_pull_filters').animate({
                'width': '500px',
                'margin-left': '-0px'
            }, {
                queue: false
            });
        } else {
            $('#close_left_pull').hide();
            $('#left_pull_filters').animate({
                'width': '0px',
                'margin-left': '-50px'
            });
        }
    });
          $( '#filter_information').off().on('click','#clearSearch',function(e){
                 removeFilters(e);
          });



})(jQuery);