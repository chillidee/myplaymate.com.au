{{ Form::open(['id'=>'escortAvailability']) }}
<div class="alignc">
    <h2>AVAILABILITY</h2>
    <p class ='profile_info'>Let clients know your availability by filling in the days, start times and end times.</p>
    <div class="alert alert-success">Escort info updated!</div>

    <table class="table" id="availabilityTable">
        <tbody>
            <tr class ='table_header'>
                <td>Availibility<span>*</span></td>
                <td>
                    <div>
                        <input name="24-7" type="checkbox" class="24-7">
                        <label for="24-7"><span></span>24h</label>
                    </div>
                </td>
                <td>Start Time</td>
                <td>End Time</td>
            </tr>
            <script>
                jQuery('#form').on('change', '.24-7', function() {
                    if (jQuery(this).is(':checked')){
                        jQuery('input.24').prop('checked', true);
                        jQuery('input.day-enabled').prop('checked', true);
                        jQuery('select.hours_select').prop('disabled', true);
                    }
                    else{
                        jQuery('input.24').prop('checked', false);
                        jQuery('input.day-enabled').prop('checked', false);
                        jQuery('select.hours_select').prop('disabled', false);
                    }
                });

                jQuery('#form').on('change', 'input.24', function() {
                    if (jQuery(this).is(':checked')){
                        jQuery(this).closest('tr.day').find('.day-enabled').prop('checked',true);
                        jQuery(this).closest('tr.day').find('.hours_select').prop('disabled',true);
                    }
                    else{
                        jQuery(this).closest('tr.day').find('.hours_select').prop('disabled',false);
                    }
                });
            </script>
            <?php $availabilities = EscortAvailability::whereEscortId($escort->id)->get(); ?>
            @foreach(Helpers::$weekDays as $key => $day)
                <?php $row = ''; ?>
                @foreach($availabilities as $availability)
                    @if($availability->day == $key)
                        <?php $row = $availability; ?>
                    @endif
                @endforeach
                <tr class="day">
                    <td>
                        {{ Form::checkbox('enabled_'.$key,1,!empty($row) ? $row->enabled : 0,array('class'=>'day-enabled')) }}
                        {{ Form::label('enabled_'.$key,$day) }}
                    </td>
                    <td >
                        {{ Form::checkbox('24_'.$key,1,!empty($row) ? $row->twenty_four_hours : 0,array('class'=>'24')) }}
                        {{ Form::label('24_'.$key,'24h') }}
                    </td>
                    <td>   
                        <table class ='time-table'>
                            <tr>
                                <td>{{ Form::select('start_hour_'.$key,Helpers::$dayHours,!empty($row) ? $row->start_hour_id : '',array('class'=>'hours_select','style'=>'width:100%')) }}</td>
                                <td>{{ Form::select('start_am_pm_'.$key,array('am','pm'),!empty($row) ? $row->start_am_pm : '',array('class'=>'hours_select am_select','style'=>'width:100%')) }}</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class ='time-table'>
                            <tr>
                                <td>{{ Form::select('end_hour_'.$key,Helpers::$dayHours,!empty($row) ? $row->end_hour_id : '',array('class'=>'hours_select','style'=>'width:100%')) }}</td>
                                <td>{{ Form::select('end_am_pm_'.$key,array('am','pm'),!empty($row) ? $row->end_am_pm : '',array('class'=>'hours_select am_select','style'=>'width:100%')) }}</td>
                            </tr>
                        </table>
                        

                        
                    </td>
                </tr>   
            @endforeach
        </tbody>
    </table>
</div>
<!-- End Of Availability table -->
<hr>

<div id="backend-profile-Rating" class="alignc">
    <h2>RATES</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="triggerAnimation animated fadeInUp" data-animate="fadeInUp">
                <table class="table table-striped" id="ratesTable">
                    <tbody>
                        <tr class ='table_header'>
                            <td>Incall Services</td>
                            <td>Outcall Services</td>
                        </tr>
                            <?php $escortRates = EscortRate::whereEscortId($escort->id)->first(); ?>
                            @foreach(Helpers::$rateDurations as $duration)
                            <tr class="rates-rows">
                                @foreach($duration as $key => $value)
                                    <td>
                                        <span class="rates-sign">$</span>
                                        {{ Form::text($key,$escortRates->$key > 0 ? $escortRates->$key : '',array('class'=>'rates-input','id'=>$key)) }}
                                        {{ Form::label($key,$value) }}
                                    </td>
                                @endforeach
                            </tr>
                            @endforeach    
                    </tbody>
                </table>
                <hr>
                <div class="alignc">
                    <h2>ADDITIONAL RATES INFORMATION</h2>
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::textarea('notes',$escort->notes,array('style'=>'width:100%')) }}
                        </div>
                    </div>
                </div>

               


                {{ Form::hidden('escort_id',$escort->id) }}
                {{ Form::hidden('next_icon','escort-gallery',array('class'=>'next-icon')) }}
                {{ Form::hidden('next_route','escortGallery',array('class'=>'next-route')) }}
                {{ Form::hidden('next_type','escort',array('class'=>'next-type')) }}
                <div class="alignc">
                    {{ Form::submit('UPDATE',array('class'=>'submit-button')) }}
                    {{ Form::submit('SAVE AND CONTINUE',array('class'=>'continue-submit submit-button')) }}
                </div>
            </div><!-- .triggerAnimation.animated end -->
        </div><!-- .col-md-12 end -->
    </div><!-- .row end -->
</div>
{{ Form::close() }}

<hr>

    

<script>
    (function($){
        $('.second-level h2').text("{{ $escort->escort_name }}");

        $('#incall_1h_rate').attr('required','required');

        $('body').on('click', '.deleteTouring', function(e) {
            e.preventDefault();
            var touringId = $(this)[0].id;
            $.ajax({
                url: '/deleteTouring',
                data: {touringId: touringId},
            })
            .done(function() {
                $('#touringEntry-'+touringId).remove();
                $('.alert').hide();
                $('#touring-delete-success').show();
            });
        });

        $('#addTouring').on('click', function(e) {
            e.preventDefault();

            var touringForm = $('#touring-form'),
                from_date = $('#from_date').val(),
                to_date = $('#to_date').val(),
                suburb_id = $('#suburb_id').val();

            touringForm.find('.form-error').remove();

            if (from_date == '' || to_date == '' || suburb_id == 0)
            {
                touringForm.append('<p class="form-error">Please fill all the fields.</p>');
                return;
            }

            if (from_date > to_date)
            {
                touringForm.append('<p class="form-error">The starting date must be earlier than the end date.</p>');
                return;
            }



            $('.form-error').remove();

            $.ajax({
                url: '/addTouring',
                data: {
                   from_date : from_date,
                   to_date : to_date,
                   suburb_id : suburb_id,
                   escort_id : {{ $escort->id }}
                },
            })
            .done(function(data) {
                var clone = $('.touringEntry').last().clone();
                clone[0].id = 'touringEntry-'+data.touring_id;
                clone.find('td.from_date').text(from_date);
                clone.find('td.to_date').text(to_date);
                clone.find('td.suburb_id').text(data.suburb_name);
                clone.find('.deleteTouring')[0].id = data.touring_id;
                clone.appendTo('#touringTable tbody').show();
                $('#touring-form input').val('');
                $('.alert').hide();
                $('#touring-add-success').show();
            });
        });
    })(jQuery);
</script>