<section class="page-wrapper top-space">
    <div class="container">
		
		<div class="row" style="line-height: normal; vertical-align: middle; padding-top: 30px">
			<div class="col-md-12" style="line-height: normal; vertical-align: middle; text-align: center">
				<span style="font-size: 4em; color: #FF0004; text-align: center">
					It's Simple to Sign Up and List with Us
				</span>
			</div>
		</div>
		
		<div class="row"style="line-height: normal; vertical-align: middle; padding-top: 30px">
			<div class="col-md-4" style="line-height: normal; text-align: center">
				<img src="http://staging.myplaymate.com.au/assets/emails/Form.png" alt="checklist icon" height="150">
				<h1 style="font-size: 2em; color: #FF0004; text-transform: uppercase">
					Verify &amp; Update
				</h1>
				<p style="font-size: 1.5em; color: #8B8B8B;">
					An email will be sent to you for you to verify your email address.
					You can then use that link to update your profile by adding your bio, pictures and services.
				</p>
			</div>
			<div class="col-md-4" style="line-height: normal; text-align: center">
				<img src="http://staging.myplaymate.com.au/assets/emails/Credit%20Card.png" alt="credit card icon" height="150">
				<h1 style="font-size: 2em; color: #FF0004; text-transform: uppercase">
					Payment
				</h1>
				<p style="font-size: 1.5em; color: #8B8B8B;">
					We'll then contact you to make payment with EFT, bank transfer or credit/debit card.
					For more information, please see our <a href="http://staging.myplaymate.com.au/escort-pricing">Pricing Guide</a>.
				</p>
			</div>
			<div class="col-md-4" style="line-height: normal; text-align: center">
				<img src="http://staging.myplaymate.com.au/assets/emails/Condom.png" alt="condom icon" height="150">
				<h1 style="font-size: 2em; color: #FF0004; text-transform: uppercase">
					Have Fun
				</h1>
				<p style="font-size: 1.5em; color: #8B8B8B;">
					Your account will then go live and be listed publicly.
					You'll be connected to more than 134,000 clients across our platforms every month!
				</p>
			</div>
		</div>
		
		<div class="row" style="line-height: normal; vertical-align: middle; margin-bottom: 0">
			<div class="col-sm-12" style="background-color: #FF0004; padding: 20px 30px 0 30px; text-align: center; ">
				<p style="font-size: 2em; color: #FFF; text-transform: capitalize">
				   So why wait? List Your Business right now!
				</p>
			</div>
		</div>
		
		<div class="row" style="line-height: normal; vertical-align: middle; margin-top: 0">
			<div class="col-sm-6" style="background-color: #FF0004; padding: 20px; text-align: center;">
				<a onclick="openSignUp('escort');" style="background: #FFF; border: 15px solid #FFF; color: #FF0004; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold; font-size: 2em;"> List as an Escort Now </a>
			</div>
			<div class="col-sm-6" style="background-color: #FF0004; padding: 20px; text-align: center;">
				<a onclick="openSignUp('business');" style="background: #FFF; border: 15px solid #FFF; color: #FF0004; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold; font-size: 2em;"> List My Business Now </a>
			</div>
		</div>
				
		<div class="row" style="line-height: normal; vertical-align: middle; padding-top: 10px">
			<div class="col-md-2 col-md-offset-3" style="padding-top: 30px; text-align: center">
				<img src="http://staging.myplaymate.com.au/assets/advertise/questionmark.png" width="200" alt="Question Mark" style="border: 0;">
			</div>
			
			<div class="col-md-4 col-md-offset-1" style="padding-top: 30px">
				<span style="font-size: 2em; color: #FF0004; text-transform: capitalize">
					Still have questions?
				</span>
				<p style="font-size: 1.5em">
					Enquire by sending us an email at <a href="mailto:admin@myplaymate.com.au">admin@myplaymate.com.au</a> or give us a call at <a href="tel:1300769766">1300 769 766</a> with your questions and we'll help answer those burning hot questions.
				</p>
				<br>
				<a href="http://myplaymate.com.au/contact" style="background: #ff0f0f; border: 15px solid #ff0f0f; padding 0 10px; color: #FFF; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold; width: 100%; font-size: 1.5em"> Contact Us </a><br>
			</div>
		</div>
	</div>
</section>
<script>
	function openSignUp(tab){		
		$('#sign-up').click();
		
		if(tab=='business')			
			$('#tb_business').click();

		if(tab=='escort')
			$('#tb_escort').click();
	}
</script>