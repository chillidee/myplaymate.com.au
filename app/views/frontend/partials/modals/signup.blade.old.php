<div class="modal fade backdrop auth-modal" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-content signup-container">
    <div class="">
      <div class="modal-header">
        {{--<img src="/assets/frontend/img/logoblack.jpg" alt="MyPlaymate. The place to be.">--}}
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <p class ='header'>WELCOME, TO MYPLAYMATE</p>
      </div>
      <div class ='tab_headers'>
        <?php $type = Session::get('type');
              $theType = $type['type'];  ?>
        <a id ='tb_escort' @if ( $theType == 3 || $theType == '' )class ='tb_active' @endif></a>
        <a id ='tb_business' @if ( $theType == 4 )class ='tb_active' @endif></a>
        <a id ='tb_customer' @if ( $theType == 2 )class ='tb_active' @endif></a>
      </div>
      <div class ='tb_tabs'>
        <div class ='col-md-6 large_only' id ='tb_esc_tab'>
            <?php /*----------------------------------------------------------------------------------------------------------
            -                                               Escort Tab
            ------------------------------------------------------------------------------------------------------------------*/?>
            <h6>ESCORT</h6>
            Take control of your personal business. Access more than 134,000 clients that use My Playmate every month. Use our tasteful and user-friendly platform to create your own profile, upload your sexiest images and get yourself noticed!<br>
            Find our pricing structure <a style="color: #F55;" href ='/escort-pricing'>HERE.</a>
            Why choose us? Check out our <a style="color: #F55;" href ='/faqs'>FAQ.</a>
            <div class ='tb_icon_group'>
             <div id ='tb_icon_heart'>
               <span>SAVE YOUR FAVOURITES</span>
               When potential clients sign up to MyPlaymate, they have the option of saving their favourite escorts as well as escorts they want to book in the future. This means you can generate a regular client base.
             </div>
             <div id ='tb_icon_support'>
               <span>GREAT SUPPORT FROM OUR TEAM</span>
               We are passionate about what we do. We can help you whenever you need tech support, help with editing or if you want to contact other businesses in the industry.<br>
               
               We are available 8:30am – 5:30pm, Monday to Friday via phone on 1300 769 766. Or we can help you after hours via email: <a href = 'mailto:admin@myplaymate.com.au'>admin@myplaymate.com.au.</a>
             </div>
             <div id ='tb_icon_message'>
               <span>INTRODUCTING PLAYMAIL!</span>
               Playmail is a fun way to message your clients through online instant messaging. Just tick the Playmail option when you set up your profile.
             </div>
           </div>
         </div>
               <?php /*----------------------------------------------------------------------------------------------------------
            -                                               Business Tab
            ------------------------------------------------------------------------------------------------------------------*/?>
            <div class ='col-md-6 large_only' id ='tb_bus_tab'>
             <h6>BUSINESS</h6>
             Take control of your business. Access more than 134,000 clients that use My Playmate every month. Use our tasteful and user-friendly platform to create your business profile and add up to 20 escorts.
             Find our full packages and pricelist <a style="color: #F55;" href ='/business-pricing'>HERE.</a><br>
             Why choose us? Check out our <a style="color: #F55;" href ='/faqs'>FAQ.</a>
             <div class ='tb_icon_group'>
               <div id ='tb_icon_heart'>
                 <span>SAVE YOUR FAVOURITES</span>
                 When potential clients sign up to MyPlaymate, they have the option of saving their favourite escorts as well as escorts they want to book in the future. This means you can generate a regular client base.
               </div>
               <div id ='tb_icon_support'>
                 <span>GREAT SUPPORT FROM OUR TEAM</span>
                 We are passionate about what we do. We can help you whenever you need tech support, help with editing or if you want to contact other businesses in the industry.<br>
                 
                 We are available 8:30am – 5:30pm, Monday to Friday via phone on 1300 769 766. Or we can help you after hours via email: <a href = 'mailto:admin@myplaymate.com.au'>admin@myplaymate.com.au.</a>
               </div>
               <div id ='tb_icon_message'>
                 <span>INTRODUCING PLAYMAIL!</span>
                 Playmail is a fun way for clients to get in contact with you through online instant messaging. Just make sure to pick an email address used regularly for your business and tick the Playmail option when you set up your escort profiles.
               </div>
             </div>
           </div>
               <?php /*----------------------------------------------------------------------------------------------------------
            -                                               Member Tab
            ------------------------------------------------------------------------------------------------------------------*/?>
            <div class ='col-md-6 large_only' id ='tb_cust_tab'>
             <h6>CUSTOMER</h6>
             Browse dream girl escorts and find the best adult establishments in your area (and all over Australia). Keep up to date with who is on tour. Easily contact the escorts you want to book.
             <div class ='tb_icon_group'>
               <div id ='tb_icon_heart'>
                 <span>SAVE YOUR FAVOURITES</span>
                 Had an unforgettable experience with an escort and want to make sure you can find them again? Save your favourite escorts and as well as people you might want to book in the future. 

               </div>
               <div id ='tb_icon_support'>
                 <span>GREAT SUPPORT FROM OUR TEAM</span>
                 We are passionate about what we do. We can help you whenever you need tech support, help with editing or if you want to contact other businesses in the industry.<br>
                 
                 We are available 8:30am – 5:30pm, Monday to Friday via phone on 1300 769 766. Or we can help you after hours via email: <a href = 'mailto:admin@myplaymate.com.au'>admin@myplaymate.com.au.</a>
               </div>
               <div id ='tb_icon_message'>
                 <span>INTRODUCING PLAYMAIL</span>
                 Playmail is a fun and discrete way to message escorts through online instant messaging. Just click the Playmail button on an escort’s profile.
               </div>
             </div>
           </div>
           <div class ='col-md-6'>

             <div class="modal-body">
              {{ Form::open(array('route' => 'postRegister','id'=>'signup-form')) }}

              {{ Form::text('type',( $theType == '' ? '3' : $theType ),array('class'=>'hidden','id'=>'typeSelect','required')) }}


              <div class="control-group">
                <div class="controls">
                  {{ Form::text('full_name','',array('data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Escort Name','required')) }}
                </div>
              </div>
              @if($errors->has('full_name'))
              <p class="form-error">
                {{ $errors->first('full_name') }}
              </p>
              @endif
              <div class="control-group">
                <div class="controls">
                  {{ Form::text('phone','',array('data-parsley-pattern-message' => 'Must be a valid email phone number!','data-parsley-length'=>'[8,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Phone','required')) }}
                </div>
              </div>
              @if($errors->has('phone'))
              <p class="form-error">
                {{ $errors->first('phone') }}
              </p>
              @endif
              <div class="control-group">
               <div class="controls">
                 <?php $pattern = "[^@]+@[^@]+\.[^@]+";?>
                 {{ Form::text('email','',array('data-parsley-pattern-message' => 'Must be a valid email address!','type' => "email",'data-parsley-type'=>'email','pattern'=>$pattern,'data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Email','required')) }}
               </div>
             </div>
             @if($errors->has('email'))
             <p class="form-error">
               {{ $errors->first('email') }}
             </p>
             @endif
             <div class="control-group">
              <div class="controls">
                {{ Form::password('password',array('id'=>'password1','data-parsley-group' => 'loginForm','data-parsley-equalto-message' => 'Passwords must match!','data-parsley-equalto' => '#password2','class'=>'form-control','placeholder'=>'Password','required')) }}
              </div>
            </div>
            @if($errors->has('password'))
            <p class="form-error">
              {{ $errors->first('password') }}
            </p>
            @endif
            <div class="control-group">
              <div class="controls">
               {{ Form::password('password_confirmation',array('data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Repeat password','required','id'=>'password2')) }}
             </div>
           </div>

           <div id="terms-and-conditions" class="checkbox">
            <label for="terms_and_conditions">
              <input type="checkbox" name="terms_and_conditions" required data-parsley-required-message = 'You must accept the terms and conditions'/> By ticking the box, you confirm that you are over the age of eighteen (18) years and agree to our <a href="https://myplaymate.com.au/advertiser-terms-and-conditions" target="_blank">Terms &amp; Conditions</a>.
            </label>
          </div>
          @if($errors->has('terms_and_conditions'))
          <p class="form-error">
            {{ $errors->first('terms_and_conditions') }}
          </p>
          @endif

          <div class="control-group">
            <div class="controls">
              {{ Form::submit('Get Started',array('class'=>'submit-button', 'id'=>'register-submit')) }}
            </div>
          </div>

          <div class="alignc">
            <a class="swap-modal" href="#login" data-dismiss="modal" data-toggle="modal">Existing Member? Login Here!</a>
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
</div>
</div>



