@extends('frontend.layout')
@section('content')
@include('frontend.partials.topbar.right_floater')
<div id ='left_pull_filters' class ='no_close'>
  <div id ='close_left_pull'></div>
  <p class="selected-filters-title">Search</p>
  <form id ='custom_search_escorts'>
    <input id="escort_side-search" name="s" type="text" placeholder="Type and hit enter..." autocomplete="off"/>
  </form>
  @include('frontend.partials.sidebar.escort-filters')
</div>
<!-- #page-title start -->
<section id="page-title" class="page-title-1">
  <div class="container">

    @if(isset($status))
    <h1 class ='_smaller'>This Profile is currently unavailable.  Here are some other girls you might like</h1>
    @else
    <h1>@if(isset($meta)) {{$meta->name}} @else Escorts @endif</h1>
    @endif
    <div id ='filter_information' class ='no-close'></div>
  </div><!-- .containr end -->
</section><!-- #page-title end -->

<!-- .boxed version of the page -->
<section class="page-wrapper section-content">
  <!-- .container start -->
  <div class="container">

    <!-- .row start -->
    <div class="row">
          @if(isset($meta->top_content))
      <section class="top-content">
        {{ nl2br($meta->top_content) }}
      </section>
      @endif
      <div id="directory-container" class="col-xs-12">
        <div id="directory">
          <ul class="escort-container grid-view">
            <li class="grid-template col-xs-12 col-ms-6 col-md-4 col-lg-3 col-xl-15 col-xl-3 escorts-wishlist">
              <figure class="escorts-img-container">
                <div class="figfavorite">
                  <a href="" class="is-not-wish">
                    <i class="fa fa-heart"></i>
                  </a>

                  <a href="" class="is-wish">
                    <i class="fa fa-times"></i>
                  </a>
                </div>

                <div class="info-container">
                  <div class="figinfo">
                    <span class="figname"></span>
                    <ul class="figdata">
                      <li class="escort-location">
                        <span class = 'business-suburb'></span>
                        @if(isset($city))
                        <span class="escort-distance"> km from {{ ucfirst($city) }}</span></li>
                        @else
                        <span class="escort-distance"> km</span></li>
                        @endif
                      </li></ul>
                    </div><!-- figinfo-->
                  </div><!-- end of info-container-->
                  <a href="{{ url('escorts/') }}">
                    <img class="img-profile" src="" alt="">
                  </a>
                </figure>
              </li>

            </ul>
            <div class="spinner">
               <div class="dot1"></div>
               <div class="dot2"></div>
            </div>
          </div>
        </div>
      </div><!-- .row end -->
    </div><!-- .container end -->
  </section> <!-- End of the Boxed Version Page -->

  <section class="page-wrapper">
    <!-- .container start -->
    <div class="container">

      <!-- .row start -->
      <div class="row">
        <div id="touring-container" class="col-xs-12">
          <div id="Touringdirectory">
            <ul class="touring-container grid-view">


            </ul>
          </div>
        </div>
      </div><!-- .row end -->
    </div><!-- .container end -->
  </section> <!-- End of the Boxed Version Page -->

  @stop

  @section('javascript')

  <script>

    window.type ='escort';

    jQuery(document).ready(function($) {

      if (typeof check == 'undefined') {
       applyFilters();
     }
     else {
       load_saved_escorts(check.show);
     }

     var toggleFilters = $('#toggleFilters');
     if($(window).width() < 768){
      toggleFilters.show();
    }
    toggleFilters.on('click', function(e) {
      e.preventDefault();
      $('.filters').fadeToggle(300);
    });
  });

</script>
<style>#trigger-overlay-mobile { display:block; }</style>

@stop
