<!DOCTYPE HTML>
<html lang="en">
    <head lang="en-AU" prefix="og: http://ogp.me/ns#">
    <meta charset="UTF-8" />
    <title>{{ isset($meta) ? $meta->meta_title : '' }}</title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="{{ isset($meta) ? $meta->meta_description : '' }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" value="{{ csrf_token() }}">
    @if(isset($meta))
      <link rel="canonical" href="https://myplaymate.com.au{{ $_SERVER['REQUEST_URI'] }}" />
    @endif
    {{-- Helpers::latestCss('build.min.css') --}}
    {{-- <link rel="stylesheet" href="/assets/frontend/css/style.css"> --}}
    @yield('css')
    <link rel="stylesheet" href="/assets/frontend/css/custom.css">
      <link rel="stylesheet" href="/assets/frontend/css/animate.css">
      <link rel="stylesheet" href="/assets/frontend/css/banner.css">
      <link rel="stylesheet" href="/assets/frontend/css/bootstrap.min.css">
      <link rel="stylesheet" href="/assets/frontend/css/dropzone.css">
      <link rel="stylesheet" href="/assets/frontend/css/font-awesome.min.css">
      <link rel="stylesheet" href="/assets/frontend/css/jquery-ui.min.css">
      <link rel="stylesheet" href="/assets/frontend/css/lato.css">
      <link rel="stylesheet" href="/assets/frontend/css/pgwslideshow.min.css">
      <link rel="stylesheet" href="/assets/frontend/css/nivo-slider.css">
      <link rel="stylesheet" href="/assets/frontend/css/owlcarousel.css">
      <link rel="stylesheet" href="/assets/frontend/css/retina.css">
      <link rel="stylesheet" href="/assets/frontend/css/search-select2.css">
      <link rel="stylesheet" href="/assets/frontend/css/nivo-slider.css">
      <link rel="stylesheet" href="/assets/frontend/css/select2.min.css">
      <link rel="stylesheet" href="/assets/frontend/css/sidemenu.css">
      <link rel="stylesheet" href="/assets/frontend/css/style.css">
      <link rel="stylesheet" href="/assets/frontend/css/josephin.css">
      
      
    <link rel="stylesheet" href="/assets/frontend/css/custom.css">
    <!--[if lte IE 8]>
    <link href="{{ url('/assets/frontend/css/ie.css') }}" rel="stylesheet" />
    <![endif]-->
    @include('frontend.partials.head.favicons')
    <meta name="google-site-verification" content="p5uVVgW7ZgDrR-1N6jgNUGnr7zhjLNKBZ1aNWFwc1zo" />
    <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"{{ url('/').$_SERVER['REQUEST_URI'] }}","name":"MyPlaymate","potentialAction":{"@type":"SearchAction","target":"{{ url('/').$_SERVER['REQUEST_URI'] }}?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
      <link href='https://fonts.googleapis.com/css?family=Lora|Roboto+Slab:400,700|Raleway' rel='stylesheet' type='text/css'>
    <meta property="og:locale" content="en_AU" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ isset($meta) ? $meta->meta_title : '' }}" />
    <meta property="og:description" content="{{ isset($meta) ? $meta->meta_description : '' }}"/>
    <meta property="og:url" content="{{ url('/').$_SERVER['REQUEST_URI'] }}"/>
    <meta property="og:site_name" content= "Myplaymate- Australia's leading Escort directory" />
</head>
  <style>
    .modal-backdrop.in { display:none!important; }
    </style>

<body class ='profile-body'>
  <div class="cc-container">
      <div class="circle"></div>
  </div>
  
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

        <button class="showLeftPush subMenu"><i class="fa fa-bars"></i></button>

        <div id="sidemenu-search" class="side-items">
            <h4>Search</h4>
            <form action="/escorts" method="GET" name="search">
                <input id="side-search" name="s" type="text" placeholder="Type and hit enter..." autocomplete="off"/>
            </form>
        </div>
    </nav>

    <div id="mpm-header-wrapper" class="<?php echo ($_SERVER['REQUEST_URI'] == '/') ? 'header-banner' : 'header-simple'; ?> profile_top_header">
        <div id="top-bar">
          <button class="arrowToggle" id="ProfilemobileArrowToggle"></button>
            <div class="container">
                <div id="profile-logo">
                       <a href="https://myplaymate.com.au">
                            <img src="https://myplaymate.com.au/assets/frontend/img/logo.png" alt="Myplaymate.com.au. the place to be">
                       </a>
                </div>
                        @include('frontend.partials.profile.topbar-userlinks')
            </div>
        </div>
  </div>

        <header id="header" class="<?php echo ($_SERVER['REQUEST_URI'] == '/') ? 'header-style-01' : 'header-style-02'; ?> profile-header">
             <ul class="nav navbar-nav" id ="profile_menu">
                      @if( !Auth::user()->escort() )
                         @if( !Auth::user()->businesses )
                                   <li class ='mobileNewEscort'><a href="">New escort</a></li>  
                         @endif  
                      @endif
                      @if(!Auth::user()->escort() )
                              <li class="mobileNewBusiness"><a href="">New business</a></li>
                      @endif
                      @if(Auth::user()->escort())
                      <li><a id = "mobile_view_escort" data-id = "{{ Auth::user()->escort()->id }}" href="">{{ Auth::user()->escort()->escort_name }}</a></li>
                      @endif
                      @if(Auth::user()->businesses)
                         @foreach(Auth::user()->businesses as $business)
                              <li>
                                   <a class="mobile-view-business" data-business-id="{{ $business->id }}"href="">{{ $business->name }}</a>
                              </li>
                         @endforeach
                      @endif
                               <li><a href="{{ route('getChangePassword') }}">Change password</a></li>
                                      @if(isset(Auth::user()->escort()->id))
                               <li><a href="{{ url('escorts/preview/'.Auth::user()->escort()->id) }}">Profile Preview</a></li>
                        @endif
                        @if ( !isset(Auth::user()->escort()->id))
                               <li><a href="/wishlist">Your Favourites</a></li>
                        @endif
                              <li><a href="{{ route('getContact') }}">Contact us</a></li>
                              <li><a href="{{ route('logout') }}">Sign Out</a></li>
</ul>  
            <!-- .container start -->
            <div class="container">
          <!-- <h1>My Dashboard</h1> -->
            </div><!-- .container end -->
            </div>
        </header><!-- #header end -->

    <div id="center" class="profile_view">
        <section>
            @yield('content')            
        </section>
    </div>
    <!-- Footer Area -->
    <div id="footer-wrapper">
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <ul class="col-md-3 col-sm-6 footer-widget-container">
                        {{ StaticBlock::getHtml('footer-col-1') }}

                        @include('frontend.partials.footer.social')
                    </ul>

                    @include('frontend.partials.footer.posts')

                    {{ StaticBlock::getHtml('footer-col-3') }}

                    {{ StaticBlock::getHtml('footer-col-4') }}
                </div>
            </div>
        </footer>

        <div id="copyright-container">
            <div class="container">
                 {{ StaticBlock::getHtml('footer-copyright') }}
            </div>
        </div>

        <a href="#" class="scroll-up">Scroll</a>
    </div><!-- .footer-wrapper end -->

    <div class="modal fade backdrop" id="geoMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ></div>
    
    <div class="overlay"></div>
    @include('frontend.partials.modals.login')
    @include('frontend.partials.modals.signup')
    @include('frontend.partials.modals.registration-success')

    @include('frontend.partials.modals.welcome')

    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    {{ Helpers::latestJs('build.min.js') }}
      @if(isset($session))
  <script src="{{$session}}?{{time()}}"></script>
    @endif
<script>

    jQuery(document).ready(function($) {
     
      var currentState = Cookies.get('state');
      var path = $('#mapId-' + currentState);
      path.attr('fill', colors[path.index() + 1]);
      
        $('.is-not-wish').show();
        @if(Auth::check())
            var userId = {{ Auth::user()->id }};
            getUserWishlist(userId);
            $('body').on('click', '.is-not-wish', function(e) {
                addToWishlist(e, $(this), userId);
            });
            $('body').on('click', '.is-wish', function(e) {
                removeFromWishlist(e, $(this), userId);
            });
        @else
            $('.is-not-wish').show();
            $('.is-not-wish').attr({'href':'#login','data-toggle':'modal'});
        @endif

        if ('{{ Session::get('loginFailed') }}' == 'failed')
            $('#login').modal('show');
        if ('{{ Session::get('registrationFailed') }}' == 'failed' || window.location.hash == '#signup')
            $('#signup').modal('show');
        if ('{{ Session::get('registrationSuccess') }}' == 'success')
            $('#registration-success').modal('show');
    });
</script>

@yield('javascript')

@yield('modal')

</body>
</html>