<?php

class ServicesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('services')->truncate();

        $services = [
            [
                'name' => 'Oral sex',
                'order' => 10
            ],
            [
                'name' => 'Girlfriend Experience',
                'order' => 9
            ],
            [
                'name' => 'Boyfriend Experience',
                'order' => 6
            ],
            [
                'name' => 'BDSM',
                'order' => 5
            ],
            [
                'name' => 'Fantasy/Role Play',
                'order' => 8
            ],
            [
                'name' => 'Erotic Massage',
                'order' => 7
            ],
            [
                'name' => 'Anal',
                'order' => 4
            ],
            [
                'name' => 'Other',
                'order' => 26
            ],
            [
                'name' => 'Overnight',
                'order' => 11
            ],
            [
                'name' => '1h Minimum',
                'order' => 2
            ],
            [
                'name' => '2h Minimum',
                'order' => 3
            ],
            [
                'name' => '30min Minimum',
                'order' => 1
            ],
            [
                'name' => 'Exotic Dancers',
                'order' => 12
            ],
            [
                'name' => 'Strippers',
                'order' => 13
            ],
            [
                'name' => 'Waitressing',
                'order' => 14
            ],
            [
                'name' => '3some',
                'order' => 15
            ],
            [
                'name' => 'PSE',
                'order' => 16
            ],
            [
                'name' => 'Striptease',
                'order' => 17
            ],
            [
                'name' => 'Dinner Date',
                'order' => 18
            ],
            [
                'name' => 'Costumes',
                'order' => 19
            ],
            [
                'name' => 'Full Service',
                'order' => 0
            ]
        ];

        // Uncomment the below to run the seeder
        DB::table('services')->insert($services);
    }
}
