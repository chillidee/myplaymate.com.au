<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('ApprovalStatusesTableSeeder');
        $this->call('CitiesTableSeeder');
        $this->call('CountriesTableSeeder');
        $this->call('BodiesTableSeeder');
        $this->call('EthnicitiesTableSeeder');
        $this->call('EyeColorsTableSeeder');
        $this->call('GendersTableSeeder');
        $this->call('HairColorsTableSeeder');
        $this->call('HeightsTableSeeder');
        $this->call('HourlyratesTableSeeder');
        $this->call('LanguagesTableSeeder');
        $this->call('ServicesTableSeeder');
        $this->call('StatesTableSeeder');
        $this->call('SuburbsTableSeeder');
        $this->call('StaticBlocksTableSeeder');
        $this->call('AgesTableSeeder');
        $this->call('PagesTableSeeder');
        $this->call('EmailTemplatesTableSeeder');

        // Faker
        // $this->call('FakeImagesTableSeeder');
        // $this->call('FakeUpdatesTableSeeder');
        // $this->call('FakePostsTableSeeder');
        // $this->call('FakeUsersTableSeeder');
        // $this->call('FakeBusinessesTableSeeder');
        // $this->call('FakeEscortBusinessTableSeeder');
        // $this->call('FakeEscortServicesTableSeeder');
        // $this->call('FakeEscortRatesTableSeeder');
        // $this->call('FakeEscortTouringsTableSeeder');
        // $this->call('FakeEscortsTableSeeder');
    }
}
