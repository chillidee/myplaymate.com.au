@if($escort->images()->whereStatus(2)->count())
                    <ul class="pgwSlideshow">
                        @foreach($escort->images()->whereStatus(2)->orderBy('order', 'asc')->get() as $key =>$image)
                            <li><img src="{{ $image->thumbnail(170,170) }}" data-large-src="{{ $image->url() }}"></li>
                        @endforeach
                    </ul>                  
@endif