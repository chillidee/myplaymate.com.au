<?php

class FakeEscortServicesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        // DB::table('escorts')->truncate();

        $faker = Faker\Factory::create();

        Db::table('escort_service')->insert([
            'escort_id'  => 1,
            'service_id' => 2
        ]);
        Db::table('escort_service')->insert([
            'escort_id'  => 1,
            'service_id' => 4
        ]);
        Db::table('escort_service')->insert([
            'escort_id'  => 2,
            'service_id' => 2
        ]);
        Db::table('escort_service')->insert([
            'escort_id'  => 5,
            'service_id' => 3
        ]);
        Db::table('escort_service')->insert([
            'escort_id'  => 2,
            'service_id' => 1
        ]);
        Db::table('escort_service')->insert([
            'escort_id'  => 5,
            'service_id' => 1
        ]);
        Db::table('escort_service')->insert([
            'escort_id'  => 2,
            'service_id' => 4
        ]);

    }
}
