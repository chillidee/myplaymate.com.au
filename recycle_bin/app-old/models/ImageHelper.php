<?php

class ImageHelper
{
	public static function cropBusinessLogo($originalPath, $originalFilename)
	{
		$img = Image::make($originalPath.$originalFilename);
		$img->fit(870,580)
			->save($originalPath.'/'.$originalFilename);
	}

    public static function createThumbnailSize($originalPath, $originalFilename, $width, $height)
    {
		$img = Image::make($originalPath.$originalFilename);

		if($img->width() > $img->height()){
			$img->resize(null,$height,function($i){
				$i->aspectRatio();
			});
		} else {
			$img->resize($width,null,function($i){
				$i->aspectRatio();
			});
		}

		$img->fit($width,$height)
			->save($originalPath.'thumbnails/thumb_'.$width.'x'.$height.'_'.$originalFilename);
    }

    public static function watermark($path)
    {
		$img = Image::make(public_path().'/'.$path);

		$img->resize(null,500,function($constraint){
			$constraint->aspectRatio();
		});
		$width = $img->width();
		$watermark = Image::make(public_path().'/assets/frontend/img/watermark.png')
						  ->resize($width, null, function ($constraint) {
	    						$constraint->aspectRatio();
		});
		$img->insert($watermark, 'center')
			->save(public_path().'/'.$path.'_watermark.jpg');
    }
}










