<?php

class ImageController extends Controller
{
	public function setMainImage()
	{
		$escort = Escort::find(Input::get('escort_id'));
		$escort->main_image_id = Input::get('image_id');
		$escort->save();
	}

	public function updateOrder()
	{
		$input = Input::all();
		foreach ($input['images'] as $image) {
			$escortImage = EscortImage::find($image[0]);
			$escortImage->order = $image[1];
			$escortImage->save();
		}
	}

	public function deleteImage()
	{
		$image = EscortImage::find(Input::get('image_id'));
		$filename = $image->filename;
		$image->delete();

		// Fallback on the first non-main image in the slideshow or on 0 (white logo)
		$alternativeImage = EscortImage::whereEscortId(Input::get('escort_id'))->orderBy('order')->first();
		$escort = Escort::find(Input::get('escort_id'));
		if ($alternativeImage)
			$escort->main_image_id = $alternativeImage->id;
		else
			$escort->main_image_id = 0;

		$escort->save();

	

		if (file_exists(public_path().'/uploads/escorts/'.$filename))
			unlink(public_path().'/uploads/escorts/'.$filename);

		if (file_exists(public_path().'/uploads/escorts/thumbnails/thumb_170x170_'.$filename))
			unlink(public_path().'/uploads/escorts/thumbnails/thumb_170x170_'.$filename);

		if (file_exists(public_path().'/uploads/escorts/thumbnails/thumb_220x330_'.$filename))
			unlink(public_path().'/uploads/escorts/thumbnails/thumb_220x330_'.$filename);
	}
}