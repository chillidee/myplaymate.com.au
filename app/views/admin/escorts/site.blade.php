@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Edit escort site - {{ $escort->escort_name }}</h1>
    </div>
</div><!--pageheader-->

@include('admin.partials.escorts-breadcrumb')

<div class="row section-content">
	<div class="col-md-6 col-md-offset-3">
    	{{ Form::open() }}
        <div class="section-content">
            {{ Form::label('use_own_url','Use your own website*',array('class'=>'control-label')) }}
            <div class="controls">
                {{ Form::select('use_own_url',array('No, I don\'t have a website','Yes'),$escort->use_own_url,array('class'=>'form-control','id'=>'use-own-url')) }}
            </div>
        </div>
        <div class="section-content own-url">
            {{ Form::label('own_url','Website URL*',array('class'=>'control-label')) }}
            <div class="controls">
                {{ Form::text('own_url',$escort->own_url,array('class'=>'form-control','data-parsley-type'=>'url')) }}
            </div>
        </div>
        {{ Form::hidden('escort_id',$escort->id) }}
        {{ Form::hidden('next_icon','escort-reviews',array('class'=>'next-icon')) }}
        {{ Form::hidden('next_route','escortReviews',array('class'=>'next-route')) }}
        {{ Form::hidden('next_type','escort',array('class'=>'next-type')) }}
        <div class="section-content alignc">
            {{ Form::submit('SAVE AND CONTINUE',array('class'=>'btn btn-primary')) }}
        </div>
    {{ Form::close() }}	
	</div>
</div>
	

@stop