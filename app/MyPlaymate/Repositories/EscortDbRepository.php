<?php

namespace MyPlaymate\Repositories;

use Escort;
use Carbon\Carbon;

class EscortDbRepository {

	public function getShowable()
	{
		return Escort::whereActive(1)
					 ->whereStatus(2)
					 ->where('suburb_id','>',0)
					 ->get(['id','escort_name','age_id','gender_id','suburb_id','featured','about_me','seo_url','main_image_id','hair_color_id','eye_color_id','does_incalls','does_outcalls','does_travel_internationally','ethnicity_id','body_id'])
					 ->toArray();
	}

	// public function getFeatured()
	// {
	// 	return Escort::whereActive(1)
	// 				 ->whereStatus(2)
	// 				 ->whereFeatured(1)
	// 				 ->where('suburb_id','>',0)
	// 				 ->get(['id','escort_name','age_id','suburb_id','seo_url','main_image_id']);
	// }

	public function getNewest()
	{
		return Escort::where('created_at','>',Carbon::now()->subDays(900))
					 ->whereActive(1)
					 ->whereStatus(2)
					 ->where('suburb_id','>',0)
					 ->orderBy('created_at','desc')
					 ->take(6)
					 ->get();
	}

	public function getBumpedUp()
	{
		return Escort::whereActive(1)
					 ->whereStatus(2)
					 //->where('suburb_id','>',0)
					 ->orderBy('bumpup_date', 'desc')
					 ->get(['id','escort_name','age_id','suburb_id', 'suburb_name', 'seo_url','main_image_id']);
	}
}








