<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortTouringsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escort_tourings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('escort_id');
			$table->timestamp('from_date');
			$table->timestamp('to_date');
			$table->integer('suburb_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escort_tourings');
	}

}
