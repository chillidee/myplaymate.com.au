<?php

class Business extends \Eloquent {
  protected $fillable = [];

   /**
     * @return mixed
     */
    public function escorts()
    {
        return $this->hasMany('Escort');
    }
        public function sendPlayMail($input)
    {
       try {
            $emailTemplate = EmailTemplate::where('name', 'playmail')->first();

            $data                = [];
            $data['escort_name'] = $this->name;
            $data['email']       = ( !App::environment('development') ? $this->email : Config::get('constants.DEV_EMAIL') );
            $data['user']        = $this->user;
            $data['message']     = $input;
            $data['id'] = $this->id;

            // Send activation email to new user and to admin
            Mail::send('emails.playmail', ['data' => $data], function ($message) use ($data) {
                $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                        ->subject('New Playmail for you from My Playmate')
                        // ->cc(Config::get('app.adminEmail'))
                        ->to($data['email'])
                        ->replyto($data['message']['email']);
            });
            Mail::send('emails.playmail', ['data' => $data], function ($message) use ($data) {
                $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                        ->subject('New Playmail to Business #' . $data['id'])
                        // ->cc(Config::get('app.adminEmail'))
                        ->to('contact@myplaymate.com.au');
            });


        } catch (Exception $ex) {
            return false;
        }
    }
    public function escortsStatusCount()
    {
      $escorts = $this->hasMany('Escort')->get();
      $sort = array( 'Pending' => 0, 'Approved' => 0, 'Disapproved' => 0, 'Unavailable' => 0);
      foreach( $escorts as $escort ) {
          switch( $escort->status ) {
              case '1':
                  $sort['Pending']++;
              break;
              case '2':
                  $sort['Approved']++;
              break;
              case '3':
                  $sort['Disapproved']++;
              break;
              case '4':
                  $sort['Unavailable']++;
              break;

           }
      }
      return $sort;
    }
      public function escortsCount()
    {
      return $escorts = $this->hasMany('Escort')->get()->count();
    }
      public function escortsStats()
    {
      $escorts = $this->hasMany('Escort')->get(array('id','escort_name','count_phone','count_playmail','count_views','main_image_id'));
      foreach ( $escorts as $escort ) {
        $image = EscortImage::where('id','=',$escort->main_image_id )->first();
        if ( $image ){
            $escort->main_image = $image->filename;
        }
        else {
            $escort->main_image = 'qCe0BsNoHWu3.jpg';
        }
      }
      return $escorts;
    }
    public function getEscorts()
    {
        $escorts = $this->hasMany('Escort')->get();
        foreach ( $escorts as $escort ) {
        $image = EscortImage::where('id','=',$escort->main_image_id )->first();
        if ( $image )
        $escort->main_image = $image->filename;
      }
      return $escorts;
    }
   public function getApprovedEscorts()
    {
        $escorts = $this->hasMany('Escort')->where('status','=',2)->get();
        foreach ( $escorts as $escort ) {
        $image = EscortImage::where('id','=',$escort->main_image_id )->first();
        if ( $image )
        $escort->main_image = $image->filename;
      }
      return $escorts;
    }
     public function getJsonEscorts()
    {
        $escorts = $this->hasMany('Escort')->get();
        $return = array();
        $i = 0;
        foreach ( $escorts as $escort ) {
        $image = EscortImage::where('id','=',$escort->main_image_id )->first();
        if ( $image )
        $escort->main_image = $image->filename;
        $return[$i]['id'] = $escort->id;
        $return[$i]['name'] = $escort->escort_name;
        $i++;
      }
      return $return;
    }
    public function getProfileSelections(){
      $arr = array('Edit','View');
      $return = array();
      $i = 0;
      foreach( $arr as $val ){
          $return[$i]['id'] = $val;
          $return[$i]['name'] = $val;
          $i++;
      }
      return $return;
    }
    public function getEscortsWithReviewsAjax()
    {
        $escorts = $this->hasMany('Escort')->get();
        foreach ( $escorts as $key => $escort ) {
            $reviews = EscortReview::where( 'escort_id','=',$escort->id)->get();
              if ( !$reviews->isEmpty() ) {
                 $set = 0;
                 foreach( $reviews as $review ){
                     if ( $review->status == 2){
                         $set = 1;
                      }

                 }
              if ( $set == 0 ){
                    unset( $escorts[$key] );
              }
              }
              else {
                unset( $escorts[$key] );
              }

      }
      $return = array();
      $i =0;
      foreach ( $escorts as $escort ){
            $return[$i]['name'] = $escort->escort_name;
            $return[$i]['id'] = $escort->id;
            $i++;
      }
      return $return;
    }
    public function getSeoUrl()
    {
        $url = '';

        $defaultLocation = Suburb::find($this->suburb_id);
        $url .= isset($defaultLocation->name) && $defaultLocation->name != '' ? $defaultLocation->name.'-' : '';
        $url .= isset($defaultLocation->state->short_name) && $defaultLocation->state->short_name != '' ? $defaultLocation->state->short_name.'-' : '';
        switch ($this->type) {
             case 2:
                $type ='agency';
             break;
             case 3:
                $type ='massage-parlour';
             default:
                $type ='brothel';
        }
        $url .= isset($this->name) && $this->name != '' ? $this->name.'-' : '';
        $url .= $type . '-';
        $url .= $this->id;

        $url = str_replace(' ', '-', $url);
        $url = str_replace('.', '', $url);
        $url = trim($url);
        $url = strtolower($url);

        return $url;
    }
    public function getEscortsWithReviews()
    {
        $escorts = $this->hasMany('Escort')->get();
        foreach ( $escorts as $key => $escort ) {
            $reviews = EscortReview::where( 'escort_id','=',$escort->id)->get();
              if ( !$reviews->isEmpty() ) {
                 $set = 0;
                 foreach( $reviews as $review ){
                     if ( $review->status == 2){
                         $set = 1;
                         $image = EscortImage::where('id','=',$escort->main_image_id )->first();
                         if ( $image )
                             $escort->main_image = $image->filename;
                      }

                 }
              if ( $set == 0 ){
                    unset( $escorts[$key] );
              }
              }
              else {
                unset( $escorts[$key] );
              }

      }
      return $escorts;
    }
        public function hasReviews()
    {
        $escorts = $this->hasMany('Escort')->get();
        $hasReviews = false;
        foreach ( $escorts as $escort ) {
              $reviews = EscortReview::where( 'escort_id','=',$escort->id)->get();
              if ( !$reviews->isEmpty() )
                foreach ($reviews as $review ){
                    if ( $review->status == 2 ) {
                      $hasReviews = true;
                      break;
                    }
                }
        }
        return $hasReviews;
    }
    public function escortReviews()
    {
        $escorts = $this->hasMany('Escort')->get();
        $reviewArray = array();
        foreach ( $escorts as $key => $escort ) {
           $reviews = EscortReview::where( 'escort_id','=',$escort->id)->get();
            if ( !$reviews->isEmpty() ) {
                foreach( $reviews as $review ){
                     $reviewArray[$escort->escort_name] = $review;
                }
            }
         }
         return $reviewArray;
    }
     public function getFirstEscortId()
    {
        $escorts = $this->hasMany('Escort')->get();
        $escortId = 0;
        foreach ( $escorts as $escort ) {
            $escortId = $escort->id;
            break;
      }
      return $escortId;
    }
}
