<div id="basic-info">
    <h2 class="alignc section-content">BASIC INFO</h2>


    {{ Form::open(['id'=>'newBusiness']) }}
        <div class="alert alert-success">Business info saved!</div>

        <fieldset>
            <div class="control-group">
                {{ Form::label('type','Business type*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::select('type',[1=>'Brothel',2=>'Agency',3=>'Massage'],'',array('class'=>'input-xlarge form-control')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('name','Business Name*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('name','',array('class'=>'business-name input-xlarge input03','required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('phone','Phone*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('phone','',array('class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('address_line_1','Address Line 1*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::text('address_line_1','',array('class'=>'input-xlarge input03','required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('address_line_2','Address Line 2',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::text('address_line_2','',array('class'=>'input-xlarge input03')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('email','Email*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('email','',array('class'=>'input-xlarge input03','required','data-parsley-type'=>'email')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('suburb_id','Location*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::select('suburb_id',$suburbs,'',array('class'=>'select2 form-control input-large input03','data-parsley-required','min'=>'1','data-parsley-min-message'=>'Please select a suburb.','style'=>'width:100%')) }}
                </div>
            </div>
        </fieldset>

        <div class="alignc">
            {{ Form::hidden('new_business',1,array('class'=>'new-business')) }}
            {{ Form::hidden('next_icon','business-logo',array('class'=>'next-icon')) }}
            {{ Form::hidden('next_route','businessLogo',array('class'=>'next-route')) }}
            {{ Form::hidden('next_type','business',array('class'=>'next-type')) }}
            {{ Form::submit('SAVE',array('class'=>'submit-button')) }}
            {{ Form::submit('SAVE AND CONTINUE',array('class'=>'continue-submit submit-button')) }}
        </div>
    {{ Form::close() }}
</div>

<script>
    (function($){
        $('.second-level h2').text('New business');
    })(jQuery);
</script>