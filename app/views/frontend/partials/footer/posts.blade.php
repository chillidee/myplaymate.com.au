<ul class="col-md-3 col-sm-6 footer-widget-container">
    <li class="widget pi_recent_posts">
        <span class="h5">latest posts</span>
        <ul id="footer-posts">
          <?php $i = 0;
                $newPostArray = array();
                foreach ( $posts as $post ) {
                      $newPostArray[strtotime($post['date_from'])] = $post;   
                  }
                      rsort (  $newPostArray );?>
                  @foreach($newPostArray as $key => $post )
              @if ( $i != 0 ) 
            <li>
                @if(!empty($post->thumbnail))
                    <div class="post-media">
                        <a href="/blog/{{ $post->seo_url }}">
                            <img src="{{ Config::get('constants.CDN') .'/posts/thumbnails/thumb_170x170_'.$post->thumbnail }}" alt="{{ $post->title }}"/>
                        </a>
                    </div>
                @endif

                <div class="post-info">
                    <div class="date">
                        <span class="day">{{ $post->getDayAttribute() }}</span><br />
                        <span class="month">{{ $post->month }}</span>
                    </div>

                    <div class="post">
                        <h5>
                            <a href="/blog/{{ $post->seo_url }}">{{ $post->title }}</a>
                        </h5>
                        @if(!empty($post->user->full_name))
                          <span class="authot">By {{ $post->user->full_name }}</span>
                        @endif
                    </div>
                </div>
            </li>
              @endif
            <?php $i++;?>
            @endforeach
        </ul>
    </li><!-- .widget.pi_recent_posts end -->
</ul><!-- .col-md-3.footer-widget-container -->