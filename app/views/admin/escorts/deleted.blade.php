@extends('admin.layout')
@section('content')

    <div class="pagetitle">
        <h1>Deleted Escorts</h1>
    </div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Suburb Name</th>
      <th>Deleted by</th>
      <th>Deleted date</th>
    </tr>
  </thead>
  <tbody>
  @foreach( $deletedEscorts as $escort )
 <?php  
 $admin = User::where('id','=',$escort->deleted_by )->first();
 $user = 'Unknown';
 if ( $admin )
     $user = $admin->full_name;
 ?>
  <tr> 
      <td>{{$escort->id}}</td>
      <td>{{$escort->escort_name}}</td>
      <td>{{$escort->suburb_name}}</td>
      <td>{{$user}}</td>
      <td>{{$escort->created_at }}</td>
  </tr>
  @endforeach
  </tbody>
    <tfoot>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Suburb Name</th>
      <th>Deleted by</th>
      <th>Deleted date</th>
    </tr>
  </tfoot>
</table>

  @stop
  @section('javascript')
  <script>
$(document).ready(function() {
    $('.admin-table').DataTable();
} );
</script>
@stop




