@extends('admin.layout')
@section('javascript')
  <script>
    function valueIfChecked(input)
    {
      if(input.is(':checked'))
        return input.val();

      return '';
     }
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      var checked = [];
      var status = [];
      states = $( '.state' );
      statuses = $( '.status' );
      $.each(statuses, function( index, value ) {
        if ( $( this ).is(":checked") ) {
           status[index] = $( this ).val();
        }
      });
      $.each(states, function( index, value ) {
        if ( $( this ).is(":checked") ) {
           checked[index] = $( this ).val();
        }
      });
      var this_status = data[2];
      if ( checked.length > 0 ) {
          var state = data[3];
          for (i = 0; i < checked.length; i++) {
             if ( state == checked[i] ){
                  if ( status.length > 0 ) {
                      if ( status.indexOf(this_status) > -1 ) 
                             return true;  
                      return false;  
                   }
                  return true;
              }
          }
       }
       else {
            if ( status.length > 0 ) {
                if ( status.indexOf(this_status) > -1 )
                       return true;
                return false;
             }
             return true;
      }
    }
);
 jQuery(document).ready(function($) {
  $( '#myform').on('submit',function(e) {
      e.preventDefault();
      data = $( this ).serialize();
      id = $( '#planUserId').val();
      $.post( "/admin/changePlan", data, function( response ) {
           console.log( response );
           $( '#userPlan_' + id ).html( response.user_id );
           $( '#popupForm').hide();
           $('#overlay').hide();
      })
  })
  $( 'body').on('click','.popupBusinessPlans',function() {
      $( '#popupForm').show();
      $('#overlay').show();
      currentPlan = $( this ).parent('li').data('currentplan');
      $( 'input[value=' + currentPlan  + ']').prop('checked', true);
      $( '#planUserId').attr( 'value', $( this ).parent('li').data('id') ).show();
})
$( '#overlay').on('click',function() {
        $( '#overlay').hide();
        $( '#popupForm').hide();
    })
    $.ajax({
       url: "/admin/getStatusCount"
       }).done(function(result) {
             window.statusCount = jQuery.parseJSON(result);
             checked = [];
             states = $( '.state' );
      $.each(states, function( index, value ) {
         if ( $( this ).is(":checked") ) {
            checked[index] = $( this ).val();
         }
      });
       if(typeof checked !== 'undefined' && checked.keys({}).length){
           var approved = 0,
               disapproved = 0,
               pending = 0,
               unavailable = 0;
           $.each(checked, function( index, value ) {
                approved = approved + window.statusCount[2][value];
                pending = pending + window.statusCount[1][value];
                disapproved = disapproved + window.statusCount[3][value];
                unavailable = unavailable + window.statusCount[4][value];
           });
            $( '#sc_approved' ).html( approved );
            $( '#sc_disapproved' ).html( disapproved );
            $( '#sc_pending' ).html( pending );
            $( '#sc_unavailable' ).html( unavailable );    
       }
        else { 
            $( '#sc_approved' ).html( window.statusCount[2]['all'] );
            $( '#sc_disapproved' ).html( window.statusCount[3]['all'] );
            $( '#sc_pending' ).html( window.statusCount[1]['all'] );
            $( '#sc_unavailable' ).html( window.statusCount[4]['all'] );
        }
       });
   function getStatusCount() {
      var checked = [],
      states = $( '.state' ),
      i = 0;
      $.each(states, function( index, value ) {
         if ( $( this ).is(":checked") ) {
            checked[i] = $( this ).val();
            i++;
         }
      });
     console.log( checked.length );
      if(typeof checked !== 'undefined' && checked.length){
           var approved = 0,
               disapproved = 0,
               pending = 0,
               unavailable = 0;
           $.each(checked, function( index, value ) {
                console.log( checked );
                approved = approved + window.statusCount[2][value];
                pending = pending + window.statusCount[1][value];
                disapproved = disapproved + window.statusCount[3][value];
                unavailable = unavailable + window.statusCount[4][value];
           });
            $( '#sc_approved' ).html( approved );
            $( '#sc_disapproved' ).html( disapproved );
            $( '#sc_pending' ).html( pending );
            $( '#sc_unavailable' ).html( unavailable );    
       }
        else { 
            $( '#sc_approved' ).html( window.statusCount[2]['all'] );
            $( '#sc_disapproved' ).html( window.statusCount[3]['all'] );
            $( '#sc_pending' ).html( window.statusCount[1]['all'] );
            $( '#sc_unavailable' ).html( window.statusCount[4]['all'] );
        }
   }
   moment.lang('en-AU');
   $.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('span', td).html() * 1;
    } );
}
  $.fn.dataTable.ext.order['dom-text-numeric2'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('span', td).html() * 1;
    } );
}
    $.fn.dataTable.ext.order['dom-text-numeric3'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('span', td).html() * 1;
    } );
 }
      $.fn.dataTable.moment( 'DD/M/YYYY' );
      $( '#_dt_search_button' ).on( 'click', function() { 
        oTable.search( $( '#_dt_search' ).val(), true, false).draw();
      });
      $("#_dt_search").keyup(function (e) {
          if (e.keyCode == 13) {
         oTable.search( $( '#_dt_search' ).val(), true, false).draw();
      }
      });
        oTable = jQuery('#datatable').DataTable({
           processing: true,
           searching: true,
           bServerSide : false,
           paging: false,
           ordering: true,
           ajax: {
           url: "/admin/ajax_escorts",
           data: function (d) {
                    d.date_from = $('#date-from').val();
                    d.date_to = $('#date-to').val();
                }
            },
          "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            { "orderDataType": "dom-text-numeric" },
            { "orderDataType": "dom-text-numeric2" },
            { "orderDataType": "dom-text-numeric3" },
            null,
            null,
            null,
            null
        ],
        initComplete: function (response) {
              $( '.row' ).on( 'click','.bumpdown',function(e) {
                         e.preventDefault();
                          $( '.loadingDiv').show();
                         $.ajax({
                           url: "/admin/bump-down",
                          data: {
                            'id' : $( this ).data('escort-id')
                         }
               }).done(function(result) {
                  alert( 'Escort has been bumped down.' );
                   $( '.loadingDiv').hide();
                  });
      });
           $( '.row' ).on( 'click','.makeUnavailable',function(e) {
                         e.preventDefault();
                         $( '.loadingDiv').show();
                         $.ajax({
                           url: "/admin/makeUnavailable",
                          data: {
                            'id' : $( this ).data('escort-id')
                         }
               }).done(function(result) {
                  alert( 'Escort has been made unavailable.' );
                  $( '.loadingDiv').hide();
                  });
      });
                  $('.state, .date-input').on('change', function() {
                    $('p.error').remove();
                    from = $("#date-from").val().split("/");
                    to = $("#date-to").val().split("/");
                    from = new Date(from[2], from[1] - 1, from[0]);
                    to = new Date(to[2], to[1] - 1, to[0]);
                    if ($('#date-from').val() != '' && $('#date-to').val() != '' && from > to)
                    {
                      $('.escort-table-filters').append('<p class="error">Start date cannot be later than End date</p>');
                      return;
        }           getStatusCount();
                    oTable.draw();
                      
                  });
          }
        });
      $('#date-from').datepicker({
        minDate: new Date(2015, 06 - 1, 1),
        defaultDate: -7,
        dateFormat: "dd/mm/yy",
        onSelect: function(dateText, inst) {
              $( '.ev' ).html( '0');
              $.ajax({
                  url: "/admin/ajaxEventsByDate",
                  data: {
                      'date_from' : $( '#date-from' ).val(),
                      'date_to'   : $( '#date-to' ).val()
                  }
               }).done(function(result) {
                  var obj = jQuery.parseJSON( result );
                  $.each(obj, function( index, value ) {
                       $( '#_ev_views_' + value.id ).html( value.visits );
                       $( '#_ev_phone_' + value.id ).html( value.phone);
                       $( '#_ev_playmail_' + value.id ).html( value.playmail );
                  });
               });
            }
      });  
   $('#date-to').datepicker({
        dateFormat: "dd/mm/yy",
        maxDate: "0",
         onSelect: function(dateText, inst) {
              $( '.ev' ).html( '0');
              $.ajax({
                  url: "/admin/ajaxEventsByDate",
                  data: {
                      'date_from' : $( '#date-from' ).val(),
                      'date_to'   : $( '#date-to' ).val()
                  }
               }).done(function(result) {
                  var obj = jQuery.parseJSON( result );
                  $.each(obj, function( index, value ) {
                       $( '#_ev_views_' + value.id ).html( value.visits );
                       $( '#_ev_phone_' + value.id ).html( value.phone);
                       $( '#_ev_playmail_' + value.id ).html( value.playmail );
                  });
               });
            }
      }); 
   $('.state').change( function() {
        oTable.draw();
    } );
   $('.status').change( function() {
        oTable.draw();
    } );

        $('.rightpanel').on('click', '.approve', approveEscort);

        $('.rightpanel').on('click', '.disapprove', disapproveEscort);

        $('.rightpanel').on('click', '.delete', deleteEscort);

        $('.rightpanel').on('click', '.bumpup', bumpup);
      
    });
  </script>
@stop

@section('content')
<div class ='overlay loadingDiv'></div>
<div class ='loader loadingDiv'></div>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>All escorts</h1>
    </div>
</div><!--pageheader-->

<div class="row">
  <a href="/admin/escorts/new" class="create-user btn btn-success btn-lg pull-right section-content">Create escort</a>
</div>

<style>
     .overlay { position:fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: #000; opacity: .3; }
     .loader {
  font-size: 10px;
  margin: 50px auto;
  z-index:1001;
  text-indent: -9999em;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background: #ffffff;
  background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  position: relative;
  -webkit-animation: load3 1.4s infinite linear;
  animation: load3 1.4s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
   display:none;
  position:fixed;
  left:50%;
  margin-left: 50px;
}
.loader:before {
  width: 50%;
  height: 50%;
  background: #ffffff;
  border-radius: 100% 0 0 0;
  position: absolute;
  top: 0;
  left: 0;
  content: '';
}
.loader:after {
  width: 75%;
  height: 75%;
  border-radius: 50%;
  content: '';
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
@-webkit-keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
#popupForm{
    position: fixed;
    background: #fff;
    width: 600px;
    z-index: 10000;
    border-radius: 5px;
    border: 1px solid #ccc;
    padding: 50px;
    left: 50%;
    margin-left: -300px;
    top: 20px;
    display:none;
  }
#popupForm select {
    height: 32px;
    background: #fff;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 3px 12px;
}
#popupForm textarea {
    width: 100%;
    height: 158px;
    border: 1px soid #ccc;
    border: 1px solid #ccc;
    border-radius: 3px;
}
#popupForm input {
    width: 32px;
    border-radius: 3px;
    border: 1px solid #ccc;
    height: 52px;
    padding: 0 19px;
  }
#popupForm label {
    font-size: 1.2em;
    font-weight: bold;
    margin: .3em 0;
    color: #384F5D;
    text-transform: none;
    display: block;
    margin: -36px 0 20px 40px;
    line-height: 12px;
    vertical-align: middle;
  }
#popupForm h3{
    color: #AD4949;
    text-transform: uppercase;
    margin-bottom: 20px;
  }
.mpBut, .mpButt {
    float: right;
    margin-top: 35px;
    background-color: #3CAF40;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 8px 48px;
    font-size: 22px;
    margin-bottom: 35px;
  }
.mpButt {
  margin: 0;
  font-size: 18px;
  padding: 2px 18px;
  background-color: #317177;
}
#overlay {
   opacity: .7;
   position:fixed;
   left:0;
   right:0;
   bottom:0;
   top:0;
   width: 100%;
   height: 100%;
   background-color: #000;
   z-index: 9999;
   display:none;
 }
  .status_count {
    font-size: 20px;
    position: absolute;
    margin-left: -100px;
    margin-top: 35px;
    width: 80px;
    /* border-top: 1px solid #481313; */
    padding-top: 6px;
    color: #C10F0F;
    text-align: center;
  }
input[type='checkbox']:checked {
    background: #86D628;
}
input[type='checkbox'] {
    -webkit-appearance: none;
    width: 20px;
    height: 20px;
    background: white;
    border-radius: 5px;
    border: 1px solid #333131;
    margin-top: 5px;
    margin-bottom: -3px;
}
  .datepicker {
    border: 1px solid #5A5454;
    border-radius: 3px;
    padding-left: 10px;
    height: 30px;
}
  .ui-datepicker-calendar td {
    width:30px;
    height: 28px;
  }
  .ui-state-active {
    background-color: green;
  }
  ._dt_search_title {
    clear:right;
  }
  #_dt_search, ._dt_search_title {
    float:right;
    margin-top: 14px;
    margin-bottom: 25px;
  }
  #_dt_search_button {
        float: right;
    margin: 0;
    z-index: 1000;
    position: relative;
    right: 0;
    margin-right: 0;
    background-color: #0866C6;
    color: #fff;
    font-size: 24px;
    border: none;
    padding: 6px 13px;
    border-radius: 3px;
  }
  #_dt_search {
    display: block;
    font-size: 18px;
    margin-right: 0;
    height: 32px;
    padding-left: 10px;
  }
  #datatable_filter {
    display:none;
  }
  #state_col {
    width: 35px;
  }
  #datatable th:nth-child(5) {
    min-width: 80px;
  }
  #_status_filter_div label {
        font-size: 1.2em;
    text-transform: none;
    font-weight: bold;
    margin: .3em 0;
    font-weight: 400;
    margin-right: 15px;
  }
  #_status_filter_div {
    border-top: 1px solid #E4E5E6;
    margin-top: 35px;
    width: 67%;
  }
  .info {
    color: #0E597D;
    font-size: 22px;
    margin-right: 20px;
  }
  .label-unavailable {
        background-color: #D74EF0;
  }
  </style>
  <div id ='overlay'></div>
<div id ='popupForm'>
<h3 id ='descriptor'>Change plan</h3>
    <form id ='myform'>
        {{ Form::open(array('url' => 'foo/bar')) }}

        {{ Form::text('user_id','',array('class'=>'hidden','id'=>'planUserId'))}}

        @foreach( $plans as $plan )

        {{ Form::radio('business_plan',$plan->id)}}    
        {{ Form::label('business_plan',$plan->name)}} </br>

        @endforeach

        <button id ='submit_newplan' class ='mpBut'>Submit</button>

        {{Form::close()}}
</div>
<div class="row">
  <div class="escort-table-filters">
    <label for=""><input type="checkbox" class="state" id="nsw" value="NSW"> NSW</label>
    <label for=""><input type="checkbox" class="state" id="qld" value="QLD"> QLD</label>
    <label for=""><input type="checkbox" class="state" id="vic" value="VIC"> VIC</label>
    <label for=""><input type="checkbox" class="state" id="nt" value="NT"> NT</label>
    <label for=""><input type="checkbox" class="state" id="sa" value="SA"> SA</label>
    <label for=""><input type="checkbox" class="state" id="wa" value="WA"> WA</label>
    <label for=""><input type="checkbox" class="state" id="tas" value="TAS"> TAS</label>
    <input type="text" class="date-input datepicker" id="date-from" placeholder="Date from">
    <input type="text" class="date-input datepicker" id="date-to" placeholder="Date to">
  </div>
</div>
<div id ='_status_filter_div'>
  <span class ='info'>Filter by status:</span>
  <label for=""><input type="checkbox" class="status" id="Approved" value="Approved" checked> Approved</label><span class ='status_count' id ='sc_approved'></span>
       <label for=""><input type="checkbox" class="status" id="Disapproved" value="Disapproved"> Disapproved</label><span class ='status_count' id ='sc_disapproved'></span>
       <label for=""><input type="checkbox" class="status" id="Pending" value="Pending"> Pending</label><span class ='status_count' id ='sc_pending'></span>
       <label for=""><input type="checkbox" class="status" id="Unavailable" value="Unavailable"> Unavailable</label><span class ='status_count' id ='sc_unavailable'></span>
</div>
<span class ="_dt_search_title"><button class ='mpm_table_button' id ='_dt_search_button'>Search</button></span><input id ="_dt_search" type ="text" placeholder ="Search">
<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Escort name</th>
      <th>Status</th>
      <th style ='width:25px!important'>State</th>
      <th style ='width:25px!important'>Phone</th>
      <th style ='width:25px!important'>Email</th>
      <th style ='width:25px!important'>Views</th>
      <th>Phone count</th>
      <th>Playmail count</th>
      <th>Date of creation</th>
      <th>Last bump up</th>
      <th>Plan</th>
      <th>Action</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tfoot>
</table>

@stop


