<?php

class HeightsTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('heights')->truncate();

        $Heights = [
            ['name' => '150 – 159 cm (5 – 5.2 ft)'],
            ['name' => '160 – 169 cm (5.3 – 5.6 ft)'],
            ['name' => '170 – 179 cm (5.7 – 5.10 ft)'],
            ['name' => '180 – 189 cm (5.11 – 6.2 ft)'],
            ['name' => '190 cm + (6.3 ft +)'],
            ];

        // Uncomment the below to run the seeder
        DB::table('heights')->insert($Heights);
    }
}
