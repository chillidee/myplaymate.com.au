@extends('frontend.layout')

@section('content')

<!-- #page-title start -->
<section id="page-title" class="page-title-1">
    <div class="container">
        <h1>MANAGE USER PROFILE</h1>
        <div class="breadcrumb-container">
            <span>You are here: </span>
            <ul class="breadcrumb">
                <li><a href="{{ url('') }}">HOME</a></li>
                <li><a href="#">MANAGE USER PROFILE</a></li>
            </ul><!-- .breadcrumb end -->
        </div><!-- .breadcrumb-container end -->
    </div><!-- .containr end -->
</section><!-- #page-title end -->

<div class="back-end">
	<div class="page-wrapper top-space">
		<div class="container">
			<div class="row">

				@include('frontend.partials.profile.profile-left-sidebar')

				<div class="col-md-9" id="backend-main">
	                <!-- Process Bar -->
	                <div id="process-bar" class="alignc">
	                	<h1>PROFILE</h1>
	                     <div id="process-items">
	                     	<div class="row no-bottom">
	                     		<div class="col-md-8 col-sm-12">
	                        		<ul class="row first-level">
	                        			<li class="col-md-3 col-sm-4 col-xs-4 user-icon">
		                                	<div class="icon icon-basic active">
		                                    	<a href="{{ url('manage/escort/profile/welcome') }}"></a>
		                                	</div>
		                                	<h6>User Info</h6>
	                        			</li>
	                        			@if(Auth::user()->escort())
	                        				<li class="col-md-3 col-sm-4 col-xs-4 escort-icon" data-escort-id="{{ Auth::user()->escort()->id }}">
			                                	<div class="icon icon-escort">
			                                    	<a href="{{ url('manage/escort/profile/welcome') }}"></a>
			                                	</div>
			                                	<h6>{{ Auth::user()->escort()->escort_name }}</h6>
		                        			</li>
	                        			@endif
	                        			<li class="col-md-3 col-sm-4 col-xs-4 escort-icon escort-icon-template">
		                                	<div class="icon icon-escort">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6></h6>
	                        			</li>
	                        			<li class="col-md-3 col-sm-4 col-xs-4 business-icon-template">
		                                	<div class="icon icon-business">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6></h6>
	                        			</li>
										@if(Auth::user()->businesses)
											@foreach(Auth::user()->businesses as $business)
		                        				<li class="col-md-3 col-sm-4 col-xs-4 business-icon" data-business-id="{{ $business->id }}">
				                                	<div class="icon icon-business">
				                                    	<a href=""></a>
				                                	</div>
				                                	<h6>{{ $business->name }}</h6>
    		                        			</li>
											@endforeach
										@endif
		                        	</ul>
	                     		</div>
	                     		{{-- If the user is not a simple customer --}}
	                     		@if(Auth::user()->type == 2)
		                     		<div class="col-md-4 col-sm-12 new-icons">
		                     			<ul class="row">
		                     				@if(!Auth::user()->escort())
		                     					<li class="col-md-6 col-sm-12 new-escort">
				                                	<div class="icon icon-add-escort">
				                                    	<a href=""></a>
				                                	</div>
				                                	<h6>New escort</h6>
		                     					</li>		
		                     				@endif

		                     				<li class="col-md-6  col-sm-12 new-business">
			                                	<div class="icon icon-add-business">
			                                    	<a href=""></a>
			                                	</div>
			                                	<h6>New business</h6>
		                     				</li>
		                     			</ul>
		                     		</div>                     			
	                     		@endif
	                     	</div>
	
							<div class="row">
								<div class="col-md-12 second-level escort-level">
									<h2></h2>
	                        		<ul class="second-level-icons">
		                            	<li class="escort-profile">
		                                	<div class="icon icon-profile">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6>Basic Info</h6>
		                            	</li>
		                            	<li class="escort-about">
		                                	<div class="icon icon-about">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6>About Me</h6>
		                            	</li>
		                            	<li class="escort-availability">
		                                	<div class="icon icon-availability">
		                                    	<a></a>
		                                	</div>
		                                	<h6>Availability / Rates</h6>
		                            	</li>
		                            	<li class="escort-gallery">
		                                	<div class="icon icon-gallery">
		                                    	<a></a>
		                                	</div>
		                                	<h6>Gallery</h6>
		                            	</li>
		                            	<li class="escort-site">
		                                	<div class="icon icon-site">
		                                    	<a></a>
		                                	</div>
		                                	<h6>Your site</h6>
		                            	</li>
		                            	<li class="escort-reviews">
		                                	<div class="icon icon-reviews">
		                                    	<a></a>
		                                	</div>
		                                	<h6>Updates / Reviews</h6>
		                            	</li>
		                        	</ul>
								</div>

								<div class="col-md-12 second-level business-level">
									<h2></h2>
	                        		<ul>
		                            	<li class="business-info">
		                                	<div class="icon icon-profile">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6>Basic Info</h6>
		                            	</li>
		                            	<li class="business-logo">
		                                	<div class="icon icon-gallery">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6>Logo</h6>
		                            	</li>
		                            	<li class="business-my-escorts">
		                                	<div class="icon icon-profile-manager">
		                                    	<a href=""></a>
		                                	</div>
		                                	<h6>My Escorts</h6>
		                            	</li>
		                        	</ul>
								</div>
							</div>

	                    </div>
	                </div><!-- End of Process Bar -->
					<div class="spinner">
						<img src="/assets/frontend/img/tail-spin.svg"/>
					</div>
					<div id="form">
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

@stop


@section('javascript')

	<script>
		var formContainer   = jQuery('#form'),
			firstLevel  	= jQuery('.first-level'),
			secondLevel 	= jQuery('.second-level'),
			escortLevel 	= jQuery('.escort-level'),
			businessLevel   = jQuery('.business-level'),
			userIcon 		= jQuery('.user-icon'),
			escortIcon  	= jQuery('.escort-icon').not('.escort-icon-template'),
			businessIcon 	= jQuery('.business-icon'),
			title 			= jQuery('.second-level h2'),
			spinner 		= jQuery('.spinner'),
			animTime   		= 300;

		function refreshForm(route,type,escort_id,business_id){
			wipeForm();
			setTimeout(loadForm(route,type,escort_id,business_id), animTime);
		}

		function wipeForm(){
			formContainer.slideUp(animTime);
			spinner.fadeIn(animTime);

			$('body').animate({scrollTop: 240}, animTime);
		}

		function populateForm(data,type){
			formContainer.empty().html(data);
			spinner.fadeOut(animTime);
			formContainer.slideDown(animTime);
			title.slideDown(animTime);

			if (type == 'escort')
				escortLevel.slideDown(animTime);
			else if (type == 'business')
				businessLevel.slideDown(animTime);
		}

		function attachPlugins(){
			// Attach the plugins where needed
		    formContainer.find('form').parsley();
		    formContainer.find('.select2').select2({
		    	minimumInputLength: 4,
		    });
	    	formContainer.find('.dropzone').dropzone();
		    jQuery('.datepicker').datepicker({dateFormat: 'dd/mm/yy'});
	        jQuery('input.radioImageSelect').radioImageSelect();
		}

		function setCurrentElement(type,escort_id,business_id){
			if (type == 'escort')
				window.escortId = escort_id;
			else if (type == 'business')
				window.businessId = business_id;
		}

		function loadForm(route,type,escort_id,business_id) {
			jQuery.ajax({
				url: '/profile/'+route,
				data: {
					escort_id:   escort_id,
					business_id: business_id
				},
			})
			.done(function(data) {
				populateForm(data,type);
				attachPlugins();

				// If you create a new escort from the business "my escorts" section, pass the business id
				if(type == 'escort' && business_id != '')
					$('input.business-id').val(business_id);
			});
		}

		function changeFormSection(clicked,route,type) {
			var escort_id = window.escortId,
				business_id = window.businessId;

			setCurrentElement(type,escort_id,business_id);
			activateIcon(clicked);
			refreshForm(route,type,escort_id,business_id);
		}

		function activateIcon(clicked,level) {
			if (level == 1)
				firstLevel.find('.icon').removeClass('active');
			else
				secondLevel.find('.icon').removeClass('active');
			
			clicked.find('.icon').addClass('active');
		}

		function goToEscortInfo() {
			changeFormSection($(this),'escortInfo','escort');
		}
		function goToEscortAbout() {
			changeFormSection($(this),'escortAbout','escort');
		}
		function goToEscortAvailability() {
			changeFormSection($(this),'escortAvailability','escort');
		}
		function goToEscortGallery() {
			changeFormSection($(this),'escortGallery','escort');
		}
		function goToEscortSite() {
			changeFormSection($(this),'escortSite','escort');
		}
		function goToEscortReviews() {
			changeFormSection($(this),'escortReviews','escort');
		}
		function deactivateEscortButtons(){
			jQuery('.escort-profile, .escort-about, .escort-availability, .escort-gallery, .escort-site, .escort-reviews').css({
				'opacity': '.5',
				'cursor':'default'
			});
			jQuery('.escort-profile').off('click', goToEscortInfo);
			jQuery('.escort-about').off('click', goToEscortAbout);
			jQuery('.escort-availability').off('click', goToEscortAvailability);
			jQuery('.escort-gallery').off('click', goToEscortGallery);
			jQuery('.escort-site').off('click', goToEscortSite);
			jQuery('.escort-reviews').off('click', goToEscortReviews);
		}
		function activateEscortButtons(){
			jQuery('.escort-profile, .escort-about, .escort-availability, .escort-gallery, .escort-site, .escort-reviews').css({
				'opacity': '1',
				'cursor':'pointer'
			});
			jQuery('.escort-profile').on('click', goToEscortInfo);
			jQuery('.escort-about').on('click', goToEscortAbout);
			jQuery('.escort-availability').on('click', goToEscortAvailability);
			jQuery('.escort-gallery').on('click', goToEscortGallery);
			jQuery('.escort-site').on('click', goToEscortSite);
			jQuery('.escort-reviews').on('click', goToEscortReviews);
		}
		function deactivateBusinessButtons(){
			jQuery('.business-info, .business-logo, .business-my-escorts').css({
				'opacity': '.5',
				'cursor':'default'
			});
			jQuery('.business-info').off();
			jQuery('.business-logo').off();
			jQuery('.business-my-escorts').off();
		}
		function activateBusinessButtons(){
			jQuery('.business-info, .business-logo, .business-my-escorts').css({
				'opacity': '1',
				'cursor':'pointer'
			});
			jQuery('.business-info').on('click', goToBusinessInfo);
			jQuery('.business-logo').on('click', goToBusinessLogo);
			jQuery('.business-my-escorts').on('click', goToBusinessMyEscorts);
		}
		function goToBusinessInfo() {
			changeFormSection(jQuery(this),'businessInfo','business');
		}
		function goToBusinessLogo() {
			changeFormSection(jQuery(this),'businessLogo','business');
		}
		function goToBusinessMyEscorts() {
			changeFormSection(jQuery(this),'businessMyEscorts','business');
		}

		(function($){
			refreshForm('userInfo');

			// If there are an escort or businesses associated with the user, show the icons
			escortIcon.fadeIn();
			businessIcon.fadeIn();

			userIcon.on('click', function(e) {
				e.preventDefault();

				activateIcon($(this),1);
				secondLevel.slideUp(animTime);

				refreshForm('userInfo');
			});

			$('.back-end').on('click', '.escort-icon', function(e) {
				e.preventDefault();

				var $this 	    = $(this),
					profileIcon = escortLevel.find('.escort-profile'),
					escort_id   = $this.data('escort-id');

				activateEscortButtons();

				activateIcon(escortIcon,1);
				activateIcon(profileIcon,2);

				secondLevel.slideUp(animTime);
				title.fadeOut(animTime);

				setCurrentElement('escort',escort_id);
				refreshForm('escortInfo','escort',escort_id);
			});

			$('.back-end').on('click','.business-icon', function(e) {
				e.preventDefault();

				var $this 	    = $(this),
					profileIcon = businessLevel.find('.business-info'),
					business_id = $this.data('business-id');

				activateBusinessButtons();

				activateIcon($this,1);
				activateIcon(profileIcon,2);

				secondLevel.slideUp(animTime);
				title.fadeOut(animTime);

				setCurrentElement('business','',business_id);
				refreshForm('businessInfo','business','',business_id);
			});

			$('.back-end').on('click', '.edit-escort', function(e) {
				e.preventDefault();
				activateEscortButtons();

				var escortProfileIcon = escortLevel.find('.icon-profile'),
					escort_id 		  = $(this).data('escort-id');
				
				activateIcon(escortProfileIcon,2);

				businessLevel.slideUp(animTime);

				setCurrentElement('escort',escort_id);
				refreshForm('escortInfo','escort',escort_id);
			});

			$('.back-end').on('click', '.new-escort', function(e) {
	            e.preventDefault();

	            secondLevel.slideUp(animTime);
	            deactivateEscortButtons();
	            activateIcon($('.escort-info'),2);
	            refreshForm('newEscort','escort');
			});	

			$('.back-end').on('click', '.new-business', function(e) {
	            e.preventDefault();

	            secondLevel.slideUp(animTime);
	            deactivateBusinessButtons();
	            activateIcon($('.business-info'),2);
	            refreshForm('newBusiness','business');
			});

			$('.back-end').on('click', 'input.continue-submit', function(event) {
				$('<input>').attr({
						'type':  'hidden',
						'name':  'continue',
						'class': 'continue'
					})
					.appendTo('#form form');
			});

			$('.back-end').on('submit', 'form', function(event) {
				event.preventDefault();

				var $this = $(this),
					route = $this[0].id,
					escortName = $this.find('input.escort-name').val(),
					businessName = $this.find('input.business-name').val(),
					nextIcon = $('.'+$this.find('input.next-icon').val()),
					nextRoute = $this.find('input.next-route').val(),
					nextType = $this.find('input.next-type').val();

				$.ajax({
					url: '/profile/'+route,
					type: 'POST',
					data: $this.serialize(),
				})
				.done(function(data) {
					activateEscortButtons();
					activateBusinessButtons();

					if ($('input.continue').length > 0) {
						if($('input.new-escort').length > 0){

							window.escortId = parseInt(data);

							if ($('input.business-id').val() == '') {
								var newIcon = $('.escort-icon-template').clone();
								newIcon.removeClass('business-icon-template');
								newIcon.addClass('escort-icon').fadeIn(animTime);
								newIcon.attr('data-escort-id', parseInt(data));
								newIcon.find('h6').text(escortName);
								firstLevel.append(newIcon);
								activateIcon(newIcon,1);
								// var escortIcon = $('.escort-icon-template');
								// escortIcon.find('h6').text(escortName);
								// escortIcon.fadeIn(animTime);						
							}
						}
						if($('input.new-business').length > 0){
							window.businessId = parseInt(data);
							if($('input.new-business').length > 0){
								var newIcon = $('.business-icon-template').clone();
								newIcon.removeClass('business-icon-template');
								newIcon.addClass('business-icon').fadeIn(animTime);
								newIcon.attr('data-business-id', parseInt(data));
								newIcon.find('h6').text(businessName);
								firstLevel.append(newIcon);
								activateIcon(newIcon,1);
							}
						}

						changeFormSection(nextIcon,nextRoute,nextType);
					} else {
						$('body').animate({scrollTop: 240}, animTime);
						$this.find('.alert-success').first().show();
					}

					// Update the name if it's been changed
					if ($('input.escort-name').length > 0 && $('input.business-id').val() != '') {
						firstLevel.find('.escort-icon').find('.active').siblings('h6').text(escortName);
						escortLevel.find('h2').text(escortName);
					}
					if ($('input.business-name').length > 0) {
						firstLevel.find('.active').siblings('h6').text(businessName);
						businessLevel.find('h2').text(businessName);
					}
				});
			});
		})(jQuery);

	</script>	

@stop



















