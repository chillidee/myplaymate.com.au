<?php

class HourlyratesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('hourly_rates')->truncate();

        $Hourlyrates = [
            ['name' => '1 - 100'],
            ['name' => '101 - 200'],
            ['name' => '201 - 300'],
            ['name' => '301 - 400'],
            ['name' => '401 - 500'],
            ['name' => '500+'],
            ];

        // Uncomment the below to run the seeder
        DB::table('hourly_rates')->insert($Hourlyrates);
    }
}
