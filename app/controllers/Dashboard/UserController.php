<?php

namespace Dashboard;

use App, Controller, View, Helpers, Response, Datatables, Input, Business, Auth, Wishlist,User;
  
 class UserController extends \ProfileController
  //class BusinessController extends Controller 

  {
    public function detail($id) {
        $ret = json_encode( $array );
        return 'angular.callbacks._0(' . $ret . ');';
    }
       public function panel($id) {
        $input = Input::get();
        $user = User::where( 'id','=',$id )->first();
         switch ( $input['panel']) {
             case '_MpOverview':

        return '<h4 class="panelHeader accountPanelHeader">Overview</h4>';
        break;
        case '_MpFavourites':
               return '<h4 class="panelHeader accountPanelHeader">Your Favourites</h4>';
        break;
         case '_MpAccount':
              return '<h4 class="panelHeader accountPanelHeader">Manage Your Account</h4>';
        break;
}
    }
   public function screen($id) {
        $input = Input::get();
        $user = User::where( 'id','=',$id)->first();
         switch ( $input['panel']) {
            case '_MpOverview':
             return View::make('dashboard.user.screens.overview', compact('user'));
        break;
      case '_MpFavourites':
             $faves = $user->getFaves();
             return View::make('dashboard.user.screens.favourites', compact('user','faves'));
        break;
        case '_MpAccount':
              $user = Auth::user();
              return View::make('dashboard.escort.screens.account', compact( 'user' ));
        break;
}
    }
}

