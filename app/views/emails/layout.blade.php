<!-- Wrapper -->
<table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td style="padding-top: 20px;" bgcolor="#ffffff" valign="top" width="100%"><!-- Start Header-->
        <table class="deviceWidth" style="width: 580px;" align="center" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td bgcolor="#ffffff" width="100%"><!-- Nav -->
                <table class="deviceWidth" align="right" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td class="center" style="font-size: 13px; color: #272727; font-weight: light; text-align: right; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 5px 0px; font-style: italic;"><a href="https://twitter.com/MyPlayMateAU"><img title="Twitter" src="https://myplaymate.com.au/assets/emails/twitter.jpg" alt="Twitter" height="35" border="0" width="35" /></a><a href="https://www.facebook.com/MyPlayMateAU"><img title="Facebook" src="https://myplaymate.com.au/assets/emails/facebook.jpg" alt="Facebook" height="35" border="0" width="35" /></a> <a href="https://www.youtube.com/user/MyPlayMateAU?feature=watch"><img title="You Tube" src="https://myplaymate.com.au/assets/emails/youtube.jpg" alt="You Tube" height="35" border="0" width="35" /></a></td>
                    </tr>
                  </tbody>
                </table>
                <!-- End Nav --></td>
              </tr>
            </tbody>
          </table>
          <!-- End Header -->


          @yield('content')


          <div style="height: 15px;">&nbsp;</div>
          <!-- spacer --> <!-- 4 Columns -->
          <table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td style="padding: 30px 0;" bgcolor="#363636">
                  <table class="deviceWidth" style="width: 580px;" align="center" bgcolor="#363636" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>
                          <table width="45%" cellpadding="0" cellspacing="0" border="0" align="left" class="deviceWidth" bgcolor="#363636">
                            <tbody>
                              <tr>
                                <td valign="top" style="font-size: 11px; color: #999; font-family: Arial, sans-serif; padding-bottom: 20px;" class="center">
                                  You are receiving this email because you are registered with <a href="https://myplaymate.com.au">myplaymate.com.au</a>.<br />
                                  If you would like to unsubscribe, or you were sent this email by mistake contact us at <a href="mailto:admin@myplaymate.com.au">admin@myplaymate.com.au</a> and we'll fix it right away.
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table class="deviceWidth" style="width: 40%;" align="right" bgcolor="#363636" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                               <td class="center" style="font-size: 11px; color: #f1f1f1; font-weight: normal; font-family: Arial, Helvetica, sans-serif; line-height: 26px; vertical-align: top; text-align: right;" valign="top">
                                <a href="https://twitter.com/MyPlayMateAU">
                                  <img title="Twitter" src="https://myplaymate.com.au/assets/emails/twitter_grey.jpg" alt="Twitter" height="35" border="0" width="35" />
                                </a>
                                &nbsp; 
                                <a href="https://www.facebook.com/MyPlayMateAU">
                                  <img title="Facebook" src="https://myplaymate.com.au/assets/emails/facebook_grey.jpg" alt="Facebook" height="35" border="0" width="35" />
                                </a>
                                &nbsp; 
                                <a href="https://www.youtube.com/user/MyPlayMateAU?feature=watch">
                                  <img title="You Tube" src="https://myplaymate.com.au/assets/emails/youtube_grey.jpg" alt="You Tube" height="35" border="0" width="35" />
                                </a>
                                
                                <br />
                                
                                <a style="text-decoration: none; color: #848484; font-weight: normal;" href="https://myplaymate.com.au/web-enquiry/">
                                  contact us
                                </a>
                                <br />
                                
                                <a style="text-decoration: none; color: #848484; font-weight: normal;" href="https://myplaymate.com.au/">
                                  website
                                </a>
                                <br />
                                
                                <a style="text-decoration: none; color: #848484; font-weight: normal;" href="https://myplaymate.com.au/privacy-policy">
                                  privacy policy
                                </a>
                              </td>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- End 4 Columns --></td>
          </tr>
        </tbody>
      </table>
      <!-- End Wrapper -->
      <p></p>