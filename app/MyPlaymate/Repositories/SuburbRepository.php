<?php
namespace MyPlaymate\Repositories;

use Suburb;

class SuburbRepository {

  public function find($id)
  {  
    $suburb =  Suburb::find($id);
    if ( is_object( $suburb )) {
       $suburb->toArray();
       return $suburb;
    }
  }

  public function populateSelect()
  {
    $suburb_obj = Suburb::get(['postcode','name','id']);
    $suburbs = array('Choose a suburb...');
    foreach ($suburb_obj as $key => $suburb)
    {
      $suburbs[$suburb->id] = $suburb->name.' - '.$suburb->postcode;
    }

    return $suburbs;
  }
}