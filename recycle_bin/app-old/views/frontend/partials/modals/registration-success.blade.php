<div class="modal fade backdrop auth-modal" id="registration-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<img src="/assets/frontend/img/logoblack.jpg" alt="MyPlaymate. The place to be.">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <p>REGISTRATION SUCCESSFUL!</p>
            </div>
            <div class="modal-body">
				<p>Please check your email to activate your account.</p>
            </div>
        </div>
	</div>
</div>



