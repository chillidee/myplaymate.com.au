<li class="widget widget_categories" id="used-filters-container" ng-hide="isEmptyFilters">
    <h5>Selected Filters</h5>

    <div>
        <nav>
            <div style="width:100%;height:auto;">
                <ul id="used-filters">
                    <li class="used-filter-template">
                        <a class="filter-name"></a>
                        <ul>
                            <li class="filter-value">
                                <a><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </li>
                </ul>
                    <div>
                        <button type="button" class="btn btn-default filterSubmit filterSubmitDesktop resetFilter" id="remove-filters">Remove filters</button>
                    </div>

            </div>
        </nav>
    </div>
    <div class="clear"></div>
</li><!-- .widget_filters end -->

<li class="widget widget_categories">
    <h5>Filters</h5>
    <div>
        <nav>
            <div style="width:100%;height:auto;">
                <ul id="escort-filters">
                    <li class="filter-name">
                        <a id="age" href="">Age</a>
                        <ul>
                            <li><a id="1825" href="">18-25</a></li>
                            <li><a id="2630" href="">26-30</a></li>
                            <li><a id="3140" href="">31-40</a></li>
                            <li><a id="4150" href="">41-50</a></li>
                            <li><a id="5160" href="">51-60</a></li>
                            <li><a id="6170" href="">61-70+</a></li>
                        </ul>
                    </li>
                    <li class="filter-name">
                        <a id="gender" href="">Gender</a>
                        <ul data-table="genders"><li><a id="female">Female</a></li><li><a id="male">Male</a></li><li><a id="transexual">Transexual</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="ethnicity" href="">Ethnicity</a>
                        <ul data-table="ethnicities"><li><a id="asian">Asian</a></li><li><a id="black">Black</a></li><li><a id="caucasian">Caucasian</a></li><li><a id="european">European</a></li><li><a id="latino">Latino</a></li><li><a id="middle-eastern">Middle Eastern</a></li><li><a id="other">Other</a></li><li><a id="indian">Indian</a></li><li><a id="eurasian">Eurasian</a></li><li><a id="mixed">Mixed</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="body" href="">Body type</a>
                        <ul data-table="bodies"><li><a id="female-petite">Female Petite</a></li><li><a id="female-slim">Female Slim</a></li><li><a id="female-busty">Female Busty</a></li><li><a id="female-athletic">Female Athletic</a></li><li><a id="female-curvy">Female Curvy</a></li><li><a id="female-heavy">Female Heavy</a></li><li><a id="male-slim">Male Slim</a></li><li><a id="male-athletic">Male Athletic</a></li><li><a id="male-average">Male Average</a></li><li><a id="male-muscular">Male Muscular</a></li><li><a id="male-heavy">Male Heavy</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="hourly-rates" href="">Hourly rates</a>
                        <ul data-table="hourly_rates"><li style="display: list-item;"><a id="1-100">1 - 100</a></li><li style="display: list-item;"><a id="101-200">101 - 200</a></li><li style="display: list-item;"><a id="201-300">201 - 300</a></li><li style="display: list-item;"><a id="301-400">301 - 400</a></li><li style="display: list-item;"><a id="401-500">401 - 500</a></li><li style="display: list-item;"><a id="500">500+</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="services" href="">Services</a>
                        <ul data-table="services"><li><a id="oral-sex">Oral sex</a></li><li><a id="girlfriend-experience">Girlfriend Experience</a></li><li><a id="boyfriend-experience">Boyfriend Experience</a></li><li><a id="bdsm">BDSM</a></li><li><a id="fantasyrole-play">Fantasy/Role Play</a></li><li><a id="erotic-massage">Erotic Massage</a></li><li><a id="anal">Anal</a></li><li><a id="other">Other</a></li><li><a id="overnight">Overnight</a></li><li><a id="1h-minimum">1h Minimum</a></li><li><a id="2h-minimum">2h Minimum</a></li><li><a id="30min-minimum">30min Minimum</a></li><li><a id="exotic-dancers">Exotic Dancers</a></li><li><a id="strippers">Strippers</a></li><li><a id="waitressing">Waitressing</a></li><li><a id="3some">3some</a></li><li><a id="pse">PSE</a></li><li><a id="striptease">Striptease</a></li><li><a id="dinner-date">Dinner Date</a></li><li><a id="costumes">Costumes</a></li><li><a id="full-service">Full Service</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="incalls-outcalls" href="">Incalls / Outcalls</a>
                        <ul>
                            <li><a id="incalls" href="">Incalls</a></li>
                            <li><a id="outcalls" href="">Outcalls</a></li>
                        </ul>
                    </li>
                    <li class="filter-name">
                        <a id="international-travel" href="">International travel</a>
                    </li>
                    <li class="filter-name">
                        <a id="height" href="">Height</a>
                        <ul data-table="heights"><li><a id="150-159-cm-5-52-ft">150 – 159 cm (5 – 5.2 ft)</a></li><li><a id="160-169-cm-53-56-ft">160 – 169 cm (5.3 – 5.6 ft)</a></li><li><a id="170-179-cm-57-510-ft">170 – 179 cm (5.7 – 5.10 ft)</a></li><li><a id="180-189-cm-511-62-ft">180 – 189 cm (5.11 – 6.2 ft)</a></li><li><a id="190-cm-63-ft-">190 cm + (6.3 ft +)</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="hair-color" href="">Hair color</a>
                        <ul data-table="hair_colors"><li><a id="blonde">Blonde</a></li><li><a id="brown">Brown</a></li><li><a id="red-head">Red Head</a></li><li><a id="black">Black</a></li><li><a id="bald">Bald</a></li><li><a id="other">Other</a></li><li><a id="dark-brown">Dark Brown</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="eye-color" href="">Eye color</a>
                        <ul data-table="eye_colors"><li><a id="black">Black</a></li><li><a id="blue">Blue</a></li><li><a id="brown">Brown</a></li><li><a id="dark-brown">Dark Brown</a></li><li><a id="green">Green</a></li><li><a id="grey">Grey</a></li><li><a id="hazel">Hazel</a></li></ul>
                    </li>
                    <li class="filter-name">
                        <a id="body-art" href="">Body art</a>
                        <ul>
                            <li><a id="tattoos" href="">Tattoos</a></li>
                            <li><a id="piercings" href="">Piercings</a></li>
                        </ul>
                    </li>
                    <li class="filter-name">
                        <a id="smokes" href="">Smokes</a>
                        <ul>
                            <li><a id="yes" href="">Yes</a></li>
                            <li><a id="no" href="">No</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="clear"></div>
</li><!-- .widget_filters end -->