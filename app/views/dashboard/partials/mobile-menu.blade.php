
@if($user->type != 1)
<style type="text/css">
	.arrowToggle:after {
		background: transparent url("/assets/frontend/img/arrow_transform.png") no-repeat left 50%;
	}
	.arrowToggleClicked:after {
		background: transparent url("/assets/frontend/img/arrow_transform.png") no-repeat right 50% !important;
	}
</style>
<div class="mobile-menu-container">
	<button class="arrowToggle" id="mobileArrowToggle"></button>
	<div id="logo">
		<a href="/">
			<img src="/assets/frontend/img/logo.png" alt="Myplaymate.com.au. the place to be">
		</a>
	</div>
</div>
<div class="mobile-nav-container">
	<ul class="mobile-nav">
		<li ng-repeat="item in busMenu" on-finish-render="test">
			<div ng-click ="changeScreens(item.id)" id='@{{ item.id }}' class='_mPIcon' title="@{{item.title}}" stateId = "@{{ item.id}}">
				@{{item.item}}
			</div>
			<ul class="@{{ item.id }}-mobile-submenu mobile-submenu">
				<li ng-repeat="name in item.submenu" id='@{{ item.id }}-submenu' ng-click = "openSubMenu(item.id)" ng-class="$index == 0 ? 'submenu-item downCaret' : 'submenu-item'" title="@{{name.title}}"  stateId = "@{{ name.id}}">
					<div ng-click="changeMobileSub(item.id, name.id)">
						@{{name.name}}
					</div>
				</li>
			</ul>
		</li>
		@if(isset($escortCount) && isset($plan))
		<li>
			<div id ='new_escort' ng-click = 'createNewEscort()'>New Escort</div>
		</li>
		@endif
		@if(Auth::check())
		<li class="dash-mobile-menu-signout-container">
			<a class="dash-mobile-menu-signout" href="/logout">Sign Out</a>
		</li>
		@endif
	</ul>
</div>
@endif