<?php

use MyPlaymate\Repositories\EscortDbRepository;
use MyPlaymate\Repositories\EscortUpdateRepository;
use MyPlaymate\Services\LocationService;

class SidebarWidgetsComposer
{
    protected $escortRepo;

    protected $updateRepo;

    protected $location;

    function __construct(EscortDbRepository $escortRepo, EscortUpdateRepository $updateRepo, LocationService $location)
    {
        $this->escortRepo = $escortRepo;
        $this->updateRepo = $updateRepo;
        $this->location = $location;
    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $spotlightEscorts = $this->escortRepo->getBumpedUp();
        $filteredSpotlightEscorts = $spotlightEscorts->take(12); //$this->location->getNearest($spotlightEscorts, 50)->take(12);
        $sexyUpdates = $this->updateRepo->getLatest(8);


        $view
            ->with('sexyUpdates',$sexyUpdates)
            ->with('featuredEscorts',$filteredSpotlightEscorts);
    }


}
