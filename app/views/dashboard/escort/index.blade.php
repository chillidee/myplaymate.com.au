@extends('dashboard.layout')

@section('content')

@include('dashboard.partials.mobile-menu')
<script>
     var id = {{$user->id}},
         mPActiveIcon = '_MpOverview',
         profiletabid = 'basicInfo',
         cache = {};
         @if( isset( $escort->id ) ) window.active_id = '{{$escort->id}}'; @else window.active_id = 0; @endif
         window.type = 'escort';
         @if( isset( $escort->id ) ) window.currentEscort = '{{$escort->id}}'; @else window.currentEscort = 0; @endif
         @if( isset( $escort->id ) ) window.currentProfileAction = 'Edit'; @else window.currentProfileAction = 'New'; @endif
</script>
<div id ='overlay'></div>
<div id ='planPopup'> @include('dashboard.partials.plan-popup') </div>
  <div class = 'container-fluid'>
    <div class ='row top_header'>
      <div class ='col-sm-3 no_padding left_header'>
          <div id ='_mp_brand_container'>
            <div id ='logo'>
              <a href ='/'><img src ='/assets/dashboard/images/mpmAv.png' alt ='MPM'></a>
            </div>
            <div id ='_mp_areaName'>
                    <span>{{ $user->full_name }}</span>
            </div>
          </div>
    </div>
    <div class ='col-sm-9 no_padding right_header'>
      <div id ='_mp_prof_header'>
        <div id ='profile_menu'>
          <ul id ='profile_dropdown'</ul>
              <li>
                  {{ $user->full_name}}
              </li>
              <li class ='signout'><a href ='/logout'>Logout</a></li>
          </ul>
        </div>
      </div>
    </div>
</div>
<div class ='row main_content'>
 <div>
   <section>
    <div class ='col-md-3 no_padding _MpDetailArea panelFixed'>
      <div id ='_mpMenu'>
        <div ng-repeat="item in busMenu" on-finish-render="test">
          <div ng-click ="changeScreens(item.id)" id = '@{{ item.id }}_' class ='_mPIcon' title="@{{item.title}}" stateId = "@{{ item.id }}">@{{item.item}}</div>
        </div>
      </div>
             <panel panelid = "@{{activeicon}}"></panel>
       </div>
      <div class ='col-md-9 col-md-offset-3 no_padding _MpScreenArea'>
             <screen screenid = "@{{activeicon}}"class ='screen'></screen>
      </div>
      </section>  

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVAKU7LtUy9yWGX_uoP1cJ9kW0Hqhf754&signed_in=true&libraries=places"
        async defer></script>
        <script src="/assets/dashboard/js/jqueryui.min.js"></script>
        <script src="/assets/dashboard/js/fastclick.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script src="/assets/dashboard/js/angular.cropper.js"></script>
        <script src="/assets/dashboard/js/ng-img-crop.js"></script>
        <script src="/assets/dashboard/js/picturecut.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
        <script src="/assets/dashboard/js/angular-sortable-view.min.js"></script>
        <script src="/assets/dashboard/js/angular-spinkit.min.js"></script>
        <script src="/assets/dashboard/js/app.js"></script>
        <script src="/assets/dashboard/js/services.js"></script>
        <script src="/assets/dashboard/js/controller.js"></script>
        <script src="/assets/dashboard/js/httpInteceptor.js"></script>
        <script src="/assets/dashboard/js/ng-file-upload-shim.min.js"></script> <!-- for no html5 browsers support -->
        <script src="/assets/dashboard/js/ng-file-upload.min.js"></script>
        <script src="/assets/dashboard/js/jquery-ui-Slid.min.js"></script>
        <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
        <script src ="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.3.5/parsley.min.js"></script>
        <script src="/assets/dashboard/js/mobile-menu.js"></script>
        <link rel="stylesheet" href="/assets/dashboard/css/jquery-ui-min.css" type="text/css" />
      </div>
</div>
</div>
@stop
