<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>MyPlaymate.com.au - Image Review</title>
</head>

<body bgcolor="#000">
	<!-- Visually Hidden Preheader Text: START -->
	<div style="display: none; font-size: 0.05; line-height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; position: absolute; top: -1000em; left: -1000em;">
		Your images have been reviewed by our team at MyPlaymate.com.au
	</div>
	<!-- Visually Hidden Preheader Text: END -->
	
<table align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#000" width="680" style="max-width: 680">
	<tr><td>
	<table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#000000" width="100%" style="max-width: 680">
		<tr>
			<td align="center">
				<a href="http://myplaymate.com.au/"><img src="http://myplaymate.com.au/assets/frontend/img/logo.png" width="80%" alt="myplaymate.com.au - Australia's Leading Directory for Escorts, Brothels and Agencies" style="max-width: 680; height: auto"></a>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 10 20 10 20; text-align: center; font-family: 'Helvetican Neue', Helvetica, Arial, sans-serif; font-size: 18; color: #FFF;">
				<h2>Hi {{$escort->escort_name}},</h2>
				<p>
					Your images have been reviewed by our team.
				</p>
			</td>
		</tr>
		
		<tr>
			<td bgcolor="#FFFFFF" align="center" width="100%">
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width: 680; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" bgcolor="#FFFFFF">
					<tr style="text-align: center">
						<td colspan="3">
							<h2 style="color: #FF0004; letter-spacing: 15px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
								IMAGE REVIEW
							</h2>
						</td>
					</tr>
					
					<tr style="text-align: center; color: #555555; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
						<td align="left" width= "30%" style="padding: 10px" valign="top">
							<h3 style="color: #FF0004">
								The following photos have been approved
							</h3>
						</td>
						
						<td align="left" width="70%" style="padding: 10px" valign="top">
							@if(isset($input['approved_images']))
							@foreach($input['approved_images'] as $image)
								<img src="{{ EscortImage::find($image[0])->thumbnail(170,170) }}" alt="">
							@endforeach
							@endif
						</td>
					</tr>
					
					<tr>						
						<td align="left" width= "30%" style="padding: 10px" valign="top">
							<h3 style="color: #FF0004">
								The following photos have been rejected
							</h3>
						</td>
						
						<td align="left" width="70%" style="padding: 10px" valign="top">
							@foreach($input['disapproved_images'] as $image)
								<img src="{{ EscortImage::find($image[0])->thumbnail(170,170) }}" alt="">
							@endforeach
							@endif
						</td>
					</tr>
					
					<tr>
						<td align="left" width= "30%" style="padding: 10px" valign="top">
							<h3 style="color: #FF0004">
								Reasons for Disapproval
							</h3>
						</td>
						
						<td align="left" width="70%" style="padding: 10px" valign="top">
							{{ $image[1] }}
						</td>
					</tr>
					
					<tr style="background-color: #000; font-size: x-small; color: #FFF; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
						<td colspan="2" style="padding: 10px"> Get in touch with us at <a href="mailto:admin@myplaymate.com.au" style="text-decoration: underline; color: #FFFFFF;">admin@myplaymate.com.au</a> or <a href="tel:1300769766" style="text-decoration: underline; color: #FFFFFF;">1300 769 766</a></td>
						
						<td align="right" style="padding: 10px">
							<a href="https://twitter.com/MyPlayMateAU" style="text-decoration: none; color: #FFFFFF; font-size: small"><img src="http://staging.myplaymate.com.au/assets/emails/Twitter.png" height="50%" width="auto" alt="twitter icon"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				
	</table>
</td></tr>	
</table>	
</body>
</html>
