   <div class="modal fade backdrop" id="leaveReviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="leaveReviewForm" role="form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Leave Review for {{ $brothel->name }} </h4>
                    <div class="alert alert-success"></div>
                </div>
                <div class="modal-body">
                    <div id="leaveReviewFields">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name:</label>
                                    <input name="name" required type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="control-label">Rating:</label>
                                    <select name="rating" class="form-control">
                                        <option value="5">5</option>
                                        <option value="4">4</option>
                                        <option value="3">3</option>
                                        <option value="2">2</option>
                                        <option value="1">1</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Review</label>
                                    <textarea name="review" required class="form-control" placeholder="Message" style="height:100px"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="escort_id" value="{{ $brothel->id }}">
                                    <button class="btn btn-default pull-right" id="leaveReviewButton">Leave Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade backdrop" id="readReviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="readReviewForm" role="form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Review(s) for {{ $brothel->name }} </h4>
                </div>
                <div class="modal-body">
                    <div id="readReviewFields">
                        @foreach($reviews as $review)
                        <div class="row">
                            <div class="col-md-12">{{$review->review}}</div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade backdrop" id="contactEscortModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="contactEscortForm" role="form">
        <div class="modal-dialog wideModal">
            <div class="modal-content darkContactModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <a href="/" class ='modalLogo'>
                     <img src="/assets/frontend/img/logo.png" alt="Myplaymate.com.au. the place to be">
                 </a>
                 <h4 class="modal-title">Contact {{ $brothel->name }} </h4>
             </div>
             <div class="modal-body">
                 @if($brothel->phone != '') 
                 @if ( $brothel->email != '' )
                 <div class ='col-md-12'>
                   <p class ='center_perc_70'>Click below to see {{$brothel->name}}'s phone number</p>
                   @else
                   <div class ='col-md-12'>
                       <p style="text-align: center;">{{$brothel->name}} is contactable by phone.  Click below to see their phone number.</p>
                       @endif
                       <div class ='actionPane actionPhone' data-num ='{{Helpers::format_phone_number($brothel->phone)}}'>{{ substr(Helpers::format_phone_number($brothel->phone), 0, 7)}}...</div>
                   </div>
                   @endif
                   @if ( $brothel->email != '')
                   @if ( $brothel->phone != 1 )
                   <div class ='col-md-12'>
                       @else
                       <div class ='col-md-6'>
                           @endif
                           <div id="playMailForm">
                            <div class="alert alert-success"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input name="name" required type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input name="phone" required type="phone" class="form-control" placeholder="Phone/Mobile Number" maxlength="10">
                                    </div>
                                    <div class="form-group">
                                        <input name="email" required type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <textarea name="message" required class="form-control" placeholder="Message" style="height:100px" maxlength="1000"></textarea>
                                    </div>
                                    <input type="hidden" name="escort_id" value="{{ $brothel->id }}">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-default pull-right" id="contactEscortButton">Send PlayMail</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </form>
</div>