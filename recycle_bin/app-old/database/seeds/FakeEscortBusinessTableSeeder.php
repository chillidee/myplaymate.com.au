<?php

class FakeEscortBusinessTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,8) as $index) {
            DB::table('escort_business')
              ->insert([
                'escort_id'         => $faker->numberBetween(1,20),
                'business_id'       => $faker->numberBetween(1,8),
            ]);
        }
    

    }
}
