<div id="topbar-buttons">
     @if(Auth::guest())
        <li style="margin-left:20px"><a href="#login" data-toggle="modal">Login</a></li>
    @endif
    @if(!Auth::guest())
        <li style="margin-left:94px"><a href="/logout">Logout</a></li>
    @endif
    @if(Auth::guest())
        <li><a href="#signup" data-toggle="modal">Sign Up</a></li>
    @endif
    <li class="search-icon">
        <a id="search-toggle">
            <img src="/assets/frontend/img/search.png" alt="">
        </a>
    </li> 
    @include('frontend.partials.topbar.search') 
</div>