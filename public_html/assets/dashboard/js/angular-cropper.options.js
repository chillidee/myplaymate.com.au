/**
 * ------ ANGULAR CROPPER GLOBAL OPTIONS --------
 * This file defines all the global options used by the Angular Cropper directive
 * all the values are the default values with which the directive gets initiated.
 */

_gCropperModule.constant('CroppableDefaults', {
	aspectRatio: 0,
	ruleOfThirds: true,
	outputType: 'base64',
	widthMin: 50,
	widthMax: 0,
	heightMin: 50,
	heightMax: 0,
	autoCropping: true,
	imageType: 'jpeg',
	centerHandles: true,
	sizeHint: true,
	loadingClass: 'cropper-loading',
	backdropOpacity: 50,
	eventPrefix: 'crop',
	optionsPrefix: 'crop',
	directiveName: '$croppable',
	preloader: true,
	lockDimensions: false
});