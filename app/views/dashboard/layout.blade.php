<!DOCTYPE HTML>
<html lang="en"  onunload = "">
    <head lang="en-AU">
    <meta charset="UTF-8" />
    <title>Myplaymate Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="/assets/dashboard/css/boostrap.min.css">
    <link rel="stylesheet" href="/assets/dashboard/css/sass/main.css">
    <link rel="stylesheet" href="/assets/dashboard/css/liveChanges.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab|Raleway:500' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   <?php // <script src="/assets/dashboard/js/picturecut.min.js"></script>?>
    @include('frontend.partials.head.favicons')
</head>
<body ng-app = 'dashboard' ng-controller ='driversController'>
<div id ='hider'>
<div class ='three-dots-row-spinner'></div>
<div id ='dashOverlay' class ='dashLoader'></div>
<div class ='spinner dashLoader'>
   <div class ='dot1'></div>
   <div class ='dot2'></div>
</div>
@yield('content') 
</div>
</body>
@if( isset( $plan ))
<script>window.plan = {{json_encode($plan)}};</script>
@endif
</html>