   <div class="modal fade backdrop" id="leaveReviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form id="leaveReviewForm" role="form">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Leave Review for {{ $escort->escort_name }} </h4>
                        <div class="alert alert-success"></div>
                    </div>
                    <div class="modal-body">
                        <div id="leaveReviewFields">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Name:</label>
                                        <input name="name" required type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="control-label">Rating:</label>
                                        <select name="rating" class="form-control">
                                            <option value="5">5</option>
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Review</label>
                                        <textarea name="review" required class="form-control" placeholder="Message" style="height:100px"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="escort_id" value="{{ $escort->id }}">
                                        <button class="btn btn-default pull-right" id="leaveReviewButton">Leave Review</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade backdrop" id="readReviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form id="readReviewForm" role="form">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Review(s) for {{ $escort->escort_name }} </h4>
                    </div>
                    <div class="modal-body">
                        <div id="readReviewFields">
                            @foreach($reviews as $review)
                                <div class="row">
                                    <div class="col-md-12">{{$review->review}}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade backdrop" id="contactEscortModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form id="contactEscortForm" role="form">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Contact {{ $escort->escort_name }} </h4>
                    </div>
                    <div class="modal-body">
                        <div id="playMailForm">
                            <h4>PlayMail</h4>
                            <div class="alert alert-success"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Name:</label>
                                        <input name="name" required type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="control-label">Phone / Mobile Number:</label>
                                        <input name="phone" required type="phone" class="form-control" placeholder="Phone/Mobile Number" maxlength="10">
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                        <input name="email" required type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Message</label>
                                        <textarea name="message" required class="form-control" placeholder="Message" style="height:100px" maxlength="1000"></textarea>
                                    </div>
                                    <input type="hidden" name="escort_id" value="{{ $escort->id }}">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-default pull-right" id="contactEscortButton">Send PlayMail</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>