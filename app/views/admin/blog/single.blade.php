@extends('admin.layout')

@section('javascript')

   <script type="text/javascript">
        (function($){
            $('textarea.tinymce').tinymce();                
        })(jQuery);
        jQuery(document).ready(function($) {
            // Dropzone.autoDiscover = false;
            // $(".dropzone").dropzone({
            //     url: '/admin/blog/upload?post_id={{ $post->id }}'
            // });
            $('.datepicker').datepicker({dateFormat: 'dd/mm/yy'});
        });
    </script>    

@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-newspaper-o"></span></div>
    <div class="pagetitle">
        <h1>Edit post - {{ $post->title }}</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open() }}
<div class="row">
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('title','Title*',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('title',$post->title,array('class'=>'form-control','required')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_title','SEO title',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_title',$post->meta_title,array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_keywords','SEO keywords',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_keywords',$post->meta_keywords,array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_description','SEO description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_description',$post->meta_description,array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
    <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('seo_url','SEO Url',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('seo_url',$post->seo_url,array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
      <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('short_content','Blog Description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('short_content',$post->short_content,array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="control-group" id="incalls">
            <h5>PUBLISHED</h5>
            <label class="radio-inline">{{ Form::radio('published', '1', $post->published==1 ? true : false) }} Yes
            </label>
            <label class="radio-inline">{{ Form::radio('published', '0', $post->published==0 ? true : false) }} No
            </label>
        </div>
    </div>
    <div class="col-md-2">
        <div class="control-group">
            {{ Form::label('date_from','Published from',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('date_from',Helpers::db_date_to_user_date($post->date_from),array('class'=>'form-control datepicker')) }}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="control-group">
            {{ Form::label('date_to','Published to',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('date_to',Helpers::db_date_to_user_date($post->date_to),array('class'=>'form-control datepicker')) }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('content','Content',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('content',$post->content,array('class'=>'tinymce')) }}
            </div>
        </div>
        <div class="alignc">
            {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
        </div>

        {{ Form::close() }}
    </div>
    <div class="col-md-3 post-thumbnail-preview">
        <h5>Thumbnail</h5>
        @if($post->thumbnail != '')
            <img src="{{Config::get('constants.CDN') .'/posts/' . str_replace ( '/uploads/', '', $post->thumbnail ) }}">
        @else
            <p>No post thumbnail uploaded yet.</p>
        @endif
    </div>
    <div class="col-md-3">
        <h5>Upload new thumbnail</h5>
        {{ Form::open(array('url'=>'/admin/blog/upload/','files'=>true,'class'=>'dropzone')) }}
        {{ Form::hidden('post_id',$post->id) }}
        {{ Form::close() }}   
    </div>

    
{{--     <div class="col-md-6">
        <div id="dZUpload" class="dropzone"></div>
    </div> --}}
</div>
{{ Form::hidden('page_id',$post->id) }}


@stop