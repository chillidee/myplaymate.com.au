@extends('frontend.layout')

@section('content')

<!-- #page-title start -->
<section id="page-title" class="page-title-1">
    <div class="container">
    	@if(isset($city))
        <h1>CITY - {{ strtoupper($city) }}</h1>
        @elseif(isset($business) && $business == 1)
        <h1>BROTHELS</h1>
        @elseif(isset($business) && $business == 2)
        <h1>AGENCIES</h1>
        @elseif(isset($business) && $business == 3)
        <h1>EROTIC MASSAGE</h1>
        @else
        <h1>ESCORTS</h1>
    	@endif
        <div class="breadcrumb-container">
            <span>You are here: </span>
            <ul class="breadcrumb">
                <li><a href="{{ url('') }}">HOME</a></li>
                @if(isset($city))
                <li><a href="#">CITY - {{ strtoupper($city) }}</a></li>
                @else
                <li><a href="#">ESCORTS</a></li>
                @endif
            </ul><!-- .breadcrumb end -->
        </div><!-- .breadcrumb-container end -->
    </div><!-- .containr end -->
</section><!-- #page-title end -->

<!-- .boxed version of the page -->
<section class="page-wrapper section-content">
    <!-- .container start -->
    <div class="container">

        <!-- .row start -->
        <div class="row">
			<aside class="col-sm-3 filters">
				@include('frontend.partials.sidebar.escort-filters')

				<ul class="aside-widgets">
				    @include('frontend.partials.sidebar.left-common-widgets')
				</ul><!-- .aside-widgets end -->	
			</aside>

			<div id="directory-container" class="col-sm-9 col-xs-12">
                <div id="view-mode" class="row clearfix">
                    <div id="view-switch">
                        <ul>
                            <li>
                            	<a href="#" id="list-view-toggle">
                            		<i class="fa fa-th-list"></i>
                            	</a>
                            </li>
                            <li>
                            	<a href="#" id="grid-view-toggle" class="active">
                            		<i class="fa fa-th-large"></i>
                            	</a>
                            </li>
                        </ul>
                    </div>
                    <div id="sort-by">
                        <h6>Sort By </h6>
                        <select>
                        	<option value="default">Default</option>
                        	<option value="my-wishlist">My Wish List</option>
                        	<option value="newest">Newest</option>
                        	<option value="oldest">Oldest</option>
                        	<option value="my-location">My Location</option>
                        </select>
                    </div>
                </div>
				<div id="directory">
					<ul class="escort-container grid-view">
						<li class="grid-template col-xs-6 col-md-4 escorts-wishlist">
		                    <figure class="escorts-img-container">
		                        <div class="figfavorite">
		                            <a href="" class="is-not-wish">
		                                <i class="fa fa-heart"></i>
		                            </a>

		                            <a href="" class="is-wish">
		                                <i class="fa fa-times"></i>
		                            </a>
		                        </div>

		                        <div class="info-container">
		                            <div class="figinfo">
		                                <span class="figname"></span>
		                                <ul class="figdata">
		                                    <li class="escort-location"></li>
		                                  	@if(isset($city))
		                                    	 <li class="escort-age"> yo - <span class="escort-distance"> km from {{ ucfirst($city) }}</span></li>
		                                    @else
		                                    	 <li class="escort-age"> yo - <span class="escort-distance"> km</span></li>
		                                    @endif
		                                </ul>
		                            </div><!-- figinfo-->
		                        </div><!-- end of info-container-->
		                        <a href="{{ url('escorts/') }}">
									<img class="img-profile" src="" alt="">	
		                        </a>
		                    </figure>
						</li>

						<li class="list-template col-md-12 escorts-wishlist">
		                    <figure class="escorts-img-container">
		                        <a href="{{ url('escorts/') }}">
									<img class="img-profile" src="" alt="">	
		                        </a>
		                        <div class="info-container">
		                            <div class="figinfo">
		                                <span class="figname"></span>
		                                <ul class="figdata">
		                                    <li class="escort-location"><a href=""></a> - </li>
		                                    <li class="escort-age"> yo - </li>
		                                    @if(isset($city))
		                                    	<li class="escort-distance"> km from {{ ucfirst($city) }}</li>
		                                    @else
		                                    	<li class="escort-distance"> km</li>
		                                    @endif
		                                    
		                                </ul>
		                                <div class="figdesc escort-description">
											<p></p>
		                                </div>
		                            </div><!-- figinfo-->
		                            <div class="fig-readmore">
		                            	<a href="{{ url('escorts/') }}">
		                            		<button>Read more...</button>
		                            	</a>
		                            </div>
		                        </div><!-- end of info-container-->

		                        <div class="figfavorite">
		                            <a href="" class="is-not-wish">
		                                <i class="fa fa-heart"></i>
		                            </a>

		                            <a href="" class="is-wish">
		                                <i class="fa fa-times"></i>
		                            </a>
		                        </div>
		                    </figure>
						</li>
					</ul>
					<div class="spinner">
						<img src="/assets/frontend/img/tail-spin.svg"/>
					</div>
				</div>
			</div>
        </div><!-- .row end -->
    </div><!-- .container end -->
</section> <!-- End of the Boxed Version Page -->

@stop

@section('javascript')

	<script>


	jQuery(document).ready(function($) {
		var listToggle = $('#list-view-toggle'),
			gridToggle = $('#grid-view-toggle'),
			container = $('.escort-container'),
			sortSelect = $('#sort-by select');

		// If the page was called by brothels, agencies or massage controller, add the business type to the body for the filters
		@if(isset($business))
			$('body').attr('data-business', {{ $business }});
		@endif

		// If the page was called by a city, add the suburb id to the body for the filters
		@if(isset($suburb_id))
			$('body').attr('data-suburb-id', '{{ $suburb_id }}');
		@endif

		// If the page was called as "my wishlist", add the wishlist attribute to the body for the filters
		@if(isset($my_wishlist))
			$('body').attr('data-wishlist', 1);
		@endif

		applyFilters();

	    infiniteScroll();

        if (Cookies.get('view') == 'list') {
            jQuery('.escort-container').removeClass('grid-view').addClass('list-view');
			listToggle.addClass('active');
			gridToggle.removeClass('active');
        } else {
            jQuery('.escort-container').removeClass('list-view').addClass('grid-view');
			gridToggle.addClass('active');
			listToggle.removeClass('active');
        }


		// Events
		listToggle.on('click', function(e) {
			e.preventDefault();

			listToggle.addClass('active');
			gridToggle.removeClass('active');
			Cookies.set('view','list');
			applyFilters();
		});

		gridToggle.on('click', function(e) {
			e.preventDefault();

			gridToggle.addClass('active');
			listToggle.removeClass('active');
			Cookies.set('view','grid');
			applyFilters();
		});

		var toggleFilters = $('#toggleFilters');
		if($(window).width() < 768){
			toggleFilters.show();
		}
		toggleFilters.on('click', function(e) {
			e.preventDefault();
			$('.filters').fadeToggle(300);
		});
	});

	</script>

@stop