@extends('frontend.layout')

@section('content')

	<!-- #page-title start -->
	<section id="page-title" class="page-title-1">
	    <div class="container">
	        <h1>REPORT AN ESCORT</h1>
	        <div class="breadcrumb-container">
	            <span>You are here: </span>
	            <ul class="breadcrumb">
	                <li><a href="{{ url('') }}">HOME</a></li>
	                <li><a href="#">REPORT AN ESCORT</a></li>
	            </ul><!-- .breadcrumb end -->
	        </div><!-- .breadcrumb-container end -->
	    </div><!-- .containr end -->
	</section><!-- #page-title end -->	

	<!-- .boxed version of the page -->
	<section class="page-wrapper top-space">
	    <!-- .container start -->
	    <div class="container">
	        <div class="row">
	            <div class="col-md-9">
	                <div class="triggerAnimation animated fadeInRight" data-animate="fadeInRight" style="">
	                    <div class="col-md-12">
	                        <h5>REPORT</h5>

							@include('frontend.partials.message')

	                        <div id="login-container">
								{{ Form::open() }}
	                                <fieldset>
	                                    <div class="control-group">
	                                    	{{ Form::label('escort_name','Escort name',array('class'=>'control-label')) }}<span>*</span>
	                                        <div class="controls">
	                                        	{{ Form::text('escort_name','',array('class'=>'input-xlarge input03')) }}
	                                        </div>
						                    @if($errors->has('escort_name'))
						                    <p class="form-error">{{ $errors->first('escort_name') }}</p>
						                    @endif
	                                    </div>
	                                    <div class="control-group">
	                                    	{{ Form::label('email','Email',array('class'=>'control-label')) }}<span>*</span>
	                                        <div class="controls">
	                                        	{{ Form::email('email','',array('class'=>'input-xlarge input03')) }}
	                                        </div>
						                    @if($errors->has('email'))
						                    <p class="form-error">{{ $errors->first('email') }}</p>
						                    @endif
	                                    </div>
	                                    <div class="control-group">
	                                    	{{ Form::label('name','Your name',array('class'=>'control-label')) }}<span>*</span>
	                                        <div class="controls">
	                                        	{{ Form::text('name','',array('class'=>'input-xlarge input03')) }}
	                                        </div>
						                    @if($errors->has('name'))
						                    <p class="form-error">{{ $errors->first('name') }}</p>
						                    @endif
	                                    </div>
	                                    <div class="control-group">
	                                    	{{ Form::label('phone','Phone',array('class'=>'control-label')) }}<span>*</span>
	                                        <div class="controls">
	                                        	{{ Form::text('phone','',array('class'=>'input-xlarge input03')) }}
	                                        </div>
						                    @if($errors->has('phone'))
						                    <p class="form-error">{{ $errors->first('phone') }}</p>
						                    @endif
	                                    </div>
	                                    <div class="control-group">
	                                    	{{ Form::label('subject','Subject',array('class'=>'control-label')) }}<span>*</span>
	                                        <div class="controls">
	                                        	{{ Form::text('subject','',array('class'=>'input-xlarge input03')) }}
	                                        </div>
						                    @if($errors->has('subject'))
						                    <p class="form-error">{{ $errors->first('subject') }}</p>
						                    @endif
	                                    </div>
	                                    <div class="control-group">
	                                    	{{ Form::label('message','Message',array('class'=>'control-label')) }}<span>*</span>
	                                        {{ Form::textarea('message','',array('class'=>'wpcf7-textarea input03')) }}
	                                    </div>
					                    @if($errors->has('message'))
					                    <p class="form-error">{{ $errors->first('message') }}</p>
					                    @endif
	                                    <div class="control-group">
	                                        <div class="controls" id="rightright">
	                                            <div class="button3">
		                                        	{{ Form::submit('Send') }}
	                                            </div>
	                                        </div>
	                                    </div>
	                                </fieldset>
	                            {{ Form::close() }}
	                        </div>
	                    </div>
	                </div><!-- .triggerAnimation.animated end -->
					</form>
	            </div><!-- .col-md-9 end -->

	            <aside class="col-md-3 col-xs-12 col-sm-10 aside aside-left triggerAnimation animated fadeInLeft" data-animate="fadeInLeft" style="">
	                <ul class="aside-widgets">
	                    <!-- .widget_navigation -->
	                    <li class="widget widget_contact">
	                        <h5>CONTACT INFO</h5>
	                        <div id="mpm-contact">
								{{ StaticBlock::getHtml('contact-info') }}
	                        </div>
	                    </li><!-- .widget_Navigation end -->
	                </ul><!-- .aside-widgets end -->
	            </aside><!-- .aside.aside-left end -->
	        </div>
	    </div><!-- .container end -->
	</section> <!-- End of the Boxed Version Page -->
@stop

	
