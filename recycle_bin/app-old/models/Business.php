<?php

class Business extends \Eloquent {
	protected $fillable = [];

   /**
     * @return mixed
     */
    public function escorts()
    {
        return $this->hasMany('Escort');
    }
}