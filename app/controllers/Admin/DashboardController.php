<?php
namespace Admin;

use Controller, View, Auth;
use Business, Escort, EscortImage, EscortReview, EscortUpdate;

class DashboardController extends Controller
{
	public function index()
	{
		$user = Auth::user();
		$businesses_waiting = Business::whereStatus(1)->count();
		$escorts_waiting = Escort::whereStatus(1)->count();
		$images_waiting = EscortImage::whereStatus(1)->count();
		/*$images_waiting = array();
		foreach( $images_waiting as $images ){
			 $escort = Escort::find( $images_waiting->escort_id );
			 if( $escort )
			 	  $images_waiting[] = $escort;
		}*/
		$reviews_waiting = EscortReview::whereStatus(1)->count();
		$updates_waiting = EscortUpdate::whereStatus(1)->count();
		return View::make('admin.dashboard')
			->with('user',$user)
			->with('businesses_waiting',$businesses_waiting)
			->with('escorts_waiting',$escorts_waiting)
			->with('images_waiting',$images_waiting)
			->with('reviews_waiting',$reviews_waiting)
			->with('updates_waiting',$updates_waiting);
	}
}

