@extends('admin.layout')
@section('javascript')
	<script>

		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": {
		        	url: "/admin/ajax_administrators",
		        	data: function(d) {
		        		d.superuser = $('#superuser').val();
		        	}
		        },
				lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'full_name', name: 'full_name'},
		            {data: 'active', name: 'active'},
		            {data: 'super_user', name: 'super_user'},
		            {data: 'phone', name: 'phone'},
		            {data: 'email', name: 'email'},
		            {data: 'created_at', name:'created_at'  },
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });
		    
		    $('.message').show();

		    $('body').on('click', '.delete', function(e) {
		    	e.preventDefault();
		    	var $this = $(this),
		    		userId = $this.parents('ul').data('user-id');
		    	$.ajax({
		    		url: '/admin/users/delete',
		    		data: {user_id: userId},
		    	})
		    	.done(function() {
					$this.parents('tr').remove();
					jQuery('.alert-success').text('User deleted!').fadeIn();
		    	});
		    });

		    $('body').on('click', '.reset-password', function(e) {
		    	e.preventDefault();
		    	var email = $(this).data('email');
		    	$.ajax({
		    		url: '/users/forgot-password',
		    		type: 'POST',
		    		data: {email: email},
		    	})
		    	.done(function() {
					jQuery('.alert-success').text('Password reset email sent!').fadeIn();
		    	});
		    	
		    });
		});
	</script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Administrators</h1>
    </div>
</div><!--pageheader-->

	@if($message_success)
		<div class="section-content message alert alert-success">{{ $message_success }}</div>
	@endif

	@include('admin.partials.users-breadcrumb')

<div class="row">
	<a href="/admin/users/new" class="create-user btn btn-success btn-lg pull-right section-content">Create user</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Full name</th>
			<th>Active</th>
			<th>Admin</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Date of creation</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

@stop



