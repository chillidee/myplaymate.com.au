@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Edit escort about - {{ $escort->escort_name }}</h1>
    </div>
</div><!--pageheader-->

@include('admin.partials.escorts-breadcrumb')
	
{{ Form::open(['id'=>'escort-about']) }}

<div class="row">
	<div class="col-md-6">
		<div class="control-group">
			<h5>ABOUT ME</h5>
    		{{ Form::textarea('about_me', $escort->about_me, array('required','rows' => 5,'class'=>'form-control')) }}
		</div>
    	<div class="control-group" id="ethnicity-items">
    		<h5>ETHNICITY</h5>
	            @foreach(Ethnicity::all() as $ethnicity)
	            	<div class="radio-inline">
	                    {{ Form::radio('ethnicity_id', $ethnicity->id, $escort->ethnicity_id==$ethnicity->id ? true : false, array('required')) }} <span>{{ $ethnicity->name }}</span>
	            	</div>
	            @endforeach
	    </div>
    	<div class="control-group" id="hair-items">
    		<h5>HAIR COLOR</h5>
            @foreach(HairColor::all() as $hairColor)
            	<div class="radio-inline">
                    {{ Form::radio('hair_color_id', $hairColor->id, $escort->hair_color_id==$hairColor->id ? true : false,array('required')) }} <span>{{ $hairColor->name }}</span>
            	</div>
            @endforeach
	    </div>
	    <div class="control-group" id="smokers" class="clearfix">
	        <h5>SMOKER</h5>
	        <label class="radio-inline">{{ Form::radio('does_smoke', '1', $escort->does_smoke==1 ? true : false) }} Yes
	        </label>
	        <label class="radio-inline">{{ Form::radio('does_smoke', '0', $escort->does_smoke==0 ? true : false) }} No
	        </label>
	        <label class="radio-inline">{{ Form::radio('does_smoke', '2', $escort->does_smoke==2 ? true : false) }} Not included
	        </label>
	    </div>
	    <div class="control-group" id="tattoos" class="clearfix">
	    	<h5>TATTOOS</h5>
	        <label class="radio-inline">{{ Form::radio('tattoos', '1', $escort->tattoos==1 ? true : false) }} Yes
	        </label>
	        <label class="radio-inline">{{ Form::radio('tattoos', '0', $escort->tattoos==0 ? true : false) }} No
	        </label>
	        <label class="radio-inline">{{ Form::radio('tattoos', '2', $escort->tattoos==2 ? true : false) }} Not included
	        </label>
	    </div>
	    <div class="control-group" id="piercings" class="clearfix">
	    	<h5>PIERCINGS</h5>
	        <label class="radio-inline">{{ Form::radio('piercings', '1', $escort->piercings==1 ? true : false) }} Yes
	        </label>
	        <label class="radio-inline">{{ Form::radio('piercings', '0', $escort->piercings==0 ? true : false) }} No
	        </label>
	        <label class="radio-inline">{{ Form::radio('piercings', '2', $escort->piercings==2 ? true : false) }} Not included
	        </label>
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="control-group" id="body-items">
	    	<p>BODY</p>
            @foreach($bodies as $body)
            	<div class="radio-inline">
                	{{ Form::radio('body_id', $body->id, $escort->body_id == $body->id ? true : false, array('required')) }} <span>{{ $body->name }}</span>
            	</div>
            @endforeach
	    </div>
    	<div class="control-group" id="eye-items">
    		<h5>EYE COLOR</h5>
            @foreach(EyeColor::all() as $eyeColor)
            	<div class="radio-inline">
	                {{ Form::radio('eye_color_id', $eyeColor->id, $escort->eye_color_id==$eyeColor->id ? true : false, array('required')) }} <span>{{ $eyeColor->name }}</span>
            	</div>
			@endforeach
	    </div>
	    <div class="control-group" id="incalls">
            <h5>INCALLS</h5>
	        <label class="radio-inline">{{ Form::radio('does_incalls', '1', $escort->does_incalls==1 ? true : false) }} Yes
	        </label>
	        <label class="radio-inline">{{ Form::radio('does_incalls', '0', $escort->does_incalls==0 ? true : false) }} No
	        </label>
	    </div>
	    <div class="control-group" id="outcalls" class="clearfix">
            <h5>OUTCALLS</h5>
	        <label class="radio-inline">{{ Form::radio('does_outcalls', '1', $escort->does_outcalls==1 ? true : false) }} Yes
	        </label>
	        <label class="radio-inline">{{ Form::radio('does_outcalls', '0', $escort->does_outcalls==0 ? true : false) }} No
	        </label>
	    </div>
	    <div class="control-group" id="outcalls" class="clearfix">
            <h5>TRAVELS INTERNATIONALLY</h5>
	        <label class="radio-inline">{{ Form::radio('does_travel_internationally', '1', $escort->does_travel_internationally==1 ? true : false) }} Yes
	        </label>
	        <label class="radio-inline">{{ Form::radio('does_travel_internationally', '0', $escort->does_travel_internationally==0 ? true : false) }} No
	        </label>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="languages clearfix">
	        <div class="languages-items-title">
	            <h5>LANGUAGES SPOKEN</h5>
	        </div>
	        <div class="languages-items-items clearfix">
	            @foreach(Language::all() as $language)
	            <div class="col-md-6">
	                {{ Form::checkbox('language_'.$language->id, $language->id, $escort->languages()->where('language_id', $language->id)->first() ? 1 : 0,array('data-parsley-multiple'=>'languages')) }}
	                {{ Form::label('language_'.$language->id,$language->name) }}
	            </div>
	            @endforeach
	            <script>
	                jQuery('.services-items-items')
	                    .find('input')
	                    .last()
	                    .attr('data-parsley-required', 'true')
	                    .attr('data-parsley-error-message','You must select at least one service.');

	                jQuery('.languages-items-items')
	                    .find('input')
	                    .last()
	                    .attr('data-parsley-required', 'true')
	                    .attr('data-parsley-error-message','You must select at least one language.');
	            </script>
	        </div>
	    </div>	
	</div>
	<div class="col-md-6">
	    <div class="services clearfix">
	        <div class="services-items-title">
	            <h5>SERVICES</h5>
	        </div>
	        <div class="services-items-items clearfix">
	            @foreach(Service::orderBy('order')->get() as $service)
	            <div class="col-md-6">
	                {{ Form::checkbox('service_'.$service->id, $service->id, $escort->services()->where('service_id', $service->id)->first() ? 1 : 0,array('data-parsley-multiple'=>'services')) }}
	                {{ Form::label('service_'.$service->id,$service->name) }}
	            </div>
	            @endforeach
	        </div>
	    </div>
	</div>
</div>
{{ Form::submit('SAVE AND CONTINUE',array('class'=>'btn btn-primary big-section-content')) }}
{{ Form::close() }}

<script>
    jQuery(document).ready(function($) {
        $('#escort-about').parsley();
    });
</script>
@stop