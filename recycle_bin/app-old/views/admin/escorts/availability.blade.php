@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Edit escort availability, rates and tourings - {{ $escort->escort_name }}</h1>
    </div>
</div><!--pageheader-->

@include('admin.partials.escorts-breadcrumb')

{{ Form::open(['id'=>'escortAvailability']) }}
	<div class="row">
		<div class="col-lg-6">
		    <table class="table table-striped" id="availabilityTable">
		        <tbody>
		            <tr>
		                <td>Availibility</td>
		                <td>
		                    <div>
		                        <input name="24-7" type="checkbox" class="24-7">
		                        <label for="24-7"><span></span>24h</label>
		                    </div>
		                </td>
		                <td>Start Time:</td>
		                <td>End Time:</td>
		            </tr>
		            <script>
		                jQuery('body').on('change', '.24-7', function() {
		                    if (jQuery(this).is(':checked')) {
		                        jQuery('input.24').prop('checked', true);
		                    }else
		                        jQuery('input.24').prop('checked', false);
		                });
		            </script>
		            <?php $availabilities = EscortAvailability::whereEscortId($escort->id)->get(); ?>
		            @foreach(Helpers::$weekDays as $key => $day)
		                <?php $row = ''; ?>
		                @foreach($availabilities as $availability)
		                    @if($availability->day == $key)
		                        <?php $row = $availability; ?>
		                    @endif
		                @endforeach
		                <tr class="day">
		                    <td>
		                        {{ Form::checkbox('enabled_'.$key,1,!empty($row) ? $row->enabled : 0,array('class'=>'day-enabled')) }}
		                        {{ Form::label('enabled_'.$key,$day) }}
		                    </td>
		                    <td >
		                        {{ Form::checkbox('24_'.$key,1,!empty($row) ? $row->twenty_four_hours : 0,array('class'=>'24')) }}
		                        {{ Form::label('24_'.$key,'24h') }}
		                    </td>
		                    <td>
		                        {{ Form::select('start_hour_'.$key,Helpers::$dayHours,!empty($row) ? $row->start_hour_id : '',array('class'=>'hours_select')) }}

		                        {{ Form::select('start_am_pm_'.$key,array('am','pm'),!empty($row) ? $row->start_am_pm : '',array('class'=>'hours_select')) }}
		                    </td>
		                    <td>
		                        {{ Form::select('end_hour_'.$key,Helpers::$dayHours,!empty($row) ? $row->end_hour_id : '',array('class'=>'hours_select')) }}

		                        {{ Form::select('end_am_pm_'.$key,array('am','pm'),!empty($row) ? $row->end_am_pm : '',array('class'=>'hours_select')) }}
		                    </td>
		                </tr>   
		            @endforeach
		        </tbody>
		    </table>		
		</div>
		<div class="col-lg-6">
            <table class="table table-striped" id="ratesTable">
                <tbody>
                    <tr>
                        <td>Incall Services</td>
                        <td>Outcall Services</td>
                    </tr>
                        <?php $escortRates = EscortRate::whereEscortId($escort->id)->first(); ?>
                        @foreach(Helpers::$rateDurations as $duration)
                        <tr class="rates-rows">
                            @foreach($duration as $key => $value)
                                <td>
                                    <span class="rates-sign">$</span>
                                    {{ Form::text($key,$escortRates->$key,array('class'=>'rates-input')) }}
                                    {{ Form::label($key,$value) }}
                                </td>
                            @endforeach
                        </tr>
                        @endforeach    
                </tbody>
            </table>
            <hr>
            <h2>Additional Rates Information</h2>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::textarea('notes',$escort->notes,array('class'=>'form-control','style'=>'width:100%')) }}
                </div>
            </div>
		</div>
	</div>
	<div class="alignc">
	    {{ Form::submit('SAVE AND CONTINUE',array('class'=>'btn btn-primary section-content')) }}	
	</div>
{{ Form::close() }}

<hr>

<div id="backend-profile-touring">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
	    <h2 class="alignc">Touring</h2>
	    <div class="alert alert-success" id="touring-delete-success">Touring deleted!</div>
	    <div class="alert alert-success" id="touring-add-success">Touring created!</div>
            <div class="triggerAnimation animated fadeInUp" data-animate="fadeInUp" style="">
                <table class="table table-striped" style="text-align:left;" id="touringTable">
                    <tbody>
                        <tr>
                            <td>Date From</td>
                            <td>Date To</td>
                            <td>Location</td>
                            <td>Action</td>
                        </tr>
                        <tr class="touringEntry touringEntryTemplate">
                            <td class="from_date"></td>
                            <td class="to_date"></td>
                            <td class="suburb_id"></td>
                            <td><a class="deleteTouring">Delete</a></td>
                        </tr>
                        @foreach(EscortTouring::whereEscortId($escort->id)->get() as $touring)

                        <tr class="touringEntry" id="touringEntry-{{ $touring->id }}">
                            <td class="from_date">{{ Helpers::db_date_to_user_date($touring->from_date) }}</td>
                            <td class="to_date">{{ Helpers::db_date_to_user_date($touring->to_date) }}</td>
                            <td class="suburb_id">{{ $touring->location ? $touring->location->name : '' }}</td>
                            <td><a class="deleteTouring" id="{{ $touring->id }}">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div id="touring-form">
                    <fieldset>
                        <div class="elements">
                            <label for="from_date" class="control-label">From Date<span>*</span></label>
                            <div class="controls">
                                <input id="from_date" name="from_date" type="text" class="form-control datepicker" placeholder="Select date.">
                            </div>
                        </div>
                        <div class="elements">
                            <label for="to_date" class="control-label">To Date<span>*</span></label>
                            <div class="controls">
                                <input id="to_date" name="to_date" type="text" class="form-control datepicker" placeholder="Select date.">
                            </div>
                        </div>
                        <div class="elements">
                            <label for="suburb_id" class="control-label">Location<span>*</span></label>
                            <div class="controls">
                                {{ Form::select('suburb_id',$suburbs,$escort->current_suburb_id,array('class'=>'select2','min'=>'1','id'=>'suburb_id','required','style'=>'width:100%')) }}
                            </div>
                        </div>
                        <br>
                        <div class="elements touring-btn-container">
                            <div class="controls alignc touring-btn">
                                <button id="addTouring" class="btn btn-primary">ADD</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div><!-- .triggerAnimation.animated end -->
        </div><!-- .col-md-12 end -->
    </div><!-- .row end -->
</div>


<script>
	(function($){
	    $('form').parsley();
	    $('.datepicker').datepicker({dateFormat: 'dd/mm/yy'});
	    $('.select2').select2({
            placeholder: "Select a suburb",
            minimumInputLength: 4
        });

        $('body').on('click', '.deleteTouring', function(e) {
            e.preventDefault();
            var touringId = $(this)[0].id;
            $.ajax({
                url: '/deleteTouring',
                data: {touringId: touringId},
            })
            .done(function() {
                $('#touringEntry-'+touringId).remove();
                $('.alert').hide();
                $('#touring-delete-success').show();
            });
        });

        $('#addTouring').on('click', function(e) {
            e.preventDefault();
            var from_date = $('#from_date').val(),
                to_date = $('#to_date').val(),
                suburb_id = $('#suburb_id').val();

            $.ajax({
                url: '/addTouring',
                data: {
                   from_date : from_date,
                   to_date : to_date,
                   suburb_id : suburb_id,
                   escort_id : {{ $escort->id }}
                },
            })
            .done(function(data) {
                var clone = $('.touringEntry').last().clone().removeClass('touringEntryTemplate');
                clone[0].id = 'touringEntry-'+data.touring_id;
                clone.find('td.from_date').text(from_date);
                clone.find('td.to_date').text(to_date);
                clone.find('td.suburb_id').text(data.suburb_name);
                clone.find('.deleteTouring')[0].id = data.touring_id;
                clone.appendTo('#touringTable tbody');
                $('#touring-form input').val('');
                $('.alert').hide();
                $('#touring-add-success').show();
            });
        });	
	})(jQuery);

</script>
@stop