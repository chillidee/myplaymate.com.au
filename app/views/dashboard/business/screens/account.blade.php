<div id ='overlayBackGround'></div>
<div class ='_MpoffWhite'>
   <message></message>
   <div>
       <h3>Edit your account</h3>
       <p>This is where you can manage your Account details, including passwords. If you have any issues editing your account, please contact <br>Myplaymate on 1300 769 766</p>
       <?php $pattern = "[^@]+@[^@]+\.[^@]+";?>
       <form class ='profileForm' controller ='userDetails' ng-submit = 'saveUser()' ng-model = 'userDetails' id ='userDetailsForm'>
        <div class="control-group">
            {{ Form::label('full_name','Full name',array('class'=>'form_required')) }}
            <div class="controls">
                {{ Form::text('full_name', $user->full_name, array('data-parsley-group' => 'userForm','class' => 'input-xlarge input03','required','ng-model'=>'userDetails.full_name', 'ng-init'=>"userDetails.full_name=" . json_encode( $user->full_name) ))}}
            </div>
        </div>

        <div class="control-group">
         {{ Form::label('email','Email',array('class'=>'form_required')) }}
         <div class="controls">
            {{ Form::text('email', $user->email, array('data-parsley-group' => 'userForm','class' => 'input-xlarge input03','required','type' => "email",'data-parsley-type'=>'email','pattern'=>$pattern,'ng-model'=>'userDetails.email', 'ng-init'=>"userDetails.email=" . json_encode( $user->email ))) }}
        </div>
    </div>
    <div class="control-group">
        {{ Form::label('phone','Phone',array('class'=>'form_required')) }}
        <div class="controls">
            {{ Form::text('phone', $user->phone, array('data-parsley-group' => 'userForm','class' => 'input-xlarge input03','ng-model'=>'userDetails.phone', 'ng-init'=>"userDetails.phone=" . json_encode ( $user->phone ),'data-parsley-length'=>'[10,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-pattern-message' => 'Please enter a valid phone number!','data-parsley-length-message' => 'Please enter a valid phone number!')) }}
        </div>
    </div>
    <h5>Change Password</h5>
    <div class="control-group">
      {{ Form::label('password','Password',array('class'=>'control-label')) }}
      <div class="controls">
        {{ Form::text('password1', '',array('data-parsley-group' => 'userForm','id' => 'password1','data-parsley-equalto-message' => 'Passwords must match!','data-parsley-equalto' => '#password2','class' => 'input-xlarge input03','ng-model'=>'userDetails.password')) }}
    </div>
</div>

<div class="control-group">
   {{ Form::label('password2','Confirm password',array('class'=>'control-label')) }}
   <div class="controls">
    {{ Form::text( 'password2', '',array( 'data-parsley-group' => 'userForm', 'id' => 'password2','class' => 'input-xlarge input03')) }}

</div>

{{ Form::submit('SAVE',array('class'=>'submit-button')) }}
</div>
{{ Form::close() }}
</div> 
<script>
    var formInstance = $('#userDetailsForm').parsley();
    var field = $('#password1').parsley();
    formInstance.validate({group:'userForm'});
    </script
