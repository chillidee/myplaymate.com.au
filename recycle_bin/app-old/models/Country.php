<?php

class Country extends Eloquent
{
    protected $guarded = [];

    public $timestamps = false;

    public static $rules = [

    ];

    public function suburbs()
    {
        return $this->hasMany('Suburb');
    }

    public function state()
    {
        return $this->hasMany('State');
    }
}
