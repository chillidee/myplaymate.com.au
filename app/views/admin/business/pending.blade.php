@extends('admin.layout')
@section('javascript')
  <script>
    jQuery(document).ready(function() {
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax_pending_businesses",
            initComplete: function (response) {
                 $( '.delete-profile' ).on( 'click', function() {
                        $( '.sure_delete' ).show();
                        $( '#yes-delete' ).attr("data-id", $( this ).data( 'id' ));
                  });
             },
        lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'suburb_id', name: 'suburb_id'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name:'created_at'  },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
      oTable.on( 'draw.dt', function () {
    $( '.delete-profile' ).on( 'click', function() {
             $( '.sure_delete' ).show();
             $( '#yes-delete' ).attr("data-id", $( this ).data( 'id' ));
    });
} );
    });
  </script>
<style>
.overlay { position:fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: #000; opacity: .3; }
.loader {
  font-size: 10px;
  margin: 50px auto;
  z-index:1001;
  text-indent: -9999em;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background: #ffffff;
  background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  position: relative;
  -webkit-animation: load3 1.4s infinite linear;
  animation: load3 1.4s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
   display:none;
  position:fixed;
  left:50%;
  margin-left: 50px;
}
.loader:before {
  width: 50%;
  height: 50%;
  background: #ffffff;
  border-radius: 100% 0 0 0;
  position: absolute;
  top: 0;
  left: 0;
  content: '';
}
.loader:after {
  width: 75%;
  height: 75%;
  border-radius: 50%;
  content: '';
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
@-webkit-keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
  .sure_delete {
    position: fixed;
    width: 500px;
    height: 197px;
    background-color: #fff;
    top: 200px;
    left: 50%;
    z-index: 10000;
    padding: 50px;
    border-radius: 8px;
    border: 1px solid #222;
    margin-left: -250px;
    font-size: 22px;
    display:none;
}
.detele_buttons {
    background-color: #1F5063;
    border: none;
    box-shadow: none;
    font-size: 20px;
    padding: 7px 22px;
    border-radius: 3px;
    color: #fff;
    border-right: 20px;
    margin-top: 19px;
    margin-right: 30px;
}
  </style>
@stop
@section('content')
<div class ='overlay loadingDiv'></div>
<div class ='loader loadingDiv'></div>
<div class ='sure_delete'>
    <div>
        Are you sure you want to delete this business?
    </div>
  <button class ='detele_buttons' id ='no-delete'>Cancel</button>
  <button class ='detele_buttons' id ='yes-delete' data-id =''>Delete</button>
</div>  
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-money"></span></div>
    <div class="pagetitle">
        <h1>Pending businesses</h1>
    </div>
</div><!--pageheader-->

<div class="row">
  <a href="/admin/businesses/new" class="create-user btn btn-success btn-lg pull-right section-content">Create business</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Location</th>
      <th>Phone</th>
      <th>Email</th>
      <th>Date of creation</th>
      <th>Action</th>
    </tr>
  </thead>
</table>
<script>
   
  function setStatus(button,status)
  {
    var url,
      escortId = button.data('escort-id');
    if(status == 2){
      url = 'approve_business';
      message = 'Business approved!';
    }
    if(status == 3){
      url = 'disapprove_business';
      message = 'Business disapproved!';
    }
    $( '.loadingDiv').show();

    jQuery.ajax({
      url: '/admin/'+url,
      data: {escort_id: escortId},
    })
    .done(function() {
      $( '.loadingDiv').hide();
      button.parents('tr').remove();
      jQuery('.alert-success').text(message).fadeIn();
      $( '#pendingBusinessesBubble').html( parseInt($( '#pendingBusinessesBubble').html() ) - 1 );
    });
  }

  (function($){
    $('body').on('click', '.approve', function(e) {
      e.preventDefault();
      setStatus($(this),2);
    });  

    $('body').on('click', '.disapprove', function(e) {
      e.preventDefault();
      setStatus($(this),3);
    });  
  })(jQuery);
</script>
  </script>
@stop



