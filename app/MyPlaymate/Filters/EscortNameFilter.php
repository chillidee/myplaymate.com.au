<?php
namespace MyPlaymate\Filters;

use MyPlaymate\Filters\Filter;

class EscortNameFilter extends Filter
{
	public $column = 'escort_name';

	public function apply($escorts,$filterType)
	{
		return array_filter($escorts,function($escort) use ($filterType)
		{
			if($filterType == '') return true;
			return  stripos($escort['escort_name'],$filterType) !== false;
		});
	}
}
