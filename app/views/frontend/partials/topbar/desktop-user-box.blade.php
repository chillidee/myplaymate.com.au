<ul class="user-box">
    @if(Auth::check())
        <li class="hidden-xs hidden-md">
            Welcome, {{ Auth::user()->full_name }}
        </li>
        <li class="hidden-xs hidden-sm hidden-md">
            <i class="fa fa-star hidden-md"></i>
            <a href="/wishlist">My Wish list</a>
        </li>
        <li class="hidden-sm">
            <i class="fa fa-star"></i>
            <a href="/user/profile">Manage profile</a>
        </li>
    @else
        <li class="login-li hidden-lg">
            <a href="#login" data-toggle="modal">Login</a>
        </li>
        <li class="register-li hidden-lg">
            <a href="#signup" data-toggle="modal">Sign Up</a>
        </li>
    @endif
</ul>