@extends('admin.layout')

@section('javascript')

	<script>
		function setStatus(button,status)
		{
			var url,
				updateId = button.data('update-id');
			if(status == 2){
				url = 'approve_update';
				message = 'Update approved!';
			}
			if(status == 3){
				url = 'disapprove_update';
				message = 'Update disapproved!';
			}

			jQuery.ajax({
				url: '/admin/'+url,
				data: {update_id: updateId},
			})
			.done(function() {
	    		var label = button.parents('tr').find('span.label');
	    		label.removeClass('label-warning label-danger label-success');
	    		if (status == 2)
	    			label.addClass('label-success').text('approved');
	    		else
	    			label.addClass('label-danger').text('disapproved');
				jQuery('.alert-success').text(message).fadeIn();
			});
		}

		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "/admin/ajax_updates",
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'escort_id', name: 'escort_id'},
		            {data: 'message', name: 'message'},
		            {data: 'status', name: 'status'},
		            {data: 'created_at', name:'created_at'},
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });


		$('body').on('click', '.approve', function(e) {
			e.preventDefault();
			setStatus($(this),2);
		});	

		$('body').on('click', '.disapprove', function(e) {
			e.preventDefault();
			setStatus($(this),3);
		});	

		});
	</script>

@stop

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Sexy updates</h1>
    </div>
</div><!--pageheader-->

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Escort name</th>
			<th>Message</th>
			<th>Status</th>
			<th>Date of creation</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

@stop