<?php 
namespace Admin;

use Redirect, Controller, View, Datatables, Input, Auth, Helper, ImageHelper, Mail, Config, Carbon\Carbon;
use Escort, Suburb, User, Age, Height, Body, EscortService, EscortLanguage, Service, Language, EscortAvailability, EscortTouring, EscortRate, EscortImage, EscortBusiness, EscortUpdate, EscortReview, EmailTemplate, Helpers, Visit, PhoneCall, Playmail;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;
use MyPlaymate\Repositories\BusinessRepository;
use Illuminate\Support\Facades\Request;
  
class CsvController extends Controller {
  
    public function import_csvIndex() {
        return View::make('admin.csv.index');
    }
  }
  ?>
  