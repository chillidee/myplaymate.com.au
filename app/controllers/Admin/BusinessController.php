<?php
namespace Admin;

use Redirect, EmailTemplate, Config, Controller, Mail, View, Datatables, Input, Auth, Helper, ImageHelper, Session, Helpers, Carbon\Carbon,BusinessPlan, DeletedBusiness, DeletedEscort;
use Business, EscortBusiness, User, Suburb, Age, Height, Body, EscortService, EscortLanguage, Service, Language, EscortAvailability, EscortRate, EscortImage, App;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;

class BusinessController extends Controller {

  protected $userRepo;

  protected $suburbRepo;

  function __construct(UserRepository $userRepo, SuburbRepository $suburbRepo) {
    $this->userRepo = $userRepo;
    $this->suburbRepo = $suburbRepo;
  }

  public function businessIndex()
  {
    $plans = BusinessPlan::where('type','=','2')->get();
    return View::make('admin.business.index',compact('plans'));
  }

  public function businessAjax()
  {
    $query = Business::select('*');
    return Datatables::of($query)
      ->editColumn('name','<a href="/admin/businesses/edit/{{$id}}">{{$name}}</a>')
      ->editColumn('suburb_id','{{ $state }}')
      ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
      ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->editColumn('current_plan',function($business){
           $user = User::where('id','=',$business->user_id )->first();
           $userPlan = 'No User';
            if ( is_object ( $user ) ){
              $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
              if ( $plan )
                 $userPlan = $plan->name;
              else if ( $user->id != 1 )
                 $userPlan = 'Restricted';
            }
            else {
              $user = new User;
            }
            return '<div id ="userPlan_' . $business->user_id .  '">' . $userPlan . '</div>';
         })
      ->editColumn('profiles',function($business){
            $user = User::where('id','=', $business->user_id )->first();
            $userPlan = 'No User';
            $x = $business->escortsCount();
            if ( is_object ( $user ) ){
              $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
              if ( $plan )
                 $userPlan = 'Used ' . $x . ' of <span id = "num_' . $business->user_id  . '">' . $plan->escort_profiles . '</span>';
            }
            return $userPlan;
      })
            ->addColumn('action',function($business){
              $plan = '';
              $userPlan = 'No User';
              $user = User::where('id','=',$business->user_id )->first();
              if ( is_object( $user ) ){
                 $userPlan = BusinessPlan::where('id','=',$user->business_plan )->first();
                 if ( $userPlan ){ $userPlan = $userPlan->id; }

                     $plan ='<li data-id =\"' . $business->user_id . '\" data-currentplan = \"' . $user->business_plan . '\" ><a class = \"popupBusinessPlans\">Change Plan</a></li>';
              }
              else {
                 $user = new User;
              }
              return '<div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$business->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Action
                <span class="caret"></span>
              </button>
              <ul data-business-id="'.$business->id.'" class="dropdown-menu" aria-labelledby="actions-'.$business->id.'">
                <li><a class="edit-profile" target =\"_blank\" href="/user/profile/?type=business&id='.$business->id.'">Edit</a></li>
                <li><a class="notes" href="#">Notes</a></li>
                <li><a class="delete-profile" data-id ="'.$business->id.'">Delete</a></li>
                <li data-id ="' . $business->user_id . '" data-currentplan ="' . $userPlan  . '"><a class="popupBusinessPlans">Change Plan</a></li>
              </ul>
            </div>';
            })
      ->make(true);
  }
    public function ajax_pending_businesses()
  {
    $query = Business::where('status','=','1' )->select('*');
    return Datatables::of($query)
      ->editColumn('name','<a href="/admin/businesses/edit/{{$id}}">{{$name}}</a>')
      ->editColumn('suburb_id','')
      ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
      ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
           ->addColumn('action', function ($business) {
                return '<a href="#" data-escort-id="'.$business->id.'" class="btn btn-xs btn-success approve"><i class="fa fa-check"></i> Approve</a>
                    <a href="#" data-escort-id="'.$business->id.'" class="btn btn-xs btn-danger disapprove"><i class="fa fa-ban"></i> Disapprove</a>
                  <a target =\"_blank\" href ="/user/profile/?type=business&id='.$business->id.'">Edit Profile</a>';
            })
      ->make(true);
  }
  public function pending_businesses() {
   return View::make('admin.business.pending');
  }

  public function plans() {
    $plans = BusinessPlan::select('*')->get();
    return View::make('admin.business.plans',compact('plans'));
  }
  public function newplan() {
    $input = Input::all();
    $plan = new BusinessPlan;
    foreach ( $input as $key => $value ){

      if ( $key != '_token' && $key != 'id' )  {
          $plan->$key = $value;
      }
    }
    $plan->save();
    return $plan;
    //return $plan;
  }
  public function changePlan() {
    $input = Input::all();
    $user = User::where('id','=',$input['user_id'])->first();
    $user->business_plan = $input['business_plan'];
    $plan = BusinessPlan::where('id','=',$input['business_plan'] )->first();
    $user->save();
    $data['full_name'] = $user->full_name;
    $data['email'] = ( !App::environment('development') ? $user->email : Config::get('constants.DEV_EMAIL') );

        Mail::send('emails.account-suspended', ['data' => $data,'user'=>$user], function ($message) use($user) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('Your Myplaymate Account has been suspended')
                    ->to($user->email);
        });

    if ( $plan->type == 1 )
         return $plan->name;
    return $plan;
  }
  public function editplan() {
    $input = Input::all();
    $plan = BusinessPlan::where('id','=', $input['id'] )->first();
    foreach ( $input as $key => $value ){

      if ( $key != '_token' && $key != 'id' )  {
          $plan->$key = $value;
      }
    }
    $plan->save();
    return $plan;
    //return $plan;
  }

  public function getNewBusiness()
  {
    $suburbs = $this->suburbRepo->populateSelect();
    $users = $this->userRepo->populateSelect();

    return View::make('admin.business.new',compact('suburbs','users'));
  }

  public function postNewBusiness()
  {
    $input = Input::all();
    $business = new Business;
    $business->type = $input['type'];
    $business->name = $input['name'];
    $business->user_id = $input['user_id'];
    $business->phone = $input['phone'];
    $business->address_line_1 = $input['address_line_1'];
    $business->address_line_2 = $input['address_line_2'];
    $business->email = $input['email'];
    $business->suburb_id = $input['suburb_id'];
    $business->save();

    return Redirect::to('/admin/businesses/edit/'.$business->id)
      ->with('message','Business created!');
  }

  public function getEdit($id)
  {
    $message = Session::get('message');
    $business = Business::find($id);
    $business_escorts = $business->escorts;

    $suburbs = $this->suburbRepo->populateSelect();
    $users = $this->userRepo->populateSelect();

    return View::make('admin.business.single',compact('business','business_escorts','suburbs','users','message'));
  }
  public function approveBusiness()
  {
    $business = Business::find(Input::get('escort_id'));
    $business->status = 2;
    $business->approved_by = Auth::user()->id;
    $business->approved_at = date('Y-m-d H:i:s');
    $business->bumpup_date = Carbon::now();
    $business->save();

      $data['full_name'] = $business->name;
      $data['email'] = ( !App::environment('development') ? $business->email : Config::get('constants.DEV_EMAIL') );
      $emailTemplate = EmailTemplate::where('name', 'escort-approved')->first();

        Mail::send('emails.business-approved', ['data' => $data,'business'=>$business], function ($message) use($business) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Business Profile Approved')
                    ->to($business->email);
        });
  }

  public function disapproveBusiness()
  {
    $business = Business::find(Input::get('escort_id'));
    $business->status = 3;
   // $business->disapproved_by = Auth::user()->id;
  //  $business->disapproved_at = date('Y-m-d H:i:s');
    $business->save();

    /*    $data['full_name'] = $escort->escort_name;
        $data['email'] = $escort->email;
        $emailTemplate = EmailTemplate::where('name', 'escort-disapproved')->first();

        Mail::send('emails.escort-disapproved', ['data' => $data,'escort'=>$escort], function ($message) use($escort) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Escort Profile Dispproved')
                    ->to($escort->email);
        });*/
  }

  public function postEdit()
  {
    $input = Input::all();
    $business = Business::find($input['business_id']);
    $business->type = $input['type'];
    $business->name = $input['name'];
    $business->user_id = $input['user_id'];
    $business->phone = $input['phone'];
    $business->address_line_1 = $input['address_line_1'];
    $business->address_line_2 = $input['address_line_2'];
    $business->email = $input['email'];
    $business->suburb_id = $input['suburb_id'];
    $business->save();

    return Redirect::to('/admin/businesses/edit/'.$business->id)
      ->with('message','Business updated!');
  }

  public function getLogo($id)
  {
    $business = Business::find($id);

    return View::make('admin.business.logo',compact('business'));
  }

  public function uploadLogo($id)
  {
    $file = Input::file('file');
    $destinationPath = public_path().'/uploads/businesses/';
    $filename = str_random(12).'.jpg';
    $file->move($destinationPath, $filename);

    $business = Business::find($id);

    if ($business->image != '') {
      $old_image = public_path().'/uploads/businesses/'.$business->image;
      if (file_exists($old_image))
        unlink($old_image);
    }

    $business->image = $filename;
    $business->save();
  }

  public function businessEscortsAjax()
  {
    $query = Business::find(Input::get('business_id'))->escorts();
    return Datatables::of($query)
      ->editColumn('escort_name',function($escort){
        return '<a href="/escorts/'.$escort->seo_url.'" target="_blank">'.$escort->escort_name.'</a>';
      })
      ->editColumn('status','{{ Helpers::get_status($status) }}')
      ->editColumn('suburb_id','{{ $suburb_id && Suburb::find($suburb_id)->state->short_name }}')
      ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
      ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->editColumn('bumpup_date','<span class="bumpup-date">{{ Helpers::db_date_to_user_datetime_or_empty($bumpup_date) }}</span>')
            ->addColumn('action',function($escort){
              return '<div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$escort->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Action
                <span class="caret"></span>
              </button>
              <ul data-escort-id="'.$escort->id.'" class="dropdown-menu" aria-labelledby="actions-'.$escort->id.'">
                <li><a class="edit-profile" href="/admin/escorts/info/'.$escort->id.'">Edit profile</a></li>
                <li><a data-escort-id="'.$escort->id.'" class="approve" href="#">Approve</a></li>
                <li><a data-escort-id="'.$escort->id.'" class="disapprove" href="#">Disapprove</a></li>
                <li><a data-escort-id="'.$escort->id.'" class="delete" href="#">Delete</a></li>
                <li><a data-escort-id="'.$escort->id.'" class="bumpup" href="#">Bump up</a></li>
                <li><a class="notes" href="#">Notes</a></li>
              </ul>
            </div>';
            })
      ->make(true);
  }
  public function deletedBusinesses(){
     $deletedBusinesses = DeletedBusiness::get();
     return View::make('admin.business.deleted',compact('deletedBusinesses'));
  }
  public function deleteBusiness($id) {
    $escorts = EscortBusiness::select('*')->where('business_id', '=', $id )->get();
    if ( $escorts->isEmpty() ) {
        $business = Business::find($id);
        $deleted = new DeletedBusiness;
        $deleted->business_id = $id;
        $deleted->business_name = $business->name;
        $deleted->business_type = $business->type;
        $deleted->deleted_by = Auth::user()->id;
        $deleted->save();
        $business->delete();

          echo 'Business deleted';
    }
    else {
          echo 'This business currently has escorts.  You must delete all escorts before deleting the business.';
    }
  }
}
