<div id="backend-profile-gallery" class="alignc" >
    <h2>LOGO</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="triggerAnimation animated" data-animate="fadeInUp">
            <p class="profile_info">Drop your file here or click on the button below to upload your logo/image. The image needs to be in a landscape format and at least 800px wide for an optimum impact. Please note that this will replace your old banner, if you have one.</p>
                {{ Form::open(array('files'=>true,'class'=>'dropzone','id'=>'businessLogo')) }}
                {{ Form::hidden('business_id',$business->id) }}
                {{ Form::close() }}

                <div class="alignc section-content" style="clear:both">
                    <a href="" id="continue" class="submit-button">SAVE AND CONTINUE</a>
                </div>

                <br /><br />
            </div><!-- .triggerAnimation.animated end -->
        </div><!-- .col-md-12 end -->
        <br /><br />
    </div><!-- .row end -->
</div><!-- End of Gallery-->

<script>
    (function($){
        $('.second-level h2').text("{{ $business->name }}");

        var escort_id     = $('.escort-level').data('escort-id'),
            myEscortsIcon = businessLevel.find('.business-my-escorts');

        $('#continue').on('click', function(e) {
            e.preventDefault();
            activateIcon(myEscortsIcon,2);
            refreshForm('businessMyEscorts','business','',{{ $business->id }});
        });  
    })(jQuery);
</script>