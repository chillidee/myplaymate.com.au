<?php
namespace Admin;
use Controller, View, Helpers, Datatables, Input, Validator,ImageHelper,Config;
use Page;

class PageController extends Controller {
  public function index()
  {
    return View::make('admin.pages.index');
  }
  public function setMainImage() {
  }
  public function ajax()
  {
    $query = Page::select('*');
    return Datatables::of($query)
    ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
    ->editColumn('updated_at','{{ Helpers::db_date_to_user_date($updated_at) }}')
    ->addColumn('action',function($page){
      return '<div class="dropdown">
      <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$page->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Action
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="actions-'.$page->id.'">
        <li><a class="edit-profile" href="/admin/pages/edit/'.$page->id.'">Edit</a></li>
        <li><a class="notes" href="#">Notes</a></li>
      </ul>
    </div>';
  })
    ->make(true);
  }

  public function getNew()
  {
    return View::make('admin.pages.create');
  }
  public function postNew()
  {
    $input = Input::all();
    $message = 'Page created!';
    $page = new Page;

    if (isset( $input->main_image ) ) {
      $rules = array(
       'file' => 'image|mime:jpg,gif,png|max:3000',
       );

      $validation = Validator::make($input, $rules);

      if ($validation->fails())
      {
        return Response::make($validation->errors->first(), 400);
      }

      $destinationPath = public_path().'/temp/'; // upload path
      $extension = Input::file('main_image')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      Input::file('main_image')->move($destinationPath, $fileName);
      $message = 'Page updated!';
      ImageHelper::s3Upload( $fileName, '/posts' );
      $page->main_image = 'posts/' . $fileName;
    }

    $page->name = $input['name'];
    $page->meta_title = $input['meta_title'];
    $page->meta_keywords = $input['meta_keywords'];
    $page->meta_description = $input['meta_description'];
    if(isset($input['top_content'])):
      $page->top_content = $input['top_content'];
    endif;
    $page->content = $input['content'];
    $page->url_key = $input['url'];
    $page->img_alt = $input['img_alt'];
    $page->published = $input['published'];
    $page->save();

    return View::make('admin.pages.index',compact('message'));
  }

  public function getEdit($id)
  {
    $page = Page::find($id);
    return View::make('admin.pages.single',compact('page'));
  }
  public function postEdit($id)
  {
    $input = Input::all();
    $page = Page::find($id);
    if ( null !== Input::file('main_image') ) {
      $rules = array(
       'file' => 'image|mime:jpg,gif,png|max:640000',
       );
      $validation = Validator::make($input, $rules);

      if ($validation->fails())
      {
        return Response::make($validation->errors->first(), 400);
      }

      $destinationPath = public_path().'/temp/'; // upload path
      $extension = Input::file('main_image')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      Input::file('main_image')->move($destinationPath, $fileName);
      $message = 'Page updated!';
      ImageHelper::s3Upload( $fileName ,'posts/');
      $page->main_image = 'posts/' .$fileName;
    }

    // $destinationPath = '/home/admin/web/dev.myplaymate.com.au/public_html/assets/frontend/img/'.str_random(8);
    // $filename = $file->getClientOriginalName();
    // $filename = $file['name'];
        //$extension =$file->getClientOriginalExtension(); //if you need extension of the file
    // $uploadSuccess = Input::file('main_image')->move($destinationPath, $filename);

    $page->name = $input['name'];
    $page->meta_title = $input['meta_title'];
    $page->meta_keywords = $input['meta_keywords'];
    $page->meta_description = $input['meta_description'];
    $page->top_content = $input['top_content'];
    $page->content = $input['content'];
    $page->url_key = $input['url'];
    $page->img_alt = $input['img_alt'];
    $page->published = $input['published'];
    $page->template = $input['template'];
    $page->save();
    return View::make('admin.pages.single',compact('page','message'));
  }
}
