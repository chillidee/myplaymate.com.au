<li>

    <br/>

    <div class="hidden-sm hidden-xs widget_featured grid-view">
        <h5>IN THE SPOTLIGHT</h5>

        <div id="owl-sidebar">
            @foreach($featuredEscorts as $escort)
                <div class="col-md-3 col-sm-3 col-xs-6 owl-item escorts-wishlist">
                    <div class="escorts-img-container" id="{{ $escort->id }}">
                        @if(Auth::check())
                            <div class="figfavorite">
                                <a href="" class="is-not-wish">
                                    <i class="fa fa-heart"></i>
                                </a>

                                <a href="" class="is-wish">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        @else
                            <div class="figfavorite">
                                <a href="{{url('register')}}">
                                    <i class="fa fa-heart"></i>
                                </a>
                            </div>
                        @endif
                        <div class="info-container"><!-- info-container-->
                            <div class="figinfo"><!-- figinfo-->
                                <span class="figname">{{ $escort->escort_name }} </span>
                                <ul class="figdata">                                 
                                    <li>{{ $escort->suburb_name }}</li>                             
                                </ul>
                            </div>
                            <!-- figinfo-->
                        </div>
                        <!-- end of info-container-->
                        <a href="/escorts/{{ $escort->seo_url }}">
                            @if($escort->main_image_id != 0 && $escort->mainImage)
                                <img class="lazyOwl" data-src="{{ $escort->mainImage->thumbnail(220,330) }}" src="{{ $escort->mainImage->thumbnail(220,330) }}" alt="{{ $escort->escort_name }}">
                            @else
                                <img src="/assets/frontend/img/profile_template.jpg">
                            @endif
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
      <div class="hidden-sm hidden-xs widget extra-top">
        <h5>Navigation</h5>
        <ul>
            <li><a href="/about">About Us</a></li>
            <li><a href="/faqs">FAQs</a></li>
        </ul>
    </div>
    <!-- .widget_Navigation end -->
</li>
















