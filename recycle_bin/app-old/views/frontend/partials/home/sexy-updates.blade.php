<!-- Sexy Update -->
<div class="page-content" id="sexyupdates">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobile-escort">
                <h2>SEXY UPDATES</h2>
                <div class="divider"></div>

                <div id="recent_comments" class="sexy-updates clearfix container-fluid">
                    <div class="row">
						@foreach($sexyUpdates as $update)
				        <div class="col-md-4 col-sm-4">
					        <figure class="escorts-img-container" id="{{ $update->escort->id }}">
                                <div class="col-md-4 col-xs-4">
                                    <a href="/escorts/{{ $update->escort->seo_url }}">
                                        <img src="{{ $update->escort->mainImage ? $update->escort->mainImage->thumbnail(220,330) : '/assets/frontend/img/profile_template.jpg' }}" alt="{{ $update->escort->escort_name }}">
                                    </a>
                                </div>
                                <div class="col-md-8 col-xs-8">
                                    <div class="figinfo">
                                        <a href="/escorts/{{ $update->escort->seo_url }}">
                                            <span class="figname">
                                                {{ $update->escort->escort_name }}
                                            </span>
                                        </a>
                                        <div class="figdesc">
                                            <p>{{ $update->message }}</p>
                                        </div>
                                    </div>
                                </div>
					        </figure>
					    </div>
					    @endforeach
                    </div>
                </div>
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content.parallax end -->
