<?php

class AdvertiseController extends Controller {

	public function getAdvertise()
	{
		$message = Session::get('message');
		$meta = Page::where('url_key','=','advertise')->first();
		return View::make('frontend.advertise',compact('meta'))
			->with('message',$message);
	}

}