<?php
namespace Admin;

use Controller, Datatables, View, Input;
use EscortUpdate;
class UpdateController extends Controller {

	public function index()
	{
		$updates = EscortUpdate::all();
		return View::make('admin.updates.index',compact('updates'));
	}

	public function ajax()
	{
		$query = EscortUpdate::select('*');
		return Datatables::of($query)
			->editColumn('escort_id','{{ Escort::find($escort_id)->escort_name }}')
			->editColumn('status','{{ Helpers::get_status($status) }}')
			->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
			->addColumn('action', function ($update) {
                return '<a href="#" data-update-id="'.$update->id.'" class="btn btn-xs btn-success approve"><i class="fa fa-check"></i> Approve</a>
                		<a href="#" data-update-id="'.$update->id.'" class="btn btn-xs btn-danger disapprove"><i class="fa fa-ban"></i> Disapprove</a>';
            })
			->make(true);
	}

	public function approve()
	{
		$update = EscortUpdate::find(Input::get('update_id'));
		$update->status = 2;
		$update->save();
	}

	public function disapprove()
	{
		$update = EscortUpdate::find(Input::get('update_id'));
		$update->status = 3;
		$update->save();
	}

}