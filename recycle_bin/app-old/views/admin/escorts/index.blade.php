@extends('admin.layout')
@section('javascript')
	<script>
		jQuery(document).ready(function() {

		    oTable = jQuery('#datatable').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "/admin/ajax_escorts",
				lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
		        columns: [
		            {data: 'id', name: 'id', 'width':'45px'},
		            {data: 'escort_name', name: 'escort_name'},
		            {data: 'status', name: 'status'},
		            {data: 'suburb_id', name: 'suburb_id'},
		            {data: 'phone', name: 'phone'},
		            {data: 'email', name: 'email'},
		            {data: 'count_views', name: 'count_views'},
		            {data: 'count_phone', name: 'count_phone'},
		            {data: 'count_playmail', name: 'count_playmail'},
		            {data: 'created_at', name:'created_at'},
		            {data: 'bumpup_date', name:'bumpup_date'},
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ],
				initComplete: function () {
		            this.api().columns().every(function () {
		                var column = this;
		                var input = document.createElement("input");
		                $(input).appendTo($(column.footer()).empty())
		                .on('keyup', function () {
		                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

		                    column.search(val ? val : '', true, false).draw();
		                });
		            });
		            $('tfoot input').first().css('width','45px');
		            $('tfoot input').eq(2).css('width','60px');
		            $('tfoot input').eq(3).css('width','45px');
		            $('tfoot input').eq(4).css('width','80px');
		            $('tfoot input').eq(6).css('width','40px');
		            $('tfoot input').eq(7).css('width','40px');
		            $('tfoot input').eq(8).css('width','40px');
		            $('tfoot input').eq(9).css('width','75px');
		            $('tfoot input').eq(10).css('width','60px');
		            $('tfoot input').eq(12).remove();
		    	}
		    });

		    jQuery('.rightpanel').on('click', '.approve', approveEscort);

		    jQuery('.rightpanel').on('click', '.disapprove', disapproveEscort);

		    jQuery('.rightpanel').on('click', '.delete', deleteEscort);

		    jQuery('.rightpanel').on('click', '.bumpup', bumpup);
		});
	</script>
@stop

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>All escorts</h1>
    </div>
</div><!--pageheader-->

<div class="row">
	<a href="/admin/escorts/new" class="create-user btn btn-success btn-lg pull-right section-content">Create escort</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Escort name</th>
			<th>Status</th>
			<th>State</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Views</th>
			<th>Phone count</th>
			<th>Playmail count</th>
			<th>Date of creation</th>
			<th>Last bump up</th>
			<th>Action</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tfoot>
</table>

@stop



