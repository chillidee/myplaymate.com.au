<div class ='grey_back'>
   <div class="row escorts-filters">
      <section id="page-title" class="page-title-1">
         <div class="container">
            <p>Our Featured escorts are Available</p>
         </div>
         <!-- .containr end -->
      </section>
   </div>
</div>
<section class="page-wrapper section-content" id = "featured_escorts">
   <!-- .container start -->
   <div class="container">
      <!-- .row start -->
      <div class="row escorts-items-holder triggerAnimation animated grid-view">
         <div id="directory-container" class="col-xs-12">
            <div>
               <ul class="escort-container grid-view">
                  <li class="grid-template col-md-2 col-sm-4 col-ms-6 col-xs-12 escorts-wishlist">
                     <figure class="escorts-img-container">
                        <div class="figfavorite">
                           <a href="" class="is-not-wish">
                              <i class="fa fa-heart"></i>
                           </a>
                           <a href="" class="is-wish">
                              <i class="fa fa-times"></i>
                           </a>
                        </div>
                        <div class="info-container">
                           <div class="figinfo">
                              <span class="figname"></span>
                              <ul class="figdata">
                                 <li class="escort-location">
                                   <span class="business-suburb"></span>
                                    @if(isset($city))
                                    <span class="escort-distance"> km from {{ ucfirst($city) }}</span>
                                 </li>
                                 @else
                                 <span class="escort-distance"> km</span></li>
                                 @endif
                              </li>
                           </ul>
                        </div>
                        <!-- figinfo-->
                     </div>
                     <!-- end of info-container-->
                     <a href="{{ url('escorts/') }}">
                      <img class="img-profile" src="" alt="">
                   </a>
                </figure>
             </li>
          </ul>
          <div class="spinner">
               <div class="dot1"></div>
               <div class="dot2"></div>
         </div>
      </div>
   </div><!-- directory -->
</div><!--directory container -->
</div><!-- .row end -->
</div><!-- .container end -->
</div>
</section>
<!-- End of the Boxed Version Page -->
