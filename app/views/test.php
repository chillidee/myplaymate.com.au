<!DOCTYPE html>
<html>
<head>
  <title>$templateCache</title>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-beta.5/angular.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-beta.5/angular-route.min.js"></script>
</head>
<body ng-app="app">
  <div ng-view></div>
  
<script>
function TestCtrl() {
  this.user = {name: 'Blake'};
}

angular.module('app', ['ngRoute'])
.config(function($routeProvider){
  $routeProvider.when('/', {
    controller: 'TestCtrl as test',
    templateUrl: '/test2'
  })
  .otherwise('/');
})
.controller('TestCtrl', TestCtrl);
</script>

  </body>
<?php /*
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
        <script src="/assets/dashboard/js/app.js"></script>
        <script src="/assets/dashboard/js/services.js"></script>
        <script src="/assets/dashboard/js/controller.js"></script>
  */
  ?>
</html>