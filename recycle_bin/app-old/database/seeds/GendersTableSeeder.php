<?php

class GendersTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('genders')->truncate();

        $Genders = [
            ['id' => 1,
                'name' => 'Female', ],
            ['id' => 2,
                'name' => 'Male', ],
            ['id' => 3,
                'name' => 'Transexual', ],
            ];

        // Uncomment the below to run the seeder
        DB::table('genders')->insert($Genders);
    }
}
