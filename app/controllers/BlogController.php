<?php
use MyPlaymate\Services\Meta;

class BlogController extends Controller {

  public function index()
  {
    $meta = Page::where('url_key','=','blog')->first();

    $posts = Post::orderBy('date_from','DESC')
           ->wherePublished(1)
           ->where('date_from','<=',date('Y-m-d'))
           ->where('date_to','>=',date('Y-m-d'))
           ->get();

    return View::make('frontend.blog.index',compact('posts','meta'));
  }

  public function show($slug)
  {
    $count = Post::count();
    $post = Post::whereSeoUrl($slug)->where( 'ID', '!=', $count )->first();
    if (!$post)
    {
      return Redirect::to('/blog');
    }

    $meta = new Meta($post->meta_title,$post->meta_keywords,$post->meta_description);
    $comments = Comment::wherePostId($post->id)
               ->whereStatus(2)
               ->orderBy('created_at','DESC')
               ->get();

    return View::make('frontend.blog.show',compact('post','comments','meta'));
  }

  public function sendComment()
  {
    $input = Input::all();

    $comment = new Comment();
    /*$comment->author = $input['author'];
    $comment->comment = $input['comment'];
    $comment->post_id = $input['post_id'];
    $comment->status = 1;
    $comment->save();*/

    //return 'Comment saved';
	return 'Comments temporarily disabled';
  }
}
