@extends('admin.layout')
@section('javascript')
  <script>
    jQuery(document).ready(function() {
      /* $( '#export_escort_data_form' ).on( 'submit', function(e) {
         e.preventDefault();
         $.ajax({
                  url: "/admin/export_escort_data",
                  data: $( this ).serialize()
               }).done(function(result) {
                 console.log( result );
               });
    });*/
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax_cityPages",
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'meta_title', name: 'meta_title'},
                {data: 'meta_description', name: 'meta_description'},
                {data: 'created_at', name:'created_at'  },
                {data: 'updated_at', name:'updated_at'  },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
  </script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-file-text-o"></span></div>
    <div class="pagetitle">
        <h1>Export data</h1>
    </div>
  <form id ='export_escort_data_form' action ='/admin/export_escort_data'>
  <div class ='info_holder'>
    <span class ='info'>Type of Export:</span><label>Spreadsheet</label><input type ='radio' name ='export_type' value ='spreadsheet' checked><label>Mobile list</label><input type ='radio' name ='export_type' value ='mobile_list'>
    </div>
   
<div class="row">
<div id ='_status_filter_div'>
  <span class ='info'>Filter by status:</span>
      <label for=""><input type="checkbox" name="status[]" class="status" id="Approved" value="2" checked> Approved</label>
       <label for=""><input type="checkbox" name="status[]" class="status" id="Disapproved" value="3"> Disapproved</label>
       <label for=""><input type="checkbox" name="status[]" class="status" id="Pending" value="1"> Pending</label>
       <label for=""><input type="checkbox" name="status[]" class="status" id="Unavailable" value="4"> Unavailable</label>
</div>
    <input type ='submit' class="form_submit_button" value ='Export Now'>
</div><!--pageheader-->
 <style>
   .info_holder {
     margin-bottom: 15px;
   }
   .note {margin-left: 25px;}
   .form_submit_button {
    background-color: #007EFF;
    color: #fff;
    font-size: 26px;
    padding: 12px 26px;
    border-radius: 6px;
    border-left: 35%;
    margin-left: 56%;
    display: block;
    width: 201px;
    text-align: center;
   }
   .escort-table-filters {
     margin-top: 15px;
   }
   input[type='radio'] {
    background-color: #ccc;
    -webkit-appearance: none;
    width: 20px;
    height: 20px;
    background: #F9F9F9;
    border-radius: 50%;
    border: 1px solid #333131;
    margin-top: 5px;
    margin-bottom: -3px;
    margin-right: 20px;
    margin-left: 5px;
}
 input[type='radio']:checked {
    background-color: #171719;
    border: 5px ridge #969A99;
   }
   label, h5 {
    text-transform: none;
    font-weight: 400;
}
  #export_escort_data_form {
    margin-top: 30px;
    border-top: 1px solid #CCC6C6;
    padding-top: 25px;
  }
    input[type='checkbox']:checked {
    background: #86D628;
  }
input[type='checkbox'] {
    -webkit-appearance: none;
    width: 20px;
    height: 20px;
    background: white;
    border-radius: 5px;
    border: 1px solid #333131;
    margin-top: 5px;
    margin-bottom: -3px;
}
  .datepicker {
    border: 1px solid #5A5454;
    border-radius: 3px;
    padding-left: 10px;
    height: 30px;
}
  .ui-datepicker-calendar td {
    width:30px;
    height: 28px;
  }
  .ui-state-active {
    background-color: green;
  }
  ._dt_search_title {
    clear:right;
  }
  #_dt_search, ._dt_search_title {
    float:right;
    margin-top: 14px;
    margin-bottom: 25px;
  }
  #_dt_search_button {
        float: right;
    margin: 0;
    z-index: 1000;
    position: relative;
    right: 0;
    margin-right: 0;
    background-color: #0866C6;
    color: #fff;
    font-size: 24px;
    border: none;
    padding: 6px 13px;
    border-radius: 3px;
  }
  #_dt_search {
    display: block;
    font-size: 18px;
    margin-right: 0;
    height: 32px;
    padding-left: 10px;
  }
  #datatable_filter {
    display:none;
  }
  #state_col {
    width: 35px;
  }
  #datatable th:nth-child(5) {
    min-width: 80px;
  }
  #_status_filter_div label {
        font-size: 1.2em;
    text-transform: none;
    font-weight: bold;
    margin: .3em 0;
    font-weight: 400;
    margin-right: 15px;
  }
  #_status_filter_div {
    margin-top: 35px;
    width: 67%;
  }
  .info {
    color: #0E597D;
    font-size: 22px;
    margin-right: 20px;
  }
  .label-unavailable {
        background-color: #D74EF0;
  }
  </style>

@stop
