@extends('admin.layout')
@section('content')

    <div class="pagetitle">
        <h1>Deleted Businesses</h1>
    </div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Type</th>
      <th>Deleted by</th>
      <th>Deleted date</th>
    </tr>
  </thead>
  <tbody>
  @foreach( $deletedBusinesses as $business )
 <?php  
 $admin = User::where('id','=',$business->deleted_by )->first();
 $user = 'Unknown';
 if ( $admin )
     $user = $admin->full_name;
 switch ($business->business_type) {
    case 1:
        $type = 'Brothel';
        break;
    case 2:
        $type = 'Agency';
        break;
    case 3:
        $type = 'Masage Parlour';
        break;
    default:
       $type = 'Unknown';
}?>
  <tr> 
      <td>{{$business->id}}</td>
      <td>{{$business->business_name}}</td>
      <td>{{$type}}</td>
      <td>{{$user}}</td>
      <td>{{$business->created_at }}</td>
  </tr>
  @endforeach
  </tbody>
    <tfoot>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Type</th>
      <th>Deleted by</th>
      <th>Deleted date</th>
    </tr>
  </tfoot>
</table>

<style>
#popupForm{
    position: fixed;
    background: #fff;
    width: 600px;
    z-index: 10000;
    border-radius: 5px;
    border: 1px solid #ccc;
    padding: 50px;
    left: 50%;
    margin-left: -300px;
    top: 20px;
    display:none;
  }
#popupForm select {
    height: 32px;
    background: #fff;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 3px 12px;
}
#popupForm textarea {
    width: 100%;
    height: 158px;
    border: 1px soid #ccc;
    border: 1px solid #ccc;
    border-radius: 3px;
}
#popupForm input {
    width: 100%;
    border-radius: 3px;
    border: 1px solid #ccc;
    height: 32px;
    margin: 10px 0;
    padding: 0 19px;
  }
#popupForm label {
    font-size: 1.2em;
    font-weight: bold;
    margin: .3em 0;
    color: #384F5D;
    text-transform:none;
    display: block;
  }
#popupForm h3{
    color: #AD4949;
    text-transform: uppercase;
    margin-bottom: 20px;
  }
.mpBut, .mpButt {
    float: right;
    margin-top: 35px;
    background-color: #3CAF40;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 8px 48px;
    font-size: 22px;
    margin-bottom: 35px;
  }
.mpButt {
  margin: 0;
  font-size: 18px;
  padding: 2px 18px;
  background-color: #317177;
}
#overlay {
   opacity: .7;
   position:fixed;
   left:0;
   right:0;
   bottom:0;
   top:0;
   width: 100%;
   height: 100%;
   background-color: #000;
   z-index: 9999;
   display:none;
 }
#popupForm .busHidden { display:none; }
  </style>
  @stop
  @section('javascript')
  <script>
$(document).ready(function() {
    $('.admin-table').DataTable();
} );
</script>
@stop




