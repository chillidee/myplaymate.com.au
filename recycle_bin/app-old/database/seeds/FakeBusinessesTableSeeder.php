<?php

class FakeBusinessesTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,8) as $index) {
            Business::create([
                'name'           => $faker->company,
                'type'           => $faker->numberBetween(1,3),
                'user_id'        => $faker->numberBetween(1,5),
                'email'          => $faker->email,
                'address_line_1' => $faker->streetAddress(),
                'address_line_2' => $faker->city(),
                'suburb_id'      => $faker->numberBetween(1,13000),
                'status'         => 2
            ]);
        }
    }
}
