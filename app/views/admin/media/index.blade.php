@extends('admin.layout')
@section('content')
<div class ='overlay loadingDiv'></div>
<div class ='loader loadingDiv'></div>
<div id="overlay"></div>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-picture-o"></span></div>
    <div class="pagetitle">
        <h1>Media Library</h1>
    </div>
</div><!--pageheader-->

<div id="popupForm">
<h3 id="descriptor">Add Media</h3>
<p>Please set alt tags for display on the front end here.</p>
    <form id="myform">
        <input name="_token" type="hidden" value="">

        <label for="Name">Image Alt Tag</label>
        <input name="img_alt" type="text" value="" id ='imgAlt'>

        <div class ='imgPreview'></div>
        <input type='file' id="imgInp" class ='hidden'>
        <img id="imagePreview" src="#" alt="your image">
        <button id ='uploadMediaButton' class ='switch_layouts centeredButton'>Upload Image</button>



        <button class ='switch_layouts centeredButton' id ='saveMedia'>Save</button>

        </form>
</div>

<button class ='float_right switch_layouts'>Add Media</button>

@if ( isset( $media ) )
   @foreach ( $media as $image )
      <div class ='col-md-2'>
         <img src ='/uploads/media/{{$image->file}}'>
         <div>SRC: '/uploads/media/{{$image->file}}'</div>
      </div>
   @endforeach
@endif

<style>
.col-md-2 img { width: 100%;  }
#imagePreview { display:none; }
#saveMedia { display:block; }
#popupForm{
    position: fixed;
    background: #fff;
    width: 600px;
    z-index: 10000;
    border-radius: 5px;
    border: 1px solid #ccc;
    padding: 50px;
    left: 50%;
    margin-left: -300px;
    top: 20px;
    display:none;
  }
#popupForm select {
    height: 32px;
    background: #fff;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 3px 12px;
}
#popupForm textarea {
    width: 100%;
    height: 158px;
    border: 1px soid #ccc;
    border: 1px solid #ccc;
    border-radius: 3px;
}
#popupForm input {
    width: 100%;
    border-radius: 3px;
    border: 1px solid #ccc;
    height: 32px;
    margin: 10px 0;
    padding: 0 19px;
  }
#popupForm label {
    font-size: 1.2em;
    font-weight: bold;
    margin: .3em 0;
    color: #384F5D;
    text-transform:none;
    display: block;
  }
#popupForm h3{
    color: #AD4949;
    text-transform: uppercase;
    margin-bottom: 20px;
  }
.mpBut, .mpButt {
    float: right;
    margin-top: 35px;
    background-color: #3CAF40;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 8px 48px;
    font-size: 22px;
    margin-bottom: 35px;
  }
.mpButt {
  margin: 0;
  font-size: 18px;
  padding: 2px 18px;
  background-color: #317177;
}
#overlay {
   opacity: .7;
   position:fixed;
   left:0;
   right:0;
   bottom:0;
   top:0;
   width: 100%;
   height: 100%;
   background-color: #000;
   z-index: 9999;
   display:none;
 }
#popupForm .busHidden { display:none; }
#imagePreview {
    display:block;
    max-width: 70%;
    margin: 5px auto;
}
#popupForm { display:none; }
</style>

<script>
$( '#imagePreview').hide();
$( '#saveMedia' ).hide();
$( '.switch_layouts').on('click',function() {
      $( '#overlay').show();
      $( '#popupForm').show();
})
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagePreview').attr('src', e.target.result);
            $( '#imagePreview' ).show();
            window.sendMedia = {};
            window.sendMedia.image = e.target.result;
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
$( '#uploadMediaButton').on('click',function(e) {
     e.preventDefault();
     $( '#imgInp').click();
     $( '#uploadMediaButton').hide();
     $( '#saveMedia').show();
  })
$( '#myform').on('submit',function(e) {
  e.preventDefault();
  window.sendMedia.alt = $( '#imgAlt').val();
  $('.loadingDiv').show();
  $.post( "/admin/upload-media", window.sendMedia, function( data ) {
     location.reload();
  });
})
  $( '#overlay').on('click',function() {
  $( '#overlay').hide();
  $( '#popupForm' ).hide();
})
</script>


@stop