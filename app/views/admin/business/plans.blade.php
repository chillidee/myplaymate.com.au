@extends('admin.layout')
@section('content')

    <div class="pagetitle">
        <h1>Plans</h1>
    </div>
<div id ='overlay'></div>
<div id ='popupForm'>
<h3 id ='descriptor'>New plan</h3>
<p>Please note any plans created here will only be visible to admin.  If you would like to add a plan for users, please edit the ones already created.</p>
    <form id ='myform'>
        {{ Form::open(array('url' => 'foo/bar')) }}

        {{ Form::label('Name')}}
        {{ Form::text('name','',array('required'))}}
        {{ Form::text('id','',array('class'=>'hidden'))}}

        {{ Form::label('Description')}}
        {{ Form::textarea('description','',array('required'))}}

        <?php $types = array( '1'=>'Escort','2'=>'Business'); ?>

        {{ Form::label('Type')}}
        {{ Form::select('type',$types)}}

        <?php $touring = array( '0'=>'No','1'=>'Yes'); ?>

        {{ Form::label('Touring','',array('class'=>'escHidden'))}}
        {{ Form::select('touring',$touring, '', array('class'=>'escHidden') )}}

        {{ Form::label('Price','',array('required'))}}
        {{ Form::text('price','',array('required'))}}

        {{ Form::label('Number of Escort Profiles', '')}}
        {{ Form::text('escort_profiles', '1')}}

        {{ Form::label('Number of business Profiles', '', array('class'=>'busHidden'))}}
        {{ Form::text('business_profiles', '1', array('class'=>'busHidden'))}}

        {{ Form::label('Bumpups', '', array('class'=>'escHidden'))}}
        {{ Form::text('bumpups', '0', array('class'=>'escHidden'))}}


        <button id ='submit_newplan' class ='mpBut'>Submit</button>

        {{Form::close()}}
</div>

<div class="row">
  <button id ='openPop' class ='mpBut'>New Plan</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Price</th>
      <th>Type</th>
      <th>Admin Only</th>
      <th>Escort Profiles</th>
      <th>Business Profiles</th>
      <th>Touring</th>
      <th>Bumpups</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  @foreach( $plans as $plan )
  <?php $touring = ( $plan->touring == 1 ? 'Yes' : 'No');?>
  <?php $type = ( $plan->type == 1 ? 'Escort' : 'Business');?>
  <tr id = 'row_{{$plan->id}}'> 
      <td class ='name'>{{$plan->name}}</td>
      <td class ='description'>{{$plan->description}}</td>
      <td class ='price'>{{$plan->price}}</td>
      <td class ='type' data-type = '{{$plan->type}}'>{{$type}}</td>
      <td class ='type'>{{ ( $plan->useraccess == 1 ? "No" : "Yes") }}</td>
      <td class ='escort_profiles'>{{$plan->escort_profiles}}</td>
      <td class ='business_profiles'>{{$plan->business_profiles}}</td>
      <td class ='touring'>{{$touring}}</td>
      <td class ='bumpups'>{{$plan->bumpups}}</td>
      <td><button data-id ='{{$plan->id}}' class ='editPlan mpButt'>Edit</button></td>
  </tr>
  @endforeach
  </tbody>
</table>

<style>
#popupForm{
    position: fixed;
    background: #fff;
    width: 600px;
    z-index: 10000;
    border-radius: 5px;
    border: 1px solid #ccc;
    padding: 50px;
    left: 50%;
    margin-left: -300px;
    top: 20px;
    display:none;
  }
#popupForm select {
    height: 32px;
    background: #fff;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 3px 12px;
}
#popupForm textarea {
    width: 100%;
    height: 158px;
    border: 1px soid #ccc;
    border: 1px solid #ccc;
    border-radius: 3px;
}
#popupForm input {
    width: 100%;
    border-radius: 3px;
    border: 1px solid #ccc;
    height: 32px;
    margin: 10px 0;
    padding: 0 19px;
  }
#popupForm label {
    font-size: 1.2em;
    font-weight: bold;
    margin: .3em 0;
    color: #384F5D;
    text-transform:none;
    display: block;
  }
#popupForm h3{
    color: #AD4949;
    text-transform: uppercase;
    margin-bottom: 20px;
  }
.mpBut, .mpButt {
    float: right;
    margin-top: 35px;
    background-color: #3CAF40;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 8px 48px;
    font-size: 22px;
    margin-bottom: 35px;
  }
.mpButt {
  margin: 0;
  font-size: 18px;
  padding: 2px 18px;
  background-color: #317177;
}
#overlay {
   opacity: .7;
   position:fixed;
   left:0;
   right:0;
   bottom:0;
   top:0;
   width: 100%;
   height: 100%;
   background-color: #000;
   z-index: 9999;
   display:none;
 }
#popupForm .busHidden { display:none; }
  </style>
  @stop
  @section('javascript')
  <script>
  window.action = 'new';
  jQuery( document ).ready(function($) {
    $( '#overlay').on('click',function() {
        $( '#overlay').hide();
        $( '#popupForm').hide();
    })
    $( '#openPop').on('click',function() {
        window.action = 'new';
        $( '#descriptor').html('New Plan');
        $( '#overlay').show();
        $( '#popupForm').show();
        $( 'input').val( '' );
        $( 'textarea').val( '' );
    })
    $( '#myform').on('submit',function(e) {
      data = $( this ).serialize();
      e.preventDefault();
      if ( window.action == 'new'){
                     console.log( data );
          $.post( "/admin/newplan", data, function( data ) {
             touring = ( data.touring == 1 ? 'Yes' : 'No');
             type = ( data.type == 1 ? 'Escort' : 'Business');
             $( '#datatable').prepend('<tr data-row = ' + data.id + '><td class ="name">' + data.name + '</td><td class ="description">' + data.description + '</td><td class ="price">' + data.price + '</td><td class ="type">' + type + '</td><td class = "admin_only">Yes</td><td class ="escort_profiles">' + data.escort_profiles + '</td><td class ="business_profiles">' + data.business_profiles +'</td><td class ="touring">' + touring + '</td><td class ="bumpumps">' + data.bumpups + '</td><td><button data-id ="' + data.id + '" class ="editPlan mpButt">Edit</button>' );
             $( '#overlay').hide();
             $( '#popupForm').hide();
         })
      }
      else {
         $.post( "/admin/editplan", data, function( data ) {
           touring = ( data.touring == 1 ? 'Yes' : 'No');
           type = ( data.type == 1 ? 'Escort' : 'Business');
           $( '#row_' + data.id ).hide();
           $( '#datatable').prepend('<tr data-row = ' + data.id + '><td class ="name">' + data.name + '</td><td class ="description">' + data.description + '</td><td class ="price">' + data.price + '</td><td class ="type">' + type + '</td><td class = "admin_only">Yes</td><td class ="escort_profiles">' + data.escort_profiles + '</td><td class ="business_profiles">' + data.business_profiles +'</td><td class ="touring">' + touring + '</td><td class ="bumpumps">' + data.bumpups + '</td><td><button data-id ="' + data.id + '" class ="editPlan mpButt">Edit</button>' );
           $( '#overlay').hide();
           $( '#popupForm').hide();
        });
      }
      });
    $( 'select[name="type"]' ).on('change',function() {
        if ( $( this ).val() == 1 ) {
           $( '#popupForm .busHidden').hide();
           $( '#popupForm .escHidden').show();
           $( '#descriptor').html('New Plan');
        }
        else {
           $( '#popupForm .busHidden').show();
           $( '#popupForm .escHidden').hide();
        }
    })
    $( 'body').on('click','.editPlan',function() {
        $( '#overlay').show();
        $( '#popupForm').show();
        $( '#descriptor').html('Edit Plan');
        id = $( this ).data('id');
        row = $( '#row_' + id );
        data = {
            id: id,
            name: row.find('.name').html(),
            description: row.find('.description').html(),
            price: row.find('.price').html(),
            type: row.find('.type').data('type'),
            escort_profiles: row.find('.escort_profiles').html(),
            business_profiles: row.find('.business_profiles').html(),
            touring: row.find('.touring').html(),
            bumpups: row.find('.bumpups').html()
        }
        window.action = 'edit';
        $( 'input[name="name"]').val( data.name );
        $( 'textarea[name="description"]').val( data.description );
        $( 'input[name="price"]').val( data.price );
        $( 'select[name="type"]' ).val( data.type );
        $( 'input[name="escort_profiles"]').val( data.escort_profiles );
        $( 'input[name="business_profiles"]').val( data.business_profiles );
        $( 'input[name="touring"]').val( data.touring );
        $( 'input[name="bumpups"]').val( data.bumpups );
        $( 'input[name="id"]').val( data.id );
    })
});
</script>
@stop





