@extends('frontend.layout')

@section('content')

	<div class="container">
		<div data-prova="1" class="things"></div>
	</div>

@stop

@section('javascript')

	<script>
	(function($){
		$.ajax({
			url: '/test/ajax',
			data: {
				gender: [2,3],
				hair_color: [1,2]
			}
		})
		.done(function(data) {
			console.log(data);
		});
		
	})(jQuery);
	</script>
	

@stop