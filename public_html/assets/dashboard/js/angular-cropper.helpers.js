/**
 * ----- GLOBAL HELPER FUNCTIONS -----
 * This file contains functions that work as helpers with repetitive tasks used
 * through all the files.
 */

// Uppercase the first letter of a string
String.prototype.ucfirst = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
// Lowercase the first letter of string
String.prototype.lcfirst = function () {
    return this.charAt(0).toLowerCase() + this.slice(1);
};
// Checks if an element is in an array/object
function contains(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}