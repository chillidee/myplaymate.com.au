<?php

class Postcode extends Eloquent
{

    protected $guarded = [];

    public $timestamps = false;

    public static $rules = [

    ];

    public $primaryKey = 'postcode';

    public function suburb()
    {
        return $this->hasMany('Suburb');
    }
}
