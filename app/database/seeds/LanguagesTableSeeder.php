<?php

class LanguagesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('languages')->truncate();

        $Languages = [
            ['name' => 'English'],
            ['name' => 'Arabic'],
            ['name' => 'Cantonese'],
            ['name' => 'Croatian'],
            ['name' => 'French'],
            ['name' => 'German'],
            ['name' => 'Hindi'],
            ['name' => 'Greek'],
            ['name' => 'Indonesian'],
            ['name' => 'Italian'],
            ['name' => 'Japanese'],
            ['name' => 'Korean'],
            ['name' => 'Macedonian'],
            ['name' => 'Maltese'],
            ['name' => 'Mandarin'],
            ['name' => 'Polish'],
            ['name' => 'Russian'],
            ['name' => 'Serbian'],
            ['name' => 'Spanish'],
            ['name' => 'Filipino'],
            ['name' => 'Turkish'],
            ['name' => 'Vietnamese'],
            ['name' => 'Other'],

            ];

        // Uncomment the below to run the seeder
        DB::table('languages')->insert($Languages);
    }
}
