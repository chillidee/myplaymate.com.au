@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class=deviceWidth style="width: 580px;" align=center bgcolor=#eeeeed border=0 cellpadding=0 cellspacing=0>
  <tbody>
    <tr>
      <td style="padding: 0;" bgcolor=#ffffff valign=top>
        <a href=http://www.myplaymate.com.au/>
        <img class=deviceWidth style="display: block; border-radius: 4px;" src="https://myplaymate.com.au/assets/emails/main-updated-4.png" alt border=0>
      </a>
    </td>
  </tr>
  <tr>
    <td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
      <table bgcolor="#eeeeed">
        <tbody>
          <tr>
            <td style="padding: 0 10px 10px 0;" valign=middle>
              <a style="text-decoration: none; color: #272727; font-size: 18px; font-weight: bold; font-family: Arial, sans-serif;" href=#>
                Hi {{$data['full_name']}}!,
              </a>
            </td>
          </tr>
        </tbody>
      </table>
      <p>Thank you for choosing to play with My Playmate! To start browsing you will need to verify your email address. Once you email has been verified you will be able to browse escorts, brothels, agencies and massage parlours all over the country.</p>
      <p>We have worked very hard to ensure My Playmate is simple and easy to use, to effectively connect you with your ideal Play Mate. However, should you require <strong>any</strong> assistance, you can contact our team on <strong>1300 769 766</strong>.</p>
      <p style="text-align: center; font-size: 20px">Time to play! Get started by verifying your email <a style="text-decoration: none; color: #b60011; font-weight: bold;" href="{{Config::get('app.url')}}/{{$data['activation_link']}}"><strong>HERE.</strong></a></p>
    </td>
  </tr>
</tbody>
</table> <!-- End One Column -->

@stop