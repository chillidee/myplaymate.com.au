<?php class Payment extends Eloquent
{

   /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }
}