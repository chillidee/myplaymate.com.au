@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Edit escort gallery - {{ $escort->escort_name }}</h1>
    </div>
</div><!--pageheader-->

@include('admin.partials.escorts-breadcrumb')


<div id="backend-profile-gallery" class="alignc" >
    <h2>GALLERY</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="triggerAnimation animated" data-animate="fadeInUp">
                <p>
                	You can upload and display a maximum of 20 viewable photos at a time on your profile.<br />
                    Drag and drop your photos to sort them.
                </p>
                {{ Form::open(array('url'=>'/admin/escorts/gallery/upload/'.$escort->id,'files'=>true,'class'=>'dropzone')) }}
                {{ Form::hidden('escort_id',$escort->id) }}
                {{ Form::close() }}
                <div class="alignc section-content">
                    <a href="" class="btn btn-primary">UPLOAD</a>
                </div>

				<hr />

                <h2>YOUR IMAGES</h2>
                <br />
                <div class="grid-view profile-gallery">
                    @foreach(EscortImage::whereEscortId($escort->id)->get() as $image)
                        <div class="gallery-element col-lg-2 col-md-4 col-sm-6 col-xs-12">
                            <div class="escorts-img-container">
                                <div id="{{ $image->id }}" class="fa fa-trash-o delete"></div>
                                @if($image->status == 1)
                                    <div class="ribbon ribbon-pending"></div>
                                @endif
                                <img src="/uploads/escorts/thumbnails/thumb_220x330_{{ $image->filename }}" />
                            </div>
                            <ul class="images-keys">
                                <li id="{{ $image->id }}" class="is-not-main-image" style="display:{{ $image->id == $escort->main_image_id ? 'none' : 'block' }}">
                                    <a class="btn btn-default">Make Main Image</a>
                                </li>
                                <li class="is-main-image" style="display:{{ $image->id != $escort->main_image_id ? 'none' : 'block' }}">
                                    <a class="btn btn-primary">Main Image</a>
                                </li>
                            </ul>
                        </div>
                    @endforeach
                </div>
                <div class="alignc section-content" style="clear:both">
                    <a href="/admin/escorts/site/{{ $escort->id }}" class="btn btn-primary">SAVE AND CONTINUE</a>
                </div>

                <br /><br />
            </div><!-- .triggerAnimation.animated end -->
        </div><!-- .col-md-12 end -->
        <br /><br />
    </div><!-- .row end -->
</div><!-- End of Gallery-->

<script>
    (function($){
        $('div.delete').on('click', function() {
            var image_id = $(this)[0].id;
            $(this).parents('.gallery-element').remove();

            $.ajax({
                url: '/deleteImage',
                data: {image_id: image_id}
            });
        });

        $('.is-not-main-image').on('click', function(e) {
            e.preventDefault();
            var $this = $(this),
                escort_id = {{ $escort->id }},
                image_id = $this[0].id;

            $('.is-main-image').hide();
            $('.is-not-main-image').show();
            $this.hide();
            $this.siblings('.is-main-image').show();

            $.ajax({
                url: '/setMainImage',
                data: {
                    escort_id : escort_id,
                    image_id: image_id
                },
            });
        });   
    })(jQuery);
</script>

@stop