<div id ='overlayBackGround' ng-init = 'scopeInit()'></div>
<div class ='_MpoffWhite businessProfileScreen'>
   <div>
     <h2>Edit Profile- {{ $escort->escort_name }}</h2>
         <div class ='mPProfileTopMenu'>
           <div><div id ='mpBasicInfo' class ='active'></div><span>BASIC INFO</span></div>
           <div><div id ='mpGallery' ng-click ='getImages()'></div><span>GALLERY</span></div>
           <div><div id ='mpSeo'></div><span>SEO</span></div>
     </div>
   </div>
        <div class ='_mpProfile_holder'>
      <message></message>
     <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
     <h3>BASIC INFORMATION</h3>
     <p>Fill in basic information for your escort profile below. We recommend that you use a fake name and have a seperate working mobile and email as your escort contact. If you have any issues creating or editing your profile, please contact Myplaymate on 02 9798 1509</p>

    <form role="form" class ='profileForm' controller ='busSaveBasic' ng-submit = 'saveEscortInfo()' id = 'escortInfo' ng-model = 'basicInfoForm'>
      <div class ='mp_image_float' >
              @if ( $main_image )
        <div id="container_image">
          <img src ='{{Config::get('constants.CDN')}}/escorts/{{$main_image->filename}}' class ='full_width'>
        </div>
        @endif
      </div>
        <div class ='right_float'>
                    {{--Form::hidden('main_image',$main_image->filename,array('ng-model'=>'basicInfoForm.image','ng-init'=>"basicInfoForm.image='$main_image->filename'", 'id' => 'image')) --}}

                    {{ Form::text('escort_name',$escort->escort_name,array('id' => 'escort_name', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Escort Name','ng-model'=>'basicInfoForm.escort_name', 'ng-init'=>"basicInfoForm.escort_name='$escort->escort_name'")) }}

         <label id ='escort_age' class ='form_required'>Age:</label>{{ Form::select('age_id',$ages,$escort->age_id,array('id' =>'age_id','class'=>'form-control','ng-model'=>'basicInfoForm.age_id','ng-init'=>"basicInfoForm.age_id='$escort->age_id'")) }}
        <div class="control-group" id="sex">
                    <label class ='form_required left_margin_15'>Gender</label>
                    {{ Form::radio('basicInfoForm.gender_id',1,$escort->gender_id == 1 ? 1 : 0,array('id'=>'gender_id-1','ng-model' => 'basicInfoForm.gender_id','ng-init' => 'basicInfoForm.gender_id = "' . $escort->gender_id . '"')) }}
                    {{ Form::label('gender_id-1','Female') }}
                    {{ Form::radio('basicInfoForm.gender_id',2,$escort->gender_id == 2 ? 1 : 0,array('id'=>'gender_id-2','ng-model' => 'basicInfoForm.gender_id')) }}
                    {{ Form::label('gender_id-2','Male') }}
                    {{ Form::radio('basicInfoForm.gender_id',3,$escort->gender_id == 3 ? 1 : 0,array('id'=>'gender_id-3','data-parsley-required','ng-model' => 'basicInfoForm.gender_id')) }}
                    {{ Form::label('gender_id-3','Transexual') }}
            </div>
          </div><br>
          <label class ='showAdvanced'>Show advanced settings</label>
        <div class ='advancedSettings'>
        <label><a href ='/admin/userprofile/{{$escort->user_id}}'>Visit user dashboard</a></label><br>
         <label>User Id</label>
                     {{ Form::text('user_id',$escort->user_id,array('ng-init'=>"basicInfoForm.user_id = " . json_encode( $escort->user_id ),'ng-model'=>'basicInfoForm.user_id','placeholder' => 'User id','class'=>'input-xlarge input03')) }}   
            <div class="control-group" id="sex"><br>
            <label>Please note that if you set status here, no email will be sent to the user.</label><br>
                    <label>Status</label>
                    {{ Form::radio('basicInfoForm.status','1','',array('id'=>'status1','ng-model' => 'basicInfoForm.status')) }}
                    {{ Form::label('gender_id-1','Pending') }}
                    {{ Form::radio('basicInfoForm.status','2','',array('ng-model' => 'basicInfoForm.status','ng-init' => 'basicInfoForm.status="' . $escort->status . '"'))}}
                    {{ Form::label('gender_id-2','Approved') }}
                    {{ Form::radio('basicInfoForm.status','3','',array('ng-model' => 'basicInfoForm.status'))}}
                    {{ Form::label('gender_id-2','Disapproved') }}
                    {{ Form::radio('basicInfoForm.status','4','',array('ng-model' => 'basicInfoForm.status'))}}
                    {{ Form::label('gender_id-2','Unavailable') }}

            </div>                 
          </div>

           {{ Form::label('servicesLabel','Escort Description',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
        {{ Form::textarea('about_me',json_encode( $escort->about ), array( 'id' => 'about_me','class'=>'form-control','ng-model'=>'basicInfoForm.about_me','ng-init'=>"basicInfoForm.about_me = " . json_encode( $escort->about_me ))) }}


        <div class="alignc">
            {{ Form::submit('SAVE',array('class'=>'submit-button')) }}

        </div>
    {{ Form::close() }}
</div>


  <div class ='mPtab' id = 'tab_mpGallery'>
      <form role="form" ng-controller ='galleryController' ng-submit = 'saveEscortGallery()' id = 'galleryForm' ng-model = 'profileGallery' class ='profileForm normalSelects'>
                      <h3>GALLERY</h3>
                      <p>You can upload and display a maximum of 10 viewable of 10 viewable photos at a time on your profile. <br>
                      Drag and drop your photos to sort them.</p>
                      <?php $mainImageFile = ( is_object ( $main_image ) ? $main_image->filename : '' ); ?>
                      <?php $class = "{{image.filename == '" . $mainImageFile . "'?'main_image':''}}";?>
                     <div class ='gallery' sv-root sv-part="profileGallery.images">
                         <div class ='col-md-2' ng-class = "{'main_image': profileGallery.main_image == image.id}" ng-repeat="image in profileGallery.images" sv-element>
                               <div class ='delete_image' ng-click = 'deleteImage($index)'></div>
                               <div class ='make_main' ng-click = 'makeMain($index)'>Make Main Image</div>
                               <img draggable ="false" ng-src ="@{{image.newImage == 1?'/temp/' + image.filename:'https://d2enq9fr2fmuvm.cloudfront.net/escorts/' + image.filename}}">
                         </div>
                        <?php /*  <div class ='col-md-2' id ='addImage' ngf-validate-fn="validate($file)" ngf-accept="'image/*'" ngf-resize-if="$width > 1200 || $height > 1200"  ngf-resize="{width: 1000, quality: .8, type: 'image/jpeg', 
               ratio: '1:2', centerCrop: true, pattern='.jpg', restoreExif: true}"  ngf-min-width = "400" ngf-max-size = "1.3MB" ngf-pattern="'.png, .jpg, .JPEG, .jpeg'" ngf-capture="'camera'" ngf-select="galleryUpload($file)" ngf-drop = "galleryUpload($file)">*/?>
               <div class ='col-md-2' id ='addImage' ngf-validate-fn="validate($file)" ngf-accept="'image/*'" ngf-capture="'camera'" ngf-select="galleryUpload($file)" ngf-drop = "galleryUpload($file)">
                     Add Image
                     <div id ='galleryLoading'></div>
        </div>
         
        </div>
            
        <span class="progress ng-hide" ng-show="progress >= 0">
          <div style="width:%" ng-bind="progress + '%'" class="ng-binding">%</div>
        </span>
        <span ng-show="result" class="ng-hide">Upload Successful</span>
        <span class="err ng-binding ng-hide" ng-show="errorMsg"></span>

        <?php $mainImage = ( is_object ( $main_image ) ? $main_image->id : '' ); ?>

        {{ Form::text('main_image','',array('class'=>'form-control hidden','ng-model'=>'profileGallery.main_image', 'ng-init'=>'profileGallery.main_image="' . $mainImage . '"')) }}

              <div class="alignc">
            {{ Form::submit('SAVE',array('class'=>'submit-button')) }}

        </div>
        </form>
        </div>
                <div class ='mPtab' id = 'tab_mpSeo'><h3>SEO</h3>
                <form role="form" ng-submit = 'saveEscortSeo()' ng-model = 'profileSeo' class ='profileForm normalSelects'>
                   {{ Form::label('seo_title','Seo Title',array('class'=>'control-label')) }}
                   {{ Form::text('seo_title',$escort->seo_title,array('id' => 'seo_title', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Seo Title','ng-model'=>'profileSeo.seo_title', 'ng-init'=>"profileSeo.seo_title='$escort->seo_title'")) }}
                   {{ Form::label('seo_keywords','Tags above Photo',array('class'=>'control-label')) }}
                   {{ Form::text('seo_keywords',$escort->seo_keywords,array('id' => 'seo_keywords', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Seo Keywords','ng-model'=>'profileSeo.seo_keywords', 'ng-init'=>"profileSeo.seo_keywords='$escort->seo_keywords'")) }}
                   {{ Form::label('seo_description','Meta Description',array('class'=>'control-label')) }}
                   {{ Form::text('seo_description',$escort->seo_description,array('id' => 'seo_description', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Seo Description','ng-model'=>'profileSeo.seo_description', 'ng-init'=>"profileSeo.seo_description='$escort->seo_description'")) }}
                   {{ Form::label('seo_content','Escort Tags',array('class'=>'control-label')) }}
                   {{ Form::text('seo_content',$escort->seo_content,array('id' => 'seo_content', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Seo Content','ng-model'=>'profileSeo.seo_content', 'ng-init'=>"profileSeo.seo_content='$escort->seo_content'")) }}
                <button class="submit-button">Save</button>
                </form>
                </div>

   <script>

    $( '.showAdvanced').on('click',function() {
       $( '.advancedSettings' ).show();
   })

     $( '.mPProfileTopMenu div').on('click',function(e) {
      var tabName = $( this ).attr('id');
      $( '.mPtab').removeClass( 'activeTab');
      $( '#tab_' + tabName ).addClass( 'activeTab');
      $( '.mPProfileTopMenu div').removeClass( 'active');
      $( this ).addClass( 'active');
 
  e.stopPropagation();
})

  var postObject = {
       userId: id,
       csrf: $( '#_token').val(),
       businessId: currentEscort
     },
     cropTypes = {
                       widescreen : true,
                       letterbox: false,
                       free: false
                  };

    </script>

  
    