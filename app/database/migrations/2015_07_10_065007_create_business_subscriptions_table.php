<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessSubscriptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_subscriptions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('plan_id');
			$table->integer('business_id');
			$table->boolean('trial_used');
			$table->timestamp('from_date');
			$table->timestamp('to_date');
			$table->float('credits');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_subscriptions');
	}

}
