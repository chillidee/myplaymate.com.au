<?php

/**
 * Class EmailTemplate
 */
class EmailTemplate extends Eloquent
{

    /**
     * @var string
     */
    public $table = 'email_templates';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [];


    public function getContent()
    {
        return $this->content;
    }
}








