@extends('frontend.layout')

@section('content')
















<div class ='showPage'>
  @if( $brothel->cover )
  <img class ='full_width' src = '{{ Config::get('constants.CDN')}}/businesses/{{$brothel->cover}}'>
  @endif
  <div class ='container'>
   <div class ='row'>
     <div class ='col-md-12'>
      @if($brothel->image != '' )
      <img src="{{ Config::get('constants.CDN')}}/businesses/{{ $brothel->image }}" alt="{{ $brothel->name }}" class ="showPageLogo @if( $brothel->cover ) coverLogo @endif ui">
      @endif
    </div>
    <div class ='col-md-9'>
      <h1>{{$brothel->name}}</h1>
      <p>{{nl2br($brothel->about )}}</p>
    </div>
    <div class ='col-md-3 showPageSidebar'>
     <div id ='reviewHolder'>
      @if( $brothel->review)
      @else 
      <?php for( $i= 0; $i < 5; $i++ ){ ?>
      <span class = 'star'></span>
      <?php }?>
      <span id ='revNot'>There are no reviews.  <a data-toggle="modal" href="#leaveReviewModal">Add review</a></span>
      @endif
      @if( isset( $brothel->phone ))  <div class ='actionPane actionPhone' data-num ='{{Helpers::format_phone_number($brothel->phone)}}'>{{ substr(Helpers::format_phone_number($brothel->phone), 0, 7)}}...</div>@endif
      @if( isset( $brothel->email )) <div class ='actionPane actionPlay'data-toggle="modal" href="#contactEscortModal">Send Playmail</div>@endif
      @if( isset( $brothel->url )) <div class ='actionPane actionPhone'>Visit Website</div>@endif
      @if( isset( $brothel->latitude )) <div class ='actionPane actionPhone'><a href ='#directions'>Get Directions</a></div>@endif 
      @if( isset( $brothel->website )) <div class ='actionPane'><a href ='http://{{$brothel->website}}'>Visit Website</a></div>@endif 
    </div>
  </div>
</div>
</div>
@if( count( $escorts )> 0 )
<div id ='showEscort'>
  <div id ='escortHolder'></div>
</div>
<div class ='container'>
  <div class ='row'>
    <div class ='col-md-12'>
      <h2>Our Girls</h2>
      <div class ='row'>
        <div class ='escort-container grid-view showPage clear'>
          @foreach ( $escorts as $escort ) 
          @if( $escort->status == 2 && $escort->main_image )
          <li class="col-xs-12 col-ms-6 col-md-4 col-lg-3 col-xl-15 col-xl-3 escorts-wishlist clone" data-distance="0">
           <figure class="escorts-img-container" id="escort-{{$escort->id}}">
            <div class="info-container">
              <div class="figinfo">
                <span class="figname">{{$escort->escort_name}}</span>
              </div><!-- figinfo-->
            </div><!-- end of info-container-->
            <img class="img-profile" src="/uploads/escorts/thumbnails/thumb_220x330_{{$escort->main_image}}" alt="" data-id ="{{$escort->id}}">  
          </figure>
        </li>
        @endif
        @endforeach
      </div>
    </div>
  </div>
</div><!-- End of Row -->
@endif
@if( $brothel->latitude )
<div id ='directions'>
  <div>{{$brothel->address_line_1}} 
    @if( isset( $brothel->address_line_2 ))
    {{$brothel->address_line_2 }}
    @endif
    @if( isset( $brothel->suburb ))
    {{$brothel->suburb }}
    @endif
    @if( isset( $brothel->state ))
    {{$brothel->state }}
    @endif
  </div>
  <div id ='map'></div>
</div>
@endif
</div><!-- End of profile Details -->
</div>
<script>
  window.activeId = {{$brothel->id}};
  @if ( $brothel->latitude )
  window.mapPage = 1;
  @else 
  window.mapPage = 0;
  @endif
  window.getGirls = 1;
  window.mapName ={{json_encode( $brothel->name )}};
  @if ( $brothel->latitude )
  window.center = {lat: {{$brothel->latitude}}, lng: {{$brothel->longitude}}};
  @endif
</script>
@stop
@include ( 'frontend.partials.businessModal' )













