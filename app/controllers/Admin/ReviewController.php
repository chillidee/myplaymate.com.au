<?php
namespace Admin;

use Controller, Datatables, View, Input, Escort;
use EscortReview;
class ReviewController extends Controller {

  public function index()
  {
    $reviews = EscortReview::all();
    return View::make('admin.reviews.index',compact('reviews'));
  }

  public function ajax()
  {
    $query = EscortReview::select('*');
    return Datatables::of($query)
      ->editColumn('escort_id',function($review) {
          $escort = Escort::find($review->escort_id);
          if ( $escort )
              return $escort->escort_name;
          })
      ->editColumn('status','{{ Helpers::get_status($status) }}')
      ->editColumn('published',function($review){
        return $review->published == 1 ? '<i class="fa fa-check"></i> Yes' : '<i class="fa fa-ban"></i> No';
      })      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->addColumn('action', function ($review) {
                return '<a href="#" data-review-id="'.$review->id.'" class="btn btn-xs btn-success approve"><i class="fa fa-check"></i> Approve</a>
                    <a href="#" data-review-id="'.$review->id.'" class="btn btn-xs btn-danger disapprove"><i class="fa fa-ban"></i> Disapprove</a>';
            })
      ->make(true);
  }

  public function approve()
  {
    $review = EscortReview::find(Input::get('review_id'));
    $review->status = 2;
    $review->save();
  }

  public function disapprove()
  {
    $review = EscortReview::find(Input::get('review_id'));
    $review->status = 3;
    $review->save();
  }

}