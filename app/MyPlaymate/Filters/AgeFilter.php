<?php
namespace MyPlaymate\Filters;

use MyPlaymate\Filters\Filter;

class AgeFilter extends Filter
{
	public $column = 'age_id';

	public function apply($escorts,$filterValue)
	{
		$ids = $this->convertAgesToIds($filterValue);

		return array_filter($escorts,function($escort) use($ids)
		{
			return  in_array($escort[$this->column], $ids);
		});	
	}

	public function convertAgesToIds($filterValue)
	{
		return explode('-', $filterValue);
	}
}










