Your last payment was on the {{ date('d M Y', $payments->first()->created_at->timestamp) }}<br>
Your next payment will be due on {{ date('d M Y', strtotime( $payments->first()->period_to ) ) }}