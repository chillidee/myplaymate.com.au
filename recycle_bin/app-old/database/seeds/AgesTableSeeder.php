<?php

class AgesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('ages')->truncate();

        $ages = [];

        for ($i = 18; $i <= 69; $i++) {
            $ages[] = ['name' => $i];
        }

        $ages[] = ['name' => '70+'];

        // Uncomment the below to run the seeder
        DB::table('ages')->insert($ages);
    }
}
