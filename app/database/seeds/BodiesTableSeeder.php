<?php

class BodiesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('bodies')->truncate();

        $bodies = [
			['name' => 'Average'
                ,'gender_id' => 2, ],
            ['name' => 'Petite'
                ,'gender_id' => 1, ],
            ['name' => 'Slim'
                ,'gender_id' => 1, ],
            ['name' => 'Athletic'
                ,'gender_id' => 1, ],
            ['name' => 'Curvy'
                ,'gender_id' => 1, ],
            ['name' => 'Muscular'
                ,'gender_id' => 2, ],
            ];

        // Uncomment the below to run the seeder
        DB::table('bodies')->insert($bodies);
    }
}
