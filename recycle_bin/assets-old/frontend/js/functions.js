//NO COLFLICT MODE OF JQUERY
var $ = jQuery.noConflict();


function dateFormat(date){
	var date = new Date(date);
	return (date.getDate() + 1) + '/' + date.getMonth() + '/' +  date.getFullYear();
}

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}

// SCRIPTS START
(function($){

	var revapi3;				
		if($('#frontpage-banner').revolution == undefined)
		revslider_showDoubleJqueryError('#frontpage-banner');
		else
	   revapi3 = $('#frontpage-banner').show().revolution(
		{
			delay:7500,
			startwidth:1070,
			// startheight:250,
			hideThumbs:200,

			touchenabled:"on",
			onHoverStop:"on",

			navigationType:"bullet",
			navigationArrows:"nexttobullets",
			navigationStyle:"square-old",
			navigationHAlign:"center",
			navigationVAlign:"bottom",
			navigationVOffset:30,

			fullScreen:"on",
			minFullScreenHeight:"",
		});

	// Remove placeholder on focus
    $('input').on('focus', function() {
        var $this = $(this),
            placeholder = $this.attr('placeholder');
        $this.attr('placeholder', '');

        $this.on('blur', function() {
            $this.attr('placeholder', placeholder);
        });
    });

	// Scroll to top
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.scroll-up').fadeIn();
		} else {
			$('.scroll-up').fadeOut();
		}
	});

	$('.scroll-up').click(function() {
		$("html, body").animate({
			scrollTop: 0
		}, 600, function(){
			//set_static_header(0);
		});
		return false;
	});

    // Dropdown menu
    $('.navbar-nav li').hover(function() {
        $(this).addClass('open');
    }, function() {
        $(this).removeClass('open');
    });

    // Welcome
    var welcome = Cookies.get('welcome');
    if(!welcome){
        $('#welcomeModal').fadeIn(200);
    }

    $('body').on('click', '#welcomeModal button', function(event) {
        Cookies.set('welcome', true);
        $('#welcomeModal').fadeOut(200);
    });

    // Dropdown mobile arrow
    $('.dropdown-caret').on('click', function(e) {
        console.log($(this).parent('li').children('ul'));
        e.preventDefault();
        $(this).parent('li').children('ul').slideToggle();
    });

    $('.showLeftPush').on('click', function(){
        $('.showLeftPush').toggleClass( 'active' );
        $('body').toggleClass('cbp-spmenu-push-toright');
        $('#cbp-spmenu-s1').toggleClass('cbp-spmenu-open');
        $('#filterEscort').toggleClass('cbp-spmenu-push-toright');
    });

    $('.accordion').on('click', '.title', function(event) {
        event.preventDefault();
        $(this).siblings('.accordion .active').next().slideUp('normal');
        $(this).siblings('.accordion .title').removeClass("active");

        if ($(this).next().is(':hidden') === true) {
            $(this).next().slideDown('normal');
            $(this).addClass("active");
        }
    });
    $('.accordion .content').hide();
    $('.accordion .active').next().slideDown('normal');

	//FEATURED SECTION IN FRONTPAGE
	$("#owl-featured").owlCarousel({
		autoPlay: 7000, //Set AutoPlay to 3 seconds
		items : 5,
		itemsDesktop : [1199,5],
		itemsDesktopSmall : [980,3],
		itemsTablet: [768,4],
		itemsTabletSmall: [568,2],
		itemsMobile : [479,2],
	});
		
	// NEW USERS CAROUSEL
	$("#owl-newusers").owlCarousel({
		autoPlay: 3000, //Set AutoPlay to 3 seconds	 
		items : 6,
		itemsDesktop : [1024,6],
		itemsDesktopSmall : [800,4],
		itemsTablet: [768,4],
		itemsTabletSmall: [568,3],
		itemsMobile : [479,3],
	});
					
	//SIDEBAR CAROUSEL
	$("#owl-sidebar").owlCarousel({
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		items : 1,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [979,1],
		itemsMobile : [479,1]
	});

})(jQuery);
