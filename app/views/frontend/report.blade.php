@extends('frontend.layout')

@section('content')

  <!-- #page-title start -->
  <section id="page-title" class="page-title-1">
      <div class="container">
          <h1>Report an escort</h1>
      </div><!-- .containr end -->
  </section><!-- #page-title end -->  

  <!-- .boxed version of the page -->
  <section class="page-wrapper top-space">
      <!-- .container start -->
      <div class="container">
          <div class="row">
              <div class="col-md-9">
                  <div class="triggerAnimation animated fadeInRight" data-animate="fadeInRight" style="">
                      <div class="col-md-12">

              @include('frontend.partials.message')

                          <div id="login-container">
                {{ Form::open() }}
                                  <fieldset>
                                      <div class="control-group">
                                        {{ Form::label('escort_name','Escort name',array('class'=>'control-label')) }}<span>*</span>
                                          <div class="controls">
                                            {{ Form::text('escort_name','',array('class'=>'input-xlarge input03')) }}
                                          </div>
                                @if($errors->has('escort_name'))
                                <p class="form-error">{{ $errors->first('escort_name') }}</p>
                                @endif
                                      </div>
                                      <div class="control-group">
                                        {{ Form::label('email','Email',array('class'=>'control-label')) }}<span>*</span>
                                          <div class="controls">
                                            {{ Form::email('email','',array('class'=>'input-xlarge input03')) }}
                                          </div>
                                @if($errors->has('email'))
                                <p class="form-error">{{ $errors->first('email') }}</p>
                                @endif
                                      </div>
                                      <div class="control-group">
                                        {{ Form::label('name','Your name',array('class'=>'control-label')) }}<span>*</span>
                                          <div class="controls">
                                            {{ Form::text('name','',array('class'=>'input-xlarge input03')) }}
                                          </div>
                                @if($errors->has('name'))
                                <p class="form-error">{{ $errors->first('name') }}</p>
                                @endif
                                      </div>
                                      <div class="control-group">
                                        {{ Form::label('phone','Phone',array('class'=>'control-label')) }}<span>*</span>
                                          <div class="controls">
                                            {{ Form::text('phone','',array('class'=>'input-xlarge input03')) }}
                                          </div>
                                @if($errors->has('phone'))
                                <p class="form-error">{{ $errors->first('phone') }}</p>
                                @endif
                                      </div>
                                      <div class="control-group">
                                        {{ Form::label('subject','Subject',array('class'=>'control-label')) }}<span>*</span>
                                          <div class="controls">
                                            {{ Form::text('subject','',array('class'=>'input-xlarge input03')) }}
                                          </div>
                                @if($errors->has('subject'))
                                <p class="form-error">{{ $errors->first('subject') }}</p>
                                @endif
                                      </div>
                                      <div class="control-group">
                                        {{ Form::label('reportMessage','Message',array('class'=>'control-label')) }}<span>*</span>
                                          {{ Form::textarea('reportMessage','',array('class'=>'wpcf7-textarea input03')) }}
                                      </div>
                              @if($errors->has('message'))
                              <p class="form-error">{{ $errors->first('message') }}</p>
                              @endif
                                      <div class="control-group">
                                          <div class="controls" id="rightright">
                                              <div class="button3">
                                              {{ Form::submit('Send') }}
                                              </div>
                                          </div>
                                      </div>
                                  </fieldset>
                              {{ Form::close() }}
                          </div>
                      </div>
                  </div><!-- .triggerAnimation.animated end -->
          </form>
              </div><!-- .col-md-9 end -->

              <aside class="col-md-3 col-xs-12 col-sm-10 aside aside-left triggerAnimation animated fadeInLeft" data-animate="fadeInLeft" style="">
                  <ul class="aside-widgets">
                      <!-- .widget_navigation -->
                      <li class="widget widget_contact">
                          <div id="mpm-contact">
							<p>Found a listing on our site that seems to be broken or is misrepresenting themselves? Report them using the form.<br /><br /></p>
							<p>You may also call or SMS us between 8:30am to 5:30pm Monday to Friday, except weekends or public hoildays.<br /><br /></p>
							<table>
							<tbody>
							<tr>
							<td>Call:</td>
							<td><a href="tel:1300769766" style="color: #f55;">1300 769 766</a></td>
							</tr>
							<tr>
							<td>SMS:</td>
							<td><a href="tel:0499188677" style="color: #f55;">0499 188 677</a></td>
							</tr>
							<tr>
							<td>Twitter:&nbsp;</td>
							<td><a href="http://twitter.com/MyPlayMateAU" target="_blank" style="color: #f55;">@MyPlaymateAU</a></td>
							</tr>
							</tbody>
							</table>
                          </div>
                      </li><!-- .widget_Navigation end -->
                  </ul><!-- .aside-widgets end -->
              </aside><!-- .aside.aside-left end -->
          </div>
      </div><!-- .container end -->
  </section> <!-- End of the Boxed Version Page -->
@stop

  
