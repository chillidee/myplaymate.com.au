<div class ='profile_tab pt-basic-info pt-active-tab'>
  <div id="basic-info">
    {{ Form::open(['id'=>'escortInfo']) }}
        <h2 class="alignc">BASIC INFORMATION</h2>
        <p class = "profile_info">
          Please fill in your details below
  </p>
        <div class="alert alert-success">Escort info updated!</div>


        @if($escort->main_image_id != 0)
            <div id="backend-profile-img">
                <figure class="escorts-img-container">
                    <img src="/uploads/escorts/{{ $escort->mainImage->filename }}">
                </figure>
            </div>
        @else
            <div id="backend-profile-img">
                <figure class="escorts-img-container">
                    <img class="img-profile" src="/assets/frontend/img/profile_template.jpg">
                </figure>
            </div>
        @endif

           <div class ='right_floater width_65'>
            <div class="control-group">
                {{ Form::label('escort_name','Escort Name*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('escort_name',$escort->escort_name,array('class'=>'escort-name input-xlarge input03','data-parsley-required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('phone','Phone*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('phone',$escort->phone,array('class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('email','Email*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('email',$escort->email,array('class'=>'input-xlarge input03','data-parsley-required','data-parsley-type'=>'email')) }}
                </div>
            </div>
          </div>
          <div class ='bottom_section'>
          <div class="control-group left_floater age_floater">
                {{ Form::label('age_id','Age*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::select('age_id',$ages,$escort->age_id,array('class'=>'form-control')) }}
                </div>
            </div>
            <div class="control-group left_floater">
                {{ Form::label('suburb_id','Location*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::select('suburb_id',$suburbs,$escort->suburb_id,array('class'=>'select2 form-control input-large input03','data-parsley-required','min'=>'1','data-parsley-min-message'=>'Please select a suburb.','style'=>'width:100%')) }}
                </div>
            </div>
            <div class="control-group left_floater">
                {{ Form::label('licence_number','Licence number (if applicable)',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('licence_number',$escort->licence_number,array('class'=>'input-xlarge input03')) }}
                </div>
            </div>

            <div class="control-group" id="sex">
                <div>
                    {{ Form::radio('gender_id',1,$escort->gender_id == 1 ? 1 : 0,array('id'=>'gender_id-1')) }}
                    {{ Form::label('gender_id-1','Female') }}
                </div>
                <div>
                    {{ Form::radio('gender_id',2,$escort->gender_id == 2 ? 1 : 0,array('id'=>'gender_id-2')) }}
                    {{ Form::label('gender_id-2','Male') }}
                </div>
                <div>
                    {{ Form::radio('gender_id',3,$escort->gender_id == 3 ? 1 : 0,array('id'=>'gender_id-3','data-parsley-required')) }}
                    {{ Form::label('gender_id-3','Transexual') }}
                </div>
            </div>
            <div class="clear" style="clear:both;"><br /></div>

            <div class="control-group phone_contact left_floater">
                {{ Form::label('contact_phone','Contact by Phone/SMS*',array('class'=>'control-label')) }}
                <div>
                    {{ Form::radio('contact_phone',1,$escort->contact_phone ? 1 : 0,array('id'=>'contact_phone-yes')) }}
                    {{ Form::label('contact_phone-yes','Yes') }}
                </div>
                <div>
                    {{ Form::radio('contact_phone',0,$escort->contact_phone ? 0 : 1,array('id'=>'contact_phone-no')) }}
                    {{ Form::label('contact_phone-no','No') }}
                </div>
            </div>
            <div class="control-group left_floater">
                {{ Form::label('contact_playmail','Contact by PlayMail*',array('class'=>'control-label')) }}
                <div>
                    {{ Form::radio('contact_playmail',1,$escort->contact_playmail ? 1 : 0,array('id'=>'contact_playmail-yes')) }}
                    {{ Form::label('contact_playmail-yes','Yes') }}
                </div>
                <div>
                    {{ Form::radio('contact_playmail',0,$escort->contact_playmail ? 0 : 1,array('id'=>'contact_playmail-no')) }}
                    {{ Form::label('contact_playmail-no','No') }}
                </div><script>
    (function($){
        $('.second-level h2').text("{{ $escort->escort_name }}");
    })(jQuery);
</script>
            </div>
          </div>
        {{ Form::hidden('escort_id',$escort->id) }}
        {{ Form::hidden('next_icon','escort-about',array('class'=>'next-icon')) }}
        {{ Form::hidden('next_route','escortAbout',array('class'=>'next-route')) }}
        {{ Form::hidden('next_type','escort',array('class'=>'next-type')) }}
        <div class="alignc section-content">
            {{ Form::submit('UPDATE',array('class'=>'submit-button')) }}
            {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }}
        </div>

    {{ Form::close() }}
</div>
</div>
<div class ='profile_tab pt-escort-about'>
@include('frontend.partials.profile.escort-about')
  </div>
<div class ='profile_tab pt-escort-availability'>
@include('frontend.partials.profile.escort-availability')
  </div>
<div class ='profile_tab pt-escort-gallery'>
@include('frontend.partials.profile.escort-gallery')
  </div>