@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class="deviceWidth" style="width: 580px;" align="center" bgcolor="#eeeeed" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 0;" bgcolor="#ffffff" valign="top"><a href="http://www.myplaymate.com.au/"><img class="deviceWidth" style="display: block; border-radius: 4px;" src="http://www.myplaymate.com.au/assets/emails/main-updated-4.png" alt="" border="0" /></a></td>
</tr>
<tr>
<td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
<table bgcolor="#eeeeed">
<tbody>
<tr>
<td style="padding: 0 10px 10px 0;" valign="middle"><a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href="#">Hey {{$data['full_name']}}, </a></td>
</tr>
</tbody>
</table>
<p>Unfortunately your profile hasn't been approved.</p>
<p>We'd like to help you with the changes made to your profile as they do not align with the terms and conditions of MyPlaymate.com.au and can't be approved. Usually inappropraite images or language are the culprits. Please update your profile so we can continue to find more playmates for you.</p>
<p>To assist you, please see our terms and conditions at <a style="text-decoration: none; color: #b60011; font-weight: bold;" href="http://myplaymate.com.au/member-terms-and-conditions-of-use">http://myplaymate.com.au/member-terms-and-conditions-of-use</a>&nbsp;for guidelines on what to include in your profile.</p>
<p>If you have any questions or require further assistance, please contact us at <a style="text-decoration: none; color: #b60011; font-weight: bold;" href="mailto:contact@myplaymate.com.au">contact@myplaymate.com.au</a></p>
<p>We hope you're enjoying being a Playmate!</p>
<p>The My Playmate Team</p>
</td>
</tr>
</tbody>
</table>
<!-- End One Column -->

@stop