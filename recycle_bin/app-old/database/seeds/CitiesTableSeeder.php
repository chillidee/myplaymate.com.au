<?php

class CitiesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('cities')->truncate();

        $cities = [
            [
                'name'      => 'Sydney',
                'state_id'  =>  1
            ],
            [
                'name'      => 'Wollongong',
                'state_id'  =>  1
            ],
            [
                'name'      => 'Canberra',
                'state_id'  =>  1
            ],
            [
                'name'      => 'Newcastle',
                'state_id'  =>  1
            ],
            [
                'name'      => 'Brisbane',
                'state_id'  => 2
            ],
            [
                'name'      => 'Townsville',
                'state_id'  => 2
            ],
            [
                'name'      => 'Cairns',
                'state_id'  => 2
            ],
            [
                'name'      =>'Surfers Paradise',
                'state_id'  => 2
            ],
            [
                'name'      => 'Melbourne',
                'state_id'  => 3
            ],
            [ 
                'name'      => 'Geelong',
                'state_id'  => 3
            ],
            [ 
                'name'      => 'Ballarat',
                'state_id'  => 3
            ],
            [ 
                'name'      => 'Bendigo',
                'state_id'  => 3
            ],
            [
                'name'      => 'Darwin',
                'state_id'  => 4
            ],
            [ 
                'name'      => 'Alice Springs',
                'state_id'  => 4
            ],
            [ 
                'name'      => 'Palmerston',
                'state_id'  => 4
            ],
            [ 
                'name'      => 'Katherine',
                'state_id'  => 4
            ],
            [
                'name'      => 'Adelaide',
                'state_id'  => 5
            ],
            [ 
                'name'      => 'Mount Gambier',
                'state_id'  => 5
            ],
            [ 
                'name'      => 'Whyalla',
                'state_id'  => 5
            ],
            [ 
                'name'      => 'Gawler',
                'state_id'  => 5
            ],
            [
                'name'      => 'Perth',
                'state_id'  => 6
            ],
            [ 
                'name'      => 'Rockingham',
                'state_id'  => 6
            ],
            [ 
                'name'      => 'Mandurah',
                'state_id'  => 6
            ],
            [ 
                'name'      => 'Bunbury',
                'state_id'  => 6
            ],
            [
                'name'      => 'Hobart',
                'state_id'  => 7
            ],
            [ 
                'name'      => 'Launceston',
                'state_id'  => 7
            ],
            [ 
                'name'      => 'Devonport',
                'state_id'  => 7
            ],
            [ 
                'name'      => 'Burnie',
                'state_id'  => 7
            ],
        ];

        // Uncomment the below to run the seeder
        DB::table('cities')->insert($cities);
    }
}
