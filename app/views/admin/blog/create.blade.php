@extends('admin.layout')

@section('javascript')

   <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.datepicker').datepicker({dateFormat: 'dd/mm/yy'});
        });
    </script>    

@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-newspaper-o"></span></div>
    <div class="pagetitle">
        <h1>New post</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open() }}
<div class="row">
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('title','Title*',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('title','',array('class'=>'form-control','required')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_title','SEO title',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_title','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_keywords','SEO keywords',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_keywords','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_description','SEO description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_description','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
    <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('seo_url','SEO Url',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('seo_url','',array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
        <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('short_content','Blog Description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('short_content','',array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="control-group" id="incalls">
            <h5>PUBLISHED</h5>
            <label class="radio-inline">{{ Form::radio('published', '1', false) }} Yes
            </label>
            <label class="radio-inline">{{ Form::radio('published', '0', true) }} No
            </label>
        </div>
    </div>    
    <div class="col-md-2">
        <div class="control-group">
            {{ Form::label('date_from','Published from',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('date_from','',array('class'=>'form-control datepicker')) }}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="control-group">
            {{ Form::label('date_to','Published to',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('date_to','',array('class'=>'form-control datepicker')) }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="control-group">
            {{ Form::label('content','Content',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('content','',array('class'=>'tinymce')) }}
            </div>
        </div>
<div class="alignc col-md-12">
    {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
</div>

{{ Form::close() }}
@stop