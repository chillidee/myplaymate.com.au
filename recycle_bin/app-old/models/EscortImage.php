<?php

/**
 * Class EscortImage
 */
class EscortImage extends Eloquent
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [];

    /**
     *
     */
    public function escort()
    {
        $this->belongsTo('Escort','escort_id');
    }

    public function url()
    {
        return url('/uploads/escorts/slider/'.$this->filename);
    }

    public function thumbnail($x, $y)
    {
        return url('/uploads/escorts/thumbnails/thumb_'.$x.'x'.$y.'_'.$this->filename);
    }
}
