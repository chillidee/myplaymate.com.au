<?php

class FakeUpdatesTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,5) as $index) {
            EscortUpdate::create([
                'escort_id'         => $faker->numberBetween(1,20),
                'message'          => $faker->realText(100)
            ]);
        }
    }
}