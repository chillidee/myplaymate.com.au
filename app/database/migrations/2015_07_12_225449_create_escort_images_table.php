<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escort_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('escort_id');
			$table->string('filename');
			$table->boolean('crop');
			$table->integer('order');
			$table->integer('status');
			$table->integer('approved_by');
			$table->timestamp('approved_at');
			$table->integer('disapproved_by');
			$table->timestamp('disapproved_at');
			$table->text('disapprove_message');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escort_images');
	}

}
