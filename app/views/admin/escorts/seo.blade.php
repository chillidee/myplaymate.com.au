@extends('admin.layout')

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Edit escort SEO - {{ $escort->escort_name }}</h1>
    </div>
</div><!--pageheader-->

@include('admin.partials.escorts-breadcrumb')

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open() }}
<div class="row">
    <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('seo_title','SEO title',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('seo_title',$escort->seo_title,array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('seo_keywords','Tags above Photo',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('seo_keywords',$escort->seo_keywords,array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('seo_description','Meta description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('seo_description',$escort->seo_description,array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('seo_content','Escort Tags',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('seo_content',$escort->seo_content,array('class'=>'form-control')) }}
            </div>
        </div>
    </div>
</div>
{{ Form::hidden('escort_id',$escort->id) }}

<div class="alignc">
    {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content')) }}
</div>

{{ Form::close() }}
@stop