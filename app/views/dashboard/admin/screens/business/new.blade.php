<div class ='_MpoffWhite businessProfileScreen'>
 <div>
   <h2>New Business</h2>
   <div class ='mPProfileTopMenu hidden'>
     <div><div id ='mpBasicInfo' class ='active'></div><span>BASIC INFO</span></div>
     <div><div id ='mpProfileDetails'></div><span>PROFILE DETAILS</span></div>
     <div><div id ='mpPayment'></div><span>PAYMENT</span></div>
   </div>
 </div>
 <div class ='_mpProfile_holder'>
  <message></message>
  <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
   <h3>BASIC INFORMATION</h3>
   <form role="form" controller ='busSaveBasic' ng-submit = 'businessInfo.$valid && newAdminBusiness()' id = 'businessInfo' name = 'businessInfo' ng-model = 'busBasicInfoForm' novalidate="">
    <div class ='mp_image_float' >
      <div id="container_image">
        <img src ='/assets/dashboard/images/escortHolderImage.jpg' class ='full_width'>
      </div>
    </div>
    <div class ='right_float'>
      {{ Form::hidden('image','',array('ng-model'=>'busBasicInfoForm.image', 'id' => 'image')) }}

      {{ Form::text('name','',array('id' => 'name', 'class'=>'business-name input-xlarge input03','placeholder'=>'Business Name','ng-model'=>'busBasicInfoForm.name','required'=>'')) }}

      {{ Form::text('phone','',array('ng-model'=>'busBasicInfoForm.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'Business Phone')) }}
      
      {{ Form::text('email','',array('id' => 'email', 'class'=>'business-email input-xlarge input03','placeholder'=>'Business Email','ng-model'=>'busBasicInfoForm.email')) }}
    </div>
    <div class ='bottom_inputs'>
     <span class ='larger'>User Id - Leave at 1 for an admin account</span>
     {{ Form::text('user_id','1',array('ng-init'=>"busBasicInfoForm.user_id = '1'",'ng-model'=>'busBasicInfoForm.user_id','placeholder' => 'User id','class'=>'input-xlarge input03')) }}
     <span>Business Type</span>
     {{ Form::select('type',$type, '',array('id'=>'busType', 'ng-model' => "busBasicInfoForm.type") )}}
     

     <div class="alignc">
      {{ Form::submit('SAVE',array('class'=>'submit-button')) }}
      <?php /* {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }} */?>

    </div>
    {{ Form::close() }}
  </div>
</div>
