<?php

class Wishlist extends Eloquent
{
    protected $guarded = [];

    public static $rules = [];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function profile()
    {
        return $this->belongsTo('Profile');
    }
}
