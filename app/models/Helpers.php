<?php

class Helpers {

  // VARIABLES

  public static $weekDays = [
    1 => 'Mon',
    2 => 'Tue',
    3 => 'Wed',
    4 => 'Thu',
    5 => 'Fri',
    6 => 'Sat',
    7 => 'Sun'
  ];

  public static $dayHours =
  [  
	'12:00 AM',
    '01:00 AM',   
    '02:00 AM',    
    '03:00 AM',    
    '04:00 AM',    
    '05:00 AM',    
    '06:00 AM',    
    '07:00 AM',    
    '08:00 AM',    
    '09:00 AM',
    '10:00 AM',    
    '11:00 AM',    
    '12:00 PM',
	'01:00 PM',   
    '03:00 PM',    
    '04:00 PM',    
    '02:00 PM',    
    '05:00 PM',    
    '06:00 PM',    
    '07:00 PM',    
    '08:00 PM',    
    '09:00 PM',
    '10:00 PM',    
    '11:00 PM'   
  ];

  public static $rateDurations =
  [
    '15' => [
      'incall_15_rate' => '15 min',
      'outcall_15_rate' => '15 min'
    ],
    '30' => [
      'incall_30_rate' => '30 min',
      'outcall_30_rate' => '30 min'
    ],
    '45' => [
      'incall_45_rate' => '45 min',
      'outcall_45_rate' => '45 min'
    ],
    '1h' => [
      'incall_1h_rate' => '1 hour',
      'outcall_1h_rate' => '1 hour'
    ],
    '2h' => [
      'incall_2h_rate' => '2 hours',
      'outcall_2h_rate' => '2 hours'
    ],
    '3h' => [
      'incall_3h_rate' => '3 hours',
      'outcall_3h_rate' => '3 hours'
    ],
    'overnight' => [
      'incall_overnight_rate' => 'Overnight',
      'outcall_overnight_rate' => 'Overnight'
    ],
  ];

  public static $states =
  [
    'nsw',
    'qld',
    'nt',
    'wa',
    'sa',
    'vic',
    'tas'
  ];

  public static function user_date_to_db_date($date)
  {
    $date = str_replace('/', '-', $date);
    return date('Y-m-d',strtotime($date));
  }

  public static function db_date_to_user_date($date)
  {
    return date('d/m/Y',strtotime($date));
  }

  public static function db_date_to_user_datetime($date)
  {
    return date('d/m/Y H:i:s',strtotime($date));
  }

  public static function db_date_to_user_datetime_or_empty($date)
  {
    if ($date == '0000-00-00 00:00:00')
    {
      return '';
    }
    return static::db_date_to_user_datetime($date);
  }

  public static function get_status($status)
  {
    if ($status == 1)
      return '<span class="label label-warning">Pending</span>';
    if ($status == 2)
      return '<span class="label label-success">Approved</span>';
    if ($status == 3)
      return '<span class="label label-danger">Disapproved</span>';
    if ($status == 4)
      return '<span class="label label-unavailable">Unavailable</span>';
  }

  public static function get_active($status)
  {
    if ($status == 0)
      return '<span class="label label-warning">No</span>';
    if ($status == 1)
      return '<span class="label label-success">Yes</span>';
  }

  public static function string_to_url($text)
  {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text))
    {
    return 'n-a';
    }

    return $text;
  }

  public static function format_phone_number($number)
  {
    if(substr($number, 0, 2) == '02')
      return preg_replace('/(\\d{2})(\\d{4})(\\d{4})/', '$1 $2 $3', $number);
    if(substr($number, 0, 2) == '04')
      return preg_replace('/(\\d{4})(\\d{3})(\\d{3})/', '$1 $2 $3', $number);
    
    return $number;
  }

  public static function latestCss($file)
  {
      $list = json_decode(File::get(base_path().'/public_html/assets/frontend/css-manifest.json'));
      return '<link rel="stylesheet" type="text/css" href="/assets/frontend/css/builds/' . $list->$file . '">';
  }

  public static function latestJs($file)
  {
      $list = json_decode(File::get(base_path().'/public_html/assets/frontend/js-manifest.json'));
      
      if (Route::getCurrentRoute()->uri() == '/')
          return '<script async id="build-js" src="/assets/frontend/js/builds/' . $list->$file . '"></script>';
      else
          return '<script id="build-js" src="/assets/frontend/js/builds/' . $list->$file . '"></script>';
  }



}














