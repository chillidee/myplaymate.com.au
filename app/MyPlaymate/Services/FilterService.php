<?php
namespace MyPlaymate\Services;

use MyPlaymate\Repositories\SuburbRepository;
use MyPlaymate\Filters\Filter;
use MyPlaymate\Filters\GenderFilter;
use MyPlaymate\Filters\HairColorFilter;
use MyPlaymate\Filters\EscortNameFilter;
use MyPlaymate\Filters\LocationFilter;
use MyPlaymate\Services\LocationService;


class FilterService
{
	protected $filter;

	public function execute($escorts,$type,$value)
	{
		$filter = $this->chooseFilter($type);
		
		if ($filter == null)
			return $escorts;
			
		return $filter->apply($escorts,$value);
	}

	public function chooseFilter($type)
	{
		if ($type == 'escort_name')
			return new EscortNameFilter;
		if ($type == 'age')
			return new AgeFilter;
		if ($type == 'gender')
			return new GenderFilter;
		if ($type == 'hair_color')
			return new HairColorFilter;
		if ($type == 'suburb_id')
			return new LocationFilter(new LocationService(new SuburbRepository));
		else
			return;
	}
}






