<?php

class LocationController extends Controller {

	public function mapModal()
	{
		return View::make('frontend.partials.modals.map');
	}

	public function getGeoIds()
	{
		$suburb = Suburb::wherePostcode(Input::get('postcode'))->first();
		$state_id = $suburb->state->id;

		return [
			'suburb_id'=>$suburb->id,
			'state_id'=>$state_id
		];
	}

	public function getStateAndCities()
	{
		$state = State::where('short_name','=',Input::get('state_name'))->first();
		if ( !$state )
			  $state = State::where( 'id','=','1' )->first();
		$cities = City::whereStateId($state->id)->get();
		return ['state_name'=>$state->short_name, 'cities'=> $cities];
	}

	public function getSuburbCoordinates()
	{
		$suburb = Suburb::find(Input::get('suburb_id'));
		return ['lat' => $suburb->lat, 'lng' => $suburb->lng];
	}

	public function getPostcodeInfo()
	{
		$postcode = Input::get('postcode');
		$suburb = Suburb::wherePostcode($postcode)->first();
		$state = $suburb->state;
		return ['suburb'=>$suburb, 'state'=>$state];
	}

	public function getSuburbInfo()
	{
		$suburb_id = Input::get('suburb_id');
		$suburb = Suburb::find($suburb_id);
		$state = $suburb->state;
		$postcode = $suburb->postcode;
		return ['suburb'=>$suburb, 'state'=>$state, 'postcode'=>$postcode];
	}
	public function get_cityLatLng() {
		$id = Input::get('city');
		$city = City::where( 'id','=',$id )->first();
		return $city;
	}
}