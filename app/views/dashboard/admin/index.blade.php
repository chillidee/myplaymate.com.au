@extends('dashboard.layout')

@section('content')

@include('dashboard.partials.mobile-menu')

<script>
     var id = {{$user->id}},
         mPActiveIcon = '_MpOverview',
         profiletabid = 'basicInfo',
         cache = {},
         currentEscort = '1',
         currentBusiness ='2',
         type = 'admin';
         suburbId = '0', 
         curEscAvail = 0,
         currentProfileAction = 'Edit';
         window.active_id = 2;
</script>
  <div class = 'container-fluid'>
    <div class ='row top_header'>
      <div class ='col-sm-3 no_padding'>
          <div id ='_mp_brand_container'>
            <div id ='logo'>
              <a href ='/'><img src ='/assets/dashboard/images/mpmAv.png' alt ='MPM'></a>
            </div>
            <div id ='_mp_areaName'>
           <span>{{$user->full_name}}</span>

            </div>
          </div>
    </div>
    <div class ='col-sm-9 no_padding'>
      <div id ='_mp_prof_header'>
      <span class ='dashTopLink'><a href ='/admin'>Go to admin area</a></span>
        <div id ='profile_menu'>
          <ul id ='profile_dropdown' class ='dropCaret'>
      @if ( isset( $businesses[0] ) )
           @foreach( $businesses as $business)
              <li>@if ( $business->image )
              <div id ='_mp_img_holder'>
                      <img src ='/uploads/businesses/{{$business->image}}'>
              </div>@endif
                  {{ $business->name}}
              </li>
           @endforeach
      @endif
           <li class ='signout'><a href ='/logout'>Logout</a></li>
          </ul>
        </div>
      </div>
    </div>
</div>
<div class ='row main_content'>
 <div>
   <section>
    <div class ='col-md-3 no_padding _MpDetailArea panelFixed'>
      <div id ='_mpMenu'>
        <div ng-repeat="item in busMenu" on-finish-render="test">
          <div ng-click ="changeScreens(item.id)" id = '@{{ item.id }}_' class ='_mPIcon' title="@{{item.title}}" stateId = "@{{ item.id }}" menuicon>@{{item.item}}</div>
        </div>
      </div>
             <panel panelid = "@{{activeicon}}"></panel>
       </div>
      <div class ='col-lg-9 col-lg-offset-3 no_padding _MpScreenArea'>
             <screen screenid = "@{{activeicon}}"class ='screen'></screen>
      </div>
      </section>  
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVAKU7LtUy9yWGX_uoP1cJ9kW0Hqhf754&signed_in=true&libraries=places"
        async defer></script>
        <script type="text/javascript" src="https://cdn.datatables.net/t/dt/jq-2.2.0,dt-1.10.11/datatables.min.js"></script>
        <script src="/assets/dashboard/js/jqueryui.min.js"></script>
        <script src="/assets/dashboard/js/fastclick.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script src="/assets/dashboard/js/angular.cropper.js"></script>
        <script src="/assets/dashboard/js/ng-img-crop.js"></script>
        <script src="/assets/dashboard/js/picturecut.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
        <script src="/assets/dashboard/js/angular-sortable-view.min.js"></script>
        <script src="/assets/dashboard/js/angular-spinkit.min.js"></script>
        <script src="/assets/dashboard/js/app.js"></script>
        <script src="/assets/dashboard/js/services.js"></script>
        <script src="/assets/dashboard/js/controller.js"></script>
        <script src="/assets/dashboard/js/httpInteceptor.js"></script>
        <script src="/assets/dashboard/js/ng-file-upload-shim.min.js"></script> <!-- for no html5 browsers support -->
        <script src="/assets/dashboard/js/ng-file-upload.min.js"></script>
        <script src="/assets/dashboard/js/jquery-ui-Slid.min.js"></script>
        <script src ="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.3.5/parsley.min.js"></script>
        <script src="/assets/dashboard/js/mobile-menu.js"></script>
        <link rel="stylesheet" href="/assets/dashboard/css/jquery-ui-min.css" type="text/css" />

      </div>
</div>
</div>
@stop
