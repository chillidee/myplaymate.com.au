<?php
use MyPlaymate\Repositories\UserRepository;
use GuzzleHttp\Client;
use MyPlaymate\Repositories\SuburbRepository;
use  Carbon\Carbon;

class ServiceController extends \ProfileController
{

    public function suburbSelect() {
        $suburbs = $this->suburbRepo->populateSelect();
        $suburbId = Input::get('suburbId');
        return View::make('dashboard.profile.suburbSelect',compact('suburbs','suburbId'));
    }

    public function updateReviewStatus() {
        $user = Auth::user();
        $type = $user->getUserType();
        if ( !$type == 1 && $user->id != $_POST['userid'] )
            die();

        $input =  Response::json(Input::all());
        $input = json_decode( $input->getContent());
        $review = EscortReview::where( 'id','=', $input->id)->first();
        $review->published = $input->action;
        $review->save();
    }

    public function mpmApi() {
		try {
        $user = Auth::user();
        $type = $user->getUserType();
        if ( !$type == 1 && $user->id != $_POST['userid'] )
            die();

        $input =  Response::json(Input::all());
        $input = json_decode( $input->getContent());
     if ( $input->action == 'delete') {
        $table = $input->table;
        $del = $table::where('id','=',$input->where->id)->delete();
           if ( $table == 'EscortTouring'){
                  $tour = EscortTouring::where('escort_id','=',$input->value->escort_id)->where('to_date','>=',date('Y-m-d H:i:s'))->get();
                  if ( $tour ){
                  $blockedDates = array();
                     if ( count( $tour ) > 0){
                        foreach($tour as $touring){
                           $begin = new DateTime( $touring->from_date );
                           $end = new DateTime( $touring->to_date );
                           $interval = DateInterval::createFromDateString('1 day');
                           $period = new DatePeriod($begin, $interval, $end);
                           foreach ( $period as $dt ){
                                 $blockedDates[] = $dt->format( "d-m-Y" );
                           }
                      $touring->from_date = Helpers::db_date_to_user_date($touring->from_date);
                      $touring->to_date = Helpers::db_date_to_user_date($touring->to_date);
             }
            return compact( 'blockedDates');
            }
          }
        }
     }
     else if ( $input->action == 'create') {
        $table = $input->table;
        $new = new $table;
        foreach ( $input->value as $key => $value ) {
           if ( $key == 'postcode' ) {
                 if ( $value != '' ) {
                     $suburb_id = Suburb::where('postcode','=',$value)->first();
                     $new->suburb_id = $suburb_id->id;
                  }
                }
            else if ( $key == 'from_date' || $key == 'to_date') {
                   $bits = explode( '-',$value );
                   $new->$key = $bits[2].'-'.$bits[1].'-'.$bits[0].' 00:00:00';
                }
            else {
               $new->$key = $value;
             }
        }
        if ( $table == 'Escort'){
            $new->bumpup_date = Carbon::now();
      }
        $new->save();
        $id = $new->id;
        if ( $table == 'Escort') {
             $rates = new EscortRate;
             $rates->escort_id = $id;
             $rates->save();
             $this->rewriteSeoUrl($id);
           }
         if ( $table == 'Business') {
             $this->rewriteBusUrl($id);
           }
        elseif ( $table == 'EscortTouring' && $input->action =='create'){
                  $tour = EscortTouring::where('escort_id','=',$input->value->escort_id)->where('to_date','>=',date('Y-m-d H:i:s'))->get();
                 if ( $tour ){
                 $blockedDates = array();
                     if ( count( $tour ) > 0){
                        foreach($tour as $touring){
                           $begin = new DateTime( $touring->from_date );
                           $end = new DateTime( $touring->to_date );
                           $interval = DateInterval::createFromDateString('1 day');
                           $period = new DatePeriod($begin, $interval, $end);
                           foreach ( $period as $dt ){
                                 $blockedDates[] = $dt->format( "d-m-Y" );
                           }
                   $touring->from_date = Helpers::db_date_to_user_date($touring->from_date);
                   $touring->to_date = Helpers::db_date_to_user_date($touring->to_date);
             }
            return compact( 'blockedDates','id' );
          }
         }
        }
        return $id;

     }
     else if ( $input->action == 'update') {
            $table = $input->table;
            foreach ( $input->where as $key => $value) {
              $target = $table::where( $key,'=',$value);
              break;
            }
            $i = 0;
            foreach ( $input->where as $key => $value ) {
            if ( $i != 0 )
                  $target->where( $key, "=", $value);
                  $i++;
                }
            $ret = $target->first();
            foreach ( $input->value as $key => $value ) {
              if ( $key == 'postcode' ) {
                 if ( $value != '' ) {
                     $suburb_id = Suburb::where('postcode','=',$value)->first();
                      $ret->suburb_id = $suburb_id->id;
                  }
                }
                else if ( $key == 'website' ) {
                 if ( $value != '' ) {
                     $value = str_replace ( 'http://','', $value );
                     $ret->$key = rtrim( str_replace ( 'https://','', $value ),'/' );
                  }
                }
               else if ( $key == 'height' ) {
                 if ( $value != '' ) {
                    if ( $value > 149 && $value < 160 ) {
                         $height_id = 1;
                    }
                    elseif ( $value > 159 && $value < 170 ) {
                         $height_id = 2;
                    }
                    elseif ( $value > 169 && $value < 180 ){
                         $height_id = 3;
                    }
                    elseif ( $value > 179 && $value < 180 ){
                         $height_id = 4;
                    }
                    elseif ( $value > 189 && $value < 190 ){
                         $height_id = 4;
                    }
                    else { $height_id = 5;}

                      $ret->height_id = $height_id;
                      $ret->height = $value;
                  }
                }
                else if ( $key == 'services') {
                      EscortService::whereEscortId( $input->where->id )->delete();
                      foreach (reset($value) as $service) {
                         $escortService = new EscortService();
                         $escortService->escort_id = $input->where->id;
                         $escortService->service_id = $service;
                         $escortService->save();
                      }
                }
                else if ( $key == 'languages') {
                      EscortLanguage::whereEscortId( $input->where->id )->delete();
                      foreach ($value->escortLanguages as $language) {
                         $escortLanguage = new EscortLanguage();
                         $escortLanguage->escort_id = $input->where->id;
                         $escortLanguage->language_id = $language;
                         $escortLanguage->save();
                      }
                }
                else if ( $key == 'availabilites'){
                         for( $i = 1; $i < 8; $i++ ){
                           $available = EscortAvailability::where( 'escort_id','=',$input->where->id )->where('day','=',$i)->first();
                           if (!$available) {
                              $available = new EscortAvailability;
                              $available->escort_id = $input->where->id;
                              $available->day = $i;
                           }
                           if ( isset( $value->start_am_pm[$i]  ) )$available->start_am_pm = $value->start_am_pm[$i];
                           if ( isset( $value->end_am_pm[$i]  ) )$available->end_am_pm = $value->end_am_pm[$i];
                           if ( isset( $value->start_hour_id[$i] ) )$available->start_hour_id = $value->start_hour_id[$i];
                           if ( isset( $value->end_hour_id[$i]  ) )$available->end_hour_id = $value->end_hour_id[$i];
                           if ( in_array ( $i, $value->twenty_four_hours ) ){ $available->twenty_four_hours = 1; } else { $available->twenty_four_hours = 0; }
                           if ( in_array( $i, $value->avail ) ){ $available->enabled = 1; } else { $available->enabled = 0; }
                           $available->save();
                         }
                }
                else if ( $key == 'rates'){					
					try {
						$rates = EscortRate::where( 'escort_id','=',$input->where->id )->first();
						$i = 0;
						foreach( $value as $rateHeader => $rate ){
							$rates->$rateHeader = $rate;
							$i++;
						}
					   // return $value;
						$rates->save();
					} catch (Exception $e) {						
						$rates = new EscortRate;
						$rates->escort_id = $input->where->id;
						$rates->save();

						$rates = EscortRate::where( 'escort_id','=',$input->where->id )->first();
						$i = 0;
						foreach( $value as $rateHeader => $rate ){
							$rates->$rateHeader = $rate;
							$i++;
						}
					   // return $value;
						$rates->save();
					}
                }
                else if ( $key == 'images') {
                    $i = 0;
                    foreach ( $value as $key => $image ) {
                      if( isset( $image->newImage ) ){
                            ImageHelper::s3thumbnail($image->filename,'escorts/thumbnails',220,330);
                            ImageHelper::s3thumbnail($image->filename,'escorts/thumbnails',170,170);
                            ImageHelper::s3Upload( $image->filename, 'escorts/');

                          $escortImage = new EscortImage;
                          $escortImage->escort_id = $input->where->id;
                          $escortImage->status = 1;
                          $escortImage->filename = $image->filename;
                      }
                      else {
                          $escortImage = EscortImage::where( 'id','=',$image->id )->first();
                      }
                      $escortImage->order = $i;
                      $escortImage->save();
                      $i++;
                    }
                }
                else if ( $key == 'newImage') {
                    $i = 0;
                    foreach ( $value as $key => $image ) {
                      if( isset( $image->newImage ) ){
                      /*     $key = 'escorts/' . $image->filename;
                           $s3 = App::make('aws')->get('s3');
                           $s3->putObject(array(
                             'Bucket'     => 'mpm-uploads',
                             'Key'        => $key,
                             'SourceFile' => Config::get('constants.PUBLIC_DIR') . '/uploads/temp/' . $image->filename,
                           ));*/
                            ImageHelper::s3Upload( $image->filename, 'businesses/' );
                            ImageHelper::s3thumbnail($image->filename,'escorts/thumbnails', 220,330);
                            ImageHelper::s3thumbnail($image->filename,'escorts/thumbnails', 170,170);
                      $ret->image = $image->filename;
                      $i++;
                      }
                    }
                }
                 else if ( $key == 'new_banner' ) {
                    continue;
                }
                 else if ( $key == 'banner_image' ) {
                  if ( $value == '' ){
                      $image = $table::where('id','=',$input->where->id)->first()->banner_image;
                      if ( $table == 'Business') $table = 'Businesse';
                      ImageHelper::s3Delete( strtolower( $table ) . 's/', $image );
                    //  if ( $user->superuser)
                      $ret->$key = $value;
                      $ret->banner_approved = 1;
                  }
                  else if ( isset( $input->value->new_banner ) ) {
                      if ( $table == 'Business') $table = 'Businesse';
                      ImageHelper::s3Upload( $value, strtolower( $table ) . 's/' );
                      $ret->$key = $value;
                      $user = Auth::user();
                      if ( $user->super_user == 1 ){
                          $ret->banner_approved = 2;
                      }
                      else {
                          $ret->banner_approved = 1;
                      }
                  }
                }
                else if ( $key == 'main_image' ) {
                  if ( substr( $value, 0, 9 ) === "newImage_" ){
                      $escortImage = EscortImage::where( 'filename','=', substr($value, 9) )->first();
                      $ret->main_image_id = $escortImage->id;
                  }
                  else { $ret->main_image_id = $value; }

                }
                else if ( $key == 'deletedImages'){
                  foreach( $value as $im ){
                     EscortImage::where('id','=', $im->id )->delete();
                     ImageHelper::s3Delete( 'escorts/', $im->filename );
                     $escort = Escort::find( $input->where->id );
                     if ( $escort->main_image_id == $im->id ){
                        $escort->main_image_id = '';
                        $escort->save();
                     }
                  }
                }
                else if ( $key == 'password') {
                   $ret->password = Hash::make($value);
                }
                 else if ( $key == 'from_date' || $key == 'to_date') {
                   $bits = explode( '-',$value );
                   $ret->$key = $bits[2].'-'.$bits[1].'-'.$bits[0].' 00:00:00';
                }


              else {
                 $ret->$key = $value;
              }
          }
              $ret->save();
              if ( $table == 'Escort' && $input->action != 'delete' ){
                $this->rewriteSeoUrl($input->where->id);
              }
              elseif ( $table =='Business' && $input->action != 'delete'){
                $this->rewriteBusUrl($input->where->id);
              }
           }
		} catch(Exception $e) {
			echo 'Error: ' . $e->getMessage() . ' - Stack Trace: ' . $e->getTraceAsString();			
		}
    }
    public function configureLocations() {

      set_time_limit(0);
      ini_set('memory_limit','246M');

      $escorts = Escort::get();
      foreach ( $escorts as $escort ){
        $suburb = Suburb::where('id','=',$escort->suburb_id )->first();
        $escort->suburb_name = $suburb->name;
        $escort->latitude = $suburb->lat;
        $escort->longitude = $suburb->lng;
        $state = State::where( 'id', '=', $suburb->state_id )->get()->first();
        $escort->state = $state->short_name;
        $escort->save();
      }
    }
    public function rewriteSeoUrl($id){
      $escort = Escort::where('id','=', $id)->get()->first();
      $ethnicity = Ethnicity::where('id','=',$escort->ethnicity_id )->first();
      if ( !$ethnicity ){
          $ethnicity = new Ethnicity;
          $ethnicity->name = '';
        }
      if ( $escort->suburb_name ){
         $url = str_replace ( ' ', '-', strtolower ( $escort->suburb_name ) ) . '-' . strtolower ( $escort->state ) . '-' . str_replace ( ' ', '-', strtolower ( $escort->escort_name ) ) . '-' . str_replace ( ' ', '-', strtolower ( $ethnicity->name ) ) . '-' . $escort->id;
         $escort->seo_url = $url;
         $escort->save();
      }
    }
    public function rewriteBusUrl($id){
      $business = Business::where('id','=', $id)->get()->first();
      if ( $business->suburb_name ){
         $url = str_replace ( ' ', '-', strtolower ( $business->suburb_name ) ) . '-' . strtolower ( $business->state ) . '-' . str_replace ( ' ', '-', strtolower ( $business->name ) ) . '-' . $business->id;
         $business->seo_url = $url;
         $business->save();
      }
    }
    public function contactChangePlan() {
	  try {
      $input = Input::get();
      $user = User::where( 'id','=',$input['user_id'])->first();
      $businessPlan = BusinessPlan::where('id','=',$input['plan_id'] )->first();
      $currentPlan = BusinessPlan::where('id','=',$user->business_plan )->first();
      $user->business_plan = $input['plan_id'];
      $user->save();
      $data = array( 'name' => $user->full_name,
                     'email' => ( !App::environment('development') ? $user->email : Config::get('constants.DEV_EMAIL') ),
                     'phone' => $user->phone,
                     'business_plan' => $businessPlan->name,
                     'current_plan' => $currentPlan->name );

      Mail::send('emails.upgradePlan', ['data' => $data], function ($message) use ($data) {
                $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                        ->subject('User requested plan upgrade')
                        ->to('contact@myplaymate.com.au')
                        ->replyto($data['email']);
            });
      return $businessPlan;
	  } catch(Exception $e) {
		echo 'Error: ' .$e->getMessage();
	  }
    }
    public function uploadTempImage() {
      $user = Auth::user();
      if ( !$user)
            die();
      $file = Input::file('file');
    //  return $file;
     if(substr($file->getMimeType(), 0, 5) !== 'image') {
           die();
     }
      $filename = str_random(12);
      $destinationPath = public_path().'/temp/';
      $file->move($destinationPath, $filename . '.png');
      $image = imagecreatefrompng($destinationPath . $filename . '.png');
      $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
      imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
      imagealphablending($bg, TRUE);
      imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
      imagedestroy($image);
      $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file
      imagejpeg($bg, $destinationPath . $filename . ".jpg", $quality);
      imagedestroy($bg);
      return $filename . '.jpg';
    }
     public function uploadTempGalleryImage() {
      $user = Auth::user();
      if ( !$user)
            die();
      $file = Input::file('file');
     if(substr($file->getMimeType(), 0, 5) !== 'image') {
           die();
     }
      $filename = str_random(12);
      $destinationPath = public_path().'/temp/';
      $file->move($destinationPath, $filename . '.jpg');
      return $filename . '.jpg';
    }

   public function getPlan($id){
      $user = Auth::user();
      return BusinessPlan::where('id','=',$user->business_plan )->first();
   }

    public function apitest() {

     $url = 'http://crm2.myplaymate.com.au/rest/index.php?_rest=customers';

     $apiKey = openssl_encrypt(Config::get('constants.CRMAPIKEY'), "AES-256-CBC", Config::get('constants.CRMAPIHASH'),true, Config::get('constants.CRMIV') );

     $fields = array(
            'crmApiKey' => $apiKey,
            'bname'=>'apitest',
            'bemail'=> 'che@imaginatewebsites.com.au',
            'extend' => 'test'
        );

    //url-ify the data for the POST
    $fields_string = '';
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string,'&');

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);

   // ob_start();

    $result = curl_exec($ch);

    $ret = ob_get_clean ();

    $var = json_decode( $ret, true );

    $url = 'http://crm2.myplaymate.com.au/rest/index.php?_rest=status';

    $fields = array(
            'crmApiKey' => $apiKey,
            'statusId'  => 1,
            'datetime'  => Carbon::now(),
            'dueDatetime' => Carbon::now(),
            'userId' => 253,
            'customerId' => $var['object']['id'],
            'latest' => 1
        );
        $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);

    ob_start();

    $result = curl_exec($ch);

    ob_clean ();

    }

    public function busLogoUpload() {
        $user = Auth::user();
        $type = $user->getUserType();
        if ( !$type == 1 && $user->id != $_POST['userid'] )
        die();
        include(app_path().'/includes/pictureCut/PictureCut.php');

        try {

             $pictureCut = PictureCut::createSingleton();

             if($pictureCut->upload()){
             print $pictureCut->toJson();
             } else {
              print $pictureCut->exceptionsToJson();
             }

             } catch (Exception $e) {
                print $e->getMessage();
             }

    }
    public function busLogoCrop() {
  include(app_path().'/includes/pictureCut/PictureCut.php');

try {

  $pictureCut = PictureCut::createSingleton();

  if($pictureCut->crop()){
    print $pictureCut->toJson();
  } else {
     print $pictureCut->exceptionsToJson(); //print exceptions if the upload fails
    }

} catch (Exception $e) {
  print $e->getMessage();
}
    }
    public function softCron() {

    $dir = public_path(). '/temp';
    $files = scandir($dir);
    $cnt = count($files);
      echo $cnt;
    $deadline = strtotime('now')-3600;
    for($i = 0; $i < $cnt; ++$i)
    {
        $files[$i] = $dir.'/'.$files[$i];
        if(!is_file($files[$i]) || $files[$i] == '.' ||
            $files[$i] == '..' || filemtime($files[$i]) >= $deadline){
            echo filemtime($files[$i]) . '-' . $deadline . '<br>';
            unset($files[$i]);
    }
      else{

            unlink($files[$i]);
      }
    }
    $dir = public_path(). '/temp/thumbnails';
    $files = scandir($dir);
    $cnt = count($files);
      echo $cnt;
    $deadline = strtotime('now')-3600;
    for($i = 0; $i < $cnt; ++$i)
    {
        $files[$i] = $dir.'/'.$files[$i];
        if(!is_file($files[$i]) || $files[$i] == '.' ||
            $files[$i] == '..' || filemtime($files[$i]) >= $deadline){
            echo filemtime($files[$i]) . '-' . $deadline . '<br>';
            unset($files[$i]);
    }
      else{

            unlink($files[$i]);
      }
    }
     $plans = BusinessPlan::select('*')->where('bumpups','>','0')->get();
     foreach( $plans as $plan )
        $users = User::where( 'business_plan','=',$plan->id )->get();
        foreach( $users as $user ){
           $escort = Escort::where( 'user_id','=',$user->id )->first();
          if ( strtotime( $escort->bumpup_date ) < strtotime( Carbon::now() ) - ( ( 60*60*24*28 ) / $plan->bumpups ) )
            $escort->bumpup_date = Carbon::now();
            $escort->save();
        }

    }

}
