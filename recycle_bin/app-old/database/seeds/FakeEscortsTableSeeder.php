<?php

class FakeEscortsTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,60) as $index) {
            $escort = [
                'escort_name'       => $faker->name('female'),
                'status'            => $faker->numberBetween(1,3),
                'phone'             => $faker->phoneNumber,
                'email'             => $faker->email,
                'suburb_id'         => $faker->numberBetween(1,10000),
                'age_id'            => $faker->numberBetween(1,53),
                'gender_id'         => $faker->numberBetween(1,3),
                'ethnicity_id'      => $faker->numberBetween(1,10),
                'body_id'           => $faker->numberBetween(1,10),
                'hourly_rate_id'    => $faker->numberBetween(1,6),
                'does_incalls'      => $faker->numberBetween(0,1),
                'does_outcalls'     => $faker->numberBetween(0,1),
                'does_travel_internationally' => $faker->numberBetween(0,1),
                'hair_color_id'     => $faker->numberBetween(1,7),
                'eye_color_id'      => $faker->numberBetween(1,6),
                'height_id'         => $faker->numberBetween(1,5),
                'tattoos'           => $faker->numberBetween(0,1),
                'piercings'         => $faker->numberBetween(0,1),
                'does_smoke'        => $faker->numberBetween(0,1),
                'main_image_id'     => $index,
                'featured'          => $faker->numberBetween(0,1),
                'about_me'          => $faker->realText(400),
                'created_at'        => $faker->dateTimeThisYear('now')
            ];
            DB::table('escorts')->insert($escort);

            $escort = Escort::orderBy('id','DESC')->first();
            $escort->seo_url = $escort->getSeoUrl();
            $escort->save();
        }
        $escort = Escort::find(1);
        $escort->user_id = 7;
        $escort->save();
    }
}
