if ( typeof thisCity == 'undefined') {
           var thisCity = '';      
}
if ( typeof pageSuburb != 'undefined' ) {
           thisCity = pageSuburb;
}
    function removeFilters(e) {

    var usedFilters = $('#used-filters'),
        unusedFilters = $('#escort-filters'),
        usedFiltersContainer = $('#used-filters-container');
        // Hide
        unusedFilters.find('ul li').hide();     
        e.preventDefault();
       // e.stopPropogation();
        usedFilters.children('li').not('.used-filter-template').remove();

        unusedFilters.find('ul li').slideUp(200);

        usedFiltersContainer.slideUp(200);

        $( '#escort_side-search').val('');

        applyFilters();

        return false;
    }

function applyFilters(page){
    $( '#load_more_escorts' ).remove();
    $( '.touring-container').empty();
    $( '#touring-container h2').remove();
    var view = Cookies.get('view') || 'grid',
        longitude = longitude,
        latitude = latitude,
        distance = window._distance,
        business = jQuery('body').data('business'),
        usedFilters = jQuery('#used-filters'),
        ageFilter   = usedFilters.find('#age'),
        genderFilter = usedFilters.find('#gender'),
        ethnicityFilter = usedFilters.find('#ethnicity'),
        bodyFilter = usedFilters.find('#body'),
        hourlyRatesFilter = usedFilters.find('#hourly-rates'),
        servicesFilter = usedFilters.find('#services'),
        incallsOutcallsFilter = usedFilters.find('#incalls-outcalls'),
        internationalFilter = usedFilters.find('#international-travel'),
        heightFilter = usedFilters.find('#heights'),
        hairColorFilter = usedFilters.find('#hair-color'),
        eyeColorFilter = usedFilters.find('#eye-color'),
        bodyArtFilter = usedFilters.find('#body-art'),
        smokeFilter = usedFilters.find('#smokes'),

        escort_name = getUrlVars()['s'] || jQuery('#search-box').val(),
        age = [],
        gender = [],
        ethnicity = [],
        body = [],
        hourly_rates = [],
        services = [],
        incallsOutcalls = [],
        international = '',
        height = [],
        hair_color = [],
        eye_color = [],
        tattoos = '',
        piercings = '',
        smoke = '';

    var input = [];

    if (ageFilter.length) {
        jQuery.each(ageFilter.find('li'), function(index, val) {
            var range = ageFilter.find('ul a')[index].id;
            age.push(makeAgeQuery(range));
        });
    };
    if (genderFilter.length) {
        jQuery.each(genderFilter.find('li'), function(index, val) {
            gender.push(genderFilter.find('ul a')[index].text);
        });
    };
    if (ethnicityFilter.length) {
        jQuery.each(ethnicityFilter.find('li'), function(index, val) {
            ethnicity.push(ethnicityFilter.find('ul a')[index].text);
        });
    };
    if (bodyFilter.length) {
        jQuery.each(bodyFilter.find('li'), function(index, val) {
            body.push(bodyFilter.find('ul a')[index].text);
        });
    };
    if (hourlyRatesFilter.length) {
        jQuery.each(hourlyRatesFilter.find('li'), function(index, val) {
            var range = hourlyRatesFilter.find('ul a')[index].id;

            hourly_rates.push(makeHourlyRatesQuery(range));
        });
    };
    if (servicesFilter.length) {
        jQuery.each(servicesFilter.find('li'), function(index, val) {
            services.push(servicesFilter.find('ul a')[index].text);
        });
    };
    if (incallsOutcallsFilter.length) {
        jQuery.each(incallsOutcallsFilter.find('li'), function(index, val) {
            incallsOutcalls.push(incallsOutcallsFilter.find('ul a')[index].text);
        });
    };
    if(internationalFilter.length)
        international = 1;

    if (heightFilter.length) {
        jQuery.each(heightFilter.find('li'), function(index, val) {
            height.push(heightFilter.find('ul a')[index].text);
        });
    };
    if (hairColorFilter.length) {
        jQuery.each(hairColorFilter.find('li'), function(index, val) {
            hair_color.push(hairColorFilter.find('ul a')[index].text);
        });
    };
    if (eyeColorFilter.length) {
        jQuery.each(eyeColorFilter.find('li'), function(index, val) {
            eye_color.push(eyeColorFilter.find('ul a')[index].text);
        });
    };
    if (bodyArtFilter.length) {
        if (bodyArtFilter.find('#tattoos').length){
            tattoos = 1;
        }
        if (bodyArtFilter.find('#piercings').length)
            piercings = 1;
    };
    if (smokeFilter.length) {
        if (smokeFilter.find('#yes').length)
            smoke = 1;
        else
            smoke = 0;
    }; 

    if ( typeof( Cookies.get('latitude') ) != 'undefined' || typeof( Cookies.get('latitude') ) != ''  ){
          window.latitude = Cookies.get('latitude'),
          window.longitude = Cookies.get( 'longitude' );
    }

    if ( typeof( window.lock ) == 'undefined'){ window.lock = 0; }

    jQuery.ajax({
            url: '/' + window.type + '/filters/apply',
            data: {
                escort_name:        escort_name,
                age:                age,
                gender:             gender,
                ethnicity:          ethnicity,
                body:               body,
                hourly_rates:       hourly_rates,
                services:           services,
                distance:           window._distance,
                incallsOutcalls:    incallsOutcalls,
                international:      international,
                height:             height,
                hair_color:         hair_color,
                eye_color:          eye_color,
                longitude:          window.longitude,
                latitude:           window.latitude,
                page:               page,
                business:           business,
                city:               window.thisCity,
                lock:               window.lock
                
            },
        })
        .done(function(data) {
          loadEscorts(data,page);
          $( '#filter_information' ).html( check.filters );
              $( '#filter_information').off().on('click','#clearSearch',function(e){
                 removeFilters(e);
          });
        });   
}

function infiniteScroll(){
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function wipeEscorts(){
    jQuery('.escort-container .escorts-wishlist').each(function(index, el) {
        // Remove everything about the empty templates
        if (!jQuery(this).hasClass('grid-template') && !jQuery(this).hasClass('list-template')) {
            jQuery(this).remove();
        }
    });    
}
var page = 0,
suburb_id = jQuery('body').data('suburb-id') || Cookies.get('suburb');  

$( '#custom_search_escorts' ).on( 'submit',function(e) {
      $( '#load_more_escorts' ).remove();
      e.preventDefault();
      wipeEscorts();
         $('.spinner').fadeIn(200);
          $.ajax({
                  url: "/_search_escorts",
                  data: {
                      'search' :      $( '#escort_side-search').val(),
                      'lar' :         Cookies.get('laravel_session') || '',
                      'latitude':     window.latitude || Cookies.get('latitude'),
                      'longitude':     window.longitude || Cookies.get('longitude'),
                      'city':         thisCity,    
                  }
               }).done(function(data) {
            loadEscorts(data,page);
        $( "#filter_information" ).html( "Showing results for search term: " + $( '#escort_side-search' ).val() );
        $( '.spinner' ).hide();
        $( '#left_pull_filters' ).animate({
             'width': '0px',
             'margin-left': '-50px'
           },{ 
               duration: 700,
               queue: false });
        });
    });
function loadEscorts(data,page){
     $( '#load_more_escorts' ).remove();
        jQuery('.spinner').fadeOut(200);
        wipeEscorts();
        if (data.length == 0) {
            directory.append('<h3>No escorts found with these criteria. Try again!</h3>');
            return;
        }
        $( '#')
        window.check = jQuery.parseJSON(data);
        show_escorts(check);
        if ( check.touring && check.touring.length > 0 ){
            show_escorts(check,'touring');
        }
}
function load_saved_escorts() {
     $( '#load_more_escorts' ).remove();
     $( '.touring-container').empty();
    jQuery('.spinner').fadeOut(200);
    show_escorts(check);
     if ( check.touring.length > 0 ){
            show_escorts(check,'touring');
        }
  }
function show_escorts(check, touring ) {

    if ( touring != 'touring'){
       data = check['show'];
       dir = 'escort';
   }
    else {
        data = check['touring'];
        dir = 'touring';
        $( '.touring-container').empty();
        $( '#touring-container h2').remove();
        if ( data.length > 0 ){
            $( '#touring-container' ).prepend('<h2>Touring in your location</h2');
        }
    }
    if (data.length === 0 && page > 0){
        return;
    }
    var view = Cookies.get('view') || 'grid',
        page = page || 0,
        directory = jQuery('#directory');

    // Wipe eventual empty query message
    directory.find('h3').remove();

    // If there is no data, wipe and show empty query message and return

    if (data.length == 0) {
        wipeEscorts();

        directory.append('<h3>No ' + window.type + 's found with these criteria. Try again!</h3>');
        return;
    };


    jQuery.each(data, function(index, escort) {

        var template = jQuery('.grid-template');
        // For each escort loaded, create a clone of the template
        var clone = template.clone();

        // Remove escort-template class to prevent infinite loop
        // Add clone class to enable infinite scroll
        clone.removeClass('grid-template').addClass('clone');

        // Add data-distance to each container
        clone.attr('data-distance', escort.distance);

        // Populate the clone
        var link = clone.children('.escorts-img-container').children('a'),
            readmore = clone.find('.fig-readmore a'),
            readmoreHref = readmore.attr('href');

        readmore.attr('href',readmoreHref+'/'+escort.seo_url);
        jQuery.each(link, function(index, val) {
            var $this = jQuery(this),
                href = $this.attr('href');
                if ( escort.busUrl != undefined ){
                    $this.attr('href', escort.busUrl);
               }
               else {
                   $this.attr('href', '/' + window.type + 's/' + escort.seo_url);
               }
        });
        clone.find('.escorts-img-container').attr('id', 'escort-'+escort.id);
        if ( escort.name != undefined ){
            clone.find('.figname').append(escort.name);
        }
        else {
            clone.find('.figname').append(escort.escort_name);
        }
        clone.find('.escort-location').prepend(escort.escortLocation);
        clone.find('.escort-age').prepend(escort.escortAge);
        clone.find('.escort-distance').prepend('~'+escort.distance);
        clone.find('.escort-description').prepend(escort.about_me);
        if ( escort.suburb_name != 'undefined'){           
               clone.find('.business-suburb').prepend(escort.suburb_name );
        }
        if (escort.mainImageUrl) {
            clone.find('.img-profile').attr('src', $cdn + '/escorts/thumbnails/thumb_220x330_'+escort.mainImageUrl);
        }
        else if ( escort.image ){
            clone.find('.img-profile').attr('src', $cdn + '/businesses/' + escort.image ).addClass('_mpBusImage');
        }
        else {
            clone.find('.img-profile').attr('src', '/assets/frontend/img/profile_template.jpg');
        }


        if (escort.featured == 0)
            clone.find('.ribbon-featured').hide();

        // Add the clone to the list
        var container = jQuery('.' + dir + '-container');

        container.append(clone);

        window.applied = 0;
    });
    // In the end, hide only the empty template
    jQuery('.escorts-wishlist').show();
    jQuery('.grid-template').first().hide();
      if (check['save'].length > 0 ){
        $( '#directory-container' ).append('<button id ="load_more_escorts">See more</button>');
        $( '#load_more_escorts' ).on( 'click',function() {
              load_saved_escorts();
        });
        delete check['show'];
        check['show'] = {};
        var temp = new Array();
        var i =0;
        var j = 0;
        $.each(check['save'], function( index, value ) {
          if ( i < 60 ) {
              check.show[i] =  $( this )[0];
              delete check['save'][index];
              i++;
          }
          else {
              temp[j] =  $( this )[0];
              j++;
          }
        });
        delete check['save'];
        check['save'] = temp;
        delete temp;
    }
}
                         
function toggleFilterDropdown(e){
    e.preventDefault();
    var li = $(this).siblings('ul').find('li');
    li.each(function(index, el) {
        if (!$(this).hasClass('hidden'))
            $(this).slideToggle(200);
    });
}

function makeAgeQuery(range){
    var min = range.substring(0,2),
        max = range.substring(2);

    if(max == 70)
        max = '70+';

    return [min, max];
}

function makeHourlyRatesQuery(range){
    var min = range.split('-')[0],
        max = range.split('-')[1];
    if (range == '500') {
        min = 500;
        max = 1000000;
    }
    return [min,max];
}

function addFilter(e)
{
    e.preventDefault();
    var $this = $(this),
        usedFiltersContainer = $('#used-filters-container'),
        usedFilters          = $('#used-filters'),
        filterParent         = $this.closest('li.filter-name').children('a'),
        filterName           = filterParent.text(),
        filterNameId         = filterParent[0].id,
        filterValue          = $this.text(),
        filterValueId        = $this[0].id,
        usedFilterType       = usedFilters.children('#'+filterNameId);

    usedFiltersContainer.show();

    $this.closest('li').hide();

    if (usedFilterType.length > 0)
    {
        var innerList  = usedFilterType.find('ul'),
            valueClone = $('.used-filter-template')
                        .find('.filter-value')
                        .clone()
                        .show()
                        .appendTo(innerList);

        valueClone.children('a').attr('id', filterValueId).prepend(filterValue);
    }
    else
    {
        var clone = usedFilters
                        .children('.used-filter-template')
                        .clone()
                        .removeClass('used-filter-template')
                        .attr('id',filterNameId)
                        .appendTo(usedFilters)
                        .show(),
            innerLi = clone.find('li');

        innerLi.show();
        clone.children('a').attr('data-filter-name', filterNameId).text(filterName);
        clone.find('ul>li>a').attr('id', filterValueId).prepend(filterValue);
    }

    if ($this.data('filter-name') == 'international-travel')
    {
        clone.find('ul').hide();
        clone.find('a').append('<i class="fa fa-times"></i>');
    }

    applyFilters();
}
function addUsedFilter(el)
{
    var $this = el,
        usedFiltersContainer = $('#used-filters-container'),
        usedFilters          = $('#used-filters'),
        filterParent         = $this.closest('li.filter-name').children('a'),
        filterName           = filterParent.text(),
        filterNameId         = filterParent[0].id,
        filterValue          = $this.text(),
        filterValueId        = $this[0].id,
        usedFilterType       = usedFilters.children('#'+filterNameId);

    usedFiltersContainer.show();

    $this.closest('li').hide();

    if (usedFilterType.length > 0)
    {
        var innerList  = usedFilterType.find('ul'),
            valueClone = $('.used-filter-template')
                        .find('.filter-value')
                        .clone()
                        .show()
                        .appendTo(innerList);

        valueClone.children('a').attr('id', filterValueId).prepend(filterValue);
    }
    else
    {
        var clone = usedFilters
                        .children('.used-filter-template')
                        .clone()
                        .removeClass('used-filter-template')
                        .attr('id',filterNameId)
                        .appendTo(usedFilters)
                        .show(),
            innerLi = clone.find('li');

        innerLi.show();
        clone.children('a').attr('data-filter-name', filterNameId).text(filterName);
        clone.find('ul>li>a').attr('id', filterValueId).prepend(filterValue);
    }

    if ($this.data('filter-name') == 'international-travel')
    {
        clone.find('ul').hide();
        clone.find('a').append('<i class="fa fa-times"></i>');
    }

}
jQuery(document).ready(function($) {
    var usedFiltersContainer = $('#used-filters-container'),
        usedFilters = $('#used-filters'),
        unusedFilters = $('#escort-filters'),
        container = $('.escort-container');

    // Hide
    unusedFilters.find('ul li').hide();
    usedFilters.find('.used-filter-template').hide();

    $('#escort-filters>li>a').on('click', toggleFilterDropdown);

    unusedFilters.on('click', 'ul>li>a, #international-travel', addFilter);

    // Remove filter
    usedFilters.on('click', 'a', function(e) {
        e.preventDefault();
        var $this = $(this),
            filter = $this.closest('li');

        if (filter.hasClass('filter-name')) {
            var filterNameId = filter.data('filter-name');
            if (filter.data('filter-name') == 'international-travel') {
                unusedFilters
                    .find('[data-filter-name="'+filterNameId+'"]')
                    .parent('li')
                    .show();
            }

            unusedFilters.find('[data-filter-name="'+filterNameId+'"]')
                         .siblings('ul')
                         .find('li').show();
        }
        else // if it's a filterValue
        {
            var filterNameLi = $this.closest('li.filter-name'),
                filterNameId = filterNameLi.data('filter-name'),
                filterId = $this.data('filter-id');

            unusedFilters.find('[data-filter-name="'+filterNameId+'"]')
                         .siblings('ul')
                         .find('[data-filter-id="'+filterId+'"]')
                         .closest('li')
                         .show();

            // If the container is empty, remove the filter-name
            if (filter.closest('ul').children('li').length == 1)
                filterNameLi.remove();
        }

        // Delete the whole filterName
        filter.remove();

        if (usedFilters.children('li').length == 1)
            usedFiltersContainer.slideUp(200);

        applyFilters();
    });

    // Remove all filters
    $('#remove-filters').on('click', function(e) {
        removeFilters(e);
     });




    // Change sorting value
    $('body').on('change', '#sort-by select', function(e) {
        e.preventDefault();
        if ($(this).val() === 'my-location') {
            container.children('li').sort(function(a,b){
                return +a.getAttribute('data-distance') - +b.getAttribute('data-distance');
            }).appendTo(container);
        } else
            applyFilters();
    });
    $( 'body').on('click','.is-not-wish',function() {
        data = { id: window.activeId };
        $( '#wishlist-notification').show();
        $.get( "/addToWishlist", data, function( response ) {
              $( '#wishlist-notification').html('Added to your favourites');
              setTimeout(function(){ 
                $( '#wishlist-notification').hide();
                $( '#wishlist-notification').html("<div class ='mpLoader'></div>");
                $( '.fa-heart').html('REMOVE FROM FAVOURITES');
                $( '.is-not-wish').removeClass('is-not-wish').addClass('is-wish');
             }, 700);
        });
    })
    $( 'body').on('click','.is-wish',function() {
        data = { id: window.activeId };
        $( '#wishlist-notification').show();
        $.get( "/removeFromWishlist", data, function( response ) {
              $( '#wishlist-notification').html('Removed from your favourites');
              setTimeout(function(){ 
                $( '#wishlist-notification').hide();
                $( '#wishlist-notification').html("<div class ='mpLoader'></div>");
                $( '.fa-heart').html('ADD TO FAVOURITES');
                $( '.is-wish').removeClass('is-wish').addClass('is-not-wish');
             }, 700);
        });
    })
});



















