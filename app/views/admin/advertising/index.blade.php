@extends('admin.layout')
@section('content')
<div class ='overlay loadingDiv'></div>
<div class ='loader loadingDiv'></div>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-puzzle-piece"></span></div>
    <div class="pagetitle">
        <h1>Advertising Spaces</h1>
    </div>
</div><!--pageheader-->

<h3 class ='layoutHeader'>CURRENT LAYOUT- Layout 1</h3>
<button class ='float_right switch_layouts'>Toggle Controls</button>
<div class ='layoutHolder layout1'>
   <div class ='row'>
   <div class ='font-page-display'>
      <h3>Display Front Page Ads?</h3>
         <form id ='frontPageDisplay'>
            <input type ='radio' name ='display' value = '0' @if ( isset( $opt )  && $opt->value == 0 ) checked @endif><label>No</label>
            <input type ='radio'  name ='display' value ='1' @if ( isset( $opt )  && $opt->value == 1 ) checked @endif><label>Yes</label>
            <button class ='submit-button'>Save</button>
         </form>
      </div>
      <div class ='col-md-4'>
         <div class ='ad1 adSpace'>
              <div class="cropit-preview"></div>
              <div class ='croppit-controls'>
                   <div class="rotation-btns">
                       <span class="fa fa-rotate-left rotate-ccw-btn"></span>
                       <span class="fa fa-rotate-right rotate-ccw-btn"></span>
                   </div>
                    <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
  
                    <!-- The actual file input will be hidden -->
                    <input type="file" class="cropit-image-input" />
                    <!-- And clicking on this button will open up select file dialog -->
                    <div class="select-image-btn"><span class="fa fa-file-image-o add_image"></span></div>
                    <input id = 'ad1_url' type ='text' placeholder ='Website' @if ( isset( $ads[0] ) ) value = '{{$ads[0]->url}}' @endif>
                    <input id = 'ad1_alt' type ='text' placeholder ='Image Alt Tag' @if ( isset( $ads[0] ) ) value = '{{$ads[0]->img_alt}}' @endif>
                </div>
         </div>
      </div>
      <div class ='col-md-4'>
         <div class ='ad2 adSpace'>
               <div class="cropit-preview"></div>
              <div class ='croppit-controls'>
                  <div class="rotation-btns">
                       <span class="fa fa-rotate-left rotate-ccw-btn"></span>
                       <span class="fa fa-rotate-right rotate-ccw-btn"></span>
                   </div>
                    <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
  
                    <!-- The actual file input will be hidden -->
                    <input type="file" class="cropit-image-input" />
                    <!-- And clicking on this button will open up select file dialog -->
                    <div class="select-image-btn"><span class="fa fa-file-image-o add_image"></span></div>
                    <input id = 'ad2_url' type ='text' placeholder ='Website' @if ( isset( $ads[1] ) ) value = '{{$ads[1]->img_url}}' @endif>
                    <input id = 'ad2_alt' type ='text' placeholder ='Image Alt Tag' @if ( isset( $ads[1] ) ) value = '{{$ads[1]->img_alt}}' @endif>
                </div>
         </div>
      </div>
      <div class ='col-md-4'>
         <div class ='ad3 adSpace'>
               <div class="cropit-preview"></div>
              <div class ='croppit-controls'>
                  <div class="rotation-btns">
                       <span class="fa fa-rotate-left rotate-ccw-btn"></span>
                       <span class="fa fa-rotate-right rotate-ccw-btn"></span>
                   </div>
                    <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
  
                    <!-- The actual file input will be hidden -->
                    <input type="file" class="cropit-image-input" />
                    <!-- And clicking on this button will open up select file dialog -->
                    <div class="select-image-btn"><span class="fa fa-file-image-o add_image"></span></div>
                    <input id = 'ad3_url' type ='text' placeholder ='Website' @if ( isset( $ads[2] ) ) value = '{{$ads[2]->img_url}}' @endif>
                    <input id = 'ad3_alt' type ='text' placeholder ='Image Alt Tag' @if ( isset( $ads[2] ) ) value = '{{$ads[2]->img_alt}}' @endif>
                </div>
         </div>
      </div>
      <div class ='col-md-4'>
         <div class ='ad4 adSpace'>
               <div class="cropit-preview"></div>
              <div class ='croppit-controls'>
                  <div class="rotation-btns">
                       <span class="fa fa-rotate-left rotate-ccw-btn"></span>
                       <span class="fa fa-rotate-right rotate-ccw-btn"></span>
                   </div>
                    <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
  
                    <!-- The actual file input will be hidden -->
                    <input type="file" class="cropit-image-input" />
                    <!-- And clicking on this button will open up select file dialog -->
                    <div class="select-image-btn"><span class="fa fa-file-image-o add_image"></span></div>
                    <input id = 'ad4_url' type ='text' placeholder ='Website' @if ( isset( $ads[3] ) ) value = '{{$ads[3]->img_url}}' @endif>
                    <input id = 'ad4_alt' type ='text' placeholder ='Image Alt Tag' @if ( isset( $ads[3] ) ) value = '{{$ads[3]->img_alt}}' @endif>
                </div>
         </div>
      </div>
      <div class ='col-md-4'>
         <div class ='ad5 adSpace'>
               <div class="cropit-preview"></div>
              <div class ='croppit-controls'>
                  <div class="rotation-btns">
                       <span class="fa fa-rotate-left rotate-ccw-btn"></span>
                       <span class="fa fa-rotate-right rotate-ccw-btn"></span>
                   </div>
                    <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
  
                    <!-- The actual file input will be hidden -->
                    <input type="file" class="cropit-image-input" />
                    <!-- And clicking on this button will open up select file dialog -->
                    <div class="select-image-btn"><span class="fa fa-file-image-o add_image"></span></div>
                    <input id = 'ad5_url' type ='text' placeholder ='Website' @if ( isset( $ads[4] ) ) value = '{{$ads[4]->img_url}}' @endif>
                    <input id = 'ad5_alt' type ='text' placeholder ='Image Alt Tag' @if ( isset( $ads[4] ) ) value = '{{$ads[4]->img_alt}}' @endif>
                </div>
         </div>
      </div>
</div>

<button class ='saveChanges'>Save Changes</button>

<script src="/assets/admin/js/jquery.cropit.js"></script>
<script>
$ad1 = $('.ad1').cropit({ 
      minZoom: 'fit',
      smallImage: 'stretch',
      onFileReaderError: function() {
          alert( 'Sorry, there was an error!')
      },
      onImageError: function(error){
        alert( error.message );
       }
      });
$ad2 = $('.ad2').cropit({ 
      minZoom: 'fit',
      smallImage: 'stretch',
      onFileReaderError: function() {
          alert( 'Sorry, there was an error!')
      },
      onImageError: function(error){
        alert( error.message );
       }
      });
$ad3 = $('.ad3').cropit({ 
      minZoom: 'fit',
      smallImage: 'stretch',
      onFileReaderError: function() {
          alert( 'Sorry, there was an error!')
      },
      onImageError: function(error){
        alert( error.message );
       }
      });
$ad4 = $('.ad4').cropit({ 
      minZoom: 'fit',
      smallImage: 'stretch',
      onFileReaderError: function() {
          alert( 'Sorry, there was an error!')
      },
      onImageError: function(error){
        alert( error.message );
       }
      });
$ad5 = $('.ad5').cropit({ 
      minZoom: 'fit',
      smallImage: 'stretch',
      onFileReaderError: function() {
          alert( 'Sorry, there was an error!')
      },
      onImageError: function(error){
        alert( error.message );
       }
      });


$('.ad1 .select-image-btn').click(function() {
      $('.ad1 .cropit-image-input').click();
});
$('.ad2 .select-image-btn').click(function() {
      $('.ad2 .cropit-image-input').click();
});
$('.ad3 .select-image-btn').click(function() {
      $('.ad3 .cropit-image-input').click();
});
$('.ad4 .select-image-btn').click(function() {
      $('.ad4 .cropit-image-input').click();
});
$('.ad5 .select-image-btn').click(function() {
      $('.ad5 .cropit-image-input').click();
});


// Handle rotation

$('.ad1 .rotate-cw-btn').click(function() {
  $ad1 .cropit('rotateCW');
});
$('.ad1 .rotate-ccw-btn').click(function() {
  $ad1 .cropit('rotateCCW');
});
$('.ad2 .rotate-cw-btn').click(function() {
  $ad2 .cropit('rotateCW');
});
$('.ad2 .rotate-ccw-btn').click(function() {
  $ad2 .cropit('rotateCCW');
});
$('.ad3 .rotate-cw-btn').click(function() {
  $ad3 .cropit('rotateCW');
});
$('.ad4 .rotate-ccw-btn').click(function() {
  $ad1 .cropit('rotateCCW');
});
$('.ad4 .rotate-cw-btn').click(function() {
  $ad1 .cropit('rotateCW');
});
$('.ad5 .rotate-ccw-btn').click(function() {
  $ad5 .cropit('rotateCCW');
});
$('.ad5 .rotate-cw-btn').click(function() {
  $ad5 .cropit('rotateCW');
});

$( '.saveChanges' ).on('click',function() {
  var ad1 = $ad1 .cropit('export', {
       type: 'image/jpeg',
       quality: .9,
       originalSize: false
    });
    var ad2 = $ad2 .cropit('export', {
       type: 'image/jpeg',
       quality: .9,
       originalSize: false
    });
    var ad3 = $ad3 .cropit('export', {
       type: 'image/jpeg',
       quality: .9,
       originalSize: false
    });
    var ad4 = $ad4 .cropit('export', {
       type: 'image/jpeg',
       quality: .9,
       originalSize: false
    });
    var ad5 = $ad5 .cropit('export', {
       type: 'image/jpeg',
       quality: .9,
       originalSize: false
    });
    var images = {
       'ad1': { image: ad1, url: $( '#ad1_url').val(), img_alt: $( '#ad1_alt').val() },
       'ad2': { image: ad2, url: $( '#ad2_url').val(), img_alt: $( '#ad2_alt').val() },
       'ad3': { image: ad3, url: $( '#ad3_url').val(), img_alt: $( '#ad3_alt').val() },
       'ad4': { image: ad4, url: $( '#ad4_url').val(), img_alt: $( '#ad4_alt').val() },
       'ad5': { image: ad5, url: $( '#ad5_url').val(), img_alt: $( '#ad5_alt').val() },
    }
    $( '.loadingDiv').show();
    $.post( "/admin/save-frontpage-ads", images, function( data ) {
       console.log( data );
       alert( 'Changes have been saved.');
       $( '.loadingDiv').hide();
    });
    })
    $( '.switch_layouts').on('click',function() {
      $( '.croppit-controls').toggle();
    })
    $( '#frontPageDisplay').on('submit',function(e) {
      e.preventDefault();
      $( '.loadingDiv').show();
      display = $( this ).serialize();
        $.post( "/admin/show-front-page-ads", display, function( data ) {
            alert( 'Changes have been saved.');
            $( '.loadingDiv').hide();
        });
    })


</script>
@if ( isset( $ads[0] ) ) 
  <script>
       $ad1.cropit('imageSrc', '/uploads/advertising/{{$ads[0]->image_file}}');
  </script>
@endif
@if ( isset( $ads[1] ) ) 
  <script>
        $ad2.cropit('imageSrc', '/uploads/advertising/{{$ads[1]->image_file}}');
  </script>
@endif
@if ( isset( $ads[2] ) ) 
  <script>
        $ad3.cropit('imageSrc', '/uploads/advertising/{{$ads[2]->image_file}}');
  </script>
@endif
@if ( isset( $ads[3] ) ) 
  <script>
        $ad4.cropit('imageSrc', '/uploads/advertising/{{$ads[3]->image_file}}');
  </script>
@endif
@if ( isset( $ads[4] ) ) 
   <script>
        $ad5.cropit('imageSrc', '/uploads/advertising/{{$ads[4]->image_file}}');
   </script>
@endif

<style>
/* Show load indicator when image is being loaded */
.cropit-preview.cropit-image-loading .spinner {
  opacity: 1;
}

/* Show move cursor when image has been loaded */
.cropit-preview.cropit-image-loaded .cropit-preview-image-container {
  cursor: move;
}

/* Gray out zoom slider when the image cannot be zoomed */
.cropit-image-zoom-input[disabled] {
  opacity: .2;
}

/* Hide default file input button if you want to use a custom button */
input.cropit-image-input {
  visibility: hidden;
}

/* The following styles are only relevant to when background image is enabled */

/* Translucent background image */
.cropit-preview-background {
  opacity: .2;
}

/*
 * If the slider or anything else is covered by the background image,
 * use non-static position on it
 */
input.cropit-image-zoom-input {
  position: relative;
}

/* Limit the background image by adding overflow: hidden */
#image-cropper {
  overflow: hidden;
}
</style>



@stop