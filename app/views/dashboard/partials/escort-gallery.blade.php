   <div class ='gallery desktop_only' sv-root sv-part="profileGallery.images">

     <div class ='col-md-2' ng-class = "{'main_image': profileGallery.main_image == image.id}" ng-repeat="image in profileGallery.images" sv-element>
       <div class ='delete_image' ng-click = 'deleteImage($index)'></div>
       <div ng-class = "{'make_main': ( image.status == 2 ),  'hidden': ( image.status !=2 ) }" ng-click = 'makeMain($index)'>Make Main Image</div>
       <div class ='im_stat@{{image.status}}'></div>
       <img draggable ="false" ng-src ="{{image.newImage == 1 ? '/temp/' + image.filename : '<?php echo Config::get('constants.CDN');?>/escorts/' + image.filename}}">
     </div>

     <div ng-class = "{'col-md-2': ( profileGallery.images.length < 10 ), 'hidden': ( profileGallery.images.length >= 10 ) }" id ='addImage' ngf-max-size = "10MB" ngf-validate-fn="validate($file)" ngf-accept="'image/*'" ngf-select="galleryUpload($file)" ngf-drop = "galleryUpload($file)">
       Add Image
       <div id ='galleryLoading'></div>
     </div>
  </div>

    <div class ='gallery mobile_only'>

     <div class ='col-md-2' ng-class = "{'main_image': profileGallery.main_image == image.id}" ng-repeat="image in profileGallery.images">
       <div class ='delete_image' ng-click = 'deleteImage($index)'></div>
       <div class ='make_main' ng-click = 'makeMain($index)'>Make Main Image</div>
       <div class ='im_stat@{{image.status}}'></div>
       <img draggable ="false" ng-src ="{{image.newImage == 1 ? '/temp/' + image.filename : '<?php echo Config::get('constants.CDN');?>/escorts/' + image.filename}}">
     </div>

     <div ng-class = "{'col-md-2': ( profileGallery.images.length < 10 ), 'hidden': ( profileGallery.images.length >= 10 ) }" id ='addImage' ngf-max-size = "10MB" ngf-validate-fn="validate($file)" ngf-accept="'image/*'" ngf-select="galleryUpload($file)" ngf-drop = "galleryUpload($file)">
       Add Image
      <div id ='galleryLoadingMobile'></div>
     </div>
  </div>