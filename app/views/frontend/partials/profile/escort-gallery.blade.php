<div id="backend-profile-gallery" class="alignc" >
    <h2>GALLERY</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="triggerAnimation animated" data-animate="fadeInUp">
                <p class ='profile_info no_line'>
                  You can upload and display a maximum of 20 viewable photos at a time on your profile.<br />
                    Drag and drop your photos to sort them.
                  <br><br>
                    Maximum upload is 800kB.
                </p>
              {{ Form::open(array('files'=>true,'class'=>'mydropzone','id'=>'escortGallery')) }}
                {{ Form::hidden('escort_id',$escort->id) }}
                {{ Form::close() }}
              <div id ='uploader_info'>Drag & Drop your files here, or click to upload</div>


        <hr />

                <h2>YOUR IMAGES</h2>
                <br />
                <div class="grid-view profile-gallery">
                    @foreach(EscortImage::whereEscortId($escort->id)->orderBy('order')->get() as $image)
                        <div data-id="{{ $image->id }}" data-order="{{ $image->order }}" class="gallery-element col-md-2 col-sm-4 col-xs-6 col-lg-1">
                            <div class="escorts-img-container">
                                <div id="{{ $image->id }}" class="fa fa-trash-o delete"></div>
                                @if($image->status == 1)
                                    <div class="ribbon ribbon-pending"></div>
                                @endif
                                <img src="/uploads/escorts/thumbnails/thumb_220x330_{{ $image->filename }}" />
                            </div>
                            <ul class="images-keys">
                                <li id="{{ $image->id }}" class="is-not-main-image" style="display:{{ $image->id == $escort->main_image_id ? 'none' : 'block' }}">
                                    <a class="btn">Make Main Image</a>
                                </li>
                                <li class="is-main-image" style="display:{{ $image->id != $escort->main_image_id ? 'none' : 'block' }}">
                                    <a class="btn">Main Image</a>
                                </li>
                            </ul>
                        </div>
                    @endforeach
                </div>

                <br /><br />
            </div><!-- .triggerAnimation.animated end -->
        </div><!-- .col-md-12 end -->
        <br /><br />
    </div><!-- .row end -->
</div><!-- End of Gallery-->

<script>
    (function($){
             $( window ).scrollTop();
             Dropzone.autoDiscover = false;
      
      var myDropzone = new Dropzone("#escortGallery",{maxFilesize:'0.85'});
      myDropzone.on("addedfile",function(){
            $( 'div.dz-error' ).remove();
            $( '#escortGallery' ).removeClass( 'dropZoneError' );
        $('html, body').animate({
                 scrollTop: $("#escortGallery").offset().top + -100
        }, 1000);
         });
      myDropzone.on("processing",function(){
            $( '#escortGallery' ).addClass( 'dropZoneProcessing' );
    });
       myDropzone.on("error",function(){
            $( '#escortGallery' ).addClass( 'dropZoneError' );
    });
           myDropzone.on("success", function(file, serverData) {
           var startPos = $( '.profile-gallery div:last-child').data('start_pos'),
           serverData = jQuery.parseJSON( serverData ),
           dataId = serverData.image_id;
           div = '<div data-id="' + dataId+ '" data-order="0" class="gallery-element col-md-2 col-sm-4 col-xs-6 col-lg-1 ui-sortable-handle" data-start_pos="5" style="cursor: move;"><div class="escorts-img-container"><div id="' + dataId+ '" class="fa fa-trash-o delete"></div><div class="ribbon ribbon-pending"></div><img src="/uploads/escorts/thumbnails/thumb_220x330_' + serverData.file_name + '"></div><ul class="images-keys"><li id="' + dataId+ '" class="is-not-main-image" style="display:block"><a class="btn">Make Main Image</a></li><li class="is-main-image" style="display:none"><a class="btn">Main Image</a></li></ul></div>';
           $( '.profile-gallery' ).append( div );
           $('div.dz-success').remove();
              $('.is-not-main-image').on('click', function(e) {
            e.preventDefault();
            var $this = $(this),
                escort_id = {{ $escort->id }},
                image_id = $this[0].id;

            $('.is-main-image').hide();
            $('.is-not-main-image').show();
            $this.hide();
            $this.siblings('.is-main-image').show();

            $.ajax({
                url: '/setMainImage',
                data: {
                    escort_id : escort_id,
                    image_id: image_id
                },
            });
              });
             
             
            $('div.delete').off().on('click', function() {
            var image_id = $(this)[0].id;
            $(this).parents('.gallery-element').remove();
            $.ajax({
                url: '/deleteImage',
                data: {
                    image_id: image_id,
                    escort_id: {{ $escort->id }}
                }
            });
        });
             setTimeout(function(){ $( "#escortGallery" ).removeClass( "dropZoneProcessing" );}, 1000);
       });
      
        $('.second-level h2').text("{{ $escort->escort_name }}");

        $('.profile-gallery>div').each(function(index, el) {
            $(this).attr('data-start_pos',$(this).index());
        });
        $('.profile-gallery>div').css('cursor', 'move');
        $('.profile-gallery').sortable({
            update: function(event, ui) {
                var imageId = ui.item.attr('data-id'),
                    start_pos = ui.item.attr('data-start_pos'),
                    end_pos = ui.item.index(),
                    images = [];

                ui.item.attr('data-order',ui.item.index());
                ui.item.attr('data-start_pos',ui.item.index());

                $('.profile-gallery>div').each(function(index, el) {
                    var $this = $(this);
                    if (end_pos > start_pos) {
                        if ($this.attr('data-start_pos') > start_pos && $this.attr('data-start_pos') <= end_pos)
                        {
                            $this.attr('data-start_pos',$this.index());
                            $this.attr('data-order',$this.index());
                        }
                    } else {
                        if ($this.attr('data-start_pos') < start_pos && $this.attr('data-start_pos') >= end_pos)
                        {
                            $this.attr('data-start_pos',$this.index());
                            $this.attr('data-order',$this.index());
                        } 
                    }
                    var imageData = [$this.attr('data-id'),$this.attr('data-order')];
                    images.push(imageData);
                });
                $.ajax({
                    url: '/updateImageOrder',
                    data: {images: images}
                }); 
            }
        });

        var formContainer = $('#form'),
            escortLevel   = $('.escort-level'),
            spinner       = $('.cc-container'),
            escort_id     = $('.escort-level').data('escort-id');

        $('#refresh-gallery').on('click',goToEscortGallery);

        $('#continue').on('click', function(e){
            e.preventDefault();
            goToEscortSite();
            activateIcon($('.escort-site'));
        });

        $('div.delete').on('click', function() {
            var image_id = $(this)[0].id;
            $(this).parents('.gallery-element').remove();

            $.ajax({
                url: '/deleteImage',
                data: {
                    image_id: image_id,
                    escort_id: {{ $escort->id }}
                }
            });
        });

        $('.is-not-main-image').on('click', function(e) {
            e.preventDefault();
            var $this = $(this),
                escort_id = {{ $escort->id }},
                image_id = $this[0].id;

            $('.is-main-image').hide();
            $('.is-not-main-image').show();
            $this.hide();
            $this.siblings('.is-main-image').show();

            $.ajax({
                url: '/setMainImage',
                data: {
                    escort_id : escort_id,
                    image_id: image_id
                },
            });
        });   
    })(jQuery);
</script>