@extends('admin.layout')

@section('content')

	<div class="pageheader">
    	<div class="pageicon"><span class="fa fa-money"></span></div>
    	<div class="pagetitle">
        	<h1>Edit business logo - {{ $business->name }}</h1>
    	</div>
	</div>

	@include('admin.partials.business-breadcrumb')

    {{ Form::open(array('url'=>'/admin/businesses/logo/upload/'.$business->id,'files'=>true,'class'=>'dropzone')) }}
    {{ Form::hidden('business_id',$business->id) }}
    {{ Form::close() }}

    @if($business->image != '')
	    <div class="alignc section-content">
	        <img src="/uploads/businesses/{{ $business->image }}" alt="{{ $business->name }}">
	    </div>
    @endif

@stop