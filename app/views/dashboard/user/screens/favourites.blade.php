<div class ='col-md-7 no_padding _MpoffWhite'>
     <h3>Your Favourites</h3>
  @if(count( $faves ) > 0 )
     @foreach ( $faves as $escort ) 
       <?php $class = ( $escort->status == 2 ? '_mpEscortContainer' : '_mpEscortContainer unavailable' ); ?>
        <div class ='{{ $class }}'>
             <div class ='_mpEscortImage'>
                 @if ( $escort->image )
               <img src = '{{ Config::get('constants.CDN') }}/escorts/thumbnails/thumb_170x170_{{$escort->image}}'>
                 @else 
               <img src ='{{ Config::get('constants.CDN') }}/escorts/thumbnails/thumb_170x170_escortHolder.jpg'>
                 @endif
          </div>

          <div class ='_mpEscortStats'>
                <h4>{{$escort->escort_name}}</h4>
            @if ( $escort->status == 2 )

            <button class ='_mpViewEscort'><a href ='/escorts/{{$escort->seo_url}}'>View</a></button>
            @if ( $escort->contact_playmail == 1 )
                <span class ='_mpStatsSpan sendPlaymail' ng-click ='sendPlaymailTo_({{$escort->id}})'>Send Playmail</span>
            @endif
            @if ( $escort->contact_phone == 1 )
                <span class ='_mpStatsSpan getPhoneNumber' id ='revealPhone_{{$escort->id}}' ng-click ='getPhoneNumber("{{$escort->phone}}","{{$escort->id}}")'>Get Phone number</span>
            @endif    

            @else 
            <span class ='_mpStatsSpan'>This escort is currently unavailable.</span>
            @endif
          </div>
               
     </div>
     @endforeach
  @else
  <div class ='emptyEscortFiller'>
  <p>You currently no favourites.  To start browsing escorts, click the lips in the top left hand corner.</p>
  </div>
  @endif
</div>
<div class ='col-md-5 no_padding _MplightGrey'>
  <div id ='_mpBusinessPlan'>
    <div>
     <a href ='https://www.adultpress.com.au/'>
         <img id ='adultPressad' src ='/assets/dashboard/images/playground.jpg'>
     </a>
    </div>
 </div>
</div>
<div id ='mPOverlay'></div>
<div id="contactEscortForm">
<form role="form" novalidate="" class ='modal' ng-submit ='sendPlaymail()'>
                            <h4 id ='pl_EscName'>Send Playmail</h4>
                            <div class="alert alert-success"></div>
                                <form id ='sendPlaymail' ng-model ='playmail'>
                                        <label for="name" class="control-label">Your Name</label>
                                        <input ng-model ='playmail.name' required="" type="text" class="form-control" placeholder="Name" data-parsley-id="4">
                                        <label for="phone" class="control-label">Phone</label>
                                        <input ng-model ='playmail.phone' required="" type="phone" class="form-control" placeholder="Phone/Mobile Number" maxlength="10" data-parsley-id="6">

                                        <label for="email" class="control-label">Email</label>
                                        <input ng-model ='playmail.email' required="" type="email" class="form-control" placeholder="Email" data-parsley-id="8">

                                        <label id ='playMessageLabel' class="control-label">Message</label>
                                        <textarea ng-model ="playmail.message" required="" class="form-control" placeholder="Message" style="height:100px" maxlength="1000" data-parsley-id="10"></textarea>
                                        <button type="submit" class="btn btn-default pull-right submit-button" id="contactEscortButton">Send PlayMail</button>
                            </div>
            </div>
        </form>
        <div class ='three-dots-row-spinner' id ='playmailSpinner'></div>
        <h3 id ='playMessage'></h3>
        </form>
        </div>
<script>
  $( '#mPOverlay').on( 'click',function() {
  	$( this ).hide();
    $( '#contactEscortForm').hide();
  })