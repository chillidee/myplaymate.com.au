<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
  if(!App::environment('development'))
  {

    if( ! Request::secure())
    {
      return Redirect::to('https://myplaymate.com.au'.$_SERVER['REQUEST_URI']);
    }

    if (substr($_SERVER['SERVER_NAME'],0, 4) == 'www.') {
      return Redirect::to('https://myplaymate.com.au'.$_SERVER['REQUEST_URI']);
    }
  }

  if (strpos($_SERVER['REQUEST_URI'],'public') !== false)
      return Redirect::to('/');


     header('Access-Control-Allow-Origin: *');
     header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
     header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
     header('Access-Control-Allow-Credentials: true');

});


App::after(function($request, $response)
{
  //
});
  /*
|--------------------------------------------------------------------------
| Allow Cors
|--------------------------------------------------------------------------
|
| The following filter allows outside resources to communicate with the api
|
  */
Route::filter('allowOrigin', function($route, $request, $response)
{
    $response->header('access-control-allow-origin','*');
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('ajax',function(){
  // if(!Request::ajax())
  // {
  //   return Redirect::to('404');
  // }
});


Route::filter('auth', function()
{
  if (Auth::guest())
  {
    if (Request::ajax())
    {
      return Response::make('Unauthorized', 401);
    }
    else
    {
      return Redirect::guest('/#login');
    }
  }
});
Route::filter('dashboard', function()
{
  if (Auth::guest())
  {
      return Response::make('Unauthorized', 401);
    }
});


Route::filter('auth.basic', function()
{
  return Auth::basic();
});

Route::filter('auth.admin', function()
{
  if (Auth::guest() || Auth::user()->super_user != 1) {
    return Redirect::guest('admin/login');
  }
});

Route::filter('development',function(){
  if(!App::environment('development'))
  {
    return Response::make('Unauthorized', 401);
  }
});

Route::filter('blockLocalConnection', function(){
  if(!App::environment('development'))
  {
    $blocked_ips = [
       '203.27.178.78',
    ];

    if(in_array($_SERVER['REMOTE_ADDR'], $blocked_ips))
    {
      return Response::make('Stats update blocked from this IP', 401);
    }
  }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
  if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
  if (Session::token() != Input::get('_token'))
  {
    throw new Illuminate\Session\TokenMismatchException;
  }
});
