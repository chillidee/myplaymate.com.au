<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
// use MyPlayMate\Dashboard\DashboardInterface;
use MyPlayMate\Observers\CustomerUserObserver;

class CustomerUser extends Eloquent implements UserInterface, RemindableInterface
{

    protected $table = 'users';

    const TYPE_CUSTOMER = 'customer';

    protected $fillable = [
        'full_name',
        'email',
        'active',
        'activation_code',
        'type',
        'phone',
        'password',
    ];

    public static $rules = [
        'full_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required',
        'terms_and_conditions' => 'accepted',
    ];

    public static function boot()
    {
        //parent::boot();

        //CustomerUser::observe(new CustomerUserObserver());
    }

    public static function filtering()
    {
        return CustomerUser::where('type', CustomerUser::TYPE_CUSTOMER);
    }

    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }
    public function getWishlistProfile()
    {
        return Whishlist::where('user_id', $this->id)->lists('profile_id');
    }
    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function dashboardRoute()
    {
        return '/manage/wishlist';
    }
    public function sendActivationEmail($message)
    {
        $emailTemplate = EmailTemplate::where('name', 'activation-code')->first();

        $templateData = [];
        $templateData['user'] = $this->toArray();
        $templateData['account'] = ['first_name' => $this->fullname];// $this->account->toArray();
        $templateData['profile'] = null;
        $templateData['activation_link'] = Config::get('app.fullUrl').'users/activate?email='.$this->email.'&activation_code='.$this->activation_code;
        $templateData['activation_code'] = $this->activation_code;
        $templateData['activation_url'] = Config::get('app.fullUrl').'users/activate';
        $templateData['campaign_title'] = "";
        $emailData = new stdClass();
        $emailData->user = $this;
        $emailData->template = $emailTemplate;
        echo Config::get('app.adminEmail');
        return $emailData->user->email;
        Mail::send(
            'admin.emailtemplates.templates.'.$emailTemplate->name,
            ['data' => $templateData],
            function ($message) use ($emailData) {
                $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);

                $message->to($emailData->user->email);
        });

        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);
            $message->to(Config::get('app.adminEmail'));
        });
    }
}
