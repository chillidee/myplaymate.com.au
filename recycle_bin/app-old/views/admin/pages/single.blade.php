@extends('admin.layout')

@section('javascript')

   <script type="text/javascript">
        (function($){
            $('textarea.tinymce').tinymce();
        })(jQuery);
    </script>    

@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-file-text-o"></span></div>
    <div class="pagetitle">
        <h1>Edit page - {{ $page->name }}</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open() }}
<div class="row">
	<div class="col-md-6">
        <div class="control-group">
            {{ Form::label('name','Name*',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('name',$page->name,array('class'=>'form-control','required')) }}
            </div>
        </div>
	</div>
	<div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_title','SEO title',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_title',$page->meta_title,array('class'=>'form-control')) }}
            </div>
        </div>
	</div>
	<div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_keywords','SEO keywords',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_keywords',$page->meta_keywords,array('class'=>'form-control')) }}
            </div>
        </div>
	</div>
	<div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_description','SEO description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_description',$page->meta_description,array('class'=>'form-control')) }}
            </div>
        </div>
	</div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="control-group">
            {{ Form::label('content','Content',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('content',$page->content,array('class'=>'tinymce')) }}
            </div>
        </div>
    </div>
</div>
{{ Form::hidden('page_id',$page->id) }}

<div class="alignc">
    {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
</div>

{{ Form::close() }}
@stop