@extends('admin.layout')

@section('javascript')

	<script>
		function setStatus(button,status)
		{
			var url,
				commentId = button.data('comment-id');
			if(status == 2){
				url = 'approve_comment';
				message = 'Comment approved!';
			}
			if(status == 3){
				url = 'disapprove_comment';
				message = 'Comment disapproved!';
			}

			jQuery.ajax({
				url: '/admin/'+url,
				data: {comment_id: commentId},
			})
			.done(function() {
	    		var label = button.parents('tr').find('span.label');
	    		label.removeClass('label-warning label-danger label-success');
	    		if (status == 2)
	    			label.addClass('label-success').text('approved');
	    		else
	    			label.addClass('label-danger').text('disapproved');
				jQuery('.alert-success').text(message).fadeIn();
			});
		}

		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "/admin/ajax_comments",
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'post_id', name: 'post_id'},
		            {data: 'author', name: 'author'},
		            {data: 'comment', name: 'comment', 'width': '400px'},
		            {data: 'status', name: 'status'},
		            {data: 'created_at', name:'created_at'},
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });


		$('body').on('click', '.approve', function(e) {
			e.preventDefault();
			setStatus($(this),2);
		});	

		$('body').on('click', '.disapprove', function(e) {
			e.preventDefault();
			setStatus($(this),3);
		});	

		});
	</script>

@stop

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Blog comments</h1>
    </div>
</div><!--pageheader-->

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Post title</th>
			<th>Author name</th>
			<th>Comment</th>
			<th>Status</th>
			<th>Date of creation</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

@stop