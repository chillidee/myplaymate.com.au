var dashboard = angular.module('dashboard.controllers', ['checklist-model', 'angular-sortable-view', 'bp.img.cropper', 'ngFileUpload', 'ngImgCrop',])
    .run(function() {
        FastClick.attach(document.body);
    })
    .config(['$compileProvider', function ($compileProvider) {
       $compileProvider.debugInfoEnabled(false);
     }])
    .controller('driversController', function($scope, $http, dashboard, mpmApi,$compile,$rootScope,Upload,$timeout,$templateCache) {

      $( 'body').on('click','#navigateThroughProfile', function() {
         $( '.mPProfileTopMenu' ).show();
         $( this ).hide();
      })

      $scope.plan = window.plan;
      $scope.escortCount = window.escortCount;
      $scope.escortStats = window.escortStats;
      $scope.escortAllowed = 1;

      if ( window.isBusiness == false && window.type == 'business')
          $scope.escortAllowed = 0;

      if ( window.planEscorts <= window.escortCount )
           $scope.escortAllowed = 0;

      /*---------------------Escort Tab Controller----------------------------
      -----------------------------------------------------------------------------*/

      $scope.clearEmptyRates = function(id) {
            $scope.profileAvailabilites.rates[id] = '';
        }

            $scope.changeEscortScreen = function(view) {
            $scope.currentaction = view;
            $('#_mpEscortMenu li').removeClass('active');
            $('#mPesmen_' + view).addClass('active');
          //  $scope.updateEscort(window.currentEscort);

        }
           $scope.changeEscort = function(escortid) {
            window.currentEscort = escortid;
            $scope.currentescort = escortid;
            $('.escortTabContainer').removeClass('current');
            $('#' + escortid).addClass('current');
            $( '.no_escorts ').hide();
            $( '.active').removeClass('active');
            $( '#mpBasicInfo').addClass('active');
            $scope.updateEscort();
            $('html, body').animate({ scrollTop: 0 });
        }
        $scope.goToEscortProfile = function( id ) {
            $scope.changeEscort( id );

        }
        $scope.createNewEscort = function() {
          if ( window.type == 'admin'){
              $scope.newEscort = {};
              $('.escortTabContainer').removeClass('current');
              $scope.changeEscortScreen("new");
              $scope.basicInfoForm.escort_name = '';
              $scope.status = 'pending';
              $scope.escortAllowed = 1;
              $( '.mp_switch' ).hide();
              $( '.escStatus').hide();
          }
          else {
             if ( window.isBusiness != 0 && ( parseInt( window.escortCount ) < parseInt( window.planEscorts ) ) ){
                $scope.newEscort = {};
                $( '.overlay').hide();
                $('.escortTabContainer').removeClass('current');
                $scope.changeScreens('_MpEscorts');
                $scope.currentescort = 'new';
                currentEscort = 'new';
                $scope.changeEscortScreen("New");
                $scope.creatingEscort = 1;
                $scope.escortAllowed = 1;
             }
             else if ( window.isBusiness == 0 ) {
                $scope.changeScreens('_MpProfile');
                $scope.escortAllowed = 0;
             }
              else {
                 $scope.changeScreens('_MpProfile');
                 $scope.escortAllowed = 0;
                 setTimeout(function(){
                   $( '.active').removeClass( 'active');
                   $( '#mpPlans').addClass( 'active');
                   $( '.mPtab' ).removeClass( 'activeTab');
                   $( '#tab_mpPlans').addClass( 'activeTab');
                }, 1000);
              }
          }
        }

        $scope.changeBusiness = function(businessid) {
            $( '.active').removeClass('active');
            $( '#mpBasicInfo').addClass('active');
            $( '.mPtab').removeClass('activeTab');
            $( '#tab_mpBasicInfo').addClass('activeTab');
            window.currentBusiness = businessid;
            $scope.currentbusiness = businessid;
            window.active_id = businessid;
            $scope.changeBusinessScreen("Edit");
            $scope.updateBusiness();
            $('html, body').animate({ scrollTop: 0 });
        }
        $scope.createNewBusiness = function() {

            $scope.currentbusiness = 'New';
            currentBusiness = 'New';
            $scope.creatingBusiness = 1;
            $scope.changeBusinessScreen("New");
            $('.escortTabContainer').removeClass('current');
        }

         $scope.currentaction = currentProfileAction;
         /*----------------End Escort tab controller------------*/

    $( '#hider' ).show();
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
  };
   $scope.changePlan = function(id) {
     $scope.plan.id = id;
     window.currentPlan = id;
   }
    $scope.popupPlanSubmit = function() {
    $scope.popupPlan = {};
    $scope.popupPlan.plan_id = window.currentPlan;
    $scope.popupPlan.user_id = window.id;
    $( '.load-container' ).show();

     return $http({
                  method : 'POST',
                  url: '/services/changeplancontact',
                  data: $scope.popupPlan
            }).then(function successCallback(response) {

              $( '.load-container' ).hide();
              if ( window.type == 'business'){
                window.planEscorts = response.data.escort_profiles;
                $scope.plan = response.data;
                if ( parseInt( window.planEscorts ) > parseInt( window.escortCount ) ){
                    $scope.changeScreens('_MpEscorts');
                    $scope.escortAllowed = 1;
                    $scope.creatingEscort = 1;
                    setTimeout(function(){
                       $( '.overlay').hide();
                    }, 500);
                }
              }
              else {
                 $scope.plan = response.data;
                 $( '#mpTouring').click();
                 $('html, body').animate({ scrollTop: 0 });
              }
     });
  }
  $scope.upgradeAccount = function() {
    if ( type =='escort'){
      changeScreens('_MpProfile');
        setTimeout(function(){

                              }, 1500);

    }
      if ( window.planEscorts > window.escortCount ){
           $scope.createNewEscort();
      }
      else {
         $( '#planPopup' ).show();
         $( '#overlay').show();
      }
  }
  $scope.changeBusinessPlan = function() {
      $scope.changeScreens('_MpProfile');
       setTimeout(function(){
          $( '.active').removeClass( 'active');
          $( '#mpPlans').addClass( 'active');
          $( '.mPtab' ).removeClass( 'activeTab');
          $( '#tab_mpPlans').addClass( 'activeTab');
                              }, 700);
  }
  $scope.changeScreens = function(stateid, scope) {
        /*     if ( $( '.ng-dirty').length ) {
                     alert( 'You have unsaved work.  Are you sure you want to do this?');
                     return;
              }*/
            //  $( '.panelController' ).addClass( 'loading' );
            window.myScreen = stateid;
            $scope.activeicon = stateid;
            if ( stateid == '_MpEscorts' && window.type =='business' && window.currentEscort == 0  ){
              $scope.creatingEscort = 1;
           }
            else {
              $scope.creatingEscort = 0;
            }
            if ( window.type == 'business' && stateid == '_MpProfile'){
              $scope.updateBusiness();
            }
            if ( window.type =='business' && stateid == '_MpEscorts'){
              $scope.changeEscort( window.currentEscort );
            //  $scope.updateEscort();
            }
            if ( window.type =='admin' && stateid =='_MpEscorts'){
              $scope.changeEscort( window.currentEscort );
              $scope.changeEscortScreen("Edit");
            }
            if ( window.type != 'escort' && stateid == '_MpProfile'){
              $scope.changeBusiness( window.currentBusiness );
            }
            if ( window.type == 'escort' && stateid == '_MpProfile'){
            //  $scope.changeEscort( window.active_id );
              $scope.updateEscort();
            }
         //   $scope.activeicon = stateid;hu
            $( '.mPActiveIcon').removeClass( 'mPActiveIcon' );
            $('#' + stateid + '_').addClass('mPActiveIcon');
            $('.panelController').removeClass('loading');

             $('#mobileArrowToggle').removeClass("arrowToggleClicked");
             $(".mobile-nav-container").animate({ width: "0%"});
           /*  if ( stateid == '_MpEscorts'){
                $scope.updateEscort( window.currentEscort );
             }*/
        }
  $scope.locations = [];
  $scope.autocomplete = [];
  $scope.searchSuburbs = function(search) {
         $( 'body' ).not( "#sub-autocomplete" ).off().on('click',function() {
           $scope.locations = [];
         })
         dashboard.getAutoComplete(search).success(function(response) {
            $scope.locations = response;
        }).error(function(response) {
            console.log( response );
        })
  }
  $scope.chooseLocation = function( id,name,formmodel,formval ){
     formmodel.suburb_name = name;
     $scope.autocompletevalue = name;
     formmodel.suburb_id = id;
     $scope.autocomplete.suburb_id = id;
     $scope.locations = [];
     $scope.autocompletevalue = '';
     dashboard.getLatLong(id).success(function(response) {
           formmodel.longitude = response.result.geometry.location.lng;
           formmodel.latitude = response.result.geometry.location.lat;
           formmodel.state = response.result.address_components[1].short_name;
        }).error(function(response) {
            console.log( response );
        })
  }
  $scope.closeSelect = function() {
    $scope.locations = [];
  }
  $scope.navigateProfile = function(){


       /* if (  $( '.ng-dirty').not('ng-submitted').length ) {
          alert( 'dirty');
            $func = $( 'form.ng-dirty').not('ng-submitted').attr('ng-submit');
            $func = $func.substring(0, $func.length - 2);
            if ( $scope[$func]() === 'function'){
                $scope[$func]();
            }
            else {
              alert( 'hi che');
            }
        }*/ //   $rootScope.$emit('cf');
             //  $scope.$broadcast('gc');
             //  $scope.$broadcast('av');

  }
  $scope.goToProfile = function(type,id){
      window.currentBusiness = id;
      $scope.currentbusiness = id;
      window.active_id = id;
     if ( type == 'business'){
        $timeout(function() {
          $scope.changeScreens('_MpProfile');
        }, 500 );

     }
     else if( type =='escort'){
        $timeout(function() {
          window.currentEscort = id;
          $scope.currentEscort = id;
          window.active_id = id;
          $scope.changeScreens('_MpEscorts');
        }, 200 );
     }
  }
  var type = getUrlParameter('type');
  if ( type != undefined ){
      $scope.goToProfile(type,getUrlParameter('id'));
  }

        $scope.activeicon = mPActiveIcon;
        $scope.id = id;

        $scope.getImages = function(){
            $scope.$broadcast('gc');
            $scope.navigateProfile();
        }

        $scope.openSubMenu = function($id) {
            if($( '.' + $id + '-mobile-submenu div').css('display') === 'block'){
               $( '.' + $id + '-mobile-submenu .downCaret' ).removeClass( 'downCaretOpened');
               $( '.mobile-submenu div').hide();
            }
            else {
               $( '.' + $id + '-mobile-submenu .downCaret' ).addClass( 'downCaretOpened');
               $( '.mobile-submenu div').hide();
               $( '.' + $id + '-mobile-submenu div').show();
            }
        }

        $scope.mPActiveIcon = mPActiveIcon;
        $scope.busMenu = [];
        $scope.$on('test', function(ngRepeatFinishedEvent) {
            $('#' + $scope.mPActiveIcon + '_').addClass('mPActiveIcon');
            $('.panelController').removeClass('loading');
        });

        $scope.getPhoneNumber = function( phone, id ){
            if ( $( '#revealPhone_' + id ).html() != phone ) {
              $( '#revealPhone_' + id ).html( phone );
              return $http({
                 method : 'POST',
                  url: '/escorts/' + id + '/reveal-phone'
              }).then(function successCallback(response) {
              }, function errorCallback(response) {
                 console.log( response );
              });
            }
      }
       $scope.sendPlaymail = function() {
           data = $scope.playmail;
           $( '#playmailSpinner').show();
           $( '#contactEscortForm form' ).hide();
              return $http({
                  method : 'POST',
                  url: '/escort/sendUserPlaymail',
                  data: data
              }).then(function successCallback(response) {
                  if ( response.data == 'success!') {
                    $( '#playmailSpinner').hide();
                    $( '#playMessage').show();
                    $( '#playMessage').html( 'Your message has been sent.');
                    setTimeout(function(){
                            $( '#contactEscortForm' ).hide();
                            $( '#playMessage').html( '');
                            $( '#playMessage').hide();
                            $( '#mPOverlay' ).hide();
                              }, 1500);
                  }
                  else {
                    $( '#playmailSpinner').hide();
                    $( '#playMessage').show();
                    $( '#playMessage').html( 'There was an error sending your message. Please try again later.');
                    setTimeout(function(){
                            $( '#contactEscortForm' ).hide();
                            $( '#playMessage').html( '');
                            $( '#playMessage').hide();
                            $( '#mPOverlay' ).hide();
                              }, 1500);
                  }
              }, function errorCallback(response) {
                    $( '#playmailSpinner').hide();
                    $( '#playMessage').show();
                    $( '#playMessage').html( 'There was an error sending your message. Please try again later.');
                    setTimeout(function(){
                            $( '#contactEscortForm' ).hide();
                            $( '#playMessage').html( '');
                            $( '#playMessage').hide();
                            $( '#mPOverlay' ).hide();
                        }, 1500);
              });
       }
       $scope.sendPlaymailTo_ = function(id){
           $scope.playmail = {};
           $scope.playmail.escort_id = id;
           $( '#mPOverlay' ).show();
           $( '#contactEscortForm' ).show();
       }

      $scope.saveEscortAvailability = function() {
            escId = ( window.currentEscort ? window.currentEscort : active_id );
            mpmApi.post('Escort', 'update', {
                'id': escId
            }, $scope.profileAvailabilites);
        }

       $scope.changeMobileSub = function(type, id ){
          if ( type == '_MpEscorts' || type == '_MpReviews'){
             window.currentEscort = id;
             $scope.currentescort = id;
           //  $scope.changeEscort(id);
             $scope.changeScreens(type);
          }
          if ( type == '_MpProfile'){
             currentProfileAction = id;
             $scope.changeScreens('_MpProfile');
             $scope.changeProfileTab(id);
          }
       }
        $scope.escNewEscort= function() {
            $scope.newEscort.status = 1;
            $scope.newEscort.user_id = $scope.id;
            $scope.newEscort.active = 1;
            callback = function(id) {
                $scope.active_id = id.data;
                window.active_id = id.data;
                window.currentEscort = id.data;
                $scope.currentprofileaction = 'Edit';
                $scope.changeScreens('_MpOverview');
                $scope.changeScreens('_MpProfile');
                $scope.updateEscort();
                $( '#newHider').removeClass( 'hidden');
                $( '#newHeader').hide();
                $( '#escStatus').html( 'Pending');
                $( '._mpStatus').attr( 'id', '#mpPending' );
                dashboard.getDrivers().success(function(response) {
                    $scope.busMenu = response.busMenu;
                })
            }
            mpmApi.post('Escort', 'create', {}, $scope.newEscort, callback);
        }
        $scope.newBusiness = function() {
             $scope.busBasicInfoForm.status = 1;
             $scope.busBasicInfoForm.user_id = $scope.id;
             callback = function(id) {
                window.isBusiness = 1;
                window.active_id = id.data;
                window.currentBusiness = id.data;
                $scope.currentprofileaction = 'Edit';
                $scope.changeProfileTab("Edit");
                $scope.updateBusiness();
                dashboard.getDrivers().success(function(response) {
                    $scope.busMenu = response.busMenu;
                })

            }
            mpmApi.post('Business', 'create', {}, $scope.busBasicInfoForm, callback);

        }
        $scope.newAdminBusiness = function() {
             $scope.busBasicInfoForm.status = 1;
             callback = function(id) {
                window.active_id = id.data;
                $scope.currentprofileaction = 'Edit';
                $scope.changeProfileTab("Edit");
                $scope.changeBusiness(id.data);
                $scope.creatingBusiness = 0;
            }
            mpmApi.post('Business', 'create', {}, $scope.busBasicInfoForm, callback);
        }
        $scope.saveBusBasicInfo = function() {
            mpmApi.post('Business', 'update', {
                'id': active_id
            }, $scope.busBasicInfoForm);
        }
        $scope.saveEscortInfo = function() {
            escId = ( window.currentEscort ? window.currentEscort : active_id );
            mpmApi.post('Escort', 'update', {
                'id': escId
            }, $scope.basicInfoForm );
        }
        $scope.saveEscortProfile = function() {
            escId = ( window.currentEscort ? window.currentEscort : active_id );
            mpmApi.post('Escort', 'update', {
                'id': escId
            }, $scope.profileDetails);
        }
        $scope.saveprofileDetails = function() {
            mpmApi.post('Business', 'update', {
                'id': active_id
            }, $scope.busProfileDetails);
        }
        $scope.saveUser = function() {
            mpmApi.post('User', 'update', {
                'id': id
            }, $scope.userDetails);
        }
        $scope.saveEscortTouring = function() {
          escId = ( window.currentEscort ? window.currentEscort : active_id );
          $scope.profileTouring.escort_id = escId;
          callback = function(response) {
            $scope.touring.push({'escort_id': $scope.profileTouring.escort_id, 'from_date' : $scope.profileTouring.from_date, 'to_date' : $scope.profileTouring.to_date, 'suburb_name': $scope.profileTouring.suburb_name,'id': response.data.id });
            $scope.profileTouring = {};
            window.blockedDates = response.data.blockedDates;
            $scope.autocompletevalue = '';
          }
          mpmApi.post('EscortTouring', 'create', {
             'id': id
            }, $scope.profileTouring,callback);
        }
        $scope.deleteTouring = function(id){
           escId = ( window.currentEscort ? window.currentEscort : active_id );
           $scope.deleteT = {};
           $scope.deleteT.escort_id = escId;
          callback = function(response){
            $( '#touringEntry-' + id ).remove();
            window.blockedDates = response.data.blockedDates;
          }
          mpmApi.post('EscortTouring', 'delete', {
                 'id': id
            }, $scope.deleteT,callback );
        }
        $scope.saveEscortSeo = function(){
          mpmApi.post('Escort', 'update', {
                'id': window.currentEscort
            }, $scope.profileSeo);
        }
        $scope.saveBusinessSeo = function(){
          mpmApi.post('Business', 'update', {
                'id': window.active_id
            }, $scope.businessSeo);
        }
        $scope.deleteEscort = function() {
          $scope.deleteEscortForm = {};
          $scope.deleteEscortForm.under_business = 0;
          $scope.deleteEscortForm.business_id = 0;
          $scope.deleteEscortForm.status = 4;
          window.escortCount--;
          $scope.escortCount--;
          callback = function(id) {
            $( '.escortContainer #' + window.currentEscort  ).remove();
            if ( window.escortCount <= window.planEscorts ){
                 $( '#new_escort').show();
                 $( '#upgradePlan').hide();
            }
            if ( window.escortCount == 0 ){
              $scope.newEscort = {};
              $scope.changeEscortScreen("New");
              $scope.creatingEscort = 1;
              $scope.escortAllowed = 0;
              setTimeout(function(){ $( '.overlay').hide() }, 1000);
            }
            else {
              $scope.changeEscort( $( '.escortContainer div:first').attr( 'escortid') );
              $scope.changeEscortScreen("Edit");
            }
          }
          mpmApi.post('Escort', 'update', {
                'id': window.currentEscort
          }, $scope.deleteEscortForm, callback );
      }
       $scope.makeUnavailableEscort = function() {
          $scope.muEscortForm.status = 4;
          mpmApi.post('Escort', 'update', {
                'id': $scope.muEscortForm.id
          }, $scope.muEscortForm );
      }

        dashboard.getDrivers().success(function(response) {
            $scope.busMenu = response.busMenu;
        })
        $scope.busNewEscort = function() {
            $scope.newEscort.under_business = 1;
            $scope.newEscort.business_id = active_id;
            $scope.newEscort.status = 1;
            $scope.newEscort.user_id = $scope.id;
            $scope.newEscort.active = 1;
            callback = function(id) {
                dashboard.getDrivers().success(function(response) {
                    $scope.busMenu = response.busMenu;
                })
                window.escortCount ++;
                $scope.escortCount++;
                $scope.changeEscort(id.data);
                $scope.creatingEscort = 0;
                $scope.currentaction = 'view';
                $scope.changeEscortScreen("edit");
                $scope.escort_status = 'Pending';
                $scope.escortStats.push({'count_phone':0, 'count_playmail': 0, 'count_views':0,'escort_name': $scope.newEscort.escort_name,'id':$scope.newEscort.id, 'main_image': 'qCe0BsNoHWu3.jpg', 'main_image_id':'0'});

                $( '.escortContainer' ).append($compile('<escorttab tabactive="current" escortid = "' + id.data + '" image = "/assets/dashboard/images/EscortHolder.jpg" name = "' + $scope.newEscort.escort_name + '"></escorttab>' )($scope));
                if ( window.escortCount >= window.planEscorts ){
                    $( '#new_escort').hide();
                    $( '#upgradePlan').show();
                }
            }
            mpmApi.post('Escort', 'create', {}, $scope.newEscort, callback);
        }
        $scope.adminNewEscort = function() {
            $scope.newEscort.status = 1;
            $scope.newEscort.active = 1;
            callback = function(id) {
              $scope.changeEscortScreen("Edit");
              $scope.changeEscort(id.data);
              $( '.mp_switch' ).show();
              $( '.escStatus').show();
            }
            mpmApi.post('Escort', 'create', {}, $scope.newEscort, callback);
        }
        $scope.changeBusinessScreen = function(view) {
            $scope.currentaction = view;
            currentProfileAction = view;
            $('.mp_switch').removeClass('active');
            $('#mPesmen_' + view).addClass('active');
            dashboard.getBusProf( window.currentBusiness ).success(function(response) {

                $scope.busBasicInfoForm = {};
                $scope.profileGallery = {};

               $scope.busBasicInfoForm.name = response.name;
               $scope.busBasicInfoForm.phone = response.phone;
               $scope.busBasicInfoForm.email = response.email;
               $scope.busBasicInfoForm.website = response.website;
               $scope.busBasicInfoForm.type = response.type;
               $scope.busBasicInfoForm.address_line_1 = response.address_line_1;
               $scope.busBasicInfoForm.address_line_2 = response.address_line_2;
               $scope.profileGallery.about= response.about;

               $( '#autocomplete').val(response.autocompleteaddress);
                   if ( response.banner_image )
                   $( '#bannerUpload' ).css({ 'background': 'url("https://d2enq9fr2fmuvm.cloudfront.net/business/' + response.banner_image + '")' });
               });
        }


        $scope.changeEscort = function(escortid) {
          //  $scope.currentescort = escortid;
            if ( window.myScreen != '_MpEscorts'){
               $scope.changeScreens('_MpEscorts');
               $scope.changeEscortScreen("edit");
            }
            currentEscort = escortid;
            $scope.currentescort = escortid;
            $('.escortTabContainer').removeClass('current');
            $('#' + escortid).addClass('current');
            $( '.no_escorts ').hide();
            $scope.updateEscort(escortid, $scope );
            $( '.active').removeClass('active');
            $( '#mpBasicInfo').addClass( 'active');
            $( '.mPtab').removeClass('activeTab');
            $( '#tab_mpBasicInfo').addClass( 'activeTab' );
            $('html, body').animate({ scrollTop: 0 });
        }

      /*  $scope.mobileMenu = function() {
             $http.get('/services/businessMobileMenu/' + id + '/' + active_id + '/' + type ).
                success(function(data, status, headers, config) {
                $scope.mobileMenu = data;
                console.log( data );
            }).
            error(function(data, status, headers, config) {
                console.log(data);

            });
        };
        $scope.mobileMenu();*/

        $scope.reviewAction = function(action, reviewId ,actionId,altId,altAction){
            if ( mpmApi['review' + reviewId] != 'alt' ) {
                mpmApi.reviewSubmit(action, reviewId ,actionId,altId,altAction);
            }
            else {
                mpmApi.reviewSubmit(altAction, reviewId ,altId,actionId,action);
            }
        }
        $scope.currentprofileaction = currentProfileAction;

        $scope.changeProfileTab = function(view) {
            $scope.currentprofileaction = view;
            currentProfileAction = view;
            $('._mp_button').removeClass('active');
            $('#_mPProfileac_' + view).addClass('active');
        }
         $scope.changePaymentTab = function(view) {
            $scope.currentpaymentaction = view;
            currentPaymentAction = view;
            $('._mp_button').removeClass('active');
            $('#_mPProfileac_' + view).addClass('active');
        }
            $scope.upload = function() {
            file = blobToFile(Upload.dataUrltoBlob($scope.croppedDataUrl, 'filename'));
               Upload.upload({
                   url: '/services/uploadTempImage',
                   data: {
                       file: file
                   },
               }).then(function (resp) {
                   $scope.profileGallery.banner_image = resp.data;
                   $scope.profileGallery.new_banner = 1;
                      $( '#bannerUpload').css({
                         'background-image': 'url( "/temp/' + resp.data + '")'
                      })
                      $( '#_cropSquare').hide();
                      $( '#overlayBackGround').hide();
               }, function (resp) {
                   console.log('Error status: ' + resp);
               }, function (evt) {
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                      $( '#bannerUploadStatus' ).removeClass().addClass( '_l' + progressPercentage  );
               });
        };

        $scope.getBusinessImages = function() {
            $scope.$broadcast('gbi');
        }

        $scope.galleryUpload = function($file) {
            Upload.upload({
                url: '/services/uploadTempGalleryImage',
                data: {
                    file: $file
                },
            }).then(function(resp) {

                $scope.busBasicInfoForm.newImage = [];
                $scope.busBasicInfoForm.newImage.push({
                    filename: resp.data,
                    newImage: 1,
                    id: 'newImage_' + resp.data
                });
                $( '#addImage').html('<img src ="/temp/' + resp.data + '">' );
                $('#galleryLoading').removeClass();
                $('#galleryLoadingMobile').removeClass();
            }, function(resp) {
                console.log('Error status: ' + resp);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $('#galleryLoadingMobile').removeClass().addClass('_l' + progressPercentage);
            });
        }
        $scope.updateEscort = function(){
        //  $( '#container_image img').attr('src', '/assets/frontend/img/tail-spin.svg' );

           dashboard.getEscProf( window.currentEscort ).success(function(response ) {

            if ( response == '' ){
              return;
            }
            if ( !$scope ){
               return;
            }

               $scope.basicInfoForm = {};
               $scope.profileDetails = {};

               $scope.basicInfoForm.escort_name = response.escort.escort_name;
               $scope.basicInfoForm.phone = response.escort.phone;
               $scope.basicInfoForm.email = response.escort.email;
               $scope.basicInfoForm.suburb_name = response.escort.suburb_name;
               $scope.basicInfoForm.contact_playmail = response.escort.contact_playmail;
               $scope.basicInfoForm.contact_phone = response.escort.contact_phone;
               $scope.basicInfoForm.gender_id = response.escort.gender_id;
               $scope.basicInfoForm.age_id = response.escort.age_id;
               $scope.escort_status = response.escort.escort_status;
               $scope.basicInfoForm.under_business = response.escort.under_business;
               $scope.basicInfoForm.business_id = response.escort.business_id;
               $scope.basicInfoForm.user_id = response.escort.user_id;
               $scope.basicInfoForm.status = response.escort.status;
               $scope.main_image_src = ( response.escort.main_image_src ) ? response.escort.main_image_src : 'myCukja2DHc3.jpg';
               $scope.basicInfoForm.id = response.escort.id;
               $scope.basicInfoForm.user_id = response.escort.user_id;
               $scope.basicInfoForm.about_me = response.escort.about_me;
               $scope.basicInfoForm.under_business = response.escort.under_business;
               $scope.basicInfoForm.own_url = response.escort.own_url;

               $scope.profileDetails.body_id = response.escort.body_id;
               $scope.profileDetails.ethnicity_id = response.escort.ethnicity_id;
               $scope.profileDetails.eye_color_id = response.escort.eye_color_id;
               $scope.profileDetails.hair_color_id = response.escort.hair_color_id;
               $scope.profileDetails.height = response.escort.height;
               $scope.profileDetails.height_id = response.escort.height_id;
               $scope.previewUrl = '/escorts/preview/' + response.escort.id;
               $scope.autocompletevalue = '';

                $scope.profileDetails.about_me = response.escort.about_me;

               /*-----------------Availabilites-------------
               ------------------------------------------------*/

               $scope.profileAvailabilites = {};

                $scope.profileAvailabilites.availabilites = {
                     avail: [],
                     val: response.availabilites.days,
                     start_hour_id: [],
                     end_hour_id: [],
                     start_am_pm: [],
                     end_am_pm: [],
                     twenty_four_hours: []
                 };
                 $scope.availabilites = {
                     val: response.availabilites.days
                 };

                $.each( response.availabilites.days, function( index, value ) {
                    $scope.profileAvailabilites.availabilites.start_hour_id[index] = response.availabilites.days[index].start_hour_id;
                    $scope.profileAvailabilites.availabilites.end_hour_id[index] = response.availabilites.days[index].end_hour_id;
                    $scope.profileAvailabilites.availabilites.start_am_pm[index] = response.availabilites.days[index].start_am_pm;
                    $scope.profileAvailabilites.availabilites.end_am_pm[index] = response.availabilites.days[index].end_am_pm;
                    if ( response.availabilites.days[index].enabled == 1 ) {
                           $scope.profileAvailabilites.availabilites.avail.push(index);
                    }
                    if ( response.availabilites.days[index].twenty_four_hours == 1 ) {
                           $scope.profileAvailabilites.availabilites.twenty_four_hours.push(index);
                    }
                });
                $scope.profileAvailabilites.does_incalls = response.escort.does_incalls;
                $scope.profileAvailabilites.does_outcalls = response.escort.does_outcalls;
                $scope.profileAvailabilites.notes = response.escort.notes;
                $scope.profileAvailabilites.rates = {};
                $scope.incalllist = response.inCallList;
                $scope.outcalllist = response.outCallList;
                $scope.outCall = ( response.escort.does_outcalls == 1 ? true : false );
                          if( response.escort.does_incalls == 1 ){
                              $('#inCallTable').removeClass('disabledTable');
                          } else {
                               $('#inCallTable').addClass('disabledTable');
                          }
                          if( response.escort.does_outcalls == 1 ){
                            $('#outCallTable').removeClass('disabledTable');
                           } else {
                            $('#outCallTable').addClass('disabledTable');
                          }
               /*-----------------------------Touring------------------------
               -----------------------------------------------------------------*/
               $scope.touring = response.tour;
               window.blockedDates = response.blockedDates;

               /*-------------------Escort rates -----------------
               ---------------------------------------------------------*/
			   if(response.escortRates.length > 0) {
                $.each(response.escortRates[0], function($index, value) {
                        $scope.profileAvailabilites.rates[$index] = value;
                    });
			   }
               services = [],
               escortServices = [];
               $.each(response.services, function( index, value ) {
                  if ( this.constructor === Array ){
                     services.push( {name: this[1], id: index });
                     escortServices.push( index );
                  }
                  else {
                     services.push({ name: this.valueOf(), id: index });
                  };
               });
              languages = [],
              escortLanguages = [];
              $.each(response.languages, function( index, value ) {
                  if ( this.constructor === Array ){
                      escortLanguages.push( index );
                      languages.push( { id: index, name: this[1] } );
                  }
                  else {
                    languages.push({ name: this.valueOf(), id: index });
                  }
              });
                $scope.languages = languages;
                $scope.profileDetails.languages = {
                  escortLanguages: escortLanguages
              };
               $scope.services = window.services;
               $scope.profileDetails.services = {
                   escortServices: window.escortServices
               };
               $scope.profileSeo = {};
                $scope.profileSeo.seo_title = response.escort.seo_title;
                $scope.profileSeo.seo_keywords = response.escort.seo_keywords;
                $scope.profileSeo.seo_description = response.escort.seo_description;
                $scope.profileSeo.seo_content = response.escort.seo_content;
                $scope.profileSeo.tag_header = response.escort.tag_header;
                $scope.profileSeo.about_header = response.escort.about_header;
               });

        }
        $scope.getHeight = function() {

                  $("#heightSelect").slider('value',$scope.profileDetails.height );
                  $( '#heightSelectlabel').html($scope.profileDetails.height + 'cm');
                  $( '.heightReadable').html( $scope.profileDetails.height + 'cm ( ' + toFeet( $scope.profileDetails.height ) + ' )');
}
        $scope.checkInCalls = function() {
           return ( $scope.profileAvailabilites.does_incalls = 1 ? 'profileTable ratesTable' : 'profileTable ratesTable disabledTable' );
        }
        $scope.updateBusiness = function(busId){

          if ( window.type == 'escort' )
              return;
          if ( !window.currentBusiness )
              return;
          if ( window.currentBusiness == 'New')
            return;

          dashboard.getBusProf( window.currentBusiness ).success(function(response) {

                  if ( response ){

                        $scope.busBasicInfoForm = {};
                        $scope.profileGallery = {};
                        $scope.businessSeo = {};

                        $scope.busBasicInfoForm.name = response.name;
                        $scope.busBasicInfoForm.image = ( response.image ? response.image  : 'ZhAVHJ6zcfM4.jpg' );
                        $scope.busBasicInfoForm.phone = response.phone;
                        $scope.busBasicInfoForm.email = response.email;
                        $scope.busBasicInfoForm.website = response.website;
                        $scope.busBasicInfoForm.suburb_name = response.suburb_name;
                        $scope.busBasicInfoForm.type = response.type;
                        $scope.busBasicInfoForm.address_line_1 = response.address_line_1;
                        $scope.busBasicInfoForm.address_line_2 = response.address_line_2;
                        $scope.busBasicInfoForm.user_id = response.user_id;
                        $scope.busBasicInfoForm.status = response.status;
                        $scope.businessStatus = response.business_status;
                        $scope.previewBusinessUrl = response.previewUrl;
                        $scope.businessSeo.seo_title = response.seo_title;
                        $scope.businessSeo.seo_keywords = response.seo_keywords;
                        $scope.businessSeo.seo_description = response.seo_description;
                        $scope.businessSeo.seo_content = response.seo_content;
                     //   $scope.plan = response.plan;

                        setTimeout(function(){
                            $('#autocomplete').val( response.autocompleteaddress );
                            }, 500);
                        }
                        if ( response.type == 2 )
                          setTimeout(function(){
                            $("#autocomplete").removeAttr('required');
                          }, 500);
                   });
        }

    })
    .directive('autocomplete', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'EA',
            controller: 'driversController',
            scope: {
               formmodel: '=formmodel',
               formval: '=formval'
             },
            templateUrl: function(elem, attrs) {
                    return '/directives/autocomplete/';
            },
        }
    })
    .directive('onfinishavailability', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function() {
                        $("#availabilityForm select").prop('disabled', true);
                        $(".twenty_four_check").prop('disabled', true);
                        $('.availDayCheck:checked').each(function(index) {
                            $(this).parents('tr').find(".twenty_four_check").prop('disabled', false);
                            if (!$(this).parents('tr').find('.twenty_four_check').is(':checked')) {
                                $(this).parents('tr').find("select").prop('disabled', false);
                            }

                        });
                    });
                }
            }
        }
    })
    .directive('onFinishRender', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function() {
                        scope.$emit(attr.onFinishRender);
                    });
                }
            }
        }
    })
    .directive('panel', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            scope: false,
            link: function(scope, element, attrs) {

                attrs.$observe('panelid', function(value) {

                    panelid = attrs.panelid;
                    if (cache.hasOwnProperty(panelid)) {
                        element.html(cache[panelid]);
                        $compile(element.contents())(scope);
                        $('.panelController').removeClass('loading');
                      }
                     else {
                        panelid = attrs.panelid;
                        template = '/services/' + type + 'Panel/' + id + '/?panel=' + attrs.panelid + '&busid=' + window.active_id
                        $http.get(template, {
                        }).success(function(tplContent) {
                            element.html(tplContent);
                            $compile(element.contents())(scope);
                            $('.panelController').removeClass('loading');
                        });
                    }
                });

            },
            templateUrl: function(elem, attrs) {
                if (attrs.panelid == '{{activeicon}}') {

                    return '/services/' + type + 'Panel/' + id + '/?panel=' + mPActiveIcon + '&busid=' + window.active_id
                } else {
                    return '/services/' + type + 'Panel/' + id + '/?panel=' + attrs.panelid + '&busid=' + window.active_id
                }
            },
        }
    })
    .controller('panelController', function($scope, dashboard) {


    })
    .directive('screen', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: function(elem, attrs) {
                if (attrs.screenid == '{{activeicon}}') {
                    return '/services/' + type + 'Screen/' + id + '/?panel=' + mPActiveIcon + '&busid=' + active_id
                } else {
                    return '/services/' + type + 'Screen/' + id + '/?panel=' + attrs.screenid + '&busid=' + active_id
                }
            },
            link: function(scope, element, attrs) {
                    attrs.$observe('screenid', function(value) {

                    screenid = attrs.screenid + 'screen';
                    if (cache.hasOwnProperty(screenid + 'screen')) {
                        element.html(cache[screenid + 'screen']);
                        $compile(element.contents())(scope);
                        $('.panelController').removeClass('loading');
                      }
                     else {
                    template = '/services/' + type + 'Screen/' + id + '/?panel=' + attrs.screenid + '&busid=' + active_id

                    $http.get(template, {
                        cache: $templateCache
                    }).success(function(tplContent) {
                        cache[screenid + 'screen'] = tplContent;
                        element.html(tplContent);
                        $compile(element.contents())(scope);

                    });
                  }
                });

            },
        }
    })
.directive('ngModelOnblur', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1, // needed for angular 1.2.x
        link: function(scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function() {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(elm.val());
                });
            });
        }
    };
})
.directive('profileinnertab', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            scope: {
                profiletabid: '@'
            },
            link: function(scope, element, attrs) {

                attrs.$observe('profiletabid', function(value) {

                    panelid = attrs.panelid;
                    panelid = attrs.panelid;
                    template = '/services/ProfieInnertab/' + id + '/?tab=' + attrs.panelid + '&busid=' + active_id
                    $http.get(template, {
                    //    cache: $templateCache
                    }).success(function(tplContent) {
                    //    cache[panelid] = tplContent;
                        element.html(tplContent);
                        $compile(element.contents())(scope);
                        $('.panelController').removeClass('loading');
                    });
                })
            },
            templateUrl: function(elem, attrs) {
                if (attrs.profiletabid == '{{activeicon}}') {
                    return '/services/' + type + 'ProfileTab/' + id + '/?tab=' + profiletabid + '&busid=' + active_id
                } else {
                    return '/services/' + type + 'ProbileTab/' + id + '/?tab=' + attrs.profiletabid + '&busid=' + active_id
                }
            },
            controller: 'profileTabController'
        }
    })
    .controller('profileTabController', function($scope, $attrs,dashboard) {


        $scope.currentprofileaction = currentProfileAction;

        $scope.changeProfileTab = function(view) {
            $scope.currentprofileaction = view;
            currentProfileAction = view;
            $('._mp_button').removeClass('active');
            $('#_mPProfileac_' + view).addClass('active');

            if ( window.type == 'escort' && view == 'Edit'){
                $scope.updateEscort();
              }
              else if ( view == 'Edit' ) {
                $scope.updateBusiness();
              }
        }
    })
    .directive('profiletabswitcher', function() {
        return {
            restrict: 'E',
            controller: 'profileTabController',
            template: function(elem, attrs) {
                $class = '';
                if (attrs.tabaction != '')
                    $class = ' ' + attrs.tabactive;
                return '<button ng-click = changeProfileTab("' + attrs.tabaction + '") id ="_mPProfileac_' + attrs.tabaction + '" class ="_mp_button' + $class + '">' + attrs.tabaction + '</button><br>';
            }
        }
    })
    .directive('paymenttabswitcher', function() {
        return {
            restrict: 'E',
            controller: 'profileTabController',
            template: function(elem, attrs) {
                $class = '';
                if (attrs.tabaction != '')
                    $class = ' ' + attrs.tabactive;
                return '<button ng-click = changePaymentTab("' + attrs.tabaction + '") id ="_mPProfileac_' + attrs.tabaction + '" class ="_mp_button' + $class + '">' + attrs.tabaction.replace(/_/g, ' ') + '</button><br>';
            }
        }
    })
    .directive('profilescreen', function($http, $templateCache, $compile, $parse) {

        return {
            restrict: 'E',
            scope: false,
            templateUrl: function(elem, attrs) {
                    if ( window.isBusiness == '0' )
                         currentProfileAction = 'New';
                    return '/services/' + type + 'ProfileTab/' + currentProfileAction;
            },
            controller: 'profileTabController',
            link: function(scope, element, attrs) {

                attrs.$observe('profileaction', function(value) {
                   screenid = attrs.profileaction + 'screen';
                    if (cache.hasOwnProperty(screenid)) {
                        element.html(cache[screenid]);
                        $compile(element.contents())(scope);
                        $('.panelController').removeClass('loading');
                      }
                    else {
                    template = '/services/' + type + 'ProfileTab/' + attrs.profileaction;
                    $http.get(template, {
                        cache: $templateCache
                    }).success(function(tplContent) {
                        cache[screenid] = tplContent;
                        element.html(tplContent);
                        $compile(element.contents())(scope);
                    });
                  }
                });
            },
        }
    })
    .directive('paymentscreen', function($http, $templateCache, $compile, $parse) {
      if ( typeof currentPaymentAction == 'undefined' ) currentPaymentAction = 'Overview';
        return {
            restrict: 'E',
            scope: false,
            templateUrl: function(elem, attrs) {
                    return '/services/' + type + 'PaymentTab/Overview/' + window.id;
            },
            controller: 'profileTabController',
            link: function(scope, element, attrs) {

                attrs.$observe('paymentaction', function(value) {
                  if ( attrs.paymentaction != '' ){
                    template = '/services/' + type + 'PaymentTab/' + attrs.paymentaction + '/' + window.id;
                    $http.get(template, {
                        cache: $templateCache
                    }).success(function(tplContent) {
                        element.html(tplContent);
                        $compile(element.contents())(scope);
                    });
                  }
                });
            },
        }
    })
    .controller('escortTabController', function($scope, $attrs,$rootScope,mpmApi,dashboard ) {

    })
    .directive('escorttab', function() {
        return {
            restrict: 'E',
            template: function(elem, attrs) {
                return '<div id = "' + attrs.escortid + '" ng-click = "changeEscort(' + attrs.escortid + ')" escortid = "' + attrs.escortid + '" class ="escortTabContainer ' + attrs.tabactive + '"><div class ="escortTab"><div><img src ="' + attrs.image + '"></div><span>' + attrs.name + '</span></div></div>'
            }
        }
    })
    .directive('escortr', function() {
        return {
            restrict: 'E',
           // controller: 'escortTabController',
            template: function(elem, attrs) {
                return '<tr id = "' + attrs.escortid + '" ng-click = "changeEscort(' + attrs.escortid + ')" escortid = "' + attrs.escortid + '" class ="escortTabContainer ' + attrs.tabactive + '"><td>' + attrs.id + '</td><td>' + attrs.name + '</td></tr>'
            }
        }
    })
    .directive('escortscreen', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            templateUrl: function(elem, attrs) {

                return '/services/' + type + 'EscortScreen/1';
            }
        }
    })
      .directive('businessscreen', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            templateUrl: function(elem, attrs) {

                return '/services/' + type + 'BusinessScreen/2';
            },
        }
    })
     .directive('reviewscreen', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            templateUrl: function(elem, attrs) {
                    return '/services/reviewScreen/' + window.currentEscort;
            },
            controller: 'escortTabController',
            link: function(scope, element, attrs) {
                attrs.$observe('currentescort', function(newValue, oldValue) {
                       template = '/services/reviewScreen/' + window.reviewEscort
                       $http.get(template, {
                             cache: $templateCache
                      }).success(function(tplContent) {
                           element.html(tplContent);
                           $compile(element.contents())(scope);

                     });
                });
            },
        }
    })
    .directive('lowerescortscreen', function($http, $templateCache, $compile, $parse) {

        return {
            restrict: 'E',
            scope: false,
            templateUrl: function(elem, attrs) {

                 window.currentBusiness = ( window.currentBusiness ? window.currentBusiness : 'New');
                 return '/services/' + type + 'LowerEscortScreen/' + window.currentBusiness + '?action=Edit';

            },
            link: function(scope, element, attrs,$scope) {
                attrs.$observe('escortaction', function(value) {

                  if( window.escortCount == 0 ) attrs.escortaction = 'New';

                  lowerescortscreen = 'screen' + attrs.escortaction;

                  if (cache.hasOwnProperty( lowerescortscreen )) {
                        element.html(cache[lowerescortscreen]);
                        $compile(element.contents())(scope);
                        $('.panelController').removeClass('loading');
                      }
                      else {
                          template = '/services/' + type + 'LowerEscortScreen/' +  window.currentBusiness + '?action=' + attrs.escortaction;
                          $http.get(template, {
                          cache: $templateCache
                            }).success(function(tplContent) {
                            cache[lowerescortscreen] = tplContent;
                             element.html(tplContent);
                             $compile(element.contents())(scope);

                        });
                  }
                });
            },
        }
    })

      .directive('lowerbusinessscreen', function($http, $templateCache, $compile, $parse) {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: function(elem, attrs) {
          //      if ( attrs.business != window.currentBusiness) {
                    return '/services/' + type + 'LowerBusinessScreen/2?action=Edit';
             //   }
            },
         //   controller: 'escortTabController',
            link: function(scope, element, attrs) {
                attrs.$observe('businessaction', function(value) {
                 //   if ( attrs.business == window.currentBusiness && value != attrs.business  && window.myScreen != '_MpReviews' ){
                        lowerbusinessscreen = 'screen' + attrs.businessaction + 'business';

                        template = '/services/' + type + 'LowerBusinessScreen/2?action=' + attrs.businessaction;
                          $http.get(template, {
                         cache: $templateCache
                            }).success(function(tplContent) {
                             cache[lowerbusinessscreen] = tplContent;
                             element.html(tplContent);
                             $compile(element.contents())(scope);

                        });
                 //   }
                });
            },
        }
    })
    .controller('galleryController', ['dashboard', '$scope', '$attrs', '$http', 'Upload', '$timeout', '$rootScope', function(dashboard,$scope, $attrs, $http, Upload, $timeout,$rootScope) {

        $scope.saveprofileDetailsGal = function() {
            mpmApi.post('Business', 'update', {
                'id': active_id
            }, $scope.profileGallery);
        }

        $scope.profileGallery = {};
        $scope.profileGallery.images = [];
        $scope.myImage = '';
        $scope.picFile = '';
        $scope.myCroppedImage = '';
        $scope.profileGallery.images = {};
        $scope.main_image = 'main_image';
        $scope.profileGallery.deletedImages = [];

       $scope.$on('gc', function() {
                $scope.getImages();

        })
         $scope.$on('gbi', function() {
                $scope.getBusinessImages();

        })

         $scope.getImages = function() {

          $scope.profileGallery.images = [];

            escId = ( window.currentEscort ? window.currentEscort : active_id );
            data = {
                id: escId
            };
            var dataReturn;
            $http.get('/services/getImages/' + escId).
                success(function(data, status, headers, config) {
                $scope.profileGallery.images = data.images;
                $scope.profileGallery.main_image = data.main_image;
                $scope.profileGallery.banner_image = data.banner_image;
                if ( data.banner_image != ''){
                   setTimeout(function(){  $( '#bannerUpload' ).css({ 'background': 'url("https://d2enq9fr2fmuvm.cloudfront.net/escorts/' + data.banner_image + '")' });
                }, 2000);
                }
                else {
                  setTimeout(function(){  $( '#bannerUpload' ).css({ 'background': 'url("/assets/dashboard/images/uploadBanner.jpg")' });
                }, 2000);
                }
            }).
            error(function(data, status, headers, config) {
                console.log(data);

            });
        }
        $scope.getBusinessImages = function() {

          $( '#bannerUpload' ).css({ 'background': 'url("/assets/dashboard/images/uploadBanner.jpg")' });


          dashboard.getBusProf( window.currentBusiness ).success(function(response) {

                  if ( response ){

                    $scope.profileGallery = {};
                    $scope.profileGallery.about = response.about;
                    $scope.profileGallery.banner_image = response.banner_image;

                    if ( response.banner_image != ''){
                        setTimeout(function(){  $( '#bannerUpload' ).css({ 'background': 'url("https://d2enq9fr2fmuvm.cloudfront.net/businesses/' + response.banner_image + '")' });
                    }, 2000);
                  }
           }
         });
        }

        $scope.checkGallery = function() {
            //console.log($scope.profileGallery.images);
        }

        $scope.deleteImage = function($index) {
            if ( $scope.profileGallery.images[$index].id.substring( 0, 9 ) != 'newImage_' ) {
               $scope.profileGallery.deletedImages.push( $scope.profileGallery.images[$index] );
            }
            $scope.profileGallery.images.splice($index, 1);
        }
        $scope.makeMain = function($index) {
            $scope.profileGallery.main_image = $scope.profileGallery.images[$index].id;
        }

        $scope.saveEscortGallery = function() {
            escId = ( window.currentEscort ? window.currentEscort : active_id );
            callback = function(){
                  if ( $scope.profileGallery.main_image != '0' && $scope.profileGallery.main_image.substr($scope.profileGallery.main_image.length - 3 )== 'jpg'){
                     $( '#' + escId + ' img').attr('src', '/temp/' + $scope.profileGallery.main_image.substring(9) );
                  }
                /*  $.each( $scope.profileGallery.images, function( index, image ) {
                       delete image['newImage'];
                       console.log( image );
                });*/
               $scope.getImages();
            }
            mpmApi.post('Escort', 'update', {
                'id': escId
            }, $scope.profileGallery, callback );
        }

        $scope.deleteBanner = function() {
          $( '#bannerUpload').css({ 'background': 'url("/assets/dashboard/images/uploadBanner.jpg")' });
             $scope.profileGallery.banner_image = '';
             $scope.profileGallery.banner_approved = '';
        }


        $scope.upload = function() {
            file = blobToFile(Upload.dataUrltoBlob($scope.croppedDataUrl, 'filename'));
               Upload.upload({
                   url: '/services/uploadTempImage',
                   data: {
                       file: file
                   },
               }).then(function (resp) {
                   $scope.profileGallery.banner_image = resp.data;
                   $scope.profileGallery.new_banner = 1;
                      $( '#bannerUpload').css({
                         'background-image': 'url( "/temp/' + resp.data + '")'
                      })
                      $( '#_cropSquare').hide();
                      $( '#overlayBackGround').hide();
               }, function (resp) {
                   console.log('Error status: ' + resp);
               }, function (evt) {
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                      $( '#bannerUploadStatus' ).removeClass().addClass( '_l' + progressPercentage  );
                      $( '#bannerDelete').removeClass('hidden');
               });
        };

        $scope.galleryUpload = function($file) {
            Upload.upload({
                url: '/services/uploadTempGalleryImage',
                data: {
                    file: $file
                },
            }).then(function(resp) {

                $scope.profileGallery.images.push({
                    filename: resp.data,
                    newImage: 1,
                    id: 'newImage_' + resp.data
                });
                $('#galleryLoading').removeClass();
                $('#galleryLoadingMobile').removeClass();
            }, function(resp) {
                console.log('Error status: ' + resp);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $('#galleryLoading').removeClass().addClass('_l' + progressPercentage);
                $('#galleryLoadingMobile').removeClass().addClass('_l' + progressPercentage);
            });
        }
        $scope.imageUpload = function() {
            $('#_cropSquare').show();
            $('#overlayBackGround').show();
            $('#bannerUploadStatus').removeClass();
        }

        $scope.validate = function($file) {

        }
        $scope.validateFn = function($something){
           // console.log( $something );
        }

        $scope.handleFileSelect = function($file) {
            //console.log($file);
            $scope.picFile = $file;
        };

        function blobToFile(theBlob, fileName) {
            //A Blob() is almost a File() - it's just missing the two properties below which we will add
            theBlob.lastModifiedDate = new Date();
            theBlob.name = fileName;
            return theBlob;
        }

    }])
    .directive('bannerupload', function() {
        return {
            restrict: 'E',
          //  controller: 'galleryController',
            template: function(elem, attrs) {
                  return '<button id="bannerUpload" ngf-select="upload($file)"  accept="image/*" ng-model="picFile" ngf-max-size="1MB" ngf-min-width="160"></button><div id = "bannerUploadStatus"></div>';
            }
        }
    })
    .directive('fileOnChange', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.fileOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    })
    .controller('availaiblityform', function($scope, dashboard, $http) {

    }).directive('menuicon', function() {
  return function(scope, element, attrs) {
    if (scope.$last){
        $('#_mpMenu>div:first-child' ).addClass('mPActiveIcon');
        $( '._mPIcon').on( 'click',function() {
            $('.mPActiveIcon').removeClass('mPActiveIcon')
            $( this ).addClass('mPActiveIcon');
       })
    }
  };
})
