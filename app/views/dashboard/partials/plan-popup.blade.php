
<h3 class ='planUpgrade'>Choose your plan</h3>
<form id ='popupPlanForm' ng-model ='popupPlan' ng-submit ='popupPlanSubmit()'>

{{Form::text('plan','',array('class'=>'hidden','ng-model'=>'popupPlan.id'))}}

<div class ='planHolder'>

   
   @if( $plan->type == 1 )
   <div class ='col-md-2'></div>
   @endif
   @foreach( $plans as $plann )	
	  @if($plann->id != 3 )
      <div class ='col-md-4'>  
           <div data-id = '{{ $plann->id }}' ng-class="{'currentPlan planOuter' : ( plan.id == '{{ $plann->id }}' ), 'planOuter' : ( plan.id != '{{ $plann->id }}' )}" ng-click ='changePlan("{{ $plann->id }}")'>

            <div class ='planHeader'>{{$plann->name}}</div>
            <div class ='planDescription'>{{$plann->description}}</div>
            <div class ='planInclusions'>
            @if( $plan->type == 1 )
                <div class ='Touring'>Touring: {{ ( $plann->touring == 0 ? 'Not Included' : 'Included')}}</div>                
            @else
                <div class ='Bumpups'>Up to {{ $plann->escort_profiles }} Escort Profiles</div>
            @endif
            </div>
           <div class ='planPrice'>${{ $plann->price }} Per Month<span class ='smaller'> + GST</span></div>


          </div>
       </div>
	   @endif
	@endforeach
	@if( $plan->type == 1 )
    <div class ='col-md-2'></div>
    @endif
	@if( $plan->type == 1 )	
		<button id ='changePlanContact'>FINISH</button>
	@else
		<button id ='changePlanContact'>NEXT</button>
    @endif
     <div class="load-container load8"><div class="loader">Loading...</div></div>

</div>
</form>
<script>

   window.currentPlan = "{{$plan->id}}";
   $( '.planOuter' ).on('click',function() {
    	 $( '.currentPlan' ).removeClass('currentPlan');
   	     $( this ).addClass('currentPlan');
   	     window.currentPlan = $( this ).data('id');
   });
   $( '#overlay' ).on('click',function() {
      $( '#planPopup' ).hide();
      $( '#overlay').hide();
   });
   </script>
