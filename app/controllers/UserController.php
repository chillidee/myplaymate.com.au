<?php

use  Carbon\Carbon;

class UserController extends Controller
{

    public function getRegister()
    {
        $error = Session::get('error');

        return View::make('frontend.user.register',compact('error'));
    }

    public function login()
    {
        $userdata = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        ];

        if (Auth::attempt($userdata)){
                return Redirect::to('user/profile');
        }

        else
            return Redirect::back()
                ->with('loginFailed','failed')
                ->with('error','Wrong email or password. Please check your credentials.')
                ->withInput();
    }

    public function postRegister()
    {
        $input = Input::all();
        $type = $input['type'];

        $v = Validator::make($input, User::$rules, User::$messages);

        if ($v->fails())
            return Redirect::back()->with('registrationFailed','failed')->withInput()->with('type',compact('type'))->withErrors($v);

        $user = new User();
        $type = ($input['type'] > 4 ? 4 : $input['type']); // returns true
        $user->type = $type;
        if ( $type == 3 ) {
            $user->business_plan = 1;
        }
        else if ( $type == 4 ){
            $user->business_plan = 9;
        }
        $user->email = $input['email'];
        $user->phone = $input['phone'];
        $user->password = Hash::make($input['password']);
        $user->full_name = $input['full_name'];
        $user->active = 0;
        $user->activation_code = str_random(6);
        $user->save();
        $user->sendActivationEmail();

            try   {
            ob_start();
                switch ($type) {
                    case 2:
                        $userType = '6';
                    break;
                    case 3:
                        $userType = '1';
                    break;
                    default:
                        $userType = '7';
                    break;
}

              $url = 'http://crm.adultpress.com.au/public/customer/store';

              $apiKey = openssl_encrypt(Config::get('constants.CRMAPIKEY'), "AES-256-CBC", Config::get('constants.CRMAPIHASH'),true, Config::get('constants.CRMIV') );

              $fields = array(
                 'crmApiKey' => $apiKey,
                 'bname'=> $user->full_name,
                 'firstname'=> $user->full_name,
                 'bemail'=> $user->email,
                 'bphone'=> $user->phone,
                 'phone'=> $user->phone,
                 'account_manager' => '253',
                 'btype' => $userType,
                 'mpm_id' => $user->id,
                 'extend' => '{"facebook":"","twitter":"","instagram":""}',
                 'laraveltrustkey' => 'DfTrwyh452erwHTwh54wgreG54gwhg'
        );

    //url-ify the data for the POST
    $fields_string = '';
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string,'&');

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);

    //execute post
    $result = curl_exec($ch);

     $ret = ob_get_clean ();

    $var = json_decode( $ret, true );

    $url = 'http://crm.adultpress.com.au/public/customer/store';

    $fields = array(
            'crmApiKey' => $apiKey,
            'statusId'  => 1,
            'datetime'  => Carbon::now(),
            'dueDatetime' => Carbon::now(),
            'userId' => 253,
            'customerId' => $var['object']['id'],
            'latest' => 1,
            'extend' => '{"facebook":"","twitter":"","instagram":""}',
            'laraveltrustkey' => 'DfTrwyh452erwHTwh54wgreG54gwhg',
            'account_manager' => 11
        );
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);

        ob_start();

        //$result = curl_exec($ch);

        ob_clean();
            }
              catch (SomeException $e)
            {}

        return Redirect::back()->with('registrationSuccess','success');
    }

    public function activateUser()
    {
        $email = Input::get('email');
        $code = Input::get('activation_code');
        $user = User::whereEmail($email)->first();
        if ($user) {
            if($user->activation_code == $code) {
                $user->active = 1;
                $user->save();

            Auth::login($user);
            
           return Redirect::to('/user/profile');
            }
        }
        return View::make('frontend.error')
            ->with('meta',Page::whereName('empty')->first())
            ->with('error','Wrong activation code. Please check your activation email again!');
    }

    public function email_test($to,$from,$file) {

        $data                    = [];
        $data['full_name']       = 'Full Name';
        $data['email']           = $from;
        $data['activation_link'] = 'link';
        $data['activation_code'] = '123456';
        $data['campaign_title']  = "";

        // Send activation email to new user and to admin
        Mail::send('emails.' . $file, ['data' => $data, $to => $to ], function ($message) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Account Verification')
                    ->to('matt@chillidee.com.au');
        });
    }


    public function forgotPassword()
    {
        $email = Input::get('email');
        $user = User::whereEmail($email)->first();
        if ($user) {
            $user->resetPasswordEmail();

            return Redirect::back()
                ->with('loginFailed','failed')
                ->with('error','Your password has been reset. Check your email for the brand new one and don\'t forget to change it as soon as you log in!');
        } else
            return Redirect::back()
                ->with('loginFailed','failed')
                ->with('error','There is no user registered with the email '.$email.'. Please check your credentials.');

    }

    public function logout()
    {
        Auth::logout();
        Cookie::forget('laravel_session');

        return Redirect::to('/');
    }

    public function wishlist()
    {
        $user = Auth::user();

        $wishlist = $user->wishlist;

        return View::make('frontend.escorts.index')
            ->with('my_wishlist',1);
    }

    public function getChangePassword()
    {
        $user = Auth::user();
        $message = Session::get('message');

        return View::make('frontend.user.change-password',compact('user'))
            ->with('message',$message);
    }

    public function postChangePassword()
    {
        $data = Input::all();

        $rules = array('password' => 'required|confirmed');
        $v = Validator::make($data, $rules);

        if ($v->fails())
            return Redirect::route('getChangePassword')->withErrors($v);

        $user = User::find($data['user_id']);
        $user->password = Hash::make($data['password']);
        $user->save();

        return Redirect::route('getProfile')
            ->with('message','Password updated!');
    }
}
















