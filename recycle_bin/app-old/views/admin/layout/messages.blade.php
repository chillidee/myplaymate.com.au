@if(Session::has('message_success'))
    <div class="alert alert-success"></strong>Success: </strong>{{ Session::get('message_success') }}</div>
@endif

@if(Session::has('message_warning'))
    <div class="alert alert-warning"></strong>Warning: </strong>{{ Session::get('message_warning') }}</div>
@endif

@if(Session::has('message_error'))
    <div class="alert alert-error"></strong>Error: </strong>{{ Session::get('message_error') }}</div>
@endif

@if(Session::has('message_info'))
    <div class="alert alert-info"></strong>Info: </strong>{{ Session::get('message_info') }}</div>
@endif
