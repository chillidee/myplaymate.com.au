@extends('admin.layout')

@section('javascript')
<script>
    jQuery(document).ready(function() {
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"/admin/ajax_business_escorts",
                "data":{"business_id":"{{ $business->id }}"}
            },
            "columns": [
                {data: 'id', name: 'id', 'width':'45px'},
                {data: 'escort_name', name: 'escort_name'},
                {data: 'status', name: 'status'},
                {data: 'suburb_id', name: 'suburb_id'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'count_views', name: 'count_views'},
                {data: 'count_phone', name: 'count_phone'},
                {data: 'count_playmail', name: 'count_playmail'},
                {data: 'created_at', name:'escorts.created_at'},
                {data: 'bumpup_date', name:'bumpup_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
        jQuery('.rightpanel').on('click', '.approve', approveEscort);

        jQuery('.rightpanel').on('click', '.disapprove', disapproveEscort);

        jQuery('.rightpanel').on('click', '.delete', deleteEscort);

        jQuery('.rightpanel').on('click', '.bumpup', bumpup);

        $('.select2').select2({
            minimumInputLength: 4
        });
        $('form').parsley();
    });
</script>
@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-money"></span></div>
    <div class="pagetitle">
        <h1>Edit business info - {{ $business->name }}</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif

@include('admin.partials.business-breadcrumb')

{{ Form::open(['id'=>'businessInfo']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="control-group">
                {{ Form::label('type','Business type*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::select('type',[1=>'Brothel',2=>'Agency',3=>'Massage'],$business->type,array('class'=>'form-control')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('user_id','User*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::select('user_id',$users,$business->user_id,array('class'=>'select2 form-control','style'=>'width:100%','min'=>1)) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('phone','Phone*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('phone',$business->phone,array('class'=>'form-control','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('email','Email*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('email',$business->email,array('class'=>'form-control','required','data-parsley-type'=>'email')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('suburb_id','Business location*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::select('suburb_id',$suburbs,$business->suburb_id,array('class'=>'select2 form-control','style'=>'width:100%','min'=>1)) }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="control-group">
                {{ Form::label('name','Business Name*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('name',$business->name,array('class'=>'business-name form-control','required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('address_line_1','Address Line 1*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::text('address_line_1',$business->address_line_1,array('class'=>'form-control','required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('address_line_2','Address Line 2',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::text('address_line_2',$business->address_line_2,array('class'=>'form-control')) }}
                </div>
            </div>

        </div>
    </div>
    {{ Form::hidden('business_id',$business->id) }}

    <div class="alignc">
        {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
    </div>
{{ Form::close() }}

<div class="container-fluid my-escorts-table">
    <div class="row">
        <a href="/admin/escorts/new?business_id={{ $business->id }}" class="create-user btn btn-success btn-lg pull-right section-content">Add escort to business</a>
    </div>

    <table class="admin-table table table-bordered table-striped table-hover" id="datatable">
        <thead>
            <tr>
                <th>ID</th>
                <th>Escort name</th>
                <th>Status</th>
                <th>State</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Views</th>
                <th>Phone count</th>
                <th>Playmail count</th>
                <th>Date of creation</th>
                <th>Last bump up</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
@stop





