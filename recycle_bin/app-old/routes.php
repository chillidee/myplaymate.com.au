<?php
// App::bind('MyPlaymate\Repositories\EscortRepository',function($app){return new MyPlaymate\Repositories\EscortDbRepository;});


App::missing(function($exception) {
    return Response::view('frontend.404', array(), 404);
});

Route::get('/',['as'=>'home','uses'=>'HomeController@show']);
Route::get('wishlist',['as'=>'wishlist','uses'=>'UserController@wishlist']);

Route::get('escorts',['as'=>'escorts','uses'=>'EscortController@index']);
Route::get('city/{city}',['as'=>'city','uses'=>'EscortController@city']);
Route::get('escorts/preview/{id}',['as'=>'escortProfilePreview','uses'=>'EscortController@escortProfilePreview'])->where('id', '[0-9]+');
Route::get('escorts/{id}',['as'=>'escortProfileById','uses'=>'EscortController@redirectToShow'])->where('id', '[0-9]+');
Route::get('escorts/{slug}',['as'=>'escortProfile','uses'=>'EscortController@show']);

Route::get('brothels',['as'=>'brothels','uses'=>'BusinessController@brothels']);
Route::get('escort-agencies',['as'=>'agencies','uses'=>'BusinessController@agencies']);
Route::get('erotic-massage/massage-parlour',['as'=>'massage','uses'=>'BusinessController@massage']);

Route::get('blog',['as'=>'blogIndex','uses'=>'BlogController@index']);
Route::get('blog/{id}',['as'=>'blogSingle','uses'=>'BlogController@show']);

Route::get('escort-media',['as'=>'about','uses'=>'PageController@media']);

Route::get('about',['as'=>'about','uses'=>'PageController@about']);
Route::get('advertiser-terms-and-conditions',['as'=>'advertiserTerms','uses'=>'PageController@advertiserTerms']);
Route::get('faqs',['as'=>'faqs','uses'=>'PageController@faqs']);
Route::get('billing-and-refund-policy',['as'=>'billingPolicy','uses'=>'PageController@billingPolicy']);
Route::get('client-terms-and-conditions-of-use',['as'=>'clientTerms','uses'=>'PageController@clientTerms']);
Route::get('links',['as'=>'links','uses'=>'PageController@links']);
Route::get('member-terms-and-conditions-of-use',['as'=>'membersTerms','uses'=>'PageController@membersTerms']);
Route::get('privacy-policy',['as'=>'privacyPolicy','uses'=>'PageController@privacyPolicy']);

Route::get('escorts-sydney',['as'=>'escorts-sydney','uses'=>'PageController@escortsSydney']);
Route::get('escorts-melbourne',['as'=>'escorts-melbourne','uses'=>'PageController@escortsMelbourne']);
Route::get('escorts-brisbane',['as'=>'escorts-brisbane','uses'=>'PageController@escortsBrisbane']);
Route::get('escorts-perth',['as'=>'escorts-perth','uses'=>'PageController@escortsPerth']);
Route::get('escorts-adelaide',['as'=>'escorts-adelaide','uses'=>'PageController@escortsAdelaide']);

Route::get('mia-pagina',['as'=>'miapagina1','uses'=>'PageController@miaPagina']);
 

Route::get('contact',['as'=>'getContact','uses'=>'ContactController@getContact']);
Route::post('contact',['before'=>'csrf','as'=>'postContact','uses'=>'ContactController@postContact']);
Route::get('report',['as'=>'getReport','uses'=>'ContactController@getReport']);
Route::post('report',['before'=>'csrf','as'=>'postReport','uses'=>'ContactController@postReport']);

/*
|--------------------------------------------------------------------------
| Ajax routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'ajax'), function()
{
	Route::get('get_geo_ids','LocationController@GetGeoIds');
	Route::get('map_modal','LocationController@mapModal');
	Route::get('get_state_and_cities','LocationController@getStateAndCities');
	Route::get('get_suburb_coordinates','LocationController@getSuburbCoordinates');
	Route::get('get_postcode_info','LocationController@getPostcodeInfo');
	Route::get('get_suburb_info','LocationController@getSuburbInfo');

	Route::get('getUserWishlist/{id}','WishlistController@getUserWishlist');
	Route::get('addToWishlist','WishlistController@addToWishlist');
	Route::get('removeFromWishlist','WishlistController@removeFromWishlist');

	Route::get('setMainImage',['as'=>'setMainImage','uses'=>'ImageController@setMainImage']);
	Route::get('updateImageOrder',['as'=>'updateImageOrder','uses'=>'ImageController@updateOrder']);
	Route::get('deleteImage',['as'=>'deleteImage','uses'=>'ImageController@deleteImage']);

	Route::get('sendComment',['as'=>'sendComment','uses'=>'BlogController@sendComment']);

	Route::get('escorts/filters/apply',['as'=>'loadEscorts','uses'=>'EscortController@loadEscorts']);
	Route::get('escorts/increaseViewCounter/{id}',['before'=>'blockLocalConnection','as'=>'increaseViewCounter','uses'=>'EscortController@increaseViewCounter']);
	Route::post('escorts/{id}/reveal-phone',['before'=>'blockLocalConnection','as'=>'postPhoneReveal','uses'=>'EscortController@postPhoneReveal']);
	Route::get('escort/sendPlaymail',['as'=>'sendPlaymail','uses'=>'EscortController@sendPlaymail']);
	Route::get('escort/leaveReview',['as'=>'leaveReview','uses'=>'EscortController@leaveReview']);

	Route::get('addTouring',['as'=>'addTouring','uses'=>'ProfileController@addTouring']);
	Route::get('deleteTouring',['as'=>'deleteTouring','uses'=>'ProfileController@deleteTouring']);
	Route::get('save-update',['as'=>'saveUpdate','uses'=>'ProfileController@saveUpdate']);
	Route::get('delete-update',['as'=>'deleteUpdate','uses'=>'ProfileController@deleteUpdate']);
	Route::get('publishReview',['as'=>'publishReview','uses'=>'ProfileController@publishReview']);
	Route::get('unpublishReview',['as'=>'unpublishReview','uses'=>'ProfileController@unpublishReview']);
	Route::get('deleteReview',['as'=>'deleteReview','uses'=>'ProfileController@deleteReview']);
});

/*
|--------------------------------------------------------------------------
| Profile routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'auth'), function()
{
	Route::get('user/profile',['as'=>'getProfile','uses'=>'ProfileController@getProfile']);
	Route::get('emailExists',['as'=>'emailExists','uses'=>'UserController@emailExists']);
	Route::get('profile/userInfo',['as'=>'getUserInfo','uses'=>'ProfileController@getUserInfo']);
	Route::post('profile/userInfo',['before'=>'csrf','as'=>'postUserInfo','uses'=>'ProfileController@postUserInfo']);

	Route::get('profile/escortInfo',['as'=>'getEscortInfo','uses'=>'ProfileController@getEscortInfo']);
	Route::post('profile/escortInfo',['before'=>'csrf','as'=>'postEscortInfo','uses'=>'ProfileController@postEscortInfo']);
	Route::get('profile/escortAbout',['as'=>'getEscortAbout','uses'=>'ProfileController@getEscortAbout']);
	Route::post('profile/escortAbout',['before'=>'csrf','as'=>'postEscortAbout','uses'=>'ProfileController@postEscortAbout']);
	Route::get('profile/escortAvailability',['as'=>'getEscortAvailability','uses'=>'ProfileController@getEscortAvailability']);
	Route::post('profile/escortAvailability',['before'=>'csrf','as'=>'postEscortAvailability','uses'=>'ProfileController@postEscortAvailability']);
	Route::get('profile/escortGallery',['as'=>'getEscortGallery','uses'=>'ProfileController@getEscortGallery']);
	Route::post('profile/escortGallery',['before'=>'csrf','as'=>'postEscortGallery','uses'=>'ProfileController@postEscortGallery']);
	Route::get('profile/escortSite',['as'=>'getEscortSite','uses'=>'ProfileController@getEscortSite']);
	Route::post('profile/escortSite',['before'=>'csrf','as'=>'postEscortSite','uses'=>'ProfileController@postEscortSite']);
	Route::get('profile/escortReviews',['as'=>'getEscortReviews','uses'=>'ProfileController@getEscortReviews']);
	Route::post('profile/escortReviews',['before'=>'csrf','as'=>'postEscortReviews','uses'=>'ProfileController@postEscortReviews']);
	Route::get('profile/businessInfo',['as'=>'getBusinessInfo','uses'=>'ProfileController@getBusinessInfo']);
	Route::post('profile/businessInfo',['before'=>'csrf','as'=>'postBusinessInfo','uses'=>'ProfileController@postBusinessInfo']);
	Route::get('profile/businessLogo',['as'=>'getBusinessLogo','uses'=>'ProfileController@getBusinessLogo']);
	Route::post('profile/businessLogo',['before'=>'csrf','as'=>'postBusinessLogo','uses'=>'ProfileController@postBusinessLogo']);
	Route::get('profile/businessMyEscorts',['as'=>'getBusinessMyEscorts','uses'=>'ProfileController@getBusinessMyEscorts']);
	Route::post('profile/businessMyEscorts',['before'=>'csrf','as'=>'postBusinessMyEscorts','uses'=>'ProfileController@postBusinessMyEscorts']);
	Route::get('profile/newEscort',['as'=>'getNewEscort','uses'=>'ProfileController@getNewEscort']);
	Route::post('profile/newEscort',['before'=>'csrf','as'=>'postNewEscort','uses'=>'ProfileController@postNewEscort']);
	Route::get('profile/newBusiness',['as'=>'getNewBusiness','uses'=>'ProfileController@getNewBusiness']);
	Route::post('profile/newBusiness',['before'=>'csrf','as'=>'postNewBusiness','uses'=>'ProfileController@postNewBusiness']);
});


/*
|--------------------------------------------------------------------------
| Composers
|--------------------------------------------------------------------------
*/

View::composer('frontend.partials.footer.posts', 'FooterPostsComposer');
View::composer('frontend.partials.sidebar.left-common-widgets','SidebarWidgetsComposer');


/*
|--------------------------------------------------------------------------
| Sitemap
|--------------------------------------------------------------------------
*/

Route::get('mysitemap', function(){
	$sitemap = new MyPlaymate\SitemapGenerator;

	$sitemap->generate();
});


/*
|--------------------------------------------------------------------------
| User routes
|--------------------------------------------------------------------------
*/

Route::get('register',['as'=>'getRegister','uses'=>'UserController@getRegister']);
Route::post('register',['as'=>'postRegister','uses'=>'UserController@postRegister']);

Route::get('users/activate',['as'=>'userActivation','uses'=>'UserController@activateUser']);
Route::post('users/forgot-password',['before'=>'csrf','as'=>'forgotPassword','uses'=>'UserController@forgotPassword']);

Route::get('users/change-password',['as'=>'getChangePassword','uses'=>'UserController@getChangePassword']);
Route::post('users/change-password',['before'=>'csrf','as'=>'postChangePassword','uses'=>'UserController@postChangePassword']);

Route::post('login',['before'=>'csrf','as'=>'login','uses'=>'UserController@login']);
Route::get('logout',['as'=>'logout','uses'=>'UserController@logout']);


/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/

Route::get('admin/login',['as'=>'adminGetLogin','uses'=>'Admin\UserController@getLogin']);
Route::post('admin/login',['as'=>'adminPostLogin','uses'=>'Admin\UserController@postLogin']);
Route::group(array('before' => 'auth.admin'), function()
{
	Route::get('admin',['as'=>'adminDashboard','uses'=>'Admin\DashboardController@index']);
	Route::get('admin/logout',['as'=>'adminLogout','uses'=>'Admin\UserController@logout']);
	Route::get('admin/users',['as'=>'adminUsers','uses'=>'Admin\UserController@usersIndex']);
	Route::get('admin/ajax_users',['as'=>'ajaxAdminUsers','uses'=>'Admin\UserController@usersAjax']);

	Route::get('admin/users/administrators',['uses'=>'Admin\UserController@administratorsIndex']);
	Route::get('admin/ajax_administrators',['uses'=>'Admin\UserController@administratorsAjax']);
	Route::get('admin/users/withEscorts',['uses'=>'Admin\UserController@withEscortsIndex']);
	Route::get('admin/ajax_withEscorts',['uses'=>'Admin\UserController@withEscortsAjax']);
	Route::get('admin/users/withBusinesses',['uses'=>'Admin\UserController@withBusinessesIndex']);
	Route::get('admin/ajax_withBusinesses',['uses'=>'Admin\UserController@withBusinessesAjax']);

	Route::get('admin/users/new',['as'=>'adminGetUserNew','uses'=>'Admin\UserController@getUserNew']);
	Route::post('admin/users/new',['before'=>'csrf','as'=>'adminPostUserNew','uses'=>'Admin\UserController@postUserNew']);
	Route::get('admin/users/delete',['as'=>'adminDeleteUser','uses'=>'Admin\UserController@deleteUser']);
	Route::get('admin/users/{id}',['as'=>'adminGetUserEdit','uses'=>'Admin\UserController@getUserEdit']);
	Route::post('admin/users/{id}',['before'=>'csrf','as'=>'adminPostUserEdit','uses'=>'Admin\UserController@postUserEdit']);

	Route::get('admin/pending_escorts',['as'=>'adminPendingEscorts','uses'=>'Admin\EscortController@pendingEscortsIndex']);
	Route::get('admin/ajax_pending_escorts',['as'=>'ajaxAdminPendingEscorts','uses'=>'Admin\EscortController@pendingEscortsAjax']);
	Route::get('admin/approve_escort',['as'=>'approveEscort','uses'=>'Admin\EscortController@approveEscort']);
	Route::get('admin/disapprove_escort',['as'=>'disapproveEscort','uses'=>'Admin\EscortController@disapproveEscort']);
	Route::get('admin/delete_escort',['as'=>'deleteEscort','uses'=>'Admin\EscortController@deleteEscort']);
	Route::get('admin/toggle_featured',['as'=>'toggleFeatured','uses'=>'Admin\EscortController@toggleFeatured']);
	Route::get('admin/bump-up',['as'=>'bumpUpEscort','uses'=>'Admin\EscortController@bumpUp']);

	Route::get('admin/escorts',['as'=>'adminEscorts','uses'=>'Admin\EscortController@escortIndex']);
	Route::get('admin/escorts/new',['as'=>'adminGetEscortsNew','uses'=>'Admin\EscortController@getEscortNew']);
	Route::post('admin/escorts/new',['before'=>'csrf','as'=>'adminPostEscortsNew','uses'=>'Admin\EscortController@postEscortNew']);
	Route::get('admin/escorts/info/{id}',['as'=>'adminEscortGetEditInfo','uses'=>'Admin\EscortController@escortGetEditInfo']);
	Route::get('admin/escorts/about/{id}',['as'=>'adminEscortGetEditAbout','uses'=>'Admin\EscortController@escortGetEditAbout']);
	Route::get('admin/escorts/availability/{id}',['as'=>'adminEscortGetEditAvailability','uses'=>'Admin\EscortController@escortGetEditAvailability']);
	Route::get('admin/escorts/gallery/{id}',['as'=>'adminEscortGetEditGallery','uses'=>'Admin\EscortController@escortGetEditGallery']);
	Route::post('admin/escorts/gallery/upload/{id}',['before'=>'csrf','as'=>'adminUploadEscortGallery','uses'=>'Admin\EscortController@uploadEscortGallery']);
	Route::get('admin/escorts/site/{id}',['as'=>'adminEscortGetEditSite','uses'=>'Admin\EscortController@escortGetEditSite']);
	Route::get('admin/escorts/reviews/{id}',['as'=>'adminEscortGetEditReviews','uses'=>'Admin\EscortController@escortGetEditReviews']);
	Route::get('admin/escorts/seo/{id}',['as'=>'adminEscortGetEditSeo','uses'=>'Admin\EscortController@escortGetEditSeo']);
	Route::post('admin/escorts/info/{id}',['before'=>'csrf','as'=>'adminEscortPostEditInfo','uses'=>'Admin\EscortController@escortPostEditInfo']);
	Route::post('admin/escorts/about/{id}',['before'=>'csrf','as'=>'adminEscortPostEditAbout','uses'=>'Admin\EscortController@escortPostEditAbout']);
	Route::post('admin/escorts/availability/{id}',['before'=>'csrf','as'=>'adminEscortPostEditAvailability','uses'=>'Admin\EscortController@escortPostEditAvailability']);
	Route::post('admin/escorts/gallery/{id}',['before'=>'csrf','as'=>'adminEscortPostEditGallery','uses'=>'Admin\EscortController@escortPostEditGallery']);
	Route::post('admin/escorts/site/{id}',['before'=>'csrf','as'=>'adminEscortPostEditSite','uses'=>'Admin\EscortController@escortPostEditSite']);
	Route::post('admin/escorts/reviews/{id}',['before'=>'csrf','as'=>'adminEscortPostEditReviews','uses'=>'Admin\EscortController@escortPostEditReviews']);
	Route::post('admin/escorts/seo/{id}',['before'=>'csrf','as'=>'adminEscortPostEditSeo','uses'=>'Admin\EscortController@escortPostEditSeo']);
	Route::get('admin/ajax_escorts',['as'=>'ajaxAdminEscorts','uses'=>'Admin\EscortController@escortAjax']);
	
	Route::get('admin/pending_images',['as'=>'adminPendingImage','uses'=>'Admin\ImageController@pendingImagesIndex']);
	Route::get('admin/show-fullsize-image',['as'=>'showFullSizeImage','uses'=>'Admin\ImageController@showFullSizeImage']);
	Route::get('admin/approve_image',['as'=>'approveImage','uses'=>'Admin\ImageController@approveImage']);
	Route::get('admin/disapprove_image',['as'=>'disapproveImage','uses'=>'Admin\ImageController@disapproveImage']);
	Route::get('/admin/send-images-report',['as'=>'sendImagesReport','uses'=>'Admin\ImageController@sendImagesReport']);

	Route::post('admin/businesses/logo/upload/{id}',['before'=>'csrf','as'=>'adminUploadBusinessLogo','uses'=>'Admin\BusinessController@uploadLogo']);
	Route::get('admin/businesses',['as'=>'adminBusinesses','uses'=>'Admin\BusinessController@businessIndex']);
	Route::get('admin/businesses/new',['as'=>'adminGetNewBusiness','uses'=>'Admin\BusinessController@getNewBusiness']);
	Route::post('admin/businesses/new',['before'=>'csrf','as'=>'adminPostNewBusiness','uses'=>'Admin\BusinessController@postNewBusiness']);
	Route::get('admin/ajax_businesses',['as'=>'ajaxAdminBusinesses','uses'=>'Admin\BusinessController@businessAjax']);
	Route::get('admin/businesses/edit/{id}',['as'=>'adminGetEditBusiness','uses'=>'Admin\BusinessController@getEdit']);
	Route::get('admin/businesses/logo/{id}',['as'=>'adminGetBusinessLogo','uses'=>'Admin\BusinessController@getLogo']);
	Route::post('admin/businesses/edit/{id}',['before'=>'csrf','as'=>'adminPostEditBusiness','uses'=>'Admin\BusinessController@postEdit']);
	Route::get('admin/ajax_business_escorts',['as'=>'ajaxAdminBusinessEscorts','uses'=>'Admin\BusinessController@businessEscortsAjax']);

	Route::get('admin/pages',['as'=>'adminPages','uses'=>'Admin\PageController@index']);
	Route::get('admin/ajax_pages',['as'=>'adminAjaxPages','uses'=>'Admin\PageController@ajax']);
	Route::get('admin/pages/new',['as'=>'adminGetNewPage','uses'=>'Admin\PageController@getNew']);
	Route::post('admin/pages/new',['as'=>'adminPostNewPage','uses'=>'Admin\PageController@postNew']);
	Route::get('admin/pages/edit/{id}',['as'=>'adminGetEditPage','uses'=>'Admin\PageController@getEdit']);
	Route::post('admin/pages/edit/{id}',['as'=>'adminPostEditPage','uses'=>'Admin\PageController@postEdit']);

	Route::get('admin/blog',['as'=>'adminBlog','uses'=>'Admin\BlogController@index']);
	Route::get('admin/ajax_blog',['as'=>'adminAjaxBlog','uses'=>'Admin\BlogController@ajax']);
	Route::get('admin/blog/new',['as'=>'adminGetNewBlog','uses'=>'Admin\BlogController@getNew']);
	Route::post('admin/blog/new',['as'=>'adminPostNewBlog','uses'=>'Admin\BlogController@postNew']);
	Route::get('admin/blog/edit/{id}',['as'=>'adminGetEditBlog','uses'=>'Admin\BlogController@getEdit']);
	Route::post('admin/blog/edit/{id}',['as'=>'adminPostEditBlog','uses'=>'Admin\BlogController@postEdit']);
	Route::get('admin/blog/upload',['as'=>'adminBlogUpload','uses'=>'Admin\BlogController@upload']);
	Route::post('admin/blog/upload',['as'=>'adminBlogUpload','uses'=>'Admin\BlogController@upload']);

	Route::get('admin/comments',['as'=>'adminComments','uses'=>'Admin\CommentController@index']);
	Route::get('admin/ajax_comments',['as'=>'adminAjaxComments','uses'=>'Admin\CommentController@ajax']);
	Route::get('admin/approve_comment',['as'=>'approveComment','uses'=>'Admin\CommentController@approve']);
	Route::get('admin/disapprove_comment',['as'=>'disapproveComment','uses'=>'Admin\CommentController@disapprove']);

	Route::get('admin/updates',['as'=>'adminUpdates','uses'=>'Admin\UpdateController@index']);
	Route::get('admin/ajax_updates',['as'=>'adminAjaxUpdates','uses'=>'Admin\UpdateController@ajax']);
	Route::get('admin/approve_update',['as'=>'approveUpdate','uses'=>'Admin\UpdateController@approve']);
	Route::get('admin/disapprove_update',['as'=>'disapproveUpdate','uses'=>'Admin\UpdateController@disapprove']);

	Route::get('admin/reviews',['as'=>'adminReviews','uses'=>'Admin\ReviewController@index']);
	Route::get('admin/ajax_reviews',['as'=>'adminAjaxReviews','uses'=>'Admin\ReviewController@ajax']);
	Route::get('admin/approve_review',['as'=>'approveReview','uses'=>'Admin\ReviewController@approve']);
	Route::get('admin/disapprove_review',['as'=>'disapproveReview','uses'=>'Admin\ReviewController@disapprove']);
});


























/*
|--------------------------------------------------------------------------
| Testing stuff
|--------------------------------------------------------------------------
|
*/

Route::get('watermark/{path}',function($path){
	ImageHelper::watermark($path);
});


Route::get('test',function(){
	return View::make('frontend.test');
});

// Route::get('test/ajax','EscortController@loadEscorts');


/*
|--------------------------------------------------------------------------
| Utilities
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'development'), function()
{
	Route::get('clean_orphan_pics',function(){
		return scandir('/uploads/escorts/');

		$pics = EscortImage::all();

		foreach ($pics as $pic) {
			if (!Escort::find($pic->escort_id)) {
				echo $pic->filename.' - '.$pic->escort_id.'<br>';
			}
		}

		$count = 0;
		foreach ($pics as $key => $pic) {
			if (!Escort::find($pic->escort_id)) {
				if (file_exists('/uploads/escorts/'.$pic->filename))
					unlink('/uploads/escorts/'.$pic->filename);
				if (file_exists('/uploads/escorts/thumbnails/thumb_86x129_'.$pic->filename))
					unlink('/uploads/escorts/thumbnails/thumb_86x129_'.$pic->filename);
				if (file_exists('/uploads/escorts/thumbnails/thumb_170x170_'.$pic->filename))
					unlink('/uploads/escorts/thumbnails/thumb_170x170_'.$pic->filename);
				if (file_exists('/uploads/escorts/thumbnails/thumb_220x330_'.$pic->filename))
					unlink('/uploads/escorts/thumbnails/thumb_220x330_'.$pic->filename);

				// unlink('/uploads/escorts/'.$pic->filename);
				// unlink('/uploads/escorts/thumbnails/thumb_86x129_'.$pic->filename);
				// unlink('/uploads/escorts/thumbnails/thumb_170x170_'.$pic->filename);
				// unlink('/uploads/escorts/thumbnails/thumb_220x330_'.$pic->filename);
				$pic->delete();

				$count++;
			}
		}
		return $count.' pictures and thumbs deleted';
	});

	Route::get('clean_orphan_reviews',function(){
		$reviews = EscortReview::all();
		foreach ($reviews as $review) {
			if(!Escort::find($review->escort_id))
				$review->delete();
		}
	});

	Route::get('migrate_escort_business',function(){
		$escortBusinesses = EscortBusiness::all();
		foreach ($escortBusinesses as $row)
		{
			$escort = Escort::find($row->escort_id);
			if ($escort)
			{
				$escort->business_id = $row->business_id;
				$escort->save();
			}
		
		}
	});

	Route::get('resize_images_for_slider',function(){
		$images = EscortImage::all();
		foreach ($images as $image)
		{
			$file = public_path().'/uploads_live/escorts/'.$image->filename;
			if (file_exists($file)) {
				ImageHelper::createSliderImage(public_path().'/uploads_live/escorts/',$image->filename);
				unlink($file);
			}
		}
		return 'finished';
	});
	Route::get('show_non_converted',function(){
		$images = EscortImage::all();
		foreach ($images as $image)
		{
			$file = public_path().'/uploads_live/escorts/slider/'.$image->filename;
			if (!file_exists($file)) {
				echo $file.'<br>';
			}
		}
		return 'finished';
	});

	Route::get('merge_escorts',function(){

		$profiles = Profile::all();

		Escort::truncate();
		EscortBusiness::truncate();

		foreach ($profiles as $profile) {
			$escort = new Escort;
			$escort->id = $profile->id;
			$escort->user_id = $profile->user_id;
			$escort->active = 1;
			if ($profile->approved == 1)
				$escort->status = 2;
			else
				$escort->status = 1;
			$escort->own_url = $profile->own_url;
			$escort->use_own_url = $profile->use_own_url;
			$escort->escort_name = $profile->escort_name;
			$escort->phone = $profile->phone;
			$escort->email = $profile->email;
			$escort->licence_number = $profile->licence_number;
			$escort->main_image_id = $profile->main_image_id;
			$escort->contact_playmail = $profile->contact_playmail;
			$escort->contact_phone = $profile->contact_phone;
			$escort->does_incalls = $profile->does_incalls || '';
			$escort->does_outcalls = $profile->does_outcalls || '';
			$escort->does_travel_internationally = $profile->does_travel_internationally || '';
			$escort->tattoos = $profile->has_tattoos || '';
			$escort->piercings = $profile->has_piercings || '';
			$escort->about_me = $profile->about_me;
			$escort->suburb_id = $profile->t_suburb_id;
			$escort->age_id = $profile->t_age_id;
			$escort->gender_id = $profile->t_gender_id;
			$escort->body_id = $profile->t_body_id;
			$escort->height_id = $profile->t_height_id;
			$escort->hair_color_id = $profile->t_hair_colour_id;
			$escort->eye_color_id = $profile->t_eye_colour_id;
			$escort->hourly_rate_id = $profile->t_hourly_rate_id;
			$escort->ethnicity_id = $profile->t_ethnicity_id;
			$escort->current_suburb_id = $profile->t_current_suburb_id || '';
			$escort->count_phone = $profile->count_phone;
			$escort->count_playmail = $profile->count_playmail;
			$escort->approved_at = $profile->approved_date;
			$escort->active_from = $profile->active_from;
			$escort->active_to = $profile->active_to;
			$escort->seo_title = $profile->seo_title;
			$escort->seo_description = $profile->seo_description;
			$escort->seo_keywords = $profile->seo_keywords;
			$escort->created_at = $profile->created_at;
			$escort->updated_at = $profile->updated_at;
			$escort->count_views = $profile->count_views;
			$escort->count_pictures = $profile->count_pictures;
			$escort->notes = $profile->rates;
			$escort->seo_content = $profile->seo_content;
			$escort->featured = $profile->featured;
			$escort->banner = $profile->banner;
			$escort->use_banner = $profile->use_banner;
			$escort->bump_date = $profile->bump_date;
			$escort->rank = $profile->rank || '';
			$escort->about_review = $profile->about_review;
			$escort->old_id = $profile->old_id;
			$escort->reported = $profile->reported;
			$escort->reported_date = $profile->reported_date;
			if ($profile->agency_id != null && $profile->agency_id != 0){
				$escort->under_business = 1;
				$escbusiness = new EscortBusiness;
				$escbusiness->escort_id = $escort->id;
				$escbusiness->business_id = $profile->agency_id;
				$escbusiness->save();
			}
			$escort->seo_url = $escort->getSeoUrl();
			$escort->save();
		}
	});

	Route::get('merge_escort_stuff',function(){
		// $services = DB::table('profile_t_service')->get();
		// EscortService::truncate();
		// foreach ($services as $service) {
		// 	$new = new EscortService;
		// 	$new->escort_id = $service->profile_id;
		// 	$new->service_id = $service->t_service_id;
		// 	$new->save();
		// }

		// $languages = DB::table('profile_t_language')->get();
		// EscortLanguage::truncate();
		// foreach ($languages as $language) {
		// 	$new = new EscortLanguage;
		// 	$new->escort_id = $language->profile_id;
		// 	$new->language_id = $language->t_language_id;
		// 	$new->save();
		// }

		// $updates = DB::table('profile_statuses')->get();
		// EscortUpdate::truncate();
		// foreach ($updates as $update) {
		// 	$new = new EscortUpdate;
		// 	$new->id = $update->id;
		// 	$new->escort_id = $update->profile_id;
		// 	$new->message = $update->message;
		// 	$new->status = 2;
		// 	$new->save();
		// }

		// $reviews = DB::table('profile_reviews')->get();
		// EscortReview::truncate();
		// foreach ($reviews as $review) {
		// 	$new = new EscortReview;
		// 	$new->id = $review->id;
		// 	$new->escort_id = $review->profile_id;
		// 	$new->name = $review->name;
		// 	$new->review = $review->review;
		// 	$new->rating = $review->rating;
		// 	$new->status = $review->approved == 1 ? 2 : 1;
		// 	$new->published = $review->approved == 1 ? 1 : 0;
		// 	$new->save();
		// }

		// $images = DB::table('profile_images')->get();
		// EscortImage::truncate();
		// foreach ($images as $image) {
		// 	$new = new EscortImage;
		// 	$new->id = $image->id;
		// 	$new->escort_id = $image->profile_id;
		// 	$new->filename = $image->filename;
		// 	$new->order = $image->order;
		// 	$new->created_at = $image->created_at;
		// 	$new->updated_at = $image->updated_at;
		// 	$new->status = $image->approved == 1 ? 2 : 1;
		// 	if ($new->status == 1)
		// 		$new->status = $image->disapproved == 1 ? 3 : 1;
		// 	$new->disapprove_message = $image->disapprove_message;
		// 	$new->save();
		// }

		$availabilities = DB::table('old_profile_availabilities')->get();
		EscortAvailability::truncate();
		foreach ($availabilities as $availability) {
			$new = new EscortAvailability;
			$new->escort_id = $availability->profile_id;
			if ($availability->day == 'MONDAY')
				$new->day = 1;
			if ($availability->day == 'TUESDAY')
				$new->day = 2;
			if ($availability->day == 'WEDNESDAY')
				$new->day = 3;
			if ($availability->day == 'THURSDAY')
				$new->day = 4;
			if ($availability->day == 'FRIDAY')
				$new->day = 5;
			if ($availability->day == 'SATURDAY')
				$new->day = 6;
			if ($availability->day == 'SUNDAY')
				$new->day = 7;

			// $new->day = ($availability->order + 1);
			$new->enabled = $availability->enabled;
			$new->twenty_four_hours = $availability->twenty_four_hours;

			$hours = [0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12];

			foreach ($hours as $key => $value) {
				if ($availability->start_hour == $value)
					$new->start_hour_id = $key;
			}
			foreach ($hours as $key => $value) {
				if ($availability->end_hour == $value)
					$new->end_hour_id = $key;
			}
			$new->start_am_pm = $availability->start_am_pm;
			$new->end_am_pm = $availability->end_am_pm;

			$new->save();
		}
	});

	Route::get('merge_users',function(){
		$users = DB::table('t_users')->get();
		User::truncate();
		foreach ($users as $user) {
			$new = new User;
			$new->id = $user->id;
			$new->type = ($user->type == 'customer') ? 1 : 2;
			$new->email = $user->email;
			$new->full_name = $user->fullname;
			$new->password = $user->password;
			$new->phone = $user->phone;
			$new->active = $user->active;
			$new->activation_code = $user->activation_code;
			$new->super_user = ($user->type == 'administrator') ? 1 : 0;
			$new->remember_token = $user->remember_token;
			$new->created_at = $user->created_at;
			$new->updated_at = $user->updated_at;

			$new->save();
		}

		$johnny = new User;
		$johnny->type = 2;
		$johnny->full_name = 'Johnny';
		$johnny->email = 'johnny@chillidee.com.au';
		$johnny->password = Hash::make('JohnnyBeGood107');
		$johnny->super_user = 1;
		$johnny->active = 1;
		$johnny->save();

		$che = new User;
		$che->type = 2;
		$che->full_name = 'Che';
		$che->email = 'che@chillidee.com.au';
		$che->password = Hash::make('JohnnyBeGood107');
		$che->super_user = 1;
		$che->active = 1;
		$che->save();

		$Tami = new User;
		$Tami->type = 2;
		$Tami->full_name = 'Tami';
		$Tami->email = 'Tami@chillidee.com.au';
		$Tami->password = Hash::make('JohnnyBeGood107');
		$Tami->super_user = 1;
		$Tami->active = 1;
		$Tami->save();

		$candice = new User;
		$candice->type = 2;
		$candice->full_name = 'candice';
		$candice->email = 'candice@chillidee.com.au';
		$candice->password = Hash::make('JohnnyBeGood107');
		$candice->super_user = 1;
		$candice->active = 1;
		$candice->save();
	});

	Route::get('merge_posts',function(){
		$posts = DB::table('t_posts')->get();
		Post::truncate();
		foreach ($posts as $post) {
			$new = new Post;
			$new->id = $post->id;
			$new->title = $post->title;
			$new->thumbnail = $post->thumbnail;
			$new->short_content	 = $post->short_content;
			$new->content = $post->content;
			$new->seo_url = $post->url_key;
			$new->meta_title = $post->meta_title;
			$new->meta_description = $post->meta_description;
			$new->meta_keywords = $post->meta_keywords;
			$new->published = $post->published;
			$new->user_id = $post->user_id;
			$new->date_from = $post->date_from;
			$new->date_to = $post->date_to;
			$new->created_at = $post->created_at;
			$new->updated_at = $post->updated_at;

			$new->save();
		}

		$comments = DB::table('t_comments')->get();
		Comment::truncate();
		foreach ($comments as $comment) {
			$new = new Comment;
			$new->id = $comment->id;
			$new->post_id = $comment->post_id;
			$new->comment	 = $comment->comment;
			$new->author = $comment->name;
			$new->status = $comment->approved == 1 ? 2 : 1;
			$new->ip = $comment->ip || '';
			$new->created_at = $comment->created_at;
			$new->updated_at = $comment->updated_at;

			$new->save();
		}
	});

	Route::get('merge_business',function(){
		$businesses = DB::table('agencies')->get();
		Business::truncate();
		foreach ($businesses as $business) {
			$new = new Business;
			$new->id = $business->id;
			$new->user_id = $business->user_id;
			$new->name = $business->name;
			$new->phone	 = $business->phone;
			$new->email = $business->email;
			$new->suburb_id = $business->t_suburb_id;
			$new->image = $business->image;
			$new->status = $business->approved == 1 ? 2 : 1;
			$new->created_at = $business->created_at;
			$new->updated_at = $business->updated_at;

			$new->save();
		}
	});

	Route::get('merge_rates',function(){
		$rates = DB::table('profile_rates')->get();
		EscortRate::truncate();
		foreach ($rates as $rate) {
			$row = EscortRate::whereEscortId($rate->profile_id)->first();
			if(empty($row)){
				$newRate = new EscortRate;
				$newRate->id = $rate->id;
				$newRate->escort_id = $rate->profile_id;
				$newRate->save();
			}
		}

		foreach ($rates as $rate) {
			if($rate->incall == 1 && $rate->service == '15 Min'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->incall_15_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->incall == 1 && $rate->service == '30 Min'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				if ($newRate) {
					$newRate->incall_30_rate = $rate->rate;
					$newRate->save();
				}
		
			}
			if($rate->incall == 1 && $rate->service == '45 Min'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->incall_45_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->incall == 1 && $rate->service == '1 Hour'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				if ($newRate) {
					$newRate->incall_1h_rate = $rate->rate;
					$newRate->save();
				}
			}
			if($rate->incall == 1 && $rate->service == '2 Hours'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->incall_2h_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->incall == 1 && $rate->service == '3 Hours'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->incall_3h_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->incall == 1 && $rate->service == 'overnight'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->incall_overnight_rate = $rate->rate;
				$newRate->save();
			}

			if($rate->outcall == 1 && $rate->service == '15 Min'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->outcall_15_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->outcall == 1 && $rate->service == '30 Min'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->outcall_30_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->outcall == 1 && $rate->service == '45 Min'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->outcall_45_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->outcall == 1 && $rate->service == '1 Hour'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				if ($newRate) {
					$newRate->incall_1h_rate = $rate->rate;
					$newRate->save();
				}
			}
			if($rate->outcall == 1 && $rate->service == '2 Hours'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->outcall_2h_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->outcall == 1 && $rate->service == '3 Hours'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->outcall_3h_rate = $rate->rate;
				$newRate->save();
			}
			if($rate->outcall == 1 && $rate->service == 'overnight'){
				$newRate = EscortRate::whereEscortId($rate->profile_id)->first();
				$newRate->outcall_overnight_rate = $rate->rate;
				$newRate->save();
			}
		}
	});
});




