@extends('frontend.layout')

@section('content')

<!-- #page-title start -->
<section id="page-title" class="page-title-1">
    <div class="container">
        <h1>CHANGE PASSWORD</h1>
        <div class="breadcrumb-container">
            <span>You are here: </span>
            <ul class="breadcrumb">
                <li><a href="{{ url('') }}">HOME</a></li>
                <li><a href="#">CHANGE PASSWORD</a></li>
            </ul><!-- .breadcrumb end -->
        </div><!-- .breadcrumb-container end -->
    </div><!-- .containr end -->
</section><!-- #page-title end -->

<div class="back-end">
	<div class="page-wrapper top-space">
		<div class="container">
			<div class="row">

				@include('frontend.partials.profile.profile-left-sidebar')

				<div class="col-md-9" id="backend-main">
	                <!-- Process Bar -->
	                <div id="process-bar" class="alignc">
	                    <h2>Change password</h2>
	                    <p>Increase your security and privacy!</p>
	                </div><!-- End of Process Bar -->
					
                    	@if(isset($message))
		                    <div class="alert alert-success">
                    			{{ $message }}
                    		</div>
                    	@endif

					<div id="basic-info" class="">
					    {{ Form::open(array('id' => 'basic-info-form')) }}
					        <fieldset>
					            <div class="control-group">
					                {{ Form::label('password','Password',array('class'=>'control-label')) }}<span>*</span>
					                <div class="controls">
					                    {{ Form::password('password', array('class' => 'input-xlarge input03')) }}

					                    @if($errors->has('password'))
					                    <p class="form-error">{{ $errors->first('password') }}</p>
					                    @endif
					                </div>
					            </div>

					            <div class="control-group">
					                {{ Form::label('password_confirmation','Confirm password',array('class'=>'control-label')) }}<span>*</span>
					                <div class="controls">
					                    {{ Form::password('password_confirmation', array('class' => 'input-xlarge input03')) }}

					                    @if($errors->has('password_confirmation'))
					                    <p class="form-error">{{ $errors->first('password_confirmation') }}</p>
					                    @endif
					                </div>
					            </div>
					            {{ Form::hidden('user_id',$user->id) }}
					        </fieldset>
					        <div style="clear:both; width:100%; text-align:right; padding:20px; padding-right: 0;">
					            {{ Form::submit('UPDATE') }}
					        </div>
					    {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
	
	

@stop