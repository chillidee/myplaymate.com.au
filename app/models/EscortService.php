<?php

/**
 * Class EscortImage
 */
class EscortService extends Eloquent
{
    protected $table = 'escort_service';

    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [];

    /**
     *
     */
    public function escort()
    {
        $this->belongsTo('Escort');
    }

    public function getList($id)
    {
        $services = Service::lists('name','id');
        $escortServices = EscortService::where('escort_id','=',$this->escort_id )->get();
        if ( !empty( $escortServices ) ) { 
           foreach( $escortServices as $escortService ) {
             $services[ $escortService->service_id] = array( 'checked', $services[ $escortService->service_id] );
           }
       }
        return $services;
    }
}
