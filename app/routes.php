<?php
// App::bind('MyPlaymate\Repositories\EscortRepository',function($app){return new MyPlaymate\Repositories\EscortDbRepository;});

  //Catchall so our new pages work.
  // opcache_reset();
App::missing(function($exception) {
    return Response::view('frontend.404', array(), 404);
});

  Route::get('apitest',['as'=>'apitest','uses'=>'ServiceController@apiTest']);


  // Route::post('testing',['before'=>'csrf','as'=>'postEscortInfo','uses'=>'ProfileController@postEscortInfo']);
Route::get('/',['as'=>'home','uses'=>'HomeController@show_home']);
Route::get('wishlist',['as'=>'wishlist','uses'=>'UserController@wishlist']);

Route::get('escorts/{id}',['as'=>'escortProfileById','uses'=>'EscortController@redirectToShow'])->where('id', '[0-9]+');
Route::get('brothel/{id}',['as'=>'brothelProfileById','uses'=>'BusinessController@redirectToShow'])->where('id', '[0-9]+');
Route::get('agencies/{id}',['as'=>'brothelProfileById','uses'=>'BusinessController@redirectToShow'])->where('id', '[0-9]+');
Route::get('massage-parlours/{id}',['as'=>'brothelProfileById','uses'=>'BusinessController@redirectToShow'])->where('id', '[0-9]+');
Route::get('escorts/{slug}',['as'=>'escortProfile','uses'=>'EscortController@show']);
Route::get('brothels/{slug}',['as'=>'businessProfile','uses'=>'BusinessController@showBusiness']);
Route::get('agencies/{slug}',['as'=>'businessProfile','uses'=>'BusinessController@showBusiness']);
Route::get('massage-parlours/{slug}',['as'=>'businessProfile','uses'=>'BusinessController@showBusiness']);

Route::get('brothels',['as'=>'brothels','uses'=>'BusinessController@brothels']);
//Route::get('escort-agencies',['as'=>'agencies','uses'=>'BusinessController@agencies']);
//Route::get('erotic-massage/massage-parlour',['as'=>'massageParlours','uses'=>'BusinessController@massageParlours']);
Route::get('escorts',['as'=>'escorts','uses'=>'EscortController@index']);
  //Route::get('brothels/{city}',['as'=>'brothels','uses'=>'BusinessController@brothels']);
Route::get('escort-agencies',['as'=>'agencies','uses'=>'BusinessController@agencies']);
Route::get('erotic-massage/massage-parlour',['as'=>'massage','uses'=>'BusinessController@massageParlours']);

Route::get('blog',['as'=>'blogIndex','uses'=>'BlogController@index']);
Route::get('blog/{id}',['as'=>'blogSingle','uses'=>'BlogController@show']);
Route::get('contact',['as'=>'getContact','uses'=>'ContactController@getContact']);
Route::post('contact',['before'=>'csrf','as'=>'postContact','uses'=>'ContactController@postContact']);
Route::get('report',['as'=>'getReport','uses'=>'ContactController@getReport']);
Route::post('report',['before'=>'csrf','as'=>'postReport','uses'=>'ContactController@postReport']);
  //Route::post('testing',['before'=>'csrf','as'=>'postReport','uses'=>'ContactController@postReport']);
Route::post('testing',['before'=>'csrf','as'=>'postEscortInfo','uses'=>'ProfileController@postEscortInfo']);
Route::post('profile/escortInfo',['before'=>'csrf','as'=>'postEscortInfo','uses'=>'ProfileController@postEscortInfo']);

Route::get('_search_escorts',['as'=>'loadEscorts','uses'=>'EscortController@loadEscorts']);
Route::get('/services/apiTest', 'ServiceController@apiTest' );


/*-------------------------------------------------------------------------
 | Test routes, remove before pushing live
 | ------------------------------------------------------------------------
*/
//Route::get('email_test/{to}/{from}/{file}',['as'=>'email_test','uses'=>'UserController@email_test']);

/*-------------------------------------------------------------------------
 | Redirect old style pages to the new ones
 | ------------------------------------------------------------------------
*/
   Route::get('{city}-escorts', function($city){
    return Redirect::to('/escorts-' . $city, 301);
});
  Route::get('city/{city}', function($city){
    return Redirect::to('/escorts-' . $city, 301);
});

//Route::get('escorts-{city}',['as'=>'index','uses'=>'EscortController@index']);

/*
|--------------------------------------------------------------------------
| Ajax routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'ajax'), function()
{
  Route::get('get_geo_ids','LocationController@GetGeoIds');
  Route::get('map_modal','LocationController@mapModal');
  Route::get('get_state_and_cities','LocationController@getStateAndCities');
  Route::get('get_suburb_coordinates','LocationController@getSuburbCoordinates');
  Route::get('get_postcode_info','LocationController@getPostcodeInfo');
  Route::get('get_suburb_info','LocationController@getSuburbInfo');

  Route::get('getUserWishlist/{id}','WishlistController@getUserWishlist');
  Route::get('addToWishlist','WishlistController@addToWishlist');
  Route::get('removeFromWishlist','WishlistController@removeFromWishlist');

  Route::get('setMainImage',['as'=>'setMainImage','uses'=>'ImageController@setMainImage']);
  Route::get('updateImageOrder',['as'=>'updateImageOrder','uses'=>'ImageController@updateOrder']);
  Route::get('deleteImage',['as'=>'deleteImage','uses'=>'ImageController@deleteImage']);

  Route::get('sendComment',['as'=>'sendComment','uses'=>'BlogController@sendComment']);
  Route::get('escort/filters/apply',['as'=>'loadEscorts','uses'=>'EscortController@loadEscorts']);
  Route::get('brothel/filters/apply',['as'=>'loadBrothels','uses'=>'BusinessController@loadBrothels']);
  Route::get('agencie/filters/apply',['as'=>'loadAgencies','uses'=>'BusinessController@loadAgencies']);
  Route::get('massage-parlour/filters/apply',['as'=>'loadMassage','uses'=>'BusinessController@loadMassage']);
  Route::get('escorts/increaseViewCounter/{id}',['before'=>'blockLocalConnection','as'=>'increaseViewCounter','uses'=>'EscortController@increaseViewCounter']);
  Route::post('escorts/{id}/reveal-phone',['before'=>'blockLocalConnection','as'=>'postPhoneReveal','uses'=>'EscortController@postPhoneReveal']);
  Route::get('escort/sendPlaymail',['as'=>'sendPlaymail','uses'=>'EscortController@sendPlaymail']);
  Route::get('business/sendPlaymail',['as'=>'sendPlaymail','uses'=>'BusinessController@sendPlaymail']);
  Route::get('escort/leaveReview',['as'=>'leaveReview','uses'=>'EscortController@leaveReview']);
  Route::get('/business/getgirl/{id}','BusinessController@getGirl');

  Route::get('addTouring',['as'=>'addTouring','uses'=>'ProfileController@addTouring']);
  Route::get('deleteTouring',['as'=>'deleteTouring','uses'=>'ProfileController@deleteTouring']);
  // Route::get('save-update',['as'=>'saveUpdate','uses'=>'ProfileController@saveUpdate']);
  // Route::get('delete-update',['as'=>'deleteUpdate','uses'=>'ProfileController@deleteUpdate']);
  Route::get('publishReview',['as'=>'publishReview','uses'=>'ProfileController@publishReview']);
  Route::get('unpublishReview',['as'=>'unpublishReview','uses'=>'ProfileController@unpublishReview']);
  Route::get('deleteReview',['as'=>'deleteReview','uses'=>'ProfileController@deleteReview']);
  Route::get('get_cityLatLng',['as'=>'get_cityLatLng','uses'=>'LocationController@get_cityLatLng']);

});
Route::group(array('before' => array('dashboard'),'after' => 'allowOrigin'), function()
{

  /*
|--------------------------------------------------------------------------
| Directive routes
|--------------------------------------------------------------------------
*/
Route::get('directives/{directive}',['as'=>'directive','uses'=>'Dashboard\DirectiveController@getDirective']);

  /*
|--------------------------------------------------------------------------
| Business routes
|--------------------------------------------------------------------------
*/

  Route::get('services/businessProfile/{id}/{type}',['uses'=>'Dashboard\BusinessController@profile']);
  Route::get('services/businessMobileMenu/{id}/{active_id}/{type}',['uses'=>'Dashboard\BusinessController@businessMobileMenu']);
  Route::get('services/autocomplete/{search}',['uses'=>'Dashboard\BusinessController@autocomplete']);
  Route::get('services/getlatlong/{search}',['uses'=>'Dashboard\BusinessController@getlatlong']);
  Route::get('services/businessDetails/{id}',['uses'=>'Dashboard\BusinessController@detail']);
  Route::get('services/businessPanel/{id}',['uses'=>'Dashboard\BusinessController@panel']);
  Route::get('services/adminPanel/{id}',['uses'=>'Dashboard\AdminController@panel']);
  Route::get('services/businessScreen/{id}',['uses'=>'Dashboard\BusinessController@screen']);
  Route::get('services/adminScreen/{id}',['uses'=>'Dashboard\AdminController@screen']);
  Route::get('services/businessEscortScreen/{id}',['uses'=>'Dashboard\BusinessController@escortScreen']);
  Route::get('services/adminBusinessScreen/{id}',['uses'=>'Dashboard\AdminController@businessScreen']);
  Route::get('services/adminEscortScreen/{id}',['uses'=>'Dashboard\AdminController@escortScreen']);
  Route::get('services/businessEscortScreen',['uses'=>'Dashboard\BusinessController@newEscort']);
  Route::get('services/businessLowerEscortScreen/{id}',['uses'=>'Dashboard\BusinessController@lowerEscortScreen']);
  Route::get('services/adminLowerEscortScreen/{id}',['uses'=>'Dashboard\AdminController@lowerEscortScreen']);
  Route::get('services/adminLowerBusinessScreen/{id}',['uses'=>'Dashboard\AdminController@lowerBusinessScreen']);
  Route::get('/services/reviewScreen/{id}',['uses'=>'Dashboard\BusinessController@reviewScreen']);

  /*-----------------Escort Routes-----------------*/
  Route::get('services/escortProfile/{id}',['uses'=>'Dashboard\EscortController@profile']);
  Route::get('services/escortDetails/{id}',['uses'=>'Dashboard\EscortController@detail']);
  Route::get('services/escortPanel/{id}',['uses'=>'Dashboard\EscortController@panel']);
  Route::get('services/escortScreen/{id}',['uses'=>'Dashboard\EscortController@screen']);
  Route::get('services/escortProfileTab/{tab}',['uses'=>'Dashboard\EscortController@profileTab']);
  Route::get('services/adminProfileTab/{tab}',['uses'=>'Dashboard\AdminController@profileTab']);
  Route::get('services/businessProfileTab/{tab}',['uses'=>'Dashboard\BusinessController@profileTab']);
  Route::get('services/businessPaymentTab/{tab}/{id}',['uses'=>'Dashboard\BusinessController@paymentTab']);
  Route::get('services/getavailabilites/{id}',['uses'=>'Dashboard\EscortController@getAvailabilites']);
  Route::get('services/getrates/{id}','Dashboard\EscortController@getRates');
  Route::get('services/getImages/{id}','Dashboard\EscortController@getImages');
  Route::get('services/getplan/{id}','Dashboard\ServiceController@getPlan');

  Route::post('services/changeplancontact','ServiceController@contactChangePlan');

  Route::get('services/userScreen/{id}',['uses'=>'Dashboard\UserController@screen']);
  Route::get('services/userPanel/{id}',['uses'=>'Dashboard\UserController@panel']);



  Route::get('services/escortescortScreen/{id}',['uses'=>'Dashboard\EscortController@escortScreen']);
  Route::get('services/escortlowerEscortScreen/{id}',['uses'=>'Dashboard\EscortController@lowerEscortScreen']);

  //Route::Resource('/services/busSaveBasic',['as'=>'busSaveBasic','uses'=>'ServiceController@busSaveBasic']);
  Route::resource('/services/busSaveBasic', 'ServiceController@busSaveBasic');
  Route::get('services/suburbSelect/',['uses'=>'Controller@suburbSelect']);

  /*-------------------Profile image script-----------
  */

  Route::post('services/busLogoUpload',['as'=>'busLogoUpload','uses'=>'ServiceController@busLogoUpload']);
  Route::post('services/busLogoCrop',['as'=>'busLogoCrop','uses'=>'ServiceController@busLogoCrop']);

  /*------------------Profile Banner script------------
  */
  Route::post('services/uploadTempImage', 'ServiceController@uploadTempImage');
  Route::post('services/uploadTempGalleryImage', 'ServiceController@uploadTempGalleryImage');
  Route::post('chetest',['as'=>'chetest','uses'=>'Dashboard\EscortController@chetest']);
  Route::get('chetest',['as'=>'chetest','uses'=>'ServiceController@chetest']);
  Route::get('services/updatepaymentstatus',['as'=>'updatepaymentsstatus','uses'=>'PaymentController@updatePaymentStatus']);

  /*-------------------Mpm Api------------------------
  */
  Route::resource('/services/mpmApi', 'ServiceController@mpmApi' );
  Route::resource('/services/updateReviewStatus', 'ServiceController@updateReviewStatus' );
  Route::resource('/escort/sendUserPlaymail','EscortController@sendUserPlaymail');

  /*-------------------Initialize Angular models------
  ---------------------------------------------------*/
  Route::get('/services/escort-profile-details/{id}', ['as'=>'EscortProfileDetails','uses'=>'Dashboard\EscortController@EscortProfileDetails']);
  Route::get('/services/business-profile-details/{id}', ['as'=>'BusinessProfileDetails','uses'=>'Dashboard\BusinessController@BusinessProfileDetails']);
  Route::get('services/getCalls/{id}', ['as'=>'getEscortCalls','uses'=>'Dashboard\EscortController@getEscortCalls']);

});


/*
|--------------------------------------------------------------------------
| Profile routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => array('auth'),'after' => 'allowOrigin'), function()
{
  Route::get('escorts/preview/{id}',['as'=>'escortProfilePreview','uses'=>'EscortController@escortProfilePreview'])->where('id', '[0-9]+');
  Route::get('business/preview/{id}',['as'=>'businessProfilePreview','uses'=>'BusinessController@businessProfilePreview'])->where('id', '[0-9]+');
  Route::get('user/profile',['as'=>'getProfile','uses'=>'ProfileController@getProfile']);
  Route::get('emailExists',['as'=>'emailExists','uses'=>'UserController@emailExists']);
  /*Route::get('profile/userInfo',['as'=>'getUserInfo','uses'=>'ProfileController@getUserInfo']);
  Route::post('profile/userInfo',['before'=>'csrf','as'=>'postUserInfo','uses'=>'ProfileController@postUserInfo']);

  Route::get('profile/escortInfo',['as'=>'getEscortInfo','uses'=>'ProfileController@getEscortInfo']);
  Route::post('profile/escortInfo',['before'=>'csrf','as'=>'postEscortInfo','uses'=>'ProfileController@postEscortInfo']);
  Route::get('profile/escortAbout',['as'=>'getEscortAbout','uses'=>'ProfileController@getEscortAbout']);
  Route::post('profile/escortAbout',['before'=>'csrf','as'=>'postEscortAbout','uses'=>'ProfileController@postEscortAbout']);
  Route::get('profile/escortAvailability',['as'=>'getEscortAvailability','uses'=>'ProfileController@getEscortAvailability']);
  Route::post('profile/escortAvailability',['before'=>'csrf','as'=>'postEscortAvailability','uses'=>'ProfileController@postEscortAvailability']);
  Route::get('profile/escortGallery',['as'=>'getEscortGallery','uses'=>'ProfileController@getEscortGallery']);
  Route::post('profile/escortGallery',['before'=>'csrf','as'=>'postEscortGallery','uses'=>'ProfileController@postEscortGallery']);
  Route::get('profile/escortSite',['as'=>'getEscortSite','uses'=>'ProfileController@getEscortSite']);
  Route::post('profile/escortSite',['before'=>'csrf','as'=>'postEscortSite','uses'=>'ProfileController@postEscortSite']);
  Route::get('profile/escortReviews',['as'=>'getEscortReviews','uses'=>'ProfileController@getEscortReviews']);
  Route::post('profile/escortReviews',['before'=>'csrf','as'=>'postEscortReviews','uses'=>'ProfileController@postEscortReviews']);
  Route::get('profile/businessInfo',['as'=>'getBusinessInfo','uses'=>'ProfileController@getBusinessInfo']);
  Route::post('profile/businessInfo',['before'=>'csrf','as'=>'postBusinessInfo','uses'=>'ProfileController@postBusinessInfo']);
  Route::get('profile/businessLogo',['as'=>'getBusinessLogo','uses'=>'ProfileController@getBusinessLogo']);
  Route::post('profile/businessLogo',['before'=>'csrf','as'=>'postBusinessLogo','uses'=>'ProfileController@postBusinessLogo']);
  Route::get('profile/businessMyEscorts',['as'=>'getBusinessMyEscorts','uses'=>'ProfileController@getBusinessMyEscorts']);
  Route::post('profile/businessMyEscorts',['before'=>'csrf','as'=>'postBusinessMyEscorts','uses'=>'ProfileController@postBusinessMyEscorts']);
  Route::get('profile/newEscort',['as'=>'getNewEscort','uses'=>'ProfileController@getNewEscort']);
  Route::post('profile/newEscort',['before'=>'csrf','as'=>'postNewEscort','uses'=>'ProfileController@postNewEscort']);
  Route::get('profile/newBusiness',['as'=>'getNewBusiness','uses'=>'ProfileController@getNewBusiness']);
  Route::post('profile/newBusiness',['before'=>'csrf','as'=>'postNewBusiness','uses'=>'ProfileController@postNewBusiness']);

  Route::get('profile/memcache_test',['as'=>'memcache_test','uses'=>'ProfileController@memcache_test']);*/

  /*--------------------Profile Services--------------
  */

 // Route::get('admin/users/administrators',['uses'=>'Admin\UserController@administratorsIndex']);


});

/*
|--------------------------------------------------------------------------
| Composers
|--------------------------------------------------------------------------
*/

View::composer('frontend.partials.footer.posts', 'FooterPostsComposer');
View::composer('frontend.partials.sidebar.left-common-widgets','SidebarWidgetsComposer');


/*
|--------------------------------------------------------------------------
| Sitemap
|--------------------------------------------------------------------------
*/

Route::get('mysitemap', function(){
  $sitemap = new MyPlaymate\SitemapGenerator;

  $sitemap->generate();
});

  /*
|--------------------------------------------------------------------------
| Delete Session files, check for auto bumpups- Runs every two minutes
|--------------------------------------------------------------------------
*/

Route::get('deleteSessions',['as'=>'softCron','uses'=>'ServiceController@softCron']);

  /*
|--------------------------------------------------------------------------
| Admin fix issue Routes
|--------------------------------------------------------------------------
*/

Route::get('configureLocations',['as'=>'configureLocations','uses'=>'ServiceController@configureLocations']);

/*
|--------------------------------------------------------------------------
| User routes
|--------------------------------------------------------------------------
*/

Route::get('register',['as'=>'getRegister','uses'=>'UserController@getRegister']);
Route::post('register',['as'=>'postRegister','uses'=>'UserController@postRegister']);

Route::get('users/activate',['as'=>'userActivation','uses'=>'UserController@activateUser']);
Route::post('users/forgot-password',['before'=>'csrf','as'=>'forgotPassword','uses'=>'UserController@forgotPassword']);

Route::get('users/change-password',['as'=>'getChangePassword','uses'=>'UserController@getChangePassword']);
Route::post('users/change-password',['before'=>'csrf','as'=>'postChangePassword','uses'=>'UserController@postChangePassword']);

Route::post('login',['before'=>'csrf','as'=>'login','uses'=>'UserController@login']);
Route::get('logout',['as'=>'logout','uses'=>'UserController@logout']);


/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/

Route::get('admin/login',['as'=>'adminGetLogin','uses'=>'Admin\UserController@getLogin']);
Route::post('admin/login',['as'=>'adminPostLogin','uses'=>'Admin\UserController@postLogin']);
Route::group(array('before' => 'auth.admin'), function()
{
 // Route::get('admin/getuserprofile/{userId}',['as'=>'$userid','uses'=>'ProfileController@adminGetUserProfile']);
  Route::get('admin/userprofile/{userId}',['as'=>'$userid','uses'=>'ProfileController@adminGetProfile']);

  Route::get('admin/admin-export',['as'=>'adminExport','uses'=>'Admin\ExportController@index']);
  Route::get('admin/export_escort_data',['as'=>'adminExport','uses'=>'Admin\ExportController@exportAjax']);
  Route::post('page_setMainImage',['as'=>'setPageImage','uses'=>'Admin\PageController@setMainImage']);
  Route::get('admin',['as'=>'adminDashboard','uses'=>'Admin\DashboardController@index']);
  Route::get('admin/logout',['as'=>'adminLogout','uses'=>'Admin\UserController@logout']);
  Route::get('admin/users',['as'=>'adminUsers','uses'=>'Admin\UserController@usersIndex']);
  Route::get('admin/ajax_users',['as'=>'ajaxAdminUsers','uses'=>'Admin\UserController@usersAjax']);

  Route::get('admin/users/administrators',['uses'=>'Admin\UserController@administratorsIndex']);
  Route::get('admin/ajax_administrators',['uses'=>'Admin\UserController@administratorsAjax']);
  Route::get('admin/users/withEscorts',['uses'=>'Admin\UserController@withEscortsIndex']);
  Route::get('admin/ajax_withEscorts',['uses'=>'Admin\UserController@withEscortsAjax']);
  Route::get('admin/users/withBusinesses',['uses'=>'Admin\UserController@withBusinessesIndex']);
  Route::get('admin/ajax_withBusinesses',['uses'=>'Admin\UserController@withBusinessesAjax']);

  Route::get('admin/users/new',['as'=>'adminGetUserNew','uses'=>'Admin\UserController@getUserNew']);
  Route::post('admin/users/new',['before'=>'csrf','as'=>'adminPostUserNew','uses'=>'Admin\UserController@postUserNew']);
  Route::get('admin/users/delete',['as'=>'adminDeleteUser','uses'=>'Admin\UserController@deleteUser']);
  Route::get('admin/users/{id}',['as'=>'adminGetUserEdit','uses'=>'Admin\UserController@getUserEdit']);
  Route::post('admin/users/{id}',['before'=>'csrf','as'=>'adminPostUserEdit','uses'=>'Admin\UserController@postUserEdit']);

  Route::get('admin/pending_escorts',['as'=>'adminPendingEscorts','uses'=>'Admin\EscortController@pendingEscortsIndex']);
  Route::get('admin/ajax_pending_escorts',['as'=>'ajaxAdminPendingEscorts','uses'=>'Admin\EscortController@pendingEscortsAjax']);
  Route::get('admin/plans',['as'=>'plans','uses'=>'Admin\BusinessController@plans']);
  Route::get('admin/approve_escort',['as'=>'approveEscort','uses'=>'Admin\EscortController@approveEscort']);
  Route::get('admin/disapprove_escort',['as'=>'disapproveEscort','uses'=>'Admin\EscortController@disapproveEscort']);
  Route::get('admin/approve_business',['as'=>'Business','uses'=>'Admin\BusinessController@approveBusiness']);
  Route::get('admin/disapprove_business',['as'=>'disapproveBusiness','uses'=>'Admin\BusinessController@disapproveBusiness']);
  Route::get('admin/delete_escort',['as'=>'deleteEscort','uses'=>'Admin\EscortController@deleteEscort']);
  Route::get('admin/toggle_featured',['as'=>'toggleFeatured','uses'=>'Admin\EscortController@toggleFeatured']);
  Route::get('admin/bump-up',['as'=>'bumpUpEscort','uses'=>'Admin\EscortController@bumpUp']);
  Route::get('admin/bump-down',['as'=>'bumpDownEscort','uses'=>'Admin\EscortController@bumpDown']);
  Route::get('admin/makeUnavailable',['as'=>'admin/makeUnavailable','uses'=>'Admin\EscortController@makeUnavailable']);

  Route::get('admin/escorts',['as'=>'adminEscorts','uses'=>'Admin\EscortController@escortIndex']);
    //Ches new admin page
  Route::get('admin/import_csv','Admin\CsvController@import_csvIndex');
  Route::get('admin/getStatusCount',['as'=>'getStatusCount','uses'=>'Admin\EscortController@getStatusCount']);
  Route::get('admin/escorts/new',['as'=>'adminGetEscortsNew','uses'=>'Admin\EscortController@getEscortNew']);
  Route::post('admin/escorts/new',['before'=>'csrf','as'=>'adminPostEscortsNew','uses'=>'Admin\EscortController@postEscortNew']);
  Route::get('admin/escorts/info/{id}',['as'=>'adminEscortGetEditInfo','uses'=>'Admin\EscortController@escortGetEditInfo']);
  Route::get('admin/escorts/about/{id}',['as'=>'adminEscortGetEditAbout','uses'=>'Admin\EscortController@escortGetEditAbout']);
  Route::get('admin/escorts/availability/{id}',['as'=>'adminEscortGetEditAvailability','uses'=>'Admin\EscortController@escortGetEditAvailability']);
  Route::get('admin/escorts/gallery/{id}',['as'=>'adminEscortGetEditGallery','uses'=>'Admin\EscortController@escortGetEditGallery']);
  Route::post('admin/escorts/gallery/upload/{id}',['before'=>'csrf','as'=>'adminUploadEscortGallery','uses'=>'Admin\EscortController@uploadEscortGallery']);
  Route::post('/escorts/gallery/upload/{id}',['before'=>'csrf','as'=>'UploadEscortGallery','uses'=>'EscortController@uploadEscortGallery']);
  Route::get('admin/escorts/site/{id}',['as'=>'adminEscortGetEditSite','uses'=>'Admin\EscortController@escortGetEditSite']);
  Route::get('admin/escorts/reviews/{id}',['as'=>'adminEscortGetEditReviews','uses'=>'Admin\EscortController@escortGetEditReviews']);
  Route::get('admin/escorts/seo/{id}',['as'=>'adminEscortGetEditSeo','uses'=>'Admin\EscortController@escortGetEditSeo']);
  Route::post('admin/escorts/info/{id}',['before'=>'csrf','as'=>'adminEscortPostEditInfo','uses'=>'Admin\EscortController@escortPostEditInfo']);
  Route::post('admin/escorts/about/{id}',['before'=>'csrf','as'=>'adminEscortPostEditAbout','uses'=>'Admin\EscortController@escortPostEditAbout']);
  Route::post('admin/escorts/availability/{id}',['before'=>'csrf','as'=>'adminEscortPostEditAvailability','uses'=>'Admin\EscortController@escortPostEditAvailability']);
  Route::post('admin/escorts/gallery/{id}',['before'=>'csrf','as'=>'adminEscortPostEditGallery','uses'=>'Admin\EscortController@escortPostEditGallery']);
  Route::post('admin/escorts/site/{id}',['before'=>'csrf','as'=>'adminEscortPostEditSite','uses'=>'Admin\EscortController@escortPostEditSite']);
  Route::post('admin/escorts/reviews/{id}',['before'=>'csrf','as'=>'adminEscortPostEditReviews','uses'=>'Admin\EscortController@escortPostEditReviews']);
  Route::post('admin/escorts/seo/{id}',['before'=>'csrf','as'=>'adminEscortPostEditSeo','uses'=>'Admin\EscortController@escortPostEditSeo']);
  Route::get('admin/ajax_escorts',['as'=>'ajaxAdminEscorts','uses'=>'Admin\EscortController@escortAjax']);
  Route::post('admin/newplan',['as'=>'newplan','uses'=>'Admin\BusinessController@newplan']);
  Route::post('admin/changePlan',['as'=>'changePlan','uses'=>'Admin\BusinessController@changePlan']);
  Route::post('admin/editplan',['before'=>'csrf','as'=>'editplan','uses'=>'Admin\BusinessController@editplan']);
  Route::get('admin/ajaxEventsByDate',['as'=>'ajaxAdminEscorts','uses'=>'Admin\EscortController@ajaxEventsByDate']);
  Route::get('admin/delete-business/{id}','Admin\BusinessController@deleteBusiness');
  Route::get('admin/pending_images',['as'=>'adminPendingImage','uses'=>'Admin\ImageController@pendingImagesIndex']);
  Route::get('admin/show-fullsize-image',['as'=>'showFullSizeImage','uses'=>'Admin\ImageController@showFullSizeImage']);
  Route::get('admin/approve_image',['as'=>'approveImage','uses'=>'Admin\ImageController@approveImage']);
  Route::get('admin/disapprove_image',['as'=>'disapproveImage','uses'=>'Admin\ImageController@disapproveImage']);
  Route::get('/admin/send-images-report',['as'=>'sendImagesReport','uses'=>'Admin\ImageController@sendImagesReport']);
  Route::get('/admin/send-banner-report',['as'=>'sendBannerReport','uses'=>'Admin\ImageController@sendBannerReport']);

  Route::post('admin/businesses/logo/upload/{id}',['before'=>'csrf','as'=>'adminUploadBusinessLogo','uses'=>'Admin\BusinessController@uploadLogo']);
  Route::get('admin/businesses',['as'=>'adminBusinesses','uses'=>'Admin\BusinessController@businessIndex']);
  Route::get('admin/pending_businesses',['as'=>'pending_businesses','uses'=>'Admin\BusinessController@pending_businesses']);
  Route::get('admin/deleted-businesses',['as'=>'deletedBusinesses','uses'=>'Admin\BusinessController@deletedBusinesses']);
  Route::get('admin/deleted-escorts',['as'=>'deletedEscorts','uses'=>'Admin\EscortController@deletedEscorts']);
  Route::get('admin/businesses/new',['as'=>'adminGetNewBusiness','uses'=>'Admin\BusinessController@getNewBusiness']);
  Route::post('admin/businesses/new',['before'=>'csrf','as'=>'adminPostNewBusiness','uses'=>'Admin\BusinessController@postNewBusiness']);
  Route::get('admin/ajax_businesses',['as'=>'ajaxAdminBusinesses','uses'=>'Admin\BusinessController@businessAjax']);
  Route::get('admin/ajax_pending_businesses',['as'=>'ajax_pending_businesses','uses'=>'Admin\BusinessController@ajax_pending_businesses']);
  Route::get('admin/businesses/edit/{id}',['as'=>'adminGetEditBusiness','uses'=>'Admin\BusinessController@getEdit']);
  Route::get('admin/businesses/logo/{id}',['as'=>'adminGetBusinessLogo','uses'=>'Admin\BusinessController@getLogo']);
  Route::post('admin/businesses/edit/{id}',['before'=>'csrf','as'=>'adminPostEditBusiness','uses'=>'Admin\BusinessController@postEdit']);
  Route::get('admin/ajax_business_escorts',['as'=>'ajaxAdminBusinessEscorts','uses'=>'Admin\BusinessController@businessEscortsAjax']);

  Route::get('admin/pages',['as'=>'adminPages','uses'=>'Admin\PageController@index']);
  Route::get('admin/city-pages',['as'=>'adminCityPages','uses'=>'Admin\CityPageController@index']);
  Route::get('admin/brothel-pages',['as'=>'adminBrothelPages','uses'=>'Admin\BrothelPageController@index']);
  Route::get('admin/ajax_pages',['as'=>'adminAjaxPages','uses'=>'Admin\PageController@ajax']);
  Route::get('admin/ajax_cityPages',['as'=>'adminAjaxCityPages','uses'=>'Admin\CityPageController@ajax']);
  Route::get('admin/ajax_brothelPages',['as'=>'adminAjaxBrothelPages','uses'=>'Admin\BrothelPageController@ajax']);
  Route::get('admin/pages/new',['as'=>'adminGetNewPage','uses'=>'Admin\PageController@getNew']);
  Route::post('admin/pages/new',['as'=>'adminPostNewPage','uses'=>'Admin\PageController@postNew']);
  Route::get('admin/pages/edit/{id}',['as'=>'adminGetEditPage','uses'=>'Admin\PageController@getEdit']);
  Route::post('admin/pages/edit/{id}',['as'=>'adminPostEditPage','uses'=>'Admin\PageController@postEdit']);

  Route::get('admin/blog',['as'=>'adminBlog','uses'=>'Admin\BlogController@index']);
  Route::get('admin/blog/categories',['as'=>'adminBlog','uses'=>'Admin\BlogController@categories']);
  Route::get('admin/blog/tags',['as'=>'adminBlog','uses'=>'Admin\BlogController@tags']);
  Route::get('admin/ajax_blog',['as'=>'adminAjaxBlog','uses'=>'Admin\BlogController@ajax']);
  Route::get('admin/ajax_tags',['as'=>'adminAjaxTags','uses'=>'Admin\BlogController@ajax_tags']);
  Route::get('admin/ajax_categories',['as'=>'adminAjaxCategories','uses'=>'Admin\BlogController@ajax_categories']);
  Route::get('admin/blog/new',['as'=>'adminGetNewBlog','uses'=>'Admin\BlogController@getNew']);
  Route::get('admin/blog/category/new',['as'=>'adminGetNewBlogCategory','uses'=>'Admin\BlogController@getNewCategory']);
  Route::post('admin/blog/new',['as'=>'adminPostNewBlog','uses'=>'Admin\BlogController@postNew']);
  Route::post('admin/blog/category/new',['as'=>'adminPostNewBlogCategory','uses'=>'Admin\BlogController@postNewCategory']);
  Route::post('admin/blog/tag/new',['as'=>'adminPostNewBlogTag','uses'=>'Admin\BlogController@postNewTag']);
  Route::get('admin/blog/edit/{id}',['as'=>'adminGetEditBlog','uses'=>'Admin\BlogController@getEdit']);
  Route::post('admin/blog/edit/{id}',['as'=>'adminPostEditBlog','uses'=>'Admin\BlogController@postEdit']);
  Route::get('admin/blog/upload',['as'=>'adminBlogUpload','uses'=>'Admin\BlogController@upload']);
  Route::post('admin/blog/upload',['as'=>'adminBlogUpload','uses'=>'Admin\BlogController@upload']);

  Route::get('admin/comments',['as'=>'adminComments','uses'=>'Admin\CommentController@index']);
  Route::get('admin/ajax_comments',['as'=>'adminAjaxComments','uses'=>'Admin\CommentController@ajax']);
  Route::get('admin/approve_comment',['as'=>'approveComment','uses'=>'Admin\CommentController@approve']);
  Route::get('admin/disapprove_comment',['as'=>'disapproveComment','uses'=>'Admin\CommentController@disapprove']);

  // Route::get('admin/updates',['as'=>'adminUpdates','uses'=>'Admin\UpdateController@index']);
  // Route::get('admin/ajax_updates',['as'=>'adminAjaxUpdates','uses'=>'Admin\UpdateController@ajax']);
  // Route::get('admin/approve_update',['as'=>'approveUpdate','uses'=>'Admin\UpdateController@approve']);
  // Route::get('admin/disapprove_update',['as'=>'disapproveUpdate','uses'=>'Admin\UpdateController@disapprove']);

  Route::get('admin/reviews',['as'=>'adminReviews','uses'=>'Admin\ReviewController@index']);
  Route::get('admin/ajax_reviews',['as'=>'adminAjaxReviews','uses'=>'Admin\ReviewController@ajax']);
  Route::get('admin/approve_review',['as'=>'approveReview','uses'=>'Admin\ReviewController@approve']);
  Route::get('admin/disapprove_review',['as'=>'disapproveReview','uses'=>'Admin\ReviewController@disapprove']);
  Route::get('admin/advertising-spaces',['as'=>'adSpaces','uses'=>'Admin\AdvertisingController@adSpaces']);
  Route::get('admin/media-library',['as'=>'index','uses'=>'Admin\MediaController@index']);
  Route::post('admin/save-frontpage-ads',['as'=>'saveFrontPageAds','uses'=>'Admin\AdvertisingController@saveFrontPageAds']);
  Route::post('admin/show-front-page-ads',['as'=>'showFrontPageAds','uses'=>'Admin\AdvertisingController@showFrontPageAds']);
  Route::post('admin/upload-media',['as'=>'uploadMedia','uses'=>'Admin\MediaController@uploadMedia']);

});

  /* Route::get('/change_bump_dates',function(){
   $escorts = Escort::select('*')->where('bumpup_date','=','0000-00-00 00:00:00')->get();
    print_r ( $escorts );
     foreach( $escorts as $escort ) {
       $escort->bumpup_date = '2010-01-01 09:00:00';
       $escort->save();
  }
  });*/
  /*  Route::get('/add_empty_authors',function(){
          $posts = Post::orderBy('date_from','DESC')
                     ->get();
   foreach ($posts as $post) {
      if ($post->user_id == 0 ){
          $post->user_id = '1174';
          $post->save();
       }
        }
  });*/
  /*  Route::get('adjustUserType',function(){
      $users = User::select('*')->get();

      foreach ($users as $user) {
        if ( $user->isSuperUser() ) {
             $user->type = 1;
             $user->save();
        }
        elseif ( count( $user->businesses)  > 0 ) {
             $user->type = 4;
             $user->save();
       }
       elseif ( count ( $user->escorts ) > 0 ) {
             $user->type = 3;
             $user->save();
         }
        else {
             $user->type = 2;
             $user->save(); }
       }
  });
   Route::get('/multipleEscorts',function(){
      $users = User::select('*')->get();

      foreach ($users as $user) {
           if ($user->type == 3 && count ( $user->escorts ) > 1 ) {
             echo 'id: ' . $user->id . '<br>';
             echo 'email: ' . $user->email .'<br>';
             echo '<pre>';
               print_r ($user->escorts );
                 echo '</pre>';
            }
      }
  });
   Route::get('/addplanstosuers',function(){
     $users = User::select('*')->get();

     foreach( $users as $user ){
        if ( $user->type == 3 ){
           $user->business_plan = 1;
        }
        elseif ( $user->type == 4 ){
           $user->business_plan = 4;
        }
        $user->save();
     }
   });*/

Route::get('/{slug}',['as'=>'slug','uses'=>'PageController@catchall']);
