angular.module('dashboard', [
  'dashboard.controllers',
  'dashboard.services',
  'dashboard.panelDirectives',
  'mpmApi',
  'checklist-model',
  'angular-spinkit',
]).
  run(function() {
    FastClick.attach(document.body);
  });