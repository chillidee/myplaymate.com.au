@extends('dashboard.layout')

@section('content')

@include('dashboard.partials.mobile-menu')

<script>
     var id = {{$user->id}},
         mPActiveIcon = '_MpOverview',
         profiletabid = 'basicInfo',
         cache = {},
         currentEscort = '{{$current_escort}}';
         window.escortCount = '{{$escortCount}}';
         @if ( isset( $businesses[0] ) ) suburbId = '{{$businesses[0]->suburb_id}}', @else suburbId = '0', @endif
         curEscAvail = 0,
         @if ( isset( $businesses[0] ) ) currentProfileAction = 'Edit'; @else currentProfileAction = 'New'; @endif
         window.type = 'business';
         window.planEscorts = '{{$plan->escort_profiles}}';
         window.isBusiness = '{{ ( isset( $businesses[0] ) ? 1 : 0 ) }}';
         window.currentBusiness  = '{{ ( isset( $businesses[0] ) ? $businesses[0]->id : 0 ) }}';
         window.escortStats = {{ json_encode( $escortStats ) }};
</script>
<div id ='overlay'></div>
<div id ='planPopup'> @include('dashboard.partials.plan-popup') </div>
  <div class = 'container-fluid'>
    <div class ='row top_header'>
      <div class ='col-sm-3 no_padding'>
          <div id ='_mp_brand_container'>
            <div id ='logo'>
              <a href ='/'><img src ='/assets/dashboard/images/mpmAv.png' alt ='MPM'></a>
            </div>
            <div id ='_mp_areaName'>
            @if ( isset( $businesses[0] ) )
                    <span>{{ $businesses[0]->name }}</span>
                    <script>var El = '0', active_id = {{ $businesses[0]->id }};</script>
            @else   <span>{{$user->full_name}}</span>
                    <script>var El = '0', active_id = 0;</script>
            @endif

            </div>
          </div>
    </div>
    <div class ='col-sm-9 no_padding'>
      <div id ='_mp_prof_header'>
        <div id ='profile_menu'>
          <ul id ='profile_dropdown' class ='dropCaret'>
      @if ( isset( $businesses[0] ) )
           @foreach( $businesses as $business)
              <li>@if ( $business->image )
              <div id ='_mp_img_holder'>
                      <img src ='{{Config::get('constants.CDN')}}/businesses/{{$business->image}}'>
              </div>@endif
                  {{ $business->name}}
              </li>
           @endforeach
      @endif
           <li class ='signout'><a href ='/logout'>Logout</a></li>
          </ul>
        </div>
      </div>
    </div>
</div>
<div class ='row main_content'>
 <div>
   <section>
    <div class ='col-md-3 no_padding _MpDetailArea panelFixed'>
      <div id ='_mpMenu'>
        <div ng-repeat="item in busMenu" on-finish-render="test">
          <div ng-click ="changeScreens(item.id)" id = '@{{ item.id }}_' class ='_mPIcon' title="@{{item.title}}" stateId = "@{{ item.id }}" menuicon>@{{item.item}}</div>
        </div>
      </div>
             <panel panelid = "@{{activeicon}}"></panel>
       </div>
      <div class ='col-lg-9 col-lg-offset-3 no_padding _MpScreenArea'>
             <screen screenid = "@{{activeicon}}"class ='screen'></screen>
      </div>
      </section>  
          <script src="/assets/dashboard/js/jqueryui.min.js"></script>
       <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVAKU7LtUy9yWGX_uoP1cJ9kW0Hqhf754&signed_in=true&libraries=places"
        async defer></script>
       <script src="/assets/dashboard/js/fastclick.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script src="/assets/dashboard/js/angular.cropper.js"></script>
        <script src="/assets/dashboard/js/ng-img-crop.js"></script>
        <script src="/assets/dashboard/js/picturecut.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
        <script src="/assets/dashboard/js/angular-sortable-view.min.js"></script>
        <script src="/assets/dashboard/js/angular-spinkit.min.js"></script>
        <script src="/assets/dashboard/js/app.js"></script>
        <script src="/assets/dashboard/js/services.js"></script>
        <script src="/assets/dashboard/js/controller.js"></script>
        <script src="/assets/dashboard/js/httpInteceptor.js"></script>
        <script src="/assets/dashboard/js/ng-file-upload-shim.min.js"></script> <!-- for no html5 browsers support -->
        <script src="/assets/dashboard/js/ng-file-upload.min.js"></script>
        <script src="/assets/dashboard/js/jquery-ui-Slid.min.js"></script>
        <script type="text/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
        <script src ="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.3.5/parsley.min.js"></script>
        <script src="/assets/dashboard/js/mobile-menu.js"></script>
        <link rel="stylesheet" href="/assets/dashboard/css/jquery-ui-min.css" type="text/css" />

      </div>
</div>
</div>
@stop
