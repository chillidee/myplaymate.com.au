@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>New escort</h1>
    </div>
</div><!--pageheader-->

{{-- @include('admin.partials.escorts-breadcrumb') --}}


<div id="basic-info">
    {{ Form::open(['id'=>'escortInfo']) }}
    	<div class="row">
    		<div class="col-md-6">
		        <div class="control-group">
		            {{ Form::label('escort_name','Escort Name*',array('class'=>'control-label')) }}

		            <div class="controls">
		                {{ Form::text('escort_name','',array('class'=>'escort-name form-control','required')) }}
		            </div>
		        </div>
		        <div class="row">
		        	<div class="col-md-12">
		        		<div class="control-group">
				            {{ Form::label('user_id','User*',array('class'=>'control-label')) }}
				            <div class="controls">
				                {{ Form::select('user_id',$users,isset($_GET['business_id']) ? Business::find($_GET['business_id'])->user_id : '',array('class'=>'select2 form-control','required','style'=>'width:100%',isset($_GET['business_id']) ? 'disabled' : '')) }}
				            </div>
				            @if(isset($_GET['business_id']))
				            	{{ Form::hidden('user_id',Business::find($_GET['business_id'])->user_id) }}
				            @endif

				        </div>
		        	</div>
		        	<div class="col-md-4">
						<div class="control-group">
				            <p>{{ Form::label('under_business','Under business',array('class'=>'control-label')) }}</p>
				            <div class="controls">
					            <div class="radio-inline">
					                {{ Form::radio('under_business',1,isset($_GET['business_id']) ? 1 : 0,array('required','id'=>'under-business-1')) }}
					                {{ Form::label('under-business-1','Yes') }}
					            </div>
				            	<div class="radio-inline">
					                {{ Form::radio('under_business',0,isset($_GET['business_id']) ? 0 : 1,array('required','id'=>'under-business-2')) }}
					                {{ Form::label('under-business-2','No') }}
					            </div>
				            </div>
				        </div>
		        	</div>
		        	<div class="col-md-8">
		        		<div class="control-group">
				            {{ Form::label('business_id','Business*',array('class'=>'control-label')) }}
				            <div class="controls">
				                {{ Form::select('business_id',$businesses,isset($_GET['business_id']) ? $_GET['business_id'] : '',array('class'=>'select2 form-control','id'=>'business-id','required','style'=>'width:100%')) }}
				            </div>
				        </div>
		        	</div>
		        </div>
        		<div class="control-group" style="clear:both">
		            {{ Form::label('email','Email*',array('class'=>'control-label')) }}
		            <div class="controls">
		                {{ Form::text('email','',array('class'=>'form-control','required','data-parsley-type'=>'email')) }}
		            </div>
		        </div>
        		<div class="control-group">
		            {{ Form::label('licence_number','Licence number (if applicable)',array('class'=>'control-label')) }}
		            <div class="controls">
		                {{ Form::text('licence_number','',array('class'=>'form-control')) }}
		            </div>
		        </div>
		        <div class="control-group">
		            {{ Form::label('height_id','Height*',array('class'=>'control-label')) }}
		            <div class="controls">
		                {{ Form::select('height_id',$heights,'',array('class'=>'form-control')) }}
		            </div>
		        </div>
        		<div class="control-group">
		            <p>{{ Form::label('contact_phone','Contact by Phone/SMS*',array('class'=>'control-label')) }}</p>
		            <div class="radio-inline">
		                {{ Form::radio('contact_phone',1,0,array('required','id'=>'contact_phone-yes')) }}
		                {{ Form::label('contact_phone-yes','Yes') }}
		            </div>
		            <div class="radio-inline">
		                {{ Form::radio('contact_phone',0,0,array('id'=>'contact_phone-no')) }}
		                {{ Form::label('contact_phone-no','No') }}
		            </div>
		        </div>
    		</div>


    		<div class="col-md-6">
		        <div class="control-group">
		            {{ Form::label('phone','Phone*',array('class'=>'control-label')) }}

		            <div class="controls">
		                {{ Form::text('phone','',array('class'=>'form-control','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)')) }}
		            </div>
		        </div>
		        <div class="control-group">
		            {{ Form::label('suburb_id','Location*',array('class'=>'control-label')) }}
		            <div class="controls">
		                {{ Form::select('suburb_id',$suburbs,'',array('class'=>'select2 form-control','required','style'=>'width:100%','min'=>1)) }}
		            </div>
		        </div>
		        <div class="control-group">
		            {{ Form::label('age_id','Age*',array('class'=>'control-label')) }}

		            <div class="controls">
		                {{ Form::select('age_id',$ages,'',array('class'=>'form-control')) }}
		            </div>
		        </div>
		        <div class="control-group" id="sex">
		            <p>{{ Form::label('gender','Gender*',array('class'=>'control-label')) }}</p>
		            <div class="controls">
			            <div class="radio-inline">
			                {{ Form::radio('gender_id',1,0,array('id'=>'gender_id-1')) }}
			                {{ Form::label('gender_id-1','Female') }}
			            </div>
			            <div class="radio-inline">
			                {{ Form::radio('gender_id',2,0,array('id'=>'gender_id-2')) }}
			                {{ Form::label('gender_id-2','Male') }}
			            </div>
			            <div class="radio-inline">
			                {{ Form::radio('gender_id',3,0,array('id'=>'gender_id-3','data-parsley-required')) }}
			                {{ Form::label('gender_id-3','Transexual') }}
			            </div>
		            </div>
	
		        </div>
		        <div class="control-group">
		            <p>{{ Form::label('contact_playmail','Contact by PlayMail*',array('class'=>'control-label')) }}</p>
		            <div class="radio-inline">
		                {{ Form::radio('contact_playmail',1,0,array('id'=>'contact_playmail-yes','required')) }}
		                {{ Form::label('contact_playmail-yes','Yes') }}
		            </div>
		            <div class="radio-inline">
		                {{ Form::radio('contact_playmail',0,0,array('id'=>'contact_playmail-no')) }}
		                {{ Form::label('contact_playmail-no','No') }}
		            </div>
		        </div>
    		</div>
    	</div>
        <div>
    		{{ Form::hidden('business_id',$business_id) }}
        	{{ Form::submit('SAVE AND CONTINUE',array('class'=>'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
</div>
<script>
	jQuery(document).ready(function($) {
		$('.select2').select2({
			minimumInputLength: 4,
			placeholder: 'Select a suburb'
		});
		$('form').parsley();

		if ($('#under-business-2').is(':checked'))
		{
			$('#business-id').prop('disabled',true);
		}
		if ($('#under-business-1').is(':checked'))
		{
			$('#business-id').prop('disabled',false);
		}

		$('#under-business-2').on('change', function() {
			if ($(this).is(':checked'))
				$('#business-id').prop('disabled',true);
		});
		$('#under-business-1').on('change', function() {
			if ($(this).is(':checked'))
				$('#business-id').prop('disabled',false);
		});
	});
</script>
@stop