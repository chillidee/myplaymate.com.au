@extends('frontend.layout')

@section('content')
@include('frontend.partials.topbar.right_floater')
<div id ='left_pull_filters' class ='no_close'>
  <div id ='close_left_pull'></div>
  <h4>Search</h4>
  <form id ='custom_search_escorts'>
    <input id="escort_side-search" name="s" type="text" placeholder="Type and hit enter..." autocomplete="off"/>
  </form>
  @include('frontend.partials.sidebar.escort-filters')
</div>
@if ( isset($main_image) )
<img id = "main_image" src ="{{ Config::get('constants.CDN').str_replace( 'uploads/','', $main_image) }}" alt ="@if(isset($meta ) && $meta->img_alt != ''){{$meta->img_alt}}@else{{$name}}@endif">
@endif
<!-- #page-title start -->
<section id="page-title" class="page-title-1">
  <div class="container">
    @if ( isset($name) )
    <h1>{{ $name }}</h1>
    <div id ='filter_information' class ='no-close'></div>
    @elseif ( isset( $city  ) )
    <h1>CITY- {{ ucfirst($city)  }}</h1>
    <div id ='filter_information' class ='no-close'></div>
    @endif
  </div><!-- .containr end -->
</section><!-- #page-title end -->

<!-- .boxed version of the page -->
<section class="page-wrapper section-content">
  <!-- .container start -->
  <div class="container">

    <!-- .row start -->
    <div class="row">
      @if(isset($city_page) && isset($top_content))
      <section class="top-content">
        {{ nl2br($top_content) }}
      </section>
      @endif
      <div id="directory-container" class="col-xs-12">
        <div id="directory">
          <ul class="escort-container grid-view">
            <li class="grid-template col-xs-12 col-ms-6 col-md-4 col-lg-3 col-xl-15 col-xl-3 escorts-wishlist">
              <figure class="escorts-img-container">
                <div class="figfavorite">
                  <a href="" class="is-not-wish">
                    <i class="fa fa-heart"></i>
                  </a>

                  <a href="" class="is-wish">
                    <i class="fa fa-times"></i>
                  </a>
                </div>

                <div class="info-container">
                  <div class="figinfo">
                    <span class="figname"></span>
                    <ul class="figdata">
                      <li class="escort-location">
                        <span class = 'business-suburb'></span>
                        @if(isset($city))
                        <span class="escort-distance"> km from {{ ucfirst($city) }}</span></li>
                        @else
                        <span class="escort-distance"> km</span></li>
                        @endif
                      </li></ul>
                    </div><!-- figinfo-->
                  </div><!-- end of info-container-->
                  <a href="{{ url('escorts/') }}">
                    <img class="img-profile" src="" alt="">
                  </a>
                </figure>
              </li>

            </ul>
            <div class="spinner">
             <div class="dot1"></div>
             <div class="dot2"></div>
           </div>
         </div>
       </div>
     </div><!-- .row end -->
   </div><!-- .container end -->
 </section> <!-- End of the Boxed Version Page -->
 <section class="page-wrapper">
  <!-- .container start -->
  <div class="container">

    <!-- .row start -->
    <div class="row">
      <div id="touring-container" class="col-xs-12">
        <div id="Touringdirectory">
          <ul class="touring-container grid-view">


          </ul>
        </div>
      </div>
    </div><!-- .row end -->
  </div><!-- .container end -->
</section> <!-- End of the Boxed Version Page -->

@stop

@section('javascript')
<style>
  #city a:hover, #city-changer {
   color: #BEB5B5!important;
   cursor: default!important;
 }
</style>
<script>
  window.thisCity = '{{ $city }}';
  window.type ='escort';


  jQuery(document).ready(function($) {
    var listToggle = $('#list-view-toggle'),
    gridToggle = $('#grid-view-toggle'),
    container = $('.escort-container'),
    sortSelect = $('#sort-by select');

    // If the page was called by brothels, agencies or massage controller, add the business type to the body for the filters
    @if(isset($business))
    $('body').attr('data-business', {{ $business }});
    @endif

    // If the page was called by a city, add the suburb id to the body for the filters
    @if(isset($suburb_id))
    $('body').attr('data-suburb-id', '{{ $suburb_id }}');
    var pageSuburb = '{{ $suburb_id }}';
    @endif

    // If the page was called as "my wishlist", add the wishlist attribute to the body for the filters
    @if(isset($my_wishlist))
    $('body').attr('data-wishlist', 1);
    @endif

    if (typeof check == 'undefined') {
     applyFilters();
   }
   else {
     load_saved_escorts(check.show);
   }

   if (Cookies.get('view') == 'list') {
    jQuery('.escort-container').removeClass('grid-view').addClass('list-view');
    listToggle.addClass('active');
    gridToggle.removeClass('active');
  } else {
    jQuery('.escort-container').removeClass('list-view').addClass('grid-view');
    gridToggle.addClass('active');
    listToggle.removeClass('active');
  }


    // Events
    listToggle.on('click', function(e) {
      e.preventDefault();

      listToggle.addClass('active');
      gridToggle.removeClass('active');
      Cookies.set('view','list');
      applyFilters();
    });

    gridToggle.on('click', function(e) {
      e.preventDefault();

      gridToggle.addClass('active');
      listToggle.removeClass('active');
      Cookies.set('view','grid');
      applyFilters();
    });

    var toggleFilters = $('#toggleFilters');
    if($(window).width() < 768){
      toggleFilters.show();
    }
    toggleFilters.on('click', function(e) {
      e.preventDefault();
      $('.filters').fadeToggle(300);
    });
  });

</script>

@stop
