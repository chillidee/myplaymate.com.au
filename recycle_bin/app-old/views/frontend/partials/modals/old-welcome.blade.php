    <div class="splash-screen welcome-modal" id="welcomeModal">
        <form class="splash-form">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1" style="text-align:center;">
                        <img src="{{ url('/assets/frontend/img/logo.png') }}" width="80%" class="hidden-xs">
                        <ul>
                            <li>This website contains content that is sexual in nature. </li>
                            <li>To enter this site, you must be an adult of the legal age of eighteen (18) or older.</li>
                            <li>By clicking on the Enter link below, you confirm and understand that this site contains adult and explicit content and agree to release myplaymate.com.au from any liability that may arise from the use of this site.</li>
                        </ul>
                        <br>
                        <span>Unless all the above criteria are met, you are not eligible to enter the site and must leave immediately.</span>
                        <br>
                        <button type="button" class="btn btn-default" style="float: initial;">Enter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>