<?php

namespace spec\MyPlaymate\Filters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class HairColorFilterSpec extends ObjectBehavior
{
	private $escorts = [
		['hair_color_id' => 1],
		['hair_color_id' => 2],
		['hair_color_id' => 1],
		['hair_color_id' => 3],
    ];

    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Filters\HairColorFilter');
    }

    function it_takes_only_id_2()
    {
    	$this->apply($this->escorts,[2])->shouldHaveCount(1);
    }

    function it_takes_id_2_and_3()
    {
    	$this->apply($this->escorts,[2,3])->shouldHaveCount(2);
    }

    function it_takes_id_2_3_and_4()
    {
    	$this->apply($this->escorts,[2,3,4])->shouldHaveCount(2);
    }
}




