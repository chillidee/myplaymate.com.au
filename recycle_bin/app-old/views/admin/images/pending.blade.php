@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-picture-o"></span></div>
    <div class="pagetitle">
        <h1>Pending images</h1>
    </div>
</div><!--pageheader-->
<?php $last_escort = 0; ?>
	<div class="pending-images-container alignc section-content col-md-12">
	@foreach($imgs as $img)
		@if($img->escort_id != $last_escort && $last_escort > 0)
			</div>
			<div class="pending-images-container alignc section-content col-md-12">
		@endif
		@if($img->escort_id != $last_escort)
			<h3>{{ Escort::find($img->escort_id)->escort_name }}</h3>
		@endif
			<div class="pending-image col-md-3" data-escort-id="{{ $img->escort_id }}" data-image-id="{{ $img->id }}">
				<img src="{{ $img->thumbnail(170,170) }}" alt="">
				<div class="alignc section-content">
					<a class="approve btn btn-success">Approve</a>
					<a class="disapprove btn btn-danger">Disapprove</a>
					<div class="disapprove-message">
						<textarea placeholder="Insert reason for disapproving."></textarea>
					</div>
				</div>
			</div>

		@if($img->escort_id != $last_escort)
			<a href="" class="btn btn-primary send-report">Save and send report</a>
		@endif
		<?php $last_escort = $img->escort_id; ?>
	@endforeach
	</div>

	<img id="big-image">
	<div class="overlay"></div>



<script>
	function setStatus(picture, url, disapproveMessage)
	{
		var imageId = picture.data('image-id'),
			disapprove_message = disapproveMessage || '';

		jQuery.ajax({
			url: '/admin/'+url,
			data: {
				image_id: imageId,
				disapprove_message : disapprove_message
			},
		})
		.done(function() {
			console.log('image status updated');
		});
	}

	(function($){
		var halfW, halfH;

		$('.rightpanel').on('click','.pending-image img',function(){
			var imageId = $(this).closest('.pending-image').attr('data-image-id'),
				bigImage = $('#big-image');
			bigImage.attr('src','');
			$.ajax({
				url: '/admin/show-fullsize-image',
				data: {image_id: imageId},
			})
			.done(function(filename) {
				
				$('.overlay').fadeIn(300);
				bigImage.attr('src','/uploads/escorts/'+filename);

				bigImage.fadeIn(300);
			});
		});

		$('#big-image').on('load', function() {
			halfW = $('#big-image').width() / 2;
			halfH = $('#big-image').height() / 2;
			console.log(halfW);
			console.log(halfH);
			$(this).css({
				marginLeft: - halfW,
				marginTop: - halfH
			});
		});

		$('.overlay').on('click', function() {
			$(this).fadeOut(300);
			$('#big-image').fadeOut(300);
		});

		$('.rightpanel').on('click', '.approve', function(e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest('.pending-image').removeClass('bg-danger').addClass('bg-success');
			$this.siblings('.disapprove-message').fadeOut(200);
		});	

		$('.rightpanel').on('click', '.disapprove', function(e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest('.pending-image').removeClass('bg-success').addClass('bg-danger');
			$this.siblings('.disapprove-message').fadeIn(200);
		});

		$('.rightpanel').on('click', '.send-report', function(e) {
			e.preventDefault();
			var $this = $(this),
				pendingImages = $this.siblings('.pending-image'),
				approvedImages = [],
				disapprovedImages = [],
				escortId;
			pendingImages.each(function(index, el) {
				var	element = $(el),
					imageId = element.attr('data-image-id');
				escortId = element.attr('data-escort-id');

				if (element.hasClass('bg-success')){
					var url = 'approve_image';
					setStatus(element,url);
					image = [imageId];
					approvedImages.push(image);
				}
				if (element.hasClass('bg-danger')){
					var url = 'disapprove_image',
						disapproveMessage = element.find('textarea').val();
					setStatus(element,url,disapproveMessage);
					image = [imageId,disapproveMessage];
					disapprovedImages.push(image);
				}
			});

			$.ajax({
				url: '/admin/send-images-report',
				data: {
					approved_images: approvedImages,
					disapproved_images: disapprovedImages,
					escort_id: escortId,
				},
			})
			.done(function() {
				pendingImages.each(function(index, el) {
					var element = $(el);
					if (element.hasClass('bg-danger') || element.hasClass('bg-success'))
						element.remove();
				});
				if ($this.siblings('.pending-image').length == 0)
					$this.closest('.pending-images-container').remove();

				$('body').animate({scrollTop: 0}, 200);
				jQuery('.alert-success').text('Report sent!').fadeIn();
			});
		});
	})(jQuery);
</script>

@stop



