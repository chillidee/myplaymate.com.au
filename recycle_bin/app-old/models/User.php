<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $guarded = [];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public static $rules = [
        'full_name' => 'required',
        'email' => 'required|unique:users,email|email',
        'phone' => 'required|regex:/0\d{9}(?!.)/',
        'password' => 'required|confirmed',
        'terms_and_conditions' => 'required'
    ];

    public static $messages = [
        'phone.regex' => 'A valid australian phone number is required. (eg 0411223344 or 0211223344)'
    ];

    public static $updateRules = [];

    public function escorts()
    {
        return $this->hasMany('Escort');
    }

    public function escort()
    {
        $escorts = $this->escorts;

        foreach ($escorts as $key => $escort) {
            if ($escort->under_business == 0)
                return $escort;
        }
    }

    public function businesses()
    {
        return $this->hasMany('Business');
    }

    public function wishlist()
    {
        return $this->hasMany('Wishlist');
    }

    public function scopeNotSuperUsers($query)
    {
        return $query->where('superuser', 0);
    }

    public function isSuperUser()
    {
        return $this->superuser;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function sendActivationEmail()
    {
        $emailTemplate = EmailTemplate::where('name', 'activation-code')->first();

        $data                    = [];
        $data['full_name']       = $this->full_name;
        $data['email']           = $this->email;
        $data['activation_link'] = Config::get('app.fullUrl').'users/activate?email='.$this->email.'&activation_code='.$this->activation_code;
        $data['activation_code'] = $this->activation_code;
        $data['campaign_title']  = "";

        // Send activation email to new user and to admin
        Mail::send('emails.activation-code', ['data' => $data], function ($message) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Account Verification')
                    ->to([$this->email,Config::get('app.adminEmail')]);
        });
    }





    public function resetPasswordEmail()
    {
        $newPassword = str_random(8);

        $this->password = Hash::make($newPassword);
        $this->save();

        $emailTemplate = EmailTemplate::where('name', 'password-reset')->first();

        $data                    = [];
        $data['full_name']       = $this->full_name;
        $data['email']           = $this->email;
        $data['new_password']    = $newPassword;

        // Send activation email to new user and to admin
        Mail::send('emails.'.$emailTemplate->name, ['data' => $data], function ($message) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Password Reset')
                    ->to($this->email);
        });
    }

    public function adminResetPasswordEmail()
    {
        $newPassword = str_random(8);

        $this->password = Hash::make($newPassword);
        $this->save();

        $emailTemplate = EmailTemplate::where('name', 'admin-password-reset')->first();

        $templateData                 = [];
        $templateData['user']         = $this->toArray();
        $templateData['new_password'] = $newPassword;

        $emailData           = new stdClass();
        $emailData->user     = $this;
        $emailData->template = $emailTemplate;

        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);

            $message->to($emailData->user->email);
        });
    }

    public function sendAdminWelcome()
    {
        $emailTemplate = EmailTemplate::where('name', 'admin-welcome')->first();

        $templateData                 = [];
        $templateData['user']         = $this->toArray();
        $templateData['new_password'] = str_random(6);

        $this->password         = Hash::make($templateData['new_password']);
        $this->save();

        $emailData           = new stdClass();
        $emailData->user     = $this;
        $emailData->template = $emailTemplate;

        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);

            $message->to($emailData->user->email);
        });
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function isValidEmail($email, $id)
    {
        $user = self::where('email', '=', $email)->get()->last();

        if (empty($user) or $user->id == $id) {
            return true;
        }

        return false;
    }

    public function isValidStorage($id)
    {
        $user = self::find($id);

        return (empty($user) or empty($user->email)) ? false : true;
    }

    public function updateEmail($email, $id)
    {
        $user        = self::find($id);
        $user->email = $email;
        $user->save();
    }
}
