<?php

class BodiesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('bodies')->truncate();

        $bodies = [
            ['name' => 'Female Petite'
                ,'gender_id' => 1, ],
            ['name' => 'Female Slim'
                ,'gender_id' => 1, ],
            ['name' => 'Female Busty'
                ,'gender_id' => 1, ],
            ['name' => 'Female Athletic'
                ,'gender_id' => 1, ],
            ['name' => 'Female Curvy'
                ,'gender_id' => 1, ],
            ['name' => 'Female Heavy'
                ,'gender_id' => 1, ],
            ['name' => 'Male Slim'
                ,'gender_id' => 2, ],
            ['name' => 'Male Athletic'
                ,'gender_id' => 2, ],
            ['name' => 'Male Average'
                ,'gender_id' => 2, ],
            ['name' => 'Male Muscular'
                ,'gender_id' => 2, ],
            ['name' => 'Male Heavy'
                ,'gender_id' => 2, ],
            ];

        // Uncomment the below to run the seeder
        DB::table('bodies')->insert($bodies);
    }
}
