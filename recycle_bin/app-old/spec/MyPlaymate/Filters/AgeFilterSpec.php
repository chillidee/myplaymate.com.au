<?php

namespace spec\MyPlaymate\Filters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Escort;

class AgeFilterSpec extends ObjectBehavior
{
	private $escorts = [
		['age_id' => 1],
		['age_id' => 20],
		['age_id' => 8],
        ['age_id' => 3]
	];

    private $ids = [1,8];

    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Filters\AgeFilter');
    }

    function it_translates_string_in_array()
    {
    	$this->stringToArray([['1-8'],['10-13']])->shouldReturn([['1','8'],['10','13']]);
    }

    function it_fills_the_ranges()
    {
        $this->fillArrayRanges([['1','8'],['10','13']])->shouldReturn([[1,2,3,4,5,6,7,8],[10,11,12,13]]);
    }

    function it_merges_the_ranges()
    {
        $this->mergeArrayRanges([[1,2,3,4,5,6,7,8],[10,11,12,13]])->shouldReturn([1,2,3,4,5,6,7,8,10,11,12,13]);
    }

    function it_returns_3_for_1_8()
    {
        $this->apply($this->escorts,[['1-8']])->shouldHaveCount(3);
    }

    function it_returns_0_for_23_33()
    {
    	$this->apply($this->escorts,[['23-33']])->shouldHaveCount(0);
    }

    function it_returns_4_for_1_8_and_10_20()
    {
        $this->apply($this->escorts,[['1-8'],['10-20']])->shouldHaveCount(4);
    }

    function it_returns_3_for_1_8_and_10_19()
    {
        $this->apply($this->escorts,[['1-8'],['10-19']])->shouldHaveCount(3);
    }
}









