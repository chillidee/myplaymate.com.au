<?php

class ImageHelper
{
	public static function cropBusinessLogo($originalPath, $originalFilename)
	{
		$img = Image::make($originalPath.$originalFilename);
		$img->fit(870,580)
			->save($originalPath.'/'.$originalFilename);
	}

    public static function createThumbnailSize($originalPath, $originalFilename, $width, $height)
    {
		$img = Image::make($originalPath.$originalFilename);

		if($img->width() > $img->height()){
			$img->resize(null,$height,function($i){
				$i->aspectRatio();
			});
		} else {
			$img->resize($width,null,function($i){
				$i->aspectRatio();
			});
		}

		$img->fit($width,$height)
			->save($originalPath.'thumbnails/thumb_'.$width.'x'.$height.'_'.$originalFilename);
    }
      public static function s3Upload( $filename, $path )
    {
   // 	$im = new Imagick(Config::get('constants.PUBLIC_DIR') . '/temp/'.$filename );
   // 	$im->trimImage(0);
   // 	$im->writeImage (Config::get('constants.PUBLIC_DIR') . '/temp/'.$filename  );

      $img = Image::make(Config::get('constants.PUBLIC_DIR') . '/temp/' . $filename);

     if($img->width() > 1000 ){
      $img->resize(null,1000,function($i){
        $i->aspectRatio();
      });
      $img->save(Config::get('constants.PUBLIC_DIR') . '/temp/' . $filename);
     } 

	      $key = $path.$filename;
        $s3 = App::make('aws')->get('s3');
        $s3->putObject(array(
                'Bucket'     => 'mpm-uploads',
                'Key'        => $key,
                'SourceFile' => Config::get('constants.PUBLIC_DIR') . '/temp/'.$filename,
                           ));
    }
      public static function s3Delete( $path, $file )
    {
                           $s3 = App::make('aws')->get('s3');
                           $s3->deleteObject(array(
                             'Bucket'     => 'mpm-uploads',
                             'Key'        => $path.$file
                           ));
                           $s3->deleteObject(array(
                             'Bucket'     => 'mpm-uploads',
                             'Key'        => $path . '/thumbnails/thumb_170x170_' .$file
                           ));
                           $s3->deleteObject(array(
                             'Bucket'     => 'mpm-uploads',
                             'Key'        => $path . '/thumbnails/thumb_220x320_' .$file
                           ));
    }

    public static function s3thumbnail($filename, $filePrefix, $width, $height)
    {
    	
		$img = Image::make(Config::get('constants.PUBLIC_DIR') . '/temp/' . $filename);

		if($img->width() > $img->height()){
			$img->resize(null,$height,function($i){
				$i->aspectRatio();
			});
		} else {
			$img->resize($width,null,function($i){
				$i->aspectRatio();
			});
		}

		$img->fit($width,$height)
			->save(Config::get('constants.PUBLIC_DIR') . '/temp/thumbnails/thumb_'.$width.'x'.$height.'_'.$filename);

		$key = $filePrefix . '/thumb_'.$width.'x'.$height.'_'.$filename;
                           $s3 = App::make('aws')->get('s3');
                           $s3->putObject(array(
                             'Bucket'     => 'mpm-uploads',
                             'Key'        => $key,
                             'SourceFile' => Config::get('constants.PUBLIC_DIR') . '/temp/thumbnails/thumb_'.$width.'x'.$height.'_'.$filename,
                           ));
    }

    public static function watermark($path)
    {
		$img = Image::make(public_path().'/'.$path);

		$img->resize(null,500,function($constraint){
			$constraint->aspectRatio();
		});
		$width = $img->width();
		$watermark = Image::make(public_path().'/assets/frontend/img/watermark.png')
						  ->resize($width, null, function ($constraint) {
	    						$constraint->aspectRatio();
		});
		$img->insert($watermark, 'center')
			->save(public_path().'/'.$path.'_watermark.jpg');
    }
}










