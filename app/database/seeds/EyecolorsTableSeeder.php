<?php

class EyeColorsTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('eye_colors')->truncate();

        $EyeColors = [
            ['name' => 'Black', 'order' => 10],
            ['name' => 'Blue', 'order' => 20],
            ['name' => 'Brown', 'order' => 30],
            ['name' => 'Dark Brown', 'order' => 40],
            ['name' => 'Green', 'order' => 50],
            ['name' => 'Grey', 'order' => 60],
            ['name' => 'Hazel', 'order' => 70],
            ];

        // Uncomment the below to run the seeder
        DB::table('eye_colors')->insert($EyeColors);
    }
}
