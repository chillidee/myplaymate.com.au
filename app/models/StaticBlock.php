<?php

class StaticBlock extends Eloquent
{
    protected $guarded = [];

    public static $rules = [];

    public static function boot()
    {
        static::saved(function()
        {
            Event::fire('sitemap.generate');
        });
    }

    public static function getHtml($key)
    {
		return static::whereKey($key)->first()->value;
    }
}
