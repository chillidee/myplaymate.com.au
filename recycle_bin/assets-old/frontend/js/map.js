/**
 * @param  string postcode
 * @return array ['suburb' => suburb,'state' => state]
 */
function getPostcodeInfo(postcode){
    jQuery.ajax({
        url: '/get_postcode_info',
        data: {postcode: postcode}
    })
    .done(function(data) {
        console.log(data.suburb);
        Cookies.set('postcode',postcode);
        Cookies.set('suburb',data.suburb.id);
        Cookies.set('suburbName',data.suburb.name);
        Cookies.set('state',data.state.id);
        jQuery('#geoloc #city a').text(data.suburb.name);
    });
}

function getSuburbInfo(suburbId){
    jQuery.ajax({
        url: '/get_suburb_info',
        data: {suburb_id: suburbId}
    })
    .done(function(data) {
        Cookies.set('postcode',data.postcode);
        Cookies.set('suburb',data.suburb.id);
        Cookies.set('suburbName',data.suburb.name);
        Cookies.set('state',data.state.id);
        jQuery('#geoloc #city a').text(data.suburb.name);
    });
}

var colors = ['#00a2bf', '#004cbf', '#0900bf', '#5f00bf', '#b500bf', '#bf0072', '#bf001c', '#bf2626', '#bf6b26', '#bfaf26', '#89bf26', '#44bf26'];

jQuery(document).ready(function($) {

    // Map and position
    var cityLink = $('#geoloc #city a'),
        postcode = Cookies.get('postcode');

    cityLink.text(Cookies.get('suburbName'));

    var geocoder = new google.maps.Geocoder();

    if(!Cookies.get('suburb')){
        // If HTML5 geolocation is available
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                // get coordinates and set latlng
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var latlng = new google.maps.LatLng(lat, lng);

                // Use geocode to turn coordinates in real address info
                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        // Filter the results object to get the suburb id
                        if (results[0]){
                            var postcode   = results[0].address_components[5].long_name,
                                suburbName = results[0].address_components[2].long_name,
                                cityName   = results[3].address_components[0].long_name;
                            
                            cityLink.text(suburbName);

                            $.ajax({
                                url: '/get_geo_ids',
                                data: {postcode: postcode},
                            })
                            .done(function(data) {
                                suburb = data.suburb_id;
                                state = data.state_id;
                                Cookies.set('suburb',suburb);
                                Cookies.set('suburbName',suburbName);
                                Cookies.set('postcode',postcode);
                                Cookies.set('state',state);
                            }); 
                        }
                        else
                          console.log("No results found");
                    } else
                        console.log("Geocoder failed due to: " + status);
                });
            }, function(err){
                if(Cookies.get('suburb')){
                    var suburb = Cookies.get('suburb');
                    getSuburbInfo(suburb);
                } else {
                    // Sydney's id in suburbs table
                    var suburb = 10906;
                    getSuburbInfo(suburb);
                }
            });
        } else {
            if(Cookies.get('suburb')){
                var suburb = Cookies.get('suburb');
                getSuburbInfo(suburb);
            } else {
                // Sydney's id in suburbs table
                var suburb = 10906;
                getSuburbInfo(suburb);
            }
        }   
    }
    

    $('#trigger-overlay').click(function(event) {
        $.ajax({
            url: '/map_modal',
        })
        .done(function(data) {
            var stateId = Cookies.get('state');
            $.ajax({
                url: '/get_state_and_cities',
                data: {state_id: stateId},
            })
            .done(function(data) {                
                $('#current-state').text(data.state_name);

                // Populate the select
                $.each(data.cities, function(index, city) {
                    $('#city-select').append('<option value="'+index+'">'+city.name+'</option>');
                });
            });

            // Create colored overlay on the clicked state on the map
            var currentState = Cookies.get('state');
            var path = $('#mapId-'+currentState);

            path.attr('fill', colors[path.index() + 1]);
            
            // Show the modal window
            $('#geoMap').html(data).modal();
        });
    });

    $('body').on('click', '#geoMap path', function () {
        // Create colored overlay on the clicked state on the map
        var $this = $(this);
        $('path').attr('fill', '#333333');
        $this.attr({'fill': colors[$this.index() + 1]});

        // Populate select and current-state
        var stateId = $this[0].id.substr(6);
        $.ajax({
            url: '/get_state_and_cities',
            data: {state_id: stateId},
        })
        .done(function(data) { 
            var citySelect = $('#city-select');
            $('#current-state').text(data.state_name);

            // Populate the select
            citySelect.find('option').remove();
            $.each(data.cities, function(index, city) {
                var option = '<option data-state="'+city.state_id+'" data-postcode="'+city.postcode+'" value="'+city.id+'">'+city.name+'</option>';
                citySelect.append(option);
            });
        });
    });


    console.log();    
    $('body').on('click', '#map-ok', function(e) {
        var citySelect = $('#city-select'),
            selectedCity = citySelect.find('option:selected')
            suburbId = citySelect.val(),
            postcode = $('#postcode').val(),
            cityLink = $('#city a');

        // Postcode commands over city
        if (postcode != '')
            getPostcodeInfo(postcode);
        else{
            Cookies.set('suburb',selectedCity.val());
            Cookies.set('suburbName',selectedCity.text());
            Cookies.set('postcode',selectedCity.data('postcode'));
            Cookies.set('state',selectedCity.data('state'));
            cityLink.text(selectedCity.text());
            if (window.location.pathname == '/')
            {
                location.reload();
            }

            applyFilters();
        }

        $('#geoMap').modal('hide');
    });

});
















