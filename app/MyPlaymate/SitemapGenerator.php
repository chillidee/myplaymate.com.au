<?php 

namespace MyPlaymate;

use App, Url, Carbon\Carbon, DB;
use Post, Escort,Page;

class SitemapGenerator {

    public function generate()
    {
        $sitemap = App::make('sitemap');
        $posts = Post::orderBy('ID','DESC')
                     ->wherePublished(1)
                     ->where('date_from','<=',date('Y-m-d'))
                     ->where('date_to','>=',date('Y-m-d'))
                     ->get();
        $i = 0;
        $j = 0;
       foreach ($posts as $post)
        {
          if ( $i == 0 )
            $homeEdited = $post->created_at;
            $i ++;
          }
        

        // add items to the sitemap (url, date, priority, freq)

        // Main menu pages
        $sitemap->add(url('/'), $homeEdited, '1.0', 'daily');
        $sitemap->add(url('escorts'),Carbon::now(),'0.9', 'daily');
        $sitemap->add(url('brothels'),Carbon::now(),'0.9', 'daily');
        $sitemap->add(url('escort-agencies'),Carbon::now(), '0.9', 'daily');
        $sitemap->add(url('erotic-massage/massage-parlour'),Carbon::now(), '0.9', 'daily');
        $sitemap->add(url('blog'),Carbon::now(), '0.9', 'daily');
      
       $pages = Page::select(['url_key','updated_at','type'])
           ->where('published', '=', '1')
           ->where('type','!=','3' )
           ->orderby( 'type','DESC' )
           ->get();
      
       foreach ($pages as $page)
        {
            $sitemap->add(url($page->url_key), $page->updated_at, ($page->type == 1 ? '0.9' : '0.8'), ($page->type == 1 ? 'weekly' : 'monthly'));
        }

        $escorts = Escort::where('active','=','1')->where('status','=','2')->orwhere('status','=','4')->orderBy('bumpup_date', 'desc')->get();
        foreach ($escorts as $escort)
        {
            $sitemap->add(url('/escorts/'.$escort->seo_url), $escort->updated_at, '0.8', 'weekly');
        }
        $business = DB::table('businesses')->where('status','=','2')->orwhere('status','=','4')->orderBy('created_at', 'desc')->get();
        foreach ($business as $business)
        {
              if ( $business->type == 1 ) 
                 $url = '/brothels/';
              if ( $business->type == 2 ) 
                 $url = '/agencies/';
              if ( $business->type == 3 )
                 $url = '/massage-parlours/';

            $sitemap->add(url($url . $business->seo_url), $business->updated_at, '0.8', 'weekly');
        }

        foreach ($posts as $post)
        {
          if ( $j != 0 )
            $sitemap->add(url('/blog/'.$post->seo_url), $post->date_from, '0.8', 'monthly');
            $j++;
        }

        // generate your sitemap (format, filename)
        $sitemap->store('xml', 'sitemap');    
    }
}

    