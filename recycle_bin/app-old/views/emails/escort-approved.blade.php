@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class="deviceWidth" align="center" bgcolor="#eeeeed" border="0" cellpadding="0" cellspacing="0" width="580">
<tbody>
<tr>
<td style="padding: 0;" bgcolor="#ffffff" valign="top"><a href="http://www.myplaymate.com.au/"><img class="deviceWidth" style="display: block; border-radius: 4px;" src="http://www.myplaymate.com.au/assets/emails/main-updated-4.png" alt="" border="0" /></a></td>
</tr>
<tr>
<td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
<table bgcolor="#eeeeed">
<tbody>
<tr>
<td style="padding: 0 10px 10px 0;" valign="middle"><a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href="#">Hi {{$data['full_name']}}, </a></td>
</tr>
</tbody>
</table>
<p>Congratulations! Your profile is approved and is now visible to the public. Feel free to make changes to your profile at anytime online by signing in at <a style="text-decoration: none; color: #b60011; font-weight: bold;" href="http://www.myplaymate.com.au">www.myplaymate.com.au</a></p>
<p>We hope you're enjoying being a Playmate!</p>
<p>The My Playmate Team</p>
</td>
</tr>
</tbody>
</table>
<!-- End One Column -->	

@stop