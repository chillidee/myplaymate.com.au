<?php

class WishlistController extends Controller
{
	public function getUserWishlist($id)
	{
		return Wishlist::whereUserId($id)->get(array('escort_id'));
	}

	public function addToWishlist()
	{
		$user = Auth::user();
		$wish = Wishlist::where('escort_id','=',Input::get('id') )->where('user_id','=', $user->id )->first();
		if ( !$wish ){
		   $wish = new Wishlist();
		   $wish->user_id = $user->id;
		   $wish->escort_id = Input::get('id');
		   $wish->save();
		}
	}

	public function removeFromWishlist()
	{
		$wish = Wishlist::whereUserId(Auth::user()->id)
						->whereEscortId(Input::get('id'))
						->first();
		if ( $wish )
	    	$wish->delete();
	}
}