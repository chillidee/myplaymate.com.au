<?php

class FakeEscortRatesTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,20) as $index) {
            DB::table('escort_rates')
              ->insert([
                'escort_id'         => $index,
                'incall_1h_rate'    => $faker->numberBetween(50,350),
                'outcall_1h_rate'   => $faker->numberBetween(50,350)
            ]);
        }
    }
}
