<?php

use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;

class ProfileController extends \BaseController {

  protected $userRepo;

  protected $suburbRepo;

  function __construct(UserRepository $userRepo, SuburbRepository $suburbRepo) {
    $this->userRepo = $userRepo;
    $this->suburbRepo = $suburbRepo;
  }

    public function getProfile()
    {
        $user = Auth::user();
        $type = $user->getUserType();
        $message = Session::get('message');
        $pass = array( 
            'user' =>  $user,
            'message' => $message, 
            'type' => $type,
            'plan' => new BusinessPlan    
        );
        if ( $type == 1 ){
            return View::make('dashboard.admin.index',$pass );
          }
        if ( $type == 4 ) {
            $businesses = $user->businesses;
            $plan= BusinessPlan::where('id','=',$user->business_plan )->first();
                if ( !$plan ){
                  $plan= BusinessPlan::where('id','=',9 )->first();       
                }
            if ( !$businesses->isEmpty() ) {
                $firstBusiness = Business::where( 'id','=', $businesses[0]->id )->first();
                $pass['current_escort'] =  $firstBusiness->getFirstEscortId();
                $pass['businesses'] = $businesses; 
                $pass['escortCount'] = $firstBusiness->escortsCount();
                $pass['plan'] = $plan;
                $pass['plans'] = BusinessPlan::where('type','=','2')->where('useraccess','=','1')->get();
                $pass['escortStats'] = $firstBusiness->escortsStats();
            }
            else {
              $pass['current_escort'] = 0;
              $pass['businesses'] = new Business;
              $pass['user'] = $user;
              $pass['escortCount'] = 0;
              $pass['plan'] = $plan;
              $pass['plans'] = BusinessPlan::where('type','=','2')->where('useraccess','=','1')->get();
              $pass['escortStats'] = array();
            }
           return View::make('dashboard.business.index',$pass );
        }
        elseif( $type == 3 ) {
               $pass = array();
               $escort = Escort::where('user_id', '=', $user->id )->first();
               $plan= BusinessPlan::where('id','=',$user->business_plan )->first();
                if ( !$plan ){
                  $plan= BusinessPlan::where('id','=',8 )->first();       
                }
               $pass['user'] = $user;
               $pass['type'] = $type;
               $pass['plan'] = $plan;
               $pass['plans'] = BusinessPlan::where('type','=','1')->where('useraccess','=','1')->get();
             if ( $escort ) {
                $pass['escort'] = $escort;
                if(EscortImage::find($escort->main_image_id)) 
                    $pass['escort_image'] = $escort->main_image_id > 0 ? EscortImage::find($escort->main_image_id)->filename : '';   
             }
             return View::make('dashboard.escort.index',$pass );
        }
        elseif( $type == 2 ) {
             return View::make('dashboard.user.index', compact( 'user' ) );
        }
      //  return View::make('frontend.user.profile',$pass );
      return $type;
}
      public function adminGetProfile($userId)
    {
        $user = User::where( 'id', '=', $userId )->first();
        if ( !$user ) 
             return 'This user id does not exist in the database';
        $type = $user->getUserType();
        $message = Session::get('message');
        if ( $type == 1 )
           return Redirect::to('/admin');
              $pass = array( 
                'user' =>  $user,
                'message' => $message, 
                'type' => $type       
        );
        if ( $type == 4 ) {
             $businesses = $user->businesses;
             $plan= BusinessPlan::where('id','=',$user->business_plan )->first();
                if ( !$plan ){
                  $plan= BusinessPlan::where('id','=',9 )->first();       
                }
            if ( !$businesses->isEmpty() ) {
                $firstBusiness = Business::where( 'id','=', $businesses[0]->id )->first();
                $pass['current_escort'] =  $firstBusiness->getFirstEscortId();
                $pass['businesses'] = $businesses; 
                $pass['escortCount'] = $firstBusiness->escortsCount();
                $pass['plan'] = $plan;
                $pass['plans'] = BusinessPlan::where('type','=','2')->where('useraccess','=','1')->get();
                $pass['escortStats'] = $firstBusiness->escortsStats();
            }
            else {
              $pass['current_escort'] = 0;
              $pass['businesses'] = new Business;
              $pass['user'] = $user;
              $pass['escortCount'] = 0;
              $pass['plan'] = $plan;
              $pass['plans'] = BusinessPlan::where('type','=','2')->where('useraccess','=','1')->get();
              $pass['escortStats'] = array();
            }
           return View::make('dashboard.business.index',$pass );
        }
        elseif( $type == 3 ) {
               $pass = array();
               $escort = Escort::where('user_id', '=', $user->id )->first();
               $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
                if ( !$plan ){
                  $plan= BusinessPlan::where('id','=',8 )->first();       
                }
               $pass['user'] = $user;
               $pass['type'] = $type;
               $pass['plan'] = $plan;
               $pass['plans'] = BusinessPlan::where('type','=','1')->where('useraccess','=','1')->get();
             if ( $escort ) {
                $pass['escort'] = $escort;
                if(EscortImage::find($escort->main_image_id)) 
                    $pass['escort_image'] = $escort->main_image_id > 0 ? EscortImage::find($escort->main_image_id)->filename : '';   
             }
             return View::make('dashboard.escort.index',$pass );
        }
        elseif( $type == 2 ) {
             return View::make('dashboard.user.index', compact( 'user' ) );
        }
      //  return View::make('frontend.user.profile',$pass );
      return $type;
}

    public function getUserInfo()
    {
    return View::make('frontend.partials.profile.user-profile');
    }

  public function postUserInfo()
  {
    $input = Input::all();
    $user = User::find($input['user_id']);

    $user->full_name = $input['full_name'];
    $user->email = $input['email'];
    $user->phone = $input['phone'];
    $user->save();
  }

  /**
   * Auth control to avoid malicious edits
   *
   * @params Integer $escort_id
   * @return Object $escort || Boolean false
   */

  public function checkProfileProperty($escort_id)
  {
    // Check if it is the user's escort
    $user_escort = Auth::user()->escort();
    if ($user_escort && $user_escort->id == $escort_id) {
      // If it is, set the variable
      return $user_escort;
    } else {
      // If it's not, check if she belongs to one of the user's businesses
     $businesses = Auth::user()->businesses;
     foreach ($businesses as $business) {
       $escorts = EscortBusiness::select('*')->where('business_id', '=', $business->id )->get();
       foreach( $escorts as $escort){
         if ( $escort->escort_id == Input::get('escort_id') ) {
             $escort = Escort::select('*')->where('id', '=', $escort->escort_id )->first();
             return $escort;
            }
         }
       }
    }
    return false;
  }
  public function memcache_test() {
    $memcache = new Memcache;
    $memcache->connect('127.0.0.1', 11211) or die ("Could not connect");

    //$version = $memcache->getVersion();
    //echo "Server's version: ".$version."<br/>\n";

    //$tmp_object = new stdClass;
    //$tmp_object->str_attr = 'test';
    //$tmp_object->int_attr = 123;


    //echo "Store data in the cache (data will expire in 10 seconds)<br/>\n";
  // $suburbs = $this->suburbRepo->populateSelect();
  //$heights = Height::lists('name','id');
  //$memcache->set('heights_list', $heights, false, 0) or die ("Failed to save data at the server");
  // $get_result = $memcache->get('suburb_dropdown');
  //  echo "Data from the cache:<br/>\n";

  // var_dump($get_result);
  }

  /**
   * GET /escortInfo
   *
   * @return Response
   */
  public function getEscortInfo()
  {
    // Check if the escort is associated with the user or one of his businesses
    $escort  = $this->checkProfileProperty(Input::get('escort_id'));
    //  if (!($escort))
    //  return Response::make('non ho escort',404);

    //$suburbs = $this->suburbRepo->populateSelect();
    // $memcache = new Memcache;
    //$memcache->connect('127.0.0.1', 11211) or die ("Could not connect");
    //$suburbs = $memcache->get('suburb_dropdown');
    //  $ages = $memcache->get('ages_list');
    $suburbs = $this->suburbRepo->populateSelect();
    // $heights = $memcache->get('heights_list');
    
    $ages = Age::lists('name','id');
           if (isset($escort->gender_id) && $escort->gender_id  == 3) {
            $bodies = Body::all();
        } else {
            $bodies = Body::where('gender_id', $escort->gender_id)->get();
        }
    $heights = Height::lists('name','id');
  // $suburbs = $this->suburbRepo->populateSelect();

   return View::make('frontend.partials.profile.escort-info',compact('escort','suburbs','ages','heights','bodies','suburbs'));
  // print_r ( $heights );
  }

  /**
   * POST /escortInfo
   *
   * @return Response
   */
  public function postEscortInfo()
  {
    $input = Input::all();
    $file = Input::file('file');
    if ( $file ) {
        $destinationPath = public_path().'/uploads/escorts/';
        $filename = str_random(12).'.jpg';
    
        $file->move($destinationPath, $filename);

        ImageHelper::createThumbnailSize($destinationPath,$filename,220,330);
        ImageHelper::createThumbnailSize($destinationPath,$filename,170,170);
        // ImageHelper::createThumbnailSize($destinationPath,$filename,86,129);

        $escortImage = new EscortImage();
        $escortImage->escort_id = Input::get('escort_id');
        $escortImage->filename = $filename;
        $escortImage->status = 1;
        $image_id = $escortImage->save();
        $pass = json_encode( array(
              'image_id' => $image_id,
              'file_name' => $filename
          ) );
        return Response::make($pass,200);
    }
    else {    
    $escort = Escort::find($input['escort_id']);
    
    if ( $escort ) {

    $escort->escort_name = $input['escort_name'];
    $escort->phone = $input['phone'];
    $escort->email = $input['email'];
    $escort->suburb_id = $input['suburb_id'];
    $escort->licence_number = $input['licence_number'];
    $escort->age_id = $input['age_id'];
    $escort->gender_id = $input['gender_id'];
    $escort->contact_phone = $input['contact_phone'];
    $escort->contact_playmail = $input['contact_playmail'];
    $escort->seo_url = $escort->getSeoUrl();
    $escort->save();

    return Response::make('Escort info updated',200);
  }
    else {
    return Response::make('There was an error saving data, please try again',200);
    }
}
  }

  /**
   * GET /escortAbout
   *
   * @return Response
   */
  public function getEscortAbout()
  {
    // Check if the escort is associated with the user or one of his businesses
    $escort  = $this->checkProfileProperty(Input::get('escort_id'));
    if (!($escort))
      return Response::make('non ho escort',404);

        if ($escort->gender_id == 3) {
            $bodies = Body::all();
        } else {
            $bodies = Body::where('gender_id', $escort->gender_id)->get();
        }
    $heights = Height::lists('name','id');

    return View::make('frontend.partials.profile.escort-about',compact('escort','bodies','heights'));
  }

  /**
   * POST /escortAbout
   *
   * @return void
   */
  public function postEscortAbout()
  {
    $input = Input::all();
    $escort = Escort::find($input['escort_id']);
    $escort->about_me = $input['about_me'];
    $escort->body_id = $input['body_id'];
    $escort->ethnicity_id = $input['ethnicity_id'];
    $escort->eye_color_id = $input['eye_color_id'];
    $escort->hair_color_id = $input['hair_color_id'];
    $escort->does_incalls = $input['does_incalls'];
    $escort->does_outcalls = $input['does_outcalls'];
    $escort->height_id = $input['height_id'];
    $escort->does_travel_internationally = $input['does_travel_internationally'];
    $escort->does_smoke = $input['does_smoke'];
    $escort->tattoos = $input['tattoos'];
    $escort->piercings = $input['piercings'];
    $escort->seo_url = $escort->getSeoUrl();
    $escort->save();

    // Clean the tables before inserting the newly selected services
    EscortService::whereEscortId($input['escort_id'])->delete();
    EscortLanguage::whereEscortId($input['escort_id'])->delete();

    $services = Service::all();
    foreach ($services as $service_id => $service) {
      foreach ($input as $key => $value) {
        if($key == 'service_'.$service_id){
          $escortService = new EscortService();
          $escortService->escort_id = $input['escort_id'];
          $escortService->service_id = $service_id;
          $escortService->save();
        }
      }
    }

    $languages = Language::all();
    foreach ($languages as $language_id => $language) {
      foreach ($input as $key => $value) {
        if($key == 'language_'.$language_id){
          $escortLanguage = new Escortlanguage();
          $escortLanguage->escort_id = $input['escort_id'];
          $escortLanguage->language_id = $language_id;
          $escortLanguage->save();
        }
      }
    }

    // return Response::make('Escort about updated',200);
  }

  /**
   * GET /escortAvailability
   *
   * @return Response
   */
  public function getEscortAvailability()
  {
    // Check if the escort is associated with the user or one of his businesses
    $escort  = $this->checkProfileProperty(Input::get('escort_id'));
    if (!($escort))
      return Response::make('I have no escort',404);

    $suburbs = $this->suburbRepo->populateSelect();


    return View::make('frontend.partials.profile.escort-availability',compact('escort','suburbs'));
  }

  /**
   * POST /escortAvailability
   *
   * @return void
   */
  public function postEscortAvailability()
  {
    $input = Input::all();

    // Availability
    EscortAvailability::whereEscortId($input['escort_id'])->delete();

    // I get input in the form enabled_x
    foreach ($input as $key => $value) {
      if(substr($key, 0, 7) == 'enabled'){
        $day = substr($key,8);
        $escortAvailability = new EscortAvailability();

        $escortAvailability->day = $day;
        $escortAvailability->enabled = 1;
        if (isset($input['24_'.$day])){
          $escortAvailability->twenty_four_hours = 1;
          $escortAvailability->start_hour_id = 0;
          // 0 is am, 1 is pm
          $escortAvailability->start_am_pm = 0;
          $escortAvailability->end_hour_id = 23;
          $escortAvailability->end_am_pm = 1;
        } else {
          $escortAvailability->start_hour_id = $input['start_hour_'.$day];
          $escortAvailability->start_am_pm = $input['start_am_pm_'.$day];
          $escortAvailability->end_hour_id = $input['end_hour_'.$day];
          $escortAvailability->end_am_pm = $input['end_am_pm_'.$day];
        }

        $escortAvailability->escort_id = $input['escort_id'];
        $escortAvailability->save();
      }
    }

    // Rates
    $rates = EscortRate::whereEscortId($input['escort_id'])->first();
    if($rates)
      $escortRates = $rates;
    else
      $escortRates = new EscortRate();

    $escortRates->incall_15_rate = $input['incall_15_rate'];
    $escortRates->incall_30_rate = $input['incall_30_rate'];
    $escortRates->incall_45_rate = $input['incall_45_rate'];
    $escortRates->incall_1h_rate = $input['incall_1h_rate'];
    $escortRates->incall_2h_rate = $input['incall_2h_rate'];
    $escortRates->incall_3h_rate = $input['incall_3h_rate'];
    $escortRates->incall_overnight_rate = $input['incall_overnight_rate'];
    $escortRates->outcall_15_rate = $input['outcall_15_rate'];
    $escortRates->outcall_30_rate = $input['outcall_30_rate'];
    $escortRates->outcall_45_rate = $input['outcall_45_rate'];
    $escortRates->outcall_1h_rate = $input['outcall_1h_rate'];
    $escortRates->outcall_2h_rate = $input['outcall_2h_rate'];
    $escortRates->outcall_3h_rate = $input['outcall_3h_rate'];
    $escortRates->outcall_overnight_rate = $input['outcall_overnight_rate'];
    $escortRates->save();

    $escort = Escort::find($input['escort_id']);
    $escort->notes = $input['notes'];
    if ($input['incall_1h_rate'] <= 100)
      $escort->hourly_rate_id = 1;
    if ($input['incall_1h_rate'] > 100 && $input['incall_1h_rate'] <= 200)
      $escort->hourly_rate_id = 2;
    if ($input['incall_1h_rate'] > 200 && $input['incall_1h_rate'] <= 300)
      $escort->hourly_rate_id = 3;
    if ($input['incall_1h_rate'] > 300 && $input['incall_1h_rate'] <= 400)
      $escort->hourly_rate_id = 4;
    if ($input['incall_1h_rate'] > 400 && $input['incall_1h_rate'] <= 500)
      $escort->hourly_rate_id = 5;
    if ($input['incall_1h_rate'] > 500)
      $escort->hourly_rate_id = 6;
    $escort->save();


    return Response::make('Escort availability updated',200);
  }

  /**
   * GET /addTouring
   *
   * @return Response
   */
  public function addTouring()
  {
    $input = Input::all();
    $touring = new EscortTouring();
    $touring->escort_id = $input['escort_id'];
    $touring->from_date = Helpers::user_date_to_db_date($input['from_date']);
    $touring->to_date   = Helpers::user_date_to_db_date($input['to_date']);
    $touring->suburb_id = $input['suburb_id'];
    $touring->save();

    $suburb_name = Suburb::find($input['suburb_id'])->name;

    return ['touring_id'=>$touring->id,'suburb_name'=>$suburb_name];
  }

  public function deleteTouring()
  {
    EscortTouring::find(Input::get('touringId'))->delete();
  }

  /**
   * GET /escortGallery
   *
   * @return Response
   */
  public function getEscortGallery()
  {
    // Check if the escort is associated with the user or one of his businesses
    $escort  = $this->checkProfileProperty(Input::get('escort_id'));
     if (!($escort))
         return Response::make('non ho escort',404);

    return View::make('frontend.partials.profile.escort-gallery',compact('escort'));    
  }

  /**
   * POST /escortGallery
   *
   * @return void
   */
  public function postEscortGallery()
  {
    $file = Input::file('file');
    $destinationPath = public_path().'/uploads/escorts/';
    $filename = str_random(12).'.jpg';
    
    $file->move($destinationPath, $filename);

    ImageHelper::createThumbnailSize($destinationPath,$filename,220,330);
    ImageHelper::createThumbnailSize($destinationPath,$filename,170,170);
    // ImageHelper::createThumbnailSize($destinationPath,$filename,86,129);

    $escortImage = new EscortImage();
    $escortImage->escort_id = Input::get('escort_id');
    $escortImage->filename = $filename;
    $escortImage->status = 1;
    $escortImage->save();
    return Response::make('Escort gallery updated',200);
  }

  
    public function uploadEscortGallery()
  {
    $file = Input::file('file');
    $destinationPath = public_path().'/uploads/escorts/';
    $filename = str_random(12).'.jpg';
    $file->move($destinationPath, $filename);

    //  ImageHelper::createSliderImage($destinationPath,$filename);
    ImageHelper::createThumbnailSize($destinationPath,$filename,220,330);
    ImageHelper::createThumbnailSize($destinationPath,$filename,170,170);

    unlink($destinationPath.$filename);

    $escortImage = new EscortImage();
    $escortImage->escort_id = Input::get('escort_id');
    $escortImage->filename = $filename;
    $escortImage->status = 1;
    $escortImage->save();

    return Redirect::to('/admin/escorts/gallery/'.Input::get('escort_id'));
  }

  /**
   * GET /escortSite
   *
   * @return Response
   */
  public function getEscortSite()
  {
    // Check if the escort is associated with the user or one of his businesses
    $escort  = $this->checkProfileProperty(Input::get('escort_id'));
    if (!($escort))
      return Response::make('non ho escort',404);

    return View::make('frontend.partials.profile.escort-site',compact('escort'));
  }

  /**
   * POST /escortSite
   *
   * @return void
   */
  public function postEscortSite()
  {
    $input = Input::all();
    $escort = Escort::find($input['escort_id']);

    $escort->use_own_url = $input['use_own_url'];
    $escort->own_url = $input['own_url'];
    $escort->save();

    return Response::make('Escort site updated',200);
  }

  /**
   * GET /escortReviews
   *
   * @return Response
   */
  public function getEscortReviews()
  {
    // Check if the escort is associated with the user or one of his businesses
    $escort  = $this->checkProfileProperty(Input::get('escort_id'));
    if (!($escort))
      return Response::make('non ho escort',404);

    return View::make('frontend.partials.profile.escort-reviews',compact('escort'));
  }

  public function saveUpdate()
  {
    $input = Input::all();
    if ($input['update_id'] != 0)
      $update = EscortUpdate::find($input['update_id']);
    else
      $update = new EscortUpdate();

    $update->message = $input['message'];
    $update->escort_id = $input['escort_id'];
    $update->save();

    return $update->id;
  }

  public function deleteUpdate()
  {
    $update = EscortUpdate::find(Input::get('update_id'));
    $update->delete();
  }

  public function publishReview()
  {
    $review = EscortReview::find(Input::get('review_id'));
    $review->published = 1;
    $review->save();
  }
  public function unpublishReview()
  {
    $review = EscortReview::find(Input::get('review_id'));
    $review->published = 0;
    $review->save();
  }
  public function deleteReview()
  {
    $review = EscortReview::find(Input::get('review_id'));
    $review->delete();
  }
  
  /**
   * GET /businessInfo
   *
   * @return Response
   */
  public function getBusinessInfo()
  {
    $business = Auth::user()->businesses->find(Input::get('business_id'));
    
    $suburbs = $this->suburbRepo->populateSelect();

    return View::make('frontend.partials.profile.business-info',compact('business','suburbs'));
  }

  /**
   * POST /businessInfo
   *
   * @return Response
   */
  public function postBusinessInfo()
  {
    $input = Input::all();

    $business = Business::find($input['business_id']);
    $business->type = $input['type'];
    $business->name = $input['name'];
    $business->phone = $input['phone'];
    $business->email = $input['email'];
    $business->address_line_1 = $input['address_line_1'];
    $business->address_line_2 = $input['address_line_2'];
    $business->suburb_id = $input['suburb_id'];
    $business->save();

    return Response::make('Business info updated',200);
  }

  /**
   * GET /businessLogo
   *
   * @return Response
   */
  public function getBusinessLogo()
  {
    $business = Auth::user()->businesses->find(Input::get('business_id'));

    return View::make('frontend.partials.profile.business-logo',compact('business'));
  }

  /**
   * POST /businessLogo
   *
   * @return void
   */
  public function postBusinessLogo()
  {
    $file = Input::file('file');
    $destinationPath = public_path().'/uploads/businesses/';
    $filename = str_random(12).'.jpg';
    $file->move($destinationPath, $filename);

    ImageHelper::cropBusinessLogo($destinationPath,$filename);

    $business = Business::find(Input::get('business_id'));
    $to_delete = $business->image;
    if ($to_delete != '' && file_exists(public_path().'/uploads/businesses/'.$to_delete))
      unlink(public_path().'/uploads/businesses/'.$to_delete);

    $business->image = $filename;
    $business->save();
    return Response::make('Business logo updated',200);
  }

  /**
   * GET /businessMyEscorts
   *
   * @return Response
   */
  public function getBusinessMyEscorts()
  {
    $business_id = Input::get('business_id');
    $business = Auth::user()->businesses->find($business_id);
    $escort_ids = EscortBusiness::whereBusinessId($business_id)->lists('escort_id');
    $my_escorts = array();
    foreach ($escort_ids as $key => $escort_id)
      $my_escorts[] = Escort::find($escort_id);

    return View::make('frontend.partials.profile.business-my-escorts',compact('business','my_escorts'));
  }

  /**
   * GET /newEscort
   *
   * @return Response
   */
  public function getNewEscort()
  {
    $suburbs = $this->suburbRepo->populateSelect();

    $ages = Age::lists('name','id');
    $heights = Height::lists('name','id');

    return View::make('frontend.partials.profile.new-escort-info',compact('suburbs','ages','heights'));
  }

  /**
   * POST /newEscort
   *
   * @return Response
   */
  public function postNewEscort()
  {
    $input = Input::all();
    $escort = new Escort;

    $escort->escort_name = $input['escort_name'];
    $escort->user_id = Auth::user()->id;
    $escort->status = 1;
    $escort->active = 1;
    $escort->phone = $input['phone'];
    $escort->email = $input['email'];
    $escort->suburb_id = $input['suburb_id'];
    $escort->suburb_id = $input['suburb_id'];
    $escort->licence_number = $input['licence_number'];
    $escort->age_id = $input['age_id'];
    $escort->gender_id = $input['gender_id'];
    $escort->contact_phone = $input['contact_phone'];
    $escort->contact_playmail = $input['contact_playmail'];
    $escort->save();

    $escort->seo_url = $escort->getSeoUrl();
    $escort->save();
    // return $escort->id;

    $escort_rates = new EscortRate;
    $escort_rates->escort_id = $escort->id;
    $escort_rates->save();

    if (!empty($input['business_id'])) {
      // Reset the user_id so that the business user doesn't have many escorts to manage from the main profile
      $escort->under_business = 1;
      $escort->save();

      $escort_business = new EscortBusiness();
      $escort_business->escort_id = $escort->id;
      $escort_business->business_id = $input['business_id'];
      $escort_business->save();
    }

    return $escort->id;
  }

  /**
   * GET /newBusiness
   *
   * @return Response
   */
  public function getNewBusiness()
  {
    $suburbs = $this->suburbRepo->populateSelect();

    return View::make('frontend.partials.profile.new-business-info',compact('suburbs'));
  }

  /**
   * POST /newBusiness
   *
   * @return Response
   */
  public function postNewBusiness()
  {
    $input = Input::all();

    $business = new Business();
    $business->type = $input['type'];
    $business->name = $input['name'];
    $business->phone = $input['phone'];
    $business->email = $input['email'];
    $business->address_line_1 = $input['address_line_1'];
    $business->address_line_2 = $input['address_line_2'];
    $business->suburb_id = $input['suburb_id'];
    $business->status = 1;
    $business->user_id = Auth::user()->id;
    $business->save();

    return $business->id;
  }


  public function getNewProcess()
  {
    return View::make('frontend.user.new-process');
  }
}


















