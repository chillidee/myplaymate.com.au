@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class="deviceWidth" align="center" bgcolor="#eeeeed" border="0" cellpadding="0" cellspacing="0" width="580">
	<tbody>
		<tr>
			<td style="padding: 0;" bgcolor="#ffffff" valign="top"><a href="https://myplaymate.com.au/"><img class="deviceWidth" style="display: block; border-radius: 4px;" src="https://myplaymate.com.au/assets/emails/main-updated-4.png" alt="" border="0" /></a></td>
		</tr>
		<tr>
			<td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
				<table bgcolor="#eeeeed">
					<tbody>
						<tr>
							<td style="padding: 0 10px 10px 0;" valign="middle"><a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href="#">Hi {{$data['full_name']}}, </a></td>
						</tr>
					</tbody>
				</table>
				<p>Your account with My Playmate is currently suspended.</p>
				<p>Please contact our team on <strong>1300 769 766</strong> to discuss.</p>
			</td>
		</tr>
	</tbody>
</table>
<!-- End One Column -->  

@stop