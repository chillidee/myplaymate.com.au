<?php

/**
 * Class Post
 */
class Post extends Eloquent
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $appends = ['img_url', 'url', 'formated_date', 'day', 'month'];

    /**
     * @var array
     */
    public static $rules = [
        'url_key' => 'required|alpha_dash',
    ];

    public static function boot()
    {
        static::saved(function()
        {
            Event::fire('sitemap.generate');
        });

        static::deleted(function()
        {
            Event::fire('sitemap.generate');
        });
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * @return mixed
     */
    public function comments()
    {
        return $this->hasMany('Comment');
    }

    /**
     * @return mixed
     */
    public function commentAvailable()
    {
        return $this->hasMany('Comment')->where('approved', '=', 1);
    }

    /**
     * @return bool|string
     */
    public function getDayAttribute()
    {
        return date('j', strtotime($this->created_at));
    }

    /**
     * @return bool|string
     */
    public function getMonthAttribute()
    {
        return date('M', strtotime($this->created_at));
    }

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return 'blog/'.$this->url_key;
    }

    /**
     * @return string
     */
    public function getImgUrlAttribute()
    {
        return url('assets/frontend/img/home_ads_photo2.jpg');
    }

    /**
     *
     */
    public function getFormatedDateAttribute()
    {
        return date('F j, Y', strtotime($this->created_at));
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOrderByNewest($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('published', true)
            ->where('date_from', '<=', new DateTime('today'))
            ->where('date_to', '>=', new DateTime('today'));
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithAuthor($query)
    {
        return $query->with('user');
    }
}
