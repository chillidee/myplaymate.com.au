@extends('admin.layout')

@section('javascript')

	<script>
		// jQuery(document).ready(function() {
		//     oTable = jQuery('#datatable').DataTable({
		//         "processing": true,
		//         "serverSide": true,
		//         "ajax": "/admin/ajax_notes",
		//         "columns": [
		//             {data: 'id', name: 'id'},
		//             {data: 'post_id', name: 'post_id'},
		//             {data: 'author', name: 'author'},
		//             {data: 'comment', name: 'comment', 'width': '400px'},
		//             {data: 'status', name: 'status'},
		//             {data: 'created_at', name:'created_at'},
	 //            	{data: 'action', name: 'action', orderable: false, searchable: false}
		//         ]
		//     });
		// });
	</script>

@stop

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-file-text-o"></span></div>
    <div class="pagetitle">
        <h1>Dashboard</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
	<div class="row">
		<div class="col-md-12 dashboard-icons">
			<ul class="row">
				<li class="col-md-2 col-md-offset-2">
					<a href="/admin/pending_escorts">
						<img src="/assets/admin/images/approve-escorts-icon.jpg" alt="Approve profiles">
					</a>
				</li>
				<li class="col-md-2">
					<a href="/admin/payments">
						<img src="/assets/admin/images/check-payments-icon.jpg" alt="Check payments">
					</a>
				</li>
				<li class="col-md-2">
					<a href="/admin/escorts">
						<img src="/assets/admin/images/view-profiles-icon.jpg" alt="View/edit escorts">
					</a>
				</li>
				<li class="col-md-2">
					<a href="/admin/businesses">
						<img src="/assets/admin/images/businesses-icon.jpg" alt="">
					</a>
				</li>
			</ul>
		</div>
		<div class="col-md-12 summary">
			<h2>System summary</h2>
			<div class="row">
				<div class="col-md-4">
					<h3>Profiles</h3>
					<ul>
						<li>There are <span class="bold">{{ $escorts_waiting }}</span> escorts waiting for <a href="/admin/pending_escorts">approval</a></li>
						<li>There are <span class="bold">{{ $images_waiting }}</span> new images waiting for <a href="/admin/pending_images">approval</a></li>
						<li>There are <span class="bold">{{ $reviews_waiting }}</span> reviews waiting for <a href="/admin/reviews">approval</a></li>
						<li>There are <span class="bold">{{ $updates_waiting }}</span> sexy updates waiting for <a href="/admin/updates">approval</a></li>
						<li>There are <span class="bold">{{ $updates_waiting }}</span> comments waiting for <a href="/admin/comments">approval</a></li>
					</ul>
					<h3>Payments</h3>
					<ul>
						<li>There are <span class="bold">X</span> profiles that are 7 days overdue</li>
						<li>There are <span class="bold">Y</span> profiles that are due in < 30 days</li>
					</ul>
				</div>
				<div class="col-md-8">
					<h3>Notes</h3>
					<ul>
						<li><span class="date">3/8 10.30am</span> Tami called Chris to say hi</li>
						<li><span class="date">1/8 11.30am</span> Johnny dropped Che's cup</li>
						<li><span class="date">30/7 3.45pm</span> Scotty found 147 dickenses in MPM site</li>
					</ul>
				</div>
			</div>
		
		
		</div>
	</div>
</div>

@stop
{{-- @include('admin.partials.modals') --}}
