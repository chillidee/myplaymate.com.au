<?php

/**
 * Class EscortImage
 */
class EscortLanguage extends Eloquent
{
    protected $table = 'escort_language';

    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [];

    /**
     *
     */
    public function escort()
    {
        $this->belongsTo('Escort');
    }
    public function getList($id)
    {
        $languages = Language::lists('name','id');
        $escortLanguages = EscortLanguage::where('escort_id','=',$this->escort_id )->get();
        if ( !empty( $escortLanguages ) ) { 
           foreach( $escortLanguages as $escortLanguage ) {
             $languages[ $escortLanguage->language_id] = array( 'checked', $languages[ $escortLanguage->language_id] );
           }
       }
        return $languages;
    }
}
