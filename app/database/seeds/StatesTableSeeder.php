<?php

class StatesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('states')->truncate();

        $States = [
            [
                'country_id' => 1,
                'short_name' => 'NSW',
                'long_name' => 'New South Wales / ACT'
            ],

            [
                'country_id' => 1,
                'short_name' => 'QLD',
                'long_name' => 'Queensland'
            ],

            [
                'country_id' => 1,
                'short_name' => 'VIC',
                'long_name' => 'Victoria'
            ],

            // [
            //     'country_id' => 1,
            //     'short_name' => 'ACT',
            //     'long_name' => 'Australian Capital Territory'
            // ],

            [
                'country_id' => 1,
                'short_name' => 'NT',
                'long_name' => 'Northern Territory'
            ],

            [
                'country_id' => 1,
                'short_name' => 'SA',
                'long_name' => 'South Australia'
            ],

            [
                'country_id' => 1,
                'short_name' => 'WA',
                'long_name' => 'Western Australia'
            ],

            [
                'country_id' => 1,
                'short_name' => 'TAS',
                'long_name' => 'Tasmnaia'
            ],
        ];

        // Uncomment the below to run the seeder
        DB::table('states')->insert($States);
    }
}
