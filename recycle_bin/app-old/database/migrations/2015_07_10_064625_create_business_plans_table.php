<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_plans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->float('price_monthly');
			$table->float('price_yearly');
			$table->integer('photos');
			$table->boolean('touring_calendar');
			$table->boolean('feedback_rating');
			$table->boolean('custom_links');
			$table->boolean('support');
			$table->boolean('private_playmail');
			$table->boolean('priority_search');
			$table->boolean('sexy_updates');
			$table->integer('free_trial_days');
			$table->boolean('featured');
			$table->integer('featured_number');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_plans');
	}

}
