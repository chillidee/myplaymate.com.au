@extends('admin.layout')
@section('javascript')
  <script>
    jQuery(document).ready(function() {
        $( '#myform').on('submit',function(e) {
      e.preventDefault();
      data = $( this ).serialize();
      id = $( '#planUserId').val();
      $.post( "/admin/changePlan", data, function( response ) {
           console.log( response );
           $( '#userPlan_' + id ).html( response.name );
           $( '#num_' + id ).html( response.escort_profiles );
           $( '#popupForm').hide();
           $('#overlay').hide();
      })
  })
  $( 'body').on('click','.popupBusinessPlans',function() {
      $( '#popupForm').show();
      $('#overlay').show();
      currentPlan = $( this ).parent('li').data('currentplan');
  //    $( 'input[value=' + currentPlan  + ']').prop('checked', true);
      $( '#planUserId').attr( 'value', $( this ).parent('li').data('id') );
})
$( '#overlay').on('click',function() {
        $( '#overlay').hide();
        $( '#popupForm').hide();
    })
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax_businesses",
            initComplete: function (response) {
                 $( '.delete-profile' ).on( 'click', function() {
                        $( '.sure_delete' ).show();
                        $( '#yes-delete' ).attr("data-id", $( this ).data( 'id' ));
                  });
             },
        lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'suburb_id', name: 'suburb_id'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name:'created_at'  },
                {data: 'current_plan',name: 'current_plan', orderable: false, searchable: false},
                {data: 'profiles',name: 'profiles', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
      oTable.on( 'draw.dt', function () {
    $( '.delete-profile' ).on( 'click', function() {
             $( '.sure_delete' ).show();
             $( '#yes-delete' ).attr("data-id", $( this ).data( 'id' ));
    });
} );
    });
  </script>
<style>
#popupForm{
    position: fixed;
    background: #fff;
    width: 600px;
    z-index: 10000;
    border-radius: 5px;
    border: 1px solid #ccc;
    padding: 50px;
    left: 50%;
    margin-left: -300px;
    top: 20px;
    display:none;
  }
#popupForm select {
    height: 32px;
    background: #fff;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 3px 12px;
}
#popupForm textarea {
    width: 100%;
    height: 158px;
    border: 1px soid #ccc;
    border: 1px solid #ccc;
    border-radius: 3px;
}
#popupForm input {
    width: 32px;
    border-radius: 3px;
    border: 1px solid #ccc;
    height: 52px;
    padding: 0 19px;
  }
#popupForm label {
    font-size: 1.2em;
    font-weight: bold;
    margin: .3em 0;
    color: #384F5D;
    text-transform: none;
    display: block;
    margin: -36px 0 20px 40px;
    line-height: 12px;
    vertical-align: middle;
  }
#popupForm h3{
    color: #AD4949;
    text-transform: uppercase;
    margin-bottom: 20px;
  }
.mpBut, .mpButt {
    float: right;
    margin-top: 35px;
    background-color: #3CAF40;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 8px 48px;
    font-size: 22px;
    margin-bottom: 35px;
  }
.mpButt {
  margin: 0;
  font-size: 18px;
  padding: 2px 18px;
  background-color: #317177;
}
#overlay {
   opacity: .7;
   position:fixed;
   left:0;
   right:0;
   bottom:0;
   top:0;
   width: 100%;
   height: 100%;
   background-color: #000;
   z-index: 9999;
   display:none;
 }
  .sure_delete {
    position: fixed;
    width: 500px;
    height: 197px;
    background-color: #fff;
    top: 200px;
    left: 50%;
    z-index: 10000;
    padding: 50px;
    border-radius: 8px;
    border: 1px solid #222;
    margin-left: -250px;
    font-size: 22px;
    display:none;
}
.detele_buttons {
    background-color: #1F5063;
    border: none;
    box-shadow: none;
    font-size: 20px;
    padding: 7px 22px;
    border-radius: 3px;
    color: #fff;
    border-right: 20px;
    margin-top: 19px;
    margin-right: 30px;
}
  </style>
@stop
@section('content')
<div id ='overlay'></div>
<div id ='popupForm'>
<h3 id ='descriptor'>Change plan</h3>
    <form id ='myform'>
        {{ Form::open(array('url' => 'foo/bar')) }}

        {{ Form::text('user_id','',array('class'=>'hidden','id'=>'planUserId'))}}

        @foreach( $plans as $plan )

        {{ Form::radio('business_plan',$plan->id)}}
        {{ Form::label('business_plan',$plan->name)}} </br>

        @endforeach

        <button id ='submit_newplan' class ='mpBut'>Submit</button>

        {{Form::close()}}
</div>
<div class ='sure_delete'>
    <div>
        Are you sure you want to delete this business?
    </div>
  <button class ='detele_buttons' id ='no-delete'>Cancel</button>
  <button class ='detele_buttons' id ='yes-delete' data-id =''>Delete</button>
</div>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-money"></span></div>
    <div class="pagetitle">
        <h1>All businesses</h1>
    </div>
</div><!--pageheader-->

<div class="row">
  <a href="/admin/businesses/new" class="create-user btn btn-success btn-lg pull-right section-content">Create business</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Location</th>
      <th>Phone</th>
      <th>Email</th>
      <th>Date of creation</th>
      <th>Current Plan</th>
      <th>Profiles</th>
      <th>Action</th>
    </tr>
  </thead>
</table>
<script>
    $( '#no-delete' ).on( 'click', function() {
         $( '.sure_delete' ).hide();
    });
    $( '#yes-delete' ).on( 'click', function() {
         url = "/admin/delete-business/" + $( this ).attr( 'data-id' );
         $.ajax({
                  url: url
               }).done(function(result) {
                       alert( result );
                       $( '.sure_delete' ).hide();
                       $( this ).parents( 'tr.row' ).remove();

               });
    });
  </script>
@stop
