var colors = ['#00a2bf', '#004cbf', '#0900bf', '#5f00bf', '#b500bf', '#bf0072', '#bf001c', '#bf2626', '#bf6b26', '#bfaf26', '#89bf26', '#44bf26'];

window.onload = function() {

    function setupAutoComplete() {

            var input = document.getElementById('postcode');
            var options = {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au"
                },
            }

            var places = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(places, 'place_changed', function() {
                var place = places.getPlace();
                var nameArr = place.formatted_address.split( "," );
                window.cityObj = {
                    name: nameArr[0],
                    latitude: place.geometry.location.lat(),
                    longitude: place.geometry.location.lng()
                }
                window.customPlace = 1;
            });
    }

    if (window.thisCity != '') {
        $('#city a').html(thisCity.charAt(0).toUpperCase() + thisCity.slice(1));
        $('#trigger-overlay').off();
        return false;
    } else {
        if (typeof(Cookies.get('latitude')) == 'undefined' && typeof(window.latitude) == 'undefined' || Cookies.get('suburbName') == 'undefined') {
            var startPos;
            var geoSuccess = function(position) {
                startPos = position;
                Cookies.set('latitude', startPos.coords.latitude);
                window.latitude = startPos.coords.latitude;
                Cookies.set('longitude', startPos.coords.longitude);
                window.longitude = startPos.coords.longitude;
                applyFilters(page);
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + startPos.coords.latitude + ',' + startPos.coords.longitude + '&sensor=true',
                    success: function(response) {

                        for (var i = 0; i < response.results[0].address_components.length; i++) {
                            if (response.results[0].address_components[i].types[0] == 'locality') {
                                window.$cityName = response.results[0].address_components[i];
                            }
                            if (response.results[0].address_components[i].types[0] == "administrative_area_level_1") {
                                window.$stateName = response.results[0].address_components[i].short_name;
                            }
                        }
                        //Fallback in case there is no locality returned by Google
                        if (typeof($cityName) == 'undefined') {
                            window.$cityName = response.results[response.results[0].address_components.length - 2].address_components[0];
                        }
                        if (typeof($stateName) == 'undefined') {
                            window.$stateName = 'NSW';
                        }
                        $('#geoloc #city a').text($cityName.long_name);
                        Cookies.set('suburbName', $cityName.long_name);
                        Cookies.set('stateName', $stateName);
                    },
                    dataType: 'json'
                });
            }
            navigator.geolocation.getCurrentPosition(geoSuccess);
        } else {
            $('#geoloc #city a').text(Cookies.get('suburbName'));
            window.$stateName = Cookies.get('stateName');
            window.$cityName = Cookies.get('suburbName');
        }

        window.customPlace = 0;

        $('#trigger-overlay').click(function(event) {
            $.ajax({
                    url: '/map_modal',
                })
                .done(function(data) {
                    $.ajax({
                            url: '/get_state_and_cities',
                            data: {
                                state_name: window.$stateName 
                            },
                        })
                        .done(function(data) {

                            $('#current-state').text(window.$stateName);
                            $('#mapId-' + window.$stateName).attr('fill', $('#mapId-' + window.$stateName).data('pathcolor'));
                            var theCity = 0;
                            if ( typeof( Cookies.get( 'suburbName') ) != 'undefined' ) {
                                   theCity = Cookies.get( 'suburbName');
                            }
                            $.each(data.cities, function(index, city) {
                                var sel = '';
                                if ( theCity != 0 && theCity == city.name ){
                                      sel = ' selected ';
                                }
                                $('#city-select').append('<option ' + sel + 'value="' + city.id + '">' + city.name + '</option>');
                            });

                        });

                    // Show the modal window
                    $('#geoMap').html(data).modal();
                    setupAutoComplete();
                    ( typeof( Cookies.get( '_distance') ) != 'undefined' ? $( '#_distance' ).val(  Cookies.get( '_distance') ) : $( '#_distance' ).val( 50 ) );
                });
        });
        $('body').on('click', '#geoMap path', function() {
            // Create colored overlay on the clicked state on the map
            window.customPlace = 0;
            var $this = $(this);
            $('path').attr('fill', '#333333');
            $this.attr({
                'fill': colors[$this.index() + 1]
            });

            // Populate select and current-state
            var stateName = $this[0].id.substr(6);
            $.ajax({
                    url: '/get_state_and_cities',
                    data: {
                        state_name: stateName
                    },
                })
                .done(function(data) {
                    var citySelect = $('#city-select');
                    $('#current-state').text(data.state_name);
                    window.selectedState = data.state_name;
                    Cookies.set('stateName', data.state_name);
                    citySelect.find('option').remove();
                    $.each(data.cities, function(index, city) {
                        var option = '<option data-state="' + city.state_id + '" value="' + city.id + '">' + city.name + '</option>';
                        citySelect.append(option);
                    });
                });
        });
        $('body').on('click', '#map-ok', function(e) {

            if (window.customPlace == 0 ) {

                selectedCity = $('#city-select').find('option:selected');
                if (!selectedCity) {
                    alert('You have not selected a city!');
                    return false;
                }
                cityname = selectedCity.html();
                cityid = selectedCity.val();
                $.ajax({
                        url: '/get_cityLatLng',
                        data: {
                            city: cityid
                        },
                    })
                    .done(function(data) {
                        window.cityObj = {
                            name: data.name,
                            latitude: data.latitude,
                            longitude: data.longitude
                        }
                        renewEscorts();
                    })
            }
            else {
                renewEscorts();
            }
         function renewEscorts() {
            $cityName = {};
            Cookies.set('suburbName', cityObj.name);
            window.$cityName.long_name = cityObj.name;
            if (typeof(window.selectedState) != 'undefined') window.$stateName = window.selectedState;
            window.latitude = window.cityObj.latitude;
            window.longitude = window.cityObj.longitude;
            Cookies.set('latitude', window.cityObj.latitude);
            Cookies.set('longitude', window.cityObj.longitude);
            Cookies.set( '_distance', $( '#_distance').val() );
            window._distance = $( '#_distance').val();
            $('#geoloc #city a').text(window.cityObj.name);
            wipeEscorts();
            applyFilters();
            $('#geoMap').modal('hide');
         }

        });

    }

};