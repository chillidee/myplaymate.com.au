<div id="basic-info">
    {{ Form::open(array('id' => 'userInfo')) }}
    <h2>User info</h2>
        <div class="alert alert-success">User info updated!</div>
        <fieldset>
            <div class="control-group">
                {{ Form::label('full_name','Full name',array('class'=>'control-label')) }}<span>*</span>
                <div class="controls">
                    {{ Form::text('full_name', Auth::user()->full_name, array('class' => 'input-xlarge input03','required')) }}
                </div>
            </div>

            <div class="control-group">
                {{ Form::label('email','Email',array('class'=>'control-label')) }}<span>*</span>
                <div class="controls">
                    {{ Form::text('email', Auth::user()->email, array('class' => 'input-xlarge input03','required','data-parsley-type'=>'email')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('phone','Phone',array('class'=>'control-label')) }}<span>*</span>
                <div class="controls">
                    {{ Form::text('phone', Auth::user()->phone, array('class' => 'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)')) }}
                </div>
            </div>
            {{ Form::hidden('user_id',Auth::user()->id) }}
        </fieldset>
        <div style="clear:both; width:100%; text-align:right; padding:20px; padding-right: 0;">
            {{ Form::submit('UPDATE') }}
        </div>
    {{ Form::close() }}
</div>