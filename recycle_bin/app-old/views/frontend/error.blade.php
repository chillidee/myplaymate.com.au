@extends('frontend.layout')

@section('content')

<!-- .boxed version of the page -->
<div class="page-wrapper extra-top">
    <!-- .container start -->
    <div class="container">
        <!-- .row start -->
        <div class="row">
			<h2>Error</h2>
			@if($error)
				<p>{{ $error }}</p>
			@endif


			@if($errors)
				<ul class="errors">
					@foreach($errors as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- End of the Boxed Version Page -->
			
@stop