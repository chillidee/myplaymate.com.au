<?php

class EscortUpdate extends \Eloquent {
	
	protected $fillable = [];

	protected $table = 'escort_updates';

    public function escort()
    {
        return $this->belongsTo('Escort', 'escort_id');
    }

    public function publishingDate()
    {
    	return date_format($this->created_at, 'd/m/Y');
    }
}