<table class="profileTable datesTable" style="text-align:left;" id="touringTable">
                                    <tbody>
                                        <tr>
                                            <th>Payment Id</th>
                                            <th>Payment Date</th>
                                            <th>Period from</th>
                                            <th>Period to</th>
                                            <th>Payment amount</th>
                                            <th>Payment method</th>
                                            <th>View Invoice</th>
                                        </tr>
                                      @if ( count( $payments ) > 0 )
                                        @foreach($payments as $payment)
                                        <tr class="touringEntry" id="touringEntry-{{ $payment->id }}">
                                            <td class="from_date">{{ $payment->id  }}</td>
                                            <td class="from_date">{{ Helpers::db_date_to_user_date($payment->created_at ) }}</td>
                                            <td class="from_date">{{ Helpers::db_date_to_user_date($payment->period_from) }}</td>
                                            <td class="to_date">{{ Helpers::db_date_to_user_date($payment->period_to) }}</td>
                                            <td class="suburb_id">{{$payment->amount}}</td>
                                            <td class="suburb_id">{{$payment->method}}</td>
                                            <td><a class="deleteTouring" id="{{ $payment->id }}"ng-click ='deleteTouring("{{$payment->id}}")'>View</a></td>
                                        </tr>
                                        @endforeach
                                      @endif
                                    </tbody>
                                </table>