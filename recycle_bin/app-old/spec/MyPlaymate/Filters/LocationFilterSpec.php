<?php

namespace spec\MyPlaymate\Filters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use MyPlaymate\Services\LocationService;


class LocationFilterSpec extends ObjectBehavior {

	function let(LocationService $location)
	{
		$this->beConstructedWith($location);
	}

    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Filters\LocationFilter');
    }

    function it_calls_suburb_find_function(LocationService $location)
    {
		$escorts = [['suburb_id' => 1]];
    	$location->calculateDistance(1,1)->shouldBeCalled();

    	$this->apply($escorts, 1);
    }

    function it_excludes_a_too_far_escort(LocationService $location)
    {
		$escorts = [['suburb_id' => 1]];
    	$location->calculateDistance(1,1)->willReturn(100);
		$this->apply($escorts, 1)->shouldHaveCount(0);
    }

    function it_includes_a_near_enough_escort(LocationService $location)
    {
		$escorts = [['suburb_id' => 1]];
    	$location->calculateDistance(1,1)->willReturn(10);
		$this->apply($escorts, 1)->shouldHaveCount(1);
    }
    function it_gets_the_first_escort_and_excludes_the_second(LocationService $location)
    {
		$escorts = [['suburb_id' => 1],['suburb_id' => 2]];
    	$location->calculateDistance(1,1)->willReturn(10);
    	$location->calculateDistance(2,1)->willReturn(50);
		$this->apply($escorts, 1)->shouldReturn([['suburb_id' => 1]]);
    }

    function it_filters_both_the_far_escorts_but_gets_the_near_one(LocationService $location)
    {
		$escorts = [['suburb_id' => 1],['suburb_id' => 2],['suburb_id' => 3]];
    	$location->calculateDistance(1,1)->willReturn(40);
    	$location->calculateDistance(2,1)->willReturn(50);
    	$location->calculateDistance(3,1)->willReturn(10);
		$this->apply($escorts, 1)->shouldHaveCount(1);
    }

    function it_gets_both_the_near_escorts(LocationService $location)
    {
		$escorts = [['suburb_id' => 1],['suburb_id' => 2]];
    	$location->calculateDistance(1,1)->willReturn(4);
    	$location->calculateDistance(2,1)->willReturn(0);
		$this->apply($escorts, 1)->shouldReturn([['suburb_id' => 1],['suburb_id' => 2]]);
    }
    function it_filters_all_the_far_escorts(LocationService $location)
    {
		$escorts = [['suburb_id' => 1],['suburb_id' => 2]];
    	$location->calculateDistance(1,1)->willReturn(40);
    	$location->calculateDistance(2,1)->willReturn(100);
		$this->apply($escorts, 1)->shouldReturn([]);
    }
}








