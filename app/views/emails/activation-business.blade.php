<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>MyPlaymate.com.au - Please Verify Your Account</title>
</head>

<body bgcolor="#000">
	<!-- Visually Hidden Preheader Text: START -->
	<div style="display: none; font-size: 0.05; line-height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; position: absolute; top: -1000em; left: -1000em;">
		Please Verify Your Account Now at MyPlaymate.com.au
	</div>
	<!-- Visually Hidden Preheader Text: END -->
	
<table align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#000" width="680" style="max-width: 680">
	<tr><td>
	<table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#000000" width="100%" style="max-width: 680">
		<tr>
			<td align="center">
				<a href="http://myplaymate.com.au/"><img src="http://myplaymate.com.au/assets/frontend/img/logo.png" width="80%" alt="myplaymate.com.au - Australia's Leading Directory for Escorts, Brothels and Agencies" style="max-width: 680; height: auto"></a>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 10 20 10 20; text-align: center; font-family: 'Helvetican Neue', Helvetica, Arial, sans-serif; font-size: 18; color: #FFF;">
				<h2>Hi {{$data['full_name']}},</h2>
				<p>
					Thank you for registering your business with <a href="https://myplaymate.com.au" style="text-decoration: underline; color: #FFFFFF">MyPlaymate.com.au</a>!<br>
					Verify your account by clicking the following link or the button below<br>
					<font style="font-size: x-small;"><a href="{{Config::get('app.url')}}/{{$data['activation_link']}}" style="color: #8B8B8B; text-decoration: underline">{{Config::get('app.url')}}/{{$data['activation_link']}}</a></font>
				</p>
				<br><br>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center" style="margin: auto;">
					<tr style="text-align: center;">
						<td align="center;">
							<a href="{{Config::get('app.url')}}/{{$data['activation_link']}}" style="background: #FF0004; border: 20px solid #FF0004; padding: 0 10px; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold; transition: ease-in; transition-delay: 100ms; transition-property: all">
								VERIFY ACCOUNT
							</a>
							<br><br>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td bgcolor="#FFFFFF" align="center" width="100%">
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width: 680; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" bgcolor="#FFFFFF">
					<tr style="text-align: center">
						<td colspan="3">
							<h2 style="color: #FF0004; letter-spacing: 15px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
								THE NEXT STEPS
							</h2>
						</td>
					</tr>
					
					<tr style="text-align: center; color: #555555; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
						<td align="center" width= "33.33%" style="padding: 10px" valign="top">
							<img src="http://staging.myplaymate.com.au/assets/emails/Form.png" height="auto" width="150" alt="checklist icon" align="top">
							<h3 style="color: #FF0004">
								UPDATE
							</h3>
							<p>
								Update your business profile after you've verified your account.
							</p>
						</td>
						
						<td align="center" width= "33.33%" style="padding: 10px" valign="top">
							<img src="http://staging.myplaymate.com.au/assets/emails/Credit%20Card.png" height="auto" width="150" alt="checklist icon" align="top">
							<h3 style="color: #FF0004">
								PAYMENT
							</h3>
							<p>
								We'll contact you for payment with debit/credit card.
							</p>
						</td>
						
						<td align="center" width= "33.33%" style="padding: 10px" valign="top">
							<img src="http://staging.myplaymate.com.au/assets/emails/Condom.png" height="auto" width="150" alt="checklist icon" align="top">
							<h3 style="color: #FF0004">
								HAVE FUN
							</h3>
							<p>
								Your account is live. Go out and make some money.
							</p>
						</td>
					</tr>
					
					<tr style="background-color: #000; font-size: x-small; color: #FFF; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
						<td colspan="2" style="padding: 10px">
							Get in touch with us at <a href="mailto:admin@myplaymate.com.au" style="text-decoration: underline; color: #FFFFFF;">admin@myplaymate.com.au</a> or <a href="tel:1300769766" style="text-decoration: underline; color: #FFFFFF;">1300 769 766</a> should you have any issues while signing up.
						</td>
						
						<td align="right" style="padding: 10px">
							<a href="https://twitter.com/MyPlayMateAU" style="text-decoration: none; color: #FFFFFF; font-size: small"><img src="http://staging.myplaymate.com.au/assets/emails/Twitter.png" height="50%" width="auto" alt="twitter icon"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				
	</table>
</td></tr>	
</table>	
</body>
</html>
