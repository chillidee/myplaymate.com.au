@extends('frontend.layout')

@section('content')

<script>window.activeId = {{json_encode( $escort->id )}};
    window.type = 'escort';
    window.escortName = {{json_encode ( $escort->escort_name )}};
</script>

@if ( $escort->preview == 1)
<div id ='iframeDisabler'></div>
@endif

<style>
.escort-about-me-text {
	word-wrap:break-word!important;
}
.escort-services-container-bottom p {
	word-wrap:break-word!important;
}
</style>

<div id ='wishlist-notification'>
  <div class ='mpLoader'></div>
</div>

<div class="container-fluid">
    <div class="row escort-page-row">
        <div class="col-md-12 escort-top-info">
            <div class="escort-top-info-text-container">
                <h2>{{ $escort->escort_name }}</h2>
                <h1 class="keywords">{{ $escort->seo_keywords }}</h1>
                @if ( $escort->age ) <p>{{ $escort->age->name  }} yo</p>@endif
                @if ( $escort->gender ) <p>{{ $escort->gender->name  }} - </p> @endif
                @if ( $escort->suburb_name ) <p>{{ $escort->suburb_name }}</p> @endif
            </div>
        </div>

        <li id="wishlist" class="col-xs-12">

            @if(Auth::check())
            @if( $escort->favourite == 1 )
            <a class="is-wish" id="{{ $escort->id }}"><i class="fa fa-heart">REMOVE FROM FAVOURITES</i></a>
            @else
            <a class="is-not-wish" id="{{ $escort->id }}"><i class="fa fa-heart">ADD TO FAVOURITES</i></a>
            @endif
            @else
            <a href="#login" data-toggle="modal"><i class="fa fa-heart">ADD TO FAVOURITES</i></a>
            @endif
        </li>

        @if (( $escort->banner_image && $escort->banner_approved == 2))

        <div class="col-md-10 col-md-offset-1">
            <div class="escort-top-banner-image">
                <img class="full_width" src="https://d2enq9fr2fmuvm.cloudfront.net/escorts/{{$escort->banner_image}}">
            </div>
        </div>
        @if($escort->contact_playmail || $escort->contact_phone)
        <div class="col-md-12">
            <button type="button" data-toggle="modal" href="#contactEscortModal" class="escort-contact-button">Contact {{ $escort->escort_name }}</button>
        </div>
        @endif
        <div class="col-md-12 escort-grey-background">

            <div class="col-md-10 col-md-offset-1">
                <div class="ratings">
                    <li class="rating-widget">
                        <a href="#leaveReviewModal" data-toggle="modal">
                            <?php
                            $sum = 0;
                            $avg = 0;
                            foreach ($reviews as $review)
                                $sum += $review->rating;

                            if (count($reviews)>0)
                                $avg = floor($sum / count($reviews));

                            for ($i = 1; $i <= 5; $i++) {
                                if ($i <= $avg)
                                    echo '<i class="fa fa-star"></i>';
                                else
                                    echo '<i class="fa fa-star-o"></i>';
                            }
                            ?>
                        </a>
                    </li>
                    <li><a class="add-review" data-toggle="modal" href="#leaveReviewModal">Add Review</a></li>
                </div>
               
				<div class="escort-about-text-container" style="padding-bottom: 20px">
	
					@if ( isset( $tourings ) && count( $tourings ) > 0 )

					<h3>Touring Dates</h3>

					<div class="row-md-12">

						<div class="col-xs-4 col-md-4">
							<p style="padding-bottom: 10px;">LOCATION</p>
							@foreach( $tourings as $tour ) 
							<p style="padding-bottom: 10px;">
								 {{$tour->suburb_name }}
							</p>
							@endforeach
						</div>

						<div class="col-xs-4 col-md-4">
							<p style="padding-bottom: 10px;"><strong> FROM </strong></p>
							@foreach( $tourings as $tour ) 
							<p style="padding-bottom: 10px;">
								 {{date( "jS F Y",strtotime( $tour->from_date ) ) }}
							</p>
							@endforeach
						</div>

						<div class="col-xs-4 col-md-4" style="padding-bottom: 25px;">
							<p style="padding-bottom: 10px;"><strong> TO </strong></p>
							@foreach( $tourings as $tour ) 
							<p style="padding-bottom: 10px;">
								 {{date( "jS F Y",strtotime( $tour->to_date ) )}} 
							</p>
							@endforeach
						</div>
					</div>

					@endif

				</div>
					
                <div class="escort-about-text-container">
                  <div class="escort-info-title rates_title">
                    <p>@if( isset( $escort->about_header ) && $escort->about_header != '' ) {{$escort->about_header}} @else About Me @endif </p>
                  </div>
                    <div class="escort-about-me-text">{{ nl2br ( $escort->about_me )}}</div>
                </div>

            </div>
        </div>

        @else

        <div class="col-md-12">
            <div class="col-md-10 col-md-offset-1">
                <div class="escort-gallery-container ">
                   @if ( $escort->preview == 1 )
                   @include( 'frontend.partials.escortPreviewSlide')
                   @else
                   @include( 'frontend.partials.escortSlide')
                   @endif
               </div>
           </div>
       </div>

       <div class="col-md-12">
        @if($escort->contact_playmail || $escort->contact_phone)
        <button type="button" data-toggle="modal" href="#contactEscortModal" class="escort-contact-button">Contact {{ $escort->escort_name }}</button>
        @endif
    </div>

    @endif


    @if (( $escort->banner_image && $escort->banner_approved == 2))

    <div class="col-md-12">
        <div class="col-md-10 col-md-offset-1">
            <div class="escort-gallery-container">
                @if ( $escort->preview == 1 )
                @include( 'frontend.partials.escortPreviewSlide')
                @else
                @include( 'frontend.partials.escortSlide')
                @endif
            </div>
        </div>
    </div>

    @endif

    <div class="col-md-10 col-md-offset-1 escort-services-container">
        <div class="col-md-9">
            <div class="col-md-12 escort-info-title">
                <p>Info</p>
            </div>
            @if ( $escort->distance != 'NULL')
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p" id="distance-title">DISTANCE</p>
                <p>{{ $escort->distance }} km</p>
            </div>
            @endif
            @if(isset($escort->gender ))
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">GENDER</p>
                <p>{{ $escort->gender->name }}</p>
            </div>
            @endif
            @if(isset($escort->age))
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">AGE</p>
                <p>{{ $escort->age->name }}</p>
            </div>
            @endif
            @if ( $escort->body )
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">BODY TYPE</p>
                <p>{{ $escort->body->name }}</p>
            </div>
            @endif
            @if ( $escort->rates )
            @if ( $escort->rates->first() )
            @if ( $escort->rates->first()->incall_1h_rate != '0.00' )
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">PRICE P/HR</p>
                <p>$ {{ $escort->rates->first()->incall_1h_rate }}</p>
            </div>
            @endif
            @endif
            @endif
            @if( $escort->ethnicity )
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">ETHNICITY</p>
                <p>{{ $escort->ethnicity->name }}</p>
            </div>
            @endif
            @if(isset($escort->eye_color))
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">EYE COLOR</p>
                <p>{{ $escort->eye_color->name }}</p>

            </div>
            @endif
            @if(isset($escort->hair_color))
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">HAIR COLOR</p>
                <p>{{ $escort->hair_color->name }}</p>

            </div>
            @endif
            @if(isset($escort->height) && $escort->height != 0 )
            <div class="col-md-3 col-xs-6">
                <p class="escort-services-title-p">HEIGHT</p>
                <p>{{ $escort->height }}cm</p>
            </div>
            @endif
			
			@if(count($escort->availabilities) > 0)
			<div class="col-md-12 escort-info-title"><br>
                <p>Availability</p>
            </div>
            <?php
            $showTitle = false;
            foreach ($escort->availabilities as $availability) {
                if ($availability->twenty_four_hours == 0)
                    $showTitle = true;
            }
            ?>
            <div class="col-md-10 escort-days-table">
                <div class="col-md-4 col-xs-4">
                    <p class="escort-services-title-p">Available Days and Times</p>
                    @foreach($escort->availabilities as $availability)
                    @if($availability->day == 1 && $availability->enabled == 1)
                    <p>Monday</p>
                    @endif
                    @if($availability->day == 2 && $availability->enabled == 1)
                    <p>Tuesday</p>
                    @endif
                    @if($availability->day == 3 && $availability->enabled == 1)
                    <p>Wednesday</p>
                    @endif
                    @if($availability->day == 4 && $availability->enabled == 1)
                    <p>Thursday</p>
                    @endif
                    @if($availability->day == 5 && $availability->enabled == 1)
                    <p>Friday</p>
                    @endif
                    @if($availability->day == 6 && $availability->enabled == 1)
                    <p>Saturday</p>
                    @endif
                    @if($availability->day == 7 && $availability->enabled == 1)
                    <p>Sunday</p>
                    @endif
                    @endforeach
                </div>
                <div class="col-md-4 col-xs-4">

                    <p class="escort-services-title-p"></p>

                    @foreach($escort->availabilities as $availability)
                    @for($i = 1; $i <= 7; $i++)
                    @if($availability->day == $i && $availability->enabled == 1)
                    @if($availability->twenty_four_hours == 1)
                    <p>24 hours</p>
                    @else
                    <p>
                        {{ Helpers::$dayHours[$availability->start_hour_id] }}
                    </p>
                    @endif
                    @endif
                    @endfor
                    @endforeach
                </div>
                @if($showTitle)
                <div class="col-md-4 col-xs-4">
                    <p class="escort-services-title-p"></p>
                    @foreach($escort->availabilities as $availability)
                    @for($i = 1; $i <= 7; $i++)
                    @if($availability->day == $i && $availability->enabled == 1)
                    @if($availability->twenty_four_hours == 0)
                    <p>
						{{ Helpers::$dayHours[$availability->end_hour_id] }}
					</p>
                    @else
                    <p style="height:22px"></p>
                    @endif
                    @endif
                    @endfor
                    @endforeach
                </div>
                @endif
                @endif
            </div>
        </div>
        <div class="col-md-3 col-xs-12 escort-tags-wrapper">
            <div class="col-md-12 escort-tags-container">
                <div class="escort-tags-image-container">
                    @if($escort->business && $escort->business->image != '')
                    <img src="{{Config::get('constants.CDN')}}/businesses/{{$escort->business->image}}" alt="{{ $escort->business->name }}"/>
                    @else
                    <?php //  <img src="/assets/frontend/img/logo.png" alt="myplaymate"/>?>
                    <p id ='mp_larger'>INDEPENDENT ESCORT</p>
                    @endif
                </div>
                <button type="button" data-toggle="modal" href="#contactEscortModal" class="escort-contact-button">Contact<br> {{ $escort->escort_name }}</button>

                @if($escort->own_url && $escort->own_url != '' )
                <?php if ( substr ( $escort->own_url, 0, 7  ) != 'http://' && substr ( $escort->own_url, 0, 7  ) != 'https://' ) {
                 $escort->own_url = 'http://' . $escort->own_url;
             }?>
             <button type="button" class="escort-contact-button"><a href ='{{$escort->own_url}}' target ="_blank" style ="color: #fff;" onclick ="ga('send','event','Escorts','Private Site Button','{{ $escort->escort_name }}');" >Visit Website</button></a>
             @endif
             @if($escort->seo_content != '')
             <h4>@if( isset( $escort->tag_header ) && $escort->tag_header != '' ) {{$escort->tag_header}} @else Escort Tags @endif </h4>
             <p>{{ $escort->seo_content }}</p>
             @endif
         </div>
     </div>

 </div>
 <div class="col-md-12 escort-grey-background">

    <div class="col-md-10 col-md-offset-1">
        @if (($escort->banner_approved != 2 || !$escort->banner_image))
        <div class="ratings">

            <li class="rating-widget">
                <a href="#leaveReviewModal" data-toggle="modal">
                    <?php
                    $sum = 0;
                    $avg = 0;
                    foreach ($reviews as $review)
                        $sum += $review->rating;

                    if (count($reviews)>0)
                        $avg = floor($sum / count($reviews));

                    for ($i = 1; $i <= 5; $i++) {
                        if ($i <= $avg)
                            echo '<i class="fa fa-star"></i>';
                        else
                            echo '<i class="fa fa-star-o"></i>';
                    }
                    ?>
                </a>
            </li>
            <li><a class="add-review" data-toggle="modal" href="#leaveReviewModal">Add Review</a></li>
        </div>
        
		<div class="escort-about-text-container" style="padding-bottom: 20px">
	
			@if ( isset( $tourings ) && count( $tourings ) > 0 )

			<h3>Touring Dates</h3>

			<div class="row-md-12">

				<div class="col-xs-4 col-md-4">
					<p style="padding-bottom: 10px;">LOCATION</p>
					@foreach( $tourings as $tour ) 
					<p style="padding-bottom: 10px;">
						 {{$tour->suburb_name }}
					</p>
					@endforeach
				</div>

				<div class="col-xs-4 col-md-4">
					<p style="padding-bottom: 10px;"><strong> FROM </strong></p>
					@foreach( $tourings as $tour ) 
					<p style="padding-bottom: 10px;">
						 {{date( "jS F Y",strtotime( $tour->from_date ) ) }}
					</p>
					@endforeach
				</div>

				<div class="col-xs-4 col-md-4" style="padding-bottom: 25px;">
					<p style="padding-bottom: 10px;"><strong> TO </strong></p>
					@foreach( $tourings as $tour ) 
					<p style="padding-bottom: 10px;">
						 {{date( "jS F Y",strtotime( $tour->to_date ) )}} 
					</p>
					@endforeach
				</div>
			</div>

			@endif

		</div>
		
        <div class="escort-info-title rates_title">
              <p>@if( isset( $escort->about_header ) && $escort->about_header != '' ) {{$escort->about_header}} @else About Me @endif </p>
        </div>
        <div class="escort-about-me-text">{{ nl2br ( $escort->about_me )}}</div>
        @endif
    </div>
</div>
<div class="col-md-12 col-xs-12 escort-grey-background">
    <div class="col-md-10 col-md-offset-1 escort-services-container-bottom">
        @if ( $reviews->count() > 0)
        <div class="col-md-12 escort-info-title reviews_container">
            <p>Reviews</p><br>
            @foreach ( $reviews as $review )
            <div class ='rating-widget'>
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    if ($i <= $review->rating)
                        echo '<i class="fa fa-star"></i>';
                    else
                        echo '<i class="fa fa-star-o"></i>';
                }
                ?>
            </div>
            <div class ='reviewDiv'><b>Reviewed by {{ ($review->name != '') ? $review->name : 'Anonymous' }} on {{date( "F jS, Y",strtotime( $review->created_at ) )}}</b><br>
                {{$review->review }}</div>
                <br>
                @endforeach
            </div>
            @endif
            @if ( count ( $escort->services)  > 0)
            <div class="col-md-12 escort-info-title">
                <p>Services</p>
            </div>
            @foreach($escort->services as $service)
            <div class="col-md-3 col-xs-6">
                <p class="ecscort-page-service">{{ $service->name }}</p>
            </div>
            @endforeach
            @endif

            @if ( isset( $rates ) )

            <div class="col-md-12 escort-info-title rates_title">
                <p>Rates</p>
            </div>
            @if ( $escort->does_incalls != 0 )
            <div class ='col-md-6 rates_column'>
               <p class="escort-services-title-p">Incalls</p>
               @if( $rates->incall_15_rate != 0.00 )
               <p><span class ='ratesSpan'>15 Mins:</span> ${{ substr( $rates->incall_15_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->incall_30_rate != 0.00 )
               <p><span class ='ratesSpan'>30 Mins:</span> ${{ substr( $rates->incall_30_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->incall_45_rate != 0.00 )
               <p><span class ='ratesSpan'>45 Mins:</span> ${{ substr( $rates->incall_45_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->incall_1h_rate != 0.00 )
               <p><span class ='ratesSpan'>1 hrs:</span> ${{ substr( $rates->incall_1h_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->incall_2h_rate != 0.00 )
               <p><span class ='ratesSpan'>2 hrs:</span> ${{ substr( $rates->incall_2h_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->incall_3h_rate != 0.00 )
               <p><span class ='ratesSpan'>3 hrs:</span> ${{ substr( $rates->incall_3h_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->incall_overnight_rate != 0.00 )
               <p><span class ='ratesSpan'>Overnight:</span> ${{ substr( $rates->incall_overnight_rate, 0, -3 ) }}</p>
               @endif
           </div>
           @endif

           @if ( $escort->does_outcalls != 0 )
           <div class ='col-md-6 rates_column'>
               <p class="escort-services-title-p">Outcalls</p>
               @if( $rates->outcall_15_rate != 0.00 )
               <p><span class ='ratesSpan'>15 Mins:</span> ${{ substr( $rates->outcall_15_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->outcall_30_rate != 0.00 )
               <p><span class ='ratesSpan'>30 Mins:</span> ${{ substr( $rates->outcall_30_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->outcall_45_rate != 0.00 )
               <p><span class ='ratesSpan'>45 Mins:</span> ${{ substr( $rates->outcall_45_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->outcall_1h_rate != 0.00 )
               <p><span class ='ratesSpan'>1 hr:</span> ${{ substr( $rates->outcall_1h_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->outcall_2h_rate != 0.00 )
               <p><span class ='ratesSpan'>2 hrs:</span> ${{ substr( $rates->outcall_2h_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->outcall_3h_rate != 0.00 )
               <p><span class ='ratesSpan'>3 hrs:</span> ${{ substr( $rates->outcall_3h_rate, 0, -3 ) }}</p>
               @endif
               @if( $rates->outcall_overnight_rate != 0.00 )
               <p><span class ='ratesSpan'>Overnight:</span> ${{ substr( $rates->outcall_overnight_rate, 0, -3 ) }}</p>
               @endif
           </div>
           @endif
           @endif
           @if ( $escort->notes != '' )
           <div class ='col-md-12'><br>
               <p><b>Additional Info:</b>
               </br></br>
               <?php echo nl2br($escort->notes) ?></p>
           </div>
           @endif
       </div>
   </div>







   @include ( 'frontend.partials.escortModal' )

   @stop

   @section('javascript')
</div>
</div>
<script>


  $('#contactEscortForm').parsley();
  $('#leaveReviewForm').parsley();

  jQuery( document ).ready(function($) {


      $('#contactEscortForm').parsley();
      $('#leaveReviewForm').parsley();

      $('.pgwSlideshow').pgwSlideshow();

      if ( $( window ).width() < 768 ){
            $( '.actionPhone').on('click',function() {
                 window.location.href = "tel:{{ $escort->phone }}";
            })
        }

            // If it's not a preview, update the visit counter
            if(window.location.href.indexOf("preview") == -1)
                $.ajax({url: '/escorts/increaseViewCounter/' + {{ $escort->id }} });

            if ($(document).width() < 415) {
                $('.showPhoneButton').text('').off( "click", "**" );
                $('.showPhoneButton').append('<a href="tel:{{ $escort->phone }}">CALL ME</a>');
                $('.playMailButton').text('EMAIL ME');
                $('.privateSiteButton').text('MY SITE');

                $('#distance-title').text('DISTANCE');
            }

            var bottomButtons = $('#mobile-bottom-buttons div'),
            bottomButtonsCount = bottomButtons.length;
            if (bottomButtonsCount == 2)
                bottomButtons.removeClass('col-xs-4').addClass('col-xs-6');
            if (bottomButtonsCount == 1)
                bottomButtons.removeClass('col-xs-4').addClass('col-xs-12');
        });

    </script>
    @stop
