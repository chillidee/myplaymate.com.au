<?php
namespace MyPlaymate\Filters;

use Suburb;
use MyPlaymate\Filters\Filter;
use MyPlaymate\Services\LocationService;

class LocationFilter extends Filter {

	protected $location;

	public $column = 'distance';

	private $max_distance = 20;

	function __construct(LocationService $location)
	{
		$this->location = $location;
	}

	public function apply($escorts,$filterType)
	{
		return array_filter($escorts,function ($escort) use ($filterType)
		{
			$escort['distance'] = $this->location->calculateDistance($escort['suburb_id'],$filterType);
			return $escort['distance'] < $this->max_distance;
		});
	}
}







