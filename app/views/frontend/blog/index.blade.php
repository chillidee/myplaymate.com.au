@extends('frontend.layout')

@section('content')

<!-- #page-title start -->
<section id="page-title" class="page-title-1">
    <div class="container">
        <h1>@if( isset($meta)){{$meta->name}}@else Blog @endif</h1>
    </div><!-- .containr end -->
</section><!-- #page-title end -->

<div class="blog-post">
    <section class="page-wrapper top-space">
        <!-- .container start -->
        <div class="container">
            <!-- .row start -->
            <div class="row">
        <aside class="col-md-3 aside aside-left triggerAnimation animated visible-lg visible-md stickyfloated fadeInLeft" data-animate="fadeInLeft">
          <ul class="aside-widgets">
              @include('frontend.partials.sidebar.left-common-widgets')
          </ul><!-- .aside-widgets end -->
        </aside><!-- .aside.aside-left end -->

                <article class="col-md-9 col-sm-12 col-xs-12">
                    <div id="directory">
                        <ul class="list-view">
                          <?php $i = 0; ?>
                          @foreach($posts as $post)
                             @if ( $i != 0 )
                              <li class="col-md-12">
                                  <div class="escorts-img-container">
                                    @if($post->thumbnail)
                                      <a href="{{ url('blog/'.$post->seo_url) }}">
                        <img src="{{ Config::get('constants.CDN') . '/posts/thumbnails/thumb_170x170_'.$post->thumbnail }}" alt="{{ $post->title }}" class="pic">
                                      </a>
                                    @endif

                                      <div class="info-container"><!-- info-container-->
                                          <div class="figinfo">
                                              <span class="figname" style="width:100%">
                          <a href="{{ url('blog/'.$post->seo_url) }}">
                                                  {{ $post->title }}</span>
                                                </a>
                                              <ul class="figdata">
                                                  <li>{{ Helpers::db_date_to_user_date($post->date_from) }}</li>
                                              </ul>
                                              <div class="figdesc">
                                                  <p>{{ $post->short_content }}</p>
                                              </div>
                                          </div>
                                          <!-- figinfo-->
                                      </div>
                                      <!-- end of info-container-->
                                      <div class="clearfix"></div>
                                      <div class="fig-readmore">
                                          <a href="{{ url('blog/'.$post->seo_url) }}">
                                              <button class="mb15">Read more...</button>
                                          </a>
                                      </div>
                                  </div>
                              </li>
                             @endif
                           <?php $i++; ?>
                          @endforeach

                        </ul>
                    </div>
                    <!-- .triggerAnimation.animated end -->
                </article>
                <!-- .col-md-9 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>
    <!-- End of the Boxed Version Page -->
</div>

@stop
