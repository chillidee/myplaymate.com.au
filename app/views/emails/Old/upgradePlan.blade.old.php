<!-- Wrapper -->
<table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td style="padding-top: 20px;" bgcolor="#ffffff" valign="top" width="100%"><!-- Start Header-->
      <table class="deviceWidth" style="width: 580px;" align="center" border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td bgcolor="#ffffff" width="100%"><!-- Nav -->
            <table class="deviceWidth" align="right" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td class="center" style="font-size: 13px; color: #272727; font-weight: light; text-align: right; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 5px 0px; font-style: italic;"><a href="https://twitter.com/MyPlayMateAU"><img title="Twitter" src="https://myplaymate.com.au/assets/emails/twitter.jpg" alt="Twitter" height="35" border="0" width="35" /></a> <a href="http://instagram.com/myplaymateau"><img title="Instagram" src="https://myplaymate.com.au/assets/emails/instagram.jpg" alt="Instagram" height="35" border="0" width="35" /></a> <a href="https://www.facebook.com/MyPlayMateAU"><img title="Facebook" src="https://myplaymate.com.au/assets/emails/facebook.jpg" alt="Facebook" height="35" border="0" width="35" /></a> <a href="http://www.youtube.com/user/MyPlayMateAU?feature=watch"><img title="You Tube" src="https://myplaymate.com.au/assets/emails/youtube.jpg" alt="You Tube" height="35" border="0" width="35" /></a></td>
                </tr>
              </tbody>
            </table>
            <!-- End Nav --></td>
          </tr>
        </tbody>
      </table>
      <!-- End Header --> <!-- One Column -->
      <table class="deviceWidth" style="width: 580px;" align="center" bgcolor="#eeeeed" border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td style="padding: 0;" bgcolor="#ffffff" valign="top"><a href="https://myplaymate.com.au/"><img class="deviceWidth" style="display: block; border-radius: 4px;" src="https://myplaymate.com.au/assets/emails/main-updated-4.png" alt="" border="0" /></a></td>
          </tr>
          <tr>
            <td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
              <table bgcolor="#eeeeed">
                <tbody>
                  <tr>
                    <td style="padding: 0 10px 10px 0;" valign="middle"><a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href="#">Hi Admin, A user has requested a plan upgrade on Myplaymate.</a></td>
                  </tr>
                </tbody>
              </table>
              UserName: {{ $data['name'] }}<br>
              Email: {{ $data['email'] }}<br>
              Phone: {{ $data['phone'] }}<br>
              Requested Plan: {{ $data['business_plan'] }}<br>
              Current Plan: {{ $data['current_plan'] }}<br>

              <p>The MyPlaymate Team</p>
            </td>
          </tr>
        </tbody>
      </table>
      <!-- End One Column -->
      <div style="height: 15px;">&nbsp;</div>
      <!-- spacer --> <!-- 4 Columns -->
      <!-- End 4 Columns --></td>
    </tr>
  </tbody>
</table>
<!-- End Wrapper -->
<p></p>