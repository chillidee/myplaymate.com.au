@extends('admin.layout')
@section('javascript')
  <script>
     $.ajax({
       url: "/admin/ajax_categories",
               }).done(function(result) {
                  console.log( result );
                  });
     jQuery(document).ready(function() {
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax_categories",
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'created_at', name:'created_at'  },
                {data: 'updated_at', name:'updated_at'  },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
  </script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-newspaper-o"></span></div>
    <div class="pagetitle">
        <h1>Categories</h1>
    </div>
</div><!--pageheader-->

<div class="row">
  <a href="/admin/blog/category/new" class="create-user btn btn-success btn-lg pull-right section-content">Create Category</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Created</th>
      <th>Edited</th>
      <th>Actions</th>
    </tr>
  </thead>
</table>

@stop

