{{ Form::open(['id'=>'businessInfo']) }}
    @if($business->image != '')
        <img class="section-content" src="/uploads/businesses/{{ $business->image }}" alt="{{ $business->name }}">
    @endif


    <div class="alignc">
        <h2>MY ESCORTS</h2>
        <p class = "profile_info no-line-padding">Manage all your team with ease!</p> 
    </div>
    <div class="section-content alignc">
        <a href="" id="business-add-escort" class="submit-button section-content">Add escort</a>
    </div>
    <ul class="row section content">
        @if(count($my_escorts))
            @foreach($my_escorts as $key => $escort)
            <div class ='escort_holder'>
                  @if($escort->main_image_id > 0)
                    <div class ='image_cutter'>
                        <img class="img-profile" src="{{ $escort->mainImage->thumbnail(220,330) }}" alt="{{ $escort->escort_name }}">
                   </div>
                        @else
                  <div class ='image_cutter'>
                        <img class="img-profile" src="/assets/frontend/img/profile_template.jpg" style="width:86px; height:129px;">
                  </div>
                        @endif
                            </a>
                      <div id ='right_stats_holder'>
                           <div class ='escort_stats_div'>{{ $escort->count_views }} Views</div>
                           <div class ='escort_stats_div'>{{ $escort->count_phone }} Phone Views</div>
                           <div class ='escort_stats_div'>{{ $escort->count_playmail }} Playmails</div>
                      </div>
                    <div class="escort-sumup">
                         <div class="info-container">
                           <a href = "/escorts/preview/{{ $escort->id }}"><span class="escort_name">{{ $escort->escort_name }}</span></a><span class ='escort_status'>{{ Helpers::get_status($escort->status) }}</span>
                                  <p class ='escort_description'>{{ $escort->about_me }}</p>
                                  <div class ='escort_short_links'>
                                          <a data-escort-id="{{ $escort->id }}" class="btn submit-button edit-escort" href="">Edit</a>
                                          <a class="btn submit-button" href="">Bump up</a>
                                          <a class="btn submit-button" href="{{ url('/escorts/').'/preview/'.$escort->id }}">Profile preview</a>
                                  </div>

                            </div><!-- end of info-container-->          
            </div>
                          </div>
            @endforeach
        @else
            <p class="alignc section-content italic">There are currently no escorts associated with your business.</p>
        @endif    
    </ul><!-- .row end -->
    
{{ Form::close() }}
<script>
    (function($){
        $('.second-level h2').text("{{ $business->name }}");

        $('body').on('click', '#business-add-escort', function(e) {
            e.preventDefault();
            businessLevel.slideUp(animTime);
            deactivateEscortButtons();
            refreshForm('newEscort','escort','',{{ $business->id }});
        });
    })(jQuery);
</script>