<?php
namespace MyPlaymate\Interfaces;

interface FilterableInterface
{
	public function apply($escorts,$type);
}