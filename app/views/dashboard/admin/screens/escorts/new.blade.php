  <div id ='overlayBackGround'></div>
  <div class ='_MpoffWhite businessProfileScreen'>
   <div>
     <h2>New Escort</h2>
   </div>
   <div class ='_mpProfile_holder'>
     <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
       <h3>BASIC INFORMATION</h3>
       <p>Fill in basic information for your escort profile below.</p>

       <form role="form" class ='profileForm' controller ='busSaveNew' ng-submit = 'adminNewEscort()' id = 'escortInfo' ng-model = 'newEscort'>
        <div class ='mp_image_float' >
          <div id="container_image">
            <img src ='/assets/dashboard/images/escortHolderImage.jpg' class ='full_width'>
          </div>
        </div>
        <div class ='right_float'>

          {{ Form::text('escort_name','',array('id' => 'escort_name', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Escort Name','ng-model'=>'newEscort.escort_name')) }}

          {{ Form::text('phone','',array('ng-model'=>'newEscort.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'escort Phone')) }}
          
          {{ Form::text('email','',array('id' => 'email', 'class'=>'escort-email input-xlarge input03','placeholder'=>'Escort Email','ng-model'=>'newEscort.email')) }}
        </div>
        <div class ='clear'>  
          <label>User Id - Leave at 1 for an admin account</label><br>
          {{ Form::text('user_id','1',array('ng-init'=>"newEscort.user_id = '1'",'ng-model'=>'newEscort.user_id','placeholder' => 'User id','class'=>'input-xlarge input03')) }}                
          <label id ='escort_age' class ='form_required'>Age:</label>{{ Form::select('age_id',$ages,'',array('id' =>'age_id','class'=>'form-control','ng-model'=>'newEscort.age_id')) }}
          <div class="control-group" id="sex">
            <label class ='form_required left_margin_15'>Gender</label>
            {{ Form::radio('newEscort.gender_id',1,'',array('ng-model' => 'newEscort.gender_id')) }}
            {{ Form::label('gender_id-1','Female') }}
            {{ Form::radio('newEscort.gender_id',2 ,'',array('ng-model' => 'newEscort.gender_id'))}}
            {{ Form::label('gender_id-2','Male') }}
            {{ Form::radio('newEscort.gender_id',3,'',array('ng-model' => 'newEscort.gender_id')) }}
            {{ Form::label('gender_id-3','Transexual') }}
          </div>
        </div><br>

        <div class="alignc">
          {{ Form::submit('SAVE',array('class'=>'submit-button')) }}
        </div>
        