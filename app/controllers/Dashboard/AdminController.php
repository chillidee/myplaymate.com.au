<?php

namespace Dashboard;

use Controller, View, Helpers, Datatables, Input, Business, Auth, Escort, User, Age, Height, Ethnicity, EyeColor, Body, HairColor, Service, EscortLanguage, EscortService, EscortAvailability, EscortRate, EscortImage, EscortReview, EscortUpdate;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;
  
 class AdminController extends \ProfileController
  //class BusinessController extends Controller 
  {
  public function profile($id, $type ) {
              if ( $type == 'user') {
                 $arr = array( 'busMenu' => 
                     array( array( 'item' => 'Favouries', 'id' => '_MpFavourites', 'title' => 'Your Favouries'),
                            array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings')
                     )
              );

              }
              else {
                  $active = ( $type =='escort' ? Escort::where( 'id','=', $id )->first() : Business::where( 'id','=', $id )->first());
                  $profileSels = ( $active ? $active->getProfileSelections() : '' );
                  $arr = array( 'busMenu' => 
                     array( array( 'item' => 'Overview', 'id' => '_MpOverview','title' => ucfirst( $type ) . ' Overview' ),
                            array( 'item' => 'Profile', 'id' => '_MpProfile', 'title' => ucfirst( $type ) . ' Profile'. ' Profile','submenu' => $profileSels )
                     )
              );
              if ( $type =='business' ) {
                    $escorts = $active->getJsonEscorts();
                    $arr['busMenu'][] = array( 'item' => 'Escorts', 'id' => '_MpEscorts', 'title' => 'Your Escorts', 'submenu' => $escorts );
              if ( $active && $active->hasReviews() )
                    $arr['busMenu'][]  = array( 'item' => 'Reviews', 'id' => '_MpReviews', 'title' => 'Manage your reviews','submenu'=> $active->getEscortsWithReviewsAjax() );
              }
              $arr['busMenu'][] = array( 'item' => 'Payments', 'id' => '_MpPayments','title' => 'Billing and Payments' );
             //  $arr['busMenu'][]  = array( 'item' => 'Playmail', 'id' => '_MpPlaymail','title' => 'Manage your emails' );
              $arr['busMenu'][]  = array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings');
            }

                
      $json = json_encode( $arr );
    return 'angular.callbacks._0(' . $json . ');';
    }
  public function businessMobileMenu($id,$active_id, $type){
              if ( $type == 'user') {
                $arr = array( 'menu' => 
                     array( array( 'item' => 'Favourites', 'id' => '_MpFavourites','title' => 'Your Favourites' ),
                            array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Your Account')
                     )
              );

              }
              else {
              $user = User::where( 'id', '=', $id )->first();
              $el = Input::get('el');
              $businesses = $user->businesses;
              $active = ( $type =='escort' ? Escort::where( 'id','=', $active_id )->first() : Business::where( 'id','=', $active_id )->first());
              $arr = array( 'menu' => 
                     array( array( 'item' => 'Overview', 'id' => '_MpOverview','title' => ucfirst( $type ) . ' Overview' ),
                            array( 'item' => 'Profile', 'id' => '_MpProfile', 'title' => ucfirst( $type ) . ' Profile','submenu' => $active->getProfileSelections() )
                     )
              );
              if ( count( $businesses) > 0 ){
                            $escorts = $active->getJsonEscorts();
                            $arr['menu'][] = array( 'item' => 'Escorts', 'id' => '_MpEscorts', 'title' => 'Your Escorts', 'submenu' => $escorts );
                           // $arr['menu'][]  = array( 'item' => 'Playmail', 'id' => '_MpPlaymail','title' => 'Manage your emails' );
                            if ( $active && $active->hasReviews() )
                                $arr['menu'][]  = array( 'item' => 'Reviews', 'id' => '_MpReviews', 'title' => 'Manage your reviews','submenu'=> $active->getEscortsWithReviewsAjax() );
              }
              $arr['menu'][] = array( 'item' => 'Payments', 'id' => '_MpPayments','title' => 'Billing and Payments' );
              $arr['menu'][]  = array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings');
          }
                
      $json = json_encode( $arr );
    return $json;
  }
   /* public function detail($id) {
        $array = array( 'details' => array('hi che') );
        $ret = json_encode( $array );
        return 'angular.callbacks._0(' . $ret . ');';
    }*/
       public function panel($id) {
        $input = Input::get();
        $business = Business::where( 'id','=',$input['busid'] )->first();
         switch ( $input['panel']) {
             case '_MpOverview':

             return '<h4 class ="panelHeader accountPanelHeader">Welcome</h4>';
        break;
    case '_MpProfile':
            $businesses = Business::get();
             return View::make('dashboard.admin.panels.businesses', compact('businesses'));
        break;
         case '_MpEscorts':
             $escorts = Escort::get();
             return View::make('dashboard.admin.panels.escorts', compact('escorts'));
        break;
        case '_MpAccount':
              return '<h4 class="panelHeader accountPanelHeader">Manage Your Account</h4>';
        break;
        case '_MpPayments':
               $approvedEscorts = Escort::where( 'status','=','2')->get();
              $approvedBusinesses = Business::where( 'status','=','2')->get();
              $users = array();
              foreach ( $approvedEscorts as $escort ) {
                  $user = User::where('id','=',$escort->user_id)->first();
                  $users[$escort->user_id]['escorts'][] = $escort;
                  $users[$escort->user_id]['details'] = $user;
                  $users[$escort->user_id]['payments'] = $user->payments()->get();
              }
              foreach ( $approvedBusinesses as $business ) {
                  $user = User::where('id','=',$escort->user_id)->first();
                  $users[$business->user_id]['businesses'][] = $business;
                  $users[$business->user_id]['details'] = $user;
                  $users[$escort->user_id]['payments'] = $user->payments()->get();
              }
              return View::make('dashboard.admin.panels.payments', compact('users'));
        break;
}
         return View::make('dashboard.panel', compact('panel'));
    }
   public function screen($id) {
        $input = Input::get();
        $business = Business::where( 'id','=',$input['busid'] )->first();
         switch ( $input['panel']) {
        case '_MpOverview':
            return $this->overviewScreen($business);
        break;
        case '_MpProfile':
             return View::make('dashboard.admin.screens.business' );
        break;
        case '_MpEscorts':
              return View::make('dashboard.business.screens.escorts' );
        break;
         case '_MpReviews':
              $reviews = $business->escortReviews();
              return View::make('dashboard.business.screens.reviews', compact( 'reviews' ));
        break;
        case '_MpAccount':
              $user = Auth::user();
              return View::make('dashboard.business.screens.account', compact( 'user' ));
        break;
         case '_MpPayments':
             
              return View::make('dashboard.admin.screens.payments', compact( 'users' ));
        break;
}

    }
    public function newEscort() {
        $ages = Age::lists('name','id');
        $user = $user = Auth::user();
        $user_id = $user->id;
        return View::make('dashboard.admin.screens.escorts.new',compact( 'ages','user_id') );
    }

  public function profileScreen($business) {
       if ( $business )
           return View::make('dashboard.business.screens.profile', compact('business','suburbs'));
         $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
         $user = $user = Auth::user();
         $user_id = $user->id;
         return View::make('dashboard.admin.screens.business.new',compact('type','user_id'));
    }

  public function overviewScreen($business) {
          $businesses_waiting = Business::whereStatus(1)->count();
          $escorts_waiting = Escort::whereStatus(1)->count();
          $pending_business_banners = Business::where('banner_image','!=', '')->where('banner_approved','=','1')->count(); 
          $pending_escort_banners = Escort::where('banner_image','!=', '')->where('banner_approved','=','1')->count(); 
          $pending_escort_images = EscortImage::whereStatus(1)->count();
          $images_waiting = $pending_business_banners + $pending_escort_banners + $pending_escort_images; 
          $reviews_waiting = EscortReview::whereStatus(1)->count();
          $updates_waiting = EscortUpdate::whereStatus(1)->count();
          return View::make('dashboard.admin.screens.overview', compact('businesses_waiting','escorts_waiting','images_waiting','reviews_waiting','updates_waiting'));
    }
    public function escortScreen($id) {
      if ( $id == 'new'){
          $ages = Age::lists('name','id');
          $user = $user = Auth::user();
          $user_id = $user->id;
          return View::make('dashboard.admin.screens.escorts.new',compact( 'ages','user_id' ));
      }
          $user = Auth::user();
          $type = $user->getUserType();
          if ( !$type == 1 && $user->id != $id )
            die();
          $escort = Escort::where('id','=',$id)->first();
          switch ($escort['status']) {
                  case '0':
                      $status = 'Pending';     
                   break;
                   case '1':
                      $status = 'Pending';     
                   break;
                   case '2':
                      $status = 'Approved';
                   break;
                   case '3':
                      $status = 'Disapproved';
                   case '4':
                      $status = 'Unavailable';
                   break;
                   case '5':
                      $status = 'Pending Payment';
                   break;
                   default :
                      $status = 'Pending';
                   break;
                 }
          $pass = array(
            'escort' => $escort, 'status' => $status );
          return View::make('dashboard.admin.screens.escorts.layout',$pass );
    }
     public function businessScreen($id) {
      $user = Auth::user();
      if ( $id == 'New'){
          $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
          $user_id = $user->id;
          return View::make('dashboard.admin.screens.business.new',compact( 'type','user_id' ));
      }
          $type = $user->getUserType();
          if ( !$type == 1 && $user->id != $id )
            die();
          $business = Business::where('id','=',$id)->first();
          $pass = array(
            'business' => $business );
          return View::make('dashboard.admin.screens.business.layout',$pass );
    }
    public function profileTab($tab) {
        $business = new Business;
        $user = Auth::user();
        switch ( $tab ) {
          case "View":
               return View::make('dashboard.business.screens.profile.view');
          break;
          case "New":
                $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
                $user_id = $user->id;
                return View::make('dashboard.admin.screens.business.new', compact('id','type','user_id'));
          break;
          case "Edit":
                $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
                $user_id = $user->id;
                return View::make('dashboard.admin.screens.business.edit', compact('business','type','user_id'));
          break;
          case "Bumpup":
                return 'Bump up';
          break;
        }

      }
    public function lowerEscortScreen($id) {
        $user = Auth::user();
        $type = $user->getUserType();
        if ( !$type == 1 && $user->id != $id )
            die();
        $action = Input::get('action');
        $escort = Escort::where('id','=',$id)->first();
        if ( !$escort ) $escort = Escort::where('id','>','0')->first();
        $business = Business::where('id','=',$escort->business_id)->first();
        $pass = array(
            'escort' => $escort );
       if ( $action == 'Edit'){
                $main_image = $escort->getMainImage();
                $ages = Age::lists('name','id');
                if ( isset( $business ) && $business->type != 2 ){
                    $phone = $business->phone;
                    $email = $business->email;
                    return View::make('dashboard.admin.screens.escorts.other.edit', compact('escort','main_image', 'escortRates','ages','phone','email'));
                  }
                $suburb = $escort->getDefaultLocation();
                $heights = Height::lists('name','id');
                $ethnicities = Ethnicity::lists('name','id');
                $eyeColors = EyeColor::lists('name','id');
                $hairColors = HairColor::lists('name','id');
                $services = EscortService::where('escort_id','=',$id)->first();
                if ( empty($services )) $services = new EscortService;
                $services = $services->getList($id);
                $languages = EscortLanguage::where('escort_id','=',$id)->first();
                if ( empty( $languages )) $languages = new EscortLanguage;
                $languages = $languages->getList($id);
                $availabilites = $this->getAvailabilites( $id );
                $escortRates = $escort->getRates($escort->id);
                $inCallList = $this->inCallList($escort->id);
                $outCallList = $this->outCallList($escort->id);
                 if (isset($escort->gender_id) && $escort->gender_id  == 3 || $escort->gender_id  == 0 ) {
                     $bodies = Body::lists('name','id');
                 } else {
                     $bodies = Body::where('gender_id', $escort->gender_id)->get()->lists('name','id');
                 }      
                return View::make('dashboard.admin.screens.escorts.agency.edit', compact('escort','main_image', 'escortRates', 'inCallList','outCallList' ,'availabilites','ages', 'suburb','bodies','heights','ethnicities','languages','services','eyeColors','hairColors'));
        }
        elseif ( $action == 'new' )
                $pass['ages'] = $ages = Age::lists('name','id');
                return View::make('dashboard.admin.screens.escorts.' . $action, $pass );
              }

     public function lowerBusinessScreen($id) {
        $user = Auth::user();
        $type = $user->getUserType();
        if ( !$type == 1 && $user->id != $id )
            die();
        $action = Input::get('action');
        $business = Business::where('id','=',$id)->first();
        return $this->profileScreen($business);

    }
     public function getAvailabilites($id) {
      $escortAvailbities = EscortAvailability::where( 'escort_id','=',$id )->get();
      $sorted = array(
           'escortDays' => array(),
           'twenty_four_hours' => array()
           );
      $daysSelect =  array(
        "1" => "Mon",
        "2" => "Tue",
        "3" => "Wed",
        "4" => "Thu",
        "5" => "Fri",
        "6" => "Sat",
        "7" => "Sun"
        );
      if ( $escortAvailbities->isEmpty() ){
          foreach( $daysSelect as $key => $day ){
            $sorted['days'][$key] = array(
              'handle' => $day,
              'day'    => $key
              );
          }
      }
      else {
          foreach ( $escortAvailbities as $key => $av ) {
            $sorted['days'][$av->day] = $av['original'];
            $sorted['days'][$av->day]['handle'] = $daysSelect[$av->day];
              if ( $av['enabled'] == 1 )
                $sorted['escortDays'][$av->day] = $av->day;
              if ( $av['twenty_four_hours'] == 1 )
                $sorted['twenty_four_hours'][]  = $av->day;
          }
      }
      $sorted['time'] = array(
         "0" =>  "12:00 AM",
         "1" =>  " 1:00 AM",
         "2" =>  " 2:00 AM",
         "3" =>  " 3:00 AM",
         "4" =>  " 4:00 AM",
         "5" =>  " 5:00 AM",
         "6" =>  " 6:00 AM",
         "7" =>  " 7:00 AM",
         "8" =>  " 8:00 AM",
         "9" =>  " 9:00 AM",
         "10" => "10:00 AM",
         "11" => "11:00 AM",
         "12" => "12:00 PM",
         "13" => " 1:00 PM",
         "14" => " 2:00 PM",
         "15" => " 3:00 PM",
         "16" => " 4:00 PM",
         "17" => " 5:00 PM",
         "18" => " 6:00 PM",
         "19" => " 7:00 PM",
         "20" => " 8:00 PM",
         "21" => " 9:00 PM",
         "22" => "10:00 PM",
         "23" => "11:00 PM",
        );
      $sorted['am'] = array(
        "0" => 'am',
        "1" => 'pm');
      
      return $sorted;
    }
    public function outCallList() {
      $ratesList = array(
        'outcall_15_rate' => '15 Min',
        'outcall_30_rate' => '30 Min',
        'outcall_45_rate' => '45 Min',
        'outcall_1h_rate' => '1 hour',
        'outcall_2h_rate' => '2 hour',
        'outcall_3h_rate' => '3 hour',
        'outcall_overnight_rate' => 'Overnight'
         );
      return $ratesList;
    }
      public function inCallList() {
      $ratesList = array(
        'incall_15_rate' => '15 Min',
        'incall_30_rate' => '30 Min',
        'incall_45_rate' => '45 Min',
        'incall_1h_rate' => '1 hour',
        'incall_2h_rate' => '2 hour',
        'incall_3h_rate' => '3 hour',
        'incall_overnight_rate' => 'Overnight'
         );
      return $ratesList;
    }
    public function getRates($id){
       $escort = Escort::where( 'id','=',$id)->first();
       $escortRates = $escort->getRates($escort->id);
       return json_encode ( $escortRates );
    }
      public function getImages($id){
       $escort = Escort::where( 'id','=',$id)->first();
       $images = $escort->images()->get();
       return json_encode ( $images );
    }
    public function reviewScreen($id){
      $escort = Escort::where( 'id','=',$id)->first();
      $reviews = $escort->Reviews()->get();
      return View::make('dashboard.business.screens.reviews.view', compact( 'reviews','escort') );
    }
}
