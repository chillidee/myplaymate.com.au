<div id ='overlayBackGround'></div>
<div class ='_MpoffWhite businessProfileScreen'>
    <div>
        <div class ='mobile_only' id ='navigateThroughProfile'>Show Profile Menu</div>
        <div class ='mPProfileTopMenu'>
            <div><div id ='mpBasicInfo' class ='active' ng-click ="navigateProfile()"></div><span>ACCOUNT</span></div>
            <div><div id ='mpProfileDetails'ng-click ="getHeight()"></div><span ng-click ="getHeight()">PROFILE </span></div>
            <div><div id ='mpAvailability'ng-click ='navigateProfile()'></div><span ng-click ='navigateProfile()'>DETAILS</span></div>
            <div><div id ='mpGallery'ng-click ='getImages()'></div><span ng-click ='getImages()'>GALLERY</span></div>
            <div><div id ='mpPlans'></div><span>PLANS</span></div>
            <div><div id ='mpTouring'></div><span id ='desc_mpTouring'>TOURING</span></div>			            
        </div>
    </div>
    <div class ='_mpProfile_holder escort_profile_form'>
        <message></message>
        <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
            <h3>ACCOUNT</h3>
            <p class ='text-left'>Please fill in your details below. Please note that information provided here will be available publicly on our site.</p>			
			<form role="form" class ='profileForm' controller ='busSaveBasic' ng-submit = 'saveEscortInfo()' id = 'escortInfo' ng-model = 'basicInfoForm'>
                <div class ='mp_image_float' style="display:none!important;">
                    <div id="container_image">
                        <img ng-src="{{Config::get('constants.CDN')}}/escorts/@{{main_image_src}}" alt="Your Profile Image" />
                    </div>
                </div>
                <div>
					<div class="col-md-6">			
						<label>Name</label>
						{{ Form::text('escort_name','',array('id' => 'escort_name', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Escort Name','ng-model'=>'basicInfoForm.escort_name', 'disabled')) }}
						<label>Phone</label>
						{{ Form::text('phone','',array('ng-model'=>'basicInfoForm.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'escort Phone')) }}
						<label>E-mail</label>
						{{ Form::text('email','',array('ng-model'=>'basicInfoForm.email','id' => 'email','class'=>'input-xlarge input03','required','email','placeholder'=>'escort E-mail')) }}

						{{ Form::text('suburb_name','',array('id' => 'suburb_name', 'class'=>'hidden', 'ng-model'=>'basicInfoForm.suburb_name')) }}
						<span class ='info'>Your current location is: @{{basicInfoForm.suburb_name}}</span>
						<autocomplete formmodel ="basicInfoForm"></autocomplete>
					</div>
					<div class="col-md-6">
						<div class="age-container">							
							<label style="display:block">Age</label>
							{{ Form::select('age_id',$ages, '',array('id' =>'age_id','class'=>'form-control', 'required', 'ng-model'=>'basicInfoForm.age_id')) }}							
						</div>						
						<div class="clear control-group" id="sex">
							<div class ='clear'>
								</br>
								<label class ='form_required' id='label_gender'>Gender</label>
							</div>
							<div class="sex-group">
								<div style="margin-top:5px;">
									{{ Form::radio('gender_id',1,'' == 1 ? 1 : 0,array('id'=>'gender_id-1','ng-model'=>'basicInfoForm.gender_id')) }}
									{{ Form::label('gender_id-1','Female') }}
								</div>
								<div>
									{{ Form::radio('gender_id',2,'' == 2 ? 1 : 0,array('id'=>'gender_id-2','ng-model'=>'basicInfoForm.gender_id')) }}
									{{ Form::label('gender_id-2','Male') }}
								</div>
								<div>
									{{ Form::radio('gender_id',3,'' == 3 ? 1 : 0,array('id'=>'gender_id-3','data-parsley-required','ng-model'=>'basicInfoForm.gender_id')) }}
									{{ Form::label('gender_id-3','Transexual') }}
								</div>
							</div>
						</div>
						<div id ='contact_methods'>  
							<div id='contact_methods_playmail'>
								<div class ='clear'>
									<p style="margin-top:10px; margin-bottom: 0;">Contact by email</p>
								</div>								
								<div>
									{{ Form::radio('contact_playmail',1,'' == 1 ? 1 : 0,array('id'=>'contact_playmail-1','ng-model'=>'basicInfoForm.contact_playmail')) }}
									{{ Form::label('contact_playmail-1','Yes') }}
								</div>
								<div>
									{{ Form::radio('contact_playmail',0,'' == 0 ? 1 : 0,array('id'=>'contact_playmail-0','ng-model'=>'basicInfoForm.contact_playmail')) }}					
									{{ Form::label('contact_playmail-0','No') }}
								</div>
							</div>
							<div id='contact_methods_phone'>
								<div class ='clear'>
									<p style="margin-top:10px; margin-bottom: 0;">Contact by Phone</p>
								</div>								
								<div>
									{{ Form::radio('contact_phone',1,'' == 1 ? 1 : 0,array('id'=>'contact_phone-1','ng-model'=>'basicInfoForm.contact_phone')) }}
									{{ Form::label('contact_phone-1','Yes') }}
								</div>
								<div>
									{{ Form::radio('contact_phone',0,'' == 0 ? 1 : 0,array('id'=>'contact_phone-0','ng-model'=>'basicInfoForm.contact_phone')) }}
									{{ Form::label('contact_phone-0','No') }}
								</div>
							</div>					                                                           
						</div>
					</div>
                </div>
                <div class="alignc">
                    {{ Form::submit('NEXT',array('class'=>'submit-button')) }}
                </div>
                {{ Form::close() }}
        </div>
        <div class ='mPtab' id ='tab_mpProfileDetails'>
            <h3>PROFILE</h3>
            <br>
			<form role="form" controller ='profileDetails' ng-submit = 'saveEscortProfile()' id = 'profileDetails' ng-model = 'profileDetails' class ='profileForm normalSelects'>
                <div class ='col-md-6'>

                    {{ Form::label('body_id','Body Type',array('class'=>'control-label')) }}

                    {{ Form::select('body_id',$bodies,'',array('class'=>'form-control','ng-model'=>'profileDetails.body_id')) }}

                </div>

                <div class ='col-md-6'>

                    {{ Form::label('ethnicity_id','Ethnicity',array('class'=>'control-label')) }}

                    {{ Form::select('ethnicity_id',$ethnicities,'',array('class'=>'form-control','ng-model'=>'profileDetails.ethnicity_id')) }}

                </div>

                <div class ='col-md-6'>

                    {{ Form::label('eye_color_id','Eye Colour',array('class'=>'control-label')) }}

                    {{ Form::select('eye_color_id',$eyeColors,'',array('class'=>'form-control','ng-model'=>'profileDetails.eye_color_id')) }}

                </div>
                <div class ='col-md-6'>

                    {{ Form::label('hair_color_id','Hair Colour',array('class'=>'control-label')) }}

                    {{ Form::select('hair_color_id',$hairColors,'',array('class'=>'form-control','ng-model'=>'profileDetails.hair_color_id')) }}

                </div>
                {{ Form::label('heightLabel','Choose your height',array('id' => 'heightLabel','class'=>'control-label centeredLabel headerLabel')) }}
                <div class="col-md-12 mobile-height-wrapper">
                    <div class="mobile-height-container">
                        <select id="mobile-height-select">
                            <?php for ($i = 150; $i <= 190; $i++): ?>
                                <option value="<?php echo $i ?>"><?php echo $i; ?>cm</option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div class ='col-md-6'>
                    <div id ='heightLabelContainer'>
                        <div id="heightSelectlabel"></div>
                    </div>
                    <div id ='heightContainer'>
                        <div id ='heightSelect'></div>
                    </div>
                </div>
                <div class ='col-md-6 heightInfo'>
                    You're height is: <span class ='heightReadable'>cm ( 4'11" )</span>
                </div>


                {{ Form::text('height','', array( 'id' => 'height','class'=>'form-control hidden','ng-model'=>'profileDetails.height')) }}


                <div class ='col-md-2'>
                    {{ Form::label('servicesLabel','Services',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
                </div>
                <div class ='col-md-10 serviceArray'>
                    <?php $holder = '{{service.name}}'; ?>
                    <div ng-repeat="service in services">
                        <div class ='col-md-4 col-sm-6'>
                            {{ Form::checkbox('services','', false, array('checklist-model'=>"profileDetails.services.escortServices", 'checklist-value'=>'service.id','class'=>'checkbox')) }}
                            {{ Form::label('serviceLabel',$holder,array('class'=>'control-label serviceLabel')) }}
                        </div>
                    </div>
                </div>

                <div class ='col-md-2'>
                    {{ Form::label('servicesLabel','Languages Spoken',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
                </div>
                <div class ='col-md-10 serviceArray'>
                    <?php $holder = '{{language.name}}'; ?>
                    <div ng-repeat="language in languages">
                        <div class ='col-md-4 col-sm-6'>
                            {{ Form::checkbox('languages','', false, array('checklist-model'=>"profileDetails.languages.escortLanguages", 'checklist-value'=>'language.id','class'=>'checkbox')) }}
                            {{ Form::label('serviceLabel',$holder,array('class'=>'control-label serviceLabel')) }}
                        </div>
                    </div>
                </div>


                {{ Form::label('servicesLabel','Let Us Dig Deeper',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}

                <p class ='text-left'>This is where you sell yourself and let your clients know a little bit more. You may include fetishes, kinks and other details that your clients can expect when booking an experience with you. Do not include prices here.</p>
                    {{ Form::textarea('about_me','', array( 'id' => 'about_me','class'=>'form-control','ng-model'=>'profileDetails.about_me')) }}

				<div class="alignc next-previous">
				<input class="submit-button previous-button" type="submit" value="PREVIOUS">
                    {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

                </div>
                {{ Form::close() }}
        </div>
        <div class ='mPtab' id = 'tab_mpAvailability'>
            <form role="form" ng-controller='availaiblityform' ng-submit = 'saveEscortAvailability()' id = 'availabilityForm' ng-model = 'profileAvailabilites' class ='profileForm normalSelects'>
                <h3>DETAILS</h3>
                <p>Let clients know your availability by filling in the days and corresponding start and end times.</P>
                <table class ='profileTable availTable responsiveAvail'>
                    <thead>
                    <th>DAY</th>                    
					<th style="padding-left:0px">
						<div class="row no-gutters">
							<div class="col-md-12 no-gutters">
								<div class="col-md-6 col-sm-6 no-gutters">
									START TIME
								</div>
								<div class="col-md-6 col-sm-6">
									END TIME
								</div>
							</div>
						</div>					
					</th>                                        
                    </thead>
                    <tbody>
                        <?php
                        $holder = '{{avail.handle}}';
                        $start_hour = '{{avail.start_hour_id}}';
                        $index = '{{$index}}';
                        ?>

                        <tr ng-repeat="avail in availabilites.val">							
                            <td>
								<div class="col-md-6 col-sm-4">
									{{ Form::checkbox('days','', false, array('changefunc'=>"availableDaysChange",'onfinishavailability','checklist-model'=>"profileAvailabilites.availabilites.avail", 'checklist-value'=>'avail.day','class'=>'checkbox availDayCheck roundedOne')) }}
									{{ Form::label('Available Days',$holder,array('class'=>'control-label availDaysLabel')) }}
								</div>
								<div class="col-md-6 col-sm-6">
									{{ Form::checkbox('24','', false, array('changefunc'=>"availableDaysChange",'checklist-model'=>"profileAvailabilites.availabilites.twenty_four_hours", 'checklist-value'=>'avail.day','class'=>'checkbox twenty_four_check roundedOne')) }}
									{{ Form::label('serviceLabel','All Day',array('class'=>'control-label serviceLabel ')) }}
								</div>
                            </td>
                            <td>
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6 col-sm-6">
											{{ Form::select('start_hour_id',$availabilites['time'],$start_hour,array('class'=>'form-control','ng-model'=>'profileAvailabilites.availabilites.start_hour_id[$index +1]')) }}                                                          
										</div>
										<div class="col-md-6 col-sm-6">
											{{ Form::select('end_hour_id',$availabilites['time'],$start_hour,array('class'=>'form-control','ng-model'=>'profileAvailabilites.availabilites.end_hour_id[$index +1]')) }}                                
										</div>
									</div>
								</div>
                            </td>                            
                        </tr>
                    </tbody>
                </table>
				</br></br>
                <div class ='col-md-10 col-md-offset-1'>
					<div class ='row'>
						<div class ='col-md-12'>
							<h3>RATES</h3>
							<p>Let clients know your incall/outcall rates.</p>
						</div>
					</div>
                    <div class ='row'>					
                        <div class ='col-md-6'>

                            {{ Form::label('In Calls','In Calls',array('class'=>'control-label headerLabel')) }}
							<div class="rates-radiogroup">
								<input id='incall-yes' type = 'radio' value = 1 ng-model ='profileAvailabilites.does_incalls' class = 'radio roundedOne'>
								{{ Form::label('In Calls','Yes',array('class'=>'control-label radioLabel')) }}
								<input id='incall-no' type = 'radio' value = 0 ng-model ='profileAvailabilites.does_incalls' class = 'radio roundedOne'>
								{{ Form::label('In Calls','No',array('class'=>'control-label radioLabel')) }}
                            </div>
                          <table ng-class="{'profileTable ratesTable' : (profileAvailabilites.does_incalls == 1), 'profileTable ratesTable disabledTable' : (profileAvailabilites.does_incalls == 0) }" id ='inCallTable'>
                                <thead>
                            <th>INCALL SERVICES</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $holder = '{{rate}}';
                                    $start_hour = '{{avail.start_hour_id}}';
                                    $index = '{{$index}}';
                                    ?>
									<tr style="height:20px"></tr>
                                    <tr ng-repeat="( name, rate ) in incalllist">										
                                        <td>
											<div class="row no-gutters">
												<div class="col-sm-6 no-gutters">
													<div class="col-sm-3">
														<span>$</span>
													</div>
													<div class="col-sm-9">
														{{ Form::text('rates','',array('id' => 'callrate', 'placeholder'=>'0.00', 'class'=>'form-control','ng-model'=>'profileAvailabilites.rates[name]')) }}
													</div>
												</div>
												<div class="col-sm-6">
													{{ Form::label($holder,$holder,array('class'=>'control-label-min')) }}
												</div>
											</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class ='col-md-6'>

                            {{ Form::label('Out Calls','Out Calls',array('class'=>'control-label headerLabel')) }}
							<div class="rates-radiogroup">
								<input id='outcall-yes' type = 'radio' value = 1 ng-model ='profileAvailabilites.does_outcalls' class = 'radio roundedOne'>
								{{ Form::label('Out Calls','Yes',array('class'=>'control-label radioLabel')) }}
								<input id='outcall-no' type = 'radio' value = 0 ng-model ='profileAvailabilites.does_outcalls' class = 'radio roundedOne'>
								{{ Form::label('Out Calls','No',array('class'=>'control-label radioLabel')) }}                            
							</div>
                            <table ng-class="{'profileTable ratesTable' : (profileAvailabilites.does_outcalls == 1), 'profileTable ratesTable disabledTable' : (profileAvailabilites.does_outcalls == 0) }" id ='outCallTable'>
                                <thead>
                                <th>OUTCALL SERVICES</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $holder = '{{rate }}';
                                    $start_hour = '{{avail.start_hour_id}}';
                                    $index = '{{$index}}';
                                    ?>
									<tr style="height:20px"></tr>
                                    <tr ng-repeat="(name, rate) in outcalllist">
                                        <td>
											<div class="row no-gutters">
												<div class="col-sm-6 no-gutters">
													<div class="col-sm-3">
														<span>$</span>
													</div>
													<div class="col-sm-9">
														{{ Form::text('rates','',array('id' => 'callrate', 'placeholder'=>'0.00', 'class'=>'form-control','ng-model'=>'profileAvailabilites.rates[name]')) }}													
													</div>
												</div>
												<div class="col-sm-6">
													{{ Form::label($holder,$holder,array('class'=>'control-label-min')) }}
												</div>
											</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    {{ Form::label('Additional Rates Information','Rate Details',array('class'=>'control-label headerLabel centeredLabel')) }}
                  <p>You may provide further details or terms on your rates.</p>
                    {{Form::textarea('notes','',array( 'ng-model' =>'profileAvailabilites.notes'))}}


                    <div class="alignc next-previous">
						<input class="submit-button previous-button" type="submit" value="PREVIOUS">
                        {{ Form::submit('NEXT',array('class'=>'submit-button')) }}
                    </div>
                    {{ Form::close() }}
                </div>
        </div>

        <div class ='mPtab' id = 'tab_mpGallery'>
            <h3 class="dashboard-banner-heading">BANNER</h3>
            <form role="form" ng-controller ='galleryController' ng-submit = 'saveEscortGallery()' id = 'galleryForm' ng-model = 'profileGallery' class ='profileForm normalSelects'>
                <div id ='_cropSquare'>
                    <div ngf-drop="" ngf-pattern="image/*" class="cropArea">
                        <img-crop image="picFile  | ngfDataUrl"
                                  result-image="croppedDataUrl"
                                  ng-init="croppedDataUrl = ''"
                                  area-type="rectangle"
                                  aspect-ratio="3"
                                  on-load-begin="imageUpload()"
                                  on-change="imageChanged(image)"
                                  result-image-size="{ w:1200, h:400 }"
                                  on-load-error= "handleError()"
                                  ></img-crop>
                    </div>
                    <div id = "bannerUploadStatus"></div>
                    <div id ='cropperInstructions'>
                        Crop to the area you would like to use for your banner.  When you are done click the finish button below.
                      <input class="submit-button" type="button" value="FINISH" ng-click = "upload()">
                    </div>
                </div>

                <p class="dashboard-banner-text">The banner image will be displayed at the top of your profile page. This is optional.</p>

                <button id ="bannerUpload" ngf-drop = "handleFileSelect($file)" ngf-select="handleFileSelect($file)"></button>
                <div ng-click ='deleteBanner()' class ='submit-button @if( $escort->banner_image == '' ) hidden @endif' id ='bannerDelete'>Delete Banner</div>


                <h3>GALLERY</h3>
                <p>Upload a maximum of 10 images for your gallery. Drag and drop to sort them.<br>
                Images upload will go through an verification process to ensure they meet our guidelines.</p>
<p>The photos you upload should be of yourself and it should not be pornographic (i.e. no genitals should be shown).</p>

                @include('dashboard.partials.escort-gallery')


                <span class="progress ng-hide" ng-show="progress >= 0">
                <div style="width:%" ng-bind="progress + '%'" class="ng-binding">%</div>
                </span>
                <span ng-show="result" class="ng-hide">Upload Successful</span>
                <span class="err ng-binding ng-hide" ng-show="errorMsg"></span>
				<div class="alignc">
					<p>Once your photos have been approved, you may make one of them your main image by clicking the 'Make Main Image' button.</p>
				</div>
                <div class="alignc next-previous">                    
                    <input class="submit-button previous-button" type="submit" value="PREVIOUS">
					{{ Form::submit('NEXT',array('class'=>'submit-button')) }}
                </div>
                {{ Form::close() }}
        </div>
        <div class ='mPtab' id = 'tab_mpPlans'>
            @include('dashboard.partials.plan-popup')
        </div>
        <div class ='mPtab' id = 'tab_mpTouring' ng-init = 'scopeInit()'><h3>TOURING</h3>
            <form role="form" ng-submit = 'saveEscortTouring()' ng-model = 'profileTouring' class ='profileForm normalSelects' id='touringForm'>
                <p class="danger">If you're planning to travel interstate for work, you may use this section to inform clients on your whereabouts and they will be able to search for you when you are in their city.</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="triggerAnimation animated fadeInUp" data-animate="fadeInUp" style="">

              <h3>CURRENT TOURING DATES</h3>

                            <table class="profileTable datesTable" style="text-align:left;" id="touringTable">
                                <tbody>
                                    <tr>
                                        <th>Date From</th>
                                        <th>Date To</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>

                                    <tr class="touringEntry" id="touringEntry-@{{ tour.id}}" ng-repeat="tour in touring">
                                        <td class="from_date">@{{ tour.from_date}}</td>
                                        <td class="to_date">@{{ tour.to_date}}</td>
                                        <td class="suburb_id">@{{ tour.suburb_name}}</td>
                                        <td><a class="deleteTouring" id="@{{ tour.id}}"ng-click ='deleteTouring(tour.id)'>Delete</a></td>
                                    </tr>

                                </tbody>
                            </table>
                            <div id="touring-form">
                                <h3>ADD TOURING DATES</h3>
                                <div class ='leftFloat'>
                                    <label for="from_date" class="control-label">From Date<span>*</span></label>
                                    <input id="from_date" required name="from_date" type="text" class="form-control datepicker" placeholder="Select date" ng-model ="profileTouring.from_date">
                                </div>
                                <div class ='leftFloat'>
                                    <label for="to_date" class="control-label">To Date<span>*</span></label>
                                    <input id="to_date" required name="to_date" type="text" class="form-control datepicker" placeholder="Select date" ng-model="profileTouring.to_date">
                                </div>
                                <div class ='clear'></div>
                                <span class ='info'>Touring suburb: @{{profileTouring.suburb_name}}</span>
                                <input id="touringLatitude" class="hidden" ng-model="profileTouring.latitude">
                                <input id="touringLongitude" class="hidden" ng-model="profileTouring.longitude">
                                <input id="touringSuburb" class="hidden" ng-model="profileTouring.suburb_name">
                                <autocomplete formmodel ="profileTouring" formval =""></autocomplete>
                            </div>
                            <br>
                            <div class="elements touring-btn-container">
                                <div class="controls touring-btn">
                                    <button id="addTouring" class="submit-button">Add</button>
                                </div>
                            </div>
                            </fieldset>
                            </form>

                            <div ng-class = "{'overlay' : (plan.touring == '0') }">
                            </div>

                        </div>
                    </div>
                </div>
        </div>
		<div id="modalFinish" class="modal">		  
		  <div class="modal-content">
			<span class="close">&times;</span>
			<p>Thanks for completing your profile. It is now Pending Approval.</p>
			<p>Our team member will be in touch shortly to finalise all details.</p>
			<p>Alternatively, please call <a href="tel:1300769766"><span class="button-label">1300 769 766</span></a> to finalise your profile and get it published now!</p>
		  </div>
		</div>				
		<style>
			.panelFixed{z-index:0!important}.modal p{border:none!important;text-align:justify!important}.modal{display:none;position:fixed;z-index:1;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:#000;background-color:rgba(0,0,0,.4)}.modal-content{max-width:800px;z-index:15;background-color:#fefefe;margin:25% auto;padding:20px;border:1px solid #888;width:80%}.close{color:#aaa;float:right;font-size:40px;font-weight:700;padding-left:20px}.close:focus,.close:hover{color:#000;text-decoration:none;cursor:pointer}			
			.escort_profile_form .alignc{text-align:center}.escort_profile_form #availabilityForm .alignc{margin-bottom:10px!important}.escort_profile_form #availabilityForm .alignc .submit-button,.escort_profile_form #galleryForm .alignc .submit-button{margin-bottom:0!important}.escort_profile_form .alignc .submit-button{margin-top:5px!important;margin-bottom:30px!important;display:inline-block!important}.escort_profile_form .previous-button{background:#0866c6!important}@media screen and (max-width:1024px){.escort_profile_form #profileDetails{margin-top:-40px}}
		</style>		
        <script>
			
			setTimeout(function(){ initTab(); }, 100);
			
			function initTab() {
				$('#_MpOverview_').removeClass('mPActiveIcon');
				$('#_MpProfile_').addClass('mPActiveIcon');
			}		
			var modal = document.getElementById('modalFinish');		
			var span = document.getElementsByClassName("close")[0];
			span.onclick = function() {
				modal.style.display = "none";
			}		
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}	
			$(document).delegate('form', 'submit', function(event) {
				navigateProfile(this);
			});
			
			var action = '';

			$(document).on("click", ":submit", function(e){				
				action = $(this).val();				
				if(action.toUpperCase() == '')
					action = 'NEXT';
			});
			
			function navigateProfile(control) {
	
				var $form = $(control);
				var id = $form.attr('id');
				
				if(action == 'NEXT') {					
					if(id == 'escortInfo') {
						$('#mpBasicInfo').removeClass('active');
						$('#mpProfileDetails').addClass('active');
						$('#tab_mpBasicInfo').removeClass('activeTab');
						$('#tab_mpProfileDetails').addClass('activeTab');
					} else if(id == 'profileDetails') {					
						$('#mpProfileDetails').removeClass('active');
						$('#mpAvailability').addClass('active');				
						$('#tab_mpProfileDetails').removeClass('activeTab');
						$('#tab_mpAvailability').addClass('activeTab');
						initDetails();
					} else if(id == 'availabilityForm') {
						$('#mpAvailability').removeClass('active');
						$('#mpGallery').addClass('active');					
						$('#tab_mpAvailability').removeClass('activeTab');
						$('#tab_mpGallery').addClass('activeTab');
						$('#mpGallery').click(); //reload plugin						
					} else if(id == 'galleryForm') {					
						$('#mpGallery').removeClass('active');
						$('#mpPlans').addClass('active');					
						$('#tab_mpGallery').removeClass('activeTab');
						$('#tab_mpPlans').addClass('activeTab');						
					} else if(id == 'popupPlanForm' && $('.currentPlan').attr("data-id") == '2') {					
						$('#mpPlans').removeClass('active');
						$('#mpTouring').addClass('active');					
						$('#tab_mpPlans').removeClass('activeTab');
						$('#tab_mpTouring').addClass('activeTab');
						$('#mpTouring').show();
						$('#desc_mpTouring').show();
						$('#tab_mpTouring').show();
						$('.overlay').removeClass('overlay');					
						modal.style.display = "block";
					} else if(id == 'popupPlanForm' && $('.currentPlan').attr("data-id") == '1') {										
						modal.style.display = "block";
					}		
				} else if (action == 'PREVIOUS') {
					if(id == 'profileDetails') {					
						$('#mpProfileDetails').removeClass('active');
						$('#mpBasicInfo').addClass('active');				
						$('#tab_mpProfileDetails').removeClass('activeTab');
						$('#tab_mpBasicInfo').addClass('activeTab');
					} else if(id == 'availabilityForm') {
						$('#mpAvailability').removeClass('active');
						$('#mpProfileDetails').addClass('active');					
						$('#tab_mpAvailability').removeClass('activeTab');
						$('#tab_mpProfileDetails').addClass('activeTab');
					} else if(id == 'galleryForm') {					
						$('#mpGallery').removeClass('active');
						$('#mpAvailability').addClass('active');					
						$('#tab_mpGallery').removeClass('activeTab');
						$('#tab_mpAvailability').addClass('activeTab');
						initDetails();
					}
				}
				$('#dashOverlay').hide();
				$('.spinner').hide();
				$('.load-container').hide();			
				
				if($('.currentPlan').attr("data-id") == '1')
				{
					$('#mpTouring').hide();
					$('#desc_mpTouring').hide();
					$('#tab_mpTouring').hide();
				}
			}

			if(plan.name.indexOf('Touring') ==-1)
			{
				$('#mpTouring').hide();
				$('#desc_mpTouring').hide();
				$('#tab_mpTouring').hide();				
			}
	
			initDetails();
			
			function initDetails() {				
				$("#incall-yes").prop('checked') ? $('#inCallTable').show() : $('#inCallTable').hide()
				$("#outcall-yes").prop('checked') ? $('#outCallTable').show() : $('#outCallTable').hide()
				$('input[id="callrate"]').each(function() {						
					if($(this).val() == '0.00')
						$(this).val('');
					$(this).change(function() { 
						$(this).val(parseFloat($(this).val()).toFixed(2));						
						if(isNaN($(this).val()))
							$(this).val('');
					});
					$(this).keypress(function(event) { if (((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) && !validateQty(event)) { event.preventDefault(); } });
					$(this).attr('maxlength','7');
				});
			}
			
			function validateQty(event) {
				var key = window.event ? event.keyCode : event.which;
				if (event.keyCode == 8 || event.keyCode == 46
				 || event.keyCode == 37 || event.keyCode == 39) {
					return true;
				}
				else if ( key < 48 || key > 57 ) {
					return false;
				}
				else return true;
			};
			
			$("#incall-no").change(function() 	{	initDetails(); });
			$("#incall-yes").change(function() 	{ 	initDetails(); });
			$("#outcall-no").change(function() 	{	initDetails(); });
			$("#outcall-yes").change(function() { 	initDetails(); });
						
            $('.datepicker').datepicker({
            minDate: 0,
                    dateFormat: "dd-mm-yy",
                    beforeShowDay: function(date){
                    var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
                    return [ blockedDates.indexOf(string) == - 1 ]
                    }
            });
            $('#mobile-height-select').on('change', function(){
            var selected = $(this).find(':selected').val();
            $('#height').val(selected).trigger('input');
            });
            $('.datepicker').datepicker({
            minDate: 0,
                    dateFormat: "dd-mm-yy"
            });
            $('.mPProfileTopMenu div').on('click', function(e) {
            var tabName = $(this).attr('id');
            if (tabName == undefined){
            var tabName = $(this).children('div').attr('id');
            }
            $('.mPtab').removeClass('activeTab');
            $('#tab_' + tabName).addClass('activeTab');
            $('.mPProfileTopMenu div').removeClass('active');
            $(this).addClass('active');
            if ($(window).width() <= 1024) {
            $('.mPProfileTopMenu').hide();
            $('#navigateThroughProfile').show();
            }
			initDetails();
            e.stopPropagation();			
            })
                    $('#overlayBackGround').on('click', function() {
            $('#_cropSquare').hide();
            $(this).hide();
            });
            function toFeet(n) {
            var realFeet = ((n * 0.393700) / 12);
            var feet = Math.floor(realFeet);
            var inches = Math.round((realFeet - feet) * 12);
            return feet + "'" + inches + '"';
            }

            var initialValue = 0, min = 150, max = 190;
            $("#heightSelect").slider({
            value:initialValue,
                    min: min,
                    max: max,
                    step: 1,
                    slide: function(event, ui) {
                    $("#height").val(ui.value);
                    $(".heightReadable").text(ui.value + 'cm ( ' + toFeet(ui.value) + ' )');
                    $('#height').trigger('input');
                    var imperial = toFeet(ui.value);
                    // console.log(imperial);
                    $("#heightSelectlabel").text(ui.value + 'cm');
                    $("#heightSelectlabel").css("margin-left", (ui.value - min) / (max - min) * 100 + "%");
                    }
            });
            $("#heightSelect").draggable();
            $("#height").val(initialValue);
            $("#heightSelectlabel").text(initialValue + 'cm ( ' + toFeet(initialValue) + ' )');
            $("#heightSelectlabel").css("margin-left", (initialValue - min) / (max - min) * 100 + "%");
            markers = [];
            var postObject = {
            userId: id,
                    csrf: $('#_token').val(),
                    businessId: active_id
            },
                    cropTypes = {
                    widescreen : true,
                            letterbox: false,
                            free: false
                    };
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

<?php
/* var placeSearch, autocomplete;
  var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
  };

  function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
  /** @type {!HTMLInputElement} */
//  (document.getElementById('autocomplete')),
// {types: ['geocode']});
/*  (document.getElementById('autocomplete')),
  {types: ['(cities)'],componentRestrictions: {country: "au"}});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
  }

  // [START region_fillform]
  function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  console.log( place.address_components );
  if ( place.address_components[3] == undefined ) {
  alert( 'Please enter a specific suburb');
  $( '#autocomplete').html('');
  }
  else {

  $('#postcode').val(place.address_components[3]['long_name']);
  }

  // document.getElementById('postcode').value = place.geometry.location.administrative_area_level_2();

  $( '#latitude' ).trigger('input');
  $( '#longitude' ).trigger('input');
  $('#autocompleteAddress').trigger('input');
  $('#postcode').trigger('input');


  }
  // [END region_fillform]

  // [START region_geolocation]
  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
  if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(function(position) {
  var geolocation = {
  lat: position.coords.latitude,
  lng: position.coords.longitude
  };
  var circle = new google.maps.Circle({
  center: geolocation,
  radius: position.coords.accuracy
  });
  autocomplete.setBounds(circle.getBounds());
  });
  }
  }
  // [END region_geolocation]
  initAutocomplete(); */

</script>