<?php

namespace Dashboard;

use Controller, View, Config, Payment, BusinessPlan, Helpers, Datatables, Input, Business, Auth, Escort, User, Age, Height, Ethnicity, EyeColor, Body, HairColor, Service, EscortLanguage, EscortService, EscortAvailability, EscortRate;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;

class BusinessController extends \ProfileController
  //class BusinessController extends Controller 
{
  public function profile($id, $type ) {
    if ( $type == 'user') {
     $arr = array( 'busMenu' => 
       array( array( 'item' => 'Favourites', 'id' => '_MpFavourites', 'title' => 'Your Favouries'),
        array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings')
        )
       );
   }
   else if ( $type == 'admin') {
     $arr = array( 'busMenu' => 
       array( array( 'item' => 'Overview', 'id' => '_MpOverview','title' => ucfirst( $type ) . ' Overview' ),
        array( 'item' => 'Escorts', 'id' => '_MpEscorts', 'title' => 'Your Escorts' ),
        array( 'item' => 'Businesses', 'id' => '_MpProfile', 'title' => ucfirst( $type ) . ' Profile'. ' Profile' ),
                           // array( 'item' => 'Payments', 'id' => '_MpPayments', 'title' => 'Manage user payments'),
        array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings')
        )
       );

   }
   else {
    $active = ( $type =='escort' ? Escort::where( 'id','=', $id )->first() : Business::where( 'id','=', $id )->first());
    $profileSels = ( $active ? $active->getProfileSelections() : '' );
    $arr = array( 'busMenu' => 
     array( array( 'item' => 'Overview', 'id' => '_MpOverview','title' => ucfirst( $type ) . ' Overview' ),
      array( 'item' => ucfirst( $type ) . ' Profile', 'id' => '_MpProfile', 'title' => ucfirst( $type ) . ' Profile'. ' Profile','submenu' => $profileSels )
      )
     );
    if ( $type =='business' ) {
      $escorts = ( $active ? $active->getJsonEscorts() : '' );
      $arr['busMenu'][] = array( 'item' => 'Your Escorts', 'id' => '_MpEscorts', 'title' => 'Your Escorts', 'submenu' => $escorts );
      if ( $active && $active->hasReviews() )
       $arr['busMenu'][]  = array( 'item' => 'Escort Reviews', 'id' => '_MpReviews', 'title' => 'Manage your reviews','submenu'=> $active->getEscortsWithReviewsAjax() );
   }
   else {
    if ( $active && $active->hasReviews() )
     $arr['busMenu'][]  = array( 'item' => 'Reviews', 'id' => '_MpReviews', 'title' => 'Manage your reviews' );
 }
            // $arr['busMenu'][] = array( 'item' => 'Payments', 'id' => '_MpPayments','title' => 'Billing and Payments' );
            //  $arr['busMenu'][]  = array( 'item' => 'Playmail', 'id' => '_MpPlaymail','title' => 'Manage your emails' );
 $user = Auth::user();
 if ( $user->super_user != 1 )
  $arr['busMenu'][]  = array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings');
}


$json = json_encode( $arr );
//return 'angular.callbacks._0(' . $json . ');';
echo $json;
}
public function autocomplete($search) {

  $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . $search . "&types=(cities)&region=AU&key=" . Config::get('constants.GOOGLEAPIKEY');
  $location = file_get_contents($url);
  return $location;

}
public function getlatlong( $search ){
  $url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" . $search . "&key=" . Config::get('constants.GOOGLEAPIKEY');
  $location = file_get_contents($url);
  return $location;
}
public function businessMobileMenu($id,$active_id, $type){
  if ( $type == 'user') {
    $arr = array( 'menu' => 
     array( array( 'item' => 'Favourites', 'id' => '_MpFavourites','title' => 'Your Favourites' ),
      array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Your Account')
      )
     );

  }
  else {
    $user = User::where( 'id', '=', $id )->first();
    $el = Input::get('el');
    $businesses = $user->businesses;
    $active = ( $type =='escort' ? Escort::where( 'id','=', $active_id )->first() : Business::where( 'id','=', $active_id )->first());
    $arr = array( 'menu' => 
     array( array( 'item' => 'Overview', 'id' => '_MpOverview','title' => ucfirst( $type ) . ' Overview' ),
      array( 'item' => 'Profile', 'id' => '_MpProfile', 'title' => ucfirst( $type ) . ' Profile','submenu' => $active->getProfileSelections() ),

      )
     );
    if ( count( $businesses) > 0 ){
      $escorts = $active->getJsonEscorts();
      $arr['menu'][] = array( 'item' => 'Escorts', 'id' => '_MpEscorts', 'title' => 'Your Escorts', 'submenu' => $escorts );
                           // $arr['menu'][]  = array( 'item' => 'Playmail', 'id' => '_MpPlaymail','title' => 'Manage your emails' );
      if ( $active && $active->hasReviews() )
        $arr['menu'][]  = array( 'item' => 'Reviews', 'id' => '_MpReviews', 'title' => 'Manage your reviews','submenu'=> $active->getEscortsWithReviewsAjax() );
    }
    else {
     if ( $active && $active->hasReviews() )
      $arr['menu'][]  = array( 'item' => 'Reviews', 'id' => '_MpReviews', 'title' => 'Manage your reviews' );
  }
  $arr['menu'][] = array( 'item' => 'Payments', 'id' => '_MpPayments','title' => 'Billing and Payments' );
  $arr['menu'][]  = array( 'item' => 'Account', 'id' => '_MpAccount', 'title' => 'Password and account settings');
}

$json = json_encode( $arr );
return $json;
}
   /* public function detail($id) {
        $array = array( 'details' => array('hi che') );
        $ret = json_encode( $array );
        return 'angular.callbacks._0(' . $ret . ');';
      }*/
      public function panel($id) {
        $input = Input::get();
        $business = Business::where( 'id','=',$input['busid'] )->first();
        switch ( $input['panel']) {
         case '_MpOverview':
         switch ($business['status']) {
          case '0':
          $status = 'Pending';     
          break;
          case '1':
          $status = 'Pending';     
          break;
          case '2':
          $status = 'Approved';
          break;
          case '3':
          $status = 'Disapproved';
          case '4':
          $status = 'Unavailable';
          break;
          case '5':
          $status = 'Pending Payment';
          break;
          break;
        }
        if ( $business ){
         $escorts = $business->escortsStatusCount();
       }
       else {
         $business = new Business;
         $escorts = '0';
         $status = 'Pending';
       }

       return View::make('dashboard.business.panels.overview', compact('business','status','escorts'));
       break;
       case '_MpProfile':
       switch ($business['status']) {
        case '0':
        $status = 'Pending';     
        break;
        case '1':
        $status = 'Pending';     
        break;
        case '2':
        $status = 'Approved';
        break;
        case '3':
        $status = 'Disapproved';
        case '4':
        $status = 'Unavailable';
        break;
        case '5':
        $status = 'Pending Payment';
        break;
        break;
      }
      if ( !isset( $status ) ) $status = 'Pending';
      return View::make('dashboard.business.panels.profile', compact('business','status','escorts'));
      break;
      case '_MpEscorts':
      $user = User::where('id','=', $id )->first();
      $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
      $count = 0;
      if ( $business )
       $count = $business->escortsCount();
     $escorts = ( $business ? $business->getEscorts() : array() );
     return View::make('dashboard.business.panels.escorts', compact('escorts','plan','count'));
     break;
     case '_MpReviews':
     $escorts = $business->getEscortsWithReviews();
     return View::make('dashboard.business.panels.reviews', compact('escorts'));
     break;
     case '_MpPayments':
     return View::make('dashboard.business.panels.payments');
     break;
     case '_MpAccount':
     return '<h4 class="panelHeader accountPanelHeader">Manage Your Account</h4>';
     break;
     default: return View::make('dashboard.business.panels.profile', compact('business','status','escorts'));
   }
   return View::make('dashboard.panel', compact('panel'));
 }
 public function screen($id) {
  $input = Input::get();
  $business = Business::where( 'id','=',$input['busid'] )->first();
  switch ( $input['panel']) {
    case '_MpOverview':
    $user = User::where('id','=', $id )->first();
    $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
    if ( $business ){
      $count = $business->escortsCount();
    }
    else {
      $count = 0;
    }
    return $this->overviewScreen( $business, $plan, $count, $user );
    break;
    case '_MpProfile':
    return $this->profileScreen($business);
    break;
    case '_MpEscorts':
    return View::make('dashboard.business.screens.escorts', compact( 'business' ) );
    break;
    case '_MpReviews':
    $reviews = $business->escortReviews();
    return View::make('dashboard.business.screens.reviews', compact( 'reviews' ));
    break;
    case '_MpAccount':
    $user = Auth::user();
    return View::make('dashboard.business.screens.account', compact( 'user' ));
    break;
    case '_MpPayments':
    $user = Auth::user();
    return View::make('dashboard.business.screens.payments', compact( 'user' ));
    break;
  }

}
public function newEscort() {
  $user = Auth::user();
  $plan = BusinessPlan::where('id','=',$user->id)->first();
  if ( !$plan ) $plan = BusinessPlan::where('id','=','9')->first();
  $ages = Age::lists('name','id');
  return View::make('dashboard.business.screens.escorts.new',compact( 'ages','plan') );
}

public function profileScreen($business) {
      // if ( $business )
 return View::make('dashboard.business.screens.profile', compact('business','status','escortStats','suburbs'));
     //  $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
     //  return View::make('dashboard.business.screens.profile.new',compact('type'));
}

public function overviewScreen($business,$plan, $count, $user ) {
  if ( $business ) {
    switch ($business['status']) {
      case '0':
      $status = 'Pending';     
      break;
      case '1':
      $status = 'Pending';     
      break;
      case '2':
      $status = 'Approved';
      break;
      case '3':
      $status = 'Disapproved';
      break;
    }
  }
  else {
    $status = 'Pending';
  }
  return View::make('dashboard.business.screens.overview', compact('business','status','escortStats','plan','count', 'user'));
}
public function escortScreen($id) {
  if ( $id == 'new'){
   $ages = Age::lists('name','id');
   $user = Auth::user();
   $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
   $business = Business::where('user_id','=',$user->id )->first();
   if ( !$business ) $business = new Business;
   if ( !$plan ) $plan = BusinessPlan::where('id','=','9')->first();
   if ( isset( $business ) && $business->type == 2 ){
    return View::make('dashboard.business.screens.escorts.agency.new',compact( 'ages','plan','business','user'));
  }
  else {
    return View::make('dashboard.business.screens.escorts.other.new',compact( 'ages','plan','business' ));
  }
}
$user = Auth::user();
$type = $user->getUserType();
if ( !$type == 1 && $user->id != $id )
  die();
$escort = Escort::where('id','=',$id)->first();
if( !$escort ){
 $status = 'New Escort';
}
else {
  switch ($escort->status) {
    case '0':
    $status = 'Pending';     
    break;
    case '1':
    $status = 'Pending';     
    break;
    case '2':
    $status = 'Approved';
    break;
    case '3':
    $status = 'Disapproved';
    break;
    case '4':
    $status = 'Unavailable';
    break;
    case '5':
    $status = 'Pending Payment';
    break;
  }
}
$pass = array(
  'escort' => $escort,'status' => $status );
return View::make('dashboard.business.screens.escorts.layout',$pass );
}
public function profileTab($tab) {
  $business = new Business;
  switch ( $tab ) {
    case "View":
    return View::make('dashboard.business.screens.profile.view', compact('url'));
    break;
    case "New":
    $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
    $user = Auth::user();
    return View::make('dashboard.business.screens.profile.new', compact('id','type','user'));
    break;
    case "Edit":
    $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
    $plans = BusinessPlan::where('type','=','2')->where('useraccess','=',1 )->get();
    $user = User::where('id','=',$business->user_id )->first();
    if ( $user ){
     $plan = BusinessPlan::where( 'id','=',$user->business_plan )->first();
   }
   else {
    $plan = new BusinessPlan;
  }
  return View::make('dashboard.business.screens.profile.edit', compact('business','type','plans','plan'));
  break;
  case "Bumpup":
  return 'Bump up';
  break;
}

}
public function paymentTab($tab, $id) {
  $user = User::where( 'id','=',$id )->first();
  $payments = $user->payments()->get();
  switch ( $tab ) {
    case "Overview":
    return View::make('dashboard.business.screens.payments.Overview', compact('payments'));
    break;
    case "Payment_History":
    return View::make('dashboard.business.screens.payments.History', compact('payments'));
    break;
    case "Make_a_Payment":
    $type = array('1'=> 'Brothel','2'=> 'Agency','3' => 'Massage Parlour');
    return View::make('dashboard.business.screens.payments.Payment', compact('business','type'));
    break;
  }

}
public function lowerEscortScreen($id) {
  $user = Auth::user();
  $type = $user->getUserType();
  if ( !$type == 1 && $user->id != $id )
    die();
  $action = Input::get('action');
  $escort = new Escort;
  $business = Business::find($id);

  if ( $action == 'New' ){
    $ages = Age::lists('name','id');
    $plan = BusinessPlan::where( 'id','=',$user->business_plan )->first();
    if ( $business && $business->type == 2 )
      return View::make('dashboard.business.screens.escorts.agency.new',compact( 'ages','plan','business' ));
    return View::make('dashboard.business.screens.escorts.other.new',compact( 'ages','plan','business' ));
  }
  $pass = array(
    'escort' => $escort );
  if ( $action == 'edit' || $action =='Edit'){
    $escort = new Escort;
    $main_image = $escort->getMainImage();
    $ages = Age::lists('name','id');
    if ( isset( $business ) && $business->type != 2 ){
      $phone = $business->phone;
      $email = $business->email;
      return View::make('dashboard.business.screens.escorts.other.edit', compact('escort','main_image', 'escortRates','ages','phone','email'));
    }
    $suburb = $escort->getDefaultLocation();
    $heights = Height::lists('name','id');
    $ethnicities = Ethnicity::lists('name','id');
    $eyeColors = EyeColor::lists('name','id');
    $hairColors = HairColor::lists('name','id');
    $services = EscortService::where('escort_id','=',$id)->first();
    if ( empty($services )) $services = new EscortService;
    $services = $services->getList($id);
    $languages = EscortLanguage::where('escort_id','=',$id)->first();
    if ( empty( $languages )) $languages = new EscortLanguage;
    $languages = $languages->getList($id);
    $availabilites = $this->getAvailabilites( $id );
    $escortRates = $escort->getRates($escort->id);
    $inCallList = $this->inCallList($escort->id);
    $outCallList = $this->outCallList($escort->id);
    if (isset($escort->gender_id) && $escort->gender_id  == 3 || $escort->gender_id  == 0 ) {
     $bodies = Body::lists('name','id');
   } else {
     $bodies = Body::where('gender_id', $escort->gender_id)->get()->lists('name','id');
   }      
   return View::make('dashboard.business.screens.escorts.agency.edit', compact('business','escort','main_image', 'escortRates', 'inCallList','outCallList' ,'availabilites','ages', 'suburb','bodies','heights','ethnicities','languages','services','eyeColors','hairColors'));
 }
 else {
  return View::make('dashboard.business.screens.escorts.' . $action, $pass );
}

}
public function getAvailabilites($id) {
  $escortAvailbities = EscortAvailability::where( 'escort_id','=',$id )->get();
  $sorted = array(
   'escortDays' => array(),
   'twenty_four_hours' => array()
   );
  $daysSelect =  array(
    "1" => "Mon",
    "2" => "Tue",
    "3" => "Wed",
    "4" => "Thu",
    "5" => "Fri",
    "6" => "Sat",
    "7" => "Sun"
    );
  if ( $escortAvailbities->isEmpty() ){
    foreach( $daysSelect as $key => $day ){
      $sorted['days'][$key] = array(
        'handle' => $day,
        'day'    => $key
        );
    }
  }
  else {
    foreach ( $escortAvailbities as $key => $av ) {
      $sorted['days'][$av->day] = $av['original'];
      $sorted['days'][$av->day]['handle'] = $daysSelect[$av->day];
      if ( $av['enabled'] == 1 )
        $sorted['escortDays'][$av->day] = $av->day;
      if ( $av['twenty_four_hours'] == 1 )
        $sorted['twenty_four_hours'][]  = $av->day;
    }
  }
  $sorted['time'] = array(
	 "0" =>  "12:00 AM",
	 "1" =>  " 1:00 AM",
	 "2" =>  " 2:00 AM",
	 "3" =>  " 3:00 AM",
	 "4" =>  " 4:00 AM",
	 "5" =>  " 5:00 AM",
	 "6" =>  " 6:00 AM",
	 "7" =>  " 7:00 AM",
	 "8" =>  " 8:00 AM",
	 "9" =>  " 9:00 AM",
	 "10" => "10:00 AM",
	 "11" => "11:00 AM",
	 "12" => "12:00 PM",
	 "13" => " 1:00 PM",
	 "14" => " 2:00 PM",
	 "15" => " 3:00 PM",
	 "16" => " 4:00 PM",
	 "17" => " 5:00 PM",
	 "18" => " 6:00 PM",
	 "19" => " 7:00 PM",
	 "20" => " 8:00 PM",
	 "21" => " 9:00 PM",
	 "22" => "10:00 PM",
	 "23" => "11:00 PM",
	);
  $sorted['am'] = array(
    "0" => 'am',
    "1" => 'pm');
  
  return $sorted;
}
public function outCallList() {
  $ratesList = array(
    'outcall_15_rate' => '15 Min',
    'outcall_30_rate' => '30 Min',
    'outcall_45_rate' => '45 Min',
    'outcall_1h_rate' => '1 hour',
    'outcall_2h_rate' => '2 hour',
    'outcall_3h_rate' => '3 hour',
    'outcall_overnight_rate' => 'Overnight'
    );
  return $ratesList;
}
public function inCallList() {
  $ratesList = array(
    'incall_15_rate' => '15 Min',
    'incall_30_rate' => '30 Min',
    'incall_45_rate' => '45 Min',
    'incall_1h_rate' => '1 hour',
    'incall_2h_rate' => '2 hour',
    'incall_3h_rate' => '3 hour',
    'incall_overnight_rate' => 'Overnight'
    );
  return $ratesList;
}
public function getRates($id){
 $escort = Escort::where( 'id','=',$id)->first();
 $escortRates = $escort->getRates($escort->id);
 return json_encode ( $escortRates );
}
public function getImages($id){
 $escort = Escort::where( 'id','=',$id)->first();
 $images = $escort->images()->get();
 return json_encode ( $images );
}
public function reviewScreen($id){
  $escort = Escort::where( 'id','=',$id)->first();
  $reviews = $escort->Reviews()->get();
  return View::make('dashboard.business.screens.reviews.view', compact( 'reviews','escort') );
}
public function BusinessProfileDetails($id) {
  if ( $id == 0 )
    return;
  $business =  Business::find( $id );
  switch ($business->status) {
    case '0':
    $status = 'Pending';     
    break;
    case '1':
    $status = 'Pending';     
    break;
    case '2':
    $status = 'Approved';
    break;
    case '3':
    $status = 'Disapproved';
    case '4':
    $status = 'Unavailable';
    break;
    case '5':
    $status = 'Pending Payment';
    break;
    break;
  }
  $business->business_status = $status;
  $business->previewUrl = '/business/preview/' . $business->id;
  return $business;
}
}
