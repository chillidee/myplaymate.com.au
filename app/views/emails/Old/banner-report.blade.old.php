@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class=deviceWidth style="width: 580px;" align=center bgcolor=#eeeeed border=0 cellpadding=0 cellspacing=0>
  <tbody>
    <tr>
      <td style="padding: 0;" bgcolor=#ffffff valign=top>
        <a href="https://www.myplaymate.com.au/">
          <img class="deviceWidth" style="display: block; border-radius: 4px;" src="https://myplaymate.com.au/assets/emails/main-updated-4.png" border="0">
        </a>
      </td>
    </tr>
    <tr>
      <td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
        <table bgcolor="#eeeeed">
          <tbody>
            <tr>
              <td style="padding: 0 10px 10px 0;" valign=middle>
                <a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href=#>
                  Hi {{ $escort->escort_name }},
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        @if ( $input['action'] == 2 )
        <p>We have reviewed your banner image, and are pleased to tell you it has been approved.</p>

        @else 
        <p>We have reviewed your banner image, and unfortunatley have not approved it.  If you would like to discuss this, please contact the team at Myplaymate.

        Reason: {{ $input['reason']}}

        @endif

      
        <p style="clear:both"></p>

        <p>The My Playmate Team</p>
      </td>
    </tr>
  </tbody>
</table> <!-- End One Column -->

@stop