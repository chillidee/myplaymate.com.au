<?php

/**
 * Class Escort
 */
class Escort extends Eloquent
{
    protected $guarded = [];

    public static function boot()
    {
        static::saved(function()
        {
            Event::fire('sitemap.generate');
        });

        static::deleted(function()
        {
            Event::fire('sitemap.generate');
        });
    }

   /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

   /**
     * @return mixed
     */
    public function business()
    {
        return $this->belongsTo('Business');
    }

    public function businesses()
    {
        return $this->belongsToMany('Business','escort_business');
    }

    /**
     * @return mixed
     */
    public function age()
    {
        return $this->belongsTo('Age', 'age_id');
    }

    /**
     * @return mixed
     */
    public function body()
    {
        return $this->belongsTo('Body', 'body_id');
    }

    /**
     * @return mixed
     */
    public function ethnicity()
    {
        return $this->belongsTo('Ethnicity', 'ethnicity_id');
    }

    /**
     * @return mixed
     */
    public function eyeColor()
    {
        return $this->belongsTo('EyeColor', 'eye_color_id');
    }

    /**
     * @return mixed
     */
    public function hairColor()
    {
        return $this->belongsTo('HairColor', 'hair_color_id');
    }

    /**
     * @return mixed
     */
    public function height()
    {
        return $this->belongsTo('Height', 'height_id');
    }

    /**
     * @return mixed
     */
    public function hourlyRate()
    {
        return $this->belongsTo('HourlyRate', 'hourly_rate_id');
    }

    /**
     * @return mixed
     */
    public function getRates($id)
    {
        $outCalls = EscortRate::where('escort_id','=',$id)->get(['incall_15_rate','incall_30_rate','incall_45_rate','incall_1h_rate','incall_2h_rate','incall_3h_rate','incall_overnight_rate',
            'outcall_15_rate','outcall_30_rate','outcall_45_rate','outcall_1h_rate','outcall_2h_rate','outcall_3h_rate','outcall_overnight_rate']);
     return $outCalls;
    }

    public function getProfileSelections(){
      $arr = array('Edit','View');
      $return = array();
      $i = 0;
      foreach( $arr as $val ){
          $return[$i]['id'] = $val;
          $return[$i]['name'] = $val;
          $i++;
      }
      return $return;
    }

    /**
     * @return mixed
     */
    public function defaultLocation()
    {
        return $this->belongsTo('Suburb', 'suburb_id');
    }
    public function getDefaultLocation()
    {
        return $this->belongsTo('Suburb', 'suburb_id')->first();
    }

    /**
     * @return mixed
     */
    public function gender()
    {
        return $this->belongsTo('Gender', 'gender_id');
    }

    /**
     * @return mixed
     */
    public function services()
    {
        return $this->belongsToMany('Service')->orderBy('order');
    }

    /**
     * @return mixed
     */
    public function languages()
    {
        return $this->belongsToMany('Language');
    }

    /**
     * @return mixed
     */
    public function rates()
    {
        return $this->hasMany('EscortRate');
    }

    public function hasIncallRates()
    {
        if($this->rates->first())
        {
            if ($this->rates->first()->incall_15_rate > 0 ||
                $this->rates->first()->incall_30_rate > 0 ||
                $this->rates->first()->incall_45_rate > 0 ||
                $this->rates->first()->incall_1h_rate > 0 ||
                $this->rates->first()->incall_2h_rate > 0 ||
                $this->rates->first()->incall_3h_rate > 0 ||
                $this->rates->first()->incall_overnight_rate > 0)
            {
                return true;
            }
        }

        return false;
    }

    public function has1hIncallRate()
    {
        if($this->rates->first() && $this->rates->first()->incall_1h_rate > 0)
        {
            return true;
        }

        return false;
    }

    public function hasOutcallRates()
    {
        if($this->rates->first())
        {
            if ($this->rates->first()->outcall_15_rate > 0 ||
                $this->rates->first()->outcall_30_rate > 0 ||
                $this->rates->first()->outcall_45_rate > 0 ||
                $this->rates->first()->outcall_1h_rate > 0 ||
                $this->rates->first()->outcall_2h_rate > 0 ||
                $this->rates->first()->outcall_3h_rate > 0 ||
                $this->rates->first()->outcall_overnight_rate > 0)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function reviews()
    {
        return $this->hasMany('EscortReview');
    }
    /**
     * @return mixed
     */
    public function hasReviews()
    {
        $reviews = $this->hasMany('EscortReview')->get();
        return ( $reviews->isEmpty() ? false : true );
    }

    /**
     * @return mixed
     */
    public function approvedReviews()
    {
        return $this->hasMany('EscortReview')->where('approved', true);
    }

    /**
     * @return mixed
     */
    public function incallRates()
    {
        return $this->hasMany('EscortRate')->where('incall', true);
    }

    /**
     * @return mixed
     */
    public function outcallRates()
    {
        return $this->hasMany('EscortRate')->where('outcall', true);
    }

    /**
     * @return mixed
     */
    public function touring()
    {
        return $this->hasMany('EscortTouring');
    }

    /**
     * @return mixed
     */
    public function images()
    {
        return $this->hasMany('EscortImage')->orderBy('order','asc');
    }

    /**
     * @return mixed
     */
    public function availabilities()
    {
        return $this->hasMany('EscortAvailability')->orderBy('day','asc');
    }


    /**
     * @return mixed
     */
    public function mainImage()
    {
        return $this->belongsTo('EscortImage', 'main_image_id');
    }

    public function getMainImage()
    {
        return $this->belongsTo('EscortImage', 'main_image_id')->first();
    }

    /**
     * @return mixed
     */
    public function mainImageApproved()
    {
        return $this->belongsTo('EscortImage', 'main_image_id')->where('approved', 1);
    }


    public function infinite_scroll($query, $escort_per_page, $page)
    {
        if (isset($page))
            $query = $query->skip($escort_per_page * $page);

        $query = $query->take($escort_per_page);
        $query = $query->get(['id','escort_name','age_id','suburb_id','featured','about_me']);

        return $query;
    }

    public function set_is_wishlist($escort)
    {
        $user_wishlist = Wishlist::whereUserId(Auth::user()->id)->get();
        foreach ($user_wishlist as $key => $wish) {
            if ($wish->escort_id == $escort->id)
                $escort->isWishlist = true;
            else
                $escort->isWishlist = false;
        }

        return $escort;
    }


    /**
     * @param $input
     * @return bool
     */
    public function sendPlayMail($input)
    {
        try {
            $emailTemplate = EmailTemplate::where('name', 'playmail')->first();

            $data                = [];
            $data['escort_name'] = $this->escort_name;
            $data['email']       = ( !App::environment('development') ? $this->email : Config::get('constants.DEV_EMAIL') );
            $data['user']        = $this->user;
            $data['message']     = $input;
            $data['id'] = $this->id;

            // Send activation email to new user and to admin
            Mail::send('emails.'.$emailTemplate->name, ['data' => $data], function ($message) use ($data) {
                $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                        ->subject('New Playmail for you from My Playmate')
                        // ->cc(Config::get('app.adminEmail'))
                        ->to($data['email'])
                        ->replyto($data['message']['email']);
            });
            Mail::send('emails.'.$emailTemplate->name, ['data' => $data], function ($message) use ($data) {
                $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                        ->subject('New Playmail to Escort #' . $data['id'])
                        // ->cc(Config::get('app.adminEmail'))
                        ->to('contact@myplaymate.com.au');
            });

            return true;

        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $input
     * @return bool
     */
    public function sendReviewMail()
    {
        try {
            $emailTemplate = EmailTemplate::where('name', 'review')->first();

            $data                = [];
            $data['escort_name'] = $this->escort_name;

            // Send activation email to new user and to admin
            Mail::send('emails.'.$emailTemplate->name, ['data' => $data], function ($message) use ($data) {
                $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                        ->subject('New Escort Review to approve from My Playmate')
                        // ->to(Config::get('app.adminEmail'));
                        ->to('admin@myplaymate.com.au');
            });

            return true;

        } catch (Exception $ex) {
            var_dump($ex);
        }
    }

    /**
     * @return string
     */
    public function getSeoUrl()
    {
        $url = '';

        $defaultLocation = Suburb::find($this->suburb_id);
        $url .= isset($defaultLocation->name) && $defaultLocation->name != '' ? $defaultLocation->name.'-' : '';
        $url .= isset($defaultLocation->state->short_name) && $defaultLocation->state->short_name != '' ? $defaultLocation->state->short_name.'-' : '';
        $url .= isset($this->escort_name) && $this->escort_name != '' ? $this->escort_name.'-' : '';
        $tname = Ethnicity::remember(5)->find($this->ethnicity_id);
        $url .= isset($tname) && $tname != '' ? $tname->name.'-' : '';
        // $thcname = HairColor::remember(5)->find($this->hair_color_id);
        $url .= isset($thcname) && $thcname->name != '' ? $thcname->name.'-' : '';
        $url .= $this->id;

        $url = str_replace(' ', '-', $url);
        $url = str_replace('.', '', $url);
        $url = trim($url);
        $url = strtolower($url);

        return $url;
    }

    public static function multiple_filter($array,$table,$column,$query)
    {
        // // Convert the actual elements into "elements_id" for the escorts table
        $table_data = DB::table($table)->get(['name','id']);
        foreach ($array as $key => $value) {
            foreach ($table_data as $row) {
                if ($row->name == $value)
                    $ids[] = $row->id;
            }
        }
        $usables['ids'] = $ids;
        $usables['column'] = $column;

        // Add the where and orWhere queries
        $query = $query->where(function($query) use ($usables) {

            $query->where($usables['column'],'=',$usables['ids'][0]);
            array_shift($usables['ids']);

            foreach ($usables['ids'] as $i => $id) {
                $query = $query->orWhere($usables['column'],'=',$id);
            }
        });

        return $query;
    }

    /**
     * @return string
     */
    public function getRatesHtml()
    {
        $out = "";

        if ($this->incallRates()->count() > 0 || $this->outcallRates()->count() > 0) {
            $out .= '<div class="row" style="margin-left:0px;margin-right:0px">';

            if ($this->incallRates()->count() > 0) {
                $in = '<div class="col-sm-6">';
                $in .= '<span style="font-weight: bold;">INCALL RATES:</span> <br/>';
                $contentIn = '';
                foreach ($this->incallRates as $rate) {
                    if ($rate->rate > 0) {
                        $contentIn .= '$'.$rate->rate.' for '.$rate->service.'<br/>';
                    }
                }
                $in .= $contentIn;
                $in .= "</div>";
                if (!empty($contentIn)) {
                    $out .= $in;
                }
            }

            if ($this->outcallRates()->count() > 0) {
                $allOut = '<div class="col-sm-6">';
                $allOut .= '<span style="font-weight: bold;">OUTCALL RATES:</span> <br/>';
                $contentOut = '';
                foreach ($this->outcallRates as $rate) {
                    if ($rate->rate > 0) {
                        $contentOut .= '$'.$rate->rate.' for '.$rate->service.'<br/>';
                    }
                }
                $allOut .= $contentOut;
                $allOut .= "</div>";

                if (!empty($contentOut)) {
                    $out .= $allOut;
                }
            }

            $out .= "</div>";
        }

        return $out;
    }

}
