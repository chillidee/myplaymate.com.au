<?php

class StaticBlocksTableSeeder extends Seeder
{

    public function run()
    {
        $staticCount = StaticBlock::whereIn('key', [
            'agency-multiple-csv',
            '404',
        ])->count();

        if ($staticCount == 0) {
            // Uncomment the below to wipe the table clean before populating
            $static_blocks = [
                [
                    'key' => 'agency-multiple-csv',
                    'name' => 'Explain CSV',
                    'value' => '',
                ],
                [
                    'key' => '404',
                    'name' => '404',
                    'value' => '<div class="col-md-4"><h2 class="error-big">404</h2><h3>OOP! PAGE NOT FOUND</h3></div><div class="col-md-4"><h4>HERE ARE SOME HELPFUL LINKS</h4><ul><li><a><i class="fa fa-play-circle-o"></i>Creative template with endless possibilites</a></li><li><a><i class="fa fa-play-circle-o"></i>Unique &amp; clean design. Awesome features and super</a></li><li><a><i class="fa fa-play-circle-o"></i>Super features with awesome elements. Try it now</a></li><li><a><i class="fa fa-play-circle-o"></i>E-commerce pages with great UX. You can launch your</a></li><li><a><i class="fa fa-play-circle-o"></i>Great Ui/UX Work, Tons of hours spend on this beautiful</a></li></ul></div><div class="col-md-4"><h4>WHAT CAN YOU DO NEXT</h4><p>You can go back to <a href="/" style="font-weight: bold;">Homepage</a><br /> or search what your looking for.</p></div>'
                ],
                [
                    'key' => 'social',
                    'name' => 'Social',
                    'value' => '<ul class=social-links><li><a href=https://twitter.com/MyPlayMateAU target=_new><i class="fa fa-twitter twitter-share-button" data-lang=en></i></a></li> <li><a href=https://www.facebook.com/MyPlayMateAU target=_new><i class="fa fa-facebook"></i></a></li> <!--<li><a href="https://plus.google.com/share?url={{ Request::url() }}"><i class="fa fa-google-plus"></i></a></li><li><a href="http://pinterest.com/pin/create/button/?url={{ Request::url() }}&media=&description=" target="_new" class="pin-it-button" count-layout="horizontal"><i class="fa fa-pinterest"></i></a></li>--></ul> <!-- .social-links end -->'
                ],
                [
                    'key' => 'homepage-banner',
                    'name' => 'Homepage banner',
                    'value' => '<div class=banner-wrapper><!-- START REVOLUTION SLIDER 4.1.4 fullscreen mode --> <div id=rev_slider class="rev_slider_wrapper fullscreen-container"> <div id=frontpage-banner class="rev_slider fullscreenbanner" style="display: none;"> <ul><!-- SLIDE 1  --> <li data-transition=random data-slotamount=7 data-masterspeed=300><!-- MAIN IMAGE --> <img src=assets/frontend/img/bgslider/slider_7.jpg alt=slider_7 data-bgposition="center top" data-bgfit=cover data-bgrepeat=no-repeat> <!-- LAYERS --> <!-- LAYER NR. 1 --> <div class="tp-caption medium_light_white lfl tp-resizeme" data-x=14 data-y=16 data-speed=1100 data-start=1700 data-easing=easeOutBack data-endspeed=300 style="z-index: 2;">AN UNFORGETTABLE EXPERIENCE</div> <!-- LAYER NR. 3 --> <div class="tp-caption small_light_white lfl tp-resizeme" data-x=19 data-y=203 data-speed=1100 data-start=2100 data-easing=easeOutBack data-endspeed=300 style="z-index: 4; background: rgba(0,0,0,0.6); padding: 15px;">Welcome to My Playmate! Showcasing Australia\'s escorts, escort agencies and high-class brothels<br>as you\'ve never seen them before. View galleries of adult services providers inviting you to live out your fantasies<br>and explore your desires. Easy to search &amp; find the sexiest escorts &hellip;.</div> <!-- LAYER NR. 4 --> <div class="tp-caption lfl tp-resizeme" data-x=20 data-y=303 data-speed=1300 data-start=2300 data-easing=easeOutBack data-endspeed=300 style="z-index: 5;"><a href=/escorts class="button small-button">Discover More</a></div> </li> <!-- SLIDE 2 --> <li data-transition=random data-slotamount=7 data-masterspeed=500><!-- MAIN IMAGE --> <img src=assets/frontend/img/bgslider/slider_8.jpg alt=slider_8 data-bgposition="center top" data-bgfit=cover data-bgrepeat=no-repeat> <!-- LAYERS --> <!-- LAYER NR. 2 --> <div class="tp-caption small_light_white lfl tp-resizeme" data-x=19 data-y=203 data-speed=1100 data-start=2100 data-easing=easeOutBack data-endspeed=300 style="z-index: 3; background: rgba(0,0,0,0.6); padding: 15px;">Is there a specific indulgence you\'re looking for? Whether it\'s a Swedish blonde escort in Sydney,<br> an educated geek goddess in Melbourne, or an exotic Asian beauty in Brisbane,<br>My Playmate showcases over 700 escorts Australia-wide. Browse at your leisure &hellip;.</div> <!-- LAYER NR. 3 --> <div class="tp-caption lfl tp-resizeme" data-x=20 data-y=303 data-speed=1300 data-start=2300 data-easing=easeOutBack data-endspeed=300 style="z-index: 4;"><a href=/escorts class="button small-button">Discover More</a></div> </li> <!-- SLIDE 3  --> <li data-transition=random data-slotamount=7 data-masterspeed=500><!-- MAIN IMAGE --> <img src=assets/frontend/img/bgslider/slider_9.jpg alt=slider_9 data-bgposition="center top" data-bgfit=cover data-bgrepeat=no-repeat> <!-- LAYERS --> <!-- LAYER NR. 2 --> <div class="tp-caption small_light_white lfl tp-resizeme" data-x=19 data-y=203 data-speed=1100 data-start=2100 data-easing=easeOutBack data-endspeed=300 style="z-index: 3; background: rgba(0,0,0,0.6); padding: 15px;">Every week thousands of visitors come to My Playmate to explore their erotic imagination,<br>sexual curiosity or need for absolute satisfaction.Escape the humdrum of everyday life<br>as our parade of eye candy enables you to exact-match your thirst for pleasure. Visit whenever you wish ...</div> <!-- LAYER NR. 3 --> <div class="tp-caption lfl tp-resizeme" data-x=20 data-y=303 data-speed=1300 data-start=2300 data-easing=easeOutBack data-endspeed=300 style="z-index: 4;"><a href=/escorts class="button small-button">Discover More</a></div> </li> </ul> </div> </div> <!-- End Of Rev-Slider --></div> <!-- Banner Wrapper -->'
                ],
                [
                    'key' => 'call-to-action',
                    'name' => 'Call to action',
                    'value' => '<!-- About Section --> <div class=page-content id=frontpage-about> <div class=container> <div class=row> <div class=col-md-12> <div class="heading-centered triggerAnimation animated" data-animate=pulse></div> </div> </div> <!-- .row end --> <div class=row> <div class="col-md-12 alignc"><span class=bold>MYPLAYMATE.COM.AU</span><br> <h1>YOUR FAVOURITE ESCORT <span class=branding-color>DIRECTORY</span></h1> <div id=about-content> <p>Found us using the searches escorts sydney, brothels melbourne, hookers brisbane, sex workers perth, adelaide, canberra or other terms? You\'ve come to the right place!</p> <p>Australia\'s libertarian democracy means across our country, despite <a href=" http://www.scarletalliance.org.au/laws/" target=_blank>variances</a> between states, sexual services are available within various restrictions sometimes local council based such as the ability for premises to operate in commercial/urban or industrial areas rather than housing.&nbsp;</p> <p>Independent escorts have even greater freedom as they\'re able to offer out-calls which means they can visit private dwellings to provide adult services.</p> <p>Protecting these freedoms for adults to explore their sexuality are a number of laws - and there are also adult service groups such as the <a href=" https://www.sexparty.org.au/" target=_blank>Australian Sex Party</a> and the <a href=http://www.eros.com.au target=_blank>Eros Foundation</a> who are both guardians and lobby groups allowing Australian adults the right to engage in consensual sex of their choosing.</p> <p>Escorts, agencies &amp; brothels are allowed to operate in most large Australian urban areas such as Sydney, Melbourne, Adelaide, Perth, Brisbane, the Gold Coast, Sunshine Coast, Canberra, Darwin, Hobart, Surfers Paradise, Cairns, Geelong, Kalgoorlie, Newcastle, Townsville, Wagga Wagga, and many smaller towns in NSW, Victoria, Queensland, WA, SA, Tasmania &amp; the Northern Territory.</p> <p>Urban areas within the larger cities which are most renown for offering escorts and brothels are Surry Hills, Chatswood, Crows Nest, Newtown, North Sydney, Fortitude Valley, South Melbourne, Kangaroo Point etc</p> <p>If you\'re an escort, register with My Playmate by calling Tami on 1300 769 766 or text 0428 258 671. Or email&nbsp;<a href=mailto:contact@myplaymate.com.au>contact@myplaymate.com.au</a></p> </div> <div id=about-readmore class=closed><a class="triggerAnimation animated vote-now"><i class="fa fa-angle-double-down"></i>Read More</a></div> </div> <!-- .col-md-12 end --></div> <!-- .row end --></div> <!-- .container end --></div> <!-- .page-content end -->'
                ],
                [
                    'key' => 'homepage-about',
                    'name' => 'Homepage banner',
                    'value' => '<h2>ESCORTS IN SYDNEY, MELBOURNE, BRISBANE &amp; More ...</h2><div class="divider"></div><p>We now have over 700 escorts profiles from around Australia. Recently we\'ve welcomed aboard Sydney escort Lia, Parramatta escort Anne, Perth escort Amber McQueen, Adelaide escort Rose Brown, Canberra escort Stacey Adams, Brisbane escort Anika Adams, Surry Hills escort Kristy and Melbourne escort Cougar Alexandrah</p>'
                ],
                [
                    'key' => 'footer-copyright',
                    'name' => 'Footer copyright',
                    'value' => '<div class=row> <div class="col-md-6 clearfix"> <p>Copyright &copy; 2015 <a href="/">myplaymate.com.au</a> All rights reserved</p> </div> <!-- .col-md-6 end --> <div class="col-md-6 clearfix"> <ul class="breadcrumb footer-breadcrumb"> <li><a href="/">Home</a></li> <li><a href="/escorts">Escorts</a></li> <li><a href="/brothels">Brothels</a></li> <li><a href="/escort-agencies">Agencies</a></li> <li><a href="/erotic-massage/massage-parlour">Massage</a></li> <li><a href="/blog">Blog</a></li> <li><a href="/contact">Contact</a></li> </ul> </div> <!-- .col-md-6 end --></div> <!-- .row end -->'
                ],
                [
                    'key' => 'footer-col-1',
                    'name' => 'Footer column 1',
                    'value' => '<ul> <li class="widget widget_text"><span class=h5>ABOUT <span class=branding-color>MYPLAYMATE.COM.AU</span></span> <img class=float-left src=/assets/frontend/img/logo-small.png alt="" height=35 width=118> <p>Welcome to My Playmate, Australia’s hottest new adult services website showcasing escorts in Sydney, Melbourne, Brisbane, Perth, Adelaide & regional areas. If you\'re looking for independent escorts, fantasy escorts, high-class escorts, male escorts or transexual escorts, you\'ll find an escort to suit your sexual preferences and interests.</p> <p></p> </li> </ul> <!-- .widget.widget_text end --> <ul> <li class="widget widget_text"> <p></p> </li> </ul> <!-- .widget.widget_text end -->'
                ],
                [
                    'key' => 'footer-col-3',
                    'name' => 'Footer column 3',
                    'value' => '<ul class="col-md-3 footer-widget-container"> <li class="widget widget_text widget_contact"><span class=h5>ESCORTS IN YOUR CITY</span> <ul class=footer-links> <li><a href=/city/brisbane>Brisbane</a></li> <li><a href=/city/sydney>Sydney</a></li> <li><a href=/city/canberra>Canberra</a></li> <li><a href=/city/melbourne>Melbourne</a></li> <li><a href=/city/hobart>Hobart</a></li> <li><a href=/city/adelaide>Adelaide</a></li> <li><a href=/city/perth>Perth</a></li> <li><a href=/city/darwin>Darwin</a></li> </ul> </li> <!-- .widget#tweetscroll-wrapper end --></ul>'
                ],
                [
                    'key' => 'footer-col-4',
                    'name' => 'Footer column 4',
                    'value' => '<ul class="col-md-3 footer-widget-container"> <li class="widget widget_text widget_contact"><span class=h5>LINKS</span> <ul class=footer-links> <li><a href=/about>About MyPlaymate</a></li> <li><a href=/blog>Blog</a></li> <li><a href=/advertiser-terms-and-conditions>Advertiser terms and Conditions</a></li> <li><a href=/faqs>FAQS</a></li> <li><a href=/billing-and-refund-policy>Billing and Refund Policy</a></li> <li><a href=/client-terms-and-conditions-of-use>Client Terms and Conditions of Use</a></li> <li><a href=/links>Links</a></li> <li><a href=/member-terms-and-conditions-of-use>Members Terms and Conditions of Use</a></li> <li><a href=/privacy-policy>Privacy Policy</a></li> <li><a href=/contact>Contact Us</a></li> <li><a href="/report">Report an escort</a><br><br></li> </ul> </li> <!-- .widget.widget_text end --></ul>'
                ],
                [
                    'key' => 'contact-info',
                    'name' => 'Contact info',
                    'value' => '<p>Hi, my name is Tami and I am the brand ambassador for My Playmate.</p> <p>My role is to make sure you and your business are looked after, and I am your point of contact 24/7.</p> <p>&nbsp;</p> <p>Call me: 1300 769 766</p> <p>Text me: 0428 258 671</p> <p>Email me: <a href=mailto:contact@myplaymate.com.au>contact@myplaymate.com.au</a></p> <p><a target=_blank href=https://www.facebook.com/MyPlayMateAU>Facebook</a></p> <p><a target=_blank href=https://twitter.com/MyPlayMateAU>Twitter</a></p> <p><a target=_blank href=http://instagram.com/myplaymateau#>Instagram</a></p>'
                ],
            ];

            StaticBlock::insert($static_blocks);
        }
    }
}
