<div class ='_MpoffWhite innerPadding'>
<reviewmessage></reviewmessage>
<h3 class ='reviews'>{{$escort->escort_name}}'s Reviews</h3>
   @foreach ( $reviews as $review )
     @if ( $review->status == 2 )
     <div class ='review_outer'>
         <div class ='review_left'>
            Review Status:<br>
            <?php switch ($review->published) {
                   case '0':
                      $label = 'Unpublished';
                      $button = 'Publish';
                      $buttonAction = '1';
                      $altAction = '0';
                      $altId = 'Unpublish';
                   break;
                   case '1':
                      $label = 'Published';
                      $button = 'Unpublish';
                      $buttonAction = '0';
                      $altAction = '1';
                      $altId = 'Publish';
               break; 
               };
               ?>
            {{$label}}
            <span id = "_mpStatusIcon_{{$review->id}}" class="_mpStatus mp{{$button}}"></span>
            <span>Posted by: {{ $review->name }}</span><br>
            <span>Date: {{ date ( 'd/M/Y',strtotime ( $review->created_at )) }}</span>
         </div>
         <div class ='review_right'>
         <div id ='stars'>
         <?php for( $x = 0; $x < $review->rating; $x++ ){ 
          echo '<span class ="ratingStar"></span>';
         }?>
         </div>
                  <button id ='reviewAction_{{$review->id}}' ng-click = 'reviewAction("{{$button}}", "{{$review->id}}","{{$buttonAction}}","{{$altAction}}","{{$altId}}")' class ='button_{{$button}}'>{{$button}}</button>
                  <p>{{$review->review}}</p>
     </div>
     </div>
    @endif
     @endforeach     </div>
