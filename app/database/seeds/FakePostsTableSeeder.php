<?php

class FakePostsTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,4) as $index) {
            $title = $faker->sentence(5);
            $url = Helpers::string_to_url($title);
            Post::create([
                'thumbnail'         => 'post_'.$index.'.jpg',
                'title'             => $title,
                'seo_url'           => $url,
                'user_id'           => $faker->numberBetween(1,5),
                'short_content'     => $faker->text(200),
                'content'           => $faker->text(3000),
                'user_id'           => $faker->numberBetween(1,5),
                'date_from'         => $faker->dateTimeBetween('-1 year', '+1 month'),
                'date_to'           => $faker->dateTimeBetween('-1 month','+1 year'),
                'published'         => 1
            ]);
        }
    }
}
