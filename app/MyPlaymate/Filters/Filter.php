<?php
namespace MyPlaymate\Filters;

use MyPlaymate\Interfaces\FilterableInterface;

abstract class Filter implements FilterableInterface
{
	protected $column;

	public function apply($escorts,$filterType)
	{
		$column = $this->column;
		return array_filter($escorts,function($escort) use($filterType,$column)
		{
			return  in_array($escort[$column], $filterType);
		});
	}
}