<div class="modal fade backdrop auth-modal" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-content signup-container">
    <div class="">
      <div class="modal-header">
        {{--<img src="/assets/frontend/img/logoblack.jpg" alt="MyPlaymate. The place to be.">--}}
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <p class ='header'>
			Welcome to MyPlaymate.com.au - Sign Up Now!
	  	</p>
      </div>
      <div class ='tab_headers'>
        <?php $type = Session::get('type');
              $theType = $type['type'];  ?>
        <a id ='tb_escort' @if ( $theType == 3 || $theType == '' )class ='tb_active' @endif></a>
        <a id ='tb_business' @if ( $theType == 4 )class ='tb_active' @endif></a>
        <a id ='tb_customer' @if ( $theType == 2 )class ='tb_active' @endif></a>
      </div>
      <div class ='tb_tabs'>
		<div class ='col-md-6 large_only' id ='tb_esc_tab'>
            <?php /*----------------------------------------------------------------------------------------------------------
            -                                               Escort Tab
            ------------------------------------------------------------------------------------------------------------------*/?>
            My Playmate gives you the tools you need to self-manage every feature of your personal business. Take full control by choosing how to work and who to work with and give your clients an unforgettable experience. Create your profile, upload your sexiest images and get noticed by over 134,000 clients that use our platforms every month.<br><br>
                        
			<div class="row" style="padding: 5px 0">
            	<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: 5px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Favourite.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">BUILD A REGULAR CLIENT BASE</span>
						<p>
							Grow your business with our ‘Add to Favourites’ feature. This feature allows clients to add you to their favourites list and it helps them easily get in touch with you at any time.
						</p>
					</div>
				</div>
			</div>
			
			<div class="row" style="padding: 5px 0">
            	<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Support.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">WE’RE HERE FOR YOU</span>
						<p>
							We are here to help if you are in need of support creating your page(s), updating your profile or if you have general questions. <a href="https://myplaymate.com.au/contact" style="color: #F55">Get in touch here</a>.	
						</p>
					</div>
				</div>
			</div>
			
			<div class="row" style="padding: 5px 0">
            	<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Mail.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">PLAYMAIL</span>
						<p>
							Playmail lets your potential clients get in touch with you easily via email.
						</p>
					</div>
				</div>
			</div>
			
			<div class="row" style="padding: 5px 0">
            	<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 5px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Price.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">PRICING</span>
						<p>
							For more information on our features and pricing, <a href="https://myplaymate.com.au/escort-pricing" style="color: #F55">click here</a>.
						</p>
					</div>
				</div>
			</div>
		  </div>
		  
               <?php /*----------------------------------------------------------------------------------------------------------
            -                                               Business Tab
            ------------------------------------------------------------------------------------------------------------------*/?>
		<div class ='col-md-6 large_only' id ='tb_bus_tab'>
              	My Playmate lets you showcase your hottest talents on your business page - feature your star escorts or your most talented masseurs. List your escort agency, brothel or massage parlour and be noticed by over 134,000 clients in Australia that use our platforms every month.<br><br>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: 5px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Talents.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">SHOWCASE YOUR TALENTS</span>
						<p>
							Thousands of visitors come to MyPlaymate to explore their erotic imaginations and sexual fantasies with a need to satiate their appetite. Your business profile lets you manage up to 20 profiles – showcasing their looks, services and availability.
						</p>
					</div>
				</div>
			</div>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Support.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">WE’RE HERE FOR YOU</span>
						<p>
							We are here to help if you are in need of support creating your page(s), updating your profile or if you have general questions. <a href="https://myplaymate.com.au/contact" style="color: #F55">Get in touch here</a>.	
						</p>
					</div>
				</div>
			</div>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Mail.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">PLAYMAIL</span>
						<p>
							Playmail lets your potential clients get in touch with you easily via email.
						</p>
					</div>
				</div>
			</div>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 5px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Price.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">PRICING</span>
						<p>
							For more information on our features and pricing, <a href="https://myplaymate.com.au/escort-pricing" style="color: #F55">click here</a>.
						</p>
					</div>
				</div>
			</div>
           </div>
               <?php /*----------------------------------------------------------------------------------------------------------
            -                                               Member Tab
            ------------------------------------------------------------------------------------------------------------------*/?>
            <div class ='col-md-6 large_only' id ='tb_cust_tab'>
              Browse a huge selection of Australian escorts, agencies, brothels, and massage parlours around Australia. Easily search for and contact any of the escorts or establishments listed on MyPlaymate and satiate your appetite.<br><br>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: 5px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Favourite.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">SAVE YOUR FAVOURITES</span>
						<p>
							Had an unforgettable experience with an escort or adult establishment and want to make sure you can find them again? Add them to your favourites and have easy access to them.
						</p>
					</div>
				</div>
			</div>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Support.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">WE’RE HERE FOR YOU</span>
						<p>
							Run into trouble with our site? Have a feature that you’d like to see? Have any questions about the features of our site? <a href="https://myplaymate.com.au/contact" style="color: #F55">Get in touch here</a>.	
						</p>
					</div>
				</div>
			</div>

			<div class="row" style="padding: 5px 0">
				<div class="col-sm-12 large_only" style="border-bottom: 1px solid #CCC; margin-top: -65px; padding-bottom: 10px;">
					<div class="col-sm-2 large_only">
						<img src="https://myplaymate.com.au/assets/frontend/icons/Signup_Mail.png">
					</div>
					<div class="col-sm-10 large_only">
						<span style="color:#F55">PLAYMAIL</span>
						<p>
							Playmail lets you easily get in contact with any of our escorts or adult establishments listed on our website.
						</p>
					</div>
				</div>
			</div>
        </div>
           
		<div class='col-md-6'>
             <div class="modal-body">
              {{ Form::open(array('route' => 'postRegister','id'=>'signup-form')) }}

              {{ Form::text('type',( $theType == '' ? '3' : $theType ),array('class'=>'hidden','id'=>'typeSelect','required')) }}


              <div class="control-group">
                <div class="controls">
                  {{ Form::text('full_name','',array('data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Escort Name','required')) }}
                </div>
              </div>
              @if($errors->has('full_name'))
              <p class="form-error">
                {{ $errors->first('full_name') }}
              </p>
              @endif
              <div class="control-group">
                <div class="controls">
                  {{ Form::text('phone','',array('data-parsley-pattern-message' => 'Must be a valid phone number!','data-parsley-length'=>'[8,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Phone','required')) }}
                </div>
              </div>
              @if($errors->has('phone'))
              <p class="form-error">
                {{ $errors->first('phone') }}
              </p>
              @endif
              <div class="control-group">
               <div class="controls">
                 <?php $pattern = "[^@]+@[^@]+\.[^@]+";?>
                 {{ Form::text('email','',array('data-parsley-pattern-message' => 'Must be a valid email address!','type' => "email",'data-parsley-type'=>'email','pattern'=>$pattern,'data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Email','required')) }}
               </div>
             </div>
             @if($errors->has('email'))
             <p class="form-error">
               {{ $errors->first('email') }}
             </p>
             @endif
             <div class="control-group">
              <div class="controls">
                {{ Form::password('password',array('id'=>'password1','data-parsley-group' => 'loginForm','data-parsley-equalto-message' => 'Passwords must match!','data-parsley-equalto' => '#password2','class'=>'form-control','placeholder'=>'Password','required')) }}
              </div>
            </div>
            @if($errors->has('password'))
            <p class="form-error">
              {{ $errors->first('password') }}
            </p>
            @endif
            <div class="control-group">
              <div class="controls">
               {{ Form::password('password_confirmation',array('data-parsley-group' => 'loginForm','class'=>'form-control','placeholder'=>'Repeat password','required','id'=>'password2')) }}
             </div>
           </div>

           <div id="terms-and-conditions" class="checkbox">
            <label for="terms_and_conditions">
              <input type="checkbox" name="terms_and_conditions" required data-parsley-required-message = 'You must accept the terms and conditions'/> By ticking the box, you confirm that you are over the age of eighteen (18) years and agree to our <a href="httpss://myplaymate.com.au/advertiser-terms-and-conditions" target="_blank">Terms &amp; Conditions</a>.
            </label>
          </div>
          @if($errors->has('terms_and_conditions'))
          <p class="form-error">
            {{ $errors->first('terms_and_conditions') }}
          </p>
          @endif

          <div class="control-group">
            <div class="controls">
              {{ Form::submit('Get Started',array('class'=>'submit-button', 'id'=>'register-submit')) }}
            </div>
          </div>

          <div class="alignc">
            <a class="swap-modal" href="#login" data-dismiss="modal" data-toggle="modal">Existing Member? Login Here!</a>
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
</div>
</div>



