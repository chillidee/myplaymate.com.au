@extends('admin.layout')

@section('javascript')

   <script type="text/javascript">
        (function($){
            $('textarea.tinymce').tinymce();
        })(jQuery);
    </script>

@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-file-text-o"></span></div>
    <div class="pagetitle">
        <h1>New page</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
    <div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open() }}
<div class="row">
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('name','Name*',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('name','',array('class'=>'form-control','required')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_title','SEO title',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_title','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_keywords','SEO keywords',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_keywords','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('meta_description','SEO description',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('meta_description','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('url','URL',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::text('url','',array('class'=>'form-control')) }}
            </div>
        </div>
  </div>
    <div class="col-md-6">
        <div class="control-group">
            {{ Form::label('main_image','',array('class'=>'control-label')) }}

            <div class="controls">
              <input type="file" id="main_image" name ="main_image"/>
            </div>
        </div>
  </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="control-group">
            {{ Form::label('content','Content',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('content','',array('class'=>'tinymce')) }}
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
      <div class="control-group">
          {{ Form::label('img_alt','IMAGE ALT',array('class'=>'control-label')) }}

          <div class="controls">
              {{ Form::text('img_alt','',array('class'=>'form-control')) }}
          </div>
      </div>
</div>
    <div class="col-md-2">
        <div class="control-group" id="incalls">
            <h5>PUBLISHED</h5>
            <label class="radio-inline">{{ Form::radio('published', '1', false) }} Yes
            </label>
            <label class="radio-inline">{{ Form::radio('published', '0', true) }} No
            </label>
        </div>
    </div>

<div class="alignc">
    {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
</div>

{{ Form::close() }}
@stop
