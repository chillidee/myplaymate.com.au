@extends('admin.layout')
@section('javascript')
	<script>
		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "/admin/ajax_pages",
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'name', name: 'name'},
		            {data: 'meta_title', name: 'meta_title'},
		            {data: 'meta_description', name: 'meta_description'},
		            {data: 'created_at', name:'created_at'  },
		            {data: 'updated_at', name:'updated_at'  },
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });
		});
	</script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-file-text-o"></span></div>
    <div class="pagetitle">
        <h1>Pages</h1>
    </div>
</div><!--pageheader-->

<div class="row">
	<a href="/admin/pages/new" class="create-user btn btn-success btn-lg pull-right section-content">Create page</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Page name</th>
			<th>Meta title</th>
			<th>Meta description</th>
			<th>Creation</th>
			<th>Last update</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

@stop



