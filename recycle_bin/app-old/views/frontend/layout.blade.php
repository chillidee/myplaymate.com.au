<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>{{ isset($meta) ? $meta->meta_title : '' }}</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="{{ isset($meta) ? $meta->meta_description : '' }}">
    <meta name="keywords" content="{{ isset($meta) ? $meta->meta_keywords : '' }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    @if(isset($meta))
        <link rel="canonical" href="{{ url('/').$_SERVER['REQUEST_URI'] }}" />
    @endif

    {{ Helpers::latestCss('build.min.css') }}
    {{-- <link rel="stylesheet" href="/assets/frontend/css/style.css"> --}}
    @yield('css')
    
    <!--[if lte IE 8]>
    <link href="{{ url('/assets/frontend/css/ie.css') }}" rel="stylesheet" />
    <![endif]-->

    @include('frontend.partials.head.favicons')

    <meta name="google-site-verification" content="p5uVVgW7ZgDrR-1N6jgNUGnr7zhjLNKBZ1aNWFwc1zo" />

</head>
<body {{ isset($bodyClass) ? 'class="'.$bodyClass.'"' : '' }}>
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

        <button class="showLeftPush subMenu"><i class="fa fa-bars"></i></button>

        <div id="sidemenu-search" class="side-items">
            <h4>Search</h4>
            <form action="/escorts" method="GET" name="search">
                <input id="side-search" name="s" type="text" placeholder="Type and hit enter..." autocomplete="off"/>
            </form>
        </div>

        @include('frontend.partials.topbar.mobile-user-box')

        @include('frontend.partials.topbar.mobile-menu')
    </nav>

    <div id="mpm-header-wrapper" class="<?php echo ($_SERVER['REQUEST_URI'] == '/') ? 'header-banner' : 'header-simple'; ?>">
        <div id="top-bar">
            <div class="container">
                <div class="hidden-xs">
                    {{ StaticBlock::getHtml('social') }}                
                </div>

                @include('frontend.partials.topbar.desktop-user-box')

            </div>
        </div>

        <header id="header" class="<?php echo ($_SERVER['REQUEST_URI'] == '/') ? 'header-style-01' : 'header-style-02'; ?>">
            <!-- .container start -->
            <div class="container">

                <div id="mobile-menu" class="hidden-lg">
                    <button class="showLeftPush"><i class="fa fa-bars"></i></button>
                </div>

                <div id="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ url('assets/frontend/img/logo.png') }}" alt="Myplaymate.com.au. the place to be"/>
                    </a>
                </div>
                <div id="toggleFilters">Filters</div>
                    <div id="geoloc">
                        <div id="trigger-overlay">
                            <div id="city" class="clearfix">
                                <i class="fa fa-map-marker"></i>
                                <a></a>
                            </div>
                            <div id="city-changer">
                                <p>Change your location</p>
                            </div>
                        </div>
                    </div>
            </div><!-- .container end -->

            <div id="main-nav" >
                <div class="container" style="position:relative;">
                    <nav class="navbar navbar-default" role="navigation">

                        @include('frontend.partials.topbar.desktop-menu')

                        @include('frontend.partials.topbar.buttons')
                    </nav>
                </div>
            </div>
        </header><!-- #header end -->
    </div><!-- #header-wrapper end -->

    <div id="center" class="{{ ($_SERVER['REQUEST_URI'] == '/') ? 'homeview' : 'view'; }}">
        <section>
            @yield('content')            
        </section>
    </div>

    <!-- Footer Area -->
    <div id="footer-wrapper">
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <ul class="col-md-3 col-sm-6 footer-widget-container">
                        {{ StaticBlock::getHtml('footer-col-1') }}

                        @include('frontend.partials.footer.social')
                    </ul>

                    @include('frontend.partials.footer.posts')

                    {{ StaticBlock::getHtml('footer-col-3') }}

                    {{ StaticBlock::getHtml('footer-col-4') }}
                </div>
            </div>
        </footer>

        <div id="copyright-container">
            <div class="container">
                 {{ StaticBlock::getHtml('footer-copyright') }}
            </div>
        </div>

        <a href="#" class="scroll-up">Scroll</a>
    </div><!-- .footer-wrapper end -->

    <div class="modal fade backdrop" id="geoMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ></div>
    
    <div class="overlay"></div>
    @include('frontend.partials.modals.login')
    @include('frontend.partials.modals.signup')
    @include('frontend.partials.modals.registration-success')

    @include('frontend.partials.modals.welcome')

    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    {{ Helpers::latestJs('build.min.js') }}

<script>
    jQuery(document).ready(function($) {
        $('.is-not-wish').show();
        @if(Auth::check())
            var userId = {{ Auth::user()->id }};
            getUserWishlist(userId);
            $('body').on('click', '.is-not-wish', function(e) {
                addToWishlist(e, $(this), userId);
            });
            $('body').on('click', '.is-wish', function(e) {
                removeFromWishlist(e, $(this), userId);
            });
        @else
            $('.is-not-wish').show();
            $('.is-not-wish').attr({'href':'#login','data-toggle':'modal'});
        @endif

        if ('{{ Session::get('loginFailed') }}' == 'failed')
            $('#login').modal('show');
        if ('{{ Session::get('registrationFailed') }}' == 'failed' || window.location.hash == '#signup')
            $('#signup').modal('show');
        if ('{{ Session::get('registrationSuccess') }}' == 'success')
            $('#registration-success').modal('show');
    });
</script>

@yield('javascript')

@yield('modal')

</body>
</html>
