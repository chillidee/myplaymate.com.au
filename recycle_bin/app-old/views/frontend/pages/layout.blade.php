@extends('frontend.layout')

@section('content')

	<!-- #page-title start -->
	<section id="page-title" class="page-title-1">
	    <div class="container">
        <h1>{{ strtoupper($title) }}</h1>
	        <div class="breadcrumb-container">
	            <span>You are here: </span>
	            <ul class="breadcrumb">
	                <li><a href="{{ url('') }}">HOME</a></li>
                <li><a href="#">{{ strtoupper($title) }}</a></li>
	            </ul><!-- .breadcrumb end -->
	        </div><!-- .breadcrumb-container end -->
	    </div><!-- .containr end -->
	</section><!-- #page-title end -->


	<section class="page-wrapper top-space">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<ul class="aside-widgets">
	    				@include('frontend.partials.sidebar.left-common-widgets')
					</ul><!-- .aside-widgets end -->
				</div>

				<div class="col-md-9">
					{{ $meta->content }}
				</div>
			</div>
		</div>	
	</section>
	




@stop