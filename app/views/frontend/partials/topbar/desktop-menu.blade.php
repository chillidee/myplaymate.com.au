<div>
    <ul class="nav navbar-nav">
        <li>
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li>
            <a href="{{ route('escorts') }}">Escorts</a>
            <span class ='downCaret mob_only'></span>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ url('/escorts-sydney') }}">Sydney Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-melbourne') }}">Melbourne Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-brisbane') }}">Brisbane Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-perth') }}">Perth Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-adelaide') }}">Adelaide Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-canberra') }}">Canberra Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-darwin') }}">Darwin Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-hobart') }}">Hobart Escorts</a>
                </li>
                <li>
                    <a href="{{ url('/escorts-goldcoast') }}">Gold Coast Escorts</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('brothels') }}">Brothels</a>
        </li>
        <li>
            <a href="{{ route('agencies') }}">Agencies</a>
        </li>
        <li>
            <a href="{{ route('massage') }}">Massage</a>
            <span class ='downCaret mob_only'></span>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ url('/erotic-massage-sydney') }}">Massage in Sydney</a>
                </li>
                <li>
                    <a href="{{ url('/erotic-massage-melbourne') }}">Massage in Melbourne</a>
                </li>
                <li>
                    <a href="{{ url('/erotic-massage-brisbane') }}">Massage in Brisbane</a>
                </li>
                <li>
                    <a href="{{ url('/erotic-massage-perth') }}">Massage in Perth</a>
                </li>
                <li>
                    <a href="{{ url('/erotic-massage-adelaide') }}">Massage in Adelaide</a>
                </li>
                <li>
                    <a href="{{ url('/erotic-massage-gold-coast') }}">Massage in Gold Coast</a>
                </li>
                <li>
                    <a href="{{ url('/erotic-massage-canberra') }}">Massage in Canberra</a>
                </li>
            </ul>
        </li>
        <li id="trigger-overlay-mobile"  class ='mob_only'>
            <a>Choose Location</a>
        </li>
        <li class ='mob_only'>
          @if(Auth::check())
          <a href="/user/profile">Your Account</a>
          <a class="dash-mobile-menu-signout" href="/logout">Sign Out</a>
          @else
          <li class="login-li hidden-lg mob_only">
            <a href="#login" data-toggle="modal" id ='loginButton' style="float: left;width: 50%;border-right: 1px solid #666">Login</a>
            <a href="#signup" data-toggle="modal" id ='signupButton' style="float: left;width: 50%;">Signup</a>
        </li>
        @endif
        <li>
            <a href="{{ url('escort-media') }}">Media Services</a>
        </li>                            
        <li>
            <a href="{{ route('blogIndex') }}">Blog</a>
        </li>
        <li class ='desk_only'>
            <a href="{{ url('contact') }}">Contact</a>
        </li>
        <li class ='mob_only'>
          <a href='tel:1300769766' id ='phone_link'>1300 769 766</a>
      </li>
      <li class ='mob_only'>
          <a href='mailto:admin@myplaymate.com.au' id ='email_link'>admin@myplaymate.com.au</a>
      </li>
  </ul>