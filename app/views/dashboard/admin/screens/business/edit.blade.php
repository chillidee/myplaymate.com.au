<div id ='overlayBackGround'></div>
<div class ='_MpoffWhite businessProfileScreen'>
 <div>   
   <div class ='mPProfileTopMenu'>
     <div><div id ='mpBasicInfo' class ='active'></div><span>ACCOUNT</span></div>
     <div><div id ='mpProfileDetails'ng-click = 'getBusinessImages()'></div><span>PROFILE</span></div>
     <div><div id ='mpSeo'></div><span>SEO</span></div>
   </div>
 </div>
 <div class ='_mpProfile_holder business_profile_form'>
  <message></message>
  <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
   <h3>ACCOUNT</h3>
   <p>Please fill in the business details below. Please note that information provided here will be available publicly on our site.</p></br>
   <form role="form" controller ='busSaveBasic' ng-submit = 'saveBusBasicInfo()' id = 'businessInfo' ng-model = 'busBasicInfoForm'>
     <div id ='addImage' @if( !$business->image ) class ='noImage' @endif ngf-validate-fn="validate($file)" ngf-accept="'image/*'" ngf-select="galleryUpload($file)" ngf-drop = "galleryUpload($file)">
       <img ng-src ="{{Config::get('constants.CDN')}}/businesses/@{{busBasicInfoForm.image}}" alt ='Click Here to add logo'>
       <div id ='galleryLoading'></div>
     </div>
     <div class ='right_float'>
      {{ Form::hidden('image',$business->image,array('ng-model'=>'busBasicInfoForm.image','id' => 'image')) }}
	  <label>Business Name</label>
      {{ Form::text('name',$business->name,array('id' => 'name', 'class'=>'business-name input-xlarge input03','placeholder'=>'Business Name','ng-model'=>'busBasicInfoForm.name') ) }}
	  <label>Phone</label>
      {{ Form::text('phone',$business->phone,array('ng-model'=>'busBasicInfoForm.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,12]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'Business Phone')) }}
      <label>E-mail</label>
      {{ Form::text('email',$business->email,array('id' => 'email', 'class'=>'business-email input-xlarge input03','placeholder'=>'Business Email','ng-model'=>'busBasicInfoForm.email')) }}
	 
	 <div> 
		<span class="inputs_span">Website</span>
		{{ Form::text('website',$business->website,array('ng-model'=>'busBasicInfoForm.website','placeholder' => 'Website','class'=>'input-xlarge input03')) }}

		<span class="inputs_span">Business Type</span>
		{{ Form::select('type',$type, $business->type,array('id'=>'busType', 'ng-model' => "busBasicInfoForm.type") )}}			
	 </div>	
    </div>
	<div class="clear"></div>
    <div class ='bottom_inputs'>
	
	<label class ='showAdvanced'>Show advanced settings</label>
	 <div class ='advancedSettings'>
	  <label><a target ='_blank' href ='/admin/userprofile/@{{busBasicInfoForm.user_id}}'>Visit user dashboard</a></label><br>
	  <label>User Id</label>
	  {{ Form::text('user_id',$business->user_id,array('ng-model'=>'busBasicInfoForm.user_id','placeholder' => 'User id','class'=>'input-xlarge input03')) }}   
	  <div class="control-group" id="sex">
		<label>Please note that if you set status here, no email will be sent to the user.</label>
		<div></br></div>
		<div>
			{{ Form::radio('busBasicInfoForm.status','1','',array('id'=>'status1','ng-model' => 'busBasicInfoForm.status')) }}
			{{ Form::label('gender_id-1','Pending') }}
		</div>		
		<div>
			{{ Form::radio('busBasicInfoForm.status','2','',array('ng-model' => 'busBasicInfoForm.status'))}}
			{{ Form::label('gender_id-2','Approved') }}
		</div>
		<div>
			{{ Form::radio('busBasicInfoForm.status','3','',array('ng-model' => 'busBasicInfoForm.status'))}}
			{{ Form::label('gender_id-2','Disapproved') }}
		</div>
		<div>
			{{ Form::radio('busBasicInfoForm.status','4','',array('ng-model' => 'busBasicInfoForm.status'))}}
			{{ Form::label('gender_id-2','Unavailable') }}
		</div>
	  </div>                 
	</div>
	<div></br></div>
    <span>Human readable address</span>
    {{ Form::text('address_line_1',$business->address_line_1,array('ng-model'=>'busBasicInfoForm.address_line_1','placeholder' => 'Address line 1','class'=>'input-xlarge input03','id' => 'address_line_1')) }}

    {{ Form::text('address_line_2',$business->address_line_2,array('ng-model'=>'busBasicInfoForm.address_line_2','placeholder' => 'Address line 2','class'=>'input-xlarge input03','id' => 'address_line_2')) }}

    <span>Address on Google Maps</span>        
    <surburbselect suburbid = '{{ $business->suburb_id }}'></surburbselect>


    <div id="locationField">
     {{ Form::text('autocompleteField',$business->autocompleteaddress,array('class'=>'business-name input-xlarge input03', 'id' => 'autocomplete', 'onFocus' => 'geolocate()','placeholder'=>'Start typing your address here')) }}
   </div>
   <div id="map"></div>
   <div id ='address'>
    {{ Form::text('latitude',$business->latitude,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.latitude','id' => 'latitude')) }}


    {{ Form::text('longitude',$business->longitude,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.longitude','id' => 'longitude')) }}

    {{ Form::text('suburb_name',$business->suburb_name,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.suburb_name','id' => 'suburb_name')) }}

    {{ Form::text('state',$business->state,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.state','id' => 'state')) }}

    {{ Form::text('autocompleteaddress',$business->autocompleteaddress,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.autocompleteAddress','id' => 'autocompleteAddress'))}}



  </div>

  <div class="alignc">
    {{ Form::submit('NEXT',array('class'=>'submit-button')) }}
    <?php /* {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }} */?>

  </div>
  {{ Form::close() }}
</div>
</div>
<div class ='mPtab' id ='tab_mpProfileDetails'>
 <h3>BANNER</h3>
 <form role="form" ng-controller ='galleryController' ng-submit = 'saveprofileDetailsGal()' id = 'profileDetails' ng-model = 'profileGallery'>
     <?php /*   @if ( $business->image)
            <span>Click above image to change your profile cover</span>
        @else
            <span>Click above image to upload your profile cover</span>
        @endif
                    {{ Form::text('cover',$business->cover,array('ng-model'=>'busProfileDetails.cover','ng-init'=>"busProfileDetails.cover=" . json_encode ( $business->cover ), 'id' => 'cover','class'=>'hidden')) }}
*/?>
<section >
  <div id ='_cropSquare'>
    <div ngf-drop="" ngf-pattern="image/*" class="cropArea">
      <img-crop image="picFile  | ngfDataUrl" 
      result-image="croppedDataUrl" 
      ng-init="croppedDataUrl=''" 
      area-type="rectangle"
      aspect-ratio="3"
      on-load-begin="imageUpload()"
      on-change="imageChanged(image)"
      result-image-size="{ w:1200, h:400 }"
      on-load-error= "handleError()"
      ></img-crop>
    </div>
    <div id = "bannerUploadStatus"></div>
    <div id ='cropperInstructions'>
      Select the area you would like to use for your banner.  When you are done click the button below.
      <input class="submit-button" type="button" value="FINISH" ng-click = "upload()">
    </div>
  </div>

            <?php /*  <div sv-root sv-part="photosArray">
                    <div ng-repeat="photo in photosArray" sv-element>
                           <img draggable="false" ng-src="@{{photo.url}}" />
                   </div>
                 </div>*/?>
                 <p>The banner image will be displayed at the top of the business profile page. This is optional.</p>
   <?php /* <button id ="bannerUpload" ngf-validate-fn="validate($file)" ngf-accept="'image/*'" ngf-resize-if="$width > 3000 || $height > 5000"  ngf-resize="{width: 2000, quality: .8, type: 'image/jpeg', 
   ratio: '1:2', centerCrop: true, pattern='.jpg', restoreExif: true}"  ngf-min-width = "1600" ngf-max-size = "1.3MB" ngf-pattern="'.png, .jpg, .JPEG, .jpeg'" ngf-capture="'camera'" ngf-select="handleFileSelect($file)" ngf-drop = "handleFileSelect($file)" ng-model="picFile" accept="image/*"></button>*/?>
   <button id ="bannerUpload" ngf-drop = "handleFileSelect($file)" ngf-select="handleFileSelect($file)"></button>
   <div ng-click ='deleteBanner()' ng-class ="{'hidden' : ( !profileGallery.banner_image ),'submit-button' : ( profileGallery.banner_image ) }" id ='bannerDelete'>Delete Banner</div>
 </section>
 <h3 class="no-display-before">DESCRIPTION</h3>
 <p>You may provide further details about the business.</p>
 {{ Form::textarea('about',json_encode( $business->about ),array('id' => 'about', 'class'=>'about input-xlarge input03','placeholder'=>'About','ng-model'=>'profileGallery.about')) }}

 <div class="alignc next-previous">  
  <input class="submit-button previous-button" type="submit" value="PREVIOUS">
  {{ Form::submit('NEXT',array('class'=>'submit-button')) }}
  <?php /* {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }} */?>

</div>
{{ Form::close() }}
</div>



<div class ='mPtab' id = 'tab_mpSeo'>
  <h3>SEO</h3>
  <p>Give your site better SEO optimization that helps you rank higher and get discovered by search engines.</p>
  <form role="form" ng-submit = 'saveBusinessSeo()' ng-model = 'businessSeo' class ='profileForm normalSelects' id='profileSEO'>
   {{ Form::label('seo_title','Seo Title',array('class'=>'control-label')) }}
   {{ Form::text('seo_title',$business->seo_title,array('id' => 'seo_title', 'class'=>'business_name input-xlarge input03','placeholder'=>'Seo Title','ng-model'=>'businessSeo.seo_title')) }}
   {{ Form::label('seo_keywords','Tags above Photo',array('class'=>'control-label')) }}
   {{ Form::text('seo_keywords',$business->seo_keywords,array('id' => 'seo_keywords', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Seo Keywords','ng-model'=>'businessSeo.seo_keywords')) }}
   {{ Form::label('seo_description','Meta Description',array('class'=>'control-label')) }}
   {{ Form::text('seo_description',$business->seo_description,array('id' => 'seo_description', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Seo Description','ng-model'=>'businessSeo.seo_description')) }}
   {{ Form::label('seo_content','Business Tags',array('class'=>'control-label')) }}
   {{ Form::text('seo_content',$business->seo_content,array('id' => 'seo_content', 'class'=>'business_name input-xlarge input03','placeholder'=>'Seo Content','ng-model'=>'businessSeo.seo_content')) }}
   <div class="alignc next-previous">  
	<input class="submit-button previous-button" type="submit" value="PREVIOUS">   
	<button class="submit-button">FINISH</button>
   </div>
 </form>
</div>

<?php if( !$business->latitude  ) {
  $business->latitude = '-33.8674869';
  $business->longitude = '151.20699020000006';
} ?>
<script>

$(document).delegate('form', 'submit', function(event) {
	navigateProfile(this);
});

var action = '';

$(document).on("click", ":submit", function(e){				
	action = $(this).val();				
	if(action.toUpperCase() == '')
		action = 'NEXT';
});

function navigateProfile(control) {

	var $form = $(control);
	var id = $form.attr('id');
		
	if(action == 'NEXT') {
		if(id == 'businessInfo') {
			$('#mpProfileDetails').click();
		} else if(id == 'profileDetails') {					
			$('#mpSeo').click();
		}
	} else if (action == 'PREVIOUS') {
		if(id == 'profileDetails') {								
			$('#mpBasicInfo').click();				
		} else if(id == 'profileSEO') {					
			$('#mpProfileDetails').click();
		}
	}
}

 $( '#overlayBackGround').on('click',function() {
                   $( '#_cropSquare').hide();
                   $( this ).hide();
                 })
  $( '.showAdvanced').on('click',function() {
   if($( '.showAdvanced').text().indexOf("Show") >= 0) {
	$( '.advancedSettings' ).show();
	$( '.showAdvanced').text($( '.showAdvanced').text().replace('Show', 'Hide'));
   } else {
	$( '.advancedSettings' ).hide();
	$( '.showAdvanced').text($( '.showAdvanced').text().replace('Hide', 'Show'));
   }  
 })

  $( '.mPProfileTopMenu div').on('click',function(e) {
    var tabName = $( this ).attr('id');
    $( '.mPtab').removeClass( 'activeTab');
    $( '#tab_' + tabName ).addClass( 'activeTab');
    $( '.mPProfileTopMenu div').removeClass( 'active');
    $( this ).addClass( 'active');
    
    e.stopPropagation();
  })
  var mapDiv = document.getElementById('map');
  var map = new google.maps.Map(mapDiv, {
    center: {lat: {{$business->latitude}}, lng: {{$business->longitude}}},
    zoom: 13
  });
  markers = [];

  var postObject = {
   userId: id,
   csrf: $( '#_token').val(),
   businessId: active_id
 },
 cropTypes = {
   widescreen : true,
   letterbox: false,
   free: false
 }

 $("#container_image").PictureCut({
  InputOfImageDirectory       : "image",
  PluginFolderOnServer        : "assets/dashboard/js/jquery.picture.cut/",
  FolderOnServer              : "/uploads/businesses/",
  EnableCrop                  : false,
  EnableResize                : true,
  MaximumSize                 : 300,
  CropWindowStyle             : "Bootstrap",
  ActionToSubmitUpload        : "/services/busLogoUpload",
  ActionToSubmitCrop          :  "/services/busLogoCrop",
  DataPost                    : postObject,
  CropModes                   : cropTypes,
  CropOrientation             : false,
  MinimumWidthToResize        : 300,
  UploadedCallback            : function(image) {
   $( '#image').val(image.currentFileName).trigger('input');
 },
 @if ( $business->image)
 DefaultImageButton          : '/uploads/businesses/{{ $business->image }}',
 @else 
 DefaultImageButton          : "/assets/dashboard/images/upload.png"
 @endif
});
 $("#cover_image").PictureCut({
  InputOfImageDirectory       : "image",
  PluginFolderOnServer        : "assets/dashboard/js/jquery.picture.cut/",
  FolderOnServer              : "/uploads/businesses/",
  EnableCrop                  : true,
  EnableResize                : true,
  MaximumSize                 : 3000,
  CropWindowStyle             : "Bootstrap",
  ActionToSubmitUpload        : "/services/busLogoUpload",
  ActionToSubmitCrop          :  "/services/busLogoCrop",
  DataPost                    : postObject,
  CropModes                   : cropTypes,
  CropOrientation             : false,
  MinimumWidthToResize        : 3000,
  MinimumHeightToResize       : 4000,
  UploadedCallback            : function(image) {
   $( '#cover').val(image.currentFileName).trigger('input');
 },
 @if ( $business->cover)
 ImageButtonCSS          : { 'background': "url( '/uploads/businesses/{{ $business->cover }}')",
 'background-size': 'cover' },
 @else 
 ImageButtonCSS          : { background: "url( '/assets/dashboard/images/upload.png' )" }
 @endif
});
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    //  (document.getElementById('autocomplete')),
     // {types: ['geocode']});
     (document.getElementById('autocomplete')),
     {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  console.log( place );

  document.getElementById('autocompleteAddress').value = 
  place.address_components[0]['long_name'] + ' ' +
  place.address_components[1]['long_name'] + ', ' +
  place.address_components[2]['long_name'];

  document.getElementById('latitude').value = place.geometry.location.lat();
  document.getElementById('longitude').value = place.geometry.location.lng();

   // document.getElementById('postcode').value = place.geometry.location.administrative_area_level_2();

   for (var i = 0; i < place.address_components.length; i++) {
    if (place.address_components[i].types[0] == 'locality' || place.address_components[i].types[1] == 'locality') {
      $( '#suburb_name').val( place.address_components[i].short_name );
    }
    if (place.address_components[i].types[0] == "administrative_area_level_1") {
     $( '#state').val( place.address_components[i].short_name );
   }
 }
     //Fallback in case there is no locality returned by Google

     $( '#latitude' ).trigger('input');
     $( '#longitude' ).trigger('input');
     $( '#suburb_name' ).trigger('input');
     $( '#state' ).trigger('input');
     $('#autocompleteAddress').trigger('input');
     
     setMapOnAll(null);
     map.setCenter(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
     myLatlng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
     markers = new google.maps.Marker({position: myLatlng, map: map,title: {{json_encode( $business->name )}}});


   }
   function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]
initAutocomplete();

</script>



