<?php
namespace Admin;

use Redirect, Business, Controller, View, Datatables, Input, Auth, Helper, Mail, Config, BaseController, Carbon\Carbon, BusinessPlan, DeletedEscort, App;
use Escort, Suburb, ImageHelper, User, Age, Height, Body, EscortService, EscortLanguage, Service, Language, EscortAvailability, EscortTouring, EscortRate, EscortImage, EscortBusiness, EscortUpdate, EscortReview, EmailTemplate, Helpers, Visit, PhoneCall, Playmail;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;
use MyPlaymate\Repositories\BusinessRepository;
use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\Model;


 set_time_limit(0);

class Escort_temp_view extends Model
{
   public $timestamps = false;
}

class EscortController extends Controller {

  protected $userRepo;

  protected $suburbRepo;

  protected $businessRepo;

  function __construct(UserRepository $userRepo, SuburbRepository $suburbRepo, BusinessRepository $businessRepo) {
    $this->userRepo = $userRepo;
    $this->suburbRepo = $suburbRepo;
    $this->businessRepo = $businessRepo;
  }

  public function pendingEscortsIndex()
  {
    $plans = BusinessPlan::where('type','=','1')->get();
    return View::make('admin.escorts.pending',compact( 'plans'));
  }

  public function pendingEscortsAjax()
  {
    $query = Escort::whereStatus(1);
    return Datatables::of($query)
      ->editColumn('escort_name',function($escort){
        return '<a href="/escorts/preview/'.$escort->id.'" target="_blank">'.$escort->escort_name.'</a>';
      })
      ->editColumn('suburb_id','{{ $state }}')
      ->editColumn('business',function($escort ) {
        $business = 'Independent Escort';
        if ( $escort->under_business != 0 ){

            $business = Business::find( $escort->business_id );
            $business = ( $business ? $business->name . '-' . $business->id : 'No Business' );
        }

        return $business; })
      ->editColumn('phone',function($escort ) {
        if ( $escort->under_business != 0 ){
            $business = Business::find( $escort->business_id );
            if ( $business )
                $escort->phone = $business->phone;
        }
        return $escort->phone; })
      ->editColumn('email',function($escort ) {
        if ( $escort->under_business != 0 ){
            $business = Business::find( $escort->business_id );
            if ( $business )
                $escort->email = $business->email;
        }
        return '<a href="mailto:' . $escort->email . '">' . $escort->email . '</a>'; })
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->editColumn('featured',function($escort){
        return $escort->featured == 1 ? '<i class="fa fa-check"></i> Yes' : '<i class="fa fa-ban"></i> No';
      })
      ->addColumn('action', function ($escort) {
                return '<a href="#" data-escort-id="'.$escort->id.'" class="btn btn-xs btn-success approve"><i class="fa fa-check"></i> Approve</a>
                    <a href="#" data-escort-id="'.$escort->id.'" class="btn btn-xs btn-danger disapprove"><i class="fa fa-ban"></i> Disapprove</a>
                  <a target =\"_blank\" href ="/user/profile/?type=escort&id='.$escort->id.'">Edit Profile</a>';
            })
      ->make(true);
  }

  public function approveEscort()
  {
    $escort = Escort::find(Input::get('escort_id'));
    $escort->status = 2;
    $escort->approved_by = Auth::user()->id;
    $escort->approved_at = date('Y-m-d H:i:s');
    $escort->bumpup_date = Carbon::now();
    $escort->save();
      if ( $escort->under_business == 1 ){
   //   $business = Business::find($escort->business_id);
   //   $escort->email = $business->email;
        return;
    }

        $data['full_name'] = $escort->escort_name;
        $data['email'] = ( !App::environment('development') ? $escort->email : Config::get('constants.DEV_EMAIL') );
        $emailTemplate = EmailTemplate::where('name', 'escort-approved')->first();

        Mail::send('emails.escort-approved', ['data' => $data,'escort'=>$escort], function ($message) use($data) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Escort Profile Approved')
                    ->to($data['email']);
        });
  }

  public function disapproveEscort()
  {
    $escort = Escort::find(Input::get('escort_id'));
    $escort->status = 3;
    $escort->disapproved_by = Auth::user()->id;
    $escort->disapproved_at = date('Y-m-d H:i:s');
    $escort->save();

        $data['full_name'] = $escort->escort_name;
        $data['email'] = $escort->email;
        $emailTemplate = EmailTemplate::where('name', 'escort-disapproved')->first();

        Mail::send('emails.escort-disapproved', ['data' => $data,'escort'=>$escort], function ($message) use($escort) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Escort Profile Dispproved')
                    ->to($escort->email);
        });
  }

  public function makeUnavailable()
  {
    $escort = Escort::find(Input::get('id'));
    $escort->status = 4;
    $escort->save();
    if ( $escort->under_business == 1 ){
     // $business = Business::find($escort->business_id);
     // $escort->email = $business->email;
      return;
    }
        $data['full_name'] = $escort->escort_name;
        $data['email'] = ( !App::environment('development') ? $escort->email : Config::get('constants.DEV_EMAIL') );
        $emailTemplate = EmailTemplate::where('name', 'escort-disapproved')->first();

        Mail::send('emails.account-suspended', ['data' => $data,'escort'=>$escort], function ($message) use($data) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('Your Myplaymate Account has been suspended')
                    ->to($data['email']);
        });
  }

  public function deleteEscort()
  {
    $id = Input::get('escort_id');
    $escort = Escort::find($id);
    $deleted = new DeletedEscort;
    $deleted->escort_id = $id;
    $deleted->escort_name = $escort->escort_name;
    $deleted->suburb_name = $escort->suburb_name;
    $deleted->deleted_by = Auth::user()->id;
    $deleted->save();
    $escort->delete();

    EscortAvailability::whereEscortId($id)->delete();
    EscortBusiness::whereEscortId($id)->delete();
    EscortLanguage::whereEscortId($id)->delete();
    EscortRate::whereEscortId($id)->delete();
    EscortService::whereEscortId($id)->delete();
    EscortTouring::whereEscortId($id)->delete();
    EscortUpdate::whereEscortId($id)->delete();
    EscortReview::whereEscortId($id)->delete();

    $images = EscortImage::whereEscortId($id)->get();
    foreach ($images as $img) {
      if (file_exists('/uploads/escorts/slider/'.$img->filename))
        unlink('/uploads/escorts/slider/'.$img->filename);
      if (file_exists('/uploads/escorts/thumbnails/thumb_170x170_'.$img->filename))
        unlink('/uploads/escorts/thumbnails/thumb_170x170_'.$img->filename);
      if (file_exists('/uploads/escorts/thumbnails/thumb_220x330_'.$img->filename))
        unlink('/uploads/escorts/thumbnails/thumb_220x330_'.$img->filename);
      $img->delete();
    }
  }

  public function bumpUp()
  {
    $escort = Escort::find(Input::get('escort_id'));
    $escort->bumpup_date = Carbon::now();
    $escort->save();
  }
  public function bumpDown()
  {
    $escort = Escort::find(Input::get('id'));
    $escort->bumpup_date = '2009-01-01 09:00:00';
    $escort->save();
  }


  public function escortIndex()
  {  $plans = BusinessPlan::where('type','=','1')->get();
     return View::make('admin.escorts.index',compact( 'plans'));
  }
  public function getStatusCount() {
      $input = Input::all();
      $escorts = Escort::select('*')
        ->whereUnderBusiness(0)
        ->where('email', 'like', '%' . Input::get('search') . '%')
        ->orWhere('escort_name', 'like', '%' . Input::get('search') . '%' )
        ->orWhere('phone', 'like', '%' . Input::get('search') . '%' )
        ->orWhere('escorts.id', 'like', '%' . Input::get('search') . '%' )
        ->get();
    $count = array(
      '1' => array(
         'all'     => 0,
         'NSW'     => 0,
         'VIC'     => 0,
         'QLD'     => 0,
         'NT'      => 0,
         'SA'      => 0,
         'WA'      => 0,
         'TAS'     => 0
      ),
      '2' => array(
         'all'     => 0,
         'NSW'     => 0,
         'VIC'     => 0,
         'QLD'     => 0,
         'NT'      => 0,
         'SA'      => 0,
         'WA'      => 0,
         'TAS'     => 0
      ),
       '3' => array(
         'all'     => 0,
         'NSW'     => 0,
         'VIC'     => 0,
         'QLD'     => 0,
         'NT'      => 0,
         'SA'      => 0,
         'WA'      => 0,
         'TAS'     => 0
      ),
       '4' => array(
         'all'     => 0,
         'NSW'     => 0,
         'VIC'     => 0,
         'QLD'     => 0,
         'NT'      => 0,
         'SA'      => 0,
         'WA'      => 0,
         'TAS'     => 0
      ),
     );
    foreach ( $escorts as $escort ) {
      $count[$escort->status]['all']++;
      if ( $escort->state != '' ){
        $count[$escort->status][$escort->state]++;
      }
    }
    return json_encode ($count );
  }
  public function ajaxEventsByDate() {

    set_time_limit(0);
    ini_set('memory_limit','246M');

    if (!isset($_GET['date_from']) || $_GET['date_from'] == '') $_GET['date_from'] = '01/02/2015';
    if (!isset($_GET['date_to']) || $_GET['date_to'] == '') $_GET['date_to'] = '30/10/2050';
      $visits = Visit::select('escort_id')
      ->where('date', '>=', Carbon::createFromFormat('d/m/Y H', $_GET['date_from'] . ' 00'))
      ->where('date', '<=', Carbon::createFromFormat('d/m/Y H', $_GET['date_to'] . ' 00')->addDay())
           ->get();

    $format = array();
     foreach ( $visits as $visit ) {
       if ( !isset($format[$visit->escort_id]['visits']) ) {
            $format[$visit->escort_id]['visits'] = 1;
            $format[$visit->escort_id]['id'] = $visit->escort_id;
       }
       else {
            $format[$visit->escort_id]['visits'] = $format[$visit->escort_id]['visits']+1;
       }
  }
     $playmails = Playmail::select('escort_id')
      ->where('date', '>=', Carbon::createFromFormat('d/m/Y H', $_GET['date_from'] . ' 00'))
      ->where('date', '<=', Carbon::createFromFormat('d/m/Y H', $_GET['date_to'] . ' 00')->addDay())
           ->get();
       foreach ( $playmails as $playmail ) {
       if ( !isset($format[$playmail->escort_id]['playmail']) ) {
            $format[$playmail->escort_id]['playmail'] = 1;
            $format[$playmail->escort_id]['id'] = $playmail->escort_id;
       }
       else {
            $format[$playmail->escort_id]['playmail'] = $format[$playmail->escort_id]['playmail']+1;
       }
       }
      $phones = PhoneCall::select('escort_id')
      ->where('date', '>=', Carbon::createFromFormat('d/m/Y H', $_GET['date_from'] . ' 00'))
      ->where('date', '<=', Carbon::createFromFormat('d/m/Y H', $_GET['date_to'] . ' 00')->addDay())
           ->get();
       foreach ( $phones as $phone ) {
       if ( !isset($format[$phone->escort_id]['phone']) ) {
            $format[$phone->escort_id]['phone'] = 1;
            $format[$phone->escort_id]['id'] = $phone->escort_id;
       }
       else {
            $format[$phone->escort_id]['phone'] = $format[$phone->escort_id]['phone']+1;
       }
  }
     print_r ( json_encode ($format ));
  }
  public function escortAjax()
  {
    $escorts = Escort::select('*')
          ->whereUnderBusiness(0)
          ->where('email', 'like', '%' . Input::get('search') . '%')
          ->orWhere('escort_name', 'like', '%' . Input::get('search') . '%' )
          ->orWhere('phone', 'like', '%' . Input::get('search') . '%' )
          ->orWhere('escorts.id', 'like', '%' . Input::get('search') . '%' )
          ->get();
    $x = 0;
     echo '{
      "data": [';
    foreach( $escorts as $line ) {
        $plan = '';
        $userPlan = 'No User';
        $user = User::where('id','=',$line->user_id )->first();
         if ( $user ){
              $thePlan = BusinessPlan::where('id','=',$user->business_plan )->first();
              if ( $thePlan )
                  $userPlan ='<div id = \"userPlan_' . $line->user_id . '\">' . $thePlan->name . '</div>';

              if ( $line->under_business == 0 ){
                  $plan ='<li data-id =\"' . $user->id . '\" data-currentplan = \"' . $user->business_plan . '\" ><a class = \"popupBusinessPlans\">Change Plan</a></li>';
              }
        }

           echo '[
             "' . $line->id . '","<a href=\"/escorts/preview/'.$line->id.'\" target=\"_blank\">' . str_replace ('"', '\'', $line->escort_name ). '</a>","' . str_replace ('"', '\'',  Helpers::get_status($line->status) ). '","' . $line->state . '","' . str_replace ('"', '\'',  Helpers::format_phone_number( $line->phone) ). '","<a href =\"mailto:' . $line->email .'\">' . str_replace ('"', '\'', $line->email ). '</a>","<span class = \"ev\" id =\"_ev_views_' . $line->id . '\">' . str_replace ('"', '\'', $line->count_views ). '</span>","<span class = \"ev\"  id =\"_ev_phone_' . $line->id . '\">' . str_replace ('"', '\'', $line->count_phone ). '</span>","<span class = \"ev\"  id =\"_ev_playmail_' . $line->id . '\">' . str_replace ('"', '\'', $line->count_playmail ). '</span>","' . str_replace ('"', '\'', Helpers::db_date_to_user_date($line->created_at)). '","<span class=\"bumpup-date\">' . str_replace ('"', '\'', Helpers::db_date_to_user_datetime_or_empty($line->bumpup_date) ). '<span class=\"bumpup-date\">","' . $userPlan . '","<div class=\"dropdown\"><button class=\"btn btn-primary dropdown-toggle\" type=\"button\" id=\"actions-'.$line->id.'\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Action<span class=\"caret\"></span></button><ul data-escort-id=\"'.$line->id.'\" class=\"dropdown-menu\" aria-labelledby=\"actions-'.$line->id.'\"><li><a target = \"_blank\" href =\"/user/profile/?type=escort&id='.$line->id.'\" class =\"edit-profile\">Edit Profile</a></li><li><a data-escort-id=\"'.$line->id.'\" class=\"approve\" href=\"#\">Approve</a></li><li><a data-escort-id=\"'.$line->id.'\" class=\"disapprove\" href=\"#\">Disapprove</a></li><li><a data-escort-id=\"'.$line->id.'\" class=\"delete\" href=\"#\">Delete</a></li><li><a data-escort-id=\"'.$line->id.'\" class=\"bumpup\" href=\"#\">Bump up</a></li><li><a data-escort-id=\"'.$line->id.'\" class=\"bumpdown\" href=\"#\">Bump Down</a></li><li><a data-escort-id=\"'.$line->id.'\" class=\"makeUnavailable\" href=\"#\">Make Unavailable</a></li>' . $plan . '</ul></div>"
           ]';
      if ( $x < count( $escorts )-1 ) echo ',';
      $x++;
    }
    echo ']
  }';

  }


  public function getEscortNew()
  {
    $business_id = Input::get('business_id');

    $suburbs = $this->suburbRepo->populateSelect();
    $users = $this->userRepo->populateSelect();
    $businesses = $this->businessRepo->populateSelect();

    $ages = Age::lists('name','id');
    $heights = Height::lists('name','id');
    return View::make('admin.escorts.new',compact('suburbs','users','businesses','ages','heights','business_id'));
  }

  public function postEscortNew()
  {
    $input = Input::all();
    $escort = new Escort;
    $escort->status = 1;
    $escort->active = 1;
    $escort->escort_name = $input['escort_name'];
    $escort->user_id = $input['user_id'];
    $escort->under_business = $input['under_business'];
    if (isset($input['business_id']))
      $escort->business_id = $input['business_id'];

    $escort->phone = $input['phone'];
    $escort->email = $input['email'];
    $escort->suburb_id = $input['suburb_id'];
    $escort->licence_number = $input['licence_number'];
    $escort->age_id = $input['age_id'];
    $escort->height_id = $input['height_id'];
    $escort->gender_id = $input['gender_id'];
    $escort->contact_phone = $input['contact_phone'];
    $escort->contact_playmail = $input['contact_playmail'];
    $escort->save();

    $escort->seo_url = $escort->getSeoUrl();
    $escort->save();

    $rates = new EscortRate;
    $rates->escort_id = $escort->id;
    $rates->save();

    if (isset($input['business_id'])) {
      $pivot = new EscortBusiness;
      $pivot->escort_id = $escort->id;
      $pivot->business_id = $input['business_id'];
      $pivot->save();
    }
    return Redirect::to('/admin/escorts/about/'.$escort->id);
  }

  public function escortGetEditInfo($id)
  {
    $escort = Escort::find($id);

    $suburbs = $this->suburbRepo->populateSelect();
    $users = $this->userRepo->populateSelect();
    $businesses = $this->businessRepo->populateSelect();

    $ages = Age::lists('name','id');
    $heights = Height::lists('name','id');

    return View::make('admin.escorts.info',compact('escort','users','suburbs','businesses','ages','heights'));
  }

  public function escortPostEditInfo($id)
  {
    $input = Input::all();
    $escort = Escort::find($id);
    $escort->escort_name = $input['escort_name'];
    $escort->user_id = $input['user_id'];
    $escort->under_business = $input['under_business'];
    if (isset($input['business_id']))
      $escort->business_id = $input['business_id'];

    $escort->phone = $input['phone'];
    $escort->email = $input['email'];
    $escort->suburb_id = $input['suburb_id'];
    $escort->licence_number = $input['licence_number'];
    $escort->age_id = $input['age_id'];
    $escort->height_id = $input['height_id'];
    $escort->gender_id = $input['gender_id'];
    $escort->contact_phone = $input['contact_phone'];
    $escort->contact_playmail = $input['contact_playmail'];
    $escort->seo_url = $escort->getSeoUrl();
    $escort->save();

    return Redirect::to('/admin/escorts/about/'.$id);
  }

  public function escortGetEditAbout($id)
  {
    $escort = Escort::find($id);
        if ($escort->gender_id == 3) {
            $bodies = Body::all();
        } else {
            $bodies = Body::where('gender_id', $escort->gender_id)->get();
        }
    return View::make('admin.escorts.about',compact('escort','bodies'));
  }

  public function escortPostEditAbout($id)
  {
    $input = Input::all();
    $escort = Escort::find($id);

    $escort->about_me = $input['about_me'];
    $escort->body_id = $input['body_id'];
    $escort->ethnicity_id = $input['ethnicity_id'];
    $escort->eye_color_id = $input['eye_color_id'];
    $escort->hair_color_id = $input['hair_color_id'];
    $escort->does_incalls = $input['does_incalls'];
    $escort->does_outcalls = $input['does_outcalls'];
    $escort->does_travel_internationally = $input['does_travel_internationally'];
    $escort->does_smoke = $input['does_smoke'];
    $escort->tattoos = $input['tattoos'];
    $escort->piercings = $input['piercings'];
    $escort->seo_url = $escort->getSeoUrl();
    $escort->save();

    // Clean the tables before inserting the newly selected services
    EscortService::whereEscortId($id)->delete();
    EscortLanguage::whereEscortId($id)->delete();

    $services = Service::all();
    foreach ($services as $service_id => $service) {
      foreach ($input as $key => $value) {
        if($key == 'service_'.$service_id){
          $escortService = new EscortService();
          $escortService->escort_id = $id;
          $escortService->service_id = $service_id;
          $escortService->save();
        }
      }
    }

    $languages = Language::all();
    foreach ($languages as $language_id => $language) {
      foreach ($input as $key => $value) {
        if($key == 'language_'.$language_id){
          $escortLanguage = new Escortlanguage();
          $escortLanguage->escort_id = $id;
          $escortLanguage->language_id = $language_id;
          $escortLanguage->save();
        }
      }
    }

    Visit::whereEscortId($id)->delete();
    PhoneCall::whereEscortId($id)->delete();
    Playmail::whereEscortId($id)->delete();

    return Redirect::to('/admin/escorts/availability/'.$id);
  }

  public function escortGetEditAvailability($id)
  {
    $escort = Escort::find($id);
    $suburbs = $this->suburbRepo->populateSelect();

    return View::make('admin.escorts.availability',compact('escort','suburbs'));
  }

  public function escortPostEditAvailability($id)
  {
    $input = Input::all();
    $escort = Escort::find($id);

    // Availability`
    EscortAvailability::whereEscortId($id)->delete();

    // I get input in the form enabled_x
    foreach ($input as $key => $value) {
      if(substr($key, 0, 7) == 'enabled'){
        $day = substr($key,8);
        $escortAvailability = new EscortAvailability();

        $escortAvailability->day = $day;
        $escortAvailability->enabled = 1;
        if (isset($input['24_'.$day])){
          $escortAvailability->twenty_four_hours = 1;
          $escortAvailability->start_hour_id = 0;
          // 0 is am, 1 is pm
          $escortAvailability->start_am_pm = 0;
          $escortAvailability->end_hour_id = 23;
          $escortAvailability->end_am_pm = 1;
        } else {
          $escortAvailability->start_hour_id = $input['start_hour_'.$day];
          $escortAvailability->start_am_pm = $input['start_am_pm_'.$day];
          $escortAvailability->end_hour_id = $input['end_hour_'.$day];
          $escortAvailability->end_am_pm = $input['end_am_pm_'.$day];
        }

        $escortAvailability->escort_id = $id;
        $escortAvailability->save();
      }
    }

    // Rates
    $rates = EscortRate::whereEscortId($id)->first();
    if($rates)
      $escortRates = $rates;
    else
      $escortRates = new EscortRate();

    $escortRates->incall_15_rate = $input['incall_15_rate'];
    $escortRates->incall_30_rate = $input['incall_30_rate'];
    $escortRates->incall_45_rate = $input['incall_45_rate'];
    $escortRates->incall_1h_rate = $input['incall_1h_rate'];
    $escortRates->incall_2h_rate = $input['incall_2h_rate'];
    $escortRates->incall_3h_rate = $input['incall_3h_rate'];
    $escortRates->incall_overnight_rate = $input['incall_overnight_rate'];
    $escortRates->outcall_15_rate = $input['outcall_15_rate'];
    $escortRates->outcall_30_rate = $input['outcall_30_rate'];
    $escortRates->outcall_45_rate = $input['outcall_45_rate'];
    $escortRates->outcall_1h_rate = $input['outcall_1h_rate'];
    $escortRates->outcall_2h_rate = $input['outcall_2h_rate'];
    $escortRates->outcall_3h_rate = $input['outcall_3h_rate'];
    $escortRates->outcall_overnight_rate = $input['outcall_overnight_rate'];
    $escortRates->save();

    $escort = Escort::find($id);
    $escort->notes = $input['notes'];
    if ($input['incall_1h_rate'] <= 100)
      $escort->hourly_rate_id = 1;
    if ($input['incall_1h_rate'] > 100 && $input['incall_1h_rate'] <= 200)
      $escort->hourly_rate_id = 2;
    if ($input['incall_1h_rate'] > 200 && $input['incall_1h_rate'] <= 300)
      $escort->hourly_rate_id = 3;
    if ($input['incall_1h_rate'] > 300 && $input['incall_1h_rate'] <= 400)
      $escort->hourly_rate_id = 4;
    if ($input['incall_1h_rate'] > 400 && $input['incall_1h_rate'] <= 500)
      $escort->hourly_rate_id = 5;
    if ($input['incall_1h_rate'] > 500)
      $escort->hourly_rate_id = 6;
    $escort->save();

    return Redirect::to('/admin/escorts/gallery/'.$id);
  }

  public function escortGetEditGallery($id)
  {
    $escort = Escort::find($id);
    return View::make('admin.escorts.gallery',compact('escort'));
  }

  public function uploadEscortGallery()
  {
    $file = Input::file('file');
    if ( $file ) {
    //$destinationPath = public_path().'/pic_test/';
    // $filename = str_random(12).'.jpg';
    $destinationPath = public_path().'/uploads/escorts/';
    $filename = str_random(12).'.jpg';
    $file->move($destinationPath, $filename);

    //  ImageHelper::createSliderImage($destinationPath,$filename);
    ImageHelper::createThumbnailSize($destinationPath,$filename,220,330);
    ImageHelper::createThumbnailSize($destinationPath,$filename,170,170);

    //   unlink($destinationPath.$filename);

    $escortImage = new EscortImage();
    $escortImage->escort_id = Input::get('escort_id');
    $escortImage->filename = $filename;
    $escortImage->status = 2;
    $escortImage->save();
  }

    return Redirect::to('/admin/escorts/gallery/'.Input::get('escort_id'));

  }

  public function escortGetEditSite($id)
  {
    $escort = Escort::find($id);
    return View::make('admin.escorts.site',compact('escort'));
  }

  public function escortPostEditSite($id)
  {
    $input = Input::all();
    $escort = Escort::find($id);
    $escort->use_own_url = $input['use_own_url'];
    $escort->own_url = $input['own_url'];
    $escort->save();
    return Redirect::to('/admin/escorts/reviews/'.$id);
  }

  public function escortGetEditReviews($id)
  {
    $escort = Escort::find($id);
    return View::make('admin.escorts.reviews',compact('escort'));
  }
  public function escortGetEditSeo($id)
  {
    $escort = Escort::find($id);
    return View::make('admin.escorts.seo',compact('escort'));
  }

  public function escortPostEditSeo($id)
  {
    $escort = Escort::find($id);
    $message = 'Escort SEO updated!';
    $input = Input::all();

    $escort->seo_title = $input['seo_title'];
    $escort->seo_keywords = $input['seo_keywords'];
    $escort->seo_description = $input['seo_description'];
    $escort->seo_content = $input['seo_content'];
    $escort->save();
    return View::make('admin.escorts.seo',compact('escort','message'));
  }
     public function deletedEscorts(){
     $deletedEscorts = DeletedEscort::get();
     return View::make('admin.escorts.deleted',compact('deletedEscorts'));
  }
}
