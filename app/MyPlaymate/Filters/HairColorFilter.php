<?php
namespace MyPlaymate\Filters;

use MyPlaymate\Filters\Filter;

class HairColorFilter extends Filter
{
	public $column = 'hair_color_id';
}