<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuburbsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suburbs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('country_id');
			$table->integer('state_id');
			$table->string('name');
			$table->string('postcode');
			$table->string('lat');
			$table->string('lng');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suburbs');
	}

}
