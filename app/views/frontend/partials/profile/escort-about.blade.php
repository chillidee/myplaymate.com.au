{{ Form::open(['id'=>'escortAbout']) }}

<div id="backend-profile-description" class="alignc">
    <h2>Profile Details</h2>

    <div class="alert alert-success">Escort info updated!</div>
    <h3>Describe Yourself</h3>
    <p>No more than 300 words</p>
    {{ Form::textarea('about_me', $escort->about_me, array('data-parsley-required','rows' => 5, 'placeholder'=> 'e.g. special notes for your lovers to read. Your description must not be more than 300 Words')) }}
</div>
<!-- End Of Backned Profile Description -->

<div class="control-group">
                {{ Form::label('height_id','Height*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::select('height_id',$heights,$escort->height_id,array('class'=>'form-control')) }}
                </div>
            </div>

<!-- Body Type -->
<div id="backend-profile-body-type" class="alignc">
    <h2>Body types</h2>
    <div id="body-items">
        <ul>
            @foreach($bodies as $body)
            <li>
                <a>
                {{ Form::radio('body_id', $body->id, $escort->body_id == $body->id ? true : false, array('required','class' => 'radioImageSelect', 'data-image' => url('assets/frontend/img/body/'.Str::slug($body->name).'.png'))) }}
                </a>
                <span>{{ $body->name }}</span>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- End Of Body Types -->

<!-- Ethnicity -->
<div id="backend-profile-ethnicity-type" class="alignc">
    <h2>Ethnicity</h2>
    <div id="ethnicity-items">
        <ul>
            @foreach(Ethnicity::all() as $ethnicity)
            <li>
                <a>
                    {{ Form::radio('ethnicity_id', $ethnicity->id, $escort->ethnicity_id==$ethnicity->id ? true : false, array('required','class' => 'radioImageSelect', 'data-image' => url('assets/frontend/img/ethnicity/'.Str::slug($ethnicity->name).'.png'))) }}
                </a>
                <span>{{ $ethnicity->name }}</span>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- End Of Ethnicity -->

<!-- Eye Color -->
<div id="backend-profile-eye-type" class="alignc">
    <h2>Describe your Eye Colour</h2>
    <div id="eye-items">
        <ul>
            @foreach(EyeColor::all() as $eyeColor)
            <li>
                <a>
                {{ Form::radio('eye_color_id', $eyeColor->id, $escort->eye_color_id==$eyeColor->id ? true : false, array('required','class' => 'radioImageSelect', 'data-image' => url('assets/frontend/img/eye-color/'.Str::slug($eyeColor->name).'.jpg'))) }}
                </a>
                <span>{{ $eyeColor->name }}</span>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- End Of Eye Color -->
<!-- Hair Color -->
<div id="backend-profile-Hair-type" class="alignc">
    <h2>Describe your Hair Colour</h2>
    <div id="hair-items">
        <ul>
            @foreach(HairColor::all() as $hairColor)
            <li>
                <a>
                    {{ Form::radio('hair_color_id', $hairColor->id, $escort->hair_color_id==$hairColor->id ? true : false, array('required','class' => 'radioImageSelect', 'data-image' =>  url('assets/frontend/img/hair-color/'.Str::slug($hairColor->name).'.jpg'))) }}
                </a>
                <span>{{ $hairColor->name }}</span>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- End Of Hair Color -->
<hr>

<!-- Services -->
<div id="backend-profile-services" class="alignc">
<h2>Describe your Services</h2>

<div id="services-items" class="clearfix">

<div id="row1" class="clearfix">
    <!-- Incalls -->
    <div id="incalls">
        <div class="services-items-title">
            <h5>INCALLS</h5>
        </div>
        <div>
            {{ Form::radio('does_incalls', '1', $escort->does_incalls==1 ? true : false) }}
            <label>Yes</label>
        </div>
        <div>
            {{ Form::radio('does_incalls', '0', $escort->does_incalls==0 ? true : false) }}
            <label>No</label>
        </div>
    </div>

    <!-- OutCalls -->
    <div id="outcalls" class="clearfix">
        <div class="services-items-title">
            <h5>OUTCALLS</h5>            
        </div>
        <div>
            {{ Form::radio('does_outcalls', '1', $escort->does_outcalls==1 ? true : false) }}
            <label>Yes</label>
        </div>
        <div>
            {{ Form::radio('does_outcalls', '0', $escort->does_outcalls==0 ? true : false) }}
            <label>No</label>
        </div>
    </div>
</div>

<div id="row2" class="clearfix">
    <!-- Smokers -->
    <div id="smokers" class="clearfix">
        <div class="services-items-title">
            <h5>SMOKER</h5>            
        </div>
        <div>
            {{ Form::radio('does_smoke', '1', $escort->does_smoke==1 ? true : false) }}
            <label>Yes</label>
        </div>
        <div>
            {{ Form::radio('does_smoke', '0', $escort->does_smoke==0 ? true : false) }}
            <label>No</label>
        </div>
    </div>

    <!-- Tattoos -->
    <div id="tatoos" class="clearfix">
        <div class="services-items-title">
            <h5>TATTOOS</h5>            
        </div>
        <div>
            {{ Form::radio('tattoos', '1', $escort->tattoos==1 ? true : false) }}
            <label>Yes</label>
        </div>
        <div>
            {{ Form::radio('tattoos', '0', $escort->tattoos==0 ? true : false) }}
            <label>No</label>
        </div>
    </div>
</div>

<div id="row3" class="clearfix">
    <!-- Piercing -->
    <div id="piercing" class="clearfix">
        <div class="services-items-title">
            <h5>PIERCING</h5>
        </div>
        <div>
            {{ Form::radio('piercings', '1', $escort->piercings==1 ? true : false) }}
            <label>Yes</label>
        </div>
        <div>
            {{ Form::radio('piercings', '0', $escort->piercings==0 ? true : false) }}
            <label>No</label>
        </div>
    </div>

    <div id="travels-internationally" class="clearfix">
        <div class="services-items-title">
            <h5>TRAVEL INTERNATIONALLY</h5>            
        </div>
        <div>
            {{ Form::radio('does_travel_internationally', '1', $escort->does_travel_internationally==1 ? true : false) }}
            <label>Yes</label>
        </div>
        <div>
            {{ Form::radio('does_travel_internationally', '0', $escort->does_travel_internationally==0 ? true : false) }}
            <label>No</label>
        </div>
    </div>

</div>

<div id="row4" class="clearfix">
    <!-- Services -->
    <div class="services clearfix">
        <div class="services-items-title">
            <h5>SERVICES</h5>
        </div>
        <div class="services-items-items clearfix">
            @foreach(Service::orderBy('order')->get() as $service)
            <div>
                {{ Form::checkbox('service_'.$service->id, $service->id, $escort->services()->where('service_id', $service->id)->first() ? 1 : 0,array('data-parsley-multiple'=>'services')) }}
                {{ Form::label('service_'.$service->id,$service->name) }}
            </div>
            @endforeach
        </div>
    </div>

</div>

<div id="row5" class="clearfix">
    <!-- Services -->
    <div class="languages clearfix">
        <div class="languages-items-title">
            <h5>LANGUAGES SPOKEN</h5>
        </div>
        <div class="languages-items-items clearfix">
            @foreach(Language::all() as $language)
            <div>
                {{ Form::checkbox('language_'.$language->id, $language->id, $escort->languages()->where('language_id', $language->id)->first() ? 1 : 0,array('data-parsley-multiple'=>'languages')) }}
                {{ Form::label('language_'.$language->id,$language->name) }}
            </div>
            @endforeach
            <script>
                jQuery('.services-items-items')
                    .find('input')
                    .first()
                    .attr('data-parsley-required', 'true')
                    .attr('data-parsley-error-message','You must select at least one service.');

                jQuery('.languages-items-items')
                    .find('input')
                    .first()
                    .attr('data-parsley-required', 'true')
                    .attr('data-parsley-error-message','You must select at least one language.');
            </script>
        </div>
    </div>
</div>
</div><!-- End Of Services-->
<hr>

    {{ Form::hidden('escort_id',$escort->id) }}
    {{ Form::hidden('next_icon','escort-availability',array('class'=>'next-icon')) }}
    {{ Form::hidden('next_route','escortAvailability',array('class'=>'next-route')) }}
    {{ Form::hidden('next_type','escort',array('class'=>'next-type')) }}
    <div class="alignc section-content">
        {{ Form::submit('UPDATE',array('class'=>'submit-button')) }}
        {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }}
    </div>
{{ Form::close() }}

</div>