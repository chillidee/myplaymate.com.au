<?php

class FakeUsersTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,5) as $index) {
            User::create([
                'full_name'        => $faker->name(),
                'email'             => $faker->email(),
                'password'          => Hash::make('ciao')
            ]);
        }

        User::create([
            'type'      => 1,
            'full_name' => 'fil_customer',
            'email'     => 'filcustomer@chillidee.com.au',
            'password'  => Hash::make('ciao'),
            'active'    => 1
        ]);
        User::create([
            'type'      => 2,
            'full_name' => 'fil_escort',
            'email'     => 'filescort@chillidee.com.au',
            'password'  => Hash::make('ciao'),
            'active'    => 1
        ]);
        User::create([
            'type'      => 2,
            'full_name' => 'fil_business',
            'email'     => 'filbusiness@chillidee.com.au',
            'password'  => Hash::make('ciao'),
            'active'    => 1
        ]);
        User::create([
            'type'      => 2,
            'super_user'=> 1,
            'full_name' => 'fil_admin',
            'email'     => 'filadmin@chillidee.com.au',
            'password'  => Hash::make('ciao'),
            'active'    => 1
        ]);
    }
}
