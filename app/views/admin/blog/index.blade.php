@extends('admin.layout')
@section('javascript')
	<script>
		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "/admin/ajax_blog",
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'title', name: 'title'},
		            {data: 'published', name: 'published'},
		            {data: 'meta_title', name: 'meta_title'},
		            {data: 'meta_description', name: 'meta_description'},
		            {data: 'date_from', name:'date_from'  },
		            {data: 'date_to', name:'date_to'  },
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });
		});
	</script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-newspaper-o"></span></div>
    <div class="pagetitle">
        <h1>Blog</h1>
    </div>
</div><!--pageheader-->

<div class="row">
	<a href="/admin/blog/new" class="create-user btn btn-success btn-lg pull-right section-content">Create post</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Published</th>
			<th>Meta title</th>
			<th>Meta description</th>
			<th>Published from</th>
			<th>Published to</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

@stop



