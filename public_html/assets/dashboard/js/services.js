angular.module('dashboard.services', []).
  factory('dashboard', function($http) {

    var dashboard = {};
        
    dashboard.getDrivers = function() {
      url = ( currentEscort ? '/services/businessProfile/' + id + '?callback=JSON_CALLBACK&el=' + active_id : '/services/EScortProfile/' + id + '?callback=JSON_CALLBACK&el=' + active_id );
      return $http({
        method: 'GET', 
        url: '/services/businessProfile/' + active_id + '/' + type
      });
    }

    dashboard.getMobileMenu = function() {
      return $http({
        method: 'JSONP', 
        url: '/services/businessProfile/' + active_id + '/' + type + '?callback=JSON_CALLBACK'
      });
    }
   dashboard.updateMenu = function() {
      $http.jsonp('/services/businessProfile/' + active_id + '/' + type + '/?callback=JSON_CALLBACK') 
          .success(function(response) {
             return response;
          })
          .error(function(response){
             return response;
          });
    }
     dashboard.getAutoComplete = function(search) {
      return $http({
        method: 'GET', 
        url: '/services/autocomplete/' + search
      });
    }
      dashboard.getLatLong = function(search) {
      return $http({
        method: 'GET', 
        url: '/services/getlatlong/' + search
      });
    }
      dashboard.getEscProf = function(escId){
        return $http({
        method: 'GET', 
        url: '/services/escort-profile-details/' + escId
      });
      }
      dashboard.getBusProf = function(busId){
        return $http({
        method: 'GET', 
        url: '/services/business-profile-details/' + busId
      });
      }


    return dashboard;
  });
  angular.module('dashboard.panelDirectives', [])
     .directive('statApproved', function() {
           return {
              restrict: 'E',
              template: '<span class ="mpStatus" id ="statApproved"></span>',
              scope: { },
              link: function(scope, elem, attr) {}
            }
      
      })
     .directive('position', function() {
           return {
              restrict: 'E',
              transclude: true,
              template: '<span class ="_mpPosition"><ng-transclude></ng-transclude></span>',
              scope: { },
              link: function(scope, elem, attr) {}
            }
      
      })
   angular.module('dashboard.screenDirectives', [])
     .directive('statApproved', function() {
           return {
              restrict: 'E',
              template: '<span class ="mpStatus" id ="statApproved"></span>',
              scope: { },
              link: function(scope, elem, attr) {}
            }
      
      })
  angular.module('mpmApi', []).
  factory('mpmApi', function($http ) {

    mpmApi = {};
        
    return mpmApi;
})
.controller('messageController',function($http, $scope, dashboard, $rootScope) {

    $rootScope.$on('cf', function() {
            
          $scope.messagecontent = '';
          $scope.messagedivclass = '';

        })

  $scope.messagecontent = '';

   mpmApi.post = function(table,action,where,value,callback) {

        $( '.dashLoader').show();

        $scope.messagecontent = '';
        $scope.messagedivclass = '';

      var data = {
            table: table,
            action: action,
            where: where,
            value: value
      };
      console.log( 'post' );
      console.log( data );
      return $http({
        method : 'POST',
        data: data,
        url: '/services/mpmApi/'
      }).then(function successCallback(response) {
         $scope.messagecontent = 'Your changes have been saved.';
         $scope.messagedivclass = 'success';
         if ( callback ) { callback(response);}
          $('html, body').animate({ scrollTop: 0 });
          $( '.ng-dirty').removeClass( 'ng-dirty');
          $( '.dashLoader').hide();
          console.log(response);
  }, function errorCallback(response) {
        console.log( response );
        $( '.dashLoader').hide();
        $scope.messagecontent = 'There was an error saving your changes.  Please try again, and if the problem persists, contact support on 1300 769 766.';
        $scope.messagedivclass = 'error';
        $('html, body').animate({ scrollTop: 0 });
  });
    }
        mpmApi.reviewSubmit = function(action, reviewId ,actionId,altId,altAction) {
             return $http({
                     method : 'POST',
                     data: { action: actionId, id: reviewId, user_id: id },
                     url: '/services/updateReviewStatus/'
             }).then(function successCallback(response) {
                     mpmApi['review' + reviewId] = ( mpmApi['review' + reviewId] == 'alt' ? 'first' : 'alt' );
                     $scope.reviewmescontent = 'Review ' + action + 'ed.';
                     $scope.reviewmesdivclass = 'success';
                     $( '#_mpStatusIcon_' + reviewId ).removeClass().addClass( '_mpStatus mp' + altAction );
                     $( '#reviewAction_' + reviewId ).removeClass().addClass( 'button_' + altAction ).html( altAction );
                     console.log( response );
             }, function errorCallback(response) {
                      console.log( response );
                      $scope.reviewmescontent = 'There was an error saving your changes.  Please try again, and if the problem persists, contact support.';
                      $scope.reviewmesdivclass = 'error';
                      $('html, body').animate({ scrollTop: 0 });
             });
        }

})
  .directive('message', function() {
           return {
              restrict: 'EA',
              scope:{
                 messagecontent : '@'
              },
              controller: 'messageController',
              link: function (scope, element) {
                },
              template: '<div class ="message {{messagedivclass}}">{{messagecontent}}</message>',
            }
     
      })
    .directive('reviewmessage', function() {
           return {
              restrict: 'EA',
              scope:{
                 reviewmescontent : '@'
              },
              controller: 'messageController',
              link: function (scope, element) {
                },
              template: '<div class ="message {{reviewmesdivclass}}">{{reviewmescontent}}</message>',
            }
     
      })

  /**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 * https://github.com/vitalets/checklist-model
 * License: MIT http://opensource.org/licenses/MIT
 */

angular.module('checklist-model', [])
.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
  // contains
  function contains(arr, item, comparator) {
    if (angular.isArray(arr)) {
      for (var i = arr.length; i--;) {
        if (comparator(arr[i], item)) {
          return true;
        }
      }
    }
    return false;
  }

  // add
  function add(arr, item, comparator) {
    arr = angular.isArray(arr) ? arr : [];
      if(!contains(arr, item, comparator)) {
          arr.push(item);
      }
    return arr;
  }  

  // remove
  function remove(arr, item, comparator) {
    if (angular.isArray(arr)) {
      for (var i = arr.length; i--;) {
        if (comparator(arr[i], item)) {
          arr.splice(i, 1);
          break;
        }
      }
    }
    return arr;
  }

  // http://stackoverflow.com/a/19228302/1458162
  function postLinkFn(scope, elem, attrs) {
     // exclude recursion, but still keep the model
    var checklistModel = attrs.checklistModel;
    attrs.$set("checklistModel", null);
    // compile with `ng-model` pointing to `checked`
    $compile(elem)(scope);
    attrs.$set("checklistModel", checklistModel);

    // getter / setter for original model
    var getter = $parse(checklistModel);
    var setter = getter.assign;
    var checklistChange = $parse(attrs.checklistChange);
    var checklistBeforeChange = $parse(attrs.checklistBeforeChange);

    // value added to list
    var value = attrs.checklistValue ? $parse(attrs.checklistValue)(scope.$parent) : attrs.value;


    var comparator = angular.equals;

    if (attrs.hasOwnProperty('checklistComparator')){
      if (attrs.checklistComparator[0] == '.') {
        var comparatorExpression = attrs.checklistComparator.substring(1);
        comparator = function (a, b) {
          return a[comparatorExpression] === b[comparatorExpression];
        };
        
      } else {
        comparator = $parse(attrs.checklistComparator)(scope.$parent);
      }
    }

    // watch UI checked change
    scope.$watch(attrs.ngModel, function(newValue, oldValue) {
      if (newValue === oldValue) { 
        return;
      } 

      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
        scope[attrs.ngModel] = contains(getter(scope.$parent), value, comparator);
        return;
      }
      setValueInChecklistModel(value, newValue);

      if ( attrs.changefunc == 'availableDaysChange' ) {
            $("#availabilityForm select").prop('disabled', true);
                  $(".twenty_four_check").prop('disabled', true);
                  $( '.availDayCheck:checked' ).each(function( index ) {
                         $( this ).parents( 'tr').find( ".twenty_four_check").prop('disabled', false);
                            if ( !$( this ).parents('tr').find( '.twenty_four_check').is(':checked') ){
                             $( this ).parents( 'tr').find( "select").prop('disabled', false);
                            }
                         
                  });
      }

      if (checklistChange) {
        checklistChange(scope);
      }   
    });

    function setValueInChecklistModel(value, checked) {
      var current = getter(scope.$parent);
      if (angular.isFunction(setter)) {
        if (checked === true) {
          setter(scope.$parent, add(current, value, comparator));
        } else {
          setter(scope.$parent, remove(current, value, comparator));
        }
      }
      
    }

    // declare one function to be used for both $watch functions
    function setChecked(newArr, oldArr) {
      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
        setValueInChecklistModel(value, scope[attrs.ngModel]);
        return;
      }
      scope[attrs.ngModel] = contains(newArr, value, comparator);
    }

    // watch original model change
    // use the faster $watchCollection method if it's available
    if (angular.isFunction(scope.$parent.$watchCollection)) {
        scope.$parent.$watchCollection(checklistModel, setChecked);
    } else {
        scope.$parent.$watch(checklistModel, setChecked, true);
    }
  }

  return {
    restrict: 'A',
    priority: 1000,
    terminal: true,
    scope: true,
    compile: function(tElement, tAttrs) {
      if ((tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') && (tElement[0].tagName !== 'MD-CHECKBOX') && (!tAttrs.btnCheckbox)) {
        throw 'checklist-model should be applied to `input[type="checkbox"]` or `md-checkbox`.';
      }

      if (!tAttrs.checklistValue && !tAttrs.value) {
        throw 'You should provide `value` or `checklist-value`.';
      }

      // by default ngModel is 'checked', so we set it if not specified
      if (!tAttrs.ngModel) {
        // local scope var storing individual checkbox model
        tAttrs.$set("ngModel", "checked");
      }

      return postLinkFn;
    }
  };
}]);
