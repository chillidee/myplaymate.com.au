@extends('frontend.layout')

@section('content')

<!-- .boxed version of the page -->
<div class="page-wrapper extra-top">
    <!-- .container start -->
    <div class="container">
        <!-- .row start -->
        <div class="row">
            <h2>Registration successful.</h2>
            <p>Please check your email to activate your account.</p>
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- End of the Boxed Version Page -->

@stop
