<?php
namespace Admin;

use Controller, View, Auth, Input, Session, Redirect, Cookie, User, Datatables, Hash;

class UserController extends Controller
{
	public function getLogin()
	{
		return View::make('admin.auth.login');
	}

    public function postLogin()
    {
    	$email = Input::get('email');
    	$password = Input::get('password');
        if (Auth::attempt(array('email' => $email, 'password' => $password,'super_user'=> 1))) {
        	Session::flash('message_success', 'Welcome back!');
            return Redirect::intended('admin');
        }

        Session::flash('message_error', 'Incorrect username or password.');

        return Redirect::to('admin/login');
    }

    public function logout()
    {
        Auth::logout();
        Cookie::forget('laravel_session');

        return Redirect::to('admin/login');
    }

    public function usersIndex()
    {
        $message_success = Session::get('message_success');

        return View::make('admin.users.index')
            ->with('message_success',$message_success);
    }

    public function administratorsIndex()
    {
        $message_success = Session::get('message_success');

        return View::make('admin.users.administrators')
            ->with('message_success',$message_success);
    }

    public function withEscortsIndex()
    {
        $message_success = Session::get('message_success');

        return View::make('admin.users.with-escorts')
            ->with('message_success',$message_success);
    }

    public function withBusinessesIndex()
    {
        $message_success = Session::get('message_success');

        return View::make('admin.users.with-businesses')
            ->with('message_success',$message_success);
    }

    public function usersAjax()
    {
        $query = User::select('*');

        $datatables = Datatables::of($query)
            ->editColumn('full_name','<a href="/admin/users/{{$id}}">{{$full_name}}</a>')
            ->editColumn('active','{{ Helpers::get_active($active) }}')
            ->editColumn('super_user','{{ Helpers::get_active($super_user) }}')
            ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
            ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
            ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
            ->addColumn('action',function($user){
                return '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$user->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Action
                                <span class="caret"></span>
                            </button>
                            <ul data-user-id="'.$user->id.'" class="dropdown-menu" aria-labelledby="actions-'.$user->id.'">
                                <li><a class="edit-user" href="/admin/users/'.$user->id.'">Edit</a></li>
                                <li><a class="reset-password" href="#" data-email="'.$user->email.'">Reset password</a></li>
                                <li><a class="verify-email" href="#">Verify Email</a></li>
                                <li><a class="delete" href="#">Delete</a></li>
                                <li><a class="notes" href="#">Notes</a></li>
                            </ul>
                        </div>';
            });
            
        return $datatables->make(true);
    }

    public function administratorsAjax()
    {
        $query = User::select('*')->whereSuperUser(1);

        $datatables = Datatables::of($query)
            ->editColumn('full_name','<a href="/admin/users/{{$id}}">{{$full_name}}</a>')
            ->editColumn('active','{{ Helpers::get_active($active) }}')
            ->editColumn('super_user','{{ Helpers::get_active($super_user) }}')
            ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
            ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
            ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
            ->addColumn('action',function($user){
                return '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$user->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Action
                                <span class="caret"></span>
                            </button>
                            <ul data-user-id="'.$user->id.'" class="dropdown-menu" aria-labelledby="actions-'.$user->id.'">
                                <li><a class="edit-user" href="/admin/users/'.$user->id.'">Edit</a></li>
                                <li><a class="reset-password" href="#" data-email="'.$user->email.'">Reset password</a></li>
                                <li><a class="verify-email" href="#">Verify Email</a></li>
                                <li><a class="delete" href="#">Delete</a></li>
                                <li><a class="notes" href="#">Notes</a></li>
                            </ul>
                        </div>';
            });
            
        return $datatables->make(true);
    }

    public function withEscortsAjax()
    {
        $query = User::select('*')->has('escorts');

        $datatables = Datatables::of($query)
            ->editColumn('full_name','<a href="/admin/users/{{$id}}">{{$full_name}}</a>')
            ->editColumn('active','{{ Helpers::get_active($active) }}')
            ->editColumn('super_user','{{ Helpers::get_active($super_user) }}')
            ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
            ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
            ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
            ->addColumn('action',function($user){
                return '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$user->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Action
                                <span class="caret"></span>
                            </button>
                            <ul data-user-id="'.$user->id.'" class="dropdown-menu" aria-labelledby="actions-'.$user->id.'">
                                <li><a class="edit-user" href="/admin/users/'.$user->id.'">Edit</a></li>
                                <li><a class="reset-password" href="#" data-email="'.$user->email.'">Reset password</a></li>
                                <li><a class="verify-email" href="#">Verify Email</a></li>
                                <li><a class="delete" href="#">Delete</a></li>
                                <li><a class="notes" href="#">Notes</a></li>
                            </ul>
                        </div>';
            });
            
        return $datatables->make(true);
    }

    public function withBusinessesAjax()
    {
        $query = User::select('*')->has('businesses');

        $datatables = Datatables::of($query)
            ->editColumn('full_name','<a href="/admin/users/{{$id}}">{{$full_name}}</a>')
            ->editColumn('active','{{ Helpers::get_active($active) }}')
            ->editColumn('super_user','{{ Helpers::get_active($super_user) }}')
            ->editColumn('phone','{{ Helpers::format_phone_number($phone) }}')
            ->editColumn('email','<a href="mailto:{{$email}}">{{$email}}</a>')
            ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
            ->addColumn('action',function($user){
                return '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$user->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Action
                                <span class="caret"></span>
                            </button>
                            <ul data-user-id="'.$user->id.'" class="dropdown-menu" aria-labelledby="actions-'.$user->id.'">
                                <li><a class="edit-user" href="/admin/users/'.$user->id.'">Edit</a></li>
                                <li><a class="reset-password" href="#" data-email="'.$user->email.'">Reset password</a></li>
                                <li><a class="verify-email" href="#">Verify Email</a></li>
                                <li><a class="delete" href="#">Delete</a></li>
                                <li><a class="notes" href="#">Notes</a></li>
                            </ul>
                        </div>';
            });
            
        return $datatables->make(true);
    }

    public function getUserNew()
    {
        $message_success = Session::get('message_success');
        return View::make('admin.users.create')
                ->with('message_success',$message_success);
    }

    public function postUserNew()
    {
        $input = Input::all();
        $user = new User;
        $user->full_name = $input['full_name'];
        $user->email = $input['email'];
        $user->phone = $input['phone'];
        $user->password = Hash::make($input['password']);
        $user->active = (isset($input['active']) ? 1 : 0);
        $user->type = $input['type'];
        $user->super_user = (isset($input['super_user']) ? 1 : 0);
        $user->save();

        Session::flash('message_success', 'User '.$user->full_name.' created!');
        return Redirect::to('/admin/users');
    }

    public function getUserEdit($id)
    {
        $user = User::find($id);
        $message_success = Session::get('message_success');
        return View::make('admin.users.single',compact('user'))
                ->with('message_success',$message_success);
    }

    public function postUserEdit($id)
    {
        $input = Input::all();
        $user = User::find($id);
        $user->full_name = $input['full_name'];
        $user->email = $input['email'];
        $user->phone = $input['phone'];
        $user->type = $input['type'];
        $user->active = (isset($input['active']) ? 1 : 0);
        $user->super_user = (isset($input['super_user']) ? 1 : 0);
        $user->save();

        Session::flash('message_success', 'User updated!');
        return Redirect::to('/admin/users/'.$id);
    }

    public function deleteUser()
    {
        $user = User::find(Input::get('user_id'));
        $user->delete();
    }
}















