<?php

/**
 * Class FooterPostsComposer
 */
class FooterPostsComposer
{

    /**
     * @param $view
     */
    public function compose($view)
    {
        $posts = Post::wherePublished(1)
                        ->where('date_from','<=',date('Y-m-d'))
                        ->where('date_to','>=',date('Y-m-d'))
                        ->orderBy('created_at','DESC')
                        ->take(4)
                        ->get();

        $view->with('posts', $posts);
    }


}
