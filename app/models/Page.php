<?php

class Page extends \Eloquent {
	protected $fillable = [];

    public static function boot()
    {
        static::saved(function()
        {
            Event::fire('sitemap.generate');
        });
    }
}