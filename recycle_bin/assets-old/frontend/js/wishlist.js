function getUserWishlist(userId)
{
    // load current user's wishlist
    $.ajax({
            url: '/getUserWishlist/'+userId,
            // url: '{{ url('getUserWishlist/'.Auth::user()->id) }}',
            dataType: 'json',
        })
        .done(function(data) {
            var wishlist = data;
            // for each wishlist element
            $.each(wishlist, function(wishIndex, val) {
                var escortId = val.escort_id,
                    // Find the div corresponding with the wishlist
                    wishedEscorts = $('.escorts-wishlist #'+escortId),
                    // For escort profile widget
                    wishlistWidget = $('li.wishlist-widget'),
                    currentEscort = $('#profile-details').data('escort-id');

                // Replace the button in the escort profile widget
                if (escortId == currentEscort) {
                    wishlistWidget.find('.is-not-wish').hide();
                    wishlistWidget.find('.is-wish').show();     
                };

                // Replace the button in the loops
                wishedEscorts.each(function(index, el) {
                    var $this = $(this);
                    $this.find('.is-not-wish').hide();
                    $this.find('.is-wish').show();
                });
            });
        });
}

function addToWishlist(e,clicked,userId)
{
    e.preventDefault();
    var container = clicked.closest('.escorts-img-container');

    if (container.length == 0)
        var escortId = clicked[0].id;
    else
        var escortId = container[0].id;

    console.log(escortId);

    if (clicked.hasClass('is-not-wish')) {
        clicked.hide();
        clicked.siblings('.is-wish').show();
    } else {
        $('.escorts-wishlist #'+escortId).each(function(index, el) {
            $(this).find('.is-not-wish').hide();
            $(this).find('.is-wish').show();
        });               
    }
    $.ajax({
        url: '/addToWishlist',
        data: {
            escort_id: escortId,
            user_id: userId
        },
    });
}

function removeFromWishlist(e,clicked,userId)
{
    e.preventDefault();
    var container = clicked.closest('.escorts-img-container');

    if (container.length == 0)
        var escortId = clicked[0].id;
    else
        escortId = container[0].id;

    if (clicked.hasClass('is-wish')) {
        clicked.hide();
        clicked.siblings('.is-not-wish').show();
    } else {
        $('.escorts-wishlist #'+escortId).each(function(index, el) {
            $(this).find('.is-wish').hide();
            $(this).find('.is-not-wish').show();
        });                
    }

    $.ajax({
        url: '/removeFromWishlist',
        data: {
            escort_id:  escortId,
            user_id: userId
        },
    });
}






















