<?php

class ContactController extends Controller {

	public function getContact()
	{
		$message = Session::get('message');
		$meta = Page::where('url_key','=','contact')->first();
		return View::make('frontend.contact',compact('meta'))
			->with('message',$message);
	}

	public function postContact(){
		$data = Input::all();

		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required',
			'phone' => 'required',
			'message' => 'required',
		);

        $v = Validator::make($data, $rules);

        if ($v->fails())
            return Redirect::route('getContact')->withInput()->withErrors($v);

        $emailTemplate = EmailTemplate::where('name', 'contact')->first();

        Mail::send('emails.'.$emailTemplate->name, array('data' => $data), function ($message) use($data) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate Contact Form - '.$data['name'])
                    ->subject('My Playmate Contact Enquiry')
                    ->to('contact@myplaymate.com.au');
        });

        return Redirect::route('getContact')
        	->with('message','Contact enquiry successfully sent!');
	}

	public function getReport()
	{
		$meta = Page::whereName('report')->first();
		$message = Session::get('message');

		return View::make('frontend.report')
			->with('meta',$meta)
			->with('message',$message);
	}

	public function postReport(){
		try {		
			$data = Input::all();

			$rules = array(
				'escort_name' => 'required',
				'name' => 'required',
				'phone' => 'required',
				'email' => 'required|email',
				'subject' => 'required',
				'message' => 'required',
			);

			$v = Validator::make($data, $rules);

			if ($v->fails())
				return Redirect::route('getReport')->withInput()->withErrors($v);

			$emailTemplate = EmailTemplate::where('name', 'report')->first();

			Mail::send('emails.'.$emailTemplate->name, array('data' => $data), function ($message) use($data) {
				$message->from(Config::get('app.adminEmail'), 'My Playmate Escort Report Form - '.$data['name'])
						->subject('My Playmate Escort Report')
						->to('contact@myplaymate.com.au');
			});

			return Redirect::route('getReport')
				->with('message','Escort report successfully sent!');
				
		} catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
	}

}
