function applyFilters(page)
{
    var view = Cookies.get('view') || 'grid',
        page = page || 0,
        paginationCount = Cookies.get('paginationCount') || 0,
        sorting = jQuery('#sort-by select option:selected').val() || 'default',
        suburb_id = jQuery('body').data('suburb-id') || Cookies.get('suburb'),
        business = jQuery('body').data('business'),
        my_wishlist = jQuery('body').data('wishlist'),
        escort_name = getUrlVars()['s'] || jQuery('#search-box').val(),
        
        usedFilters = jQuery('#used-filters'),
        input = {};

    usedFilters.children('li').not('.used-filter-template').each(function() {
        var $this = $(this),
            filterName = $this[0].id,
            elements = [];
        $this.find('ul a').each(function(index, el) {
            elements.push($(el)[0].id);
        });
        input[filterName] = elements;
    });

    console.dir(input);

    // Wipe if it's the first turn of pagination (for sorting and view change)
    if (page === 0)
        wipeEscorts();
    jQuery('.spinner').fadeIn(200);


    jQuery.ajax({
        url: '/escorts/filters/apply',
        data: {
            input:              input,
            escort_name:        escort_name,
            sorting:            sorting,
            suburb_id:          suburb_id,
            page:               page,
            paginationCount:    paginationCount,
            business:           business,
            my_wishlist:        my_wishlist,
            view:               view
        },
    })
    .done(function(data) {
        if (view == 'grid')
            jQuery('.escort-container').removeClass('list-view').addClass('grid-view');
        else
            jQuery('.escort-container').removeClass('grid-view').addClass('list-view');

        console.dir(data);
        $('#directory').html(data);
        // loadEscorts(data,page);
    });
}



// function applyFilters(page){
//     var view = Cookies.get('view') || 'grid',
//         page = page || 0,
//         paginationCount = Cookies.get('paginationCount') || 0,
//         sorting = jQuery('#sort-by select option:selected').val() || 'default',
//         suburb_id = jQuery('body').data('suburb-id') || Cookies.get('suburb'),
//         business = jQuery('body').data('business'),
//         my_wishlist = jQuery('body').data('wishlist'),

//         usedFilters = jQuery('#used-filters'),
//         ageFilter   = usedFilters.find('#age'),
//         genderFilter = usedFilters.find('#gender'),
//         ethnicityFilter = usedFilters.find('#ethnicity'),
//         bodyFilter = usedFilters.find('#body-type'),
//         hourlyRatesFilter = usedFilters.find('#hourly-rates'),
//         servicesFilter = usedFilters.find('#services'),
//         incallsOutcallsFilter = usedFilters.find('#incalls-outcalls'),
//         internationalFilter = usedFilters.find('#international-travel'),
//         heightFilter = usedFilters.find('#height'),
//         hairColorFilter = usedFilters.find('#hair-color'),
//         eyeColorFilter = usedFilters.find('#eye-color'),
//         bodyArtFilter = usedFilters.find('#body-art'),
//         smokeFilter = usedFilters.find('#smokes'),

//         escort_name = getUrlVars()['s'] || jQuery('#search-box').val(),
//         age = [],
//         gender = [],
//         ethnicity = [],
//         body = [],
//         hourly_rates = [],
//         services = [],
//         incallsOutcalls = [],
//         international = '',
//         height = [],
//         hair_color = [],
//         eye_color = [],
//         tattoos = '',
//         piercings = '',
//         smoke = '';

//     var input = [];

//     if (ageFilter.length) {
//         jQuery.each(ageFilter.find('li'), function(index, val) {
//             var range = ageFilter.find('ul a')[index].id;
//             age.push(makeAgeQuery(range));
//         });
//     };
//     if (genderFilter.length) {
//         jQuery.each(genderFilter.find('li'), function(index, val) {
//             gender.push(genderFilter.find('ul a')[index].text);
//         });
//     };
//     if (ethnicityFilter.length) {
//         jQuery.each(ethnicityFilter.find('li'), function(index, val) {
//             ethnicity.push(ethnicityFilter.find('ul a')[index].text);
//         });
//     };
//     if (bodyFilter.length) {
//         jQuery.each(bodyFilter.find('li'), function(index, val) {
//             body.push(bodyFilter.find('ul a')[index].text);
//         });
//     };
//     if (hourlyRatesFilter.length) {
//         jQuery.each(hourlyRatesFilter.find('li'), function(index, val) {
//             var range = hourlyRatesFilter.find('ul a')[index].id;

//             hourly_rates.push(makeHourlyRatesQuery(range));
//         });
//     };
//     if (servicesFilter.length) {
//         jQuery.each(servicesFilter.find('li'), function(index, val) {
//             services.push(servicesFilter.find('ul a')[index].text);
//         });
//     };
//     if (incallsOutcallsFilter.length) {
//         jQuery.each(incallsOutcallsFilter.find('li'), function(index, val) {
//             incallsOutcalls.push(incallsOutcallsFilter.find('ul a')[index].text);
//         });
//     };
//     if(internationalFilter.length)
//         international = 1;

//     if (heightFilter.length) {
//         jQuery.each(heightFilter.find('li'), function(index, val) {
//             height.push(heightFilter.find('ul a')[index].text);
//         });
//     };
//     if (hairColorFilter.length) {
//         jQuery.each(hairColorFilter.find('li'), function(index, val) {
//             hair_color.push(hairColorFilter.find('ul a')[index].text);
//         });
//     };
//     if (eyeColorFilter.length) {
//         jQuery.each(eyeColorFilter.find('li'), function(index, val) {
//             eye_color.push(eyeColorFilter.find('ul a')[index].text);
//         });
//     };
//     if (bodyArtFilter.length) {
//         if (bodyArtFilter.find('#tattoos').length){
//             tattoos = 1;
//         }
//         if (bodyArtFilter.find('#piercings').length)
//             piercings = 1;
//     };
//     if (smokeFilter.length) {
//         if (smokeFilter.find('#yes').length)
//             smoke = 1;
//         else
//             smoke = 0;
//     }; 

//     // Wipe if it's the first turn of pagination (for sorting and view change)
//     if (page === 0)
//         wipeEscorts();
//     jQuery('.spinner').fadeIn(200);

//     jQuery.ajax({
//             url: '/escorts/filters/apply',
//             data: {
//                 escort_name:        escort_name,
//                 age:                age,
//                 gender:             gender,
//                 ethnicity:          ethnicity,
//                 body:               body,
//                 hourly_rates:       hourly_rates,
//                 services:           services,
//                 incallsOutcalls:    incallsOutcalls,
//                 international:      international,
//                 height:             height,
//                 hair_color:         hair_color,
//                 eye_color:          eye_color,
//                 tattoos:            tattoos,
//                 piercings:          piercings,
//                 smoke:              smoke,
//                 sorting:            sorting,
//                 suburb_id:          suburb_id,
//                 page:               page,
//                 paginationCount:    paginationCount,
//                 business:           business,
//                 my_wishlist:        my_wishlist,
//             },
//         })
//         .done(function(data) {
//             if (view == 'grid')
//                 jQuery('.escort-container').removeClass('list-view').addClass('grid-view');
//             else
//                 jQuery('.escort-container').removeClass('grid-view').addClass('list-view');

//             loadEscorts(data,page);
//         });   
// }








function infiniteScroll(){
    var page = 0;
    jQuery(window).scroll(function() {
        var suburb_id = suburb_id || '',
            scroll = jQuery(window).scrollTop(),
            winHeight = jQuery(window).height(),
            sum = scroll + winHeight,
            container = jQuery('#directory-container'),
            distance = container.offset().top + container.height();
        if(sum > distance - 50 && sum < distance + 50) {
            page++;
            if (window.applied == 0){
                applyFilters(page);
                window.applied = 1;
            }
        }
    });
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function wipeEscorts(){
    jQuery('.escort-container .escorts-wishlist').each(function(index, el) {
        // Remove everything about the empty templates
        if (!jQuery(this).hasClass('grid-template') && !jQuery(this).hasClass('list-template')) {
            jQuery(this).remove();
        }
    });    
}

function loadEscorts(data,page){
    jQuery('.spinner').fadeOut(200);
    // If there are no more escorts to load, return
    if (data.length === 0 && page > 0){
        return;
    }
    var view = Cookies.get('view') || 'grid',
        page = page || 0,
        directory = jQuery('#directory');

    // Wipe eventual empty query message
    directory.find('h3').remove();

    // If there is no data, wipe and show empty query message and return
    if (data.length == 0) {
        wipeEscorts();

        directory.append('<h3>No escorts found with these criteria. Try again!</h3>');
        return;
    };

    jQuery.each(data, function(index, escort) {
        var template = jQuery('.'+view+'-template');
        // For each escort loaded, create a clone of the template
        var clone = template.clone();
        // Remove escort-template class to prevent infinite loop
        // Add clone class to enable infinite scroll
        clone.removeClass(view+'-template').addClass('clone');

        // Add data-distance to each container
        clone.attr('data-distance', escort.distance);

        // Populate the clone
        var link = clone.children('.escorts-img-container').children('a'),
            readmore = clone.find('.fig-readmore a'),
            readmoreHref = readmore.attr('href');

        readmore.attr('href',readmoreHref+'/'+escort.seo_url);
        jQuery.each(link, function(index, val) {
            var $this = jQuery(this),
                href = $this.attr('href');
            $this.attr('href', href+'/'+escort.seo_url);
        });
        clone.find('.escorts-img-container').attr('id', escort.id);
        clone.find('.figname').append(escort.escort_name);
        clone.find('.escort-location').prepend(escort.escortLocation);
        clone.find('.escort-age').prepend(escort.escortAge);
        clone.find('.escort-distance').prepend('~'+escort.distance);
        clone.find('.escort-description').prepend(escort.about_me);
        if (escort.mainImageUrl)
            clone.find('.img-profile').attr('src', '/uploads/escorts/thumbnails/thumb_220x330_'+escort.mainImageUrl);
        else
            clone.find('.img-profile').attr('src', '/assets/frontend/img/profile_template.jpg');
    

        if (escort.isWishlist == true) {
            clone.find('.is-not-wish').hide();
            clone.find('.is-wish').show();
        };

        if (escort.featured == 0)
            clone.find('.ribbon-featured').hide();

        // Add the clone to the list
        var container = jQuery('.escort-container');
        container.append(clone);

        if (jQuery('#sort-by select').val() === 'my-location') {
            container.children('li').sort(function(a,b){
                return +a.getAttribute('data-distance') - +b.getAttribute('data-distance');
            }).appendTo(container);
        }

        window.applied = 0;
    });
    // In the end, hide only the empty template
    jQuery('.escorts-wishlist').show();
    jQuery('.grid-template').first().hide();
    jQuery('.list-template').first().hide();
}

function toggleFilterDropdown(e){
    e.preventDefault();
    var li = $(this).siblings('ul').find('li');
    li.each(function(index, el) {
        if (!$(this).hasClass('hidden'))
            $(this).slideToggle(200);
    });
}

function makeAgeQuery(range){
    var min = range.substring(0,2),
        max = range.substring(2);

    if(max == 70)
        max = '70+';

    return [min, max];
}

function makeHourlyRatesQuery(range){
    var min = range.split('-')[0],
        max = range.split('-')[1];
    if (range == '500') {
        min = 500;
        max = 1000000;
    }
    return [min,max];
}

function addFilter(e)
{
    e.preventDefault();

    var $this = $(this),
        usedFiltersContainer = $('#used-filters-container'),
        usedFilters          = $('#used-filters'),
        filterParent         = $this.closest('li.filter-name').children('a'),
        filterName           = filterParent.text(),
        filterNameId         = filterParent[0].id,
        filterValue          = $this.text(),
        filterValueId        = $this[0].id,
        usedFilterType       = usedFilters.children('#'+filterNameId);

    usedFiltersContainer.show();

    $this.closest('li').hide();

    if (usedFilterType.length > 0)
    {
        var innerList  = usedFilterType.find('ul'),
            valueClone = $('.used-filter-template')
                        .find('.filter-value')
                        .clone()
                        .show()
                        .appendTo(innerList);

        valueClone.children('a').attr('id', filterValueId).prepend(filterValue);
    }
    else
    {
        var clone = usedFilters
                        .children('.used-filter-template')
                        .clone()
                        .removeClass('used-filter-template')
                        .attr('id',filterNameId)
                        .appendTo(usedFilters)
                        .show(),
            innerLi = clone.find('li');

        innerLi.show();
        clone.children('a').attr('data-filter-name', filterNameId).text(filterName);
        clone.find('ul>li>a').attr('id', filterValueId).prepend(filterValue);
    }

    if ($this.data('filter-name') == 'international-travel')
    {
        clone.find('ul').hide();
        clone.find('a').append('<i class="fa fa-times"></i>');
    }

    applyFilters();
}


jQuery(document).ready(function($) {
    var usedFiltersContainer = $('#used-filters-container'),
        usedFilters = $('#used-filters'),
        unusedFilters = $('#escort-filters'),
        container = $('.escort-container');

    // Hide
    unusedFilters.find('ul li').hide();
    usedFilters.find('.used-filter-template').hide();

    $('#escort-filters>li>a').on('click', toggleFilterDropdown);

    unusedFilters.on('click', 'ul>li>a, #international-travel', addFilter);

    // Remove filter
    usedFilters.on('click', 'a', function(e) {
        e.preventDefault();
        var $this = $(this),
            filter = $this.closest('li');

        if (filter.hasClass('filter-name')) {
            var filterNameId = filter[0].id;
            if (filter[0].id == 'international-travel') {
                unusedFilters
                    .find('#'+filterNameId)
                    .parent('li')
                    .show();
            }

            unusedFilters.find('#'+filterNameId)
                         .siblings('ul')
                         .find('li').show();
        }
        else // if it's a filterValue
        {
            var filterNameLi = $this.closest('li.filter-name'),
                filterNameId = filterNameLi.data('filter-name'),
                filterId = $this[0].id;

            unusedFilters.find('#'+filterNameId)
                         .siblings('ul')
                         .find('#'+filterId)
                         .closest('li')
                         .show();

            // If the container is empty, remove the filter-name
            if (filter.closest('ul').children('li').length == 1)
                filterNameLi.remove();
        }

        // Delete the whole filterName
        filter.remove();

        if (usedFilters.children('li').length == 1)
            usedFiltersContainer.slideUp(200);

        applyFilters();
    });

    // Remove all filters
    $('body').on('click', '#remove-filters', function(e) {
        e.preventDefault();
        usedFilters.children('li').not('.used-filter-template').remove();

        unusedFilters.find('ul li').slideUp(200);

        usedFiltersContainer.slideUp(200);

        applyFilters();
    });

    // Change sorting value
    $('body').on('change', '#sort-by select', function(e) {
        e.preventDefault();
        if ($(this).val() === 'my-location') {
            container.children('li').sort(function(a,b){
                return +a.getAttribute('data-distance') - +b.getAttribute('data-distance');
            }).appendTo(container);
        } else
            applyFilters();
    });
});






















