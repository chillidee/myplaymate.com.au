<?php

namespace spec\MyPlaymate\Services;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use MyPlaymate\Filters\LocationFilter;

class FilterServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Services\FilterService');
    }

}
