@extends('frontend.layout')

@section('content')

<!-- #page-title start -->
<section id="page-title" class="page-title-1">
    <div class="container">
    	<div class="meta">
	        <h1>{{ strtoupper($post->title) }}</h1>
    	</div>
        <div class="breadcrumb-container">
            <span>You are here: </span>
            <ul class="breadcrumb">
                <li><a href="{{ url('') }}">HOME</a></li>
                <li><a href="/blog">BLOG</a></li>
                <li><a href="#">{{ strtoupper($post->title) }}</a></li>
            </ul><!-- .breadcrumb end -->
        </div><!-- .breadcrumb-container end -->
    </div><!-- .containr end -->
</section><!-- #page-title end -->

<div class="blog-post">
    <section class="page-wrapper top-space">
        <!-- .container start -->
        <div class="container">
            <!-- .row start -->
            <div class="row">
				<aside class="col-md-3 aside aside-left triggerAnimation animated visible-lg visible-md stickyfloated fadeInLeft" data-animate="fadeInLeft">
					<ul class="aside-widgets">
				    	@include('frontend.partials.sidebar.left-common-widgets')
					</ul><!-- .aside-widgets end -->
				</aside><!-- .aside.aside-left end -->

                <article class="col-md-9 col-sm-12 col-xs-12">
                	@if($post->thumbnail)
                		<img src="{{ '/uploads/posts/'.$post->thumbnail }}" alt="{{ $post->title }}" class="pic">
                	@endif

                    <div class="triggerAnimation animated" data-animate="fadeInRight">
                        <p class="blog">{{ $post->content }}</p>

						<div class="comments">
						    <h3>Comments</h3>
						    @if(!empty($comments))
							    @foreach($comments as $comment)
								    <div class="well well-lg">
								        <span class="pull-right">{{ Helpers::db_date_to_user_date($comment->created_at) }}</span>
								        <h4 class="list-group-item-heading">{{ $comment->author }}</h4>
								        <p class="list-group-item-text">{{ $comment->comment }}</p>
								    </div>
							    @endforeach					    	
						    @endif
	
						    <div class="well well-lg">
							    <div class="alert alert-success">Comment submitted! Please wait for the administrators approval before you can see it listed here.</div>
								{{ Form::open(['class'=>'parsley']) }}
						            <div class="row">
						                <div class="col-lg-12">
						                    <div class="form-group">
						                    	{{ Form::label('author','Name:') }}
						                    	{{ Form::text('author','',array('class'=>'form-control author','required','placeholder'=>'Name')) }}
						                    </div>

						                    <div class="form-group">
						                    	{{ Form::label('comment','Comment:') }}
						                    	{{ Form::textarea('comment','',array('class'=>'form-control comment','required','placeholder'=>'Place your comment here')) }}
						                    </div>
						                    <div class="form-group">
						                    	{{ Form::hidden('post_id',$post->id,array('class'=>'post_id')) }}
						                    	{{ Form::submit('SEND COMMENT',array('class'=>'btn submit-button btn-default pull-right','id'=>'send-comment')) }}
						                    </div>
						                </div>
						            </div>
								{{ Form::close() }}
						    </div>
						</div>
                    </div><!-- .triggerAnimation.animated end -->
                </article><!-- .col-md-9 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </section><!-- End of the Boxed Version Page -->
</div>

@stop

@section('javascript')

	<script>
		(function($){
			$('#send-comment').on('click', function(e) {
				e.preventDefault();
				var $this   = $(this),
					form 	= $this.parents('form'),
					author  = form.find('input.author').val(),
					post_id = form.find('input.post_id').val(),
					comment = form.find('textarea.comment').val();

				$.ajax({
					url: '/sendComment',
					data: {
						author:  author,
						post_id:  post_id,
						comment: comment
					},
				})
				.done(function(data) {
					console.log(data);
					$('.alert-success').fadeIn(300);
				});
			});
		})(jQuery);
	</script>

@stop











