<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $guarded = [];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public static $rules = [
        'full_name' => 'required',
        'email' => 'required|unique:users,email|email',
        'phone' => 'required|regex:/0\d{9}(?!.)/|unique:users,phone',
        'password' => 'required|confirmed',
        'terms_and_conditions' => 'required'
    ];

    public static $messages = [
        'phone.regex' => 'A valid australian phone number is required. (eg 0411223344 or 0211223344)'
    ];

    public static $updateRules = [];

    public function escorts()
    {
        return $this->hasMany('Escort');
    }
    public function payments()
    {
        return $this->hasMany('Payment');
    }

    public function escort()
    {
        $escorts = $this->escorts;

        foreach ($escorts as $key => $escort) {
            if ($escort->under_business == 0)
                return $escort;
        }
    }
  
  public function BusinessEscorts()
    {
        $escorts = $this->escorts;
        $arr = array();
        foreach ($escorts as $key => $escort) {
          if ($escort->under_business == 0) {
                $arr[0][] = $escort;
          }
          else {
            $arr[$escort->business_id][] = $escort;
          } 
        }
        return $arr;
    }
    public function getFirstBusiness() {
        return $this->hasMany('Business')->first();
    }
    public function FirstBusinessEscort() {
        $business = $this->getFirstBusiness();
    }
    public function BusinessEscortsReviews()
    {
        $escorts = $this->escorts;
        $arr = array();
        foreach ( $escorts as $escort ) {
           $reviews = EscortReview::where( 'escort_id', '=', $escort->id );
          if ( !empty( $reviews) && count( $reviews ) > 0 ) {
            foreach( $reviews as $review ) {
                  $arr[] = $review;
            }
          }
        }   
        return $arr;
    }


    public function businesses()
    {
       return $this->hasMany('Business');
        
      /*  foreach( $businesses as $business ) {
            $escorts = Escorts::where( 'under_business','=', $business->id )->get();
            $business->escorts = 'hi che';
        }
      return $businesses;*/
    }

    public function wishlist()
    {
        return $this->hasMany('Wishlist');
    }

     public function getFaves()
    {
        $faves = $this->hasMany('Wishlist')->get();
        $arr = array();
        foreach( $faves as $escort ) {
            $esc = Escort::where( 'id','=', $escort->escort_id )->first();
            if ( !$esc )
                 continue;
            if ( $esc->main_image_id ){
              $escImage = EscortImage::where( 'id','=', $esc->main_image_id )->first();
              $esc->image = $escImage->filename;
            }
            $arr[] = $esc;
        }
        return $arr;
    }

    public function scopeNotSuperUsers($query)
    {
        return $query->where('superuser', 0);
    }

    public function isSuperUser()
    {
        return $this->super_user;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the user Type
     *
     * @return string
     */
    public function getUserType()
    {
        return $this->type;
    }
    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function sendActivationEmail()
    {
        $emailTemplate = EmailTemplate::where('name', 'activation-code')->first();

        $data                    = [];
        $data['full_name']       = $this->full_name;
        $data['email']           = $this->email;
        $data['activation_link'] = Config::get('app.fullUrl').'users/activate?email='.$this->email.'&activation_code='.$this->activation_code;
        $data['activation_code'] = $this->activation_code;
        $data['campaign_title']  = "";
        switch ($this->type) {
            case '2':
               $template = 'emails.activation-members';
            break;
            case '4':
                $template = 'emails.activation-business';
            break;
            default:
                $template = 'emails.activation-escorts';
            break;
    }

        // Send activation email to new user and to admin
        Mail::send( $template, ['data' => $data], function ($message) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Account Verification')
                    ->to([$this->email,Config::get('app.adminEmail')]);
        });
    }





    public function resetPasswordEmail()
    {
        $newPassword = str_random(8);

        $this->password = Hash::make($newPassword);
        $this->save();

        $emailTemplate = EmailTemplate::where('name', 'password-reset')->first();

        $data                    = [];
        $data['full_name']       = $this->full_name;
        $data['email']           = $this->email;
        $data['new_password']    = $newPassword;

        // Send activation email to new user and to admin
        Mail::send('emails.'.$emailTemplate->name, ['data' => $data], function ($message) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Password Reset')
                    ->to($this->email);
        });
    }

    public function adminResetPasswordEmail()
    {
        $newPassword = str_random(8);

        $this->password = Hash::make($newPassword);
        $this->save();

        $emailTemplate = EmailTemplate::where('name', 'admin-password-reset')->first();

        $templateData                 = [];
        $templateData['user']         = $this->toArray();
        $templateData['new_password'] = $newPassword;

        $emailData           = new stdClass();
        $emailData->user     = $this;
        $emailData->template = $emailTemplate;

        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);

            $message->to($emailData->user->email);
        });
    }

    public function sendAdminWelcome()
    {
        $emailTemplate = EmailTemplate::where('name', 'admin-welcome')->first();

        $templateData                 = [];
        $templateData['user']         = $this->toArray();
        $templateData['new_password'] = str_random(6);

        $this->password         = Hash::make($templateData['new_password']);
        $this->save();

        $emailData           = new stdClass();
        $emailData->user     = $this;
        $emailData->template = $emailTemplate;

        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);

            $message->to($emailData->user->email);
        });
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function isValidEmail($email, $id)
    {
        $user = self::where('email', '=', $email)->get()->last();

        if (empty($user) or $user->id == $id) {
            return true;
        }

        return false;
    }

    public function isValidStorage($id)
    {
        $user = self::find($id);

        return (empty($user) or empty($user->email)) ? false : true;
    }

    public function updateEmail($email, $id)
    {
        $user        = self::find($id);
        $user->email = $email;
        $user->save();
    }
}
