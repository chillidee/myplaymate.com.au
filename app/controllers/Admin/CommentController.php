<?php
namespace Admin;

use Controller, Datatables, View, Input;
use Comment;
class CommentController extends Controller {

  public function index()
  {
    $comments = Comment::all();
    return View::make('admin.comments.index',compact('comments'));
  }

  public function ajax()
  {
    $query = Comment::select('*')->orderBy('created_at', 'DESC');
    return Datatables::of($query)
      ->editColumn('post_id','{{ Post::find($post_id)->title }}')
      ->editColumn('status','{{ Helpers::get_status($status) }}')
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->addColumn('action', function ($comment) {
                return '<a href="#" data-comment-id="'.$comment->id.'" class="btn btn-xs btn-success approve"><i class="fa fa-check"></i> Approve</a>
                    <a href="#" data-comment-id="'.$comment->id.'" class="btn btn-xs btn-danger disapprove"><i class="fa fa-ban"></i> Disapprove</a>';
            })
      ->make(true);
  }

  public function approve()
  {
    $comment = Comment::find(Input::get('comment_id'));
    $comment->status = 2;
    $comment->save();
  }

  public function disapprove()
  {
    $comment = Comment::find(Input::get('comment_id'));
    $comment->status = 3;
    $comment->save();
  }

}