<!-- Wrapper -->
<table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="padding-top: 20px;" bgcolor="#ffffff" valign="top" width="100%"><!-- Start Header-->
			<table class="deviceWidth" style="width: 580px;" align="center" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td bgcolor="#ffffff" width="100%"><!-- Nav -->
						<table class="deviceWidth" align="right" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td class="center" style="font-size: 13px; color: #272727; font-weight: light; text-align: right; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 5px 0px; font-style: italic;"><a href="https://twitter.com/MyPlayMateAU"><img title="Twitter" src="http://myplaymate.com.au/twitter.jpg" alt="Twitter" height="35" border="0" width="35" /></a> <a href="http://instagram.com/myplaymateau"><img title="Instagram" src="http://myplaymate.com.au/instagram.jpg" alt="Instagram" height="35" border="0" width="35" /></a> <a href="https://www.facebook.com/MyPlayMateAU"><img title="Facebook" src="http://myplaymate.com.au/facebook.jpg" alt="Facebook" height="35" border="0" width="35" /></a> <a href="http://www.youtube.com/user/MyPlayMateAU?feature=watch"><img title="You Tube" src="http://myplaymate.com.au/youtube.jpg" alt="You Tube" height="35" border="0" width="35" /></a></td>
								</tr>
							</tbody>
						</table>
						<!-- End Nav --></td>
					</tr>
				</tbody>
			</table>
			<!-- End Header --> <!-- One Column -->
			<table class="deviceWidth" style="width: 580px;" align="center" bgcolor="#eeeeed" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td style="padding: 0;" bgcolor="#ffffff" valign="top"><a href="http://myplaymate.com.au/"><img class="deviceWidth" style="display: block; border-radius: 4px;" src="http://myplaymate.com.au/assets/emails/main-updated-4.png" alt="" border="0" /></a></td>
					</tr>
					<tr>
						<td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
							<table bgcolor="#eeeeed">
								<tbody>
									<tr>
										<td style="padding: 0 10px 10px 0;" valign="middle"><a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href="#">Hi {{$data['full_name']}},</a></td>
									</tr>
								</tbody>
							</table>
							<p>Here is your new temporary password: <span style="text-decoration: none; color: #b60011; font-weight: bold;">{{$data['new_password']}}<br /><a style="text-decoration: none; color: #b60011; font-weight: bold;" href="http://myplaymate.com.au/register">Click here</a> </span>to sign in at <a style="text-decoration: none; color: #b60011; font-weight: bold;" href="http://myplaymate.com.au">myplaymate.com.au</a> then under "Account Menu" &rArr; "Help Section", change your password to something that you will easily remember.</p>
							<p>Here are a couple of tips to make an easy to remember password:<br />1, Combine three small words of significance to you, and make a single password2, Choose two words and combine their letters</p>
							<p>We hope you continue enjoying being a Playmate!</p>
							<p>The MyPlaymate Team</p>
						</td>
					</tr>
				</tbody>
			</table>
			<!-- End One Column -->
			<div style="height: 15px;">&nbsp;</div>
			<!-- spacer --> <!-- 4 Columns -->
			<table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td style="padding: 30px 0;" bgcolor="#363636">
							<table class="deviceWidth" style="width: 580px;" align="center" bgcolor="#363636" border="0" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td>
											<table width="45%" cellpadding="0" cellspacing="0"  border="0" align="left" class="deviceWidth" bgcolor="#363636">
												<tr>
													<td valign="top" style="font-size: 11px; color: #f1f1f1; color:#999; font-family: Arial, sans-serif; padding-bottom:20px" class="center">
														You are receiving this email because<br/>
														1.) You're an awesome customer of "My Playmate Pty Ltd" <br/>
														2.) All participants are deemed to have agreed and are bound to the terms and conditions of use.<br/>

														<br/><br/>
														Want to be removed? No problem, please send an email to <a href="mailto: admin@myplaymate.com.au?subject=Unsubscribe%20Me&body=Please%20remove%20my%20email%20{{$data['email']}}%20from%20your%20mailing%20list%20group.%20Thank%20you,%20{{$data['full_name']}}." style="color:#999;text-decoration:underline;">admin@myplaymate.com.au</a> and we won't bug you again.
													</td>
												</tr>
											</table>
											<table class="deviceWidth" style="width: 40%;" align="right" bgcolor="#363636" border="0" cellpadding="0" cellspacing="0">
												<tbody>
													<tr>
														<td class="center" style="font-size: 11px; color: #f1f1f1; font-weight: normal; font-family: Arial, Helvetica, sans-serif; line-height: 26px; vertical-align: top; text-align: right;" valign="top"><a href="https://twitter.com/MyPlayMateAU"><img title="Twitter" src="http://myplaymate.com.au/twitter_grey.jpg" alt="Twitter" height="35" border="0" width="35" /></a> &nbsp; <a href="http://instagram.com/myplaymateau"><img title="Instagram" src="http://myplaymate.com.au/instagram_grey.jpg" alt="Instagram" height="35" border="0" width="35" /></a> &nbsp; <a href="https://www.facebook.com/MyPlayMateAU"><img title="Facebook" src="http://myplaymate.com.au/facebook_grey.jpg" alt="Facebook" height="35" border="0" width="35" /></a> &nbsp; <a href="http://www.youtube.com/user/MyPlayMateAU?feature=watch"><img title="You Tube" src="http://myplaymate.com.au/youtube_grey.jpg" alt="You Tube" height="35" border="0" width="35" /></a> <br /> <a style="text-decoration: none; color: #848484; font-weight: normal;" href="http://myplaymate.com.au/web-enquiry/">contact us</a><br /> <a style="text-decoration: none; color: #848484; font-weight: normal;" href="http://myplaymate.com.au/">website</a><br /> <a style="text-decoration: none; color: #848484; font-weight: normal;" href="http://myplaymate.com.au/privacy-policy">privacy policy</a></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<!-- End 4 Columns --></td>
		</tr>
	</tbody>
</table>
<!-- End Wrapper -->
<p></p>
