<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escort_rates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('escort_id');
			$table->float('incall_15_rate');
			$table->float('incall_30_rate');
			$table->float('incall_45_rate');
			$table->float('incall_1h_rate');
			$table->float('incall_2h_rate');
			$table->float('incall_3h_rate');
			$table->float('incall_overnight_rate');
			$table->float('outcall_15_rate');
			$table->float('outcall_30_rate');
			$table->float('outcall_45_rate');
			$table->float('outcall_1h_rate');
			$table->float('outcall_2h_rate');
			$table->float('outcall_3h_rate');
			$table->float('outcall_overnight_rate');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escort_rates');
	}

}
