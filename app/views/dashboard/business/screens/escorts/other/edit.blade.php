<div id ='overlayBackGround' ng-init = 'scopeInit()'></div>
<div class ='_MpoffWhite businessProfileScreen'>
 <div>
   <div class ='mobile_only' id ='navigateThroughProfile'>Show Profile Menu</div>
   <div class ='mPProfileTopMenu'>
     <div><div id ='mpBasicInfo' class ='active'></div><span>ESCORT</span></div>
     <div><div id ='mpGallery' ng-click ='getImages()'></div><span ng-click ='getImages()'>GALLERY</span></div>
     <div id ='new_escort' class="submit-button mobile-only" ng-click = 'createNewEscort()'>New Escort</div>
   </div>
 </div>
 <div class ='_mpProfile_holder escort_profile_form'>
  <message></message>
  <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
   <h3>ESCORT PROFILE INFORMATION</h3>
   <p>Saved and completed profiles go into moderation for approval before going live to the site.
     If you have any issues creating or editing your profiles, please contact My Playmate on 1300 769 766</p>

     <form role="form" class ='profileForm' controller ='busSaveBasic' ng-submit = 'saveEscortInfo()' id = 'escortInfo' ng-model = 'basicInfoForm'>
      <div class ='mp_image_float' >

        <div id="container_image">
         <img ng-src="{{Config::get('constants.CDN')}}/escorts/@{{main_image_src}}" alt="Your Profile Image" />
       </div>

     </div>
     <div class ='right_float'>
      {{--Form::hidden('main_image',$main_image->filename,array('ng-model'=>'basicInfoForm.image','id' => 'image')) --}}

      {{ Form::text('escort_name',$escort->escort_name,array('id' => 'escort_name', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Escort Name','ng-model'=>'basicInfoForm.escort_name', 'disabled')) }}

      <label id ='escort_age' class ='form_required'>Age:</label>{{ Form::select('age_id',$ages,$escort->age_id,array('id' =>'age_id','class'=>'form-control','ng-model'=>'basicInfoForm.age_id')) }}
      <div class="control-group" id="sex">
        <label class ='form_required left_margin_15'>Gender</label>
        {{ Form::radio('basicInfoForm.gender_id',1,$escort->gender_id == 1 ? 1 : 0,array('id'=>'gender_id-1','ng-model' => 'basicInfoForm.gender_id')) }}
        {{ Form::label('gender_id-1','Female') }}
        {{ Form::radio('basicInfoForm.gender_id',2,$escort->gender_id == 2 ? 1 : 0,array('id'=>'gender_id-2','ng-model' => 'basicInfoForm.gender_id')) }}
        {{ Form::label('gender_id-2','Male') }}
        {{ Form::radio('basicInfoForm.gender_id',3,$escort->gender_id == 3 ? 1 : 0,array('id'=>'gender_id-3','data-parsley-required','ng-model' => 'basicInfoForm.gender_id')) }}
        {{ Form::label('gender_id-3','Transexual') }}
      </div>
    </div> 


    {{ Form::label('servicesLabel','BIO',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
    <p>Bios give your escort profiles a little more character, this lets your clients know a little bit about the escort’s personality before they decide. </p>
    <p>Bios should be at least 75 words and no more than 200.</p>
    <p><b>(please note, all text will be checked for duplicate content. If duplicates are found your profile may be disapproved)</b></p>
    {{ Form::textarea('about_me','', array( 'id' => 'about_me','class'=>'form-control','ng-model'=>'basicInfoForm.about_me') )}}

    <div class="alignc">
      {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

    </div>
    {{ Form::close() }}
  </div>

  <div class ='mPtab' id = 'tab_mpGallery'>
    <form role="form" ng-controller ='galleryController' ng-submit = 'saveEscortGallery()' id = 'galleryForm' ng-model = 'profileGallery' class ='profileForm normalSelects'>                
      <h3>GALLERY</h3>
      <p>Upload and display a maximum of 10 images at a time. Delete, upload and refresh your images at any time. Drag and drop to rearrange photo order. Uploaded images will automatically set as pending until approved by My Playmate administration. </p>
      <p><b>Images need to be sexy, tasteful and of very good quality; no poor grainy images. Pornographic styled images will not be approved. 
      </b></p>
      <?php $mainImageFile = ( is_object ( $main_image ) ? $main_image->filename : '' ); ?>
      <?php $class = "{{image.filename == '" . $mainImageFile . "'?'main_image':''}}";?>

@include('dashboard.partials.escort-gallery')


                       <span class="progress ng-hide" ng-show="progress >= 0">
                        <div style="width:%" ng-bind="progress + '%'" class="ng-binding">%</div>
                      </span>
                      <span ng-show="result" class="ng-hide">Upload Successful</span>
                      <span class="err ng-binding ng-hide" ng-show="errorMsg"></span>

                      <?php $mainImage = ( is_object ( $main_image ) ? $main_image->id : '' ); ?>

                      {{ Form::text('main_image','',array('class'=>'form-control hidden','ng-model'=>'profileGallery.main_image')) }}
                      
                    <p>To select a profile image, hover over the image you want, then click the ‘Make main image’ bar.</p>
                    <div class="alignc next-previous">                    
						<input class="submit-button previous-button" type="submit" value="PREVIOUS">
						{{ Form::submit('FINISH',array('class'=>'submit-button')) }}
                    </div>
                  </form>
                </div>
<style>	
	.escort_profile_form .alignc{text-align:center}.alignc .submit-button{margin-bottom:0!important}.escort_profile_form .alignc .submit-button{margin-top:5px!important;margin-bottom:30px!important;display:inline-block!important}.escort_profile_form .previous-button{background:#0866c6!important}
</style>
                <script>
				
$(document).delegate('form', 'submit', function(event) {
	navigateProfile(this);
});

var action = '';

$(document).on("click", ":submit", function(e){				
	action = $(this).val();				
	if(action.toUpperCase() == '')
		action = 'NEXT';
});				

function navigateProfile(control) {

	var $form = $(control);
	var id = $form.attr('id');
	
	if(action == 'NEXT') {
		if(id == 'escortInfo') {			
			$('#mpGallery').click();
		}
	} else if (action == 'PREVIOUS') {
		if(id == 'galleryForm') {								
			$('#mpBasicInfo').click();				
		}
	}
}

reloadCode();

function reloadCode() {	
	if (window.location.hash.substr(1) == "/newEscort") {
		window.location.hash = '';				
		setTimeout(function(){ $('.escortContainer').children().last().children()[0].click(); $('#mpGallery').click(); }, 100);		
	}
}	
					
  $( '.mPProfileTopMenu div').on('click',function(e) {
    var tabName = $( this ).attr('id');
    if ( tabName == undefined ){
         var tabName = $( this ).children('div').attr('id');
    }
    $( '.mPtab').removeClass( 'activeTab');
    $( '#tab_' + tabName ).addClass( 'activeTab');
    $( '.mPProfileTopMenu div').removeClass( 'active');
    $( this ).addClass( 'active');
    if ( $( window ).width() <= 1024 ) {
      $( '.mPProfileTopMenu' ).hide();
      $( '#navigateThroughProfile' ).show();
    }
    
    e.stopPropagation();
  })

                 var postObject = {
                   userId: id,
                   csrf: $( '#_token').val(),
                   businessId: currentEscort
                 },
                 cropTypes = {
                   widescreen : true,
                   letterbox: false,
                   free: false
                 };

               </script>


