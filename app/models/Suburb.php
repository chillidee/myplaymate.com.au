<?php

class Suburb extends Eloquent
{
    protected $guarded = [];

    public $timestamps = false;

    public static $rules = [

    ];

    public function Postcode()
    {
        return $this->belongsTo('Postcode', 'postcode');
    }

    public function state()
    {
        return $this->belongsTo('State', 'state_id');
    }

    public function country()
    {
        return $this->belongsTo('Country');
    }

    public function profiles()
    {
        return $this->hasMany('Escort', 'current_suburb_id');
    }
}
