@extends('frontend.layout')

@section('content')
@if ( $meta->main_image )
<img src ='{{Config::get('constants.CDN')}}{{ $meta->main_image }}' class ='full_width' alt ='@if( isset($meta)&& $meta->img_alt != '') {{ $meta->img_alt }} @else Contact Myplaymate @endif'>
@endif

<!-- #page-title start -->
<section id="page-title" class="page-title-1 contact_page_section">
    <div class="container">
        <h1>@if( isset($meta)){{$meta->name}}@else Get in touch  @endif</h1>
    </div><!-- .containr end -->
</section><!-- #page-title end -->

<!-- .boxed version of the page -->
<section class="page-wrapper top-space">
    <!-- .container start -->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="triggerAnimation animated fadeInRight" data-animate="fadeInRight" style="">
                    <h5 class="mt15">PLEASE LEAVE A MESSAGE</h5>


                    @include('frontend.partials.message')

                    <div id="login-container">
                        {{ Form::open() }}
                        <fieldset>
                            <div class="control-group">
                                {{ Form::label('name','Name',array('class'=>'control-label')) }}<span>*</span>
                                <div class="controls">
                                    {{ Form::text('name','',array('class'=>'input-xlarge input03')) }}
                                </div>
                                @if($errors->has('name'))
                                <p class="form-error">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="control-group">
                                {{ Form::label('email','Email',array('class'=>'control-label')) }}<span>*</span>
                                <div class="controls">
                                    {{ Form::email('email','',array('class'=>'input-xlarge input03')) }}
                                </div>
                                @if($errors->has('email'))
                                <p class="form-error">{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                            <div class="control-group">
                                {{ Form::label('subject','Subject',array('class'=>'control-label')) }}<span>*</span>
                                <div class="controls">
                                    {{ Form::text('subject','',array('class'=>'input-xlarge input03')) }}
                                </div>
                                @if($errors->has('subject'))
                                <p class="form-error">{{ $errors->first('subject') }}</p>
                                @endif
                            </div>
                            <div class="control-group">
                                {{ Form::label('phone','Phone',array('class'=>'control-label')) }}<span>*</span>
                                <div class="controls">
                                    {{ Form::text('phone','',array('class'=>'input-xlarge input03')) }}
                                </div>
                                @if($errors->has('phone'))
                                <p class="form-error">{{ $errors->first('phone') }}</p>
                                @endif
                            </div>
                            <div class="control-group">
                                {{ Form::label('message','Message',array('class'=>'control-label')) }}<span>*</span>
                                {{ Form::textarea('message','',array('class'=>'wpcf7-textarea input03')) }}
                            </div>
                            @if($errors->has('message'))
                            <p class="form-error">{{ $errors->first('message') }}</p>
                            @endif
                            <div class="control-group">
                                <div class="controls" id="rightright">
                                    <div class="button3">
                                        {{ Form::submit('Send') }}
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        {{ Form::close() }}
                    </div>
                </div><!-- .triggerAnimation.animated end -->
                </form>
            </div><!-- .col-md-9 end -->

            <aside class="col-md-3 col-xs-12 aside aside-left triggerAnimation animated fadeInLeft" data-animate="fadeInLeft" style="">
                <ul class="aside-widgets">
                    <!-- .widget_navigation -->
                    <li class="widget widget_contact">
                        <h5 class="mt15">CONTACT INFO</h5>
                        <div id="mpm-contact">
                            @if( $meta->content)
                            {{$meta->content}}
                            @else
                            {{ StaticBlock::getHtml('contact-info') }}
                            @endif
                        </div>
                    </li><!-- .widget_Navigation end -->
                </ul><!-- .aside-widgets end -->
            </aside><!-- .aside.aside-left end -->
        </div>
    </div><!-- .container end -->
</section> <!-- End of the Boxed Version Page -->
@stop
