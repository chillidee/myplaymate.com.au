@extends('frontend.layout')

@section('content')

<section id="page-title" class="page-title-1">
    <div class="container">
        <h1>MANAGE USER PROFILE</h1>
        <div class="breadcrumb-container">
            <span>You are here: </span>
            <ul class="breadcrumb">
                <li><a href="{{ url('') }}">HOME</a></li>
                <li><a href="#">MANAGE USER PROFILE</a></li>
            </ul>
        </div>
    </div>
</section>

<div class="container">
	<div class="escort-process new-escort-process">
		<div class="process-container">
			<div class="process-item">
				<a href=""></a>
				<h4>ACCOUNT</h4>
			</div>

			<div class="process-item">
				<a href=""></a>
				<h4>PROFILE</h4>
			</div>

			<div class="process-item">
				<a href=""></a>
				<h4>DETAILS</h4>
			</div>

			<div class="process-item">
				<a href=""></a>
				<h4>TOURING</h4>
			</div>

			<div class="process-item">
				<a href=""></a>
				<h4>GALLERY</h4>
			</div>

			<div class="process-item">
				<a href=""></a>
				<h4>EXTRAS</h4>
			</div>

			<div class="process-item">
				<a href=""></a>
				<h4>PAYMENT</h4>
			</div>			
		</div>
	</div>
	<div class="spinner">
		<img src="/assets/frontend/img/tail-spin.svg"/>
	</div>

	<div id="form"></div>
	
</div>
@stop


@section('javascript')

	<script src="/assets/frontend/js/process.js"></script>

@stop







