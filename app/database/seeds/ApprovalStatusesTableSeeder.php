<?php

class ApprovalStatusesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('approval_statuses')->truncate();

        $statuses = [
            [
                'id' => 1,
                'name' => 'Pending'
            ],
            [
                'id' => 2,
                'name' => 'Approved'
            ],
            [
                'id' => 3,
                'name' => 'Disapproved'
            ],
        ];

        // Uncomment the below to run the seeder
        DB::table('approval_statuses')->insert($statuses);
    }
}
