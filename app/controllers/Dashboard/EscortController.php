<?php

namespace Dashboard;

use App, Controller, EscortTouring, EscortImage, View, BusinessPlan, Helpers, Response, Datatables, Input, Business, Auth, Escort, User, Age, Height, Ethnicity, EyeColor, Body, HairColor, Service, EscortLanguage, EscortService, EscortAvailability, EscortRate;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;
use AWS, DateTime, DateInterval, DatePeriod;
  
 class EscortController extends \ProfileController
  //class BusinessController extends Controller 

  {
    public function detail($id) {
        $ret = json_encode( $array );
        return 'angular.callbacks._0(' . $ret . ');';
    }
       public function panel($id) {
        $input = Input::get();
        $escort = Escort::where( 'id','=',$input['busid'] )->first();
         switch ( $input['panel']) {
             case '_MpOverview':
              if ( $escort ){
                switch ($escort->status ) {
                   case '1':
                      $status = 'Pending';     
                   break;
                   case '2':
                      $status = 'Approved';
                   break;
                   case '3':
                      $status = 'Disapproved';
                   break;
                   case '4':
                      $status = 'Unavailable';
                   break;
                }
              }
              else { $status = '0'; }
          return View::make('dashboard.escort.panels.overview', compact('escort','status'));
        break;
        case '_MpProfile':
            if ( $escort ){
               switch ($escort->status ) {
                   case '1':
                      $status = 'Pending';     
                   break;
                   case '2':
                      $status = 'Approved';
                   break;
                   case '3':
                      $status = 'Disapproved';
                   case '4':
                      $status = 'Unavailable';
                   break;
               break;
               }
               }
              else { $status = '0'; }
             return View::make('dashboard.escort.panels.profile', compact('escort','status'));
        break;
           case '_MpReviews':
              return '<h4 class="panelHeader accountPanelHeader">Manage Your Reviews</h4>';
        break;
         case '_MpPayments':
              return '<h4 class="panelHeader accountPanelHeader">Manage Your Payments</h4>';
        break;
         case '_MpAccount':
              return '<h4 class="panelHeader accountPanelHeader">Manage Your Account</h4>';
        break;
    default: return View::make('dashboard.panel.profile', compact('business','status','escorts'));
}
         return View::make('dashboard.panel', compact('panel'));
    }
   public function screen($id) {
        $input = Input::get();
        $escort = Escort::where( 'id','=',$input['busid'] )->first();
        if ( !$escort ) $escort = new Escort;
         switch ( $input['panel']) {
            case '_MpOverview':
            $user = User::where( 'id','=',$id)->first();
            $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
            return $this->overviewScreen($escort,$user,$plan);
        break;
      case '_MpProfile':
             return $this->profileScreen($escort);
        break;
        case '_MpReviews':
           $escort = Escort::where( 'id', '=', $input['busid'] )->first();
           $reviews = $escort->reviews()->get();
           return View::make('dashboard.escort.screens.reviews', compact('escort','reviews'));
        break;
        case '_MpAccount':
              $user = Auth::user();
              return View::make('dashboard.escort.screens.account', compact( 'user' ));
        break;
         case '_MpPayments':
              $user = Auth::user();
              return View::make('dashboard.escort.screens.payments', compact( 'user' ));
        break;
}
    }

  public function profileScreen($escort) {
       return View::make('dashboard.escort.screens.profile', compact('escort','status'));
    }
  public function overviewScreen($escort,$user,$plan) {
               switch ($escort['status']) {
                   case '1':
                      $status = 'Pending';     
                   break;
                   case '2':
                      $status = 'Approved';
                   break;
                   case '3':
                      $status = 'Disapproved';
                  case '4':
                      $status = 'Unapproved';
               break;
               }
             return View::make('dashboard.escort.screens.overview', compact('escort','status','user','plan'));
    }
    public function profileTab($tab) {
        switch ( $tab ) {
          case "View":
               return View::make('dashboard.escort.screens.profileScreens.view', compact('id'));
          break;
          case "New":
                $ages = Age::lists('name','id');
                $user = Auth::user();
                $usr = ( $user->super_user == 1 ? New User : $user );
                return View::make('dashboard.escort.screens.profileScreens.new', compact('id','ages','user'));
          break;
          case "Edit":
                $ages = Age::lists('name','id');
              //  $escort = Escort::where( 'id','=',$id )->first();
                $user = Auth::user();
                $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
                if ( !$plan ){
                  $plan = BusinessPlan::where('id','=', 1 )->first();
                }
                $plans = BusinessPlan::where('type','=','1' )->where('useraccess','=',1)->get();
             //   $main_image = $escort->getMainImage();
             //   $suburb = $escort->getDefaultLocation();
                $heights = Height::lists('name','id');
                $ethnicities = Ethnicity::lists('name','id');
                $eyeColors = EyeColor::lists('name','id');
                $hairColors = HairColor::lists('name','id');
              //  $services = EscortService::where('escort_id','=',$id)->first();
               //    if ( !$services ) $services = new EscortService;
              //  $services = $services->getList($id);
              //  $languages = EscortLanguage::where('escort_id','=',$id)->first();
              //  if ( !$languages ) $languages = new EscortLanguage;
             //   $languages = $languages->getList($id);
                $availabilites = $this->getAvailabilites(0);
                $escort = new Escort;
                $escortRates = $escort->getRates(0);
              //  $inCallList = $this->inCallList($escort->id);
               // $outCallList = $this->outCallList($escort->id);

                $bodies = Body::lists('name','id');  
                return View::make('dashboard.escort.screens.profileScreens.edit', compact('plans','escort','plan','css','main_image', 'escortRates', 'inCallList','outCallList' ,'availabilites','ages', 'suburb','bodies','heights','ethnicities','languages','services','eyeColors','hairColors'));
          break;
          case "Bumpup":
                return 'Bump up';
          break;
        }

      }
     public function escortScreen($id) {
          $user = Auth::user();
          $type = $user->getUserType();
          if ( !$type == 1 && $user->id != $id )
            die();
          $escort = Escort::where('id','=',$id)->first();
          $pass = array(
            'escort' => $escort );
          return View::make('dashboard.screen.escortScreen.layout',$pass );
    }
    public function lowerEscortScreen($id) {
        $user = Auth::user();
        $type = $user->getUserType();
        if ( !$type == 1 && $user->id != $id )
            die();
        $action = Input::get('action');
        $escort = Escort::where('id','=',$id)->first();
          $pass = array(
            'escort' => $escort );
          if ( $action == 'new') $pass['ages'] = $ages = Age::lists('name','id');
          return View::make('dashboard.screen.escortScreen.' . $action ,$pass );

    }
    public function getAvailabilites($id) {
      $escortAvailbities = EscortAvailability::where( 'escort_id','=',$id )->get();
      $sorted = array(
           'escortDays' => array(),
           'twenty_four_hours' => array()
           );
      $daysSelect =  array(
        "1" => "Mon",
        "2" => "Tue",
        "3" => "Wed",
        "4" => "Thu",
        "5" => "Fri",
        "6" => "Sat",
        "7" => "Sun"
        );
      if ( $escortAvailbities->isEmpty() ){
          foreach( $daysSelect as $key => $day ){
            $sorted['days'][$key] = array(
              'handle' => $day,
              'day'    => $key
              );
          }
      }
      else {
          $k = 0;
          foreach ( $escortAvailbities as $key => $av ) {
            $sorted['days'][$av->day] = $av['original'];
            $sorted['days'][$av->day]['handle'] = $daysSelect[$av->day];
              if ( $av['enabled'] == 1 )
                $sorted['escortDays'][$k] = $av->day;
              if ( $av['twenty_four_hours'] == 1 )
                $sorted['twenty_four_hours'][]  = $av->day;
              $k++;
          }
      }
      $sorted['time'] = array(
         "0" =>  "12:00 AM",
         "1" =>  " 1:00 AM",
         "2" =>  " 2:00 AM",
         "3" =>  " 3:00 AM",
         "4" =>  " 4:00 AM",
         "5" =>  " 5:00 AM",
         "6" =>  " 6:00 AM",
         "7" =>  " 7:00 AM",
         "8" =>  " 8:00 AM",
         "9" =>  " 9:00 AM",
         "10" => "10:00 AM",
         "11" => "11:00 AM",
         "12" => "12:00 PM",
         "13" => " 1:00 PM",
         "14" => " 2:00 PM",
         "15" => " 3:00 PM",
         "16" => " 4:00 PM",
         "17" => " 5:00 PM",
         "18" => " 6:00 PM",
         "19" => " 7:00 PM",
         "20" => " 8:00 PM",
         "21" => " 9:00 PM",
         "22" => "10:00 PM",
         "23" => "11:00 PM",
        );
      $sorted['am'] = array(
        "0" => 'am',
        "1" => 'pm');
      
      return $sorted;
    }
    public function outCallList() {
      $ratesList = array(
        'outcall_15_rate' => '15 Min',
        'outcall_30_rate' => '30 Min',
        'outcall_45_rate' => '45 Min',
        'outcall_1h_rate' => '1 hour',
        'outcall_2h_rate' => '2 hour',
        'outcall_3h_rate' => '3 hour',
        'outcall_overnight_rate' => 'Overnight'
         );
      return $ratesList;
    }
      public function inCallList() {
      $ratesList = array(
        'incall_15_rate' => '15 Min',
        'incall_30_rate' => '30 Min',
        'incall_45_rate' => '45 Min',
        'incall_1h_rate' => '1 hour',
        'incall_2h_rate' => '2 hour',
        'incall_3h_rate' => '3 hour',
        'incall_overnight_rate' => 'Overnight'
         );
      return $ratesList;
    }
    public function getRates($id){
       $escort = Escort::where( 'id','=',$id)->first();
       $escortRates = $escort->getRates($escort->id);
       return json_encode ( $escortRates );
    }
      public function getImages($id){
       $escort = Escort::where( 'id','=',$id)->first();
       $images = $escort->images()->where('status','!=',3 )->get();
       $main_image = $escort->main_image_id;
       $banner_image = $escort->banner_image;
   //    return json_encode ( $images );
       return compact( 'images','main_image','banner_image');
    }
    public function chetest() {
     // $s3 = App::make('aws')->createClient('s3');
     $s3 = App::make('aws')->get('s3');
$s3->putObject(array(
    'Bucket'     => 'mpm-uploads',
    'Key'        => str_random(12) . 'png',
    'SourceFile' => '/home/admin/web/remote.myplaymate.com.au/public_html/assets/dashboard/images/uploadBanner.jpg',
));
    }
    public function EscortProfileDetails($id) {
      if ( $id == 0 )
           return '';
       $services = EscortService::where('escort_id','=',$id)->first();
       if ( !$services ) $services = new EscortService;
       $services = $services->getList($id);
       $languages = EscortLanguage::where('escort_id','=',$id)->first();
           if ( !$languages ) $languages = new EscortLanguage;
      $languages = $languages->getList($id);
      $escort =  Escort::find($id);
      $main_image = EscortImage::find($escort->main_image_id);
      $escort->main_image_src =  ( $main_image ? $main_image->filename : '' );
      switch ($escort->status ) {
                   case '1':
                      $escort->escort_status = 'Pending';     
                   break;
                   case '2':
                      $escort->escort_status = 'Approved';
                   break;
                   case '3':
                      $escort->escort_status = 'Disapproved';
                   break;
                   case '4':
                      $escort->escort_status = 'Unavailable';
                   break;
      }
      $availabilites = $this->getAvailabilites($escort->id);
      $escortRates = $escort->getRates($escort->id);
      $inCallList = $this->inCallList($escort->id);
      $outCallList = $this->outCallList($escort->id);
      $user = Auth::user();
      $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
      $tour = EscortTouring::where('escort_id','=',$escort->id)->where('to_date','>=',date('Y-m-d H:i:s'))->get();
      if ( $tour ){
        $blockedDates = array();
          if ( count( $tour ) > 0){
            foreach($tour as $touring){
                  $begin = new DateTime( $touring->from_date );
                  $end = new DateTime( $touring->to_date );
                  $interval = DateInterval::createFromDateString('1 day');
                  $period = new DatePeriod($begin, $interval, $end);
                        foreach ( $period as $dt ){
                              $blockedDates[] = $dt->format( "d-m-Y" );
                        }
                   $touring->from_date = Helpers::db_date_to_user_date($touring->from_date);
                   $touring->to_date = Helpers::db_date_to_user_date($touring->to_date);
            }
        }
      }
       return compact( 'plan','services', 'escort', 'languages', 'availabilites','escortRates','inCallList','outCallList','tour','blockedDates');
   }
   public function getEscortCalls($id) {
      return Escort::find($id);
   }
}
