@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class=deviceWidth style="width: 580px;" align=center bgcolor=#eeeeed border=0 cellpadding=0 cellspacing=0>
    <tbody>
        <tr>
            <td style="padding: 0;" bgcolor=#ffffff valign=top>
                <a href=http://www.myplaymate.com.au/>
                <img class=deviceWidth style="display: block; border-radius: 4px;" src="https://myplaymate.com.au/assets/emails/main-updated-4.png" alt border=0>
                </a>
            </td>
        </tr>
        <tr>
            <td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor=#eeeeed>
                <table bgcolor=#eeeeed>
                    <tbody>
                        <tr>
                            <td style="padding: 0 10px 10px 0;" valign=middle>
                                <a style="text-decoration: none; color: #272727; font-size: 18px; font-weight: bold; font-family: Arial, sans-serif;" href=#>
                                    Hi {{$data['full_name']}},
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p>Thank you for registering with My Playmate! Before you can get started on creating your profile, you will need to verify your email address.</p>
                <p>Once your email address has been verified, you can set up your profile. After your profile is completed, one of our friendly sales consultants will get in touch with you to verify payment and get your profile approved. Then you can play!</p>
                <p>Our development team has worked hard to ensure profile set up is a simple and easy process. However, should you require <strong>any</strong> assistance, you can contact our team on <strong>1300 769 766</strong>.</p>
                <p style="text-align: center; font-size: 20px">Get started by verifying your email <strong><a style="text-decoration: none; color: #b60011; font-weight: bold;" href="{{Config::get('app.url')}}/{{$data['activation_link']}}">HERE.</a></strong></p>
            </td>
        </tr>
    </tbody>
</table> <!-- End One Column -->

@stop