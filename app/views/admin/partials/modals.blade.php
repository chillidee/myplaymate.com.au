@section('modals')
<script type="text/javascript" src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#dyntable').dataTable({
            "sPaginationType": "full_numbers",
            'iDisplayLength': 25,

            "fnDrawCallback": function(oSettings) {
                jQuery.uniform.update();
            },
			fnRowCallback: function(nRow, aData, iDisplayIndex){
				var index = iDisplayIndex +1;
				jQuery('td:eq(0)',nRow).html(index);
				return nRow;
			}
        });

        jQuery('.deleteProfile').live('click', function() {
            var profileId = jQuery(this).data('profile-id');

            var newFormUrl = '{{ url('admin/profiles') }}/'+profileId;
            var newTitle = 'Delete Profile '+profileId+'?';
            jQuery('#deleteProfileForm').attr('action', newFormUrl);
            jQuery('#deleteProfileTitle').text(newTitle);
        });

        jQuery('.disaproveProfile').live('click', function() {
            var profileId = jQuery(this).data('profile-id');

            var newFormUrl = '{{ url('admin/profiles') }}/'+profileId+'/unapprove';
            var newTitle = 'Disapprove / Hold Profile '+profileId+'?';
            jQuery('#disaproveProfileForm').attr('action', newFormUrl);
            jQuery('#disaproveProfileTitle').text(newTitle);
        });


        jQuery('#buttonDisaprove').off('click').on('click',function(){

              jQuery.ajax({
                url: jQuery('#disaproveProfileForm').attr('action'),
                type: "GET",
                data: { message: jQuery('#dissaproveMessage').val()}
              }).done(function() {
                window.location.reload();
              });
        });

    });
function notesDialog(obj){
	var dialogueElement = jQuery(obj).parent().next().dialog({
        modal:true,
        title: 'Wtite a note',
        buttons:{
            'Save':function(){
                    var that = this;
                    jQuery.ajax({
                        url: '{{ url('admin/profiles/notes') }}/' + jQuery(that).attr('id').replace('notes_', ''),
                        method: 'POST',
                        data: {
                                    notes: jQuery(that).find('textarea').val()
                            }
                    }).done(function() {
                        dialogueElement.dialog( "close" );
                    });
                }
            }
    });
}
</script>
<div aria-hidden="false" aria-labelledby="deleteProfileModal" role="dialog" tabindex="-1" class="modal hide fade in" id="deleteProfileModal">
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
        <h3 id="deleteProfileTitle">Delete Profile</h3>
    </div>

    {{ Form::open(array('method' => 'DELETE', /*'route' => array('admin.profiles.destroy', 0),*/ 'id' => 'deleteProfileForm')) }}
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn">Cancel</button>

        <button class="btn btn-primary">Delete</button>
    </div>
    {{ Form::close() }}
</div><!--#myModal-->

<div aria-hidden="false" aria-labelledby="disaproveProfileModal" role="dialog" tabindex="-1" class="modal hide fade in" id="disaproveProfileModal">
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
        <h3 id="disaproveProfileTitle">Unapprove Profile</h3>
    </div>
    <form id="disaproveProfileForm" method="get" action="">
    <p style="margin-left: 8%;margin-top: 15px;font-weight: bold;">Reason</p>
    <textarea id="dissaproveMessage" name="message" style="width:80%;margin-left: 8%;margin-right: 10%;margin-top: 20px;margin-bottom: 20px;height: 200px;"></textarea>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn">Cancel</button>

        <button class="btn btn-primary" id="buttonDisaprove" type="submit">Disapprove</button>
    </div>
    </form>
</div><!--#myModal-->


@stop
