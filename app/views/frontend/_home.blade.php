@extends('frontend.layout')

@section('content')

<!-- Banner Section - Revolution Slider -->
<section class="mpm-wrapper hide-onload">
    <div class="mpm-banner-container visible-md visible-lg">
        <div class="banner-wrapper"><!-- START REVOLUTION SLIDER 4.1.4 fullscreen mode -->
            <div id="rev_slider" class="rev_slider_wrapper fullscreen-container">
                <div id="frontpage-banner" class="rev_slider fullscreenbanner" style="display: none;">
                    <ul><!-- SLIDE 1  -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                            <!-- MAIN IMAGE -->
                            <img id="slider-first" src="assets/frontend/img/bgslider/slider_7.jpg" alt="slider_7" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" /> 

                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption medium_light_white lfl tp-resizeme" data-x="14" data-y="16" data-speed="1100" data-start="1700" data-easing="easeOutBack" data-endspeed="300" style="z-index: 2;">
                                AN UNFORGETTABLE EXPERIENCE
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption small_light_white lfl tp-resizeme" data-x="19" data-y="203" data-speed="1100" data-start="2100" data-easing="easeOutBack" data-endspeed="300" style="z-index: 4; background: rgba(0,0,0,0.6); padding: 15px;">
                                Welcome to My Playmate! Showcasing Australia's escorts, escort agencies and high-class brothels<br />as you've never seen them before. View galleries of adult services providers inviting you to live out your fantasies<br />and explore your desires. Easy to search &amp; find the sexiest escorts &hellip;.
                            </div>
                            
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lfl tp-resizeme" data-x="20" data-y="303" data-speed="1300" data-start="2300" data-easing="easeOutBack" data-endspeed="300" style="z-index: 5;">
                                <a href="/escorts" class="button small-button">Discover More</a>
                            </div>
                        </li>
                        
                        <!-- SLIDE 2 -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="500">
                            <!-- MAIN IMAGE --> 
                            <img src="assets/frontend/img/bgslider/slider_8.jpg" alt="slider_8" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" /> 

                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption small_light_white lfl tp-resizeme" data-x="19" data-y="203" data-speed="1100" data-start="2100" data-easing="easeOutBack" data-endspeed="300" style="z-index: 3; background: rgba(0,0,0,0.6); padding: 15px;">
                                Is there a specific indulgence you're looking for? Whether it's a Swedish blonde escort in Sydney,<br /> an educated geek goddess in Melbourne, or an exotic Asian beauty in Brisbane,<br />My Playmate showcases over 700 escorts Australia-wide. Browse at your leisure &hellip;.
                            </div>
                            
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption lfl tp-resizeme" data-x="20" data-y="303" data-speed="1300" data-start="2300" data-easing="easeOutBack" data-endspeed="300" style="z-index: 4;">
                                <a href="/escorts" class="button small-button">Discover More</a>
                            </div>
                        </li>
                        
                        <!-- SLIDE 3  -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="500">
                            <!-- MAIN IMAGE -->
                            <img src="assets/frontend/img/bgslider/slider_9.jpg" alt="slider_9" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" /> 

                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption small_light_white lfl tp-resizeme" data-x="19" data-y="203" data-speed="1100" data-start="2100" data-easing="easeOutBack" data-endspeed="300" style="z-index: 3; background: rgba(0,0,0,0.6); padding: 15px;">
                                Every week thousands of visitors come to My Playmate to explore their erotic imagination,<br />sexual curiosity or need for absolute satisfaction.Escape the humdrum of everyday life<br />as our parade of eye candy enables you to exact-match your thirst for pleasure. Visit whenever you wish ...
                            </div>
                            
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption lfl tp-resizeme" data-x="20" data-y="303" data-speed="1300" data-start="2300" data-easing="easeOutBack" data-endspeed="300" style="z-index: 4;">
                                <a href="/escorts" class="button small-button">Discover More</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div><!-- End Of Rev-Slider -->
        </div><!-- Banner Wrapper -->
    </div>
</section><!-- .mpm-wrapper end -->

<!-- Page Wrapper -->
<div class="page-wrapper top-space">

    <!-- Featured escorts Section -->
    <div id="featured-escorts">
        <!-- .container start -->
        @include('frontend.partials.home.spotlight-escorts')

        <div id="slogan" class="container" style="text-align:center">
            <div class="divider"></div>
            <h5>AN UNFORGETTABLE EXPERIENCE</h5>
            <h2>WHAT EVER YOU CAN</h2>
            <h3>DESIRE & IMAGINE</h3>
        </div>
    </div><!-- Featured Escorts -->


    <!-- About Section -->
    <div class="page-content" id="frontpage-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-centered triggerAnimation animated" data-animate="pulse"></div>
                </div>
            </div>
            <!-- .row end -->
            <div class="row">
                {{ StaticBlock::getHtml('call-to-action') }}
            </div>

            <!-- .row end --></div>
        <!-- .container end --></div>
    <!-- .page-content end -->

    @include('frontend.partials.home.sexy-updates')

    <!-- Escort Information -->
    <div class="page-content" id="escorts-info">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {{ StaticBlock::getHtml('homepage-about') }}
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- end of Escort Information -->


</div> <!-- End of Page wrapper -->

@stop


@section ('javascript')

<script>

    (function($){
        $('.mpm-wrapper').removeClass('hide-onload');

        $('body').on('click', '#about-readmore', function(event) {
            var $this = $(this);
            console.log($this.innerHeight());
            if ($this.hasClass('closed')) {
                if($(window).width() < 400)
                    $('#about-content').animate({height: "700px"}, 500);
                else
                    $('#about-content').animate({height: "450px"}, 500);

                $this.removeClass('closed').addClass('opend');
            } else {
                $this.removeClass('opend').addClass('closed');
                $('#about-content').animate({
                    height: "85px"
                }, 500);
            }
        });   
    })(jQuery);
    

</script>

@stop



















