/*
 * 	Additional function for wysiwyg.html
 *	Written by ThemePixels	
 *	http://themepixels.com/
 *
 *	Built for Shamcey Metro Admin Template
 *  http://themeforest.net/category/site-templates/admin-templates
 */

jQuery().ready(function() {
		jQuery('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/assets/admin/js/tinymce/tinymce.min.js',

			// General options
			theme : "modern",
			width: "100%",
			plugins: [
				 "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				 "save table contextmenu directionality emoticons template paste textcolor moxiemanager"
		    ],
		    extended_valid_elements : 'script[charset|defer|language|src|type]',
            convert_urls: false,
            remove_script_host: false,
            relative_urls: false,
			verify_html : false,
			toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | jbimages' ,

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			},
			cleanup:false,
		});
				
		
		jQuery('.editornav a').click(function(){
			jQuery('.editornav li.current').removeClass('current');
			jQuery(this).parent().addClass('current');
			if(jQuery(this).hasClass('visual'))
				jQuery('#elm1').tinymce().show();
			else
				jQuery('#elm1').tinymce().hide();
			return false;
		});
});
