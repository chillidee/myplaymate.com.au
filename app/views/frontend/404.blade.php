@extends('frontend.layout')

@section('content')

<!-- .boxed version of the page -->
<div class="page-wrapper extra-top">
    <!-- .container start -->
    <div class="container">
        <!-- .row start -->
        <div class="row">
			<h2>Page not found</h2>
			<p>Please recheck the address you inserted, or <a href="/">return to home page.</a></p>
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- End of the Boxed Version Page -->
			
@stop