<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escort_reviews', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('escort_id');
			$table->string('name');
			$table->text('review');
			$table->integer('rating');
			$table->integer('status');
			$table->integer('published');
			$table->integer('approved_by');
			$table->timestamp('approved_at');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escort_reviews');
	}

}
