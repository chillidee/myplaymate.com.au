@extends('frontend.layout')
@section('content')
<div id ='left_pull_filters' class ='no_close'>
  <div id ='close_left_pull'></div>
  <h4>Search</h4>
  <form id ='custom_search_escorts'>
    <input id="escort_side-search" name="s" type="text" placeholder="Type and hit enter..." autocomplete="off"/>
  </form>
</div>
<!-- #page-title start -->
<section id="page-title" class="page-title-1">
  <div class="container">
    @if(isset($status))
    <h1 class ='_smaller'>This Profile is currently unavailable.  Here are some other {{$type}}s you might like</h1>
    @else
    <h1>@if(isset($meta) && isset( $meta->name )){{$meta->name}} @else Businesses @endif</h1>
    @endif
    <div id ='filter_information' class ='no-close'></div>
  </div><!-- .containr end -->
</section><!-- #page-title end -->

<!-- .boxed version of the page -->
<section class="page-wrapper section-content business-index-page">
  <!-- .container start -->
  <div class="container">

    <!-- .row start -->
    <div class="row">
      @if(isset($meta->top_content))
      <section class="top-content">
        {{ nl2br($meta->top_content) }}
      </section>
      @endif
      <div id="directory-container" class="col-xs-12">
        <div id="directory">
          <ul class="escort-container grid-view">
            <li class="grid-template col-xs-12 col-ms-6 col-md-4 col-lg-3 col-xl-15 col-xl-3 escorts-wishlist">
              <figure class="escorts-img-container">
                <div class="figfavorite">
                  <a href="" class="is-not-wish">
                    <i class="fa fa-heart"></i>
                  </a>

                  <a href="" class="is-wish">
                    <i class="fa fa-times"></i>
                  </a>
                </div>

                <div class="info-container">
                  <div class="figinfo">
                    <span class="figname"></span>
                    <ul class="figdata">
                      <li class="escort-location">
                        <span class = 'business-suburb'></span>
                        @if(isset($city))
                        <span class="escort-distance"> km from {{ ucfirst($city) }}</span></li>
                        @else
                        <span class="escort-distance"> km</span></li>
                        @endif
                      </li></ul>
                    </div><!-- figinfo-->
                  </div><!-- end of info-container-->
                  <a href="{{ url('escorts/') }}">
                    <img class="img-profile" src="" alt="">
                  </a>
                </figure>
              </li>

            </ul>
            <div class="spinner">
               <div class="dot1"></div>
               <div class="dot2"></div>
            </div>
          </div>
        </div>
      </div><!-- .row end -->
    </div><!-- .container end -->
  </section> <!-- End of the Boxed Version Page -->

  @stop

  @section('javascript')

  <script>
    window.type = '{{$type}}';
    window.mapPage = 0;
    window.getGirls = 0;

    jQuery(document).ready(function($) {

      function MyloadEscorts(data,page){
        jQuery('.spinner').fadeOut(200);
        // If there are no more escorts to load, return
        check = jQuery.parseJSON(data);
        data = check['show'];
        if (data.length === 0 && page > 0){
          return;
        }
        var view = Cookies.get('view') || 'grid',
        page = page || 0,
        directory = jQuery('#directory');

    // Wipe eventual empty query message
    directory.find('h3').remove();

    // If there is no data, wipe and show empty query message and return
    if (data.length == 0) {
      wipeEscorts();

      directory.append('<h3>No Business found with these criteria. Try again!</h3>');
      return;
    };

    jQuery.each(data, function(index, escort) {
      var template = jQuery('.'+view+'-template');
        // For each escort loaded, create a clone of the template
        var clone = template.clone();
        // Remove escort-template class to prevent infinite loop
        // Add clone class to enable infinite scroll
        clone.removeClass(view+'-template').addClass('clone');

        // Add data-distance to each container
        clone.attr('data-distance', escort.distance);

        // Populate the clone
        var link = clone.children('.escorts-img-container').children('a'),
        readmore = clone.find('.fig-readmore a'),
        readmoreHref = readmore.attr('href');

        readmore.attr('href',readmoreHref+'/'+escort.seo_url);
        jQuery.each(link, function(index, val) {
          var $this = jQuery(this),
          href = $this.attr('href');
          $this.attr('href', href+'/'+escort.seo_url);
        });
        clone.find('.escorts-img-container').attr('id', 'escort-'+escort.id);
        clone.find('.figname').append(escort.name);
        clone.find('.escort-location').prepend(escort.escortLocation);
        clone.find('.escort-age').prepend(escort.escortAge);
        clone.find('.escort-distance').prepend('~'+escort.distance);
        clone.find('.escort-description').prepend(escort.about_me);
        if (escort.mainImageUrl)
          clone.find('.img-profile').attr('src', '/uploads/escorts/thumbnails/thumb_220x330_'+escort.mainImageUrl);
        else
          clone.find('.img-profile').attr('src', '/assets/frontend/img/profile_template.jpg');


        if (escort.isWishlist == true) {
          clone.find('.is-not-wish').hide();
          clone.find('.is-wish').show();
        };

        if (escort.featured == 0)
          clone.find('.ribbon-featured').hide();

        // Add the clone to the list
        var container = jQuery('.escort-container');
        container.append(clone);

        if (jQuery('#sort-by select').val() === 'my-location') {
          container.children('li').sort(function(a,b){
            return +a.getAttribute('data-distance') - +b.getAttribute('data-distance');
          }).appendTo(container);
        }

        window.applied = 0;
      });
    // In the end, hide only the empty template
    jQuery('.escorts-wishlist').show();
    jQuery('.grid-template').first().hide();
    jQuery('.list-template').first().hide();
  }
  var listToggle = $('#list-view-toggle'),
  gridToggle = $('#grid-view-toggle'),
  container = $('.escort-container'),
  sortSelect = $('#sort-by select');

    // If the page was called by brothels, agencies or massage controller, add the business type to the body for the filters
    @if(isset($business))
    $('body').attr('data-business', {{ $business }});
    @endif

    // If the page was called by a city, add the suburb id to the body for the filters
    @if(isset($suburb_id))
     //$('body').attr('data-suburb-id', '{{ $suburb_id }}');
     var pageSuburb = '{{ $suburb_id }}';
     @endif

    // If the page was called as "my wishlist", add the wishlist attribute to the body for the filters
    @if(isset($my_wishlist))
    $('body').attr('data-wishlist', 1);
    @endif

    if (typeof check == 'undefined') {
     applyFilters();
   }
   else {
     load_saved_escorts(check.show);
   }

   if (Cookies.get('view') == 'list') {
    jQuery('.escort-container').removeClass('grid-view').addClass('list-view');
    listToggle.addClass('active');
    gridToggle.removeClass('active');
  } else {
    jQuery('.escort-container').removeClass('list-view').addClass('grid-view');
    gridToggle.addClass('active');
    listToggle.removeClass('active');
  }


    // Events
    listToggle.on('click', function(e) {
      e.preventDefault();

      listToggle.addClass('active');
      gridToggle.removeClass('active');
      Cookies.set('view','list');
      applyFilters();
    });

    gridToggle.on('click', function(e) {
      e.preventDefault();

      gridToggle.addClass('active');
      listToggle.removeClass('active');
      Cookies.set('view','grid');
      applyFilters();
    });

    var toggleFilters = $('#toggleFilters');
    if($(window).width() < 768){
      toggleFilters.show();
    }
    toggleFilters.on('click', function(e) {
      e.preventDefault();
      $('.filters').fadeToggle(300);
    });
  });

</script>
<style>
#myToggleFilters { display:none; }
#trigger-overlay-mobile { display:block; }
</style>
@stop
