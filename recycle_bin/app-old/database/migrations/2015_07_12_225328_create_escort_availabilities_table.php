<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortAvailabilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escort_availabilities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('escort_id');
			$table->integer('day');
			$table->boolean('enabled');
			$table->boolean('twenty_four_hours');
            $table->integer('start_hour_id');
            $table->integer('end_hour_id');
            $table->boolean('start_am_pm');
            $table->boolean('end_am_pm');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escort_availabilities');
	}

}
