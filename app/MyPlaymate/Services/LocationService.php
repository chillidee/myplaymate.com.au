<?php
namespace MyPlaymate\Services;

use EscortTouring, Suburb;
use MyPlaymate\Repositories\SuburbRepository;

class LocationService {

	private $latitude;
	
	private $longitude;

	protected $repo;

	protected $currentSuburb;

	protected $touring;

	function __construct(SuburbRepository $repo) {
		$this->repo = $repo;

		$this->currentSuburb = $this->getCurrentSuburb();
	}

	public function setTouring($escort)
	{
	 	$this->touring = EscortTouring::whereEscortId($escort->id)
			->where('from_date','<=',date('Y-m-d'))
			->where('to_date','>=',date('Y-m-d'))
			->first();
	}

	public function setCoordinates($suburb_id)
	{
		$suburb = $this->repo->find($suburb_id);
		$this->latitude = $suburb['lat'];
		$this->longitude = $suburb['lng'];
		return [$this->latitude, $this->longitude];
	}

	public function getCurrentLocationName($escort, $suburb_id)
	{
		$this->setTouring($escort);

		if (count($this->touring))
		{
			return Suburb::find($this->touring->suburb_id)->name;
		}

		return Suburb::find($escort->suburb_id)->name;
	}

	public function getCurrentDistance($escort, $suburb_id)
	{
		/*$this->setTouring($escort);

		if (count($this->touring))
		{
			return $this->calculateDistance($this->touring->suburb_id,$suburb_id);
		}

		return $this->calculateDistance($escort->suburb_id,$suburb_id);*/
	}

	public function calculateDistance($lat1, $lng1, $lat2, $lng2 )
	{

		$pi80 = M_PI / 180;
		$lat1 *= $pi80;
		$lng1 *= $pi80;
		$lat2 *= $pi80;
		$lng2 *= $pi80;
	 
		$r = 6372.797; // mean radius of Earth in km
		$dlat = $lat2 - $lat1;
		$dlng = $lng2 - $lng1;
		$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$km = $r * $c;
	 
		return (int)number_format($km,0,'.','');
	}

	public function getNearest($escorts,$maxDistance)
	{
		foreach ($escorts as $key => $escort)
		{
			$escort->distance = $this->getCurrentDistance($escort,$this->currentSuburb);
			if($escort->distance > $maxDistance)
			{
				$escorts->forget($key);
			}
		}
		return $escorts;
	}

	public function getCurrentSuburb()
	{
		if (isset($_COOKIE['suburb']))
		{
			return $_COOKIE['suburb'];
		}
		return 10906;
	}

}







