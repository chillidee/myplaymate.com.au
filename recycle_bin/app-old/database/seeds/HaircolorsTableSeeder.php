<?php

class HairColorsTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('hair_colors')->truncate();

        $HairColors = [
            ['name' => 'Blonde', 'order' => 20],
            ['name' => 'Brown', 'order' => 30],
            ['name' => 'Red Head', 'order' => 50],
            ['name' => 'Black', 'order' => 10],
            ['name' => 'Bald', 'order' => 60],
            ['name' => 'Other', 'order' => 70],
            ['name' => 'Dark Brown', 'order' => 40],
            ];

        // Uncomment the below to run the seeder
        DB::table('hair_colors')->insert($HairColors);
    }
}
