/**
 * ---------- ANGULAR CROPPER MODULE ----------
 * This file only takes care of creating the Angular Cropper module as well
 * as injecting any dependencies required by the module.
 */

var _gCropperModule = angular.module('bp.img.cropper', []);