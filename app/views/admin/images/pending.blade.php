@extends('admin.layout')

@section('content')
<div class ='overlay loadingDiv'></div>
<div class ='loader loadingDiv'></div>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-picture-o"></span></div>
    <div class="pagetitle">
        <h1>Pending images</h1>
    </div>
</div><!--pageheader-->
<?php $last_escort = 0; ?>
	<div class="pending-images-container alignc section-content col-md-12">
	@foreach($imgs as $img)
	  <?php $escort = Escort::find($img->escort_id); ?>
	    @if ( $escort )
		@if($img->escort_id != $last_escort && $last_escort > 0)
			</div>
			<div class="pending-images-container alignc section-content col-md-12">
		@endif
		@if($img->escort_id != $last_escort)
		 <h3>{{ Escort::find($img->escort_id)->escort_name }}
		@if ( $escort->under_business )
		    <?php $business = Business::find( $escort->business_id ); ?>
		    @if( $business )
		       -{{ $business->name}}
		    @endif
		 @endif
		  </h3>
		<?php //	<h3>{{ Escort::find($img->escort_id)->escort_name }}</h3>?>
		@endif
			<div class="pending-image col-md-3" data-escort-id="{{ $img->escort_id }}" data-image-id="{{ $img->id }}">
				<img src="{{ $img->thumbnail(170,170) }}" alt="">
				<div class="alignc section-content">
					<a class="approve btn btn-success" data-type="image">Approve</a>
					<a class="disapprove btn btn-danger" data-type="image">Disapprove</a>
					<div class="disapprove-message">
						<textarea placeholder="Insert reason for disapproving."></textarea>
					</div>
				</div>
			</div>
		@if($img->escort_id != $last_escort)
			<a href="" class="btn btn-primary send-report">Save and send report</a>
		@endif
		<?php $last_escort = $img->escort_id; ?>
		@else
						@endif
	@endforeach
	</div>
	<h2>Pending Banners</h2>
	@foreach ( $banners as $banner )
	       <div class="pending-image col-md-3 fixed_height" data-active-id="{{ $banner->id }}" data-type ="{{$banner->type}}" id ='holder-{{$banner->id}}-{{$banner->type}}'>
	          <h4 class ='center'>{{ $banner->name }}</h4>
	          <h4 class ='center'>{{$banner->type}} id : {{ $banner->id }}</h5>
				<img src="{{ $banner->src }}" alt="" class ='full_width'>
				<div class="alignc section-content">
					<a class="approve btn btn-success" data-type="banner">Approve</a>
					<a class="disapprove btn btn-danger" data-type="banner">Disapprove</a>
					<div class="disapprove-message">
						<textarea id ='{{$banner->type}}-{{$banner->id}}' placeholder="Insert reason for disapproving."></textarea>
					</div>
				</div>
				<a href="" class="btn btn-primary send-report_banner" data-name = "{{$banner->name}}" data-id ="{{$banner->id}}" data-type ="{{$banner->type}}" data-action ="2">Save and send report</a>
			</div>
	@endforeach
	</div>


	<img id="big-image">
	<div class="overlay"></div>

<style>.full_width { width: 100%;}
       .center { text-align:center; }
       .fixed_height { max-height: 359px; }
       .fixed_height .section-content { margin-bottom: 21px;}
       .fixed_height .send-report_banner { position:relative;display:block;float:none;margin:0 auto; right: 0;}
       .overlay { position:fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: #000; opacity: .3; }
       .loader {
  font-size: 10px;
  margin: 50px auto;
  z-index:1001;
  text-indent: -9999em;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background: #ffffff;
  background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  position: relative;
  -webkit-animation: load3 1.4s infinite linear;
  animation: load3 1.4s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
   display:none;
  position:fixed;
  left:50%;
  margin-left: 50px;
}
.loader:before {
  width: 50%;
  height: 50%;
  background: #ffffff;
  border-radius: 100% 0 0 0;
  position: absolute;
  top: 0;
  left: 0;
  content: '';
}
.loader:after {
  width: 75%;
  height: 75%;
  border-radius: 50%;
  content: '';
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
@-webkit-keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
 </style>

<script>
	function setStatus(picture, url, disapproveMessage)
	{
		var imageId = picture.data('image-id'),
			disapprove_message = disapproveMessage || '';

		jQuery.ajax({
			url: '/admin/'+url,
			data: {
				image_id: imageId,
				disapprove_message : disapprove_message
			},
		})
		.done(function() {
			console.log('image status updated');
		});
	}

	(function($){
		var halfW, halfH;

		$('.rightpanel').on('click','.pending-image img',function(){
			var imageId = $(this).closest('.pending-image').attr('data-image-id'),
				bigImage = $('#big-image');
			bigImage.attr('src','');
			$.ajax({
				url: '/admin/show-fullsize-image',
				data: {image_id: imageId},
			})
			.done(function(filename) {
				
				$('.overlay').fadeIn(300);
				bigImage.attr('src','https://d2enq9fr2fmuvm.cloudfront.net/escorts/'+filename);

				bigImage.fadeIn(300);
			});
		});

		$('#big-image').on('load', function() {
			halfW = $('#big-image').width() / 2;
			halfH = $('#big-image').height() / 2;
			console.log(halfW);
			console.log(halfH);
			$(this).css({
				marginLeft: - halfW,
				marginTop: - halfH
			});
		});

		$('.overlay').on('click', function() {
			$(this).fadeOut(300);
			$('#big-image').fadeOut(300);
		});

		$('.rightpanel').on('click', '.approve', function(e) {
			e.preventDefault();
			var $this = $(this);
			if ( $( this ).data('type') == 'banner' ){
				 $( this ).parent( '.alignc' ).children('.send-report_banner' ).data('action','2' );
			}
			$this.closest('.pending-image').removeClass('bg-danger').addClass('bg-success');
			$this.siblings('.disapprove-message').fadeOut(200);
		});	

		$('.rightpanel').on('click', '.disapprove', function(e) {
			e.preventDefault();
			var $this = $(this);
			if ( $( this ).data('type') == 'banner' ){
				 $( this ).parent( '.alignc' ).next().data('action','3' );
			}

			$this.closest('.pending-image').removeClass('bg-success').addClass('bg-danger');
			$this.siblings('.disapprove-message').fadeIn(200);
		});

		$('.rightpanel').on('click', '.send-report', function(e) {
			e.preventDefault();
			var $this = $(this),
				pendingImages = $this.siblings('.pending-image'),
				approvedImages = [],
				disapprovedImages = [],
				escortId;
				count = 0;
			pendingImages.each(function(index, el) {
				count++;
				var	element = $(el),
					imageId = element.attr('data-image-id');
				escortId = element.attr('data-escort-id');

				if (element.hasClass('bg-success')){
					var url = 'approve_image';
					setStatus(element,url);
					image = [imageId];
					approvedImages.push(image);
				}
				if (element.hasClass('bg-danger')){
					var url = 'disapprove_image',
						disapproveMessage = element.find('textarea').val();
					setStatus(element,url,disapproveMessage);
					image = [imageId,disapproveMessage];
					disapprovedImages.push(image);
				}
			});
			$( '.loadingDiv').show();

			$.ajax({
				url: '/admin/send-images-report',
				data: {
					approved_images: approvedImages,
					disapproved_images: disapprovedImages,
					escort_id: escortId,
				},
			})
			.done(function() {
				pendingImages.each(function(index, el) {
					var element = $(el);
					if (element.hasClass('bg-danger') || element.hasClass('bg-success'))
						element.remove();
				});
				if ($this.siblings('.pending-image').length == 0)
					$this.closest('.pending-images-container').remove();
				$( '#pendingImagesBubble').html( parseInt($( '#pendingImagesBubble').html() ) - count );

				$('body').animate({scrollTop: 0}, 200);
				jQuery('.alert-success').text('Report sent!').fadeIn();
				$( '.loadingDiv').hide();
			});
		});
$('.rightpanel').on('click', '.send-report_banner', function(e) {
			e.preventDefault();
            data = $(this).data();
            data.reason = $( '#' + data.type + '-' + data.id ).val();
            $( '.loadingDiv').show();

			$.ajax({
				url: '/admin/send-banner-report',
				data: data
			})
			.done(function(response) {
			$( '#holder-' + data.id + '-' + data.type ).hide();
				$( '.loadingDiv').hide();
			});
		});
	})(jQuery);
</script>

@stop



