@extends('admin.layout')

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Edit escort reviews and sexy updates - {{ $escort->escort_name }}</h1>
    </div>
</div><!--pageheader-->

@include('admin.partials.escorts-breadcrumb')

<div class="row">
{{--     <div class="col-md-6">
	    <h2 class="alignc">My sexy updates</h2>
        <div class="alert alert-success" id="update-saved">Sexy update saved!</div>
        <div class="alert alert-success" id="update-deleted">Sexy update deleted!</div>

        <div class="row" style="margin-bottom: 20px">
            <div class="col-lg-12">
                <a class="add-update-button btn btn-primary pull-right">ADD UPDATE</a>
            </div>
        </div>
        <table class="table table-striped updates-table" style="text-align:left;">
            <tbody>
                <tr>
                    <td>Message</td>
                    <td>Published</td>
                    <td>Action</td>
                </tr>
                <tr class="update-clone">
                    <td class="show-update"></td>
                    <td class="edit-update">
                        <textarea style="margin:0; width:100%"></textarea>
                    </td>
                    <td>{{ date("d/m/Y") }}</td>
                    <td>
                        <button type="button" class="pull-right btn btn-danger delete-update-button">Delete</button>
                        <button type="button" class="pull-right btn btn-primary edit-update-button">Edit</button>
                        <button type="button" class="pull-right btn btn-primary save-update-button">Save</button>
                    </td>
                </tr>
                @foreach(EscortUpdate::whereEscortId($escort->id)->get() as $update)
                <tr id="{{ $update->id }}">
                    <td class="show-update">{{ $update->message }}</td>
                    <td class="edit-update">
                        <textarea style="margin:0; width:100%">{{ $update->message }}</textarea>
                    </td>
                    <td>{{ Helpers::db_date_to_user_date($update->created_at) }}</td>
                    <td>
                        <button type="button" class="pull-right btn btn-danger delete-update-button">Delete</button>
                        <button type="button" class="pull-right btn btn-primary edit-update-button">Edit</button>
                        <button type="button" class="pull-right btn btn-primary save-update-button">Save</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- .col-md-6 end --> --}}

    <div class="col-md-6">
    	<h2>Reviews</h2>
        <table class="table table-striped reviews-table" style="text-align:left;">
            <tbody>
                <tr>
                    <td>Date</td>
                    <td>Rating</td>
                    <td style="max-width:300px">Review</td>
                    <td>Published</td>
                    <td>Action</td>
                </tr>
                @foreach(EscortReview::whereEscortId($escort->id)->whereStatus(2)->get() as $review)
                    <tr id="{{ $review->id }}">
                        <td>{{ Helpers::db_date_to_user_date($review->created_at) }}</td>
                        <td>{{ $review->rating }}</td>
                        <td style="max-width:300px">{{ $review->review }}</td>
                        <td class="published">{{ $review->published ? 'yes' : 'no' }}</td>
                        <td>
                            <button class="btn publish-review btn-success">Publish</button>
                            <button class="btn unpublish-review btn-warning">Unpublish</button>
                            <button class="btn delete-review btn-danger">Delete</button>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
	</div>
</div>

<script>
	(function($){
        $('body').on('click', '.edit-update-button', function(e) {
            e.preventDefault();
            var $this = $(this),
                container = $this.parents('tr'),
                showUpdate = container.find('.show-update'),
                editUpdate = container.find('.edit-update'),
                currentMessage = showUpdate.text();
            showUpdate.hide();
            editUpdate.find('textarea').val(currentMessage);
            editUpdate.show();
            $this.hide();
            $this.siblings('.save-update-button').show();
        });

        $('body').on('click', '.save-update-button', function(e) {
            e.preventDefault();
            var $this = $(this),
                container = $this.parents('tr'),
                showUpdate = container.find('.show-update'),
                editUpdate = container.find('.edit-update'),
                currentMessage = editUpdate.find('textarea').val(),
                updateId = parseInt(container[0].id) || 0;

            $.ajax({
                url: '/save-update',
                data: {
                    escort_id: {{ $escort->id }},
                    update_id: updateId,
                    message: currentMessage,
                }
            })
            .done(function(update_id) {
                $('.alert-success').hide();
                $('#update-saved').show();
                container[0].id = update_id;
                editUpdate.hide();
                container.find('.edit-update-button').show();
                container.find('.delete-update-button').show();
                $this.hide();
                showUpdate.text(currentMessage).show();
            });
        });

        $('body').on('click', '.delete-update-button', function(e) {
            var $this = $(this),
                container = $this.parents('tr'),
                updateId = parseInt(container[0].id);

            $.ajax({
                url: '/delete-update',
                data: { update_id: updateId }
            })
            .done(function() {
                $('.alert-success').hide();
                $('#update-deleted').show();
                container.remove();
            });
        });

        $('.add-update-button').on('click', function(e) {
            var clone = $('.update-clone').clone();
            clone.find('.show-update').hide();
            clone.find('.edit-update').show();
            clone.find('.edit-update-button').hide();
            clone.find('.save-update-button').show();
            clone.find('.delete-update-button').hide();
            clone.removeClass('update-clone').appendTo('.updates-table tbody');
            console.log(clone);
        });

        $('.reviews-table tr').each(function(index, el) {
            var $this = $(this);
            if ($this.find('.published').text() == 'yes'){
                $this.find('.publish-review').hide();
                $this.find('.unpublish-review').show();
            } else {
                $this.find('.unpublish-review').hide();
                $this.find('.publish-review').show();
            }
        });

        $('.publish-review').on('click', function(e) {
            e.preventDefault();
            var container = $(this).parents('tr'),
                reviewId = container[0].id;

            $.ajax({
                url: '/publishReview',
                data: {review_id: reviewId},
            })
            .done(function() {
                container.find('.published').text('yes');
                container.find('.publish-review').hide();
                container.find('.unpublish-review').show();
            });
        });

        $('.unpublish-review').on('click', function(e) {
            e.preventDefault();
            var container = $(this).parents('tr'),
                reviewId = container[0].id;

            $.ajax({
                url: '/unpublishReview',
                data: {review_id: reviewId},
            })
            .done(function() {
                container.find('.published').text('no');
                container.find('.publish-review').show();
                container.find('.unpublish-review').hide();
            });
        });

        $('.delete-review').on('click', function(e) {
            e.preventDefault();
            var container = $(this).parents('tr'),
                reviewId = container[0].id;

            $.ajax({
                url: '/deleteReview',
                data: {review_id: reviewId},
            })
            .done(function() {
                container.remove();
            });
        });	
	})(jQuery);
</script>


@stop