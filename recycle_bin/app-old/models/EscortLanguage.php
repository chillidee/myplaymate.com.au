<?php

/**
 * Class EscortImage
 */
class EscortLanguage extends Eloquent
{
    protected $table = 'escort_language';

    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [];

    /**
     *
     */
    public function escort()
    {
        $this->belongsTo('Escort');
    }
}
