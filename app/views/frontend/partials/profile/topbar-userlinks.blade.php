<ul id ='profile_userbox' class="user-box">
    @if(Auth::check())
        <li class="profile_welcome">
            Welcome, {{ Auth::user()->full_name }}
        </li>
        <a href="/user/profile">
        <li class="profile_dropdown">{{ Auth::user()->full_name }}
    @if(isset($escort_image) )
              <div id ='profile_image_cutter'>
                  <img class ='small_profile_image' src ='/uploads/escorts/{{ $escort_image }}'>
              </div>
    @endif
           <ul class ='profile_submenu'>
                        <li><a href="{{ route('getChangePassword') }}">Change password</a></li>
                        @if(isset(Auth::user()->escort()->id))
                            <li><a href="{{ url('escorts/preview/'.Auth::user()->escort()->id) }}">Profile Preview</a></li>
                        @endif
                        @if ( !isset(Auth::user()->escort()->id))
                          <li><a href="/wishlist">Your Favourites</a></li>
                        @endif
                        <li><a href="{{ route('getContact') }}">Contact us</a></li>
                        <li><a href="{{ route('logout') }}">Sign Out</a></li>
                    </ul>
        </li>  
            </a>
    @else
        <li class="login-li hidden-lg">
            <a href="#login" data-toggle="modal">Login</a>
        </li>
        <li class="register-li hidden-lg">
            <a href="#signup" data-toggle="modal">Sign Up</a>
        </li>
    @endif
</ul>