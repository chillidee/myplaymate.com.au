(function($){
    var welcome = Cookies.get('welcome');
    if(!welcome)
    {
        $('#welcomeModal').modal('show');
    }

    $('#login').on('click', '#forgot-password', function(event) {
        event.preventDefault();
        var $this = $(this),
            form = $('#login-form');

        $('#login-password').slideUp(200);
        form.attr('action','users/forgot-password');
        $('#login-submit').val('SEND ME AN EMAIL');
        $this[0].id = 'remembered-password';
        $this.text('Now I got it!');
    });

    $('#login').on('click', '#remembered-password', function(event) {
        event.preventDefault();
        var $this = $(this),
            form = $('#login-form');
        console.log('ricordato');
        $('#login-password').slideDown(200);
        form.attr('action','login');
        $('#login-submit').val('SIGN IN');
        $this[0].id = 'forgot-password';
        $this.text('Forgot your password');
    });   
})(jQuery);

    