<?php
namespace Admin;

use Redirect, Controller, View, Datatables, Input, Auth, Helper, Mail, Config, BaseController, Carbon\Carbon;
use Escort, Suburb, ImageHelper, User, Age, Height, Body, EscortService, EscortLanguage, Service, Language, EscortAvailability, EscortTouring, EscortRate, EscortImage, EscortBusiness, EscortUpdate, EscortReview, EmailTemplate, Helpers, Visit, PhoneCall, Playmail;
use MyPlaymate\Repositories\UserRepository;
use MyPlaymate\Repositories\SuburbRepository;
use MyPlaymate\Repositories\BusinessRepository;
use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\Model;
 
 
 set_time_limit(0);
  

class ExportController extends Controller {
  public function index()
  {
    return View::make('admin.export.index');
  }
  public function exportAjax() {
      $input = Input::all();
    if ( $input['export_type'] == 'spreadsheet' ) 
             return View::make('admin.export.csv',array( 'status'=>$input['status']) );
    return View::make('admin.export.mobile', array( 'status'=>$input['status']));
    
   }
    
 }