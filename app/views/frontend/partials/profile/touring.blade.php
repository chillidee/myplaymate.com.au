 <div id="backend-profile-touring" class="alignc">
                    <h2>Touring</h2>
                    <p class="danger">If you’re planning on venturing to a new city to see clients, post it here!<br>
                    This way any new clients can see that you’re in the area and can contact you to see you.</p>
                    <p class="danger">For example: if you are in Sydney but are in Brisbane in 2 weeks time, you can drop down the start and end date to fit when you’re there. That means, anyone that searches Brisbane as their location will find you in Brisbane instead of Sydney for that time period.</p>
                    <div class="alert alert-success" id="touring-delete-success">Touring deleted!</div>
                    <div class="alert alert-success" id="touring-add-success">Touring created!</div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="triggerAnimation animated fadeInUp" data-animate="fadeInUp" style="">
                                <table class="table table-striped" style="text-align:left;" id="touringTable">
                                    <tbody>
                                        <tr>
                                            <td>Date From</td>
                                            <td>Date To</td>
                                            <td>Location</td>
                                            <td>Action</td>
                                        </tr>
                                        <tr class="touringEntry touringEntryTemplate">
                                            <td class="from_date"></td>
                                            <td class="to_date"></td>
                                            <td class="suburb_id"></td>
                                            <td><a class="deleteTouring">Delete</a></td>
                                        </tr>
                                        @foreach(EscortTouring::whereEscortId($escort->id)->get() as $touring)
                                        <tr class="touringEntry" id="touringEntry-{{ $touring->id }}">
                                            <td class="from_date">{{ Helpers::db_date_to_user_date($touring->from_date) }}</td>
                                            <td class="to_date">{{ Helpers::db_date_to_user_date($touring->to_date) }}</td>
                                            <td class="suburb_id">{{ $touring->location ? $touring->location->name : '' }}</td>
                                            <td><a class="deleteTouring" id="{{ $touring->id }}">Delete</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div id="touring-form">
                                    <fieldset>
                                        <div class="elements">
                                            <label for="from_date" class="control-label">From Date<span>*</span></label>
                                            <div class="controls">
                                                <input id="from_date" name="from_date" type="text" class="form-control datepicker" placeholder="Select date.">
                                            </div>
                                        </div>
                                        <div class="elements">
                                            <label for="to_date" class="control-label">To Date<span>*</span></label>
                                            <div class="controls">
                                                <input id="to_date" name="to_date" type="text" class="form-control datepicker" placeholder="Select date.">
                                            </div>
                                        </div>
                                        <div class="elements">
                                            <label for="suburb_id" class="control-label">Location<span>*</span></label>
                                            <div class="controls">
                                                {{ Form::select('suburb_id',$suburbs,$escort->current_suburb_id,array('class'=>'select2 input-large input03','id'=>'suburb_id','style'=>'width:70%')) }}
                                            </div>
                                        </div>
                                        <br>
                                        <div class="elements touring-btn-container">
                                            <div class="controls touring-btn">
                                                <button id="addTouring" class="btn btn-default pull-right">Add</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div><!-- .triggerAnimation.animated end -->
                        </div><!-- .col-md-12 end -->
                    </div><!-- .row end -->
                </div>
