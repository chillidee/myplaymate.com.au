@extends('frontend.layout')

@section('content')

   @if ( isset( $preview ))
     <div id ='iframeDisabler'></div>
   @endif

<?php $approved_girls = false;
foreach($escorts as $escort):
  if(($escort->status == 2 && $escort->main_image) || (isset( $preview ) && $escort->main_image) ):
    $approved_girls = true;
  break;
  endif;
  endforeach;
  ?>
  <script>window.businessName = '{{$brothel->name}}';</script>
  <div class ='showPage'>
    <div class="container brothel-container">
      <div class="row brothel-page-row">
        @if(( $brothel->banner_image && $brothel->banner_approved == 2) || (isset( $preview ) && $brothel->banner_image))
        <div class="brothel-banner-container">
          <img class ='full_width' src = '{{ Config::get('constants.CDN')}}/businesses/{{$brothel->banner_image}}'>
        </div>
        @endif
        <div class="brothel-page-info-container col-md-12">
         <div class="brothel-page-address col-md-8 col-md-offset-2 brothels-page-info">
           <p>
             @if ( $brothel->type != 2 )
             @if( $brothel->address_line_1 )
             {{$brothel->address_line_1}},
             @if( $brothel->address_line_2 )
             {{$brothel->address_line_2}},
             @endif
             @if( isset( $brothel->suburb ))
             {{$brothel->suburb }}
             @endif
             @if( isset( $brothel->state ))
             {{$brothel->state }}
             @endif
             @else
             {{$brothel->autocompleteaddress}}
             @endif
             @else
             {{ $brothel->suburb_name}}
             @endif
           </p>
         </div>
         <div class="col-md-8 col-md-offset-2 brothel-page-about-text-container">
           <div class="brothel-page-about-text">
             <p>{{nl2br($brothel->about )}}</p>
           </div>
         </div>
         <div class="col-md-8 col-md-offset-2 brothels-about-buttons">
           @if($brothel->type != 2)
           <div class="col-md-4">
            @if( isset( $brothel->latitude )) <div class ='actionPane actionPhone'><a href ='#directions' onclick ="ga('send', 'event', 'Business', 'Directions', '{{$brothel->name}}' )">Get Directions</a></div>@endif 
          </div>
          @endif
          <div class="col-md-4">
            <div class ='actionPane actionPlay'data-toggle="modal" href="#contactEscortModal">Contact</div>
          </div>
          @if( (isset( $brothel->website)) && ($brothel->website != "" ))
          <div class="col-md-4">
           <div class ='actionPane'><a target ='_blank' href ='http://{{$brothel->website}}' onclick ="ga('send', 'event', 'Business', 'Website click', '{{$brothel->name}}' )">Visit Website</a></div> 
         </div>
         @endif
       </div>
       @if( count( $escorts )> 0 && $approved_girls || (isset( $preview )))
       <div id="otherGirls" class="col-md-12 brothel-page-girls-grey-background">
        <div id ='showEscort'>
          <div id ='escortHolder'></div>
        </div>
        <div class="brothel-page-girls-title col-md-10 col-md-offset-1">
          <h2>Our Escorts</h2>
        </div>
        <div class="brothel-page-girls-container col-md-10 col-md-offset-1">
          <div class ='escort-container grid-view showPage clear'>
            @foreach ( $escorts as $escort ) 
            @if(($escort->status == 2 && $escort->main_image) || (isset( $preview )) )
            <li class="col-xs-12 col-ms-6 col-md-4 col-lg-3 col-xl-15 col-xl-3 escorts-wishlist clone" data-distance="0">
             <figure class="escorts-img-container business-page-escort-container" id="escort-{{$escort->id}}">
              <div class="info-container">
                <div class="figinfo">
                  <span class="figname">{{$escort->escort_name}}</span>
                  <span class="figdata">
                    <span class="escort-age">Age: {{$escort->escortAge}}</span>
                  </span>
                </div><!-- figinfo-->
              </div><!-- end of info-container-->
              <img class="img-profile" src="{{ Config::get('constants.CDN') }}/escorts/thumbnails/thumb_220x330_{{$escort->main_image}}" alt="" data-id ="{{$escort->id}}">  
            </figure>
          </li>
          @endif
          @endforeach


        </div>
      </div>
    </div>
    @endif
    <div class="col-md-12 brothels-page-google-map">
      @if( $brothel->latitude && $brothel->type != 2)
      <div id ='directions'>
        <div>
         @if( $brothel->address_line_1 )
         {{$brothel->address_line_1}},
         @if( $brothel->address_line_2 )
         {{$brothel->address_line_2}},
         @endif
         @if( isset( $brothel->suburb ))
         {{$brothel->suburb }}
         @endif
         @if( isset( $brothel->state ))
         {{$brothel->state }}
         @endif
         @else
         {{$brothel->autocompleteaddress}}
         @endif
       </div>
       <div id ='map'></div>
       @endif
     </div>
   </div>
 </div>
 <script>
  window.activeId = {{$brothel->id}};
  @if ( $brothel->latitude && $brothel->type != 2)
  window.mapPage = 1;
  @else 
  window.mapPage = 0;
  @endif
  window.getGirls = 1;
  window.mapName ={{json_encode( $brothel->name )}};
  @if ( $brothel->latitude && $brothel->type != 2 )
  window.center = {lat: {{$brothel->latitude}}, lng: {{$brothel->longitude}}};
  @endif
</script>
@include ( 'frontend.partials.businessModal' )
@stop













