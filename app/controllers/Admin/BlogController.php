<?php
namespace Admin;
use Controller, Auth, View, Helpers, Datatables, Input, ImageHelper, Carbon\Carbon,Config;
use Post, PostCategory;

class BlogController extends Controller {
  public function index()
  {
    return View::make('admin.blog.index');
  }
  
   public function categories()
  {
    return View::make('admin.blog.categories');
  }
  
   public function tags()
  {
    return View::make('admin.blog.tags');
  }

  public function ajax()
  {
    $query = Post::select('*');
    return Datatables::of($query)
      ->editColumn('published',function($post){
        return Helpers::get_active($post->published == 1);
      })
      ->editColumn('date_from','{{ Helpers::db_date_to_user_date($date_from) }}')
      ->editColumn('date_to','{{ Helpers::db_date_to_user_date($date_to) }}')
            ->addColumn('action',function($post){
              return '<div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$post->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Action
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="actions-'.$post->id.'">
                <li><a class="edit-profile" href="/admin/blog/edit/'.$post->id.'">Edit</a></li>
                <li><a class="notes" href="#">Notes</a></li>
              </ul>
            </div>';
            })
      ->make(true);
  }
  public function ajax_categories()
  {
    $query = PostCategory::select('*');
    return Datatables::of($query)
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->editColumn('updated_at','{{ Helpers::db_date_to_user_date($updated_at) }}')
            ->addColumn('action',function($postCategory){
              return '<div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$postCategory->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Action
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="actions-'.$postCategory->id.'">
                <li><a class="edit-profile" href="/admin/blog/edit/'.$postCategory->id.'">Edit</a></li>
                <li><a class="notes" href="#">Notes</a></li>
              </ul>
            </div>';
            })
      ->make(true);
  }

  public function getNew()
  {
    return View::make('admin.blog.create');
  }
    public function getNewCategory()
  {
    return View::make('admin.blog.create-category');
  }
  public function postNew()
  {
    $input = Input::all();
    $message = 'Blog post created!';
    $post = new Post;
    $user = Auth::user();

    $post->title = $input['title'];
    $post->user_id = $user->id;
    $post->meta_title = $input['meta_title'];
    $post->meta_keywords = $input['meta_keywords'];
    $post->meta_description = $input['meta_description'];
    $post->seo_url = $input['seo_url'];
    $post->short_content = $input['short_content'];
    $post->published = $input['published'];
    $post->date_from = Helpers::user_date_to_db_date($input['date_from']);
    $post->date_to = Helpers::user_date_to_db_date($input['date_to']);
    $post->content = $input['content'];
    $post->save();

    return View::make('admin.blog.index',compact('message'));
  }
    public function postNewCategory()
  {
    $input = Input::all();
    $message = 'Category created!';
    $post = new PostCategory;
    $user = Auth::user();

    $post->name = $input['name'];
    $post->description = $input['description'];
    $post->save();
   
    return View::make('admin.blog.categories',compact('message'));
  }

  public function getEdit($id)
  {
    $post = Post::find($id);
    return View::make('admin.blog.single',compact('post'));
  }
  public function postEdit($id)
  {
    $input = Input::all();
    $message = 'Blog post updated!';
    $post = Post::find($id);

    $post->title = $input['title'];
    $post->meta_title = $input['meta_title'];
    $post->meta_keywords = $input['meta_keywords'];
    $post->meta_description = $input['meta_description'];
    $post->seo_url = $input['seo_url'];
    $post->short_content = $input['short_content'];
    $post->published = $input['published'];
    $post->date_from = Helpers::user_date_to_db_date($input['date_from']);
    $post->date_to = Helpers::user_date_to_db_date($input['date_to']);
    $post->content = $input['content'];
    $post->save();

    return View::make('admin.blog.single',compact('post','message'));
  }

 /* public function upload()
  {
    $destinationPath = public_path().'/uploads/posts/';
    $filename = str_random(12).'.jpg';

    $post = Post::find(Input::get('post_id'));
    $old = $destinationPath.$post->thumbnail;
    $old_thumb = $destinationPath.'/thumbnails/thumb_170x170_'.$post->thumbnail;
    if ($post->thumbnail != '' && file_exists($old))
      unlink($old);
    if ($post->thumbnail != '' && file_exists($old_thumb))
      unlink($old_thumb);

    $post->thumbnail = $filename;
    $post->save();

    $file = Input::file('file');
    $file->move($destinationPath, $filename);

    ImageHelper::createThumbnailSize($destinationPath,$filename,170,170);
  }*/
   public function upload()
  {
    $destinationPath = public_path().'/temp/';
    $filename = str_random(12).'.jpg';

    $post = Post::find(Input::get('post_id'));
    $old = $post->thumbnail;
    ImageHelper::s3Delete( 'posts', $old );

    $post->thumbnail = $filename;
    $post->save();

    $file = Input::file('file');
    $file->move($destinationPath, $filename);

    ImageHelper::s3Upload(  $filename, 'posts/' );
    ImageHelper::s3thumbnail($filename,'posts/thumbnails', 170,170);
  }
}




