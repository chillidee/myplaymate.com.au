@extends('frontend.layout')

@section('content')
@if ( isset($meta->main_image ) && $meta->main_image != '' )
<img id = "main_image" src ="{{ Config::get('constants.CDN').'/'.str_replace( 'uploads/','', $meta->main_image) }}" alt ="{{ $title }}">
@endif;


<!-- #page-title start -->
<section id="page-title" class="page-title-1">
  <div class="container">
    <h1>{{ ucfirst ( $title ) }}</h1>
  </div><!-- .containr end -->
</section><!-- #page-title end -->

@if ( isset( $meta->template ) && $meta->template == 'full_width' )
<section class="page-wrapper top-space">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        {{ $meta->content }}
      </div>
    </div>
  </div>  
</section>
@else
<section class="page-wrapper top-space">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <ul class="aside-widgets">
          @include('frontend.partials.sidebar.left-common-widgets')
        </ul><!-- .aside-widgets end -->
      </div>

      <div class="col-md-9">
        {{ $meta->content }}
      </div>
    </div>
  </div>  
</section>
@endif
@stop