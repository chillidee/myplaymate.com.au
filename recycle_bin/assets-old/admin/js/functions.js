function changeApproval(clicked,action,escortId){
	$.ajax({
		url: '/admin/'+action+'_escort',
		data: {escort_id: escortId},
	})
	.done(function() {
		var label = clicked.parents('tr').find('span.label');
		label.removeClass('label-warning label-danger label-success');
		if (action == 'approve'){
			label.addClass('label-success').text('approved');
			updateBumpupDate(clicked.closest('tr').find('span.bumpup-date'));
		}
		else
			label.addClass('label-danger').text('disapproved');
	});
}

function approveEscort(e)
{
	e.preventDefault();
	var	$this = $(this),
		escortId = $this.data('escort-id');
	changeApproval($this,'approve',escortId);
}

function disapproveEscort(e)
{
	e.preventDefault();
	var	$this = $(this),
		escortId = $this.data('escort-id');
	changeApproval($this,'disapprove',escortId);
}

function deleteEscort(e)
{
	e.preventDefault();
	var $this = $(this),
		confirmation = confirm('Are you sure you want to delete this escort?');
	if (confirmation == true){
		$.ajax({
			url: '/admin/delete_escort',
			data: {escort_id: $this.data('escort-id')},
		})
		.done(function() {
			$this.parents('tr').remove();
			jQuery('.alert-success').text('Escort deleted!').fadeIn();
		});
	}
}

function bumpup(e)
{
	e.preventDefault();
	var	$this = $(this),
		escortId = $this.data('escort-id');
	
	$.ajax({
		url: '/admin/bump-up',
		data: {escort_id: escortId},
	})
	.done(function() {
		updateBumpupDate($this.closest('tr').find('span.bumpup-date'));
	});
}

function updateBumpupDate(element)
{
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var hour = d.getHours();
	var min = d.getMinutes();
	var sec = d.getSeconds();

	var output = (day<10 ? '0' : '') + day + '/' +
	    (month<10 ? '0' : '') + month + '/' +
	    d.getFullYear() + ' ' +
	    (hour<10 ? '0' : '') + hour + ':' +
	    (min<10 ? '0' : '') + min + ':' +
	    (sec<10 ? '0' : '') + sec;

	element.text(output);
}