{{ Form::open(['id'=>'businessInfo']) }}
    @if($business->image != '')
        <img class="section-content" src="/uploads/businesses/{{ $business->image }}" alt="{{ $business->name }}">
    @endif


    <div class="alignc">
        <h2>MY ESCORTS</h2>
        <p>Manage all your team with ease!</p> 
    </div>
    <div class="section-content alignc">
        <a href="" id="business-add-escort" class="submit-button section-content">Add escort</a>
    </div>
    <ul class="row section content">
        @if(count($my_escorts))
            @foreach($my_escorts as $key => $escort)
                <li class="col-md-6">
                    <div class="escort-sumup">
                        <figure class="escorts-img-container">
                            <a href="{{ url('/escorts').'/'.$escort->id }}">
                                @if($escort->main_image_id > 0 && $escort->mainImage->status == 2)
                                    <img class="img-profile" src="{{ $escort->mainImage->thumbnail(220,330) }}" alt="{{ $escort->escort_name }}">
                                @else
                                    <img class="img-profile" src="/assets/frontend/img/profile_template.jpg" style="width:86px; height:129px;">
                                @endif
                            </a>
                            <div class="info-container">
                                <div class="figinfo">
                                    <span class="figname">{{ $escort->escort_name }}</span>
                                    <ul class="escort-stats">
                                        <li>
                                            Status: {{ Helpers::get_status($escort->status) }}
                                        </li>
                                        <li>
                                            Rank: 
                                            <span class="bold">
                                                {{ $escort->rank }}
                                            </span> 
                                        </li>
                                        <li>
                                            Profile views: 
                                            <span class="bold">
                                                {{ $escort->count_views }}
                                            </span>    
                                        </li>
                                        <li>
                                            Phone number: 
                                            <span class="bold">
                                                {{ $escort->count_phone }}
                                            </span> 
                                        </li>
                                        <li>
                                            Playmail: 
                                            <span class="bold">
                                                {{ $escort->count_playmail }}
                                            </span> 
                                        </li>
                                    </ul>
                                </div><!-- figinfo-->
                                <div class="buttons">
                                    <a data-escort-id="{{ $escort->id }}" class="btn submit-button edit-escort" href="">Edit</a>
                                    <a class="btn submit-button" href="">Bump up</a>
                                    <a class="btn submit-button" href="{{ url('/escorts/').'/preview/'.$escort->id }}">Profile preview</a>
                                </div>
                            </div><!-- end of info-container-->
                        </figure>            
                    </div>
                </li>
            @endforeach
        @else
            <p class="alignc section-content italic">There are currently no escorts associated with your business.</p>
        @endif    
    </ul><!-- .row end -->
    
{{ Form::close() }}
<script>
    (function($){
        $('.second-level h2').text("{{ $business->name }}");

        $('body').on('click', '#business-add-escort', function(e) {
            e.preventDefault();

            businessLevel.slideUp(animTime);
            deactivateEscortButtons();
            refreshForm('newEscort','escort','',{{ $business->id }});
        });
    })(jQuery);
</script>