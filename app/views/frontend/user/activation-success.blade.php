@extends('frontend.layout')

@section('content')

<!-- .boxed version of the page -->
<div class="page-wrapper extra-top">
    <!-- .container start -->
    <div class="container">
        <!-- .row start -->
        <div class="row">
            <h2>Activation successful.</h2>
            <p>You are now logged in.</p>
            @if(Auth::user()->type == 2)
                <p>
                    <a href="/user/profile">Please click here to activate your profile.</a>
                </p>
            @else
            	<p>Go to the <a href="/">homepage</a> or discover our <a href="/escorts">escorts</a>!</p>
           	@endif
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- End of the Boxed Version Page -->

@stop
