@extends('admin.layout')
@section('javascript')
  <script>
    jQuery(document).ready(function() {
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax_pending_escorts",
            "order": [[ 5, "desc" ]],
        lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'escort_name', name: 'escort_name'},
                {data: 'business', name: 'business',orderable: false, searchable: false},
                {data: 'suburb_id', name: 'suburb_id'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name:'created_at'  },
                {data: 'featured', name: 'featured'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
  </script>
@stop
@section('content')
<div class ='overlay loadingDiv'></div>
<div class ='loader loadingDiv'></div>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Pending escorts</h1>
    </div>
</div><!--pageheader-->

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Escort name</th>
      <th>Business</th>
      <th>State</th>
      <th>Phone</th>
      <th>Email</th>
      <th>Date of creation</th>
      <th>Featured</th>
      <th>Action</th>
    </tr>
  </thead>
</table>

<script>
  function setStatus(button,status)
  {
    var url,
      escortId = button.data('escort-id');
    if(status == 2){
      url = 'approve_escort';
      message = 'Escort approved!';
    }
    if(status == 3){
      url = 'disapprove_escort';
      message = 'Escort disapproved!';
    }
    $( '.loadingDiv').show();

    jQuery.ajax({
      url: '/admin/'+url,
      data: {escort_id: escortId},
    })
    .done(function() {
      button.parents('tr').remove();
      jQuery('.alert-success').text(message).fadeIn();
       $( '#pendingEscortsBubble').html( parseInt($( '#pendingEscortsBubble').html() ) - 1 );
       $( '.loadingDiv').hide();
    });
  }

  (function($){
    $('body').on('click', '.approve', function(e) {
      e.preventDefault();
      setStatus($(this),2);
    });  

    $('body').on('click', '.disapprove', function(e) {
      e.preventDefault();
      setStatus($(this),3);
    });  
  })(jQuery);
</script>
<style>
 .overlay { position:fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: #000; opacity: .3; }
       .loader {
  font-size: 10px;
  margin: 50px auto;
  z-index:1001;
  text-indent: -9999em;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background: #ffffff;
  background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
  position: relative;
  -webkit-animation: load3 1.4s infinite linear;
  animation: load3 1.4s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
   display:none;
  position:fixed;
  left:50%;
  margin-left: 50px;
}
.loader:before {
  width: 50%;
  height: 50%;
  background: #ffffff;
  border-radius: 100% 0 0 0;
  position: absolute;
  top: 0;
  left: 0;
  content: '';
}
.loader:after {
  width: 75%;
  height: 75%;
  border-radius: 50%;
  content: '';
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
@-webkit-keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes load3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>

@stop


