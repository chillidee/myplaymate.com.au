<?php
namespace Admin;
use Controller, View, Helpers, Datatables, Input;
use Page;

class BrothelPageController extends Controller {
  public function index()
  {
    return View::make('admin.brothel.index');
  }

  public function ajax()
  {
    $query = Page::select('*')
             ->where('type', '=', '2' );
    return Datatables::of($query)
      ->editColumn('created_at','{{ Helpers::db_date_to_user_date($created_at) }}')
      ->editColumn('updated_at','{{ Helpers::db_date_to_user_date($updated_at) }}')
            ->addColumn('action',function($page){
              return '<div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" type="button" id="actions-'.$page->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Action
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="actions-'.$page->id.'">
                <li><a class="edit-profile" href="/admin/pages/edit/'.$page->id.'">Edit</a></li>
                <li><a class="notes" href="#">Notes</a></li>
              </ul>
            </div>';
            })
      ->make(true);
  }

  public function getNew()
  {
    return View::make('admin.pages.create');
  }
  public function postNew()
  {
    $input = Input::all();
    $message = 'Page created!';
    $page = new Page;

    $page->name = $input['name'];
    $page->meta_title = $input['meta_title'];
    $page->meta_keywords = $input['meta_keywords'];
    $page->meta_description = $input['meta_description'];
    $page->content = $input['content'];
    $page->save();

    return View::make('admin.pages.index',compact('message'));
  }

  public function getEdit($id)
  {
    $page = Page::find($id);
    return View::make('admin.pages.single',compact('page'));
  }
  public function postEdit($id)
  {
    $input = Input::all();
    $message = 'Page updated!';
    $page = Page::find($id);

    $page->name = $input['name'];
    $page->meta_title = $input['meta_title'];
    $page->meta_keywords = $input['meta_keywords'];
    $page->meta_description = $input['meta_description'];
    $page->content = $input['content'];
    $page->save();

    return View::make('admin.pages.single',compact('page','message'));
  }
}



