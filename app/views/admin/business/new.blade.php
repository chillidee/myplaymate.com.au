@extends('admin.layout')

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-money"></span></div>
    <div class="pagetitle">
        <h1>New business</h1>
    </div>
</div><!--pageheader-->

{{ Form::open(['id'=>'businessInfo']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="control-group">
                {{ Form::label('type','Business type*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::select('type',[1=>'Brothel',2=>'Agency',3=>'Massage'],'',array('class'=>'form-control')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('user_id','User*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::select('user_id',$users,'',array('class'=>'select2 form-control','style'=>'width:100%','min'=>1)) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('phone','Phone*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('phone','',array('class'=>'form-control','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('email','Email*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('email','',array('class'=>'form-control','required','data-parsley-type'=>'email')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('suburb_id','Business location*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::select('suburb_id',$suburbs,'',array('class'=>'select2 form-control','style'=>'width:100%','min'=>1)) }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="control-group">
                {{ Form::label('name','Business Name*',array('class'=>'control-label')) }}

                <div class="controls">
                    {{ Form::text('name','',array('class'=>'business-name form-control','required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('address_line_1','Address Line 1*',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::text('address_line_1','',array('class'=>'form-control','required')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::label('address_line_2','Address Line 2',array('class'=>'control-label')) }}
                <div class="controls">
                    {{ Form::text('address_line_2','',array('class'=>'form-control')) }}
                </div>
            </div>

        </div>
    </div>

    <div class="alignc">
        {{ Form::submit('SAVE',array('class'=>'btn btn-primary')) }}
    </div>
{{ Form::close() }}


<script>
    jQuery(document).ready(function($) {
        $('.select2').select2({
            minimumInputLength: 4
        });
        $('form').parsley();
    });
</script>
@stop




