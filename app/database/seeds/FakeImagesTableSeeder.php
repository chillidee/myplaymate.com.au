<?php

class FakeImagesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('escort_images')->truncate();

        $faker = Faker\Factory::create();

        foreach (range(1,60) as $index) {
            EscortImage::create([
                'escort_id'         => $index,
                'filename'          => 'mpm-'.$index.'.jpg',
                'status'            => $faker->numberBetween(1,3)
            ]);
        }

        foreach (range(1,60) as $index) {
            EscortImage::create([
                'escort_id'         => $index,
                'filename'          => 'esc1_watermark.jpg',
                'status'            => $faker->numberBetween(1,3)
            ]);
            EscortImage::create([
                'escort_id'         => $index,
                'filename'          => 'esc2_watermark.jpg',
                'status'            => $faker->numberBetween(1,3)
            ]);
            EscortImage::create([
                'escort_id'         => $index,
                'filename'          => 'esc3_watermark.jpg',
                'status'            => $faker->numberBetween(1,3)
            ]);
        }
    }
}


