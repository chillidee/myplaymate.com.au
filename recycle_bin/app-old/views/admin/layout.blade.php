<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin - My Play Mate</title>
        <link rel="stylesheet" href="{{ asset('assets/admin/css/dropzone.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.min.css') }}">
        <link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
        <link rel="stylesheet" href="{{ asset('assets/admin/css/style.default.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}" type="text/css" />

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/jquery-ui-1.10.3.min.js') }}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.cookie.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/lib/select2.full.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/lib/dropzone.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/tinymce/jquery.tinymce.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/wysiwyg.js') }}"></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script src="{{ asset('/assets/frontend/js/lib/parsley.min.js') }}"></script>
        <script src="{{ asset('/assets/admin/js/functions.js') }}"></script>
        {{-- <script type="text/javascript" src="{{ asset('assets/admin/js/custom.js') }}"></script> --}}
        @yield('javascript')
    </head>
    <body {{ isset($bodyClass) ? 'class="'.$bodyClass.'"' : '' }}>
        <div id="mainwrapper" class="mainwrapper">
            <div class="header">
                <div class="logo">
                    <a href="/admin"><img src="{{ asset('assets/frontend/img/logo.png') }}" alt="myplaymate.com.au" /></a>
                </div>
                <div class="headerinner">
                    <ul class="headmenu">
                        <li class="right">
                            <div class="userloggedinfo">
                                <div class="userinfo">
                                    <h5>{{ Auth::user()->full_name }} <small>- {{ Auth::user()->email }}</small></h5>
                                    <ul>
                                        <li><a href="{{ url('admin/logout') }}">Log Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="leftpanel">
                <div class="leftmenu">
                    <ul class="nav nav-tabs nav-stacked">
                        <li><a href="{{ url('admin') }}"><span class="fa fa-laptop"></span> Dashboard</a></li>
                        <li><a href="{{ url('admin/pending_escorts') }}"><span class="fa fa-user"></span> Pending Escorts <span class="badge">{{ Escort::whereStatus(1)->count() > 0 ? Escort::whereStatus(1)->count() : '' }}</span></a></li>
                        <li><a href="{{ url('admin/pending_images') }}"><span class="fa fa-picture-o"></span> Pending images <span class="badge">{{ EscortImage::whereStatus(1)->count() > 0 ? EscortImage::whereStatus(1)->count() : '' }}</span></a></li>
                        <li><a href="{{ url('admin/users') }}"><span class="fa fa-user"></span> Users</a></li>
                        <li><a href="{{ url('admin/escorts') }}"><span class="fa fa-user"></span> All Escorts</a></li>
                        <li><a href="{{ url('admin/businesses') }}"><span class="fa fa-money"></span> Businesses</a></li>
                        <li><a href="{{ url('admin/pages') }}"><span class="fa fa-file-text-o"></span> Pages</a></li>
                        <li><a href="{{ url('admin/blog') }}"><span class="fa fa-newspaper-o"></span> Blog</a></li>
{{--                         <li><a href="{{ url('admin/updates') }}"><span class="fa fa-comment"></span> Sexy updates <span class="badge">{{ EscortUpdate::whereStatus(1)->count() > 0 ? EscortUpdate::whereStatus(1)->count() : ''; }}</span></a></li> --}}
                        <li><a href="{{ url('admin/reviews') }}"><span class="fa fa-comment"></span> Reviews <span class="badge">{{ EscortReview::whereStatus(1)->count() > 0 ? EscortReview::whereStatus(1)->count() : ''; }}</span></a></li>
                        <li><a href="{{ url('admin/comments') }}"><span class="fa fa-comment"></span> Comments <span class="badge">{{ Comment::whereStatus(1)->count() > 0 ? Comment::whereStatus(1)->count() : ''; }}</span></a></li>
                        {{--<li><a href="{{ url('admin/users') }}"><span class="fa fa-user"></span> Administrators</a></li>
                    <li><a href="{{ url('admin/emailtemplates') }}"><span class="fa fa-envelope"></span> Email Templates</a></li>
                    <li><a href="{{ url('admin/settings') }}"><span class="fa fa-envelope"></span> Settings</a></li> --}}
                    </ul>
                </div><!--leftmenu-->
            </div><!-- leftpanel -->
            <div class="rightpanel">
                <div class="right-wrapper container-fluid">
                    <div class="alert alert-success"></div>

                    @yield('content')

                </div>
            </div><!--rightpanel-->
        </div><!--mainwrapper-->
    
        @yield('modals')

    </body>
</html>