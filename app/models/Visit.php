<?php

class Visit extends Eloquent
{
    protected $guarded = [];

    public static $rules = [];

    public $timestamps = false;
}
