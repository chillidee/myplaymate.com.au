<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Admin\\AdvertisingController' => $baseDir . '/app/controllers/Admin/AdvertisingController.php',
    'Admin\\BlogController' => $baseDir . '/app/controllers/Admin/BlogController.php',
    'Admin\\BrothelPageController' => $baseDir . '/app/controllers/Admin/BrothelPageController.php',
    'Admin\\BusinessController' => $baseDir . '/app/controllers/Admin/BusinessController.php',
    'Admin\\CityPageController' => $baseDir . '/app/controllers/Admin/CityPageController.php',
    'Admin\\CommentController' => $baseDir . '/app/controllers/Admin/CommentController.php',
    'Admin\\CsvController' => $baseDir . '/app/controllers/Admin/CsvController.php',
    'Admin\\DashboardController' => $baseDir . '/app/controllers/Admin/DashboardController.php',
    'Admin\\EscortController' => $baseDir . '/app/controllers/Admin/EscortController.php',
    'Admin\\Escort_temp_view' => $baseDir . '/app/controllers/Admin/EscortController.php',
    'Admin\\ExportController' => $baseDir . '/app/controllers/Admin/ExportController.php',
    'Admin\\ImageController' => $baseDir . '/app/controllers/Admin/ImageController.php',
    'Admin\\MediaController' => $baseDir . '/app/controllers/Admin/MediaController.php',
    'Admin\\PageController' => $baseDir . '/app/controllers/Admin/PageController.php',
    'Admin\\ReviewController' => $baseDir . '/app/controllers/Admin/ReviewController.php',
    'Admin\\UpdateController' => $baseDir . '/app/controllers/Admin/UpdateController.php',
    'Admin\\UserController' => $baseDir . '/app/controllers/Admin/UserController.php',
    'Advertisement' => $baseDir . '/app/models/Advertisement.php',
    'Age' => $baseDir . '/app/models/Age.php',
    'AgesTableSeeder' => $baseDir . '/app/database/seeds/AgesTableSeeder.php',
    'ApprovalStatusesTableSeeder' => $baseDir . '/app/database/seeds/ApprovalStatusesTableSeeder.php',
    'BaseController' => $baseDir . '/app/controllers/BaseController.php',
    'BlogController' => $baseDir . '/app/controllers/BlogController.php',
    'BodiesTableSeeder' => $baseDir . '/app/database/seeds/BodiesTableSeeder.php',
    'Body' => $baseDir . '/app/models/Body.php',
    'Business' => $baseDir . '/app/models/Business.php',
    'BusinessController' => $baseDir . '/app/controllers/BusinessController.php',
    'BusinessPlan' => $baseDir . '/app/models/BusinessPlan.php',
    'BusinessReview' => $baseDir . '/app/models/BusinessReview.php',
    'CitiesTableSeeder' => $baseDir . '/app/database/seeds/CitiesTableSeeder.php',
    'City' => $baseDir . '/app/models/City.php',
    'Comment' => $baseDir . '/app/models/Comment.php',
    'ContactController' => $baseDir . '/app/controllers/ContactController.php',
    'CountriesTableSeeder' => $baseDir . '/app/database/seeds/CountriesTableSeeder.php',
    'Country' => $baseDir . '/app/models/Country.php',
    'CreateAdvertisementsTable' => $baseDir . '/app/database/migrations/2015_07_10_042555_create_advertisements_table.php',
    'CreateAgesTable' => $baseDir . '/app/database/migrations/2015_07_10_063657_create_ages_table.php',
    'CreateApprovalStatusesTable' => $baseDir . '/app/database/migrations/2015_07_10_063746_create_approval_statuses_table.php',
    'CreateBannersTable' => $baseDir . '/app/database/migrations/2015_07_10_064002_create_banners_table.php',
    'CreateBlocksTable' => $baseDir . '/app/database/migrations/2015_07_10_064044_create_blocks_table.php',
    'CreateBodiesTable' => $baseDir . '/app/database/migrations/2015_07_10_064211_create_bodies_table.php',
    'CreateBusinessPlansTable' => $baseDir . '/app/database/migrations/2015_07_10_064625_create_business_plans_table.php',
    'CreateBusinessSubscriptionsTable' => $baseDir . '/app/database/migrations/2015_07_10_065007_create_business_subscriptions_table.php',
    'CreateBusinessesTable' => $baseDir . '/app/database/migrations/2015_07_10_064335_create_businesses_table.php',
    'CreateCitiesTable' => $baseDir . '/app/database/migrations/2015_07_10_065320_create_cities_table.php',
    'CreateCommentsTable' => $baseDir . '/app/database/migrations/2015_07_10_065457_create_comments_table.php',
    'CreateCountriesTable' => $baseDir . '/app/database/migrations/2015_07_10_065549_create_countries_table.php',
    'CreateCouponsTable' => $baseDir . '/app/database/migrations/2015_07_10_065642_create_coupons_table.php',
    'CreateEmailTemplatesTable' => $baseDir . '/app/database/migrations/2015_07_10_065744_create_email_templates_table.php',
    'CreateEnquiriesTable' => $baseDir . '/app/database/migrations/2015_07_12_224236_create_enquiries_table.php',
    'CreateEscortAvailabilitiesTable' => $baseDir . '/app/database/migrations/2015_07_12_225328_create_escort_availabilities_table.php',
    'CreateEscortBusinessTable' => $baseDir . '/app/database/migrations/2015_07_13_012446_create_escort_business_table.php',
    'CreateEscortImagesTable' => $baseDir . '/app/database/migrations/2015_07_12_225449_create_escort_images_table.php',
    'CreateEscortLanguageTable' => $baseDir . '/app/database/migrations/2015_07_12_225551_create_escort_language_table.php',
    'CreateEscortPlansTable' => $baseDir . '/app/database/migrations/2015_07_12_225808_create_escort_plans_table.php',
    'CreateEscortRatesTable' => $baseDir . '/app/database/migrations/2015_07_12_225922_create_escort_rates_table.php',
    'CreateEscortReviewsTable' => $baseDir . '/app/database/migrations/2015_07_12_230209_create_escort_reviews_table.php',
    'CreateEscortServicesTable' => $baseDir . '/app/database/migrations/2015_07_12_231134_create_escort_services_table.php',
    'CreateEscortSubscriptionsTable' => $baseDir . '/app/database/migrations/2015_07_12_231254_create_escort_subscriptions_table.php',
    'CreateEscortTouringsTable' => $baseDir . '/app/database/migrations/2015_07_12_231357_create_escort_tourings_table.php',
    'CreateEscortUpdatesTable' => $baseDir . '/app/database/migrations/2015_07_15_055314_create_escort_updates_table.php',
    'CreateEscortsTable' => $baseDir . '/app/database/migrations/2015_07_12_231053_create_escorts_table.php',
    'CreateEthnicitiesTable' => $baseDir . '/app/database/migrations/2015_07_12_231443_create_ethnicities_table.php',
    'CreateEyeColorsTable' => $baseDir . '/app/database/migrations/2015_07_12_231500_create_eye_colors_table.php',
    'CreateGendersTable' => $baseDir . '/app/database/migrations/2015_07_12_231515_create_genders_table.php',
    'CreateHairColorsTable' => $baseDir . '/app/database/migrations/2015_07_12_231524_create_hair_colors_table.php',
    'CreateHeightsTable' => $baseDir . '/app/database/migrations/2015_07_12_231534_create_heights_table.php',
    'CreateHourlyRatesTable' => $baseDir . '/app/database/migrations/2015_07_12_231553_create_hourly_rates_table.php',
    'CreateLanguagesTable' => $baseDir . '/app/database/migrations/2015_07_12_231605_create_languages_table.php',
    'CreatePagesTable' => $baseDir . '/app/database/migrations/2015_07_12_231702_create_pages_table.php',
    'CreatePasswordRemindersTable' => $baseDir . '/app/database/migrations/2015_07_12_231739_create_password_reminders_table.php',
    'CreatePostCategoriesTable' => $baseDir . '/app/database/migrations/2015_07_12_232130_create_post_categories_table.php',
    'CreatePostsTable' => $baseDir . '/app/database/migrations/2015_07_12_232103_create_posts_table.php',
    'CreateServicesTable' => $baseDir . '/app/database/migrations/2015_07_12_232400_create_services_table.php',
    'CreateSessionsTable' => $baseDir . '/app/database/migrations/2015_07_12_232431_create_sessions_table.php',
    'CreateSettingsTable' => $baseDir . '/app/database/migrations/2015_07_12_232458_create_settings_table.php',
    'CreateStatesTable' => $baseDir . '/app/database/migrations/2015_07_12_232528_create_states_table.php',
    'CreateStaticBlocksTable' => $baseDir . '/app/database/migrations/2015_07_12_232600_create_static_blocks_table.php',
    'CreateSuburbsTable' => $baseDir . '/app/database/migrations/2015_07_12_232627_create_suburbs_table.php',
    'CreateUserExportsTable' => $baseDir . '/app/database/migrations/2015_07_12_232708_create_user_exports_table.php',
    'CreateUsersTable' => $baseDir . '/app/database/migrations/2015_07_10_045221_create_users_table.php',
    'CreateWishlistsTable' => $baseDir . '/app/database/migrations/2015_07_12_232744_create_wishlists_table.php',
    'CustomerUser' => $baseDir . '/app/models/CustomerUser.php',
    'Dashboard\\AdminController' => $baseDir . '/app/controllers/Dashboard/AdminController.php',
    'Dashboard\\BusinessController' => $baseDir . '/app/controllers/Dashboard/BusinessController.php',
    'Dashboard\\DirectiveController' => $baseDir . '/app/controllers/Dashboard/DirectiveController.php',
    'Dashboard\\EscortController' => $baseDir . '/app/controllers/Dashboard/EscortController.php',
    'Dashboard\\UserController' => $baseDir . '/app/controllers/Dashboard/UserController.php',
    'DatabaseSeeder' => $baseDir . '/app/database/seeds/DatabaseSeeder.php',
    'DeletedBusiness' => $baseDir . '/app/models/DeletedBusiness.php',
    'DeletedEscort' => $baseDir . '/app/models/DeletedEscort.php',
    'EmailTemplate' => $baseDir . '/app/models/EmailTemplate.php',
    'EmailTemplatesTableSeeder' => $baseDir . '/app/database/seeds/EmailTemplatesTableSeeder.php',
    'Escort' => $baseDir . '/app/models/Escort.php',
    'EscortAvailability' => $baseDir . '/app/models/EscortAvailability.php',
    'EscortBusiness' => $baseDir . '/app/models/EscortBusiness.php',
    'EscortController' => $baseDir . '/app/controllers/EscortController.php',
    'EscortImage' => $baseDir . '/app/models/EscortImage.php',
    'EscortLanguage' => $baseDir . '/app/models/EscortLanguage.php',
    'EscortRate' => $baseDir . '/app/models/EscortRate.php',
    'EscortReview' => $baseDir . '/app/models/EscortReview.php',
    'EscortService' => $baseDir . '/app/models/EscortService.php',
    'EscortTouring' => $baseDir . '/app/models/EscortTouring.php',
    'EscortUpdate' => $baseDir . '/app/models/EscortUpdate.php',
    'EscortUser' => $baseDir . '/app/models/EscortUser.php',
    'EthnicitiesTableSeeder' => $baseDir . '/app/database/seeds/EthnicitiesTableSeeder.php',
    'Ethnicity' => $baseDir . '/app/models/Ethnicity.php',
    'EyeColor' => $baseDir . '/app/models/EyeColor.php',
    'EyeColorsTableSeeder' => $baseDir . '/app/database/seeds/EyecolorsTableSeeder.php',
    'FakeBusinessesTableSeeder' => $baseDir . '/app/database/seeds/FakeBusinessesTableSeeder.php',
    'FakeEscortBusinessTableSeeder' => $baseDir . '/app/database/seeds/FakeEscortBusinessTableSeeder.php',
    'FakeEscortRatesTableSeeder' => $baseDir . '/app/database/seeds/FakeEscortRatesTableSeeder.php',
    'FakeEscortServicesTableSeeder' => $baseDir . '/app/database/seeds/FakeEscortServicesTableSeeder.php',
    'FakeEscortTouringsTableSeeder' => $baseDir . '/app/database/seeds/FakeEscortTouringsTableSeeder.php',
    'FakeEscortsTableSeeder' => $baseDir . '/app/database/seeds/FakeEscortsTableSeeder.php',
    'FakeImagesTableSeeder' => $baseDir . '/app/database/seeds/FakeImagesTableSeeder.php',
    'FakePostsTableSeeder' => $baseDir . '/app/database/seeds/FakePostsTableSeeder.php',
    'FakeUpdatesTableSeeder' => $baseDir . '/app/database/seeds/FakeUpdatesTableSeeder.php',
    'FakeUsersTableSeeder' => $baseDir . '/app/database/seeds/FakeUsersTableSeeder.php',
    'FooterPostsComposer' => $baseDir . '/app/composers/FooterPostsComposer.php',
    'Gender' => $baseDir . '/app/models/Gender.php',
    'GendersTableSeeder' => $baseDir . '/app/database/seeds/GendersTableSeeder.php',
    'HairColor' => $baseDir . '/app/models/HairColor.php',
    'HairColorsTableSeeder' => $baseDir . '/app/database/seeds/HaircolorsTableSeeder.php',
    'Height' => $baseDir . '/app/models/Height.php',
    'HeightsTableSeeder' => $baseDir . '/app/database/seeds/HeightsTableSeeder.php',
    'Helpers' => $baseDir . '/app/models/Helpers.php',
    'HomeController' => $baseDir . '/app/controllers/HomeController.php',
    'HourlyRate' => $baseDir . '/app/models/HourlyRate.php',
    'HourlyratesTableSeeder' => $baseDir . '/app/database/seeds/HourlyratesTableSeeder.php',
    'IlluminateQueueClosure' => $vendorDir . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
    'ImageController' => $baseDir . '/app/controllers/ImageController.php',
    'ImageHelper' => $baseDir . '/app/models/ImageHelper.php',
    'Language' => $baseDir . '/app/models/Language.php',
    'LanguagesTableSeeder' => $baseDir . '/app/database/seeds/LanguagesTableSeeder.php',
    'LocationController' => $baseDir . '/app/controllers/LocationController.php',
    'Media' => $baseDir . '/app/models/Media.php',
    'Normalizer' => $vendorDir . '/patchwork/utf8/src/Normalizer.php',
    'Option' => $baseDir . '/app/models/Option.php',
    'Orangehill\\Iseed\\TableNotFoundException' => $vendorDir . '/orangehill/iseed/src/Orangehill/Iseed/Exceptions.php',
    'OtherComposer' => $baseDir . '/app/composers/OtherComposer.php',
    'Page' => $baseDir . '/app/models/Page.php',
    'PageController' => $baseDir . '/app/controllers/PageController.php',
    'PagesTableSeeder' => $baseDir . '/app/database/seeds/PagesTableSeeder.php',
    'Payment' => $baseDir . '/app/models/Payment.php',
    'PaymentController' => $baseDir . '/app/controllers/PaymentController.php',
    'PhoneCall' => $baseDir . '/app/models/PhoneCall.php',
    'Playmail' => $baseDir . '/app/models/Playmail.php',
    'Post' => $baseDir . '/app/models/Post.php',
    'PostCategory' => $baseDir . '/app/models/PostCategory.php',
    'Postcode' => $baseDir . '/app/models/Postcode.php',
    'Profile' => $baseDir . '/app/models/Profile.php',
    'ProfileController' => $baseDir . '/app/controllers/ProfileController.php',
    'ProfilesTableSeeder' => $baseDir . '/app/database/seeds/ProfilesTableSeeder.php',
    'SebastianBergmann\\Comparator\\ArrayComparator' => $vendorDir . '/sebastian/comparator/src/ArrayComparator.php',
    'SebastianBergmann\\Comparator\\Comparator' => $vendorDir . '/sebastian/comparator/src/Comparator.php',
    'SebastianBergmann\\Comparator\\ComparisonFailure' => $vendorDir . '/sebastian/comparator/src/ComparisonFailure.php',
    'SebastianBergmann\\Comparator\\DOMNodeComparator' => $vendorDir . '/sebastian/comparator/src/DOMNodeComparator.php',
    'SebastianBergmann\\Comparator\\DateTimeComparator' => $vendorDir . '/sebastian/comparator/src/DateTimeComparator.php',
    'SebastianBergmann\\Comparator\\DoubleComparator' => $vendorDir . '/sebastian/comparator/src/DoubleComparator.php',
    'SebastianBergmann\\Comparator\\ExceptionComparator' => $vendorDir . '/sebastian/comparator/src/ExceptionComparator.php',
    'SebastianBergmann\\Comparator\\Factory' => $vendorDir . '/sebastian/comparator/src/Factory.php',
    'SebastianBergmann\\Comparator\\MockObjectComparator' => $vendorDir . '/sebastian/comparator/src/MockObjectComparator.php',
    'SebastianBergmann\\Comparator\\NumericComparator' => $vendorDir . '/sebastian/comparator/src/NumericComparator.php',
    'SebastianBergmann\\Comparator\\ObjectComparator' => $vendorDir . '/sebastian/comparator/src/ObjectComparator.php',
    'SebastianBergmann\\Comparator\\ResourceComparator' => $vendorDir . '/sebastian/comparator/src/ResourceComparator.php',
    'SebastianBergmann\\Comparator\\ScalarComparator' => $vendorDir . '/sebastian/comparator/src/ScalarComparator.php',
    'SebastianBergmann\\Comparator\\SplObjectStorageComparator' => $vendorDir . '/sebastian/comparator/src/SplObjectStorageComparator.php',
    'SebastianBergmann\\Comparator\\TypeComparator' => $vendorDir . '/sebastian/comparator/src/TypeComparator.php',
    'SebastianBergmann\\Diff\\Chunk' => $vendorDir . '/sebastian/diff/src/Chunk.php',
    'SebastianBergmann\\Diff\\Diff' => $vendorDir . '/sebastian/diff/src/Diff.php',
    'SebastianBergmann\\Diff\\Differ' => $vendorDir . '/sebastian/diff/src/Differ.php',
    'SebastianBergmann\\Diff\\LCS\\LongestCommonSubsequence' => $vendorDir . '/sebastian/diff/src/LCS/LongestCommonSubsequence.php',
    'SebastianBergmann\\Diff\\LCS\\MemoryEfficientImplementation' => $vendorDir . '/sebastian/diff/src/LCS/MemoryEfficientLongestCommonSubsequenceImplementation.php',
    'SebastianBergmann\\Diff\\LCS\\TimeEfficientImplementation' => $vendorDir . '/sebastian/diff/src/LCS/TimeEfficientLongestCommonSubsequenceImplementation.php',
    'SebastianBergmann\\Diff\\Line' => $vendorDir . '/sebastian/diff/src/Line.php',
    'SebastianBergmann\\Diff\\Parser' => $vendorDir . '/sebastian/diff/src/Parser.php',
    'SebastianBergmann\\Exporter\\Exporter' => $vendorDir . '/sebastian/exporter/src/Exporter.php',
    'SebastianBergmann\\RecursionContext\\Context' => $vendorDir . '/sebastian/recursion-context/src/Context.php',
    'SebastianBergmann\\RecursionContext\\Exception' => $vendorDir . '/sebastian/recursion-context/src/Exception.php',
    'SebastianBergmann\\RecursionContext\\InvalidArgumentException' => $vendorDir . '/sebastian/recursion-context/src/InvalidArgumentException.php',
    'Service' => $baseDir . '/app/models/Service.php',
    'ServiceController' => $baseDir . '/app/controllers/ServiceController.php',
    'ServicesTableSeeder' => $baseDir . '/app/database/seeds/ServicesTableSeeder.php',
    'SessionHandlerInterface' => $vendorDir . '/symfony/http-foundation/Symfony/Component/HttpFoundation/Resources/stubs/SessionHandlerInterface.php',
    'Sidebar' => $baseDir . '/app/models/Sidebar.php',
    'SidebarWidgetsComposer' => $baseDir . '/app/composers/SidebarWidgetsComposer.php',
    'State' => $baseDir . '/app/models/State.php',
    'StatesTableSeeder' => $baseDir . '/app/database/seeds/StatesTableSeeder.php',
    'StaticBlock' => $baseDir . '/app/models/StaticBlock.php',
    'StaticBlocksTableSeeder' => $baseDir . '/app/database/seeds/StaticBlocksTableSeeder.php',
    'Suburb' => $baseDir . '/app/models/Suburb.php',
    'SuburbsTableSeeder' => $baseDir . '/app/database/seeds/SuburbsTableSeeder.php',
    'TestCase' => $baseDir . '/app/tests/TestCase.php',
    'User' => $baseDir . '/app/models/User.php',
    'UserController' => $baseDir . '/app/controllers/UserController.php',
    'Visit' => $baseDir . '/app/models/Visit.php',
    'Whoops\\Module' => $vendorDir . '/filp/whoops/src/deprecated/Zend/Module.php',
    'Whoops\\Provider\\Zend\\ExceptionStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/ExceptionStrategy.php',
    'Whoops\\Provider\\Zend\\RouteNotFoundStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/RouteNotFoundStrategy.php',
    'Wishlist' => $baseDir . '/app/models/Wishlist.php',
    'WishlistController' => $baseDir . '/app/controllers/WishlistController.php',
    '_SuburbsTableSeeder' => $baseDir . '/app/database/seeds/_SuburbsTableSeeder.php',
);
