<?php

namespace MyPlaymate\Repositories;

use User;

class UserRepository {

	public function populateSelect()
	{
		$user_obj = User::get(['id','email']);
		$users = array('Choose a user...');
		foreach ($user_obj as $key => $user)
		{
			$users[$user->id] = $user->id . ' - ' . $user->email;
		}

		return $users;
	}
}