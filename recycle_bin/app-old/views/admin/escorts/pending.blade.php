@extends('admin.layout')
@section('javascript')
	<script>
		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "/admin/ajax_pending_escorts",
				lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'escort_name', name: 'escort_name'},
		            {data: 'suburb_id', name: 'suburb_id'},
		            {data: 'phone', name: 'phone'},
		            {data: 'email', name: 'email'},
		            {data: 'created_at', name:'created_at'  },
		            {data: 'featured', name: 'featured'},
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });
		});
	</script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Pending escorts</h1>
    </div>
</div><!--pageheader-->

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Escort name</th>
			<th>State</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Date of creation</th>
			<th>Featured</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

<script>
	function setStatus(button,status)
	{
		var url,
			escortId = button.data('escort-id');
		if(status == 2){
			url = 'approve_escort';
			message = 'Escort approved!';
		}
		if(status == 3){
			url = 'disapprove_escort';
			message = 'Escort disapproved!';
		}

		jQuery.ajax({
			url: '/admin/'+url,
			data: {escort_id: escortId},
		})
		.done(function() {
			button.parents('tr').remove();
			jQuery('.alert-success').text(message).fadeIn();
		});
	}

	(function($){
		$('body').on('click', '.approve', function(e) {
			e.preventDefault();
			setStatus($(this),2);
		});	

		$('body').on('click', '.disapprove', function(e) {
			e.preventDefault();
			setStatus($(this),3);
		});	
	})(jQuery);
</script>

@stop



