 <div id ='overlayBackGround' ng-init = 'scopeInit()'></div>
 <div class ='_MpoffWhite businessProfileScreen'>
   <div>
     <div class ='mobile_only' id ='navigateThroughProfile'>Show Profile Menu</div>
     <div class ='mPProfileTopMenu'>
       <div><div id ='mpBasicInfo' class ='active'></div><span>ESCORT</span></div>
       <div><div id ='mpProfileDetails' ng-click ='getHeight()'></div><span ng-click ='getHeight()'>PROFILE</span></div>
       <div><div id ='mpAvailability' ng-click ='getAv()'></div><span>DETAILS</span ng-click ='getAv()'></div>
       <div><div id ='mpGallery' ng-click ='getImages()'></div><span ng-click ='getImages()'>GALLERY</span></div>
       <div><div id ='mpTouring'></div><span id ='desc_mpTouring'>TOURING</span></div>
     </div>
   </div>
   <div class ='_mpProfile_holder escort_profile_form'>
    <message></message>
    <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
      <h3>ESCORT PROFILE INFORMATION</h3>
      <p>Fill information for your escort’s profile below. You can go back and make changes at any time during the set up process. </p>
      <p>Saved and completed profiles go into moderation for approval before going live to the site.</p>
      <p><b>If you have any issues creating or editing your profiles, please contact My Playmate on 1300 769 766</b></p>

      <form role="form" class ='profileForm' controller ='busSaveBasic' ng-submit = 'saveEscortInfo()' id = 'escortInfo' ng-model = 'basicInfoForm'>
       <div class ='mp_image_float' >

        <div id="container_image">
         <img ng-src="{{Config::get('constants.CDN')}}/escorts/@{{main_image_src}}" alt="Your Profile Image" />
       </div>

     </div>
     <div class ='right_float'>
      {{--Form::hidden('main_image',$main_image->filename,array('ng-model'=>'basicInfoForm.image','id' => 'image')) --}}

      {{ Form::text('escort_name',$escort->escort_name,array('id' => 'escort_name', 'class'=>'escort_name input-xlarge input03','placeholder'=>'Escort Name','ng-model'=>'basicInfoForm.escort_name', 'disabled')) }}

      {{ Form::text('website','',array('id' => 'website', 'class'=>'input-xlarge input03','placeholder'=>'Your Private Website','ng-model'=>'basicInfoForm.own_url')) }}


      {{ Form::text('suburb_name',$escort->suburb_name,array('id' => 'suburb_name', 'class'=>'hidden','ng-model'=>'basicInfoForm.suburb_name')) }}
      <span class ='info'>Escort Service area: @{{basicInfoForm.suburb_name}}</span>
      <autocomplete formmodel ="basicInfoForm"></autocomplete>

    </div>
    <div class ='clear'>                    
     <label id ='escort_age' class ='form_required'>Age:</label>{{ Form::select('age_id',$ages,$escort->age_id,array('id' =>'age_id','class'=>'form-control','ng-model'=>'basicInfoForm.age_id')) }}
     <div class="control-group" id="sex">
      <label class ='form_required left_margin_15'>Gender</label>
      {{ Form::radio('basicInfoForm.gender_id',1,$escort->gender_id == 1 ? 1 : 0,array('id'=>'gender_id-1','ng-model' => 'basicInfoForm.gender_id')) }}
      {{ Form::label('gender_id-1','Female') }}
      {{ Form::radio('basicInfoForm.gender_id',2,$escort->gender_id == 2 ? 1 : 0,array('id'=>'gender_id-2','ng-model' => 'basicInfoForm.gender_id')) }}
      {{ Form::label('gender_id-2','Male') }}
      {{ Form::radio('basicInfoForm.gender_id',3,$escort->gender_id == 3 ? 1 : 0,array('id'=>'gender_id-3','data-parsley-required','ng-model' => 'basicInfoForm.gender_id')) }}
      {{ Form::label('gender_id-3','Transexual') }}
    </div>
  </div>

  <div class="alignc">
    {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

  </div>
  {{ Form::close() }}
</div>

<div class ='mPtab' id ='tab_mpProfileDetails'>
 <h3>PROFILE DETAILS</h3>
 <form role="form" controller ='profileDetails' ng-submit = 'saveEscortProfile()' id = 'profileDetails' ng-model = 'profileDetails' class ='profileForm normalSelects'>

  <div class ='col-md-6'>

    {{ Form::label('body_id','Body Type',array('class'=>'control-label')) }}

    {{ Form::select('body_id',$bodies,$escort->body_id,array('class'=>'form-control','ng-model'=>'profileDetails.body_id')) }}

  </div>

  <div class ='col-md-6'>

    {{ Form::label('ethnicity_id','Ethnicity',array('class'=>'control-label')) }}

    {{ Form::select('ethnicity_id',$ethnicities,$escort->ethnicity_id,array('class'=>'form-control','ng-model'=>'profileDetails.ethnicity_id')) }}

  </div>

  <div class ='col-md-6'>

    {{ Form::label('eye_color_id','Eye Colour',array('class'=>'control-label')) }}

    {{ Form::select('eye_color_id',$eyeColors,$escort->eye_color_id,array('class'=>'form-control','ng-model'=>'profileDetails.eye_color_id')) }}

  </div>
  <div class ='col-md-6'>

    {{ Form::label('hair_color_id','Hair Colour',array('class'=>'control-label')) }}

    {{ Form::select('hair_color_id',$hairColors,$escort->hair_color_id,array('class'=>'form-control','ng-model'=>'profileDetails.hair_color_id')) }}

  </div>
  {{ Form::label('heightLabel','Choose your height',array('id' => 'heightLabel','class'=>'control-label centeredLabel headerLabel')) }}
  <div class="col-md-12 mobile-height-wrapper">
    <div class="mobile-height-container">
      <select id="mobile-height-select">
        <?php for ($i=150; $i <= 190; $i++): ?>
          <option value="<?php echo $i ?>"><?php echo $i; ?>cm</option>
        <?php endfor; ?>
      </select>
    </div>
  </div>
  <div class ='col-md-8'>
   <div id ='heightLabelContainer'>
    <div id="heightSelectlabel"></div>
  </div>
  <div id ='heightContainer'>
    <div id ='heightSelect'></div>
  </div>
</div>
<div class ='col-md-4 heightInfo'>
  You're height is: <span class ='heightReadable'>{{$escort->height}}cm</span>
</div>


{{ Form::text('height',$escort->height, array( 'id' => 'height','class'=>'form-control hidden','ng-model'=>'profileDetails.height')) }}


<div class ='col-md-2'>
 {{ Form::label('servicesLabel','Services',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
</div>
<div class ='col-md-10 serviceArray'>
 <?php $holder = '{{service.name}}'; ?>
 <div ng-repeat="service in services">
  <div class ='col-md-3'>
   {{ Form::checkbox('services','', false, array('checklist-model'=>"profileDetails.services.escortServices", 'checklist-value'=>'service.id','class'=>'checkbox')) }}
   {{ Form::label('serviceLabel',$holder,array('class'=>'control-label serviceLabel')) }}
 </div>
</div>
</div>

<script>
  window.services = [],
  window.escortServices = [];
  @if ( $services != 0 ) 
  @foreach ( $services as $key => $service )
  @if( is_array( $service ))
  escortServices.push( "{{$key}}" );
  services.push( { id: "{{$key}}", name: "{{$service[1]}}" } );
  @else 
  services.push( { id: "{{$key}}", name: "{{$service}}" } );
  @endif
  @endforeach
  @endif
</script>

<div class ='col-md-2'>
 {{ Form::label('servicesLabel','Languages Spoken',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
</div>
<div class ='col-md-10 serviceArray'>
 <?php $holder = '{{language.name}}'; ?>
 <div ng-repeat="language in languages">
  <div class ='col-md-3'>
   {{ Form::checkbox('languages','', false, array('checklist-model'=>"profileDetails.languages.escortLanguages", 'checklist-value'=>'language.id','class'=>'checkbox')) }}
   {{ Form::label('serviceLabel',$holder,array('class'=>'control-label serviceLabel')) }}
 </div>
</div>
</div>
<script>
  window.languages = [],
  window.escortLanguages = [];
  @if ( $languages != 0 ) 
  @foreach ( $languages as $key => $language ) 
  @if( is_array( $language ))
  escortLanguages.push( "{{$key}}" );
  languages.push( { id: "{{$key}}", name: "{{$language[1]}}" } );
  @else 
  languages.push( { id: "{{$key}}", name: "{{$language}}" } );
  @endif
  @endforeach
  @endif
</script>

<?php 
$about = addslashes ( $escort->about_me);
$about = htmlentities ( $about );
?>

{{ Form::label('servicesLabel','Escort Bio',array('id' => 'serviceLabel','class'=>'control-label centeredLabel headerLabel')) }}
<p>Bios give your escort’s profiles a little more character, this lets your clients know a little bit about the escort’s personality before they decide. </p>
<p>Bios should be at least 75 words and no more than 200. </p>
<p><b>(please note, all text will be checked for duplicate content. If duplicates are found your profile may be disapproved)</b></p>
{{ Form::textarea('about_me',$about, array( 'id' => 'about_me','class'=>'form-control','ng-model'=>'profileDetails.about_me')) }}

<div class="alignc next-previous">  
	<input class="submit-button previous-button" type="submit" value="PREVIOUS">
  {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

</div>
{{ Form::close() }}
</div>
<div class ='mPtab' id = 'tab_mpAvailability'>
  <form role="form" ng-controller='availaiblityform' ng-submit = 'saveEscortAvailability()' id = 'availabilityForm' ng-model = 'profileAvailabilites' class ='profileForm normalSelects'>
   <h3>AVAILABILITY</h3>
   <p>Enter the specific availabilities to your escort. Available days, start and finish times. You can amend these anytime you need. </P>
     <table class ='profileTable availTable responsiveAvail'>
		<thead>
		<th>DAY</th>                    
		<th style="padding-left:0px">
			<div class="row no-gutters">
				<div class="col-md-12 no-gutters">
					<div class="col-md-6 col-sm-6 no-gutters">
						START TIME
					</div>
					<div class="col-md-6 col-sm-6">
						END TIME
					</div>
				</div>
			</div>					
		</th>                                        
		</thead>
		<tbody>
			<?php
			$holder = '{{avail.handle}}';
			$start_hour = '{{avail.start_hour_id}}';
			$index = '{{$index}}';
			?>

			<tr ng-repeat="avail in availabilites.val">							
				<td>
					<div class="col-md-6 col-sm-4">
						{{ Form::checkbox('days','', false, array('changefunc'=>"availableDaysChange",'onfinishavailability','checklist-model'=>"profileAvailabilites.availabilites.avail", 'checklist-value'=>'avail.day','class'=>'checkbox availDayCheck roundedOne')) }}
						{{ Form::label('Available Days',$holder,array('class'=>'control-label availDaysLabel')) }}
					</div>
					<div class="col-md-6 col-sm-6">
						{{ Form::checkbox('24','', false, array('changefunc'=>"availableDaysChange",'checklist-model'=>"profileAvailabilites.availabilites.twenty_four_hours", 'checklist-value'=>'avail.day','class'=>'checkbox twenty_four_check roundedOne')) }}
						{{ Form::label('serviceLabel','All Day',array('class'=>'control-label serviceLabel ')) }}
					</div>
				</td>
				<td>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6 col-sm-6">
								{{ Form::select('start_hour_id',$availabilites['time'],$start_hour,array('class'=>'form-control','ng-model'=>'profileAvailabilites.availabilites.start_hour_id[$index +1]')) }}                                                          
							</div>
							<div class="col-md-6 col-sm-6">
								{{ Form::select('end_hour_id',$availabilites['time'],$start_hour,array('class'=>'form-control','ng-model'=>'profileAvailabilites.availabilites.end_hour_id[$index +1]')) }}                                
							</div>
						</div>
					</div>
				</td>                            
			</tr>
		</tbody>
	</table>
<div class ='col-md-10 col-md-offset-1'>
<div class ='row'>					
	<div class ='col-md-6'>

		{{ Form::label('In Calls','In Calls',array('class'=>'control-label headerLabel')) }}
		<div class="rates-radiogroup">
			<input id='incall-yes' type = 'radio' value = 1 ng-model ='profileAvailabilites.does_incalls' class = 'radio roundedOne'>
			{{ Form::label('In Calls','Yes',array('class'=>'control-label radioLabel')) }}
			<input id='incall-no' type = 'radio' value = 0 ng-model ='profileAvailabilites.does_incalls' class = 'radio roundedOne'>
			{{ Form::label('In Calls','No',array('class'=>'control-label radioLabel')) }}
		</div>
	  <table ng-class="{'profileTable ratesTable' : (profileAvailabilites.does_incalls == 1), 'profileTable ratesTable disabledTable' : (profileAvailabilites.does_incalls == 0) }" id ='inCallTable'>
			<thead>
		<th>INCALL SERVICES</th>
			</thead>
			<tbody>
				<?php
				$holder = '{{rate}}';
				$start_hour = '{{avail.start_hour_id}}';
				$index = '{{$index}}';
				?>
				<tr style="height:20px"></tr>
				<tr ng-repeat="( name, rate ) in incalllist">										
					<td>
						<div class="row no-gutters">
							<div class="col-sm-6 no-gutters">
								<div class="col-sm-3">
									<span>$</span>
								</div>
								<div class="col-sm-9">
									{{ Form::text('rates','',array('id' => 'callrate', 'placeholder'=>'0.00', 'class'=>'form-control','ng-model'=>'profileAvailabilites.rates[name]')) }}
								</div>
							</div>
							<div class="col-sm-6">
								{{ Form::label($holder,$holder,array('class'=>'control-label-min')) }}
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class ='col-md-6'>

		{{ Form::label('Out Calls','Out Calls',array('class'=>'control-label headerLabel')) }}
		<div class="rates-radiogroup">
			<input id='outcall-yes' type = 'radio' value = 1 ng-model ='profileAvailabilites.does_outcalls' class = 'radio roundedOne'>
			{{ Form::label('Out Calls','Yes',array('class'=>'control-label radioLabel')) }}
			<input id='outcall-no' type = 'radio' value = 0 ng-model ='profileAvailabilites.does_outcalls' class = 'radio roundedOne'>
			{{ Form::label('Out Calls','No',array('class'=>'control-label radioLabel')) }}                            
		</div>
		<table ng-class="{'profileTable ratesTable' : (profileAvailabilites.does_outcalls == 1), 'profileTable ratesTable disabledTable' : (profileAvailabilites.does_outcalls == 0) }" id ='outCallTable'>
			<thead>
			<th>OUTCALL SERVICES</th>
			</thead>
			<tbody>
				<?php
				$holder = '{{rate }}';
				$start_hour = '{{avail.start_hour_id}}';
				$index = '{{$index}}';
				?>
				<tr style="height:20px"></tr>
				<tr ng-repeat="(name, rate) in outcalllist">
					<td>
						<div class="row no-gutters">
							<div class="col-sm-6 no-gutters">
								<div class="col-sm-3">
									<span>$</span>
								</div>
								<div class="col-sm-9">
									{{ Form::text('rates','',array('id' => 'callrate', 'placeholder'=>'0.00', 'class'=>'form-control','ng-model'=>'profileAvailabilites.rates[name]')) }}													
								</div>
							</div>
							<div class="col-sm-6">
								{{ Form::label($holder,$holder,array('class'=>'control-label-min')) }}
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<?php 
$notes = addslashes ( $escort->notes);
$notes = htmlentities ( $notes );?>
<?php // <script>window.notes = '{{$notes}}';</script>?>
{{ Form::label('Additional Rates Information','Rates',array('class'=>'control-label headerLabel centeredLabel')) }}
<p>If you have any additional information that you would like to provide, please include it here.</p>
{{Form::textarea('notes',$notes,array( 'ng-model' =>'profileAvailabilites.notes'))}}


<script>
  window.outCallList = [];
  window.inCallList = [];
  @if ( $outCallList )
  @foreach ( $outCallList as $key => $rate )
  window.outCallList.push( { id: '{{$key}}', name: '{{$rate}}' } );         
  @endforeach
  @endif
  @if ( $inCallList )
  @foreach ( $inCallList as $key => $rate )
  window.inCallList.push( { id: '{{$key}}', name: '{{$rate}}' } );         
  @endforeach
  @endif
</script>


<div class="alignc next-previous">  
  <input class="submit-button previous-button" type="submit" value="PREVIOUS">
  {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

</div>
{{ Form::close() }}
</div>
</div>
<div class ='mPtab' id = 'tab_mpGallery'>
  <h3>BANNER</h3>
  <form role="form" ng-controller ='galleryController' ng-submit = 'saveEscortGallery()' id = 'galleryForm' ng-model = 'profileGallery' class ='profileForm normalSelects'>
    <div id ='_cropSquare'>
      <div ngf-drop="" ngf-pattern="image/*" class="cropArea">
        <img-crop image="picFile  | ngfDataUrl" 
        result-image="croppedDataUrl" 
        ng-init="croppedDataUrl=''" 
        area-type="rectangle"
        aspect-ratio="3"
        on-load-begin="imageUpload()"
        on-change="imageChanged(image)"
        result-image-size="{ w:1200, h:400 }"
        on-load-error= "handleError()"
        ></img-crop>
      </div>
      <div id = "bannerUploadStatus"></div>
      <div id ='cropperInstructions'>
        Select the area you would like to use for your banner.  When you are done click the button below.
        <input class="submit-button" type="button" value="FINISH" ng-click = "upload()">
      </div>
    </div>
    <p>This is an optional image that will be displayed at the top of your profile page.  It must be a minimum width of 1600px.<br>
     This image must be a very wide style landscape shot.  If you need help with this, please contact Angie on 1300 769 766</p>

     <button id ="bannerUpload" ngf-drop = "handleFileSelect($file)" ngf-select="handleFileSelect($file)"></button>
     <div ng-click ='deleteBanner()' ng-class ="{'hidden' : ( !profileGallery.banner_image ),'submit-button' : ( profileGallery.banner_image ), }" id ='bannerDelete'>Delete Banner</div>


     <h3>GALLERY</h3>
     <p>Upload and display a maximum of 10 images at a time. Delete, upload and refresh your images at any time. Drag and drop to rearrange photo order. Uploaded images will automatically set as pending until approved by My Playmate administration. </p>
     <p><b>Images need to be sexy, tasteful and of very good quality; no poor grainy images. Pornographic styled images will not be approved. </b></p>
     <?php $mainImageFile = ( is_object ( $main_image ) ? $main_image->filename : '' ); ?>
     <?php $class = "{{image.filename == '" . $mainImageFile . "'?'main_image':''}}";?>

    @include('dashboard.partials.escort-gallery')

     <span class="progress ng-hide" ng-show="progress >= 0">
      <div style="width:%" ng-bind="progress + '%'" class="ng-binding">%</div>
    </span>
    <span ng-show="result" class="ng-hide">Upload Successful</span>
    <span class="err ng-binding ng-hide" ng-show="errorMsg"></span>

    {{ Form::text('main_image','',array('class'=>'form-control hidden','ng-model'=>'profileGallery.main_image')) }}
    <p>To select a profile image, hover over the image you want, then click the ‘Make main image’ bar.</p>
    <div class="alignc next-previous">  
	<input class="submit-button previous-button" type="submit" value="PREVIOUS">
      {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

    </div>
  </form>
</div>

<div class ='mPtab' id = 'tab_mpTouring' ng-init = 'scopeInit()'><h3>TOURING</h3>
  <form role="form" ng-submit = 'saveEscortTouring()' ng-model = 'profileTouring' class ='profileForm normalSelects'>
   <p class="danger">If you’re planning on venturing to a new city to see clients, post it here!</p>
   <p class="danger">This way any new clients can see that you’re in their area and can contact you to see you.</p>

   <p class="danger">For example: if you are located in Sydney but are visiting Brisbane in 2 weeks time, you can enter the start and end dates of when you will be in Brisbane. Then, anyone that searches Brisbane as their location will find you in Brisbane and be able to book time with you for the dates you will be there.</p>

   <p class="danger">This then gives you the bonus of being displayed on 2 different city pages at once – taking bookings for when you get to your visiting city or back to your home town!</p>
   <div class="row">
    <div class="col-md-12">
      <div class="triggerAnimation animated fadeInUp" data-animate="fadeInUp" style="">

       <h3>CURRENT DATES</h3>

       <table class="profileTable datesTable" style="text-align:left;" id="touringTable">
        <tbody>
          <tr>
            <th>Date From</th>
            <th>Date To</th>
            <th>Location</th>
            <th>Action</th>
          </tr>

          <tr class="touringEntry" id="touringEntry-@{{ tour.id }}" ng-repeat="tour in touring">
            <td class="from_date">@{{ tour.from_date}}</td>
            <td class="to_date">@{{ tour.to_date }}</td>
            <td class="suburb_id">@{{ tour.suburb_name }}</td>
            <td><a class="deleteTouring" id="@{{ tour.id }}"ng-click ='deleteTouring(tour.id)'>Delete</a></td>
          </tr>

        </tbody>
      </table>
      <div id="touring-form">
        <h3>Add Touring dates</h3>
        <div class ='leftFloat'>
          <label for="from_date" class="control-label">From Date<span>*</span></label>
          <input id="from_date" required name="from_date" type="text" class="form-control datepicker" placeholder="Select date" ng-model ="profileTouring.from_date">
        </div>
        <div class ='leftFloat'>
          <label for="to_date" class="control-label">To Date<span>*</span></label>
          <input id="to_date" required name="to_date" type="text" class="form-control datepicker" placeholder="Select date" ng-model="profileTouring.to_date">
        </div>    
        <div class ='clear'></div>  
        <span class ='info'>Touring suburb: @{{profileTouring.suburb_name}}</span>
        <input id="touringLatitude" class="hidden" ng-model="profileTouring.latitude">
        <input id="touringLongitude" class="hidden" ng-model="profileTouring.longitude">
        <input id="touringSuburb" class="hidden" ng-model="profileTouring.suburb_name">
        <autocomplete formmodel ="profileTouring" formval =""></autocomplete>
      </div>
      <br>
      <div class="elements touring-btn-container">
        <div class="controls touring-btn">
          <button id="addTouring" class="submit-button">Add</button>
        </div>
      </div>
    </fieldset>
  </form>

  <div ng-class = "{'overlay' : ( plan.touring == '0' ) }">
  </div>
</div></div>
<style>
	.panelFixed{z-index:0!important}.modal p{border:none!important;text-align:justify!important}.modal{display:none;position:fixed;z-index:1;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:#000;background-color:rgba(0,0,0,.4)}.modal-content{max-width:800px;z-index:15;background-color:#fefefe;margin:25% auto;padding:20px;border:1px solid #888;width:80%}.close{color:#aaa;float:right;font-size:40px;font-weight:700;padding-left:20px}.close:focus,.close:hover{color:#000;text-decoration:none;cursor:pointer}			
	.escort_profile_form .alignc{text-align:center}.escort_profile_form #availabilityForm .alignc{margin-bottom:10px!important}.escort_profile_form #availabilityForm .alignc .submit-button,.escort_profile_form #galleryForm .alignc .submit-button{margin-bottom:0!important}.escort_profile_form .alignc .submit-button{margin-top:5px!important;margin-bottom:30px!important;display:inline-block!important}.escort_profile_form .previous-button{background:#0866c6!important}@media screen and (max-width:1024px){.escort_profile_form #profileDetails{margin-top:-40px}}
</style>
<script>
$(document).delegate('form', 'submit', function(event) {
	navigateProfile(this);
});

var action = '';

$(document).on("click", ":submit", function(e){				
	action = $(this).val();				
	if(action.toUpperCase() == '')
		action = 'NEXT';
});

function navigateProfile(control) {

	var $form = $(control);
	var id = $form.attr('id');
	
	if(action == 'NEXT') {	
		if(id == 'escortInfo') {
			$('#mpBasicInfo').removeClass('active');
			$('#mpProfileDetails').addClass('active');
			$('#tab_mpBasicInfo').removeClass('activeTab');
			$('#tab_mpProfileDetails').addClass('activeTab');
		} else if(id == 'profileDetails') {					
			$('#mpProfileDetails').removeClass('active');
			$('#mpAvailability').addClass('active');				
			$('#tab_mpProfileDetails').removeClass('activeTab');
			$('#tab_mpAvailability').addClass('activeTab');
			initDetails();
		} else if(id == 'availabilityForm') {
			$('#mpAvailability').removeClass('active');
			$('#mpGallery').addClass('active');					
			$('#tab_mpAvailability').removeClass('activeTab');
			$('#tab_mpGallery').addClass('activeTab');
			$('#mpGallery').click(); //reload plugin
		} else if(id == 'galleryForm') {					
			$('#mpGallery').removeClass('active');
			$('#mpTouring').addClass('active');					
			$('#tab_mpGallery').removeClass('activeTab');
			$('#tab_mpTouring').addClass('activeTab');
		}
	} else if (action == 'PREVIOUS') {
		if(id == 'profileDetails') {					
			$('#mpProfileDetails').removeClass('active');
			$('#mpBasicInfo').addClass('active');				
			$('#tab_mpProfileDetails').removeClass('activeTab');
			$('#tab_mpBasicInfo').addClass('activeTab');
		} else if(id == 'availabilityForm') {
			$('#mpAvailability').removeClass('active');
			$('#mpProfileDetails').addClass('active');					
			$('#tab_mpAvailability').removeClass('activeTab');
			$('#tab_mpProfileDetails').addClass('activeTab');
		} else if(id == 'galleryForm') {					
			$('#mpGallery').removeClass('active');
			$('#mpAvailability').addClass('active');					
			$('#tab_mpGallery').removeClass('activeTab');
			$('#tab_mpAvailability').addClass('activeTab');
			initDetails();
		}
	}
	
	$('#dashOverlay').hide();
	$('.spinner').hide();	
}	
if(plan.name.indexOf('Touring') ==-1)
{
	$('#mpTouring').hide();
	$('#desc_mpTouring').hide();
	$('#tab_mpTouring').hide();				
}

initDetails();
			
function initDetails() {				
	$("#incall-yes").prop('checked') ? $('#inCallTable').show() : $('#inCallTable').hide()
	$("#outcall-yes").prop('checked') ? $('#outCallTable').show() : $('#outCallTable').hide()
	$('input[id="callrate"]').each(function() {						
		if($(this).val() == '0.00')
			$(this).val('');
		$(this).change(function() { 
			$(this).val(parseFloat($(this).val()).toFixed(2));						
			if(isNaN($(this).val()))
				$(this).val('');
		});
		$(this).keypress(function(event) { if (((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) && !validateQty(event)) { event.preventDefault(); } });
		$(this).attr('maxlength','7');
	});
}

function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;
	if (event.keyCode == 8 || event.keyCode == 46
	 || event.keyCode == 37 || event.keyCode == 39) {
		return true;
	}
	else if ( key < 48 || key > 57 ) {
		return false;
	}
	else return true;
};

$("#incall-no").change(function() 	{	initDetails(); });
$("#incall-yes").change(function() 	{ 	initDetails(); });
$("#outcall-no").change(function() 	{	initDetails(); });
$("#outcall-yes").change(function() { 	initDetails(); });

reloadCode();

function reloadCode() {	
	if (window.location.hash.substr(1) == "/newEscort") {
		window.location.hash = '';				
		setTimeout(function(){ $('.escortContainer').children().last().children()[0].click(); $('#mpProfileDetails').click(); }, 100);		
	}
}

 $( '.datepicker').datepicker({
  minDate: 0,
  dateFormat: "dd-mm-yy",
  beforeShowDay: function(date){
    var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
    return [ blockedDates.indexOf(string) == -1 ]
  }
});
 $( '#overlayBackGround').on('click',function() {
   $( '#_cropSquare').hide();
   $( this ).hide();
 })
 $('#mobile-height-select').on('change',function(){
  var selected = $(this).find(':selected').val();
  $('#height').val(selected).trigger('input');
});
 $( '.datepicker').datepicker({
  minDate: 0,
  dateFormat: "dd-mm-yy"
});
 $( '#overlayBackGround').on('click',function() {
   $( '#_cropSquare').hide();
   $( this ).hide();
 })
 var initialValue= 0, min=150, max=190;

 function toFeet(n) {
  var realFeet = ((n*0.393700) / 12);
  var feet = Math.floor(realFeet);
  var inches = Math.round((realFeet - feet) * 12);
  return feet + "'" + inches + '"';
}

var initialValue=0, min=150, max=190;

$( "#heightSelect" ).slider({
  value:initialValue,
  min: min,
  max: max,
  step: 1,
  slide: function( event, ui ) {
    $("#height").val( ui.value );
    $( ".heightReadable").text(ui.value + 'cm ( ' + toFeet( ui.value) + ' )');
    $('#height').trigger('input');
    var imperial = toFeet( ui.value );
         // console.log(imperial);
         $("#heightSelectlabel").text(ui.value + 'cm');
         $("#heightSelectlabel").css("margin-left", (ui.value-min)/(max-min)*100+"%");
       }
     });
$( "#heightSelect" ).draggable();
$("#height").val(initialValue);
$("#heightSelectlabel").text(initialValue + 'cm ( ' + toFeet( initialValue ) + ' )');
$("#heightSelectlabel" ).css("margin-left", (initialValue -min)/(max-min)*100+"%");

  $( '.mPProfileTopMenu div').on('click',function(e) {
    var tabName = $( this ).attr('id');
    if ( tabName == undefined ){
         var tabName = $( this ).children('div').attr('id');
    }
    $( '.mPtab').removeClass( 'activeTab');
    $( '#tab_' + tabName ).addClass( 'activeTab');
    $( '.mPProfileTopMenu div').removeClass( 'active');
    $( this ).addClass( 'active');
    if ( $( window ).width() <= 1024 ) {
      $( '.mPProfileTopMenu' ).hide();
      $( '#navigateThroughProfile' ).show();
    }
    initDetails();
    e.stopPropagation();
  })

var postObject = {
 userId: id,
 csrf: $( '#_token').val(),
 businessId: currentEscort
},
cropTypes = {
 widescreen : true,
 letterbox: false,
 free: false
};
$('#heightSelect').draggable();
</script>

