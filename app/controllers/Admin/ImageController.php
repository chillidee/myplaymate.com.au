<?php
namespace Admin;
use Controller, View, Input, Auth, Mail, Config, App;
use Escort, EscortImage, EmailTemplate, Business;

class ImageController extends Controller
{
	public function pendingImagesIndex()
	{
		$bodyClass = 'pending-images';
		$imgs = EscortImage::whereStatus(1)->orderBy('escort_id','DESC')->get();
		$escortsBanners = Escort::where('banner_approved','=','1' )->where('banner_image','!=','' )->get(['banner_image','id','escort_name','banner_approved']);
		$businessBanners = Business::where('banner_image','!=','' )->where('banner_approved','==','1')->get(['banner_image','id','name']);
		$banners = array();
		foreach ( $escortsBanners as $banner ){
			$banner->src = Config::get('constants.CDN') . '/escorts/' . $banner->banner_image;
			$banner->name = $banner->escort_name;
			$banner->type = 'Escort';
			$banners[] = $banner;
		}
		foreach ( $businessBanners as $banner ){
			$banner->src = Config::get('constants.CDN') . '/businesses/' . $banner->banner_image;
			$banner->type = 'Business';
			$banners[] = $banner;
		}
		return View::make('admin.images.pending',compact('bodyClass','imgs','banners', 'escortsBanners', 'businessBanners'));
	}

	public function showFullSizeImage()
	{
		return EscortImage::find(Input::get('image_id'))->filename;
	}

	public function approveImage()
	{
		$image = EscortImage::find(Input::get('image_id'));
	 	$image->status = 2;
		$image->approved_by = Auth::user()->id;
		$image->approved_at = date('Y-m-d H:i:s');
		$image->save();
	}

	public function disapproveImage()
	{
		$image = EscortImage::find(Input::get('image_id'));
		$image->status = 3;
		$image->disapproved_by = Auth::user()->id;
		$image->disapproved_at = date('Y-m-d H:i:s');
		$image->disapprove_message = Input::get('disapprove_message');
		$image->save();
	}

	public function sendImagesReport()
	{
		$input = Input::all();

		$escort = Escort::find($input['escort_id']);
		if ( $escort->under_business == 1 ){
			$business = Business::find($escort->business_id);
			$escort->email = $business->email;
		}
        $data['full_name'] = $escort->escort_name;
        $data['email'] = ( !App::environment('development') ? $escort->email : Config::get('constants.DEV_EMAIL') );
        $emailTemplate = EmailTemplate::where('name', 'send-images-report')->first();

        // Send activation email to new user and to admin
        Mail::send('emails.send-images-report', ['data' => $data,'input' => $input,'escort'=>$escort], function ($message) use($escort) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Image Approval Report')
                    ->to($escort->email);
                  //  ->to('che@chillidee.com.au');
        });
	}
	public function sendBannerReport() {
		$input = Input::all();
		$reciever = $input['type']::find($input['id']);
		$reciever->banner_approved = $input['action'];
		if ( $reciever->under_business == 1 ){
			$business = Business::find($reciever->business_id);
			$reciever->email = $business->email;
		}
		$reciever->save();
        $data['full_name'] = $input['name'];
        $data['email'] = ( !App::environment('development') ? $reciever->email : Config::get('constants.DEV_EMAIL') );
        $emailTemplate = EmailTemplate::where('name', 'send-images-report')->first();

        if ( $input){
                  if ( $input['action'] == 3 ){
        // Send activation email to new user and to admin
        Mail::send('emails.banner-report', ['data' => $data,'input' => $input,'escort'=>$reciever], function ($message) use($reciever) {
            $message->from(Config::get('app.adminEmail'), 'My Playmate - Admin')
                    ->subject('My Playmate Image Approval Report')
                    ->to($reciever->email);
                  //  ->to('che@chillidee.com.au');
        });
          }
        return $input;
    }
	}
}
