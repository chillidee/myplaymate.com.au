var formContainer   = jQuery('#form'),
	spinner 		= jQuery('.spinner'),
	animTime   		= 300;

function setIconsClass(clicked)
{
	$('.process-item')
		.removeClass('active')
		.removeClass('past')
		.each(function() {
			var $this = $(this);
			if (isBefore($this,clicked))
				$this.addClass('past');
		}
	);
	clicked.addClass('active');		
}

function isBefore(checked,target)
{
	return checked.index() < target.index();
}

function wipeForm()
{
	formContainer.slideUp(animTime).empty();
	spinner.fadeIn(animTime);
}

function loadNewContent(url)
{
	wipeForm();

	$.ajax({
		url: '/profile/'+url
	})
	.done(function(data) {
		formContainer.append(data).slideDown(animTime);
		spinner.fadeOut(animTime);
		// Reactivate the click event
		// $('.process-item').on('click', changeSection);
	});
}

function changeSection(event)
{
	event.preventDefault();
	$('.process-item').off();

	setIconsClass($(this));
	loadNewContent();
}

(function($){
	// Not clickable in new escort process
	// $('.process-item').on('click', changeSection);

	$('.process-item:first').addClass('active');
	loadNewContent('newEscort');
})(jQuery);