<?php

class PhoneCall extends Eloquent
{
	protected $table = 'phone_calls';

    protected $guarded = [];

    public static $rules = [];

    public $timestamps = false;
}
