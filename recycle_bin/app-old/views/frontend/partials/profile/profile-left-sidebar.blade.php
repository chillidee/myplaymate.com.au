<aside class="col-md-3 aside aside-left triggerAnimation animated visible-lg visible-md stickyfloated fadeInLeft" data-animate="fadeInLeft">
    <ul class="aside-widgets widget clearfix">
        <li>
            <ul class="tabs">
                <li class="active help-section">
                    <a href="#tab4">Help section</a>
                </li>
            </ul>
            <!-- .tabs end -->

            <!-- .tabs-content-wrap start -->
            <div class="tab-content-wrap widget">
                <!-- #tab4 content start -->
                <div id="tab4" class="tab-content" style="display: block;">
                    <ul>
                        <li><a href="{{ route('getChangePassword') }}">Change password</a></li>
                        @if(Auth::user()->escort())
                            <li><a href="{{ url('escorts/preview/'.Auth::user()->escort()->id) }}">Profile Preview</a></li>
                        @endif
                        <li><a href="{{ route('getContact') }}">Contact us</a></li>
                        <li><a href="{{ route('logout') }}">Sign Out</a></li>
                    </ul>

                </div><!-- #tab4 content end -->

            </div><!-- .tab-content-wrap end -->
        </li><!-- .widget.help section end -->
    </ul><!-- .aside-widgets end -->


    @include('frontend.partials.sidebar.left-common-widgets')

</aside><!-- .aside.aside-left end -->
