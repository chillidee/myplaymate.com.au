<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('businesses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			// 1 = brothel, 2 = agency, 3 = massage
			$table->integer('type');
			$table->integer('user_id');
			$table->string('phone');
			$table->string('email');
			$table->string('address_line_1');
			$table->string('address_line_2');
			$table->integer('suburb_id');
			$table->string('image');
			$table->integer('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('businesses');
	}

}
