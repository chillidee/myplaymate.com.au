<?php
namespace MyPlaymate\Repositories;

use Business;

class BusinessRepository {

	public function find($id)
	{
		return Business::find($id)->toArray();
	}

	public function populateSelect()
	{
		$business_obj = Business::get();
		$businesses = array('Choose a business...');
		foreach ($business_obj as $key => $business)
		{
			$businesses[$business->id] = $business->id.' - '.$business->name;
		}

		return $businesses;
	}
}