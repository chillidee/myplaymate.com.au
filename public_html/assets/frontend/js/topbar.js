(function($){
    var searchToggle = $('#search-toggle');
    searchToggle.hover(function() {
        $(this).find('img').attr('src', '/assets/frontend/img/search-hover.png');
    }, function() {
        var $this = $(this);
        if (!$this.hasClass('active'))
            $this.find('img').attr('src', '/assets/frontend/img/search.png');
    });

    searchToggle.on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            target = $('#search-dropdown'),
            overlay = $('.overlay');
        overlay.css('background-color', 'rgba(0, 0, 0, 0.3)');

        if (target.is(':visible')){
           $this.find('img').attr('src', '/assets/frontend/img/search.png');
            target.fadeOut();
            overlay.fadeOut();
        }
        else{
           $this.find('img').attr('src', '/assets/frontend/img/search-hover.png');
            target.fadeIn();
            overlay.fadeIn().on('click',closeSearch);
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27){
            if ($('.overlay').is(':visible'))
            {
                closeSearch();
            }
        }
    });

    function closeSearch()
    {
        $('.overlay').fadeOut();
        $('#search-dropdown').fadeOut();
    }

    // $('#mpm-header-wrapper').on('submit', '#search-form', function(event) {
    //     event.preventDefault();
    //     applyFilters();
    // });
})(jQuery);
