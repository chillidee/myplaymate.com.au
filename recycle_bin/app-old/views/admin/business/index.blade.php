@extends('admin.layout')
@section('javascript')
	<script>

		jQuery(document).ready(function() {
		    oTable = jQuery('#datatable').DataTable({
		        "processing": true,
		        "serverSide": true,
		        "ajax": "/admin/ajax_businesses",
				lengthMenu: [ [10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"] ],
		        "columns": [
		            {data: 'id', name: 'id'},
		            {data: 'name', name: 'name'},
		            {data: 'suburb_id', name: 'suburb_id'},
		            {data: 'phone', name: 'phone'},
		            {data: 'email', name: 'email'},
		            {data: 'created_at', name:'created_at'  },
	            	{data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		    });
		});
	</script>
@stop
@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-money"></span></div>
    <div class="pagetitle">
        <h1>All businesses</h1>
    </div>
</div><!--pageheader-->

<div class="row">
	<a href="/admin/businesses/new" class="create-user btn btn-success btn-lg pull-right section-content">Create business</a>
</div>

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Location</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Date of creation</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

@stop



