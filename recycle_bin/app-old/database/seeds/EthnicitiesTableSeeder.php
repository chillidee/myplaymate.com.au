<?php

class EthnicitiesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('ethnicities')->truncate();

        $Ethnicities = [
            ['name' => 'Asian', 'order' => 10],
            ['name' => 'Black', 'order' => 20],
            ['name' => 'Caucasian', 'order' => 30],
            ['name' => 'European', 'order' => 40],
            ['name' => 'Latino', 'order' => 50],
            ['name' => 'Middle Eastern', 'order' => 60],
            ['name' => 'Other', 'order' => 1000],
            ['name' => 'Indian', 'order' => 45],
            ['name' => 'Eurasian', 'order' => 35],
            ['name' => 'Mixed', 'order' => 65],
            ];

        // Uncomment the below to run the seeder
        DB::table('ethnicities')->insert($Ethnicities);
    }
}
