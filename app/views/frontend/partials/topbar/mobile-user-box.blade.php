<div id="sidemenu-user" class="side-items">
    <h4>Users</h4>
    @if(Auth::check())
        <li class="welcome">Welcome, {{ Auth::user()->full_name }}</li>
        <li><a href="/wishlist">My Wish list</a></li>
        <li><a href="/user/profile">Manage profile</a></li>
        <li><a href="/logout">Logout</a></li>
    @else
        <li><a href="#login" data-toggle="modal">Login</a></li>
        <li><a href="#signup" data-toggle="modal">Sign up</a></li>
    @endif
</div>
