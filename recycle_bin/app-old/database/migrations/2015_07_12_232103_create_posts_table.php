<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('short_content');
			$table->text('content');
			$table->string('seo_url');
			$table->string('meta_title');
			$table->text('meta_description');
			$table->string('meta_keywords');
			$table->boolean('published');
			$table->integer('user_id');
			$table->timestamp('date_from');
			$table->timestamp('date_to');
			$table->integer('category_id');
			$table->string('thumbnail');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
