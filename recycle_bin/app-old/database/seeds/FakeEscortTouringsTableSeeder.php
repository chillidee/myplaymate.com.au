<?php

class FakeEscortTouringsTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1,5) as $i) {
            foreach (range(1,20) as $index) {
                DB::table('escort_tourings')
                  ->insert([
                    'escort_id'         => $index,
                    'from_date'         => $faker->dateTimeThisYear(),
                    'to_date'           => $faker->dateTimeThisYear(),
                    'suburb_id'         => $faker->numberBetween(1,13000)
                ]);
            }
        }
    

    }
}
