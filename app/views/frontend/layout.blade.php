<!DOCTYPE HTML>
<html lang="en">
<head lang="en-AU" prefix="og: http://ogp.me/ns#">
  <meta charset="UTF-8" />
  <title>{{ isset($meta) ? $meta->meta_title : '' }}</title>
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="{{ isset($meta) ? $meta->meta_description : '' }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  @if(isset($meta))
  <link rel="canonical" href="https://myplaymate.com.au{{ $_SERVER['REQUEST_URI'] }}" />
  @endif
  {{ Helpers::latestCss('build.min.css') }} 
 <?php /*---------Development Stylesheets------------------------
 ----------------------------------------------------------------
  <link rel="stylesheet" href="/assets/frontend/css/animate.css">
  <link rel="stylesheet" href="/assets/frontend/css/banner.css">
  <link rel="stylesheet" href="/assets/frontend/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/frontend/css/dropzone.css">
  <link rel="stylesheet" href="/assets/frontend/css/font-awesome.min.css">
  <link rel="stylesheet" href="/assets/frontend/css/jquery-ui.min.css">
  <link rel="stylesheet" href="/assets/frontend/css/lato.css">
  <link rel="stylesheet" href="/assets/frontend/css/pgwslideshow.min.css">
  <link rel="stylesheet" href="/assets/frontend/css/nivo-slider.css">
  <link rel="stylesheet" href="/assets/frontend/css/owlcarousel.css">
  <link rel="stylesheet" href="/assets/frontend/css/retina.css">
  <link rel="stylesheet" href="/assets/frontend/css/search-select2.css">
  <link rel="stylesheet" href="/assets/frontend/css/nivo-slider.css">
  <link rel="stylesheet" href="/assets/frontend/css/select2.min.css">
  <link rel="stylesheet" href="/assets/frontend/css/sidemenu.css">
  <link rel="stylesheet" href="/assets/frontend/css/style.css">
  <link rel="stylesheet" href="/assets/frontend/css/josephin.css">
  <link rel="stylesheet" href="/assets/frontend/css/custom.css">*/?>
    <!--[if lte IE 8]>
    <link href="{{ url('/assets/frontend/css/ie.css') }}" rel="stylesheet" />
    <![endif]-->
    @include('frontend.partials.head.favicons')
    <meta name="google-site-verification" content="p5uVVgW7ZgDrR-1N6jgNUGnr7zhjLNKBZ1aNWFwc1zo" />
    <script>$cdn = '{{Config::get('constants.CDN')}}';</script>    
    <meta property="og:locale" content="en_AU" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ isset($meta) ? $meta->meta_title : '' }}" />
    <meta property="og:description" content="{{ isset($meta) ? $meta->meta_description : '' }}"/>
    <meta property="og:url" content="{{ url('/').$_SERVER['REQUEST_URI'] }}"/>
    <meta property="og:site_name" content= "Myplaymate- Australia's leading Escort directory" />
  </head>
  <script>
      function loadCSS( href, before, media ){ 
          "use strict"; 
          var ss = window.document.createElement( "link" ); 
          var ref = before || window.document.getElementsByTagName( "script" )[ 0 ]; 
          ss.rel = "stylesheet"; 
          ss.href = href; 
          ss.media = "only x"; 
          ref.parentNode.insertBefore( ss, ref ); 
          setTimeout( function(){ 
          ss.media = media || "all"; 
          } ); 
          return ss; 
      }      
      loadCSS("https://fonts.googleapis.com/css?family=Lato%7CPlayfair+Display&display=swap");
      loadCSS("https://fonts.googleapis.com/css?family=Raleway");
      loadCSS("https://fonts.googleapis.com/css?family=Roboto+Slab");
      loadCSS("https://fonts.googleapis.com/css?family=Alice&display=swap");
      loadCSS("/assets/frontend/css/liveChanges.min.css");
	</script>
  <style>
    .modal-backdrop.in { display:none!important; }
	.advertise-button-mobile a,.advertise-button-mobile2 a,.nav-button-mobile a{font-size:20px;font-family:'Josefin Sans','Open Sans',Helvetica,Arial,sans-serif}.advertise-button-mobile a,.advertise-button-mobile2 a,.nav-button-mobile,.nav-button-mobile a{font-family:'Josefin Sans','Open Sans',Helvetica,Arial,sans-serif}.button-mobile-container{text-align:center}.advertise-button-mobile,.advertise-button-mobile2{padding:15px 30px;background-color:rgb(208,10,10,.8);text-align:center;display:inline}.advertise-button-mobile a{color:#fff}.advertise-button-mobile2 a{color:#fff;background-color:rgb(208,10,10,.8)}.nav-button-mobile{text-align:center;padding:15px 10px;margin:0 10px;display:inline}.nav-button-mobile a{color:#FFF}.topbar-buttons-style a{margin:0 10px;font-family:'Josefin Sans','Open Sans',Helvetica,Arial,sans-serif!important;text-align:right}@media (min-width:1200px){.page-title-1{margin-top:90px!important}}@media (min-width:1024px){.button-mobile-container{display:none}}@media (max-width:1200px){.page-title-1{margin-top:160px!important}}@media (max-width:1024px){#header{padding:0!important}.page-title-1{margin-top:25px!important}.advertise-button-mobile2{display:none}}@media (max-width:560px){.button-mobile-container{width:100%;margin:0 5}.advertise-button-mobile a,.nav-button-mobile a{font-size:small;margin:0 5}}
  </style>

  <body {{ isset($bodyClass) ? 'class="'.$bodyClass.'"' : '' }} onunload = "">
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

      <button class="showLeftPush subMenu"><i class="fa fa-bars"></i></button>

      <div id="sidemenu-search" class="side-items">
        <p class="selected-filters-title">Search</p>
        <form action="/escorts" method="GET" name="search">
          <input id="side-search" name="s" type="text" placeholder="Type and hit enter..." autocomplete="off"/>
        </form>
      </div>
    </nav>

    <div id="mpm-header-wrapper" class="<?php echo ($_SERVER['REQUEST_URI'] == '/') ? 'header-banner' : 'header-simple'; ?>">
      <div id="top-bar">
        <div class="container">
          <div class="hidden-xs">
            {{ StaticBlock::getHtml('social') }}                
          </div>

          @include('frontend.partials.topbar.buttons')

          @include('frontend.partials.topbar.desktop-user-box')

        </div>
      </div>
      <header id="header" class="<?php echo ($_SERVER['REQUEST_URI'] == '/') ? 'header-style-01' : 'header-style-02'; ?>">
		<!-- .container start -->
		<div class="container">
          <div id="mobile-menu" class="hidden-lg">
            <button class="arrowToggle" id ='mobileArrowToggle'></button>
          </div>

          <div id="logo">
            <a href="{{ route('home') }}">
              <img src="{{ url('assets/frontend/img/logo.png') }}" alt="Myplaymate.com.au. the place to be"/>
            </a>
          </div>
          @if(isset($city_page) || isset($escorts ) || isset($business) )
          <div id="myToggleFilters" class ="no_close"><span id="_rf_search2" class="no_close" onclick="$('#myToggleFilters').click();"></span></div>
          @endif
          @include('frontend.partials.topbar.desktop-menu')
			
		</div><!-- .container end -->	
		<div class="button-mobile-container">
				<div class="advertise-button-mobile">
					<a href="{{ url('advertise') }}" class="branding-color">Advertise With Us</a>
				</div>
				
				@if(Auth::guest())
				<div class="nav-button-mobile">
					<a href="#login" data-toggle="modal">Login</a>
				</div>
				@endif
				
				@if(Auth::guest())
				<div class="nav-button-mobile">
					<a href="#signup" data-toggle="modal">Sign Up</a>
				</div>
				@endif
				
				@if(!Auth::guest())
				<div class="nav-button-mobile">
					<a href="/logout">Logout</a>
				</div>
				@endif
				
				@if(!Auth::guest())
				<div class="nav-button-mobile">
					<a href="/user/profile">Your Account</a>
				</div>
				@endif
												
			</div>
    </header><!-- #header end -->		
  </div><!-- #header-wrapper end -->
	  
  @if( isset( $escort->about_me ) && isset( $status ) )
  <div id="m_clean" class="{{ ($_SERVER['REQUEST_URI'] == '/') ? 'homeview' : 'view'; }}">
    <section>
     @yield('content')            
   </section>
   <div id ='bottom_content'>
     <div class ='container'>
      @if( isset($escort->about_me))
      <h4>Profile information:</h4>
      <p>{{$escort->about_me}}</p>
      @endif
      @if( isset($escort->seo_content))
      <p>{{$escort->seo_content}}</p>
      @endif            
    </div>
  </div>
</div>
@elseif( isset( $brothel->about ) && isset( $status ) )
<div id="m_clean" class="{{ ($_SERVER['REQUEST_URI'] == '/') ? 'homeview' : 'view'; }}">
  <section>
   @yield('content')            
 </section>
 <div id ='bottom_content'>
   <div class ='container'>
     @if( isset($brothel->about))
     <h4>Profile information:</h4>
     <p>{{$brothel->about}}</p>
     @endif
     @if( isset($brothel->seo_content))
     <p>{{$brothel->seo_content}}</p>
     @endif            
   </div>
 </div>
</div>
@elseif(isset($city_page))
<div id="m_clean" class="{{ ($_SERVER['REQUEST_URI'] == '/') ? 'homeview' : 'view'; }}">
  <section>
    @yield('content')            
  </section>
  <div id ='bottom_content'>
   <div class ='container'>
     {{ $content }}
   </div>
 </div>
</div>
@else
<div id="m_clean" class="{{ ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/?signup=true') ? 'homeview' : 'view'; }}">
  <section>
    @yield('content')            
  </section>
</div>
@endif
<!-- Footer Area -->
<div id="footer-wrapper">
  <footer id="footer">
    <div class="container">
      <div class="row">
        <ul class="col-md-3 col-sm-6 footer-widget-container">
          {{ StaticBlock::getHtml('footer-col-1') }}

          @include('frontend.partials.footer.social')
        </ul>

        @include('frontend.partials.footer.posts')

        {{ StaticBlock::getHtml('footer-col-3') }}

        {{ StaticBlock::getHtml('footer-col-4') }}
      </div>
    </div>
  </footer>

  <div id="copyright-container">
    <div class="container">
     {{ StaticBlock::getHtml('footer-copyright') }}
   </div>
 </div>

 <a href="#" class="scroll-up">Scroll</a>
</div><!-- .footer-wrapper end -->

<div class="modal fade backdrop" id="geoMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ></div>

<div class="overlay"></div>
@include('frontend.partials.modals.login')
@include('frontend.partials.modals.signup')
@include('frontend.partials.modals.registration-success')
@include('frontend.partials.modals.welcome')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVAKU7LtUy9yWGX_uoP1cJ9kW0Hqhf754&signed_in=true&libraries=places"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

{{ Helpers::latestJs('build.min.js') }}
@if(isset($session))
<script async src="{{$session}}?{{time()}}"></script>
@endif

@yield('javascript')
<script>
  jQuery(document).ready(function($) {

    if ('{{ Session::get('loginFailed') }}' == 'failed')
      $('#login').modal('show');
    if ('{{ Session::get('registrationFailed') }}' == 'failed' || window.location.hash == '#signup'){
      $('#signup').modal('show');
    }
    if ('{{ Session::get('registrationSuccess') }}' == 'success')
      $('#registration-success').modal('show');
<?php /*
    $( 'a[href="/user/profile"]').click(function(){
      if($('#contactEscortForm')){
        console.log('hi')
      }
      if($('#contactEscortForm').css('display','block')){
        console.log('open');
        $('#contactEscortForm').css('display','none');
      }
    });*/?>

    //$( 'a[href="#signup"]').click(function(){

      //$('#mobileArrowToggle').hasClass("arrowToggleClicked") ? ($('#mobileArrowToggle').removeClass("arrowToggleClicked"), $(".navbar-nav").animate({
       // width: "0%"
      //})) : ($('#mobileArrowToggle').addClass("arrowToggleClicked"), $(".navbar-nav").animate({
      //  width: "100%"
      //}))
    //});

  });
</script>

@yield('modal')

</body>
</html>