<div id ='overlayBackGround'></div>
<div class ='_MpoffWhite businessProfileScreen'>
 <div>   
   <div class ='mobile_only' id ='navigateThroughProfile'>Show Profile Menu</div>
   <div class ='mPProfileTopMenu'>
     <div><div id ='mpBasicInfo' class ='active'></div><span>ACCOUNT</span></div>
     <div><div id ='mpProfileDetails' ng-click = 'getBusinessImages()'></div><span ng-click = 'getBusinessImages()'>PROFILE</span></div>
     <div><div id ='mpPlans'></div><span>PLAN</span></div>
   </div>
 </div>
 <div class ='_mpProfile_holder business_profile_form'>
  <message></message>
  <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
   <h3>ACCOUNT</h3>
   <p class ='text-left'>Please fill in your details below. Please note that information provided here will be available publicly on our site.</p>			
   <form role="form" controller ='busSaveBasic' ng-submit = 'saveBusBasicInfo()' id = 'businessInfo' ng-model = 'busBasicInfoForm'>

    {{ Form::hidden('image',$business->image,array('ng-model'=>'busBasicInfoForm.image','id' => 'image')) }}
	<label>Name</label>
    {{ Form::text('name',$business->name,array('id' => 'name', 'class'=>'business-name input-xlarge input03','placeholder'=>'Business Name','ng-model'=>'busBasicInfoForm.name' )) }}
	<label>Phone</label>
    {{ Form::text('phone',$business->phone,array('ng-model'=>'busBasicInfoForm.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'\b\d{3}[-.]?\d{3}[-.]?\d{4}\b','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'Business Phone')) }}
	<label>E-mail</label>
    {{ Form::text('email',$business->email,array('id' => 'email', 'class'=>'business-email input-xlarge input03','placeholder'=>'Business Email','ng-model'=>'busBasicInfoForm.email')) }}
	
    <div class ='bottom_inputs'>	
	 <span>Business Type</span>
    {{ Form::select('type',$type, '',array('required','id'=>'busType', 'ng-model' => "busBasicInfoForm.type","data-parsley-required"=>"true") )}}
	</br>
     <span>Website</span>

     {{ Form::text('website',$business->website,array('ng-model'=>'busBasicInfoForm.website','placeholder' => 'Your Website','class'=>'input-xlarge input03','id' => 'website')) }}

     <div ng-class ="{'hidden' : ( busBasicInfoForm.type == 2 )}">
       <span>Business Address</span>

       {{ Form::text('address_line_1',$business->address_line_1,array('ng-model'=>'busBasicInfoForm.address_line_1','placeholder' => 'Address line 1','class'=>'input-xlarge input03','id' => 'address_line_1')) }}

       {{ Form::text('address_line_2',$business->address_line_2,array('ng-model'=>'busBasicInfoForm.address_line_2','placeholder' => 'Address line 2','class'=>'input-xlarge input03','id' => 'address_line_2')) }}

       <span>Business Address on Google Maps</span>        
       <surburbselect suburbid = '{{ $business->suburb_id }}'></surburbselect>

       <div id="locationField">
         {{ Form::text('autocompleteField','',array('class'=>'business-name input-xlarge input03', 'id' => 'autocomplete', 'onFocus' => 'geolocate()','placeholder'=>'Start typing your address here')) }}
       </div>
       <div id="map"></div>
       <div id ='address'>
        {{ Form::text('latitude',$business->latitude,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.latitude','id' => 'latitude')) }}


        {{ Form::text('longitude',$business->longitude,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.longitude','id' => 'longitude')) }}

        {{ Form::text('suburb_name',$business->suburb_name,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.suburb_name','id' => 'suburb_name')) }}

        {{ Form::text('state',$business->state,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.state','id' => 'state')) }}

        {{ Form::text('autocompleteaddress',$business->autocompleteaddress,array('class' => 'hidden','ng-model'=>'busBasicInfoForm.autocompleteaddress','id' => 'autocompleteAddress'))}}

      </div>
    </div>
    <div ng-class ="{'hidden' : ( busBasicInfoForm.type != 2 )}">
      {{ Form::text('suburb_name','',array('id' => 'suburb_name', 'class'=>'hidden','ng-model'=>'busBasicInfoForm.suburb_name')) }}
      <span class ='info'> Agency Service area: @{{busBasicInfoForm.suburb_name}}</span>
      <autocomplete formmodel ="busBasicInfoForm"></autocomplete>
    </div>

    <div class="alignc">
      {{ Form::submit('NEXT',array('class'=>'submit-button')) }}

    </div>
    {{ Form::close() }}
  </div>
</div>
<div class ='mPtab' id ='tab_mpProfileDetails'>

 <h3>DETAILS</h3>
 <form role="form" ng-controller ='galleryController' ng-submit = 'saveprofileDetailsGal()' id = 'profileDetails' ng-model = 'profileGallery'>

   <p>A small blurb about your business that lets your clients know why you're a cut above the rest.  Maybe you have state of the art facilities, pride yourself on customer service or have some of the most stunning escorts in your area. Anything that will entice your clients to book with you.  <br><b>Please note, all text will be checked for duplicate content anywhere on the internet.  If duplicates are found your business profile may be disapproved.</b></p>

   {{ Form::textarea('about','',array('id' => 'about', 'class'=>'about input-xlarge input03','placeholder'=>'About','ng-model'=>'profileGallery.about')) }}


   <div class="alignc next-previous">  
	<input class="submit-button previous-button" type="submit" value="PREVIOUS">
    {{ Form::submit('NEXT',array('class'=>'submit-button')) }}


  </div>
  {{ Form::close() }}
</div>
<div class ='mPtab' id ='tab_mpPlans'>
  @include('dashboard.partials.plan-popup')
</div>


<?php if( !$business->latitude  ) {
  $business->latitude = '-33.8674869';
  $business->longitude = '151.20699020000006';
} ?>
			
<script>

setTimeout(function(){ initTab(); }, 100);
			
function initTab() {
	$('#_MpOverview_').removeClass('mPActiveIcon');
	$('#_MpProfile_').addClass('mPActiveIcon');
	initBusinessType();
}		

function initBusinessType() {
	$('#busType').prop('disabled', $('#busType').val() == '1' || $('#busType').val() == '2' || $('#busType').val() == '3');	
}

$( "#busType" ).click(function() {
	if($('#busType').val() != '1' && $('#busType').val() != '2' && $('#busType').val() == '3')
		$('#busType').prop('disabled', false);
});

$(document).delegate('form', 'submit', function(event) {	
	navigateProfile(this);
});

var action = '';

$(document).on("click", ":submit", function(e){				
	action = $(this).val();				
	if(action.toUpperCase() == '')
		action = 'NEXT';
});
			
function navigateProfile(control) {

	var $form = $(control);
	var id = $form.attr('id');
		
	if(action == 'NEXT') {
		if(id == 'businessInfo') {
			$('#mpProfileDetails').click();
		} else if(id == 'profileDetails') {					
			$('#mpPlans').click();
		} else {
			setTimeout(function(){ window.location.hash = "planSelected"; window.location.reload(); }, 1000);
		}
	} else if (action == 'PREVIOUS') {
		if(id == 'profileDetails') {								
			$('#mpBasicInfo').click();				
		}
	}
	$('#dashOverlay').hide();
	$('.spinner').hide();
	$('.load-container').hide();
	initBusinessType();
}	

  $( '.mPProfileTopMenu div').on('click',function(e) {
    var tabName = $( this ).attr('id');
    if ( tabName == undefined ){
         var tabName = $( this ).children('div').attr('id');
    }
    $( '.mPtab').removeClass( 'activeTab');
    $( '#tab_' + tabName ).addClass( 'activeTab');
    $( '.mPProfileTopMenu div').removeClass( 'active');
    $( this ).addClass( 'active');
    if ( $( window ).width() <= 1024 ) {
      $( '.mPProfileTopMenu' ).hide();
      $( '#navigateThroughProfile' ).show();
    }
	
	/*if($('.currentPlan').size() == 0) {
		$('div').find("[data-id=4]")[1].click();
	}*/
    
    e.stopPropagation();
  })
  var mapDiv = document.getElementById('map');
  var map = new google.maps.Map(mapDiv, {
    center: {lat: {{$business->latitude}}, lng: {{$business->longitude}}},
    zoom: 13
  });
  console.log( map );
  markers = [];

  var postObject = {
   userId: id,
   csrf: $( '#_token').val(),
   businessId: active_id
 };



// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    //  (document.getElementById('autocomplete')),
     // {types: ['geocode']});
     (document.getElementById('autocomplete')),
     {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function iosAutoCompleteFixer(){
  if($('html').width() <= 1024){
    FastClick.prototype.needsClick = function(target) {
      return true;
    };
  }
}

iosAutoCompleteFixer();

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  console.log( place );

  document.getElementById('autocompleteAddress').value = 
  place.address_components[0]['long_name'] + ' ' +
  place.address_components[1]['long_name'] + ', ' +
  place.address_components[2]['long_name'];

  document.getElementById('latitude').value = place.geometry.location.lat();
  document.getElementById('longitude').value = place.geometry.location.lng();

   // document.getElementById('postcode').value = place.geometry.location.administrative_area_level_2();

   for (var i = 0; i < place.address_components.length; i++) {
    if (place.address_components[i].types[0] == 'locality' || place.address_components[i].types[1] == 'locality') {
      $( '#suburb_name').val( place.address_components[i].short_name );
    }
    if (place.address_components[i].types[0] == "administrative_area_level_1") {
     $( '#state').val( place.address_components[i].short_name );
   }
 }
     //Fallback in case there is no locality returned by Google

     $( '#latitude' ).trigger('input');
     $( '#longitude' ).trigger('input');
     $( '#suburb_name' ).trigger('input');
     $( '#state' ).trigger('input');
     $('#autocompleteAddress').trigger('input');
     
     setMapOnAll(null);
     map.setCenter(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
     myLatlng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
     markers = new google.maps.Marker({position: myLatlng, map: map,title: {{json_encode( $business->name )}}});


   }
   function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

// [END region_geolocation]
initAutocomplete();

</script>



