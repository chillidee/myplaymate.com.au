@extends('admin.layout')

@section('javascript')

  <script>
    function setStatus(button,status)
    {
      var url,
        reviewId = button.data('review-id');
      if(status == 2){
        url = 'approve_review';
        message = 'Review approved!';
      }
      if(status == 3){
        url = 'disapprove_review';
        message = 'Review disapproved!';
      }

      jQuery.ajax({
        url: '/admin/'+url,
        data: {review_id: reviewId},
      })
      .done(function() {
          var label = button.parents('tr').find('span.label');
          label.removeClass('label-warning label-danger label-success');
          if (status == 2)
            label.addClass('label-success').text('approved');
          else
            label.addClass('label-danger').text('disapproved');
        jQuery('.alert-success').text(message).fadeIn();
      });
    }

    jQuery(document).ready(function() {
        oTable = jQuery('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax_reviews",
            "order": [[ 7, "desc" ]],
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'escort_id', name: 'escort_id'},
                {data: 'name', name: 'name'},
                {data: 'review', name: 'review', 'width':'400px'},
                {data: 'rating', name: 'rating'},
                {data: 'status', name: 'status'},
                {data: 'published', name: 'published'},
                {data: 'created_at', name:'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });


    $('body').on('click', '.approve', function(e) {
      e.preventDefault();
      setStatus($(this),2);
    });  

    $('body').on('click', '.disapprove', function(e) {
      e.preventDefault();
      setStatus($(this),3);
    });  

    });
  </script>

@stop

@section('content')
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-user"></span></div>
    <div class="pagetitle">
        <h1>Escort Reviews</h1>
    </div>
</div><!--pageheader-->

<table class="admin-table table table-bordered table-striped table-hover" id="datatable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Escort name</th>
      <th>Author name</th>
      <th>Review</th>
      <th>Rating</th>
      <th>Status</th>
      <th>Published</th>
      <th>Date of creation</th>
      <th>Action</th>
    </tr>
  </thead>
</table>

@stop