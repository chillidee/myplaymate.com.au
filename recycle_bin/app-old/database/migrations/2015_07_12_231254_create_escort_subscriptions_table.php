<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortSubscriptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escort_subscriptions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('escort_id');
			$table->integer('escort_plan_id');
			$table->boolean('trial_used');
			$table->float('credits');
			$table->timestamp('from_date');
			$table->timestamp('to_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escort_subscriptions');
	}

}
