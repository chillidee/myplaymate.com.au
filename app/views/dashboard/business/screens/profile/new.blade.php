<div class ='_MpoffWhite businessProfileScreen'>
   <div>
     <h2>New Business</h2>
         <div class ='mPProfileTopMenu hidden'>
           <div><div id ='mpBasicInfo' class ='active'></div><span>BASIC INFO</span></div>
           <div><div id ='mpProfileDetails'></div><span>PROFILE DETAILS</span></div>
           <div><div id ='mpPayment'></div><span>PAYMENT</span></div>
     </div>
   </div>
        <div class ='_mpProfile_holder'>
      <message></message>
     <div id ='tab_mpBasicInfo' class ='mPtab activeTab'>
     <h3>BUSINESS INFORMATION</h3>
    <form role="form" controller ='busSaveBasic' ng-submit = 'newBusiness()' id = 'businessInfo' ng-model = 'busBasicInfoForm'>
      <div class ='mp_image_float' >
        <div id="container_image">
          <img src ='/assets/dashboard/images/escortHolderImage.jpg' class ='full_width'>
        </div>
      </div>
        <div class ='right_float'>
                    {{ Form::hidden('image','',array('ng-model'=>'busBasicInfoForm.image', 'id' => 'image')) }}

                    {{ Form::text('name','',array('required','id' => 'name', 'class'=>'business-name input-xlarge input03','placeholder'=>'Business Name','ng-model'=>'busBasicInfoForm.name','ng-init'=>'busBasicInfoForm.name=' . json_encode( $user->full_name ) )) }}

                    {{ Form::text('phone','',array('ng-model'=>'busBasicInfoForm.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'Business Phone','ng-init'=>'busBasicInfoForm.phone=' . json_encode( $user->phone ) )) }}
 
                    {{ Form::text('email','',array('id' => 'email', 'class'=>'business-email input-xlarge input03','placeholder'=>'Business Email','ng-model'=>'busBasicInfoForm.email','ng-init'=>'busBasicInfoForm.email=' . json_encode( $user->email ) )) }}

                    {{ Form::text('suburb_name','',array('id' => 'suburb_name', 'class'=>'hidden','ng-model'=>'busBasicInfoForm.suburb_name')) }}

                    {{ Form::text('state','',array('id' => 'state', 'class'=>'hidden','ng-model'=>'busBasicInfoForm.state')) }}
        </div>
        <div class ='bottom_inputs'>
                     
                     
                     
                <?php /* <span>Business Type</span> {{ Form::select('type',$type, '',array('required','id'=>'busType', 'ng-model' => "busBasicInfoForm.type","data-parsley-required"=>"true","ng-init"=>"busBasicInfoForm.type='1'") )}}
				<span>Human readable address</span>
                    {{ Form::text('address_line_1','',array('ng-model'=>'busBasicInfoForm.address_line_1','placeholder' => 'Address line 1','class'=>'input-xlarge input03','id' => 'address_line_1')) }}

                    {{ Form::text('address_line_2','',array('ng-model'=>'busBasicInfoForm.address_line_2','placeholder' => 'Address line 2','class'=>'input-xlarge input03','id' => 'address_line_2')) }}

                    <span>Address on Google Maps</span>        
                    <surburbselect suburbid = ''></surburbselect>

        <div id="locationField">
             {{ Form::text('autocompleteField','',array('required','class'=>'business-name input-xlarge input03', 'id' => 'autocomplete', 'onFocus' => 'geolocate()','placeholder'=>'Start typing your address here')) }}
        </div>
        <div id="map"></div>
        <div id ='address'>
                    {{ Form::text('latitude','',array('class' => 'hidden','ng-model'=>'busBasicInfoForm.latitude','id' => 'latitude')) }}


                    {{ Form::text('longitude','',array('class' => 'hidden','ng-model'=>'busBasicInfoForm.longitude','id' => 'longitude')) }}


                    {{ Form::text('autocompleteaddress','',array('class' => 'hidden','ng-model'=>'busBasicInfoForm.autocompleteAddress','id' => 'autocompleteAddress'))}}*/?>



        </div>

        <div class="alignc">
            {{ Form::submit('NEXT',array('class'=>'submit-button')) }}
           <?php /* {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }} */?>

        </div>
    {{ Form::close() }}
</div>
        </div>

<script>
	setTimeout(function(){ autoSubmit(); }, 100);
	
	jQuery('form').submit(function(){
		jQuery(this).unbind('submit'); // unbind this submit handler first and ...
		jQuery(this).submit(function(){ // added the new submit handler (that does nothing)
			return false;
		});    
	});
	
	function autoSubmit() {
		if ($('#name') == '' && $('#phone') == '' && $('#email') == '') { 
			window.reload();
		} else {			
			$('.submit-button').click();
		}
	}
	
</script>
		

 <?php /* <script>

    var mapDiv = document.getElementById('map');
    var map = new google.maps.Map(mapDiv, {
      center: { lat: -33.8674869, lng: 151.20699020000006},
      zoom: 13
    });
    markers = [];

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */
    //  (document.getElementById('autocomplete')),
     // {types: ['geocode']});
 /*(document.getElementById('autocomplete')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    console.log( place );

    document.getElementById('autocompleteAddress').value = 
    place.address_components[0]['long_name'] + ' ' +
    place.address_components[1]['long_name'] + ', ' +
    place.address_components[2]['long_name'];

    document.getElementById('latitude').value = place.geometry.location.lat();
    document.getElementById('longitude').value = place.geometry.location.lng();

   // document.getElementById('postcode').value = place.geometry.location.administrative_area_level_2();

     for (var i = 0; i < place.address_components.length; i++) {
        if (place.address_components[i].types[0] == 'locality' || place.address_components[i].types[1] == 'locality') {
                $( '#suburb_name').val( place.address_components[i].short_name );
        }
        if (place.address_components[i].types[0] == "administrative_area_level_1") {
               $( '#state').val( place.address_components[i].short_name );
        }
    }
     //Fallback in case there is no locality returned by Google

    $( '#latitude' ).trigger('input');
    $( '#longitude' ).trigger('input');
    $( '#suburb_name' ).trigger('input');
    $( '#state' ).trigger('input');
    $('#autocompleteAddress').trigger('input');
    
    setMapOnAll(null);
    map.setCenter(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
    myLatlng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
    markers = new google.maps.Marker({position: myLatlng, map: map,title: 'Your Business'});


}
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]
initAutocomplete();

    </script>


  
    