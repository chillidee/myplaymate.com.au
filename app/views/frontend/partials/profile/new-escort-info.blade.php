<div id="basic-info">
    {{ Form::open(['id'=>'newEscort']) }}
        <h2 class="alignc">BASIC INFORMATION</h2>
          <p class = "profile_info">
          Please fill in your details below.
          </p>
            <div id="backend-profile-img">
                <figure class="new_escort-image">
                    <img class="img-profile" src="/assets/frontend/img/profile_template.jpg">
                </figure>
            </div>

           <div class ='right_floater width_65'>
            <div class="control-group">

        <div class="control-group">
            {{ Form::label('escort_name','Escort Name*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('escort_name','',['class'=>'','required']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('phone','Phone*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('phone','',['class'=>'','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('email','Email*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('email','',['class'=>'','data-parsley-required','data-parsley-type'=>'email']) }}
            </div>
        </div>
</div>
</div>
<div class ='bottom_section'>
           <div class="control-group left_floater age_floater">
            {{ Form::label('age_id','Age*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::select('age_id',$ages,'',['class'=>'form-control']) }}
            </div>
        </div>
        <div class="control-group left_floater">
            {{ Form::label('suburb_id','Current location*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::select('suburb_id',$suburbs,'',['class'=>'select2 form-control input-large input03','data-parsley-required','min'=>'1','data-parsley-min-message'=>'Please select a suburb.','style'=>'width:100%']) }}
            </div>
        </div>
        <div class="control-group left_floater">
            {{ Form::label('licence_number','Licence number (if applicable)',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('licence_number','',['class'=>'']) }}
            </div>
        </div>

        <div class="control-group left_floater">
            <div>
                {{ Form::radio('gender_id',1, 0,['id'=>'gender_id-1']) }}
                {{ Form::label('gender_id-1','Female') }}
            </div>
            <div>
                {{ Form::radio('gender_id',2, 0,['id'=>'gender_id-2']) }}
                {{ Form::label('gender_id-2','Male') }}
            </div>
            <div>
                {{ Form::radio('gender_id',3, 0,['id'=>'gender_id-3','data-parsley-required']) }}
                {{ Form::label('gender_id-3','Transexual') }}
            </div>
        </div>
        <div class="clear" style="clear:both;"><br /></div>

        <div class="control-group phone_contact left_floater">
            {{ Form::label('contact_phone','Contact by Phone/SMS*',['class'=>'control-label']) }}
            <div>
                {{ Form::radio('contact_phone',1,0,['id'=>'contact_phone-yes']) }}
                {{ Form::label('contact_phone-yes','Yes') }}
            </div>
            <div>
                {{ Form::radio('contact_phone',0,1,['id'=>'contact_phone-no']) }}
                {{ Form::label('contact_phone-no','No') }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('contact_playmail','Contact by PlayMail*',['class'=>'control-label']) }}
            <div>
                {{ Form::radio('contact_playmail',1, 0,['id'=>'contact_playmail-yes']) }}
                {{ Form::label('contact_playmail-yes','Yes') }}
            </div>
            <div>
                {{ Form::radio('contact_playmail',0, 1,['id'=>'contact_playmail-no']) }}
                {{ Form::label('contact_playmail-no','No') }}
            </div>
        </div>
        {{ Form::hidden('business_id','',['class'=>'business-id']) }}
        {{ Form::hidden('new_escort','',['class'=>'new-escort']) }}
        {{ Form::hidden('next_icon','escort-about',['class'=>'next-icon']) }}
        {{ Form::hidden('next_route','escortAbout',['class'=>'next-route']) }}
        {{ Form::hidden('next_type','escort',['class'=>'next-type']) }}
        <div class="alignc section-content">
            {{ Form::submit('CREATE ESCORT',['id'=>'new_escort_create']) }}
        </div>
</div>

    {{ Form::close() }}
</div>

<script>
    (function($){
            $('#new_escort_create').on('click', 'input.continue-submit', function(event) {
              alert( 'button clicked' );
        $('<input>').attr({
            'type':  'hidden',
            'name':  'continue',
            'class': 'continue'
          })
          .appendTo('#form form');
      });
        $('.back-end').on('submit', 'form', function(event) {
             alert( 'back end clicked' );
             event.preventDefault();
             var $this = $(this),
             route = $this[0].id,
             nextIcon = $('.'+$this.find('input.next-icon').val()),
             nextRoute = $this.find('input.next-route').val(),
             nextType = $this.find('input.next-type').val(),
             escortName = $this.find('input.escort-name' ),
             pass = $this.serialize();
             $( '.cc-container' ).addClass( 'cc-cover' ).show();
              $.ajax({
                 url: '/profile/newEscort',
                 type: 'POST',
                 data: pass
             })
            .done(function(id) {
               location.reload();
              /* jQuery.ajax({
                     url: '/profile/escortInfo',
                     data: {
                         escort_id:   id,
                         business_id: $( 'input[name="business_id"]').val()
                     },
                  })
                     .done(function(data) {
                       /*    $( '.cc-container' ).removeClass( 'cc-cover' ).hide();
                      populateForm(data,'escort');
                      attachPlugins();
                      activateEscortButtons();
                      goToEscortAbout();
                   //   loadForm('escortInfo','escort',id,'');
                      if(type == 'escort' && business_id != '')
                       $('input.business-id').val(business_id);
                       location.reload();
                     });*/
        });

         });
              
        $('.second-level h2').text('New escort');

        if ($('.firstLevel')) {};
    })(jQuery);
</script>