<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use MyPlayMate\Dashboard\DashboardInterface;
use MyPlayMate\Observers\EscortUserObserver;

class EscortUser extends Eloquent implements UserInterface, RemindableInterface, DashboardInterface
{

    protected $table = 'users';

    protected $fillable = [
        'fullname',
        'email',
        'active',
        'activation_code',
        'type',
        'phone',
        'password',

    ];

    public static $rules = [
        'fullname'             => 'required',
        'phone'                => 'required',
        'email'                => 'required|email',
        'password'             => 'required',
        'terms_and_conditions' => 'accepted',
        'phone'                => [
            "regex:/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/",
            'required',
        ],
    ];

    public static $messages = [
        'phone.regex' => 'Phone number must be a valid Australian phone number including your area code. Example 0299991111, 0499999999',
    ];

    public static function boot()
    {
        parent::boot();

        EscortUser::observe(new EscortUserObserver());
    }

    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function dashboardRoute()
    {
        $account = Account::where('user_id', $this->id)->with('Plan')->with('AgencyPlan')->get()->last();

        if (!empty($account) and empty($account->agency_plan->have_to_paid)) {
            return '/manage/profile/welcome';
        }

        return '/payment';
    }

    public function sendActivationEmail($message)
    {
        $emailTemplate = EmailTemplate::where('name', 'activation-code')->first();

        $templateData                    = [];
        $templateData['user']            = $this->toArray();
        $templateData['account']         = $this->account->toArray();
        $templateData['profile']         = $this->profile->toArray();
        $templateData['activation_link'] = Config::get('app.fullUrl').'users/activate?email='.$this->email.'&activation_code='.$this->activation_code;
        $templateData['activation_code'] = $this->activation_code;
        $templateData['activation_url']  = Config::get('app.fullUrl').'users/activate';
        $templateData['campaign_title']  = "";
        $emailData                       = new stdClass();
        $emailData->user                 = $this;
        $emailData->template             = $emailTemplate;

        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);

            $message->to($emailData->user->email);
        });
        Mail::send('admin.emailtemplates.templates.'.$emailTemplate->name, ['data' => $templateData], function ($message) use ($emailData) {
            $message->from(Config::get('app.adminEmail'), $emailData->template->from_name)->subject($emailData->template->subject);
            $message->to(Config::get('app.adminEmail'));
        });
    }

    public function profile()
    {
        return $this->hasOne('Profile', 'user_id');
    }

    public function account()
    {
        return $this->hasOne('Account', 'user_id');
    }

    public static function featured($id)
    {
        Profile::where('user_id', $id)->update(['featured' => 1]);
    }

    public static function unfeatured($id)
    {
        Profile::where('user_id', $id)
            ->update(['featured' => 0]);
    }
}
