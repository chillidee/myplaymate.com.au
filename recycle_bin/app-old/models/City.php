<?php

class City extends Eloquent
{
    protected $guarded = [];

    public $timestamps = false;

    public static $rules = [

    ];

    public function state()
    {
        return $this->belongsTo('State');
    }
}
