<?php

class CountriesTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('countries')->truncate();

        $countries = [
            ['id' => 1,
                    'code' => 'AU',
                    'name' => 'Australia', ],

        ];

        // Uncomment the below to run the seeder
        DB::table('countries')->insert($countries);
    }
}
