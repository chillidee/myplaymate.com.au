/**
 *  ----- ANGULAR CROPPER DIRECTIVE DEFINITION -----
 * This file defines the Angular Cropper directive as well as set initial configurations
 * for when the directive gets linked.
 */

// ----- DIRECTIVE CONTRUCTION ----- //
_gCropperModule.directive('croppable', ['$parse', '$interpolate', 'CroppableDefaults', function ($parse, $interpolate, CroppableDefaults) {
    return {
        // Since cropper can only be used on img tags
        restict: 'A',
        // Priority before attribute interpolation
        priority: 100,
        controller: 'CroppableController',
        link: {
            pre: function (scope, element, attrs, cropCtrl) {
                // We need to save the ngSrc raw value before interpolation, because when the value is undefined
                // and the cropper gets removed, on DOM cleanup the ngSrc attribute doesn't get copied over because the replaceWith won't set undefined attributes
                // therefore we need to know the initial string to manually put it back.
                cropCtrl.$srcAttr = attrs.ngSrc;
            },
            post: function (scope, element, attrs, cropCtrl) {
                // If the element is not an image cancel out
                if (element[0].tagName !== 'IMG') {
                    throw 'The croppable element must be an image tag!';
                }
                else if (!attrs.hasOwnProperty('ngSrc') && !attrs.hasOwnProperty('src')) {
                    throw 'The image tag has no ngSrc or src attribute, croppable directive can\'t function without this';
                }
                // Directives options
                var options = {};
                // If user provided an options object attribute
                if (attrs.hasOwnProperty('cropOptions') && angular.isDefined(attrs.cropOptions) && angular.isObject(scope.$eval(attrs.cropOptions))) {
                    options = angular.extend(options, scope.$eval(attrs.cropOptions));
                }
                // For each available options check if user has set an individual option in the attributes
                for (var option in CroppableDefaults) {
                    if (angular.isDefined(scope.$eval(attrs[CroppableDefaults.optionsPrefix + option.ucfirst()]))) {
                        options[option] = scope.$eval(attrs[CroppableDefaults.optionsPrefix + option.ucfirst()]);
                    }
                }
                // Create the cropper
                cropCtrl.$init(options);
                // Attach the instance to the $scope if provided a name
                if (attrs.hasOwnProperty(cropCtrl.$options('optionsPrefix') + 'Name') && angular.isString(attrs[cropCtrl.$options('optionsPrefix') + 'Name'])) {
                    cropCtrl.$name = $interpolate(attrs[cropCtrl.$options('optionsPrefix') + 'Name'])(scope)
                    $parse(CroppableDefaults.directiveName + '.' + cropCtrl.$name).assign(scope, cropCtrl);
                }
            }
        }
    };
}]);