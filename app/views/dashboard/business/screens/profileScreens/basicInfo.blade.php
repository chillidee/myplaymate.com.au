<h3>BASIC INFORMATION</h3>
<form role="form" controller ='busSaveBasic' ng-submit = 'saveBusBasicInfo()' id = 'businessInfo' ng-model = 'busbusBasicInfoForm'>
  <div class ='mp_image_float' >
    <div id="container_image"></div>
    @if ( $business->image)
    <span>Click above image to change your logo</span>
    @else
    <span>Click above image to upload your logo</span>
    @endif
  </div>
  <div class ='right_float'>
    {{ Form::hidden('image',$business->image,array('ng-model'=>'busBasicInfoForm.image','ng-init'=>"busBasicInfoForm.image='$business->image'", 'id' => 'image')) }}

    {{ Form::text('name',$business->name,array('id' => 'name', 'class'=>'business-name input-xlarge input03','placeholder'=>'Business Name','ng-model'=>'busBasicInfoForm.name', 'ng-init'=>"busBasicInfoForm.name='$business->name'")) }}

    {{ Form::text('phone',$business->phone,array('ng-init'=>"busBasicInfoForm.phone='$business->phone'", 'ng-model'=>'busBasicInfoForm.phone','id' => 'phone','class'=>'input-xlarge input03','required','data-parsley-length'=>'[10,10]','pattern'=>'\D*([0]\d{1})(\D*)([1-9]\d{3})(\D*)(\d{4})\D*','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)','placeholder'=>'Business Phone')) }}
    
    {{ Form::text('email',$business->email,array('id' => 'email', 'class'=>'business-email input-xlarge input03','placeholder'=>'Business Email','ng-model'=>'busBasicInfoForm.email', 'ng-init'=>"busBasicInfoForm.email='$business->email'")) }}
  </div>
  <div class ='bottom_inputs'>
   <span>Human readable address</span>
   {{ Form::text('address_line_1',$business->address_line_1,array('ng-init'=>"busBasicInfoForm.address_line_1 ='$business->address_line_1'",'ng-model'=>'busBasicInfoForm.address_line_1','placeholder' => 'Address line 1','class'=>'input-xlarge input03','id' => 'address_line_1')) }}

   {{ Form::text('address_line_2',$business->address_line_2,array('ng-init'=>"busBasicInfoForm.address_line_2 ='$business->address_line_2'",'ng-model'=>'busBasicInfoForm.address_line_2','placeholder' => 'Address line 2','class'=>'input-xlarge input03','id' => 'address_line_2')) }}

   <span>Address on Google Maps</span>        
   <surburbselect suburbid = '{{ $business->suburb_id }}'></surburbselect>

   <div id="locationField">
     {{ Form::text('autocompleteField',$business->autocompleteaddress,array('class'=>'business-name input-xlarge input03', 'id' => 'autocomplete', 'onFocus' => 'geolocate()','placeholder'=>'Start typing your address here')) }}
   </div>
   <div id="map"></div>
   <div id ='address'>
    {{ Form::text('latitude',$business->latitude,array('class' => 'hidden','ng-init'=>"busBasicInfoForm.latitude='$business->latitude'",'ng-model'=>'busBasicInfoForm.latitude','id' => 'latitude')) }}


    {{ Form::text('longitude',$business->longitude,array('class' => 'hidden','ng-init'=>"busBasicInfoForm.longitude='$business->longitude'",'ng-model'=>'busBasicInfoForm.longitude','id' => 'longitude')) }}

    {{ Form::text('postcode',$business->postcode,array('class' => 'hidden','ng-init'=>"busBasicInfoForm.postcode='$business->postcode'",'ng-model'=>'busBasicInfoForm.postcode','id' => 'postcode')) }}

    {{ Form::text('autocompleteaddress',$business->autocompleteaddress,array('class' => 'hidden','ng-init'=>"busBasicInfoForm.autocompleteAddress='$business->autocompleteAddress'",'ng-model'=>'busBasicInfoForm.autocompleteAddress','id' => 'autocompleteAddress'))}}



  </div>

  <div class="alignc">
    {{ Form::submit('SAVE',array('class'=>'submit-button')) }}
    <?php /* {{ Form::submit('SAVE AND CONTINUE',array('class'=>'submit-button continue-submit')) }} */?>

  </div>
  {{ Form::close() }}
</div>
</div>
<?php if( !$business->latitude  ) {
  $business->latitude = '-33.8674869';
  $business->longitude = '151.20699020000006';
} ?>
<script>
  var mapDiv = document.getElementById('map');
  var map = new google.maps.Map(mapDiv, {
    center: {lat: {{$business->latitude}}, lng: {{$business->longitude}}},
    zoom: 13
  });
  markers = [];

  var postObject = {
   userId: id,
   csrf: $( '#_token').val(),
   businessId: business_id
 },
 cropTypes = {
   widescreen : true,
   letterbox: false,
   free: false
 }

 $("#container_image").PictureCut({
  InputOfImageDirectory       : "image",
  PluginFolderOnServer        : "assets/dashboard/js/jquery.picture.cut/",
  FolderOnServer              : "/uploads/businesses/",
  EnableCrop                  : true,
  EnableResize                : true,
  MaximumSize                 : 300,
  CropWindowStyle             : "Bootstrap",
  ActionToSubmitUpload        : "/services/busLogoUpload",
  ActionToSubmitCrop          :  "/services/busLogoCrop",
  DataPost                    : postObject,
  CropModes                   : cropTypes,
  CropOrientation             : false,
  MinimumWidthToResize        : 300,
  UploadedCallback            : function(image) {
   $( '#image').val(image.currentFileName).trigger('input');
 },
 @if ( $business->image)
 DefaultImageButton          : '/uploads/businesses/{{ $business->image }}',
 @else 
 DefaultImageButton          : "/assets/dashboard/images/upload.png"
 @endif
});
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    //  (document.getElementById('autocomplete')),
     // {types: ['geocode']});
     (document.getElementById('autocomplete')),
     {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  document.getElementById('autocompleteAddress').value = 
  place.address_components[0]['long_name'] + ' ' +
  place.address_components[1]['long_name'] + ', ' +
  place.address_components[2]['long_name'];

  $('#postcode').val(place.address_components[5]['long_name']);
  console.log(place.address_components[5]['long_name'] );
  document.getElementById('latitude').value = place.geometry.location.lat();
  document.getElementById('longitude').value = place.geometry.location.lng();
   // document.getElementById('postcode').value = place.geometry.location.administrative_area_level_2();

   $( '#latitude' ).trigger('input');
   $( '#longitude' ).trigger('input');
   $('#autocompleteAddress').trigger('input');
   $('#postcode').trigger('input');
   
   setMapOnAll(null);
   map.setCenter(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
   myLatlng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
   markers = new google.maps.Marker({position: myLatlng, map: map,title: '{{$business->name}}'});


 }
 function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]
initAutocomplete();

</script>