    <div class="splash-screen welcome-modal" id="welcomeModal">
        <div class="container alignc">
            <img src="{{ url('/assets/frontend/img/logo.png') }}" class="hidden-xs">
            <p>This website contains content that is sexual in nature. </p>
            <p>To enter this site, you must be an adult of the legal age of eighteen (18) or older.</p>
            <p>By clicking on the Enter link below, you confirm and understand that this site contains adult and explicit content and agree to release myplaymate.com.au from any liability that may arise from the use of this site.</p>
            <p>Unless all the above criteria are met, you are not eligible to enter the site and must leave immediately.</p>
            <br>
            <button type="button" class="btn submit-button">ENTER</button>
        </div>
    </div>