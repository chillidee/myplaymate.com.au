<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>MyPlaymate.com.au - Profile Approved</title>
</head>

<body bgcolor="#000">
	<!-- Visually Hidden Preheader Text: START -->
	<div style="display: none; font-size: 0.05; line-height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; position: absolute; top: -1000em; left: -1000em;">
		Your profiles have been approved by our team at MyPlaymate.com.au
	</div>
	<!-- Visually Hidden Preheader Text: END -->
	
<table align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#000" width="680" style="max-width: 680">
	<tr><td>
	<table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#000000" width="100%" style="max-width: 680">
		<tr>
			<td align="center">
				<a href="http://myplaymate.com.au/"><img src="http://myplaymate.com.au/assets/frontend/img/logo.png" width="80%" alt="myplaymate.com.au - Australia's Leading Directory for Escorts, Brothels and Agencies" style="max-width: 680; height: auto"></a>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 10 20 0px 20; text-align: center; font-family: 'Helvetican Neue', Helvetica, Arial, sans-serif; font-size: 18; color: #FFF;">
				<h2>Hi {{$data['full_name']}},</h2>
				<p>
					Your business and escort profiles have been approved and is now visible to the public.
				</p>
			
				<p>
					You may make changes at any time from your dashboard at MyPlaymate.com.au
				</p>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center" style="margin: auto;">
					<tr style="text-align: center;">
						<td align="center;" style="padding: 0px 10px 0px 10px">
							<a href="http://myplaymate.com.au/#login" style="background: #FF0004; border: 20px solid #FF0004; padding: 0 10px; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold; transition: ease-in; transition-delay: 100ms; transition-property: all">
								LOG IN NOW
							</a>
						</td>
						<br><br>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td bgcolor="#FFFFFF" align="center" width="100%">
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width: 680; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" bgcolor="#FFFFFF">
					<tr style="background-color: #000; font-size: x-small; color: #FFF; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
						<td style="padding: 10px">
							Get in touch with us at <a href="mailto:admin@myplaymate.com.au" style="text-decoration: underline; color: #FFFFFF;">admin@myplaymate.com.au</a> or <a href="tel:1300769766" style="text-decoration: underline; color: #FFFFFF;">1300 769 766</a>
						</td>
						
						<td align="right" style="padding: 10px">
							<a href="https://twitter.com/MyPlayMateAU" style="text-decoration: none; color: #FFFFFF; font-size: small"><img src="http://staging.myplaymate.com.au/assets/emails/Twitter.png" height="50%" width="auto" alt="twitter icon"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				
	</table>
</td></tr>	
</table>	
</body>
</html>
