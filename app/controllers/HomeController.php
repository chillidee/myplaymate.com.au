<?php

use MyPlaymate\Repositories\EscortDbRepository;
use MyPlaymate\Repositories\EscortUpdateRepository;
use MyPlaymate\Services\LocationService;

class HomeController extends \EscortController {

  protected $escortRepo;

  protected $updateRepo;

  protected $location;

  function __construct(EscortDbRepository $escortRepo, EscortUpdateRepository $updateRepo, LocationService $location)
  {
    $this->escortRepo = $escortRepo;
    $this->updateRepo = $updateRepo;
    $this->location = $location;
  }

  public function show_home()
  {
    $meta = Page::whereName('home')->first();
    $post = Post::orderBy('date_from','DESC')
                     ->wherePublished(1)
                     ->first();

    $pass = array( 
                           'meta' => $meta,
                           'latestPost' => $post
                         );
    $opt = Option::where('name','=','show_front_page_ads')->first();

    if ( isset( $opt ) && $opt->value == 1 ){

       $pass['ads'] = Advertisement::where('type','=','1' )->get();

    }

    return View::make('frontend.home',$pass);
  }

}
