<div id="backend-profile-site" class="alignc" >
    <h2>YOUR SITE</h2>
    <div class="row">
        <div class="col-md-12">

            <div class="triggerAnimation animated" data-animate="fadeInUp">
                <p>You can advertise your own website on your profile page.</p>
                {{ Form::open(array('id'=>'escortSite')) }}
                    <div class="alert alert-success">Escort info updated!</div>

                    <div class="elements">
                        {{ Form::label('use_own_url','Use your own website*',array('class'=>'control-label')) }}
                        <div class="controls">
                            {{ Form::select('use_own_url',array('No, I don\'t have a website','Yes'),$escort->use_own_url,array('id'=>'use-own-url')) }}
                        </div>
                    </div>
                    <div class="elements own-url">
                        {{ Form::label('own_url','Website URL*',array('class'=>'control-label')) }}
                        <div class="controls">
                            {{ Form::text('own_url',$escort->own_url,array('data-parsley-type'=>'url')) }}
                        </div>
                    </div>
                    {{ Form::hidden('escort_id',$escort->id) }}
                    {{ Form::hidden('next_icon','escort-reviews',array('class'=>'next-icon')) }}
                    {{ Form::hidden('next_route','escortReviews',array('class'=>'next-route')) }}
                    {{ Form::hidden('next_type','escort',array('class'=>'next-type')) }}
                    <div class="alignc section-content">
                        {{ Form::submit('UPDATE',array('class'=>'submit-button')) }}
                        {{ Form::submit('SAVE AND CONTINUE',array('class'=>'continue-submit submit-button')) }}
                    </div>
                {{ Form::close() }}

            </div><!-- .triggerAnimation.animated end -->
        </div><!-- .col-md-12 end -->
        <br /><br />
    </div><!-- .row end -->
</div><!-- End of Gallery-->

<script>
    (function($){
        $('.second-level h2').text('{{ $escort->escort_name }}');

        var useOwnUrl =  $('#use-own-url');
            ownUrlInput = $('.own-url');

        if (useOwnUrl.val() == 1){
            ownUrlInput.show();
            ownUrlInput.find('input').attr('required','required');
        }
        else{
            ownUrlInput.hide();
        }

        useOwnUrl.on('change', function(e) {
            if (useOwnUrl.val() == 1){
                ownUrlInput.slideDown();
                ownUrlInput.find('input').attr('required','required');            
            }
            else{
                ownUrlInput.slideUp();
                ownUrlInput.find('input').removeAttr('required');
            }
        });

    })(jQuery);
</script>


