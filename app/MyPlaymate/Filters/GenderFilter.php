<?php
namespace MyPlaymate\Filters;

use MyPlaymate\Filters\Filter;

class GenderFilter extends Filter
{
	public $column = 'gender_id';
}