<?php

use  Carbon\Carbon;
use MyPlaymate\Services\Meta;
use MyPlaymate\Services\FilterService;
use MyPlaymate\Repositories\EscortDbRepository;
use MyPlaymate\Services\LocationService;

class BusinessController extends Controller {

   protected $location;

  function __construct(LocationService $location)
  {
    $this->location = $location;
  }
    public function getBusiness($type, $url_key ) {
    $meta = Page::where('url_key','=',$url_key)->first();
    if (isset($_COOKIE['mpm_session']) && file_exists('temp/bus-1-' . $_COOKIE['mpm_session'] . '.js')) {
          $pass = array(
                        //   'session' =>  'temp/bus-1-' . $_COOKIE['mpm_session'] . '.js',
                           'meta' => $meta,
                           'business' => '1'
                         );
              //  return View::make('frontend.brothels.index',$pass );
          return View::make('frontend.business.index')
      ->with('business',1)
      ->with('meta',$meta)
      ->with('type',$type);
     }
     else {

    return View::make('frontend.business.index')
      ->with('business',1)
      ->with('meta',$meta)
      ->with('type',$type);
    }
    return $type;
  }
  public function brothels() {
     return $this->getBusiness('brothel','brothels');
  }
  public function agencies() {
     return $this->getBusiness('agencie','escort-agencies');
  }
  public function massageParlours() {
    return $this->getBusiness('massage-parlour','erotic-massage/massage-parlour');
  }
  public function showBusiness($slug) {
    $brothel = Business::whereSeoUrl($slug)->first();

    //  if(!$escort || $escort->active == 0 || $escort->status == 0 || $escort->status == 1  || $escort->status == 3)
    if(!$brothel )
    {
       $slugParts = explode('-', $slug);
       $brothelId = (int)array_pop($slugParts);

       if ($brothelId > 0)
         return Redirect::to('/brothel/'.$brothelId, 301);
       else
         return Redirect::to('/', 301);
    }
    if($brothel->status == 2 || $brothel->status == 4 ) {
           $distance = $this->location->getCurrentDistance($brothel, $this->location->getCurrentSuburb());

        //   $meta = new Meta($brothel->seo_title,$brothel->seo_keywords,$escort->seo_description);

           $reviews = BusinessReview::whereBusinessId($brothel->id)
                 ->whereStatus(2)
                 ->wherePublished(1)
                 ->get();
           $bodyClass = 'escort-single';

           $escorts = $brothel->getApprovedEscorts();
           foreach ($escorts as $key => $escort) {

          $escort->escortAge = $escort->age_id ? $escort->age->name : '';
         }

          $user = User::where( 'id','=',$brothel->user_id )->first();
          if ( !$user )
              $user = new User;

           if ( $brothel->status == 4 || $user->super_user != 1 && $user->business_plan == 8 || $user->business_plan == 9 ){
           if ( $brothel->type == 1 )
               $type = 'brothel';
           if ( $brothel->type == 2 )
               $type = 'agencie';
           if ( $brothel->type == 3 )
               $type = 'massage-parlour';

             $meta = new Meta($brothel->seo_title,$brothel->seo_keywords,$brothel->seo_description);

           $pass = array(
                'meta' => $meta,
                'brothel' => $brothel,
                'escorts' => 'true',
                'status' => 'unavailable',
                'type' => $type
           );
           return View::make('frontend.business.index',$pass);
        }
        else {
           $meta = new Meta($brothel->seo_title,$brothel->seo_keywords,$brothel->seo_description);
           return View::make('frontend.business.show',compact('brothel','reviews','escorts','meta', 'bodyClass','distance','meta'));
        }
    }
    else {
        return Redirect::to('/', 301);
    }

  }
   public function redirectToShow($id)
    {
      $business = Business::find($id);
      if (!$business)
        return Redirect::to('/',301);

           if ( $business->type == 1 )
               $url = 'brothels';
           if ( $business->type == 2 )
               $url = 'agencies';
           if ( $business->type == 3 )
               $url = 'massage-parlours';

    return Redirect::to('/'.$url . '/' . $business->seo_url,301);

    }
  public function businessProfilePreview($id){
       $brothel = Business::find($id);
    if ( !$brothel)
       return 'No escort found with that id';

    $escorts = $brothel->getApprovedEscorts();
           foreach ($escorts as $key => $escort) {

          $escort->escortAge = $escort->age_id ? $escort->age->name : '';
         }
    $reviews = BusinessReview::whereBusinessId($brothel->id)
                 ->whereStatus(2)
                 ->wherePublished(1)
                 ->get();
           $bodyClass = 'escort-single';

    $preview = 1;

    $meta = new Meta($brothel->seo_title,$brothel->seo_keywords,$brothel->seo_description);

    $bodyClass = 'escort-single';

    return View::make('frontend.business.show',compact('brothel','escorts','reviews','preview'));
  }
  public function loadBrothels(){
    return $this->loadBusinesses('1');
  }
  public function loadAgencies(){
    return $this->loadBusinesses('2');
  }
  public function loadMassage(){
    return $this->loadBusinesses('3');
  }
  public function loadBusinesses($type)
  {
    $input = Input::all();

    // Neutralize empty filters
    foreach ($input as $key => $value) {
      if (!isset($value))
         $value == '';
    }
    // Sydney Fallback
    if (!isset($input['latitude']) || $input['latitude'] == ''){
         $input['latitude'] = '-33.8693574';
         $input['longitude'] = '151.1109774';
    }


    // Display only the approved escorts
    $businesses = Business::whereStatus(2)->where('type','=',$type)->orderBy('bumpup_date','desc');

    $businesses = $businesses->get();

    // Get coordinates

    $take = array();
    $save = array();
    $i = 0;
    foreach ($businesses as $key => $business) {

      $user = User::where('id','=',$business->user_id )->first();
    if ( $user && $user->super_user != 1 ){
         $plan = BusinessPlan::where('id','=',$user->business_plan )->first();
         if ( $plan ){
            if ( $plan->id == 8 || $plan->id == 9 )
                 continue;
         }
         else {
            continue;
         }
    }
    if ( !$user )
       continue;
   /*   if ( $business->seo_url == '') {
        $business->seo_url = $business->getSeoUrl();
        $business->save();
      }*/
   //   $business->businessLocation = $this->location->getCurrentLocationName($business, $input['suburb_id']);
    //  $business->distance = $this->location->getCurrentDistance($business, $input['suburb_id']);

      $business->distance = $this->location->calculateDistance($business->latitude, $business->longitude, $input['latitude'], $input['longitude'] );

      if ( !isset( $input['distance'] )) $input['distance'] = 50;

      if ($business->distance < $input['distance']) {

      if ( $i < 60 ) {
           $take[] = $business;
      }
      else {
           $save[] = $business;
      }
        $i++;
}
}
    $filter_string = '';
    $function_string = '';
    foreach( $input as $key => $filter ) {
      if ( $filter != '' && $key  != 'sorting' && $key  != 'suburb_id' && $key != 'longitude' && $key != 'lock' && $key != 'latitude' && $key != 'distance' && $key != 'business' && $key  != 'city' && $key  != 'page' && $key  != 'paginationCount') {
        $filter_string.= ucfirst(str_replace ('_',' ',$key)) .  ' : ';
        if ( is_array( $filter ) ) {
        foreach ( $filter as $value ) {
          if ( is_array ( $value ) ) {
             $i = 0;
             foreach ( $value as $a => $value ) {
               if ( $i < 1 ) {
                 $filter_string.= $value . ' - ';
                 $function_string .= 'addUsedFilter($( "#' . $value;
                 if ( $key == 'hourly_rates' )
                      $function_string .= '-';
               }
               else {
                 $filter_string.= $value . ' | ';
                 $function_string .= $value . '"));';
               }
               $i++;
              }
          }
          else {
            $function_string .= 'addUsedFilter($( "#' .  str_replace ('+','',str_replace ('/','', str_replace (' ', '-', strtolower($value) ) ) ). '" ) );';
            $filter_string.= $value. ' | ';}
        }
      }
    }
  }
       $sorted = array(
           'show' => $take,
           'save' => $save,
           'filters' => substr($filter_string, 0, -3)
      );
  /*  if ( !isset( $_COOKIE['mpm_session'] ) || $_COOKIE['mpm_session'] == '' ) {
          function generateRandomString($length = 10) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                   for ($i = 0; $i < $length; $i++) {
                      $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
            }
          $lar = generateRandomString(15);
      setcookie('mpm_session',$lar,time() + 3600,'/'); // i hour
    }
    else { $lar = $_COOKIE['mpm_session']; }
    if ( isset($input['city'] ) && $input['city']  != '' ) $lar = $input['city']  . '-' . $lar;
    if( isset($input['business'] )&& $input['business'] != '' ) $lar = 'bus-' . $input['business']  . '-' . $lar;
    $handle = fopen('temp/' .$lar . '.js', 'w') or die('Cannot open file: temp/' .$lar . '.js');
    $data = '
              window.check = ' . json_encode ($sorted) . '
              jQuery(document).ready(function($) {
                  $( "#filter_information" ).html( "' . substr($filter_string, 0, -3) . '" );
                  ' . $function_string . '
              });';
            fwrite($handle, $data);
            fclose($handle);*/
      return json_encode ($sorted);
  }

/*   public function brothels()
  {
    $meta = Page::whereName('brothels')->first();
    if (isset($_COOKIE['mpm_session']) && file_exists('temp/bus-1-' . $_COOKIE['mpm_session'] . '.js')) {
          $pass = array(
                           'session' =>  'temp/bus-1-' . $_COOKIE['mpm_session'] . '.js',
                           'meta' => $meta,
                           'business' => '1'
                         );
                return View::make('frontend.escorts.index',$pass );
     }
     else {

    return View::make('frontend.escorts.index')
      ->with('business',1)
      ->with('meta',$meta);
  }
}*/

  /* public function brothels($city = null)
  { if ( $city == null ) {
    $meta = Page::whereName('brothels')->first();

    return View::make('frontend.escorts.index')
      ->with('business',1)
      ->with('meta',$meta);
  }
    if ( $city == 'sydney' ) {
                     $get = Page::select('*')
                       ->where('url_key', '=', $city)
                       ->first();
                       $meta = new meta( $get->meta_title, $get->meta_keywords, $get->meta_description );
                       $pass = array(
                           'name' =>    $get->name,
                           'city' =>    $city,
                           'content' => $get->content,
                           'city_page' => '1',
                           'main_image' => $get->main_image,
                           'meta' => $meta

                         );
                //  return View::make('frontend.escorts.city',compact('name','suburb_id','meta','content', 'city'));
                return View::make('frontend.escorts.city',$pass )
                       ->with('business',1);
                 }
                $cities = array(
                    'brisbane', 'melbourne','perth', 'adelaide', 'tasmania', 'act', 'canberra', 'darwin', 'hobart' );
                if ( !in_array( $city, $cities ) )
                        return Response::make('Page not found',404);
    $suburb_id = Suburb::whereName($city)->first()->id;

    if ($city == 'perth')
      $suburb_id = 9140;

    $meta = Page::whereName($city)->first();
    return View::make('frontend.escorts.index',compact('city','suburb_id','meta'))
                ->with('business',1);
  }*/
  public function getGirl($id) {
      $escort = Escort::where('id','=',$id)->first();
      $business = Business::where( 'id','=',$escort->business_id )->first();
      $escort->images = $escort->images()->get();
      $escort->escortAge = $escort->age_id ? $escort->age->name : '';
      return View::make('frontend.partials.showEscort', compact( 'escort','business' ) );
  }

/* public function agencies()
  {
    $meta = Page::whereName('agencies')->first();
       if (isset($_COOKIE['mpm_session']) && file_exists('temp/bus-2-' . $_COOKIE['mpm_session'] . '.js')) {
          $pass = array(
                           'session' =>  'temp/bus-2-' . $_COOKIE['mpm_session'] . '.js',
                           'meta' => $meta,
                           'business' => '2'
                         );
                return View::make('frontend.escorts.index',$pass );
       }
        else {

    return View::make('frontend.escorts.index')
      ->with('business',2)
      ->with('meta',$meta);
    }
  }*/
  public function sendPlayMail()
    {
        $input = Input::all();

        $business = Business::find($input['escort_id']);
        $success = $business->sendPlayMail($input);

       return $success;
        return 'Playmail sent correctly to '.$business->name.'!';
    }

  public function massage()
  {
    $meta = Page::whereName('Massages')->first();
    if (isset($_COOKIE['mpm_session']) && file_exists('temp/bus-3-' . $_COOKIE['mpm_session'] . '.js')) {
          $pass = array(
                           'session' =>  'temp/bus-3-' . $_COOKIE['mpm_session'] . '.js',
                           'meta' => $meta,
                           'business' => '3'
                         );
                return View::make('frontend.escorts.index',$pass );
       }
        else {

    return View::make('frontend.escorts.index')
      ->with('business',3)
      ->with('meta',$meta);
  }
}
}
