<?php

namespace spec\MyPlaymate\Filters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GenderFilterSpec extends ObjectBehavior
{
	private $escorts = [
		['gender_id' => 1],
		['gender_id' => 2],
		['gender_id' => 1],
		['gender_id' => 3],
    ];

    function it_is_initializable()
    {
        $this->shouldHaveType('MyPlaymate\Filters\GenderFilter');
    }

    function it_shows_only_males()
    {
    	$this->apply($this->escorts,[2])->shouldHaveCount(1);
    }

    function it_shows_males_and_females()
    {
    	$this->apply($this->escorts,[1,2])->shouldHaveCount(3);
    }

    function it_shows_males_and_trans()
    {
    	$this->apply($this->escorts,[2,3])->shouldHaveCount(2);
    }
}









