(function($){

$.extend(true, window.Parsley, {
  asyncValidators: {
    'default': {
      fn: function (xhr) {
        return 'resolved' === xhr.state();
      },
      url: false
    },
    reverse: {
      fn: function (xhr) {
        return 'rejected' === xhr.state();
      },
      url: false
    }
  },

  addAsyncValidator: function (name, fn, url, options) {
    window.Parsley.asyncValidators[name.toLowerCase()] = {
      fn: fn,
      url: url || false,
      options: options || {}
    };

    return this;
  },

  eventValidate: function (event) {
    if (new RegExp('key').test(event.type))
      if (!this._ui.validationInformationVisible && this.getValue().length <= this.options.validationThreshold)
        return;

    this._ui.validatedOnce = true;
    this.whenValidate();
  }
});

window.Parsley.addValidator('remote', {
  requirementType: {
    '': 'string',
    'validator': 'string',
    'reverse': 'boolean',
    'options': 'object'
  },

  validateString: function (value, url, options, instance) {
    var
      data = {},
      ajaxOptions,
      csr,
      validator = options.validator || (true === options.reverse ? 'reverse' : 'default');

    if ('undefined' === typeof window.Parsley.asyncValidators[validator])
      throw new Error('Calling an undefined async validator: `' + validator + '`');

    data[instance.$element.attr('name') || instance.$element.attr('id')] = value;

    var remoteOptions = $.extend(true, options.options || {} , window.Parsley.asyncValidators[validator].options);

    ajaxOptions = $.extend(true, {}, {
      url: window.Parsley.asyncValidators[validator].url || url,
      data: data,
      type: 'GET'
    }, remoteOptions);

    csr = $.param(ajaxOptions);
 
    if ('undefined' === typeof window.Parsley._remoteCache)
      window.Parsley._remoteCache = {};

    var xhr = window.Parsley._remoteCache[csr] = window.Parsley._remoteCache[csr] || $.ajax(ajaxOptions);

    var handleXhr = function() {
      var result = window.Parsley.asyncValidators[validator].fn.call(instance, xhr, url, options);
      if (!result) // Map falsy results to rejected promise
        result = $.Deferred().reject();
      return $.when(result);
    };

    return xhr.then(handleXhr, handleXhr);
  },

  priority: -1
});

window.Parsley.on('form:submit', function () {
  window.Parsley._remoteCache = {};
});

window.ParsleyExtend.addAsyncValidator = function () {
  window.ParsleyUtils.warnOnce('Accessing the method `addAsyncValidator` through an instance is deprecated. Simply call `window.Parsley.addAsyncValidator(...)`');
  return window.Parsley.apply(window.Parsley.addAsyncValidator, arguments);
};

})(jQuery);