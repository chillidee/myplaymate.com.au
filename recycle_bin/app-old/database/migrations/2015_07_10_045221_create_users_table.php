<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type');
			$table->string('email');
			$table->string('password');
			$table->string('full_name');
			$table->string('address_line_1');
			$table->string('address_line_2');
			$table->string('suburb');
			$table->string('postcode');
			$table->string('state');
			$table->string('phone');
			$table->string('mobile');
			$table->boolean('active');
			$table->string('activation_code');
			$table->boolean('super_user');
			$table->string('remember_token');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
