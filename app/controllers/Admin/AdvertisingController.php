<?php
namespace Admin;
use Controller, Auth, View, Helpers, Datatables, Input, ImageHelper, Carbon\Carbon,Config, Advertisement,Option;
use Post, PostCategory;

class AdvertisingController extends Controller {
  public function adSpaces()
  {
    $ads = Advertisement::where('type','=','1' )->get();
    $opt = Option::where('name','=','show_front_page_ads')->first();
    return View::make('admin.advertising.index',compact('ads','opt'));
  }
  public function saveFrontPageAds() {
     $user = Auth::user();
      if ( !$user || !$user->super_user )
            die();

      $input = Input::all();

      function base64_to_jpeg($base64_string, $filename ) {

           $output_file = Config::get('constants.PUBLIC_DIR') . '/uploads/advertising/' . $filename;

           $ifp = fopen($output_file, "wb"); 

           $data = explode(',', $base64_string);

           fwrite($ifp, base64_decode($data[1])); 
           fclose($ifp); 

           return $data[1];
      }
      $fileNames = array();

      $i = 0;

      foreach( $input as $ad ){
        if ( !isset( $ad['image']))
             continue;
         $i++;
         $fileName = str_random(12) . '.jpg';
         $img = base64_to_jpeg( $ad['image'], $fileName );
         $fileNames[] = $img;
         $adv = Advertisement::where('order','=',$i )->where('type','=','1' )->first();
         if ( !$adv )
            $adv = new Advertisement;
         $adv->type = 1;
         $adv->order = $i;
         $adv->name = $ad['url'];
         $adv->url = $ad['url'];
         $adv->image_file = $fileName;
         $adv->img_alt = $ad['img_alt'];
         $adv->save();
      }
      return $fileNames;
    }
    public function showFrontPageAds() {
      $user = Auth::user();
      if ( !$user || !$user->super_user )
            die();
      $input = Input::all();
      $opt = Option::where('name','=','show_front_page_ads')->first();
        if ( !$opt )
            $opt = new Option;
        $opt->name = 'show_front_page_ads';
        $opt->value = $input['display'];
        $opt->save();
    }
}