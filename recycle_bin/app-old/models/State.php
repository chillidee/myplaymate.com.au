<?php

class State extends Eloquent
{

    protected $guarded = [];

    public $timestamps = false;

    public static $rules = [

    ];

    public function country()
    {
        return $this->belongsTo('Country', 'country_id');
    }

    public function suburbs()
    {
        return $this->hasMany('Suburb', 'state_id');
    }
}
