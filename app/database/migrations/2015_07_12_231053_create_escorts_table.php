<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEscortsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escorts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->boolean('under_business');
			$table->integer('status');
			$table->integer('approved_by');
			$table->timestamp('approved_at');
			$table->integer('disapproved_by');
			$table->timestamp('disapproved_at');
			$table->boolean('active');
			$table->timestamp('active_from');
			$table->timestamp('active_to');
			$table->string('seo_url');
			$table->string('own_url');
			$table->boolean('use_own_url');
			$table->string('escort_name');
			$table->integer('main_image_id');
			$table->string('phone');
			$table->string('mobile');
			$table->string('email');
			$table->string('state');
			$table->string('licence_number');
			$table->boolean('contact_playmail');
			$table->boolean('contact_phone');
			$table->boolean('does_incalls');
			$table->boolean('does_outcalls');
			$table->boolean('does_travel_internationally');
			$table->boolean('does_smoke');
			$table->boolean('tattoos');
			$table->boolean('piercings');
			$table->text('about_me');
			$table->integer('age_id');
			$table->integer('gender_id');
			$table->integer('body_id');
			$table->integer('height_id');
			$table->integer('hair_color_id');
			$table->integer('eye_color_id');
			$table->integer('hourly_rate_id');
			$table->integer('ethnicity_id');
			$table->integer('suburb_id');
			$table->integer('current_suburb_id');
			$table->string('current_lat');
			$table->string('current_lng');
			$table->string('seo_title');
			$table->string('seo_keywords');
			$table->string('seo_description');
			$table->integer('count_phone');
			$table->integer('count_playmail');
			$table->integer('count_views');
			$table->integer('count_pictures');
			$table->text('notes');
			$table->text('seo_content');
			$table->string('facebook');
			$table->string('twitter');
			$table->string('instagram');
			$table->integer('agency_id');
			$table->boolean('featured');
			$table->integer('step');
			$table->string('banner');
			$table->boolean('use_banner');
			$table->integer('rank');
			$table->timestamp('bump_date');
			$table->text('about_review');
			$table->boolean('reported');
			$table->timestamp('reported_date');
			$table->boolean('archived');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('escorts');
	}

}
