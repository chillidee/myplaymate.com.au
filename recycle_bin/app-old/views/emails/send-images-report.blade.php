@extends('emails.layout')

@section('content')

<!-- One Column -->
<table class=deviceWidth style="width: 580px;" align=center bgcolor=#eeeeed border=0 cellpadding=0 cellspacing=0>
	<tbody>
		<tr>
			<td style="padding: 0;" bgcolor=#ffffff valign=top>
				<a href="https://www.myplaymate.com.au/">
					<img class="deviceWidth" style="display: block; border-radius: 4px;" src="http://www.myplaymate.com.au/assets/emails/main-updated-4.png" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td style="font-size: 13px; color: #202022; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding: 10px 8px 10px 8px;" bgcolor="#eeeeed">
				<table bgcolor="#eeeeed">
					<tbody>
						<tr>
							<td style="padding: 0 10px 10px 0;" valign=middle>
								<a style="text-decoration: none; color: #272727; font-size: 16px; font-weight: bold; font-family: Arial, sans-serif;" href=#>
									Hi {{ $escort->escort_name }},
								</a>
							</td>
						</tr>
					</tbody>
				</table>
				<p>Your new images have been reviewed.</p>

				<p><span style="color: #339966;">These images have been approved:</span></p>
				@if(isset($input['approved_images']))
					@foreach($input['approved_images'] as $image)
						<img src="{{ EscortImage::find($image[0])->thumbnail(170,170) }}" alt="">
					@endforeach
				@endif

				<p></p>

				<p><span style="color: #ff0000;">These have been disapproved:</span></p>
				@if(isset($input['disapproved_images']))
					@foreach($input['disapproved_images'] as $image)
						<div style="float:left;width:170px">
							<img src="{{ EscortImage::find($image[0])->thumbnail(170,170) }}" alt="">
							<p>Reason: {{ $image[1] }}</p>
						</div>
		
					@endforeach
				@endif
			
				<p style="clear:both"></p>

				<p>The My Playmate Team</p>
				<p>We hope you enjoy creating your profile,</p>
				<p>The My Playmate Team</p>
			</td>
		</tr>
	</tbody>
</table> <!-- End One Column -->

@stop