<div id="basic-info">
    {{ Form::open(['id'=>'newEscort']) }}
        <h2 class="alignc">BASIC INFO</h2>

        <div class="control-group">
            {{ Form::label('escort_name','Escort Name*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('escort_name','',['class'=>'','required']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('phone','Phone*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('phone','',['class'=>'','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)','data-parsley-pattern-message'=>'A valid australian phone number is required. (eg 0411223344 or 0211223344)']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('email','Email*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('email','',['class'=>'','data-parsley-required','data-parsley-type'=>'email']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('suburb_id','Current location*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::select('suburb_id',$suburbs,'',['class'=>'select2 form-control input-large input03','data-parsley-required','min'=>'1','data-parsley-min-message'=>'Please select a suburb.','style'=>'width:100%']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('licence_number','Licence number (if applicable)',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::text('licence_number','',['class'=>'']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('age_id','Age*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::select('age_id',$ages,'',['class'=>'form-control']) }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('height_id','Height*',['class'=>'control-label']) }}

            <div class="controls">
                {{ Form::select('height_id',$heights,'',['class'=>'form-control']) }}
            </div>
        </div>

        <div class="control-group" id="sex">
            <div>
                {{ Form::radio('gender_id',1, 0,['id'=>'gender_id-1']) }}
                {{ Form::label('gender_id-1','Female') }}
            </div>
            <div>
                {{ Form::radio('gender_id',2, 0,['id'=>'gender_id-2']) }}
                {{ Form::label('gender_id-2','Male') }}
            </div>
            <div>
                {{ Form::radio('gender_id',3, 0,['id'=>'gender_id-3','data-parsley-required']) }}
                {{ Form::label('gender_id-3','Transexual') }}
            </div>
        </div>
        <div class="clear" style="clear:both;"><br /></div>

        <div class="control-group">
            {{ Form::label('contact_phone','Contact by Phone/SMS*',['class'=>'control-label']) }}
            <div>
                {{ Form::radio('contact_phone',1,0,['id'=>'contact_phone-yes']) }}
                {{ Form::label('contact_phone-yes','Yes') }}
            </div>
            <div>
                {{ Form::radio('contact_phone',0,1,['id'=>'contact_phone-no']) }}
                {{ Form::label('contact_phone-no','No') }}
            </div>
        </div>
        <div class="control-group">
            {{ Form::label('contact_playmail','Contact by PlayMail*',['class'=>'control-label']) }}
            <div>
                {{ Form::radio('contact_playmail',1, 0,['id'=>'contact_playmail-yes']) }}
                {{ Form::label('contact_playmail-yes','Yes') }}
            </div>
            <div>
                {{ Form::radio('contact_playmail',0, 1,['id'=>'contact_playmail-no']) }}
                {{ Form::label('contact_playmail-no','No') }}
            </div>
        </div>
        {{ Form::hidden('business_id','',['class'=>'business-id']) }}
        {{ Form::hidden('new_escort','',['class'=>'new-escort']) }}
        {{ Form::hidden('next_icon','escort-about',['class'=>'next-icon']) }}
        {{ Form::hidden('next_route','escortAbout',['class'=>'next-route']) }}
        {{ Form::hidden('next_type','escort',['class'=>'next-type']) }}
        <div class="alignc section-content">
            {{ Form::submit('SAVE AND CONTINUE',['class'=>'submit-button continue-submit']) }}
        </div>

    {{ Form::close() }}
</div>

<script>
    (function($){
        $('.second-level h2').text('New escort');

        if ($('.firstLevel')) {};
    })(jQuery);
</script>