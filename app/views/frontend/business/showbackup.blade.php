@extends('frontend.layout')

@section('content')
<script>window.businessName = '{{$brothel->name}}';</script>
<div class ='showPage'>
  @if( $brothel->banner_image && $brothel->banner_approved == 2 || isset( $preview ) && $preview == 1 )
  <img class ='full_width' src = '{{ Config::get('constants.CDN')}}/businesses/{{$brothel->banner_image}}'>
  @endif
  <div class ='container'>
   <div class ='row'>
     <div class ='col-md-12'>
      @if($brothel->image != '' )
      <img src="{{ Config::get('constants.CDN')}}/businesses/{{ $brothel->image }}" alt="{{ $brothel->name }}" class ="showPageLogo @if( $brothel->cover ) coverLogo @endif ui">
      @endif
    </div>
    <div class ='col-md-9'>
      @if( !isset( $brothel->image ) || $brothel->image == '' )
      <h1>{{$brothel->name}}</h1>
      @endif
      @if( $brothel->address_line_1 )
      <h3>{{$brothel->address_line_1}} 
        @if( isset( $brothel->address_line_2 ))
        {{$brothel->address_line_2 }}
        @endif
        @if( isset( $brothel->suburb ))
        {{$brothel->suburb }}
        @endif
        @if( isset( $brothel->state ))
        {{$brothel->state }}
        @endif
      </h3>
      @endif
      <p>{{nl2br($brothel->about )}}</p>
    </div>
    <div class ='col-md-3 showPageSidebar'>
  <?php /*   <div id ='reviewHolder'>
      @if( $brothel->review)
      @else 
      <?php for( $i= 0; $i < 5; $i++ ){ ?>
      <span class = 'star'></span>
      <?php }?>
      <span id ='revNot'>There are no reviews.  <a data-toggle="modal" href="#leaveReviewModal">Add review</a></span>
      @endif */?>
      
      <div class ='actionPane actionPlay'data-toggle="modal" href="#contactEscortModal">Contact</div>
      @if( isset( $brothel->latitude )) <div class ='actionPane actionPhone'><a href ='#directions' onclick ="ga('send', 'event', 'Business', 'Directions', '{{$brothel->name}}' )";>Get Directions</a></div>@endif 
      @if( isset( $brothel->website )) <div class ='actionPane'><a href ='http://{{$brothel->website}}' onclick ="ga('send', 'event', 'Business', 'Website click', '{{$brothel->name}}' )">Visit Website</a></div>@endif 
    </div>
  </div>
</div>
</div>
@if( count( $escorts )> 0 )
<div id ='showEscort'>
  <div id ='escortHolder'></div>
</div>
<div id ='otherGirls'>
  <div class ='container'>
    <div class ='row'>
      <div class ='col-md-12'>
        <h2>Our Girls</h2>
        <div class ='row'>
          <div class ='escort-container grid-view showPage clear'>
            @foreach ( $escorts as $escort ) 
            @if( $escort->status == 2 && $escort->main_image )
            <li class="col-xs-12 col-ms-6 col-md-4 col-lg-3 col-xl-15 col-xl-3 escorts-wishlist clone" data-distance="0">
             <figure class="escorts-img-container business-page-escort-container" id="escort-{{$escort->id}}">
              <div class="info-container">
                <div class="figinfo">
                  <span class="figname">{{$escort->escort_name}}</span>
                  <span class="figdata">
                    <span class="escort-age">Age: {{$escort->escortAge}}</span>
                  </span>
                </div><!-- figinfo-->
              </div><!-- end of info-container-->
              <img class="img-profile" src="{{ Config::get('constants.CDN') }}/escorts/thumbnails/thumb_220x330_{{$escort->main_image}}" alt="" data-id ="{{$escort->id}}">  
            </figure>
          </li>
          @endif
          @endforeach
        </div>
      </div>
    </div>
  </div><!-- End of Row -->
</div>
</div>
@endif
@if( $brothel->latitude )
<div class ='container'>
  <div id ='directions'>
    <div>
      @if( isset( $brothel->address_line_1 ))
      Address: {{$brothel->address_line_1}} 
      @endif
      @if( isset( $brothel->address_line_2 ))
      {{$brothel->address_line_2 }}
      @endif
      @if( isset( $brothel->suburb ))
      {{$brothel->suburb }}
      @endif
      @if( isset( $brothel->state ))
      {{$brothel->state }}
      @endif
    </div>
    <div id ='map'></div>
  </div>
  @endif
</div><!-- End of profile Details -->
</div>
<script>
  window.activeId = {{$brothel->id}};
  @if ( $brothel->latitude )
  window.mapPage = 1;
  @else 
  window.mapPage = 0;
  @endif
  window.getGirls = 1;
  window.mapName ={{json_encode( $brothel->name )}};
  @if ( $brothel->latitude )
  window.center = {lat: {{$brothel->latitude}}, lng: {{$brothel->longitude}}};
  @endif
</script>
@include ( 'frontend.partials.businessModal' )
@stop













