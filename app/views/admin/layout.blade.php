<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin - My Play Mate</title>
        <link rel="stylesheet" href="{{ asset('assets/admin/css/dropzone.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.min.css') }}">
        <link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
        <link rel="stylesheet" href="{{ asset('assets/admin/css/style.default.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}" type="text/css" />

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/jquery-ui-1.10.3.min.js') }}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.cookie.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/lib/select2.full.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/lib/dropzone.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/tinymce/jquery.tinymce.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/wysiwyg.js') }}"></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script src ="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.10/sorting/datetime-moment.js"></script>
        <script src="{{ asset('/assets/frontend/js/lib/parsley.min.js') }}"></script>
        <script src="{{ asset('/assets/admin/js/functions.js') }}"></script>
        {{-- <script type="text/javascript" src="{{ asset('assets/admin/js/custom.js') }}"></script> --}}
        @yield('javascript')
    </head>
    <body {{ isset($bodyClass) ? 'class="'.$bodyClass.'"' : '' }}>
        <div id="mainwrapper" class="mainwrapper">
            <div class="header">
                <div class="logo">
                    <a href="/admin"><img src="{{ asset('assets/frontend/img/logo.png') }}" alt="myplaymate.com.au" /></a>
                </div>
                <span class ='dashTopLink'><a href ='/user/profile'>Go to User Dashboard</a></span>
                <div class="headerinner">
                    <ul class="headmenu">
                        <li class="right">
                            <div class="userloggedinfo">
                                <div class="userinfo">
                                    <h5>{{ Auth::user()->full_name }} <small>- {{ Auth::user()->email }}</small></h5>
                                    <ul>
                                        <li><a href="{{ url('admin/logout') }}">Log Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="leftpanel">
            <?php $pending_business_banners = Business::where('banner_image','!=', '')->where('banner_approved','=','1')->count();
                  $pending_escort_banners = Escort::where('banner_image','!=', '')->where('banner_approved','=','1')->count();
                  $pending_escort_images = array();
                  $images_waiting = EscortImage::whereStatus(1)->get();
                  foreach( $images_waiting as $image ){
                       $escort = Escort::find( $image->escort_id );
                          if( $escort )
                              $pending_escort_images[] = $escort;
                 }
                  $pending_banners = $pending_business_banners + $pending_escort_banners + count( $pending_escort_images ); ?>
                <div class="leftmenu">
                    <ul class="nav nav-tabs nav-stacked">
                        <li><a href="{{ url('admin') }}"><span class="fa fa-laptop"></span> Dashboard</a></li>
                        <li><a href="{{ url('admin/pending_images') }}"><span class="fa fa-picture-o"></span> Pending images <span class="badge" id = "pendingImagesBubble">{{ $pending_banners > 0 ? $pending_banners : '' }}</span></a></li>
                        <li><a href="{{ url('admin/pending_escorts') }}"><span class="fa fa-intersex"></span> Pending Escorts <span class="badge" id = "pendingEscortsBubble">{{ Escort::whereStatus(1)->count() > 0 ? Escort::whereStatus(1)->count() : '' }}</span></a></li>
                        <li><a href="{{ url('admin/pending_businesses') }}"><span class="fa fa-money"></span> Pending Businesses<span class="badge" id ="pendingBusinessesBubble">{{ Business::whereStatus(1)->count() > 0 ? Business::whereStatus(1)->count() : '' }}</span></a></li>
                        <li><a href="{{ url('admin/users') }}"><span class="fa fa-user"></span> Users</a></li>
                        <li><a href="{{ url('admin/escorts') }}"><span class="fa fa-intersex"></span> All Escorts <span class="fa fa-caret-down icon-float-right"></span></a>
                           <ul class ='sub-menu'>
                               <li><a href="{{ url('admin/pending_escorts') }}"><span class="fa fa-intersex"></span> Pending Escorts </a></li>
                               <li><a href="{{ url('admin/deleted-escorts') }}"><span class="fa fa-remove"></span> Deleted Escorts</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('admin/businesses') }}"><span class="fa fa-money"></span> Businesses</a></li>
                        <li><a href="{{ url('admin/deleted-businesses') }}"><span class="fa fa-remove"></span> Deleted Businesses</a></li>
                        <li><a href="{{ url('admin/plans') }}"><span class="fa fa-money"></span>Plans</a></li>
                        <li><a href="{{ url('admin/pages') }}"><span class="fa fa-file-text-o"></span> Pages <span class="fa fa-caret-down icon-float-right"></span></a>
                          <ul class ='sub-menu'>
                              <li><a href="{{ url('admin/city-pages') }}"><span class="fa city_icon"></span> City Pages</a></li>
                              <li><a href="{{ url('admin/brothel-pages') }}"><span class="fa brothel_icon"></span> Brothel Pages</a></li>
                          </ul>
                              <li><a href="{{ url('admin/advertising-spaces') }}"><span class="fa fa-puzzle-piece"></span> Advertising Spaces</a></li>
                              <li><a href="{{ url('admin/media-library') }}"><span class="fa fa-picture-o"></span> Media Library</a></li>
                              <li><a href="{{ url('admin/blog') }}"><span class="fa fa-newspaper-o"></span>  Blog <span class="fa fa-caret-down icon-float-right"></span></a>
                            <ul class ='sub-menu'>
                              <li><a href="{{ url('admin/blog/categories') }}">Categories</a></li>
                              <li><a href="{{ url('admin/blog/tags') }}">Tags</a></li>
                      </ul></li>

{{--                         <li><a href="{{ url('admin/updates') }}"><span class="fa fa-comment"></span> Sexy updates <span class="badge">{{ EscortUpdate::whereStatus(1)->count() > 0 ? EscortUpdate::whereStatus(1)->count() : ''; }}</span></a></li> --}}
                        <li><a href="{{ url('admin/reviews') }}"><span class="fa fa-comment"></span> Reviews <span class="badge">{{ EscortReview::whereStatus(1)->count() > 0 ? EscortReview::whereStatus(1)->count() : ''; }}</span></a></li>
                        <li><a href="{{ url('admin/comments') }}"><span class="fa fa-comment"></span> Comments <span class="badge">{{ Comment::whereStatus(1)->count() > 0 ? Comment::whereStatus(1)->count() : ''; }}</span></a></li>
                        {{--<li><a href="{{ url('admin/users') }}"><span class="fa fa-user"></span> Administrators</a></li>
                    <li><a href="{{ url('admin/emailtemplates') }}"><span class="fa fa-envelope"></span> Email Templates</a></li>
                    <li><a href="{{ url('admin/settings') }}"><span class="fa fa-envelope"></span> Settings</a></li> --}}
                    <li><a href="{{ url('admin/admin-export') }}"><span class="fa fa-file-text-o"></span> Export Escort Data</a></li>
                    </ul>
                </div><!--leftmenu-->
            </div><!-- leftpanel -->
            <div class="rightpanel">
                <div class="right-wrapper container-fluid">
                    <div class="alert alert-success"></div>

                    @yield('content')

                </div>
            </div><!--rightpanel-->
        </div><!--mainwrapper-->

        @yield('modals')

    </body>
    <script>
    $( '.fa-caret-down').on('click',function(e) {
      e.preventDefault();
      $submenu = $( this ).parents('li').children('.sub-menu' ).toggle();

    })
    </script>
</html>
