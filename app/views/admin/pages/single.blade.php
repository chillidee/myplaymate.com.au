@extends('admin.layout')

@section('javascript')

<script type="text/javascript">
    (function($){
        $('textarea.tinymce').tinymce();
    })(jQuery);
</script>

@stop

@section('content')

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-file-text-o"></span></div>
    <div class="pagetitle">
        <h1>Edit page - {{ $page->name }}</h1>
    </div>
</div><!--pageheader-->

@if(!empty($message))
<div class="alert alert-success">{{ $message }}</div>
@endif
{{ Form::open(array('files' => true)) }}
<div class="row">
  <div class="col-md-6">
    <div class="control-group">
        {{ Form::label('name','Name*',array('class'=>'control-label')) }}

        <div class="controls">
            {{ Form::text('name',$page->name,array('class'=>'form-control','required')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('meta_title','SEO title',array('class'=>'control-label')) }}

        <div class="controls">
            {{ Form::text('meta_title',$page->meta_title,array('class'=>'form-control')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('meta_keywords','SEO keywords',array('class'=>'control-label')) }}

        <div class="controls">
            {{ Form::text('meta_keywords',$page->meta_keywords,array('class'=>'form-control')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('meta_description','SEO description',array('class'=>'control-label')) }}

        <div class="controls">
            {{ Form::text('meta_description',$page->meta_description,array('class'=>'form-control')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('url','SLUG',array('class'=>'control-label')) }}

        <div class="controls">
            {{ Form::text('url',$page->url_key,array('class'=>'form-control')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('url','TEMPLATE',array('class'=>'control-label')) }}

        <?php $templates = array( 'full_width' => 'Full Width','sidebar' => 'Sidebar' );?>

        <div class="controls">
            {{ Form::select('template',$templates,array('class'=>'form-control')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('img_alt','IMAGE ALT TAG',array('class'=>'control-label')) }}

        <div class="controls">
            {{ Form::text('img_alt',$page->img_alt, array('class'=>'form-control')) }}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="control-group">
        {{ Form::label('main_image','Main Image',array('class'=>'control-label')) }}

        <div class="controls">
          <input type="file" id="main_image" name ="main_image"/>
      </div>
  </div>
</div>
@if ( isset($page->main_image ) && $page->main_image != '')
<img src = "{{Config::get('constants.CDN') .'/' . str_replace ( '/uploads/', '', $page->main_image )}}" class = "preview_image">
@endif
</div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="control-group">
            {{ Form::label('top_content','Top Content',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('top_content',$page->top_content,array('class'=>'tinymce')) }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="control-group">
            {{ Form::label('content','Content',array('class'=>'control-label')) }}

            <div class="controls">
                {{ Form::textarea('content',$page->content,array('class'=>'tinymce')) }}
            </div>
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="control-group" id="incalls">
        <h5>PUBLISHED</h5>
        <label class="radio-inline">{{ Form::radio('published', '1', $page->published==1 ? true : false) }} Yes
        </label>
        <label class="radio-inline">{{ Form::radio('published', '0', $page->published==0 ? true : false) }} No
        </label>
    </div>
</div>
{{ Form::hidden('page_id',$page->id) }}

<div class="alignc">
    {{ Form::submit('SAVE',array('class'=>'btn btn-primary section-content  ')) }}
</div>
<style>
  .preview_image {
    width: 60%;
}
{{ Form::close() }}
@stop
