<div id="topbar-buttons">
	<li class="topbar-buttons-style"><a href="{{ url('advertise') }}" style="background-color: rgba(208, 10, 10, 0.8); color: #FFF; padding: 5px 20px 5px 20px;">Advertise With Us</a></li>
    
	@if(Auth::guest())
    	<li class="topbar-buttons-style"><a href="#login" data-toggle="modal">Login</a></li>
    @endif
	
    @if(!Auth::guest())
    	<li class="topbar-buttons-style"><a href="/logout">Logout</a></li>
  		<li class="topbar-buttons-style"><a href ='/user/profile'>Your account</a></li>
    @endif
	
    @if(Auth::guest())
        <li class="topbar-buttons-style"><a id="sign-up" href="#signup" data-toggle="modal">Sign Up</a></li>
    @endif
  		
	<li id="geoloc" style="vertical-align: middle">
		<div id="trigger-overlay">
        <div id="city" class="clearfix">
        <div id="city-changer">
        	<i class="fa fa-map-marker"></i>
        	<a></a>
        </div>
		</div>
        </div>
</div>