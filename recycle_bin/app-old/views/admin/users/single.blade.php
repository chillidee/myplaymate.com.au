@extends('admin.layout')

@section('content')
	<div class="pageheader">
	    <div class="pageicon"><span class="fa fa-user"></span></div>
	    <div class="pagetitle">
	        <h1>Edit user - {{ $user->full_name }}</h1>
	    </div>
	</div><!--pageheader-->
	
	@if($message_success)
		<div class="section-content message alert alert-success">{{ $message_success }}</div>
	@endif

    {{ Form::open(array('id' => 'edit-user')) }}
        <div class="control-group">
            {{ Form::label('full_name','Full name',array('class'=>'control-label')) }}<span>*</span>
            <div class="controls">
                {{ Form::text('full_name', $user->full_name, array('class' => 'form-control','data-parsley-required')) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('email','Email',array('class'=>'control-label')) }}<span>*</span>
            <div class="controls">
                {{ Form::text('email', $user->email, array('class' => 'form-control','required','data-parsley-type'=>'email')) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('phone','Phone',array('class'=>'control-label')) }}<span>*</span>
            <div class="controls">
                {{ Form::text('phone', $user->phone, array('class' => 'form-control','required','data-parsley-length'=>'[10,10]','pattern'=>'0\d{9}(?!.)')) }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('type','Type',array('class'=>'control-label')) }}<span>*</span>
            <div class="controls">
                {{ Form::select('type', [1=>'Customer',2=>'Escort/Business'],$user->type, array('class' => 'form-control','required')) }}
            </div>
        </div>

		<div class="checkbox">
    		<label for="active">
				{{ Form::checkbox('active', 1, $user->active == 1 ? true : false) }} Active
    		</label>
		</div>
		<div class="checkbox">
    		<label for="super_user">
				{{ Form::checkbox('super_user',1, $user->super_user == 1 ? true : false) }} Administrator
    		</label>
		</div>

        {{ Form::hidden('user_id',$user->id) }}

        <div class="section-content">
            {{ Form::submit('UPDATE',array('class'=>'btn btn-primary')) }}
        </div>
    {{ Form::close() }}

	<script>
        jQuery(document).ready(function($) {
            $('#edit-user').parsley();
            $('.message').show();
        });
    </script>	
@stop